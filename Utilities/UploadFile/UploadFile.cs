﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Utilities.UploadFile
{ 
    public  class UploadFile
    {
        public static string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }



        private static Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformats"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"},
                {".zip", "application/zip"},
                {".rar", "application/x-rar-compressed"},

                {".TXT", "text/plain"},
                {".PDF", "application/pdf"},
                {".DOC", "application/vnd.ms-word"},
                {".DOCX", "application/vnd.ms-word"},
                {".XLS", "application/vnd.ms-excel"},
                {".XLSX", "application/vnd.openxmlformats"},
                {".PNG", "image/png"},
                {".JPG", "image/jpeg"},
                {".JPEG", "image/jpeg"},
                {".GIF", "image/gif"},
                {".CSV", "text/csv"},
                {".ZIP", "application/zip"},
                {".RAR", "application/x-rar-compressed"},
            };
        }

        public static bool IsExtensionValid(string filename)
        {
            UploadFile uf = new UploadFile();
            var spilitName = filename.Split('.');
            var ext = '.' + spilitName.Last();
            var types = GetMimeTypes();
            var exists = types.Any(x => x.Key == ext);
            if (exists)
                return true;
            return false;
        }
        public static string[] GetSplitFileName(string filename)
        {

            var SplitName = filename.Split('.');
            string Extension = SplitName[SplitName.Length - 1];
            string Name = "";
            for (int i = 0; i < SplitName.Length - 1; i++)
            {
                Name += SplitName[i];
            }
            string[] str = new string[] { Name, '.' + Extension };
            return str;
        }
        public static string GetFileName(string filename)
        {

            var SplitName = filename.Split('_');
            string MainFileName = "";
            for (int i = 0; i < SplitName.Length - 1; i++)
            {
                MainFileName += SplitName[i];
            }
            var SplitExtension = filename.Split('.');
            string Extension = SplitExtension[SplitExtension.Length - 1];
            return MainFileName + '.' + Extension;
        }
        public static int[] GetSplitId(string BindId)
        {

            var str = BindId.Split('-');
            int[] SplitId = new int[] { Convert.ToInt32(str[0]), Convert.ToInt32(str[1]) };
            return SplitId;
        }
    }
}
