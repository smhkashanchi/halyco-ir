﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public class ElasticEmails
    {
        
        public void Configuration(string Recipients,string Body)
        {
            NameValueCollection values = new NameValueCollection();
            values.Add("apikey", "3b928004-3485-47a4-80ab-9cde3c50bfab");
            values.Add("from", "amirhosseink.7795@gmail.com");
            values.Add("fromName", "ITSACO");
            values.Add("to", Recipients); //"recipient1@gmail.com;recipient2@gmail.com");
            values.Add("subject", "تبلیغات");
            values.Add("bodyText", "سلام همراه سایت دانا کدنگار !");
            values.Add("bodyHtml", Body);
            values.Add("isTransactional", "true");

            string address = "https://api.elasticemail.com/v2/email/send";

            string response = Send(address, values);
        }
        static string Send(string address, NameValueCollection values)
        {
            using (WebClient client = new WebClient())
            {
                try
                {
                    byte[] apiResponse = client.UploadValues(address, values);
                    return Encoding.UTF8.GetString(apiResponse);

                }
                catch (Exception ex)
                {
                    return "Exception caught: " + ex.Message + "\n" + ex.StackTrace;
                }
            }
        }
    }
}
