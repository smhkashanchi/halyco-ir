﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Utilities
{
    public static class Slug
    {
        public static string ToSlug(this string Value)
        {
            Value=Value.Replace(" ", "-");
            //Value = Value.Insert(Value.Length, "/");
            return Value;
        }
        public static string ToTitle(this string Value)
        {
            return Value.Replace("-", " ");
        }
    }
}
