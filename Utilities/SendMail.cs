﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Utilities
{
    public static class SendMail
    {
        public static void Send(string From,string To, string Subject, string Body,string Name,string filePath)
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            mail.From = new MailAddress(From, Name);
            mail.To.Add(To);
            mail.Subject = Subject;
            mail.Body = Body;
            mail.IsBodyHtml = true;

            Attachment attachment;
            attachment = new Attachment(filePath);
            mail.Attachments.Add(attachment);


            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential("amirhosseink.7795@gmail.com", "amir1997");
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);

        }


        public static void SendMails(string mailFrom, string mailTo, string subject, string mailText,string Name)
        {
            SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 25);

            smtpClient.Credentials = new System.Net.NetworkCredential("amirhosseink.7795@gmail.com", "amir1997");
            smtpClient.UseDefaultCredentials = true;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.EnableSsl = true;
            MailMessage mail = new MailMessage();

            //Setting From , To and CC
            mail.From = new MailAddress(mailFrom,Name);
            mail.To.Add(new MailAddress(mailTo));
            mail.Body = mailText;

            smtpClient.Send(mail);
        }
    }
}
