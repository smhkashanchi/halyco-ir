﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public class SendMultiSMS
    {
        public void Config(string[] recpts,string userName,string password,string message,string from)
        {

            WebRequest request = WebRequest.Create("http://ippanel.com/services.jspd");
            //string[] rcpts = new string[] { "989100000009" };
            string json = JsonConvert.SerializeObject(recpts);
            request.Method = "POST";
            string postData = "op=send&uname="+userName+"&pass="+password+"&message="+message+"&to=" + json + "&from="+from+"";
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            WebResponse response = request.GetResponse();
            Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            Console.WriteLine(responseFromServer);
            reader.Close();
            dataStream.Close();
            response.Close();
            System.Diagnostics.Debug.WriteLine(responseFromServer);
        }
    }
}
