﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Utilities.Dictionary
{
}
public class ControllerDictionary
{
    public ControllerDictionary()
    {
        ControllerName_Dic.Add("Color", "رنگ");
        ControllerName_Dic.Add("Connection", "راه های ارتباطی");
        ControllerName_Dic.Add("NewsLetters", "اشتراک ایمیل");
        ControllerName_Dic.Add("ContactUsMessage", "پیام های ارتباطی");
        ControllerName_Dic.Add("FAQ", "سوالات متداول");
        ControllerName_Dic.Add("FilesManage", "دانلود ها و ویدئو ها");
        ControllerName_Dic.Add("Gallery", "گالری");
        ControllerName_Dic.Add("Roles", "مدیریت نقش");
        ControllerName_Dic.Add("Language", "زبان");
        ControllerName_Dic.Add("Location", "شهر و استان");
        ControllerName_Dic.Add("Products", "محصولات");
        ControllerName_Dic.Add("Features", "ویژگی");
        ControllerName_Dic.Add("Garanty", "گارانتی");
        ControllerName_Dic.Add("Contents", "اخبار و مطالب آموزشی");

        ControllerName_Dic.Add("SlideShow", "اسلایدشو");
        ControllerName_Dic.Add("Catalog", "کاتالوگ");
        ControllerName_Dic.Add("SMS_Subscribe", "اشتراک پیامک");
        ControllerName_Dic.Add("SocialNetwork", "شبکه های اجتماعی");
        ControllerName_Dic.Add("Users", "مدیریت کاربران");
        ControllerName_Dic.Add("AboutUs", "درباره ما");
        ControllerName_Dic.Add("CoWorker", "گواهینامه ها و افتخارات");
        ControllerName_Dic.Add("Projects", "پروژه");
        ControllerName_Dic.Add("Unit", "واحد");
        ControllerName_Dic.Add("SeoPageProduct", "سئو");
        ControllerName_Dic.Add("SeoPage", "سئو");
        ControllerName_Dic.Add("Off", "تخفیف");
        ControllerName_Dic.Add("Producer", "تولیدکننده");
        ControllerName_Dic.Add("OfficePost", "پست های اداری");
        ControllerName_Dic.Add("OfficeUnit", "واحدهای اداری");
        ControllerName_Dic.Add("Employee", "کارکنان");
        ControllerName_Dic.Add("Vacation", "مرخصی");
        ControllerName_Dic.Add("Mission", "ماموریت");

        ControllerName_Dic.Add("MissionSP", "ماموریت");
        ControllerName_Dic.Add("MissionSuperior", "لیست ماموریت کارمندان");

        ControllerName_Dic.Add("Missions", "ماموریت قدیمی");
        ControllerName_Dic.Add("MissionsRequest", "درخواست ماموریت قدیمی");
        ControllerName_Dic.Add("MissionSuprior", "درخوست های ماموریت بخش");
        ControllerName_Dic.Add("MissionManager", "درخواست های ماموریت واحد");
        ControllerName_Dic.Add("MissionHRManager", "درخواست های ماموریت شرکت");
        ControllerName_Dic.Add("MissionsAllEmployees", "لیست ماموریت های همه کارمندان");
        ControllerName_Dic.Add("Sizes", "پست های اداری");
        ControllerName_Dic.Add("HalycoManagers", "هیئت مدیره و مدیران اجرایی");
        ControllerName_Dic.Add("ActivityField", "زمینه فعالیت");
        ControllerName_Dic.Add("Activity", "فعالیت");
        ControllerName_Dic.Add("VacationRequest", "مرخصی های درخواستی");
        ControllerName_Dic.Add("altVacationRequest", "مرخصی همکاران");
        ControllerName_Dic.Add("VRsuperior", "درخواست مرخصی کارمندان(مافوق)");
        ControllerName_Dic.Add("VRHR", "درخواست مرخصی کارمندان(مدیر)");
        ControllerName_Dic.Add("VacationHRManager", "درخواست مرخصی کارمندان(م.م.انسانی)");
        ControllerName_Dic.Add("Vacationnew", "مرخصی");
        ControllerName_Dic.Add("VacationCoworkernew", "جایگزین مرخصی همکاران");
        ControllerName_Dic.Add("VacationSuperiornew", "درخواست های مرخصی بخش");
        ControllerName_Dic.Add("VacationManagernew", "درخواست های مرخصی واحد");
        ControllerName_Dic.Add("VacationHRManagernew", "درخواست های مرخصی شرکت");
        ControllerName_Dic.Add("Suppliers", "تامین کنندگان");
        ControllerName_Dic.Add("Complaint", "شکایات مشتریان");
        ControllerName_Dic.Add("Proposal", "فرم ارائه پیشنهادات");
        ControllerName_Dic.Add("adminProposals", "لیست پیشنهاد ها (دسترسی مدیر)");
        ControllerName_Dic.Add("VacationSuperior", "لیست مرخصی کارمندان");
        ControllerName_Dic.Add("VacationsAllEmployee", "لیست مرخصی های همه کارمندان");


        ControllerName_Dic.Add("MainSurvey", "نظرسنجی اصلی");
        ControllerName_Dic.Add("SurveyQuestionGroup", "گروه سوالات نظرسنجی");
        ControllerName_Dic.Add("SurveyQuestion", " سوالات نظرسنجی");
        ControllerName_Dic.Add("SurveyAnswerType", "نوع جواب نظرسنجی");
        ControllerName_Dic.Add("SurveyQuestionReply", " جواب سوال نظرسنجی");

        ControllerName_Dic.Add("EmployeeSurvey", "فرم نظرسنجی کارکنان");

        ControllerName_Dic.Add("EmployeeSalary", "فیش حقوقی کارمندان");
        ControllerName_Dic.Add("EmployeeSalaryUpload", "آپلود فیش حقوقی کارمندان");

        ControllerName_Dic.Add("Employements", "لیست پیشنهاد دهندگان استخدام");
        ControllerName_Dic.Add("ActivityOrgItems", "فعالیت های سازمان (استخدام)");
        ControllerName_Dic.Add("ComputerSkills", "مهارت های کامپیوتری (استخدام)");

        ControllerName_Dic.Add("ShiftChange", "تعویض شیفت");
        ControllerName_Dic.Add("ShiftChangeSuperior", "درخواست های تعویض شیفت کارمندان");
        ControllerName_Dic.Add("ShiftChangesAllEmployees", "لیست تعویض شیفت های همه کارمندان");
        ControllerName_Dic.Add("ShiftCoworker", "تعویض شیفت همکاران");
        
        ControllerName_Dic.Add("ShiftSuprior", "درخواست های تعویض شیفت بخش");
        ControllerName_Dic.Add("ShiftManager", "درخواست های تعویض شیفت واحد");
        ControllerName_Dic.Add("ShiftHRManager", "درخواست های تعویض شیفت شرکت");

        ControllerName_Dic.Add("ExtraWork", "اضافه کاری");
        ControllerName_Dic.Add("ExtraWorkSuperior", "درخواست اضافه کاری کارمندان");
        ControllerName_Dic.Add("ExtraWorksAllEmployees", "لیست اضافه کاری های همه کارمندان");
        
        ControllerName_Dic.Add("ExtraWorkManager", "درخواست های اضافه کاری واحد");
        ControllerName_Dic.Add("ExtraWorkHRManager", "درخواست های اضافه کاری شرکت");
        ControllerName_Dic.Add("Loan", "درخواست وام");
        ControllerName_Dic.Add("LoanSuperior", "درخواست های وام بخش");
        ControllerName_Dic.Add("LoanManager", "درخواست های وام واحد");
        ControllerName_Dic.Add("LoanHRManager", "درخواست های وام شرکت");

        HideController_List.Add("ContactDev");
        HideController_List.Add("Account");
        HideController_List.Add("Manage");
        HideController_List.Add("Home");
        HideController_List.Add("SeoPage");
        HideController_List.Add("SeoPageProduct");
        HideController_List.Add("Sizes");
       // HideController_List.Add("Vacation");
        HideController_List.Add("VacationRequest");
        HideController_List.Add("altVacationRequest");
        HideController_List.Add("VRsuperior");
        HideController_List.Add("VRHR");
        HideController_List.Add("VacationHRManager");




        HidePermission_Dic.Add("Account");
        HidePermission_Dic.Add("Manage");
        HidePermission_Dic.Add("Home");
        HidePermission_Dic.Add("SeoPageProduct");
        //HidePermission_Dic.Add("SeoPage");
        HidePermission_Dic.Add("EnginerDesign");
        HidePermission_Dic.Add("ImportaionPart");
        HidePermission_Dic.Add("Material");
        //HidePermission_Dic.Add("Vacation");
        HidePermission_Dic.Add("VacationRequest");
        HidePermission_Dic.Add("altVacationRequest");
        HidePermission_Dic.Add("VRsuperior");
        HidePermission_Dic.Add("VRHR");
        HidePermission_Dic.Add("VacationHRManager");



        //**********************
        //Common Actions
        //HideAction_List.Add("checkboxSelected");
        //HideAction_List.Add("createTreeView");
        //HideAction_List.Add("ViewInTreeView");

        //NewsLetter
        HideAction_List.Add("ViewAllMemberOfNewsLetterByGroupId");
        HideAction_List.Add("SendMail");
        HideAction_List.Add("ViewAllGroupList");


        //Connection
        HideAction_List.Add("ViewAllCnGroupList");

        //Content
        HideAction_List.Add("ViewCnGroupData");
        //HideAction_List.Add("checkboxSelectedComments");
        HideAction_List.Add("CreateContentsTag");
        HideAction_List.Add("EditContentsTag");
        HideAction_List.Add("GetAllGalleryByGroupId");
        HideAction_List.Add("DeleteFilesInProj");
        HideAction_List.Add("ViewCnGroupData");

        //Feature
        HideAction_List.Add("SetIsDefault");
        HideAction_List.Add("GetAllFeatureByProductGroupId");
        HideAction_List.Add("SetIsDefault");

        //ContactUs
        HideAction_List.Add("ViewAllCMG");
        HideAction_List.Add("createTreeView");

        //FileManage
        HideAction_List.Add("GetAllGroupsByLang");
        HideAction_List.Add("DeleteFilesInProj2");
        HideAction_List.Add("DeleteFilesInProj");

        //Gallery
        HideAction_List.Add("IsExist");
        HideAction_List.Add("RemoveFile");
        HideAction_List.Add("SaveToMainFolder");
        HideAction_List.Add("RemoveFileFromTemp");
        HideAction_List.Add("RemoveTempFiles");
        HideAction_List.Add("ViewAllGroupGallery");


        //Language
        HideAction_List.Add("GetAllLangForLayoutPage");

        //Location
        HideAction_List.Add("GetAllState");
        HideAction_List.Add("GetAllCountry");

        //Off
        HideAction_List.Add("ViewAllOffList");

        //Producer
        HideAction_List.Add("ViewAllProducerList");

        //Product 
        HideAction_List.Add("CreateProduct");
        HideAction_List.Add("EditProduct");
        HideAction_List.Add("GetProductImageSizesViewComponent");
        HideAction_List.Add("GetAllColorForAdd");

        //SlideShow
        HideAction_List.Add("ViewAllSSG");
        HideAction_List.Add("ViewAllProducerList");
        HideAction_List.Add("DeleteFiles");

        //Unit
        HideAction_List.Add("DeleteFilesInProj");

        //Catalog
        HideAction_List.Add("ViewAllUnitList");

        //Sms
        HideAction_List.Add("ViewAllMemberOfSms");
        HideAction_List.Add("SendSMS");

        //Project
        HideAction_List.Add("GetAllVideoByIdInProject");
        HideAction_List.Add("createTreeViewVideo");


        Product_SubMenu.Add("Color");
        Product_SubMenu.Add("Features");
        Product_SubMenu.Add("Garanty");
        Product_SubMenu.Add("Off");
        Product_SubMenu.Add("Unit");
        Product_SubMenu.Add("Producer");
        Product_SubMenu.Add("Products");

        Connention_SubMenu.Add("ContactUsMessage");
        Connention_SubMenu.Add("Connection");
        Connention_SubMenu.Add("SocialNetwork");

        Content_SubMenu.Add("AboutUs");
        Content_SubMenu.Add("Contents");
        Content_SubMenu.Add("Catalog");


        BaseInformation_SubMenu.Add("OfficePost");
        BaseInformation_SubMenu.Add("OfficeUnit");
        BaseInformation_SubMenu.Add("ActivityField");
        BaseInformation_SubMenu.Add("Activity");


        Employee_SubMenu.Add("VacationRequest");
        Employee_SubMenu.Add("Vacation");
        //Employee_SubMenu.Add("VacationSuperior");
        Employee_SubMenu.Add("altVacationRequest");
        Employee_SubMenu.Add("MissionSP");
        //Employee_SubMenu.Add("MissionSuperior");
        Employee_SubMenu.Add("ExtraWork");
        //Employee_SubMenu.Add("ExtraWorkSuperior");
        Employee_SubMenu.Add("ShiftChange");
        //Employee_SubMenu.Add("ShiftChangeSuperior");

        EmployementRequestMySection_SubMenu.Add("VacationSuperior");
        EmployementRequestMySection_SubMenu.Add("MissionSuperior");
        EmployementRequestMySection_SubMenu.Add("ExtraWorkSuperior");
        EmployementRequestMySection_SubMenu.Add("ShiftChangeSuperior");

        Employee_SubMenu.Add("Loan");
        Employee_SubMenu.Add("Vacationnew");


        EmployeeCoworker_SubMenu.Add("ShiftCoworker");
        EmployeeCoworker_SubMenu.Add("VacationCoworkernew");

        EmployementRequestMySection_SubMenu.Add("VRsuperior");
        EmployementRequestMyUnit_SubMenu.Add("VRHR");
        EmployementRequestMyCompany_SubMenu.Add("VacationHRManager");
       
        //EmployementRequestMySection_SubMenu.Add("ExtraWorkSuperior");
        EmployementRequestMyUnit_SubMenu.Add("ExtraWorkManager");
        EmployementRequestMyCompany_SubMenu.Add("ExtraWorkHRManager");
        EmployementRequestMyCompany_SubMenu.Add("ExtraWorksAllEmployees");
        EmployementRequestMySection_SubMenu.Add("LoanSuperior");
        EmployementRequestMyUnit_SubMenu.Add("LoanManager");
        EmployementRequestMyCompany_SubMenu.Add("LoanHRManager");
        EmployementRequestMySection_SubMenu.Add("ShiftSuprior");
        EmployementRequestMyUnit_SubMenu.Add("ShiftManager");
        EmployementRequestMyCompany_SubMenu.Add("ShiftHRManager");
        EmployementRequestMyCompany_SubMenu.Add("ShiftChangesAllEmployees");
        EmployementRequestMySection_SubMenu.Add("MissionSuprior");
        EmployementRequestMyUnit_SubMenu.Add("MissionManager");
        EmployementRequestMyCompany_SubMenu.Add("MissionHRManager");
        EmployementRequestMyCompany_SubMenu.Add("MissionsAllEmployees");
        EmployementRequestMySection_SubMenu.Add("VacationSuperiornew");
        EmployementRequestMyUnit_SubMenu.Add("VacationManagernew");
        EmployementRequestMyCompany_SubMenu.Add("VacationHRManagernew");
        EmployementRequestMyCompany_SubMenu.Add("VacationsAllEmployee");

        Survey_SubMenu.Add("MainSurvey");
        Survey_SubMenu.Add("EmployeeSurvey");
        Survey_SubMenu.Add("SurveyQuestionReply");
        Survey_SubMenu.Add("SurveyAnswerType");
        Survey_SubMenu.Add("SurveyQuestion");
        Survey_SubMenu.Add("SurveyQuestionGroup");


        Employement_SubMenu.Add("Employements");
        Employement_SubMenu.Add("ActivityOrgItems");
        Employement_SubMenu.Add("ComputerSkills");

       
        

        MultiMedia_SubMenu.Add("SlideShow");
        MultiMedia_SubMenu.Add("Gallery");
        MultiMedia_SubMenu.Add("FilesManage");

        AccessControl_SubMenu.Add("Users");
        AccessControl_SubMenu.Add("Roles");
        AccessControl_SubMenu.Add("Employee");

        Halyco_SubMenu.Add("HalycoManagers");
        Halyco_SubMenu.Add("CoWorker");

        Salary_SubMenu.Add("EmployeeSalary");
        Salary_SubMenu.Add("EmployeeSalaryUpload");

        Proposal_SubMenu.Add("adminProposals");
        Proposal_SubMenu.Add("Proposal");

        Suppliers_SubMenu.Add("Suppliers");

        Customer_SubMenu.Add("Complaint");
        /////////////Action/////////////

        Actions_List.Add("NewsLetters", new List<string>
        {
            "ViewAllMemberOfNewsLetterByGroupId",
            "ViewAllGroupList"
        });



    }



    public Dictionary<string, string> ControllerName_Dic = new Dictionary<string, string>();
    public List<string> HideController_List = new List<string>();
    public List<string> HidePermission_Dic = new List<string>();
    public Dictionary<string, string> ActionName_Dic = new Dictionary<string, string>();
    public List<string> HideAction_List = new List<string>();

    public Dictionary<string, List<string>> Actions_List = new Dictionary<string, List<string>>();
    public List<string> Product_SubMenu = new List<string>();
    public List<string> Connention_SubMenu = new List<string>();
    public List<string> Content_SubMenu = new List<string>();
    public List<string> Employee_SubMenu = new List<string>();
    public List<string> EmployeeCoworker_SubMenu = new List<string>();
    public List<string> Survey_SubMenu = new List<string>();
    public List<string> BaseInformation_SubMenu = new List<string>();
    public List<string> Employement_SubMenu = new List<string>();
    public List<string> EmployementRequestMySection_SubMenu = new List<string>();
    public List<string> EmployementRequestMyUnit_SubMenu = new List<string>();
    public List<string> EmployementRequestMyCompany_SubMenu = new List<string>();
    public List<string> MultiMedia_SubMenu = new List<string>();
    public List<string> AccessControl_SubMenu = new List<string>();
    public List<string> Halyco_SubMenu = new List<string>();
    public List<string> Salary_SubMenu = new List<string>();
    public List<string> Proposal_SubMenu = new List<string>();
    public List<string> Suppliers_SubMenu = new List<string>();
    public List<string> Customer_SubMenu = new List<string>();


    public static string FindControllerName(string Key)
    {
        ControllerDictionary cd = new ControllerDictionary();
        var con = cd.ControllerName_Dic.SingleOrDefault(m => m.Key == Key);
        if (con.Value == "" || con.Value == null) return "؟؟؟؟؟؟؟";
        else return con.Value;
    }
    public static bool HideController(string Key)
    {
        ControllerDictionary cd = new ControllerDictionary();
        return (cd.HideController_List.Any(x => x == Key));
    }
    public static bool HidePermission(string Key)
    {
        ControllerDictionary cd = new ControllerDictionary();
        return (cd.HidePermission_Dic.Any(x => x== Key));
    }
    /////
    /////
    ///
    public static string FindNameAction(string Key)
    {
        ControllerDictionary cd = new ControllerDictionary();
        return (cd.ActionName_Dic.Single(m => m.Key == Key).Value);
    }
    public static bool HideAction(string Key)
    {
        ControllerDictionary cd = new ControllerDictionary();
        return (cd.HideAction_List.Any(x => x == Key));
    }
    ////Actions Access ////

    public static List<string> ReturnActions(string Key)
    {
        ControllerDictionary cd = new ControllerDictionary();
        var list = cd.Actions_List.Where(x => x.Key == Key).Select(x => x.Value).FirstOrDefault();
        return (list);
    }
    public static bool IsSubMenu_Product(string Key)
    {
        ControllerDictionary cd = new ControllerDictionary();
        return (cd.Product_SubMenu.Any(x => x == Key));
    }
    public static bool IsSubMenu_Connention(string Key)
    {
        ControllerDictionary cd = new ControllerDictionary();
        return (cd.Connention_SubMenu.Any(x => x == Key));
    }
    public static bool IsSubMenu_Content(string Key)
    {
        ControllerDictionary cd = new ControllerDictionary();
        return (cd.Content_SubMenu.Any(x => x == Key));
    }
    public static bool IsSubMenu_BaseInformation(string Key)
    {
        ControllerDictionary cd = new ControllerDictionary();
        return (cd.BaseInformation_SubMenu.Any(x => x == Key));
    }
    public static bool IsSubMenu_Employee(string Key)
    {
        ControllerDictionary cd = new ControllerDictionary();
        return (cd.Employee_SubMenu.Any(x => x == Key));
    }
    public static bool IsSubMenu_Survey(string Key)
    {
        ControllerDictionary cd = new ControllerDictionary();
        return (cd.Survey_SubMenu.Any(x => x == Key));
    }
    public static bool IsSubMenu_Employement(string Key)
    {
        ControllerDictionary cd = new ControllerDictionary();
        return (cd.Employement_SubMenu.Any(x => x == Key));
    }

    public static bool IsSubMenu_EmployeeCoworker(string Key)
    {
        ControllerDictionary cd = new ControllerDictionary();
        return (cd.EmployeeCoworker_SubMenu.Any(x => x == Key));
    }

    public static bool IsSubMenu_EmployementRequestMySection(string Key)
    {
        ControllerDictionary cd = new ControllerDictionary();
        return (cd.EmployementRequestMySection_SubMenu.Any(x => x == Key));
    }

    public static bool IsSubMenu_EmployementRequestMyUnit(string Key)
    {
        ControllerDictionary cd = new ControllerDictionary();
        return (cd.EmployementRequestMyUnit_SubMenu.Any(x => x == Key));
    }

    public static bool IsSubMenu_EmployementRequestMyCompany(string Key)
    {
        ControllerDictionary cd = new ControllerDictionary();
        return (cd.EmployementRequestMyCompany_SubMenu.Any(x => x == Key));
    }

    public static bool IsSubMenu_MultiMedia(string Key)
    {
        ControllerDictionary cd = new ControllerDictionary();
        return (cd.MultiMedia_SubMenu.Any(x => x == Key));
    }

    public static bool IsSubMenu_AccessControl(string Key)
    {
        ControllerDictionary cd = new ControllerDictionary();
        return (cd.AccessControl_SubMenu.Any(x => x == Key));
    }

    public static bool IsSubMenu_Halyco(string Key)
    {
        ControllerDictionary cd = new ControllerDictionary();
        return (cd.Halyco_SubMenu.Any(x => x == Key));
    }
    public static bool IsSubMenu_Salary(string Key)
    {
        ControllerDictionary cd = new ControllerDictionary();
        return (cd.Salary_SubMenu.Any(x => x == Key));
    }
    public static bool IsSubMenu_Proposal(string Key)
    {
        ControllerDictionary cd = new ControllerDictionary();
        return (cd.Proposal_SubMenu.Any(x => x == Key));
    }
    public static bool IsSubMenu_Suppliers(string Key)
    {
        ControllerDictionary cd = new ControllerDictionary();
        return (cd.Suppliers_SubMenu.Any(x => x == Key));
    }
    public static bool IsSubMenu_Customer(string Key)
    {
        ControllerDictionary cd = new ControllerDictionary();
        return (cd.Customer_SubMenu.Any(x => x == Key));
    }
}

