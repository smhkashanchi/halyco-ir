﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Utilities.Dictionary
{
    public class IconDictionary
    {
        public IconDictionary()
        {
          
            IconPermission_Dic.Add("NewsLetters", "fa fa-envelope");
            IconPermission_Dic.Add("FAQ", "fa fa-info-circle");
            IconPermission_Dic.Add("FilesManage", "fa fa-video");
            IconPermission_Dic.Add("Roles", "fa fa-lock");
            IconPermission_Dic.Add("Gallery", "fa fa-image");
            IconPermission_Dic.Add("Language", "fa fa-list");
            IconPermission_Dic.Add("Location", "fa fa-map-marker-alt");
            IconPermission_Dic.Add("SlideShow", "fa fa-sliders-h");
            IconPermission_Dic.Add("SMS_Subscribe", "fa fa-envelope");
            IconPermission_Dic.Add("SocialNetwork", "fa fa-paper-plane");
            IconPermission_Dic.Add("Users", "fa fa-user");
            IconPermission_Dic.Add("Projects", "fa fa-cog");
            IconPermission_Dic.Add("CoWorker", "fa fa-user");

            IconPermission_Dic.Add("Color", "fa fa-brush");
            IconPermission_Dic.Add("Connection", "fa fa-envelope");
            IconPermission_Dic.Add("ContactUsMessage", "fa fa-envelope");
            IconPermission_Dic.Add("Products", "fa fa-file");
            IconPermission_Dic.Add("Garanty", "fa fa-cog");
            IconPermission_Dic.Add("Features", "fa fa-list");
            IconPermission_Dic.Add("Contents", "fa fa-newspaper");
            IconPermission_Dic.Add("Catalog", "fa fa-list");
            IconPermission_Dic.Add("AboutUs", "fa fa-info-circle");
            IconPermission_Dic.Add("Unit", "fa fa-clone");
            IconPermission_Dic.Add("Off", "fa fa-money-bill-alt");
            IconPermission_Dic.Add("Producer", "fa fa-industry");
            IconPermission_Dic.Add("Sizes", "fa fa-industry");

            IconPermission_Dic.Add("OfficePost", "fa fa-fw fa-share-alt");
            IconPermission_Dic.Add("OfficeUnit", "fa fa-th");
            IconPermission_Dic.Add("Employee", "fa fa-user");
            IconPermission_Dic.Add("Vacation", "fa fa-user");
            IconPermission_Dic.Add("altVacationRequest", "fa fa-user");
            IconPermission_Dic.Add("VRsuperior", "fa fa-user");
            IconPermission_Dic.Add("VRHR", "fa fa-user");
            IconPermission_Dic.Add("VacationHRManager", "fa fa-user");

            IconPermission_Dic.Add("Vacationnew", "fa fa-user");
            IconPermission_Dic.Add("VacationCoworkernew", "fa fa-user");
            IconPermission_Dic.Add("VacationSuperiornew", "fa fa-user");
            IconPermission_Dic.Add("VacationSuperior", "fa fa-user");
            IconPermission_Dic.Add("VacationsAllEmployee", "fa fa-user");
            IconPermission_Dic.Add("VacationManagernew", "fa fa-user");
            IconPermission_Dic.Add("VacationHRManagernew", "fa fa-user");

            IconPermission_Dic.Add("ShiftChange", "fa fa-user");
            IconPermission_Dic.Add("ShiftChangeSuperior", "fa fa-user");
            IconPermission_Dic.Add("ShiftCoworker", "fa fa-user");
            IconPermission_Dic.Add("ShiftSuprior", "fa fa-user");
            IconPermission_Dic.Add("ShiftManager", "fa fa-user");
            IconPermission_Dic.Add("ShiftHRManager", "fa fa-user");
            IconPermission_Dic.Add("ShiftChangesAllEmployees", "fa fa-user");
            IconPermission_Dic.Add("ExtraWork", "fa fa-user");
            IconPermission_Dic.Add("ExtraWorkSuperior", "fa fa-user");
            IconPermission_Dic.Add("ExtraWorksAllEmployees", "fa fa-user");
            IconPermission_Dic.Add("ExtraWorkManager", "fa fa-user");
            IconPermission_Dic.Add("ExtraWorkHRManager", "fa fa-user");
            IconPermission_Dic.Add("Loan", "fa fa-user");
            IconPermission_Dic.Add("LoanSuperior", "fa fa-user");
            IconPermission_Dic.Add("LoanManager", "fa fa-user");
            IconPermission_Dic.Add("LoanHRManager", "fa fa-user");
            IconPermission_Dic.Add("Mission", "fa fa-user");
            IconPermission_Dic.Add("MissionSP", "fa fa-user");
            IconPermission_Dic.Add("MissionSuperior", "fa fa-user");
            IconPermission_Dic.Add("MissionSuprior", "fa fa-user");
            IconPermission_Dic.Add("MissionManager", "fa fa-user");
            IconPermission_Dic.Add("MissionHRManager", "fa fa-user");
            IconPermission_Dic.Add("MissionsAllEmployees", "fa fa-user");
            IconPermission_Dic.Add("HalycoManagers", "fa fa-user");
            IconPermission_Dic.Add("ActivityField", "fa fa-user");
            IconPermission_Dic.Add("Activity", "fa fa-user");
            IconPermission_Dic.Add("VacationRequest", "fas fa-assistive-listening-systems");
            IconPermission_Dic.Add("Suppliers", "fa fa-user");
            IconPermission_Dic.Add("Complaint", "fas fa-balance-scale");
            IconPermission_Dic.Add("Proposal", "fas fa-box");
            IconPermission_Dic.Add("adminProposals", "fas fa-box");


            IconPermission_Dic.Add("MainSurvey", "fas fa-question");
            IconPermission_Dic.Add("SurveyQuestionGroup", "fas fa-question");
            IconPermission_Dic.Add("EmployeeSurvey", "fas fa-question");
            IconPermission_Dic.Add("SurveyQuestionReply", "fas fa-question");
            IconPermission_Dic.Add("SurveyAnswerType", "fas fa-question");
            IconPermission_Dic.Add("SurveyQuestion", "fas fa-question");

            IconPermission_Dic.Add("EmployeeSalary", "fa fa-file");
            IconPermission_Dic.Add("EmployeeSalaryUpload", "fa fa-fw fa-file-excel");

            IconPermission_Dic.Add("Employements", "fa fa-stethoscope");
            IconPermission_Dic.Add("ActivityOrgItems", "fa fa-list-alt");
            IconPermission_Dic.Add("ComputerSkills", "fa fa-fw fa-language");


        }
        public  Dictionary<string, string> IconPermission_Dic = new Dictionary<string, string>();
        public static string FindIcon(string nameController)
        {
            var dic = new IconDictionary();
            var icon = dic.IconPermission_Dic.FirstOrDefault(x => x.Key == nameController);
            if (icon.Key == null) return "fa fa-question";
            else return icon.Value;
        }
    }
}
