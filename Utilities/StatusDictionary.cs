﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Utilities
{
    public class StatusDictionary
    {
        public StatusDictionary()
        {

            PayMent_Type.Add(1, "پرداخت شده");
            PayMent_Type.Add(2, "پرداخت نشده");

            Order_Status.Add(1, "انصراف");
            Order_Status.Add(2, "ثبت");
            Order_Status.Add(3, "تائید شده");
            Order_Status.Add(4, "معلق");
            //Role_Status_Dic
            Role_Status_Dic.Add(1, "فعال");
            Role_Status_Dic.Add(2, "غیر فعال");

            SocialNetwork_Type.Add(1, "تلگرام");
            SocialNetwork_Type.Add(2, "لینکدین");
            SocialNetwork_Type.Add(3, " فیس بوک");
            SocialNetwork_Type.Add(4, "توئیتر");
            SocialNetwork_Type.Add(5, "گوگل پلاس");
            SocialNetwork_Type.Add(6, "یوتیوب");
            SocialNetwork_Type.Add(7, "واتس اپ");

            SocialNetwork_Icon.Add(1, "fa fa-paper-plane");
            SocialNetwork_Icon.Add(2, "fab fa-linkedin");
            SocialNetwork_Icon.Add(3, "fab fa-facebook-square");
            SocialNetwork_Icon.Add(4, "fab fa-twitter");
            SocialNetwork_Icon.Add(5, "fab fa-google-plus-g");
            SocialNetwork_Icon.Add(6, "fab fa-youtube");
            SocialNetwork_Icon.Add(7, "fab fa-whatsapp");

            RoleDisplay_Dic.Add("Customer", "مشتری");
            RoleDisplay_Dic.Add("Admin", "مدیر");
            RoleDisplay_Dic.Add("SubAdmin", "زیر مدیر");
            RoleDisplay_Dic.Add("Employee", "کارمند");
            //RoleDisplay_Dic.Add("Supervisor", "سرپرست");
            //RoleDisplay_Dic.Add("Manager", "مدیرعامل");
            //RoleDisplay_Dic.Add("OfficeAdmin", "رئیس اداری");
            //RoleDisplay_Dic.Add("FactoryAdmin", "رئیس کارخانه");

            //Role_Type
            Role_Type_Dic.Add(1, "هویتی");
            Role_Type_Dic.Add(2, "عملکردی");

            RoleType_Identity.Add("SubAdmin", "زیر مدیر");
            RoleType_Identity.Add("Admin", "مدیر");
            RoleType_Identity.Add("Supervisor", "سرپرست");
            RoleType_Identity.Add("Manager", "مدیرعامل");
            RoleType_Identity.Add("OfficeAdmin", "رئیس اداری");
            RoleType_Identity.Add("FactoryAdmin", "رئیس کارخانه");

            MissionType_Dic.Add(1, "داخل شهری");
            MissionType_Dic.Add(2, "داخل کشور");
            MissionType_Dic.Add(3, "خارج کشور");

            AllDic.TryAdd("PayMent", PayMent_Type);
            AllDic.TryAdd("Order", Order_Status);
            AllDic.TryAdd("SocialNetwork", SocialNetwork_Type);
            AllDic.TryAdd("SocialNetwork_Icon", SocialNetwork_Icon);
            AllDic.TryAdd("RoleType", Role_Type_Dic);
            AllDic.TryAdd("Role", Role_Status_Dic);
            AllDic.TryAdd("MissionType", MissionType_Dic);

           


        }

        public Dictionary<int, string> PayMent_Type = new Dictionary<int, string>();
        public Dictionary<int, string> Order_Status = new Dictionary<int, string>();
        public Dictionary<int, string> SocialNetwork_Type = new Dictionary<int, string>();
        public Dictionary<int, string> SocialNetwork_Icon = new Dictionary<int, string>();
        public Dictionary<string, string> RoleDisplay_Dic = new Dictionary<string, string>();
        public Dictionary<int, string> Role_Type_Dic = new Dictionary<int, string>();
        public Dictionary<int, string> Role_Status_Dic = new Dictionary<int, string>();
        public Dictionary<string, string> RoleType_Identity = new Dictionary<string, string>();
        public Dictionary<int, string> MissionType_Dic = new Dictionary<int, string>();

        public static Dictionary<string, Dictionary<int, string>> AllDic = new Dictionary<string, Dictionary<int, string>>();

        public static string FindStatus(string Key, int Status)
        {
            StatusDictionary s = new StatusDictionary();
            var dictionary = AllDic.Single(m => m.Key == Key).Value;
            string StatusName = dictionary[Status];
            return StatusName;
        }
        public static bool ExistsRole_Identity(string role)
        {
            StatusDictionary statusDictionary = new StatusDictionary();
            var dictionary = statusDictionary.RoleDisplay_Dic.FirstOrDefault(m => m.Key == role).Value;
            if (dictionary != null)
                return true;
            return false;

        }
        public static string FindRoleDisplay(string role)
        {
            StatusDictionary statusDictionary = new StatusDictionary();
            var dictionary = statusDictionary.RoleDisplay_Dic.FirstOrDefault(m => m.Key == role).Value;
            if (dictionary != null)
                return dictionary;
            return
               role;

        }
        public static string FindRole_Identity(string role)
        {
            StatusDictionary statusDictionary = new StatusDictionary();
            var dictionary = statusDictionary.RoleType_Identity.FirstOrDefault(m => m.Key == role).Value;
            if (dictionary != null)
                return dictionary;
            return role;

        }
    }
}
