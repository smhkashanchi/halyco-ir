﻿using MD.PersianDateTime;
using MD.PersianDateTime.Core;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Utilities.DatePersion
{
    public class ConvertDate
    {
        public static string GetDatePersion()
        {

            PersianDateTime persianDateTime = new PersianDateTime(DateTime.Now);
            return persianDateTime.ToShortDateString();
        }
    }
   
}