﻿using GoogleRecaptchaInCore.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Utilities
{
    public class Captcha
    {
        public Captcha()
        {

        }
        public static reCaptchaResponse Verify(IFormCollection form)
        {
            string urlToPost = "https://www.google.com/recaptcha/api/siteverify";

            //Secret key خود را در این قسمت وارد کنید

            //string secretKey = "6Lecvn0UAAAAAPUjy6CvDs5941A54x7KRzENZlCT"; // local
            string secretKey = "6LfuJaoUAAAAAJO97f4WCmmOf5BQzoWapPuLzMOC"; // halyco.itisa.ir
            string gRecaptchaResponse = form["g-recaptcha-response"];

            var postData = "secret=" + secretKey + "&response=" + gRecaptchaResponse;

            // send post data
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlToPost);
            request.Method = "POST";
            request.ContentLength = postData.Length;
            request.ContentType = "application/x-www-form-urlencoded";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(postData);
            }

            // receive the response now
            string res = string.Empty;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    res = reader.ReadToEnd();
                }
            }

            // validate the response from Google reCaptcha
            var captChaesponse = JsonConvert.DeserializeObject<reCaptchaResponse>(res);
            return (captChaesponse);
        }
    }
}
