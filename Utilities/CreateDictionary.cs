﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Utilities
{
    public class CreateDictionary
    {
        public Dictionary<string, string> myList;
       
        public IEnumerable<string> MakeDictionary(string key,string text)
        {
            myList.Add(key, text);
            return myList as IEnumerable<string>;
        }
    }
}
