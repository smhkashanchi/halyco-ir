﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using MD.PersianDateTime.Core;
using System.Text.RegularExpressions;

namespace Utilities
{
    public static class DateConvertor
    {
        public static string ToShamsi(this DateTime value)
        {
            PersianCalendar pc=new PersianCalendar();

            return pc.GetYear(value) + "/" + pc.GetMonth(value).ToString("00") + "/" +
                   pc.GetDayOfMonth(value).ToString("00");
        }
        public static string ToShamsiAttendance(this DateTime value)
        {
            PersianCalendar pc=new PersianCalendar();

            return pc.GetYear(value) + "/" + pc.GetMonth(value).ToString() + "/" +
                   pc.GetDayOfMonth(value).ToString();
        }
        public static string ToShamsiWithTime(this DateTime value)
        {
            PersianCalendar pc = new PersianCalendar();

            return pc.GetYear(value) + "/" + pc.GetMonth(value).ToString("00") + "/" +
                  pc.GetDayOfMonth(value).ToString("00") + " " + pc.GetHour(value) + ":" + pc.GetMinute(value);
        }
        public static string ToShamsiWithTime(this DateTime? value)
        {
            PersianCalendar pc = new PersianCalendar();

            return pc.GetYear(value.Value) + "/" + pc.GetMonth(value.Value).ToString("00") + "/" +
                  pc.GetDayOfMonth(value.Value).ToString("00") + " " + pc.GetHour(value.Value) + ":" + pc.GetMinute(value.Value) ;
        }
        public static string ToPersian(this string str)
        {
            PersianCalendar pc = new PersianCalendar();
            DateTime value = DateTime.ParseExact(str, "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

            return pc.GetYear(value) + "/" + pc.GetMonth(value).ToString("00") + "/" +
                    pc.GetDayOfMonth(value).ToString("00") + " " + pc.GetHour(value) + ":" + pc.GetMinute(value) + ":" + pc.GetSecond(value);


        }
        public static string ToShortPersian2(this string str) //بدون زمان
        {
            PersianCalendar pc = new PersianCalendar();
            //str += " 00:00:00";
            DateTime value = DateTime.ParseExact(str, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);

            return pc.GetYear(value) + "/" + pc.GetMonth(value).ToString("00") + "/" +
                    pc.GetDayOfMonth(value).ToString("00");
        }
        public static string ToShortPersian(this string str) //همراه با زمان
        {
            PersianCalendar pc = new PersianCalendar();
            DateTime value = new DateTime();
            if (Regex.IsMatch(str, @"[0-9][0-9]//[0-9][0-9]//[0-9][0-9][0-9][0-9]"))
            {
                value = DateTime.ParseExact(str, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);

            }
            else
            {
                value = DateTime.ParseExact(str, "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            string testStr = pc.GetYear(value) + "/" + pc.GetMonth(value).ToString("00") + "/" + pc.GetDayOfMonth(value).ToString("00");
            return pc.GetYear(value) + "/" + pc.GetMonth(value).ToString("00") + "/" + pc.GetDayOfMonth(value).ToString("00");
        }
        public static string ToShamsiTime(this string str) // زمان
        {
            PersianCalendar pc = new PersianCalendar();
            DateTime value = new DateTime();
            if (Regex.IsMatch(str, @"[0-9][0-9]//[0-9][0-9]//[0-9][0-9][0-9][0-9]"))
            {
                value = DateTime.ParseExact(str, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);

            }
            else
            {
                value = DateTime.ParseExact(str, "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            return pc.GetHour(value).ToString("00") + ":" + pc.GetMinute(value).ToString("00") + ":" +
                               pc.GetSecond(value).ToString("00");
        }
        public static string ToLongShamsi(this DateTime value)
        {
            PersianCalendar pc = new PersianCalendar();
            DayOfWeek dw = pc.GetDayOfWeek(value);
            string persianDayOfWeek = "";
            switch (dw)
            {
                case DayOfWeek.Friday:
                    {
                        persianDayOfWeek = "جمعه";
                        break;
                    }
                case DayOfWeek.Monday:
                    {
                        persianDayOfWeek = "دوشنبه";
                        break;
                    }
                case DayOfWeek.Saturday:
                    {
                        persianDayOfWeek = "شنبه";
                        break;
                    }
                case DayOfWeek.Sunday:
                    {
                        persianDayOfWeek = "یک شنبه";
                        break;
                    }
                case DayOfWeek.Thursday:
                    {
                        persianDayOfWeek = "پنج شنبه";
                        break;
                    }
                case DayOfWeek.Tuesday:
                    {
                        persianDayOfWeek = "سه شنبه";
                        break;
                    }
                case DayOfWeek.Wednesday:
                    {
                        persianDayOfWeek = "چهارشنبه";
                        break;
                    }
            }

            int month = pc.GetMonth(value);
            string monthName = "";
            switch (month)
            {
                case 1:
                    {
                        monthName = "فروردین";
                        break;
                    }
                case 2:
                    {
                        monthName = "اردیبهشت";
                        break;
                    }
                case 3:
                    {
                        monthName = "خرداد";
                        break;
                    }
                case 4:
                    {
                        monthName = "تیر";
                        break;
                    }
                case 5:
                    {
                        monthName = "مرداد";
                        break;
                    }
                case 6:
                    {
                        monthName = "شهریور";
                        break;
                    }
                case 7:
                    {
                        monthName = "مهر";
                        break;
                    }
                case 8:
                    {
                        monthName = "آبان";
                        break;
                    }
                case 9:
                    {
                        monthName = "آذر";
                        break;
                    }
                case 10:
                    {
                        monthName = "دی";
                        break;
                    }
                case 11:
                    {
                        monthName = "بهمن";
                        break;
                    }
                case 12:
                    {
                        monthName = "اسفند";
                        break;
                    }
            }
            var t= persianDayOfWeek + " " + pc.GetDayOfMonth(value).ToString() + " " + monthName + " " + pc.GetYear(value);
            return t.PersianToEnglish();/*+"--"+dw.ToString();*/
        }
        public static string ToLongShamsi(this string str)
        {
            PersianCalendar pc = new PersianCalendar();
            DateTime value = new DateTime();
            if (Regex.IsMatch(str, @"[0-9][0-9]//[0-9][0-9]//[0-9][0-9][0-9][0-9]"))
            {
                value = DateTime.ParseExact(str, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);

            }
            else
            {
                value = DateTime.ParseExact(str, "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            DayOfWeek dw = pc.GetDayOfWeek(value);
            string persianDayOfWeek = "";
            switch (dw)
            {
                case DayOfWeek.Friday:
                    {
                        persianDayOfWeek = "جمعه";
                        break;
                    }
                case DayOfWeek.Monday:
                    {
                        persianDayOfWeek = "دوشنبه";
                        break;
                    }
                case DayOfWeek.Saturday:
                    {
                        persianDayOfWeek = "شنبه";
                        break;
                    }
                case DayOfWeek.Sunday:
                    {
                        persianDayOfWeek = "یک شنبه";
                        break;
                    }
                case DayOfWeek.Thursday:
                    {
                        persianDayOfWeek = "پنج شنبه";
                        break;
                    }
                case DayOfWeek.Tuesday:
                    {
                        persianDayOfWeek = "سه شنبه";
                        break;
                    }
                case DayOfWeek.Wednesday:
                    {
                        persianDayOfWeek = "چهارشنبه";
                        break;
                    }
            }

            int month = pc.GetMonth(value);
            string monthName = "";
            switch (month)
            {
                case 1:
                    {
                        monthName = "فروردین";
                        break;
                    }
                case 2:
                    {
                        monthName = "اردیبهشت";
                        break;
                    }
                case 3:
                    {
                        monthName = "خرداد";
                        break;
                    }
                case 4:
                    {
                        monthName = "تیر";
                        break;
                    }
                case 5:
                    {
                        monthName = "مرداد";
                        break;
                    }
                case 6:
                    {
                        monthName = "شهریور";
                        break;
                    }
                case 7:
                    {
                        monthName = "مهر";
                        break;
                    }
                case 8:
                    {
                        monthName = "آبان";
                        break;
                    }
                case 9:
                    {
                        monthName = "آذر";
                        break;
                    }
                case 10:
                    {
                        monthName = "دی";
                        break;
                    }
                case 11:
                    {
                        monthName = "بهمن";
                        break;
                    }
                case 12:
                    {
                        monthName = "اسفند";
                        break;
                    }
            }
            var t= persianDayOfWeek + " " + pc.GetDayOfMonth(value).ToString() + " " + monthName + " " + pc.GetYear(value);
            return t.PersianToEnglish();/*+"--"+dw.ToString();*/
        }
        public static string ToShamsi(this DateTime? value)
        {
            PersianCalendar pc = new PersianCalendar();

            return pc.GetYear(value.Value) + "/" + pc.GetMonth(value.Value).ToString("00") + "/" +
                   pc.GetDayOfMonth(value.Value).ToString("00");
        }
        public static string ToShamsi2(this DateTime value)
        {
            PersianCalendar pc = new PersianCalendar();

            return pc.GetYear(value) + "/" + pc.GetMonth(value).ToString("00") + "/" +
                   pc.GetDayOfMonth(value).ToString("00");
        }
        public static string ToShamsiFullDateTime(this DateTime value)
        {
            PersianCalendar pc = new PersianCalendar();

            return pc.GetYear(value) + "/" + pc.GetMonth(value).ToString("00") + "/" +
                   pc.GetDayOfMonth(value).ToString("00") + " " + pc.GetHour(value).ToString() + ":" + pc.GetMinute(value).ToString() + ":" +
                   pc.GetSecond(value).ToString();
        }
        public static string ToShamsiTime(this DateTime value)
        {
            PersianCalendar pc = new PersianCalendar();

            return pc.GetHour(value).ToString("00") + ":" + pc.GetMinute(value).ToString("00");
        }
        public static string GetDatePersion(this DateTime datetime)
        {
            PersianDateTime persianDateTime = new PersianDateTime(datetime);
            return persianDateTime.ToShortDateString();
        }
        public static string DigitToPersianMonth(this string value, int lengthth)
        {
            string result = "";
            string mnth = "";
            string year = value.Substring(0, 2);
            if (lengthth >= value.Length)
                return value;
            mnth = value.Substring(value.Length - lengthth);
            switch (mnth)
            {
                case "01":
                    result = "فروردین" + year;
                    break;
                case "02":
                    result = "اردیبهشت" + year;
                    break;
                case "03":
                    result = "خرداد" + year;
                    break;
                case "04":
                    result = "تیر" + year;
                    break;
                case "05":
                    result = "مرداد" + year;
                    break;
                case "06":
                    result = "شهریور" + year;
                    break;
                case "07":
                    result = "مهر" + year;
                    break;
                case "08":
                    result = "آبان" + year;
                    break;
                case "09":
                    result = "آذر" + year;
                    break;
                case "10":
                    result = "دی" + year;
                    break;
                case "11":
                    result = "بهمن" + year;
                    break;
                case "12":
                    result = "اسفند" + year;
                    break;
            }
            return result;

        }
        public static DateTime GetDate(this DateTime datetime)
        {
            PersianDateTime persianDateTime = new PersianDateTime(datetime);
            return persianDateTime.Date;
        }
        public static DateTime GetMiladiDate(this int persianYear, int persianMonth, int persianDay)
        {
            PersianDateTime pdt = new PersianDateTime(persianYear, persianMonth, persianDay);
            return pdt.ToDateTime();
        }

        private static string[] persian = { "۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹" };
        private static string[] english = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
        public static string EnglishToPersian(this string strNum)
        {
            string chash = strNum;
            for (int i = 0; i < 10; i++)
                chash = chash.Replace(english[i], persian[i]);
            return chash;
        }

        public static string PersianToEnglish(this string strNum)
        {
            string chash = strNum;
            for (int i = 0; i < 10; i++)
                chash = chash.Replace(persian[i], english[i]);
            return chash;
        }
    }
}
