﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Services
{
    public interface ISMSSender
    {
        Task CheckAmount();
        Task SendSMSAsync(string receptor, string message);
        Task SendFactorSMSAsync(string receptor, string message);
        Task SendAdminFactorSMSAsync(string receptor, string mobile, string code, string date);
       // Task SendToSMSSubscribe(SMS_SubscribeViewModel viewModel);
        Task SendDenyFactorSMSAsync(string receptor, string price);
        Task SendDenyAdminFactorSMSAsync(string receptor, string mobile, string code, string date);
        //Task CheckAmount();
        Task SendSubmitvacationMessageSMSAsync(string receptor,string karmandName,string duration, string vacationType);
        Task SendSubmitvacationMessageForCoworkerSMSAsync(string receptor, string karmandName, string alternateKarmandName, string vacationDate);
        Task SendRefuseVacationByCoworkerMessageSMSAsync(string receptor, string CoworkerkarmandName, string vacationType);
        Task SendfinalVacationAcceptSMSAsync(string receptor, string karmandName, string persiandate, string status); 
        
        Task finalVacationAcceptHRManagerSMSAsync(string receptor, string karmandName, string persiandate, bool status, string VacationType);

        //=========================================== Shift change sms template
        Task SendSubmitshiftChangeForCoworkerMessageSMSAsync(string receptor, string karmandName, string alternatekarmandName, string shiftDate);
        Task SendSubmitShiftChangeMessageSMSAsync(string receptor, string karmandName, string shiftDate);
        Task SendRefuseShiftChangeByCoworkerMessageSMSAsync(string receptor, string karmandName, string shiftDate);
        Task SendfinalShiftChangeAcceptSMSAsync(string receptor, string karmandName, string persiandate, string status);
        Task finalShiftChangeAcceptHRManagerSMSAsync(string receptor, string karmandName, string persiandate, bool status);
        //=============================================== Extra Work sms template
        Task SendSubmitExtraWorkMessageSMSAsync(string receptor, string karmandName, string duration);
        Task SendfinalExtraworkAcceptSMSAsync(string receptor, string karmandName, string persiandate, string status);
        Task finalExtraworkAcceptHRManagerSMSAsync(string receptor, string karmandName, string persiandate, bool status);
        //=============================================== Mission sms template
        Task SendSubmitMissionMessageSMSAsync(string receptor, string karmandName, string duration);
        Task SendfinalMissionAcceptSMSAsync(string receptor, string karmandName, string persiandate, string status);
        Task finalMissionAcceptHRManagerSMSAsync(string receptor, string karmandName, string persiandate, bool status);
    }
}

