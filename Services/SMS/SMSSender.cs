﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Utilities;

using ViewModels.SMS;

namespace Services
{
    // This class is used by the application to send email for account confirmation and password reset.
    // For more details see https://go.microsoft.com/fwlink/?LinkID=532713
    public class SMSSender : ISMSSender
    {
        private readonly IConfiguration _configuration;
        private readonly string _apiKey;
        public SMSSender(IConfiguration configuration)
        {
            _configuration = configuration;
            _apiKey = "726C344B456441446D7A6C465A544448536A2F384C7A4E45726B3067566262732F4B3071315061715A6A383D";
            CheckAmount();
        }

        #region Panel 2
        //var sender = "10004346";
        //var api = new Kavenegar.KavenegarApi("4f526c6c3856476345456d514e73454771586f547a622f526a796c796f517169");
        //var result = await (api.Send(sender, receptor, message)); 
        #endregion

        public async Task CheckAmount()
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/account/info.json");
                string resultContent = await result.Content.ReadAsStringAsync();
                var res = JsonConvert.DeserializeObject<ResultAmountSMS>(resultContent);
                if (res?.entries?.remaincredit <= 1000)
                {
                    var result_send = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={09133643450}&token={res.entries.remaincredit}template=EndSmsAmount");
                    string result_send1 = await result_send.Content.ReadAsStringAsync();
                }
            };

        }
        public async Task SendSMSAsync(string receptor, string message)
        {
            #region Panel 1

            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={message}&template=digipesteh");
                string resultContent = await result.Content.ReadAsStringAsync();
                Console.WriteLine(resultContent);
            };

            #endregion
        }
        public async Task SendFactorSMSAsync(string receptor, string message)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={message}&template=digipestehFactor");
                string resultContent = await result.Content.ReadAsStringAsync();
                Console.WriteLine(resultContent);
            };

        }
        public async Task SendAdminFactorSMSAsync(string receptor, string mobile, string code, string date)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={mobile}&token2={date}&token3={code}&template=digipestehadminFactor");
                string resultContent = await result.Content.ReadAsStringAsync();
                Console.WriteLine(resultContent);
            };
        }
        public async Task SendDenyFactorSMSAsync(string receptor, string price)
        {
            using (var client = new HttpClient())
            {
                var date = DateTime.Now.ToShamsi();
                var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={date}&token2={price}&template=digipestehDenyFactor");
                string resultContent = await result.Content.ReadAsStringAsync();
                Console.WriteLine(resultContent);
            };
        }
        public async Task SendDenyAdminFactorSMSAsync(string receptor, string mobile, string price, string date)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={mobile}&token2={date}&token3={price}&template=digipestehAdminDenyFactor");
                string resultContent = await result.Content.ReadAsStringAsync();
                Console.WriteLine(resultContent);
            };
        }

        //public async Task SendToSMSSubscribe(SMS_SubscribeViewModel viewModel)
        //{


        //    try
        //    {

        //        Kavenegar.KavenegarApi api = new Kavenegar.KavenegarApi($"{_apiKey}");
        //        var res = await api.SendArray(viewModel.Sender, viewModel.Receptor, viewModel.Message);
        //        foreach (var r in res)
        //        {
        //            Console.Write("r.Messageid.ToString()");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        // در صورتی که خروجی وب سرویس 200 نباشد این خطارخ می دهد.
        //        Console.Write("Message : " + ex.Message);
        //    }

        //}

        //public async Task CheckAmount()
        //{
        //    using (var client = new HttpClient())
        //    {
        //        var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/account/info.json");
        //        string resultContent = await result.Content.ReadAsStringAsync();
        //        var res = JsonConvert.DeserializeObject<ResultAmountSMS>(resultContent);
        //        if( res?.entries?.remaincredit <= 1000)
        //        {
        //            var result_send = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={09133643450}&token={res.entries.remaincredit}template=EndSmsAmount");
        //            string result_send1 = await result_send.Content.ReadAsStringAsync();
        //        }
        //    };

        //}

        /// <summary>
        /// ارسال پیامک برای مافوق کارمندی که درخواست مرخصی کرده
        /// </summary>
        /// <param name="receptor">شماره موبایلی که پیامک برای آن ارسال می شود</param>
        /// <param name="karmandName">نام کارمند درخواست کننده مرخصی</param>
        /// <param name="duration">مدت زمان مرخصی</param>
        /// <param name="vacationType">نوع مرخصی(روزانه/ساعتی(</param>
        /// <returns></returns>
        public async Task SendSubmitvacationMessageSMSAsync(string receptor, string karmandName, string duration, string vacationType)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={karmandName.Replace(" ", "\b")}&token2={duration.Replace(" ", "\b")}&token3={vacationType.Replace(" ", "\b")}&template=SubmitvacationMessage");
                string resultContent = await result.Content.ReadAsStringAsync();
                Console.WriteLine(resultContent);
            };
        }

        /// <summary>
        /// ارسال پیامک برای همکار جایگزین کارمندی که درخواست مرخصی کرده
        /// </summary>
        /// <param name="receptor">شماره موبایلی که پیامک برای آن ارسال می شود</param>
        /// <param name="karmandName">نام کارمند درخواست کننده مرخصی</param>
        /// <param name="vacationDate">تاریخ مرخصی</param>
        /// <param name="vacationType">نوع مرخصی(روزانه/ساعتی(</param>
        /// <returns></returns>
        public async Task SendSubmitvacationMessageForCoworkerSMSAsync(string receptor, string karmandName, string alternateKarmandName, string vacationDate)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={karmandName.Replace(" ", "\b")}&token2={alternateKarmandName.Replace(" ", "\b")}&token3={vacationDate.Replace(" ", "\b")}&template=SubmitvacationMessageForCoworker");
                string resultContent = await result.Content.ReadAsStringAsync();
                Console.WriteLine(resultContent);
            };
        }


        /// <summary>
        /// ارسال پیامک برای رد درخواست توسط همکار جایگزین کارمندی که درخواست مرخصی کرده
        /// </summary>
        /// <param name="receptor">شماره موبایلی که پیامک برای آن ارسال می شود</param>
        /// <param name="CoworkerkarmandName">نام همکار کارمند درخواست کننده مرخصی</param>
        /// <param name="vacationType">نوع مرخصی(روزانه/ساعتی(</param>
        /// <returns></returns>
        public async Task SendRefuseVacationByCoworkerMessageSMSAsync(string receptor, string CoworkerkarmandName, string vacationType)
        {
            #region Panel 1
            //receptor = "09133643450";
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={CoworkerkarmandName.Replace(" ", "\b")}&token2={vacationType.Replace(" ", "\b")}&template=RefuseVacationByCoworker");
                string resultContent = await result.Content.ReadAsStringAsync();
                Console.WriteLine(resultContent);
            };

            #endregion
        }


        public async Task SendfinalVacationAcceptSMSAsync(string receptor, string karmandName, string persiandate, string status)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={karmandName.Replace(" ", "\b")}&token2={persiandate.Replace(" ", "\b")}&token3={status.Replace(" ", "\b")}&template=finalVacationAccept");
                string resultContent = await result.Content.ReadAsStringAsync();
                Console.WriteLine(resultContent);
            };
        }

        public async Task finalVacationAcceptHRManagerSMSAsync(string receptor, string karmandName, string persiandate, bool status, string VacationType)
        {
            using (var client = new HttpClient())
            {
                if (status)
                {
                    var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={karmandName.Replace(" ", "\b")}&token2={persiandate.Replace(" ", "\b")}&token3={VacationType.Replace(" ", "\b")}&template=finalVacationAcceptHRManager");
                    string resultContent = await result.Content.ReadAsStringAsync();
                }
                else
                {
                    var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={karmandName.Replace(" ", "\b")}&token2={persiandate.Replace(" ", "\b")}&token3={VacationType.Replace(" ", "\b")}&template=finalVacationRefuseHRManager");
                    string resultContent = await result.Content.ReadAsStringAsync();
                }
            }
        }

        //=========================================== Shift change sms template
        /// <summary>
        /// ارسال پیامک برای همکار جایگزین کارمندی که درخواست تعویض شیفت کرده
        /// </summary>
        /// <param name="receptor">شماره موبایلی که پیامک برای آن ارسال می شود</param>
        /// <param name="karmandName">نام کارمند درخواست کننده تعویض شیفت</param>
        /// <param name="vacationDate">نام کرامند جایگزین</param>
        /// <param name="vacationType">تاریخ تعویض شیفت(</param>
        /// <returns></returns>
        public async Task SendSubmitshiftChangeForCoworkerMessageSMSAsync(string receptor, string karmandName, string alternatekarmandName,string shiftDate)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={karmandName.Replace(" ", "\b")}&token2={alternatekarmandName.Replace(" ", "\b")}&token3={shiftDate.Replace(" ", "\b")}&template=SubmitshiftChangeForCoworker");
                string resultContent = await result.Content.ReadAsStringAsync();
                Console.WriteLine(resultContent);
            };
        }

        /// <summary>
        /// ارسال پیامک برای مافوق کارمندی که درخواست تعویض شیفت کرده
        /// </summary>
        /// <param name="receptor">شماره موبایلی که پیامک برای آن ارسال می شود</param>
        /// <param name="karmandName">نام کارمند درخواست کننده تعویض شیفت</param>
        /// <param name="shiftDate">تاریخ تعویض شیفت(</param>
        /// <returns></returns>
        public async Task SendSubmitShiftChangeMessageSMSAsync(string receptor, string karmandName, string shiftDate)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={karmandName.Replace(" ", "\b")}&token2={shiftDate.Replace(" ", "\b")}&template=SubmitShiftChange");
                string resultContent = await result.Content.ReadAsStringAsync();
                Console.WriteLine(resultContent);
            };
        }

        /// <summary>
        /// ارسال پیامک رد درخواست برای کارمندی که درخواست تعویض شیفت کرده
        /// </summary>
        /// <param name="receptor">شماره موبایلی که پیامک برای آن ارسال می شود</param>
        /// <param name="karmandName">نام کارمند درخواست کننده تعویض شیفت</param>
        /// <param name="shiftDate">تاریخ تعویض شیفت(</param>
        /// <returns></returns>
        public async Task SendRefuseShiftChangeByCoworkerMessageSMSAsync(string receptor, string karmandName, string shiftDate)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={karmandName.Replace(" ", "\b")}&token2={shiftDate.Replace(" ", "\b")}&template=RefuseShiftChangeByCoworker");
                string resultContent = await result.Content.ReadAsStringAsync();
                Console.WriteLine(resultContent);
            };
        }

        public async Task SendfinalShiftChangeAcceptSMSAsync(string receptor, string karmandName, string persiandate, string status)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={karmandName.Replace(" ", "\b")}&token2={persiandate.Replace(" ", "\b")}&token3={status.Replace(" ", "\b")}&template=finalShiftChangeAccept");
                string resultContent = await result.Content.ReadAsStringAsync();
                Console.WriteLine(resultContent);
            };
        }

        public async Task finalShiftChangeAcceptHRManagerSMSAsync(string receptor, string karmandName, string persiandate, bool status)
        {
            using (var client = new HttpClient())
            {
                if (status)
                {
                    var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={karmandName.Replace(" ", "\b")}&token2={persiandate.Replace(" ", "\b")}&template=finalShiftChangeAcceptHRManager");
                    string resultContent = await result.Content.ReadAsStringAsync();
                }
                else
                {
                    var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={karmandName.Replace(" ", "\b")}&token2={persiandate.Replace(" ", "\b")}&template=finalShiftChangeRefuseHRManager");
                    string resultContent = await result.Content.ReadAsStringAsync();
                }
            }
        }

        //=============================================== Extra Work
        /// <summary>
        /// ارسال پیامک برای مافوق کارمندی که درخواست اضافه کاری کرده
        /// </summary>
        /// <param name="receptor">شماره موبایلی که پیامک برای آن ارسال می شود</param>
        /// <param name="karmandName">نام کارمند درخواست کننده اضافه کاری</param>
        /// <param name="duration">مدت زمان اضافه کاری</param>
        /// <param name="vacationType">نوع مرخصی(روزانه/ساعتی(</param>
        /// <returns></returns>
        public async Task SendSubmitExtraWorkMessageSMSAsync(string receptor, string karmandName, string duration)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={karmandName.Replace(" ", "\b")}&token2={duration.Replace(" ", "\b")}&template=SubmitExtraworkMessage");
                string resultContent = await result.Content.ReadAsStringAsync();
                Console.WriteLine(resultContent);
            };
        }

        public async Task SendfinalExtraworkAcceptSMSAsync(string receptor, string karmandName, string persiandate, string status)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={karmandName.Replace(" ", "\b")}&token2={persiandate.Replace(" ", "\b")}&token3={status.Replace(" ", "\b")}&template=finalExtraworkAccept");
                string resultContent = await result.Content.ReadAsStringAsync();
                Console.WriteLine(resultContent);
            };
        }

        public async Task finalExtraworkAcceptHRManagerSMSAsync(string receptor, string karmandName, string persiandate, bool status)
        {
            using (var client = new HttpClient())
            {
                if (status)
                {
                    var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={karmandName.Replace(" ", "\b")}&token2={persiandate.Replace(" ", "\b")}&template=finalExtraworkAcceptHRManager");
                    string resultContent = await result.Content.ReadAsStringAsync();
                }
                else
                {
                    var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={karmandName.Replace(" ", "\b")}&token2={persiandate.Replace(" ", "\b")}&template=finalExtraworkRefuseHRManager");
                    string resultContent = await result.Content.ReadAsStringAsync();
                }
            }
        }

        //=============================================== Mission
        /// <summary>
        /// ارسال پیامک برای مافوق کارمندی که درخواست ماموریت کرده
        /// </summary>
        /// <param name="receptor">شماره موبایلی که پیامک برای آن ارسال می شود</param>
        /// <param name="karmandName">نام کارمند درخواست کننده ماموریت</param>
        /// <param name="duration">مدت زمان ماموریت</param>
        /// <param name="vacationType">نوع مرخصی(روزانه/ساعتی(</param>
        /// <returns></returns>
        public async Task SendSubmitMissionMessageSMSAsync(string receptor, string karmandName, string duration)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={karmandName.Replace(" ", "\b")}&token2={duration.Replace(" ", "\b")}&template=SubmitMissionMessage");
                string resultContent = await result.Content.ReadAsStringAsync();
                Console.WriteLine(resultContent);
            };
        }

        public async Task SendfinalMissionAcceptSMSAsync(string receptor, string karmandName, string persiandate, string status)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={karmandName.Replace(" ", "\b")}&token2={persiandate.Replace(" ", "\b")}&token3={status.Replace(" ", "\b")}&template=finalMissionAccept");
                string resultContent = await result.Content.ReadAsStringAsync();
                Console.WriteLine(resultContent);
            };
        }

        public async Task finalMissionAcceptHRManagerSMSAsync(string receptor, string karmandName, string persiandate, bool status)
        {
            using (var client = new HttpClient())
            {
                if (status)
                {
                    var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={karmandName.Replace(" ", "\b")}&token2={persiandate.Replace(" ", "\b")}&template=finalMissionAcceptHRManager");
                    string resultContent = await result.Content.ReadAsStringAsync();
                }
                else
                {
                    var result = await client.GetAsync($"https://api.kavenegar.com/v1/{_apiKey}/verify/lookup.json?receptor={receptor}&token={karmandName.Replace(" ", "\b")}&token2={persiandate.Replace(" ", "\b")}&template=finalMissionRefuseHRManager");
                    string resultContent = await result.Content.ReadAsStringAsync();
                }
            }
        }

    }
}

