﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Services
{
    // This class is used by the application to send email for account confirmation and password reset.
    // For more details see https://go.microsoft.com/fwlink/?LinkID=532713
    public class EmailSender : IEmailSender
    {
        private readonly IConfiguration _configuration;
        public EmailSender(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task SendEmailAsync( string email, string subject, string message)
        {
            using (var client = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = _configuration["EmailSender:Username"],
                    Password = _configuration["EmailSender:Password"]
                };

                client.Credentials = credential;
                client.Host = _configuration["EmailSender:Host"];
                client.Port = int.Parse(_configuration["EmailSender:Port"]);
                client.EnableSsl = false;

                using (var emailMessage = new MailMessage())
                {
                    emailMessage.To.Add(new MailAddress(email));
                    emailMessage.From = new MailAddress(_configuration["EmailSender:Email"]);
                    emailMessage.Subject = subject;
                    emailMessage.Body = message;
                    emailMessage.IsBodyHtml = true;
                    client.Send(emailMessage);
                }
            }
            await Task.CompletedTask;
        }
        public async Task SendEmailWithFileAsync( string email, string subject, string message, string filePath)
        {
            using (var client = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = _configuration["EmailSender:Username"],
                    Password = _configuration["EmailSender:Password"]
                };

                client.Credentials = credential;
                client.Host = _configuration["EmailSender:Host"];
                client.Port = int.Parse(_configuration["EmailSender:Port"]);
                client.EnableSsl = false;

                using (var emailMessage = new MailMessage())
                {


                    emailMessage.To.Add(new MailAddress(email));
                    emailMessage.From = new MailAddress(_configuration["EmailSender:Email"]);
                    emailMessage.Subject = subject;
                    emailMessage.IsBodyHtml = true;
                    emailMessage.Body = message;


                    if (filePath != null)
                    {
                        Attachment attachment;
                        attachment = new Attachment(filePath);
                        emailMessage.Attachments.Add(attachment);
                    }

                    client.Send(emailMessage);
                }
            }
            await Task.CompletedTask;
        }
    }
}
