﻿using System.Collections.Generic;
using DomainClasses.FilterAuthorization;
namespace Services.FilterAuthorization
{
    public interface IMvcControllerDiscovery
    {
        IEnumerable<MvcControllerInfo> GetControllers();
    }
}