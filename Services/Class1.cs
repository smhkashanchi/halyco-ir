﻿//using System;

//namespace Services
//{
//    public class Class1
//    {
//    <div class="wrapper">
//        <header class="main-header">
//            <!-- Logo -->
//            <a href="#" class="logo">
//                <!-- mini logo for sidebar mini 50x50 pixels -->
//                <span class="logo-mini">پنل</span>
//                <!-- logo for regular state and mobile devices -->
//                <span class="logo-lg"><b>مدیریت پروژه آیتیسا</b></span>
//            </a>
//            <!-- Header Navbar: style can be found in header.less -->
//            <nav class="navbar navbar-static-top">
//                <!-- Sidebar toggle button-->
//                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
//                    <i class="fas fa-bars"></i>
//                </a>

//                <div class="navbar-custom-menu">
//                    <ul class="nav navbar-nav">

//                        @*@await Component.InvokeAsync("TicketMessage")*@

//                        @*@await Component.InvokeAsync("ShowAllLanguage", false)*@


//                        @await Html.PartialAsync("_LoginPartial")


//                        <!-- Control Sidebar Toggle Button -->
//                        <li>
//                            <a href="#" data-toggle="control-sidebar"><i class="fa fa-cogs"></i></a>
//                        </li>
//                    </ul>
//                </div>
//            </nav>
//        </header>
//        <!-- right side column. contains the logo and sidebar -->
//        <aside class="main-sidebar">
//            <!-- sidebar: style can be found in sidebar.less -->
//            <div class="sidebar">
//                <!-- Sidebar user panel -->
//                <!-- sidebar menu: : style can be found in sidebar.less -->
//                @{

//                    //موقت
//                    <div class="user-panel">
//                        <div class="pull-right image">
//                            <img src="~/images/user2-160x160.jpg" id="userphoto" class="img-circle" alt="User Image">
//                        </div>
//                        <div class="pull-right info">
//                            <p id="UserFullName">مهدی</p>
//                            <a href="#"><i class="fa fa-circle text-success"></i> آنلاین</a>
//                        </div>
//                    </div>

//                    <ul class="sidebar-menu" data-widget="tree">
//                        <li class="header">منو</li>

//                        <li class="hidden"><a href="/Admin/Language"><i class="fa fa-th"></i> <span> زبان ها</span></a></li>

//                        <li class="treeview">
//                            <a href="#">
//                                <i class="fa fa-paperclip pull-right"></i>
//                                <span> ارتباط با سایت</span>
//                                <span class="pull-left-container">
//                                    <i class="fa fa-angle-right pull-left"></i>
//                                </span>

//                            </a>
//                            <ul class="treeview-menu">
//                                <li><a href="/Admin/Connection"><i class="fa fa-circle-o"></i>راه های ارتباطی </a></li>
//                                <li><a href="/Admin/ContactUsMessage"><i class="fa fa-circle-o"></i>پیام های ارتباطی</a></li>
//                            </ul>
//                        </li>
//                        <li class="treeview">
//                            <a href="#">
//                                <i class="fa fa-file-archive-o pull-right"></i>
//                                <span>مدیریت محتوا </span>
//                                <span class="pull-left-container">
//                                    <i class="fa fa-angle-right pull-left"></i>
//                                </span>

//                            </a>
//                            <ul class="treeview-menu">
//                                <li><a href="/Admin/AboutUs"><i class="fa fa-circle-o"></i>درباره ما</a></li>
//                                <li><a href="/Admin/Contents"><i class="fa fa-newspaper-o"></i> <span> اخبار و مطالب آموزشی </span></a></li>
//                                <li><a href="/Admin/Catalog"><i class="fa fa-newspaper-o"></i> <span> کاتالوگ </span></a></li>
//                                <li class="hidden"><a href="/Admin/Material"><i class="fa fa-circle-o"></i>ساخت تجهیزات </a></li>
//                                <li class="hidden"><a href="/Admin/ImportaionPart"><i class="fa fa-circle-o"></i>واردات و تامین تجهیزات</a></li>
//                                <li class="hidden"><a href="/Admin/EnginerDesign"><i class="fa fa-circle-o"></i>طراحی و مهندسی  </a></li>
//                            </ul>
//                        </li>

//                        <li><a href="/Admin/SlideShow"><i class="fa fa-slideshare"></i> <span> اسلایدشو </span></a></li>
//                        <li class="hidden"><a href="/Admin/Gallery"><i class="fa fa-image"></i> <span> گالری </span></a></li>
//                        <li class="hidden"><a href="/Admin/Products"><i class="fa fa-product-hunt"></i> <span> محصولات </span></a></li>
//                        <li><a href="/Admin/Projects"><i class="fa fa-th"></i> <span> محصولات </span></a></li>
//                        <li class="hidden"><a href="/Admin/FAQ"><i class="fa fa-th"></i> <span>سوالات متداول </span></a></li>
//                        <li class="treeview hidden">
//                            <a href="#">
//                                <i class="fa fa-shopping-cart pull-right"></i>
//                                <span>مدیریت محصولات </span>
//                                <span class="pull-left-container">
//                                    <i class="fa fa-angle-right pull-left"></i>
//                                </span>

//                            </a>
//                            <ul class="treeview-menu">
//                                <li><a href="/Admin/Products"><i class="fa fa-circle-o"></i>محصولات </a></li>
//                                <li><a href="/Admin/Color"><i class="fa fa-circle-o"></i>رنگ ها</a></li>
//                                <li><a href="/Admin/Features"><i class="fa fa-circle-o"></i>ویژگی ها </a></li>
//                                <li><a href="/Admin/Producer"><i class="fa fa-circle-o"></i>تولید کننده ها</a></li>
//                                <li><a href="/Admin/Off"><i class="fa fa-circle-o"></i>تخفیفات</a></li>
//                                <li><a href="/Admin/Garanty"><i class="fa fa-circle-o"></i>گارانتی</a></li>
//                                <li><a href="/Admin/Unit"><i class="fa fa-circle-o"></i>واحد</a></li>
//                            </ul>
//                        </li>

//                        <li><a href="/Admin/FilesManage"><i class="fa fa-video-camera"></i> <span> دانلود ها و ویدئو ها</span></a></li>
//                        <li><a href="/Admin/NewsLetters"><i class="fa fa-envelope"></i> <span> اشتراک ایمیل</span></a></li>
//                        <li><a href="/Admin/SMS_Subscribe"><i class="fa fa-envelope-open"></i> <span> اشتراک پیامک</span></a></li>
//                        <li class="hidden"><a href="/Admin/CoWorker"><i class="fa fa-support"></i> <span> مشتریان و همکاران</span></a></li>
//                        <li class=""><a href="/Admin/SocialNetwork"><i class="glyphicon glyphicon-send"></i> <span> شبکه های اجتماعی</span></a></li>
//                        <li class="hidden"><a href="/Admin/Location"><i class="fa fa-th"></i> <span>  شهر و استان</span></a></li>

//                        <li class=""><a href="/"><i class="glyphicon glyphicon-eye-open"></i> <span> سایت</span></a></li>
//                    </ul>
//                    //موقت

//                    @*@await Component.InvokeAsync("Permission")*@


//                }
//            </div>
//            <!-- /.sidebar -->
//        </aside>

//        <!-- Content Wrapper. Contains page content -->
//        <div class="content-wrapper">
//            <!-- Content Header (Page header) -->
//            <div class="content-header">
//                <div class="row">
//                    <div class="col-md-12">
//                        <div class="box box-primary">
//                            <breadcrumb asp-homepage-title="خانه"
//                                        asp-homepage-url="@Url.Action("Index", "Home", values: new { area = "" })"
//                                        asp-homepage-glyphicon="glyphicon glyphicon-home"></breadcrumb>
//                        </div>
//                    </div>

//                </div>
//            </div>



//            <!-- Main content -->


//            <div class="content table-responsive" id="Main_Content">
//                @RenderBody()

//                @await Html.PartialAsync("_Modal")
//            </div>

//            <!-- /.content -->
//        </div>
//        <!-- /.content-wrapper -->
//        <footer class="main-footer text-left">
//            <strong> <a href="https://itsaco.ir">شرکت فناوری اطلاعات آیتیسا</a> </strong>
//        </footer>

//        <!-- Control Sidebar -->
//        <aside class="control-sidebar control-sidebar-dark">
//            <!-- Create the tabs -->
//            <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
//                <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
//                <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
//            </ul>
//            <!-- Tab panes -->
//            <div class="tab-content">
//                <!-- Home tab content -->
//                <div class="tab-pane" id="control-sidebar-home-tab">
//                    <h3 class="control-sidebar-heading">فعالیت ها</h3>
//                    <ul class="control-sidebar-menu">
//                        <li>
//                            <a href="javascript:void(0)">
//                                <i class="menu-icon fa fa-birthday-cake bg-red"></i>

//                                <div class="menu-info">
//                                    <h4 class="control-sidebar-subheading">تولد غلوم</h4>

//                                    <p>۲۴ مرداد</p>
//                                </div>
//                            </a>
//                        </li>
//                        <li>
//                            <a href="javascript:void(0)">
//                                <i class="menu-icon fa fa-user bg-yellow"></i>

//                                <div class="menu-info">
//                                    <h4 class="control-sidebar-subheading">آپدیت پروفایل سجاد</h4>

//                                    <p>تلفن جدید (800)555-1234</p>
//                                </div>
//                            </a>
//                        </li>
//                        <li>
//                            <a href="javascript:void(0)">
//                                <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

//                                <div class="menu-info">
//                                    <h4 class="control-sidebar-subheading">نورا به خبرنامه پیوست</h4>

//                                    <p>nora@example.com</p>
//                                </div>
//                            </a>
//                        </li>
//                        <li>
//                            <a href="javascript:void(0)">
//                                <i class="menu-icon fa fa-file-code-o bg-green"></i>

//                                <div class="menu-info">
//                                    <h4 class="control-sidebar-subheading">کرون جابز اجرا شد</h4>

//                                    <p>۵ ثانیه پیش</p>
//                                </div>
//                            </a>
//                        </li>
//                    </ul>
//                    <!-- /.control-sidebar-menu -->

//                    <h3 class="control-sidebar-heading">پیشرفت کارها</h3>
//                    <ul class="control-sidebar-menu">
//                        <li>
//                            <a href="javascript:void(0)">
//                                <h4 class="control-sidebar-subheading">
//                                    ساخت پوستر های تبلیغاتی
//                                    <span class="label label-danger pull-left">70%</span>
//                                </h4>

//                                <div class="progress progress-xxs">
//                                    <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
//                                </div>
//                            </a>
//                        </li>
//                        <li>
//                            <a href="javascript:void(0)">
//                                <h4 class="control-sidebar-subheading">
//                                    آپدیت رزومه
//                                    <span class="label label-success pull-left">95%</span>
//                                </h4>

//                                <div class="progress progress-xxs">
//                                    <div class="progress-bar progress-bar-success" style="width: 95%"></div>
//                                </div>
//                            </a>
//                        </li>
//                        <li>
//                            <a href="javascript:void(0)">
//                                <h4 class="control-sidebar-subheading">
//                                    آپدیت لاراول
//                                    <span class="label label-warning pull-left">50%</span>
//                                </h4>

//                                <div class="progress progress-xxs">
//                                    <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
//                                </div>
//                            </a>
//                        </li>
//                        <li>
//                            <a href="javascript:void(0)">
//                                <h4 class="control-sidebar-subheading">
//                                    بخش پشتیبانی سایت
//                                    <span class="label label-primary pull-left">68%</span>
//                                </h4>

//                                <div class="progress progress-xxs">
//                                    <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
//                                </div>
//                            </a>
//                        </li>
//                    </ul>
//                    <!-- /.control-sidebar-menu -->

//                </div>
//                <!-- /.tab-pane -->
//                <!-- Stats tab content -->
//                <div class="tab-pane" id="control-sidebar-stats-tab">وضعیت</div>
//                <!-- /.tab-pane -->
//                <!-- Settings tab content -->
//                <div class="tab-pane" id="control-sidebar-settings-tab">
//                    <form method="post">
//                        <h3 class="control-sidebar-heading">تنظیمات عمومی</h3>

//                        <div class="form-group">
//                            <label class="control-sidebar-subheading">
//                                گزارش کنترلر پنل
//                                <input type="checkbox" class="pull-left" checked>
//                            </label>

//                            <p>
//                                ثبت تمامی فعالیت های مدیران
//                            </p>
//                        </div>
//                        <!-- /.form-group -->

//                        <div class="form-group">
//                            <label class="control-sidebar-subheading">
//                                ایمیل مارکتینگ
//                                <input type="checkbox" class="pull-left" checked>
//                            </label>

//                            <p>
//                                اجازه به کاربران برای ارسال ایمیل
//                            </p>
//                        </div>
//                        <!-- /.form-group -->

//                        <div class="form-group">
//                            <label class="control-sidebar-subheading">
//                                در دست تعمیرات
//                                <input type="checkbox" class="pull-left" checked>
//                            </label>

//                            <p>
//                                قرار دادن سایت در حالت در دست تعمیرات
//                            </p>
//                        </div>
//                        <!-- /.form-group -->

//                        <h3 class="control-sidebar-heading">تنظیمات گفتگوها</h3>

//                        <div class="form-group">
//                            <label class="control-sidebar-subheading">
//                                آنلاین بودن من را نشان نده
//                                <input type="checkbox" class="pull-left" checked>
//                            </label>
//                        </div>
//                        <!-- /.form-group -->

//                        <div class="form-group">
//                            <label class="control-sidebar-subheading">
//                                اعلان ها
//                                <input type="checkbox" class="pull-left">
//                            </label>
//                        </div>
//                        <!-- /.form-group -->

//                        <div class="form-group">
//                            <label class="control-sidebar-subheading">
//                                حذف تاریخته گفتگوهای من
//                                <a href="javascript:void(0)" class="text-red pull-left"><i class="fa fa-trash-o"></i></a>
//                            </label>
//                        </div>
//                        <!-- /.form-group -->
//                    </form>
//                </div>
//                <!-- /.tab-pane -->
//            </div>
//        </aside>
//        <!-- /.control-sidebar -->
//        <!-- Add the sidebar's background. This div must be placed
//         immediately after the control sidebar -->
//    </div>

//    }
//}
