﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services.Repositories
{
    public  interface IViewRender
    {
        Task<string> RenderToStringAsync(string viewName, object model);
    }
}
