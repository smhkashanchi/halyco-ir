﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModels.UsersViewModels
{
    public class UserEditViewModel
    {
        public string User_Id { get; set; }
        
        [Display(Name = "تلفن همراه1")]
        [StringLength(12)]
        public string Mobile_1 { get; set; }

        [Display(Name = "تلفن همراه2")]
        [StringLength(12)]
        public string Mobile_2 { get; set; }

        [Display(Name = "تلفن ثابت")]

        public  string PhoneNumber { get; set; }
        [Required(ErrorMessage = "{0} را وارد کنید")]
        [StringLength(200, ErrorMessage = "{0} حداکثر باید 100 کاراکتر باشد")]
        [Display(Name = "نام و نام خانوادگی")]
        public string FullName { get; set; }
        [Required(ErrorMessage = "{0} را وارد کنید")]
        [EmailAddress(ErrorMessage = "{0} نامعتبر نمی باشد")]
        [Display(Name = "ایمیل")]
        [StringLength(100, ErrorMessage = "{0} حداکثر باید 100 کاراکتر باشد")]

        public string Email { get; set; }
        [Required(ErrorMessage = "{0} را وارد کنید")]
        [Display(Name = "نام کاربری")]

        public string UserName { get; set; }


      
    }
}
