﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ViewModels.UsersViewModels
{
    public class UserPhotoViewModel
    {
        [Display(Name ="تصویر پروفایل")]
        [Required(ErrorMessage ="{0} را انتخاب کنید")]

        public IFormFile UserPhoto { get; set; }
        public string UserPhoto_Address { get; set; }
        public bool HasUserPhoto{ get; set; }

    }
}
