﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModels.UsersViewModels
{
    public class UserViewModel
    {
        public string User_Id { get; set; }
        [Display(Name = "تصویر پروفایل")]

        public string UserPhoto { get; set; }
        [RegularExpression(@"^\(?([0-9]{4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "{0} معتبر نمی باشد")]
        [Required(ErrorMessage = "{0} را وارد کنید")]
        [Display(Name = "تلفن همراه1")]
        [StringLength(12)]
        public string Mobile_1 { get; set; }

        [RegularExpression(@"^\(?([0-9]{4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "{0} معتبر نمی باشد")]
        [Display(Name = "تلفن همراه2")]
        [StringLength(12)]
        public string Mobile_2 { get; set; }
        [StringLength(11, ErrorMessage = "{0} باید 11 رقم باشد ")]

        [Display(Name = "تلفن ثابت")]

        public  string PhoneNumber { get; set; }
        [Required(ErrorMessage = "{0} را وارد کنید")]
        [StringLength(200, ErrorMessage = "{0} حداکثر باید 100 کاراکتر باشد")]
        [Display(Name = "نام و نام خانوادگی")]
        public string FullName { get; set; }
        [Required(ErrorMessage = "{0} را وارد کنید")]
        [EmailAddress(ErrorMessage = "{0} نامعتبر نمی باشد")]
        [Display(Name = "ایمیل")]
        [StringLength(100, ErrorMessage = "{0} حداکثر باید 100 کاراکتر باشد")]

        public string Email { get; set; }
        [Required(ErrorMessage = "{0} را وارد کنید")]
        [Display(Name = "نام کاربری")]

        public string UserName { get; set; }

        public string RegisterDate { get; set; }

        [Display(Name = "رمز عبور")]
        [Required(ErrorMessage = "{0} را وارد کنید")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required(ErrorMessage = "{0} را وارد کنید")]
        [DataType(DataType.Password)]
        [Display(Name = "تکرار رمز عبور")]
        [Compare("Password", ErrorMessage = "رمز عبور با تکرار رمز عبور یکسان نیست")]
        public string ConfirmPassword { get; set; }
    }
}
