﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModels.Manage
{
    public class ChangeUserNameViewModel
    {
        [Required(ErrorMessage = "{0} را وارد کنید")]
        [Display(Name = "نام کاربری جدید")]
        [StringLength(100, ErrorMessage = "{0} حداکثر باید 100 کاراکتر باشد")]

        public string NewUserName { get; set; }


        [Compare("NewUserName", ErrorMessage = "نام کاربری جدید با تکرار نام کاربری یکسان نیست")]
        [Display(Name = "تکرار نام کاربری جدید")]
        public string ConfirmUserName { get; set; }


        [StringLength(100, ErrorMessage = "رمز عبور حداقل باید 6 کاراکتر باشد", MinimumLength = 6)]
        [Required(ErrorMessage = "{0} را وارد کنید")]
        [Display(Name = "رمز عبور")]
        public string Password { get; set; }
    }
}
