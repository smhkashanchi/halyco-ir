﻿using DomainClasses.Employees;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ViewModels
{
     public class MissionViewModel
    {
        [Key]
        public int Mission_ID { get; set; }

        [DisplayName("تاریخ شروع ماموریت")]
        [Required(ErrorMessage = "{0} را وارد کنید")]
        public string MissionStart { get; set; }
        [DisplayName("تاریخ پایان ماموریت")]
        [Required(ErrorMessage = "{0} را وارد کنید")]
        public string MissionEnd { get; set; }
        [DisplayName("نوع ماموریت")]
        public byte MissionType { get; set; }
        [DisplayName("علت ماموریت")]
        [Required(ErrorMessage = "{0} را وارد کنید")]
        [MaxLength(250, ErrorMessage = "{0} نمی تواند بیش از 250 کاراکتر باشد")]
        public string Reason { get; set; }
        [DisplayName("همراهان")]
        [MaxLength(200)]
        public string Followers { get; set; }
        [DisplayName("موضوع ماموریت")]
        [MaxLength(900)]
        [Required(ErrorMessage = "{0} را وارد کنید")]
        public string Issue { get; set; }
        [DisplayName("تاریخ صدور")]
        public string IssueDate { get; set; }
        [DisplayName("مقصد")]

        public string Destination { get; set; }
        public bool Mission_Status { get; set; }
        public bool Human_Status { get; set; }

        public virtual Employee Employee_1 { get; set; }
        public string PersonalCode_1 { get; set; }
        public virtual Employee Employee_2 { get; set; }
        public string ApplicantSignature { get; set; }
        public string FullName { get; set; }
        public string FullName_Manager { get; set; }
        public string FullName_HumanResources { get; set; }
        public string TimeMission { get; set; }
        public string ManagerSignature_1 { get; set; }


        public string HumanResourcesSignature_1 { get; set; }


    }
}
