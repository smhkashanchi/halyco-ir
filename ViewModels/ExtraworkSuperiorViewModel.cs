﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModels
{
    public class ExtraworkSuperiorViewModel
    {
        public DomainClasses.Employees.ExtraWork extraWork{get;set;}
        public List<DomainClasses.Employees.ExtraworkSignature> extraworkSignatures { get; set; }
        public DomainClasses.Employees.ExtraworkSignature CurrentSuperiorExtraWorkSignatures { get; set; }
        public bool? ThisUserCanSignature { get; set; }
    }
}
