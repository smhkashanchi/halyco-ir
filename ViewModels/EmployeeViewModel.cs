﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace ViewModels
{
    public class EmployeeViewModel
    {
        public EmployeeViewModel()
        {

        }

        public DomainClasses.Employees.Employee eemployee{get;set;}

        [DisplayName("نقش های کارمند")]
        public string RolesName { get; set; }
    }
}
