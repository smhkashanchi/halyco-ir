﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModels
{
    public class VacationSuperiorViewModel
    {
        public DomainClasses.Employees.Vacation vacation{get;set;}
        public List<DomainClasses.Employees.VacationSignature> vacationSignatures { get; set; }
        public DomainClasses.Employees.VacationSignature CurrentSuperiorVacationSignatures { get; set; }
        public bool? ThisUserCanSignature { get; set; }
    }
}
