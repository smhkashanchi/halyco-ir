﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ViewModels
{
    public class ShoppingCartItems
    {
        public int ProductID { get; set; }
        public int Count { get; set; }
        public int Price { get; set; }
    }

    public class ShopCartItemViewModel
    {
        public int ProductID { get; set; }
        public string Title { get; set; }
        public string ImageName { get; set; }
        public int Count { get; set; }
        public int? ProductPrice { get; set; }
        public int EndPrice { get; set; }
    }
}
