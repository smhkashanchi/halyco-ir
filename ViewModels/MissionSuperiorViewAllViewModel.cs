﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModels
{

    public class MissionSuperiorViewAllViewModel
    {
        /// <summary>
        /// این فیلد بیخودی هست و فقط برای نمایش شماره سطر استفاده میشه و تازه تو بک اند نیز مقدار دهی نمیشه و خالی فرستاده میشه سمت ایجکس
        /// </summary>
        public int row { get; set; }
        public int Mission_ID { get; set; }
        public string CreateDate { get; set; }
        public string MissionStart { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string MissionEnd { get; set; }
        public string MissionSubject { get; set; }
        public bool? FinalStatus { get; set; }
        public bool type { get; set; }
        public string FullName { get; set; }

    }
}
