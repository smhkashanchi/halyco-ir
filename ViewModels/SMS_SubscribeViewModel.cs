﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModels
{
    public class SMS_SubscribeViewModel
    {
        public List<string> Receptor { get; set; }
        public List<string> Sender { get; set; }
        public List<string> Message { get; set; }
    }
}
