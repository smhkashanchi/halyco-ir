﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ViewModels
{
    public class ShowProductViewModel
    {
        public int Product_Id { get; set; }
        public string Product_Title { get; set; }
        //public int Product_Price { get; set; }
        public string Product_Image { get; set; }


    }
}
