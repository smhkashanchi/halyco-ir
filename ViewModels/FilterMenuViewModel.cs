﻿using DomainClasses.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ViewModels
{
    public class FilterMenuViewModel
    {
        public IEnumerable<Color> Colors { get; set; }
        public IEnumerable<Feature> Features { get; set; }
        public IEnumerable<ProductsGroup> Groups { get; set; }
        public int Selected_GroupId { get; set; }

    }
}
