﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ViewModels
{
    public class CustomerVM
    {
        public CustomerVM()
        {

        }
        [DisplayName("نام")]
        [MaxLength(200)]
        public string Name { get; set; }
        [DisplayName("ایمیل")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(200)]
        [EmailAddress(ErrorMessage ="لطفا {0} را بصورت صحیح وارد نمایید")]
        public string Email { get; set; }
        [DisplayName("کلمه عبور")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string Password { get; set; }
        [DisplayName(" تکرار کلمه عبور")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        [Compare("Password",ErrorMessage ="کلمه عبور با تکرار کلمه عبور مغایرت دارند")]
        public string RePassword { get; set; }
    }
}
