﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModels
{
    public class ShiftChangeSuperiorViewModel
    {
        public DomainClasses.Employees.ShiftChange shiftChange{get;set;}
        public List<DomainClasses.Employees.ShiftChangeSignature> shiftChangeSignatures { get; set; }
        public DomainClasses.Employees.ShiftChangeSignature CurrentSuperiorShiftChangeSignatures { get; set; }
        public bool? ThisUserCanSignature { get; set; }
    }
}
