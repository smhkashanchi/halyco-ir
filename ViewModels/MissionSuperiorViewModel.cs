﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModels
{
    public class MissionSuperiorViewModel
    {
        public DomainClasses.Employees.Mission mission{get;set;}
        public List<DomainClasses.Employees.MissionSignature> missionSignature { get; set; }
        public DomainClasses.Employees.MissionSignature CurrentSuperiorMissionSignature { get; set; }
        public bool? ThisUserCanSignature { get; set; }
    }
}
