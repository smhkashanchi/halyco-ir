﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModels
{

    public class ExtraworkSuperiorViewAllViewModel
    {
        /// <summary>
        /// این فیلد بیخودی هست و فقط برای نمایش شماره سطر استفاده میشه و تازه تو بک اند نیز مقدار دهی نمیشه و خالی فرستاده میشه سمت ایجکس
        /// </summary>
        public int row { get; set; }
        public int ExtraWork_ID { get; set; }
        public string DateVacationTime { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string CreateDate { get; set; }
        public bool? FinalStatus { get; set; }
        public string FullName { get; set; }

    }
}
