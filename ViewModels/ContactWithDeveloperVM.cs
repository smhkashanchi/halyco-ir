﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ViewModels
{
    public class ContactWithDeveloperVM
    {
        [DisplayName("نام")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        public string Name { get; set; }
        [DisplayName("تلفن")]

        public string Mobile { get; set; }
        [DisplayName("ایمیل")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [EmailAddress(ErrorMessage ="ایمیل وارد شده معتبر نمی باشد")]
        public string Email { get; set; }
        [DisplayName("متن پیام")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [DataType(DataType.MultilineText)]

        public string MessageText { get; set; }

        public string PageUrl { get; set; }

    }
}
