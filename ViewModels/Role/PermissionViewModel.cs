﻿using DomainClasses.FilterAuthorization;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ViewModels.Role
{
    public class PermissionViewModel
    {
        [Required]
        [StringLength(256, ErrorMessage = "The {0} must be at least {2} characters long.")]
        public string Name { get; set; }
        public string UserFullName { get; set; }

        public IEnumerable<MvcControllerInfo> SelectedControllers { get; set; }
        public List<MvcControllerInfo> ProductControllers { get; set; }
        public List<MvcControllerInfo> ConnectionControllers { get; set; }
        public List<MvcControllerInfo> ContentControllers { get; set; }
        public List<MvcControllerInfo> BaseInformationControllers { get; set; }
        public List<MvcControllerInfo> EmployeeControllers { get; set; }
        public List<MvcControllerInfo> EmployeeCoworkerControllers { get; set; }
        public List<MvcControllerInfo> SurveyControllers { get; set; }
        public List<MvcControllerInfo> EmployementControllers { get; set; }
        public List<MvcControllerInfo> EmployementRequestMySectionControllers { get; set; }
        public List<MvcControllerInfo> EmployementRequestMyUnitControllers { get; set; }
        public List<MvcControllerInfo> EmployementRequestMyCompanyControllers { get; set; }
        public List<MvcControllerInfo> MultiMediaControllers { get; set; }
        public List<MvcControllerInfo> AccessControlControllers { get; set; }
        public List<MvcControllerInfo> HalycoControllers { get; set; }
        public List<MvcControllerInfo> SalaryControllers { get; set; }
        public List<MvcControllerInfo> ProposalControllers { get; set; }
        public List<MvcControllerInfo> SuppliersControllers { get; set; }
        public List<MvcControllerInfo> CustomerControllers { get; set; }

    }
}