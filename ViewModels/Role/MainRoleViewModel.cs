﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModels.Role
{
  public  class MainRoleViewModel
    {
        public MainRoleViewModel()
        {

        }
        public string Id { get; set; }
        [Display(Name = (" نام نقش"))]
        [StringLength(100, ErrorMessage = "{0} حداکثر باید 100 کاراکتر باشد")]
        [Required(ErrorMessage = "{0} را وارد کنید")]
        public string Title { get; set; }
        [Display(Name = ("وظیفه"))]
        [StringLength(100, ErrorMessage = "{0} حداکثر باید 100 کاراکتر باشد")]
        public string Task { get; set; }
        [Display(Name = ("مسئولیت"))]
        [StringLength(100, ErrorMessage = "{0} حداکثر باید 100 کاراکتر باشد")]
        public string Responsibilty { get; set; }

        [Display(Name = ("توضیحات"))]

        public string Description { get; set; }
        [Display(Name = ("وضعیت نفش"))]

        public byte Role_Status { get; set; }

        [Display(Name = ("نقش پدر"))]
        public string ParentRole { get; set; }

        [Display(Name = ("نوع"))]
        public byte Role_Type { get; set; }
        public string Access { get; set; }

        [Display(Name = ("نقشی که تعیین وضعیت نهایی درخواست مرخصی را مشخص می کند"))]
        public string SendFinalSmsToUser { get; set; }

        [DisplayName("آیا پیامک مرخصی برای نقش پدر نیز ارسال شود؟")]
        public bool SendSmsToParent { get; set; }

        [DisplayName("آیا نماینده مدیریت(مدیر پشتیبانی) درخواست های این کاربر را ببیند؟")]
        public bool IsAgentOfManager { get; set; }
    }
}
