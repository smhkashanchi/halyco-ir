﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ViewModels
{
    public class GeneralUserViewModel
    {
        public GeneralUserViewModel()
        {

        }
        [DisplayName("ایمیل")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        public string Email { get; set; }
        [DisplayName("نام کاربری")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string UserName { get; set; }
        [DisplayName("تلفن")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string PhoneNumber { get; set; }
    }
}
