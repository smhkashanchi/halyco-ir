﻿using System;
using System.Collections.Generic;
using System.Text;
using DomainClasses.Product;
using DomainClasses.SlideShow;
using DomainClasses.Connection;

namespace ViewModels
{
    public class HalycoViewModel
    {
        public HalycoViewModel()
        {

        }

        public Dictionary<int, string> staticPage_dic;
        public IEnumerable<ProductInfo> productInfos;
        public IEnumerable<SlideShow> slideShows;
        public IEnumerable<Connection> connections;
        public string aboutUsSummmary;

    }
}
