﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModels
{

    public class VacationSuperiorViewAllViewModel
    {
        /// <summary>
        /// این فیلد بیخودی هست و فقط برای نمایش شماره سطر استفاده میشه و تازه تو بک اند نیز مقدار دهی نمیشه و خالی فرستاده میشه سمت ایجکس
        /// </summary>
        public int row { get; set; }
        public int Vacation_ID { get; set; }
        public bool VacationType { get; set; }
        public string DateVacationTime { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string DayVacationStartDate { get; set; }
        public string DayVacationEndDate { get; set; }
        public string CreateDate { get; set; }
        public bool? FinalStatus { get; set; }
        public string FullName { get; set; }

    }
}
