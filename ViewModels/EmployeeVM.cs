﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModels
{
    public class EmployeeVM
    {
        public EmployeeVM()
        {

        }
        [DisplayName("کد پرسنلی")]
        [MaxLength(10,ErrorMessage ="کد پرسنلی بیش از 10 کاراکتر مجاز نمی باشد")]
        [Required(ErrorMessage = "درج کد پرسنلی الزامی می باشد")]
        public string PersonalCode { get; set; }

        public string UserIdentity_Id { get; set; }
        [DisplayName("نام و نام خانوادگی")]
        [Required(ErrorMessage = "نام و نام خانوادگی کارمند را درج نمایید")]
        [MaxLength(200, ErrorMessage = "نام و نام خانوادگی نمی تواند بیش از 200 کاراکتر باشد")]
        public string FullName { get; set; }

        [DisplayName("کد ملی")]
        [MaxLength(10, ErrorMessage = "کد ملی حتما باید 10 رقم باشد")]
        [Required(ErrorMessage = "درج کد ملی الزامی می باشد")]
        public string NationalCode { get; set; }


        [DisplayName("کلمه عبور")]
        [MinLength(6,ErrorMessage ="کلمه عبور باید بیش از 5 کاراکتر باشد")]
        [MaxLength(50)]
        [Required(ErrorMessage = "درج کلمه عبور الزامی می باشد")]
        public string PersonPassword { get; set; }
        [DisplayName("تکرار کلمه عبور")]
        [MaxLength(50)]
        [Required(ErrorMessage = "تکرار کلمه عبور الزامی می باشد")]
        [Compare("PersonPassword", ErrorMessage = "تکرار کلمه عبور با کلمه عبور یکسان نمی باشد")]
        public string PersonRePassword { get; set; }

        [DisplayName("تصویر")]
        [MaxLength(500)]
        //[Required(ErrorMessage = "درج کلمه عبور الزامی می باشد")]
        public string PersonPic { get; set; }

        [DisplayName("تلفن ثابت")]
        [MaxLength(15)]
        //[Required(ErrorMessage = "درج کلمه عبور الزامی می باشد")]
        public string Tel { get; set; }

        [DisplayName("تلفن همراه")]
        [MaxLength(15)]
        [Required(ErrorMessage = "درج تلفن همراه الزامی می باشد")]
        public string Mobile { get; set; }

        [DisplayName("سمت")]
        [Required(ErrorMessage = "انتخاب سمت الزامی می باشد")]
        public Int16 OfficePostId { get; set; }

        [DisplayName("واحد")]
        [Required(ErrorMessage = "انتخاب واحد الزامی می باشد")]
        public Int16 OfficeUnitId { get; set; }


        [DisplayName("فعال / غیرفعال")]
        public bool IsActive { get; set; }

        [DisplayName("وظایف")]
        public string Responsiblity { get; set; }

        [DisplayName("وضعیت شیفت")]
        public bool HasShift { get; set; }

        [DisplayName("وضعیت جایگزین")]
        public bool HasAlternateEmployee { get; set; }

        [DisplayName("شیفت")]
        public string ShiftName { get; set; }

        [DisplayName("سطح نظارت")]
        public byte? SupervisionLevel { get; set; }

        [DisplayName("شغل")]
        public string Job { get; set; }
    }
}
