﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ViewModels
{
    public class CityViewModel
    {
        public CityViewModel()
        {

        }
        public int City_Id { get; set; }
        public string City_Name { get; set; }
    }
}
