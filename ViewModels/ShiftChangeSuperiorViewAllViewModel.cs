﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModels
{

    public class ShiftChangeSuperiorViewAllViewModel
    {
        /// <summary>
        /// این فیلد بیخودی هست و فقط برای نمایش شماره سطر استفاده میشه و تازه تو بک اند نیز مقدار دهی نمیشه و خالی فرستاده میشه سمت ایجکس
        /// </summary>
        public int row { get; set; }
        public int ShiftChange_ID { get; set; }
        public string CreateDate { get; set; }
        public string shiftDate { get; set; }
        public bool? FinalStatus { get; set; }
        public string FullName { get; set; }
        public string AlternateFullName { get; set; }

    }
}
