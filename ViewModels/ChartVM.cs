﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModels
{
    public class Pie_ChartVM
    {
        public string OptionTitle { get; set; }
        public float Votes { get; set; }
    }
}
