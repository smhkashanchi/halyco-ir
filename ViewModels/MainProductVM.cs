﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ViewModels
{
    public class MainProductVM
    {
        public int Product_ID { get; set; }
        public string ProductImage { get; set; }
        public string ProductCatalog { get; set; }
        public Int16? GalleryId { get; set; }
        public string ProductCode { get; set; }
        public bool IsPopular { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsExsit { get; set; }
        public int? ExistCount { get; set; }
        public bool IsDecor { get; set; }
        public int? SaledCount { get; set; }
        public bool IsActive { get; set; }
        public int smallPicWidth { get; set; }
        public int smallPicHeight { get; set; }

    }
}
