﻿using DomainClasses.Survey;
using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModels
{
    public class SurveyVM
    {
        public SurveyVM()
        {

        }
        public IEnumerable<SurvayQuestionGroup> SurvayQuestionGroup { get; set; }
        public SurveyAnswerType SurveyAnswerType { get; set; }
        public IEnumerable<SurveyQuestion> SurveyQuestions { get; set; }

        public IEnumerable<SurveyQuestionReply> surveyQuestionReplies { get; set; }

        public SurveyCustomerOtherData SurveyCustomerOtherData { get; set; } 

        public IEnumerable<CustomerSurveyProducts> customerSurveyProducts { get; set; }

        public SurveyEmployeesOtherData surveyEmployeesOtherData { get; set; }

        public SurveySuppliersOtherData SurveySuppliersOtherData { get; set; }
    }
}
