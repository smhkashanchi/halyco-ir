﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e14 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AdministrativeAffairsReason",
                table: "Vacation_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AlternateSignatureReason",
                table: "Vacation_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "StatusAlternateSignature",
                table: "Vacation_tb",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "UnitsSupervisorReason",
                table: "Vacation_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "HasAlternateEmployee",
                table: "Employee_tb",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "HasShift",
                table: "Employee_tb",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "ShiftName",
                table: "Employee_tb",
                maxLength: 50,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdministrativeAffairsReason",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "AlternateSignatureReason",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "StatusAlternateSignature",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "UnitsSupervisorReason",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "HasAlternateEmployee",
                table: "Employee_tb");

            migrationBuilder.DropColumn(
                name: "HasShift",
                table: "Employee_tb");

            migrationBuilder.DropColumn(
                name: "ShiftName",
                table: "Employee_tb");
        }
    }
}
