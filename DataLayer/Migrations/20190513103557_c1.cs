﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class c1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProposalProviders_tb_OfficePost_tb_OfficePostID",
                table: "ProposalProviders_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_ProposalProviders_tb_OfficeUnit_tb_OfficeUnitID",
                table: "ProposalProviders_tb");

            migrationBuilder.DropIndex(
                name: "IX_ProposalProviders_tb_OfficePostID",
                table: "ProposalProviders_tb");

            migrationBuilder.DropIndex(
                name: "IX_ProposalProviders_tb_OfficeUnitID",
                table: "ProposalProviders_tb");

            migrationBuilder.DropColumn(
                name: "OfficePostID",
                table: "ProposalProviders_tb");

            migrationBuilder.DropColumn(
                name: "OfficeUnitID",
                table: "ProposalProviders_tb");

            migrationBuilder.AlterColumn<string>(
                name: "SmokingReason",
                table: "Emp_BaseInformation_tb",
                maxLength: 200,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 200);

            migrationBuilder.AlterColumn<short>(
                name: "OvertimeHours",
                table: "Emp_BaseInformation_tb",
                nullable: true,
                oldClrType: typeof(short));

            migrationBuilder.AlterColumn<string>(
                name: "HomeDescription",
                table: "Emp_BaseInformation_tb",
                maxLength: 200,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 200);

            migrationBuilder.AlterColumn<string>(
                name: "CriminalRecordReason",
                table: "Emp_BaseInformation_tb",
                maxLength: 200,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 200);

            migrationBuilder.CreateIndex(
                name: "IX_ProposalProviders_tb_PostId",
                table: "ProposalProviders_tb",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_ProposalProviders_tb_UnitId",
                table: "ProposalProviders_tb",
                column: "UnitId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProposalProviders_tb_OfficePost_tb_PostId",
                table: "ProposalProviders_tb",
                column: "PostId",
                principalTable: "OfficePost_tb",
                principalColumn: "OfficePostID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProposalProviders_tb_OfficeUnit_tb_UnitId",
                table: "ProposalProviders_tb",
                column: "UnitId",
                principalTable: "OfficeUnit_tb",
                principalColumn: "OfficeUnitID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProposalProviders_tb_OfficePost_tb_PostId",
                table: "ProposalProviders_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_ProposalProviders_tb_OfficeUnit_tb_UnitId",
                table: "ProposalProviders_tb");

            migrationBuilder.DropIndex(
                name: "IX_ProposalProviders_tb_PostId",
                table: "ProposalProviders_tb");

            migrationBuilder.DropIndex(
                name: "IX_ProposalProviders_tb_UnitId",
                table: "ProposalProviders_tb");

            migrationBuilder.AddColumn<short>(
                name: "OfficePostID",
                table: "ProposalProviders_tb",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "OfficeUnitID",
                table: "ProposalProviders_tb",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "SmokingReason",
                table: "Emp_BaseInformation_tb",
                maxLength: 200,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 200,
                oldNullable: true);

            migrationBuilder.AlterColumn<short>(
                name: "OvertimeHours",
                table: "Emp_BaseInformation_tb",
                nullable: false,
                oldClrType: typeof(short),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "HomeDescription",
                table: "Emp_BaseInformation_tb",
                maxLength: 200,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 200,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CriminalRecordReason",
                table: "Emp_BaseInformation_tb",
                maxLength: 200,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 200,
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProposalProviders_tb_OfficePostID",
                table: "ProposalProviders_tb",
                column: "OfficePostID");

            migrationBuilder.CreateIndex(
                name: "IX_ProposalProviders_tb_OfficeUnitID",
                table: "ProposalProviders_tb",
                column: "OfficeUnitID");

            migrationBuilder.AddForeignKey(
                name: "FK_ProposalProviders_tb_OfficePost_tb_OfficePostID",
                table: "ProposalProviders_tb",
                column: "OfficePostID",
                principalTable: "OfficePost_tb",
                principalColumn: "OfficePostID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProposalProviders_tb_OfficeUnit_tb_OfficeUnitID",
                table: "ProposalProviders_tb",
                column: "OfficeUnitID",
                principalTable: "OfficeUnit_tb",
                principalColumn: "OfficeUnitID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
