﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e20 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_shiftChange_tb_Employee_tb_PersonalCode_manager",
                table: "shiftChange_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_shiftChange_tb_Employee_tb_PersonalCode_superior",
                table: "shiftChange_tb");

            migrationBuilder.AlterColumn<bool>(
                name: "StatusSuperiorSignature",
                table: "shiftChange_tb",
                nullable: true,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "StatusManagerSignature",
                table: "shiftChange_tb",
                nullable: true,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<string>(
                name: "PersonalCode_superior",
                table: "shiftChange_tb",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "PersonalCode_manager",
                table: "shiftChange_tb",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddForeignKey(
                name: "FK_shiftChange_tb_Employee_tb_PersonalCode_manager",
                table: "shiftChange_tb",
                column: "PersonalCode_manager",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_shiftChange_tb_Employee_tb_PersonalCode_superior",
                table: "shiftChange_tb",
                column: "PersonalCode_superior",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_shiftChange_tb_Employee_tb_PersonalCode_manager",
                table: "shiftChange_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_shiftChange_tb_Employee_tb_PersonalCode_superior",
                table: "shiftChange_tb");

            migrationBuilder.AlterColumn<bool>(
                name: "StatusSuperiorSignature",
                table: "shiftChange_tb",
                nullable: false,
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "StatusManagerSignature",
                table: "shiftChange_tb",
                nullable: false,
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PersonalCode_superior",
                table: "shiftChange_tb",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PersonalCode_manager",
                table: "shiftChange_tb",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_shiftChange_tb_Employee_tb_PersonalCode_manager",
                table: "shiftChange_tb",
                column: "PersonalCode_manager",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_shiftChange_tb_Employee_tb_PersonalCode_superior",
                table: "shiftChange_tb",
                column: "PersonalCode_superior",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
