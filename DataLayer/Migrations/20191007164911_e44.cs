﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e44 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCode",
                table: "Vacation_tb");

            migrationBuilder.RenameColumn(
                name: "PersonalCode",
                table: "Vacation_tb",
                newName: "PersonalCodeFirst");

            migrationBuilder.RenameIndex(
                name: "IX_Vacation_tb_PersonalCode",
                table: "Vacation_tb",
                newName: "IX_Vacation_tb_PersonalCodeFirst");

            migrationBuilder.AddForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCodeFirst",
                table: "Vacation_tb",
                column: "PersonalCodeFirst",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCodeFirst",
                table: "Vacation_tb");

            migrationBuilder.RenameColumn(
                name: "PersonalCodeFirst",
                table: "Vacation_tb",
                newName: "PersonalCode");

            migrationBuilder.RenameIndex(
                name: "IX_Vacation_tb_PersonalCodeFirst",
                table: "Vacation_tb",
                newName: "IX_Vacation_tb_PersonalCode");

            migrationBuilder.AddForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCode",
                table: "Vacation_tb",
                column: "PersonalCode",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
