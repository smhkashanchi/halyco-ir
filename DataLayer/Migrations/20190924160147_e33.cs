﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e33 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "HRManagerReason",
                table: "shiftChange_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PersonalCode_HRmanager",
                table: "shiftChange_tb",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "StatusHRManagerSignature",
                table: "shiftChange_tb",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_shiftChange_tb_PersonalCode_HRmanager",
                table: "shiftChange_tb",
                column: "PersonalCode_HRmanager");

            migrationBuilder.AddForeignKey(
                name: "FK_shiftChange_tb_Employee_tb_PersonalCode_HRmanager",
                table: "shiftChange_tb",
                column: "PersonalCode_HRmanager",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_shiftChange_tb_Employee_tb_PersonalCode_HRmanager",
                table: "shiftChange_tb");

            migrationBuilder.DropIndex(
                name: "IX_shiftChange_tb_PersonalCode_HRmanager",
                table: "shiftChange_tb");

            migrationBuilder.DropColumn(
                name: "HRManagerReason",
                table: "shiftChange_tb");

            migrationBuilder.DropColumn(
                name: "PersonalCode_HRmanager",
                table: "shiftChange_tb");

            migrationBuilder.DropColumn(
                name: "StatusHRManagerSignature",
                table: "shiftChange_tb");
        }
    }
}
