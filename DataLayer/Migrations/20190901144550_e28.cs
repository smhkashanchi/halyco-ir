﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e28 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "HRManagerReason",
                table: "Vacation_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PersonalCode_HRmanager",
                table: "Vacation_tb",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "StatusHRManagerSignature",
                table: "Vacation_tb",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Vacation_tb_PersonalCode_HRmanager",
                table: "Vacation_tb",
                column: "PersonalCode_HRmanager");

            migrationBuilder.AddForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCode_HRmanager",
                table: "Vacation_tb",
                column: "PersonalCode_HRmanager",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCode_HRmanager",
                table: "Vacation_tb");

            migrationBuilder.DropIndex(
                name: "IX_Vacation_tb_PersonalCode_HRmanager",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "HRManagerReason",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "PersonalCode_HRmanager",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "StatusHRManagerSignature",
                table: "Vacation_tb");
        }
    }
}
