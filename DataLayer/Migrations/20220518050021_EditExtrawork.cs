﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class EditExtrawork : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExtraWork_tb_Employee_tb_PersonalCode_HRmanager",
                table: "ExtraWork_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_ExtraWork_tb_Employee_tb_PersonalCode_manager",
                table: "ExtraWork_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_ExtraWork_tb_Employee_tb_PersonalCode_superior",
                table: "ExtraWork_tb");

            migrationBuilder.DropIndex(
                name: "IX_ExtraWork_tb_PersonalCode_HRmanager",
                table: "ExtraWork_tb");

            migrationBuilder.DropIndex(
                name: "IX_ExtraWork_tb_PersonalCode_manager",
                table: "ExtraWork_tb");

            migrationBuilder.DropIndex(
                name: "IX_ExtraWork_tb_PersonalCode_superior",
                table: "ExtraWork_tb");

            migrationBuilder.DropColumn(
                name: "HRManagerDateTime",
                table: "ExtraWork_tb");

            migrationBuilder.DropColumn(
                name: "HRManagerReason",
                table: "ExtraWork_tb");

            migrationBuilder.DropColumn(
                name: "ManagerDateTime",
                table: "ExtraWork_tb");

            migrationBuilder.DropColumn(
                name: "ManagerReason",
                table: "ExtraWork_tb");

            migrationBuilder.DropColumn(
                name: "PersonalCode_HRmanager",
                table: "ExtraWork_tb");

            migrationBuilder.DropColumn(
                name: "PersonalCode_manager",
                table: "ExtraWork_tb");

            migrationBuilder.DropColumn(
                name: "StatusHRManagerSignature",
                table: "ExtraWork_tb");

            migrationBuilder.DropColumn(
                name: "StatusManagerSignature",
                table: "ExtraWork_tb");

            migrationBuilder.DropColumn(
                name: "SuperiorDateTime",
                table: "ExtraWork_tb");

            migrationBuilder.DropColumn(
                name: "SuperiorReason",
                table: "ExtraWork_tb");

            migrationBuilder.RenameColumn(
                name: "StatusSuperiorSignature",
                table: "ExtraWork_tb",
                newName: "FinalStatus");

            migrationBuilder.RenameColumn(
                name: "PersonalCode_superior",
                table: "ExtraWork_tb",
                newName: "PersonalCodeFirst");

            migrationBuilder.AlterColumn<string>(
                name: "PersonalCodeFirst",
                table: "ExtraWork_tb",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "ExtraWorkSignature_tb",
                columns: table => new
                {
                    ExtraworkSignature_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ExtraWork_ID = table.Column<int>(nullable: false),
                    PersonalCodeSignaturer = table.Column<string>(maxLength: 450, nullable: false),
                    SignatureStatus = table.Column<bool>(nullable: true),
                    SignatureReason = table.Column<string>(maxLength: 1000, nullable: true),
                    SignatureDateTime = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExtraWorkSignature_tb", x => x.ExtraworkSignature_ID);
                    table.ForeignKey(
                        name: "FK_ExtraWorkSignature_tb_ExtraWork_tb_ExtraWork_ID",
                        column: x => x.ExtraWork_ID,
                        principalTable: "ExtraWork_tb",
                        principalColumn: "ExtraWorkID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExtraWorkSignature_tb_AspNetUsers_PersonalCodeSignaturer",
                        column: x => x.PersonalCodeSignaturer,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExtraWorkSignature_tb_ExtraWork_ID",
                table: "ExtraWorkSignature_tb",
                column: "ExtraWork_ID");

            migrationBuilder.CreateIndex(
                name: "IX_ExtraWorkSignature_tb_PersonalCodeSignaturer",
                table: "ExtraWorkSignature_tb",
                column: "PersonalCodeSignaturer");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExtraWorkSignature_tb");

            migrationBuilder.RenameColumn(
                name: "PersonalCodeFirst",
                table: "ExtraWork_tb",
                newName: "PersonalCode_superior");

            migrationBuilder.RenameColumn(
                name: "FinalStatus",
                table: "ExtraWork_tb",
                newName: "StatusSuperiorSignature");

            migrationBuilder.AlterColumn<string>(
                name: "PersonalCode_superior",
                table: "ExtraWork_tb",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "HRManagerDateTime",
                table: "ExtraWork_tb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HRManagerReason",
                table: "ExtraWork_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ManagerDateTime",
                table: "ExtraWork_tb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ManagerReason",
                table: "ExtraWork_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PersonalCode_HRmanager",
                table: "ExtraWork_tb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PersonalCode_manager",
                table: "ExtraWork_tb",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "StatusHRManagerSignature",
                table: "ExtraWork_tb",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "StatusManagerSignature",
                table: "ExtraWork_tb",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "SuperiorDateTime",
                table: "ExtraWork_tb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SuperiorReason",
                table: "ExtraWork_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ExtraWork_tb_PersonalCode_HRmanager",
                table: "ExtraWork_tb",
                column: "PersonalCode_HRmanager");

            migrationBuilder.CreateIndex(
                name: "IX_ExtraWork_tb_PersonalCode_manager",
                table: "ExtraWork_tb",
                column: "PersonalCode_manager");

            migrationBuilder.CreateIndex(
                name: "IX_ExtraWork_tb_PersonalCode_superior",
                table: "ExtraWork_tb",
                column: "PersonalCode_superior");

            migrationBuilder.AddForeignKey(
                name: "FK_ExtraWork_tb_Employee_tb_PersonalCode_HRmanager",
                table: "ExtraWork_tb",
                column: "PersonalCode_HRmanager",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ExtraWork_tb_Employee_tb_PersonalCode_manager",
                table: "ExtraWork_tb",
                column: "PersonalCode_manager",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ExtraWork_tb_Employee_tb_PersonalCode_superior",
                table: "ExtraWork_tb",
                column: "PersonalCode_superior",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
