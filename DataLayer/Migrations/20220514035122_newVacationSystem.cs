﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class newVacationSystem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCodeFirst",
                table: "Vacation_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCodeSecond",
                table: "Vacation_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCode_HRmanager",
                table: "Vacation_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCode_manager",
                table: "Vacation_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCode_superior",
                table: "Vacation_tb");

            migrationBuilder.DropIndex(
                name: "IX_Vacation_tb_PersonalCodeSecond",
                table: "Vacation_tb");

            migrationBuilder.DropIndex(
                name: "IX_Vacation_tb_PersonalCode_HRmanager",
                table: "Vacation_tb");

            migrationBuilder.DropIndex(
                name: "IX_Vacation_tb_PersonalCode_manager",
                table: "Vacation_tb");

            migrationBuilder.DropIndex(
                name: "IX_Vacation_tb_PersonalCode_superior",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "AlternateDateTime",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "AlternateSignatureReason",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "HRManagerDateTime",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "HRManagerReason",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "ManagerDateTime",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "ManagerReason",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "PersonalCodeSecond",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "PersonalCode_HRmanager",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "PersonalCode_manager",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "PersonalCode_superior",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "StatusAlternateSignature",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "StatusHRManagerSignature",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "StatusManagerSignature",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "SuperiorDateTime",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "SuperiorReason",
                table: "Vacation_tb");

            migrationBuilder.RenameColumn(
                name: "StatusSuperiorSignature",
                table: "Vacation_tb",
                newName: "FinalStatus");

            migrationBuilder.AlterColumn<string>(
                name: "VacationReason",
                table: "Vacation_tb",
                maxLength: 300,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 300);

            migrationBuilder.AlterColumn<string>(
                name: "PersonalCodeFirst",
                table: "Vacation_tb",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.CreateTable(
                name: "VacationSignature_tb",
                columns: table => new
                {
                    VacationSignature_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Vacation_ID = table.Column<int>(nullable: false),
                    PersonalCodeSignaturer = table.Column<string>(maxLength: 450, nullable: false),
                    SignatureStatus = table.Column<bool>(nullable: true),
                    SignatureReason = table.Column<string>(maxLength: 1000, nullable: true),
                    SignatureDateTime = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VacationSignature_tb", x => x.VacationSignature_ID);
                    table.ForeignKey(
                        name: "FK_VacationSignature_tb_AspNetUsers_PersonalCodeSignaturer",
                        column: x => x.PersonalCodeSignaturer,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VacationSignature_tb_Vacation_tb_Vacation_ID",
                        column: x => x.Vacation_ID,
                        principalTable: "Vacation_tb",
                        principalColumn: "Vacation_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_VacationSignature_tb_PersonalCodeSignaturer",
                table: "VacationSignature_tb",
                column: "PersonalCodeSignaturer");

            migrationBuilder.CreateIndex(
                name: "IX_VacationSignature_tb_Vacation_ID",
                table: "VacationSignature_tb",
                column: "Vacation_ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Vacation_tb_AspNetUsers_PersonalCodeFirst",
                table: "Vacation_tb",
                column: "PersonalCodeFirst",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vacation_tb_AspNetUsers_PersonalCodeFirst",
                table: "Vacation_tb");

            migrationBuilder.DropTable(
                name: "VacationSignature_tb");

            migrationBuilder.RenameColumn(
                name: "FinalStatus",
                table: "Vacation_tb",
                newName: "StatusSuperiorSignature");

            migrationBuilder.AlterColumn<string>(
                name: "VacationReason",
                table: "Vacation_tb",
                maxLength: 300,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 300,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PersonalCodeFirst",
                table: "Vacation_tb",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<DateTime>(
                name: "AlternateDateTime",
                table: "Vacation_tb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AlternateSignatureReason",
                table: "Vacation_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "HRManagerDateTime",
                table: "Vacation_tb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HRManagerReason",
                table: "Vacation_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ManagerDateTime",
                table: "Vacation_tb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ManagerReason",
                table: "Vacation_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PersonalCodeSecond",
                table: "Vacation_tb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PersonalCode_HRmanager",
                table: "Vacation_tb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PersonalCode_manager",
                table: "Vacation_tb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PersonalCode_superior",
                table: "Vacation_tb",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "StatusAlternateSignature",
                table: "Vacation_tb",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "StatusHRManagerSignature",
                table: "Vacation_tb",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "StatusManagerSignature",
                table: "Vacation_tb",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "SuperiorDateTime",
                table: "Vacation_tb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SuperiorReason",
                table: "Vacation_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Vacation_tb_PersonalCodeSecond",
                table: "Vacation_tb",
                column: "PersonalCodeSecond");

            migrationBuilder.CreateIndex(
                name: "IX_Vacation_tb_PersonalCode_HRmanager",
                table: "Vacation_tb",
                column: "PersonalCode_HRmanager");

            migrationBuilder.CreateIndex(
                name: "IX_Vacation_tb_PersonalCode_manager",
                table: "Vacation_tb",
                column: "PersonalCode_manager");

            migrationBuilder.CreateIndex(
                name: "IX_Vacation_tb_PersonalCode_superior",
                table: "Vacation_tb",
                column: "PersonalCode_superior");

            migrationBuilder.AddForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCodeFirst",
                table: "Vacation_tb",
                column: "PersonalCodeFirst",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCodeSecond",
                table: "Vacation_tb",
                column: "PersonalCodeSecond",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCode_HRmanager",
                table: "Vacation_tb",
                column: "PersonalCode_HRmanager",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCode_manager",
                table: "Vacation_tb",
                column: "PersonalCode_manager",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCode_superior",
                table: "Vacation_tb",
                column: "PersonalCode_superior",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
