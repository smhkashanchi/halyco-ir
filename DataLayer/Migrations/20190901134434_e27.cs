﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e27 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "HRManagerReason",
                table: "ExtraWork_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PersonalCode_HRmanager",
                table: "ExtraWork_tb",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "StatusHRManagerSignature",
                table: "ExtraWork_tb",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ExtraWork_tb_PersonalCode_HRmanager",
                table: "ExtraWork_tb",
                column: "PersonalCode_HRmanager");

            migrationBuilder.AddForeignKey(
                name: "FK_ExtraWork_tb_Employee_tb_PersonalCode_HRmanager",
                table: "ExtraWork_tb",
                column: "PersonalCode_HRmanager",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExtraWork_tb_Employee_tb_PersonalCode_HRmanager",
                table: "ExtraWork_tb");

            migrationBuilder.DropIndex(
                name: "IX_ExtraWork_tb_PersonalCode_HRmanager",
                table: "ExtraWork_tb");

            migrationBuilder.DropColumn(
                name: "HRManagerReason",
                table: "ExtraWork_tb");

            migrationBuilder.DropColumn(
                name: "PersonalCode_HRmanager",
                table: "ExtraWork_tb");

            migrationBuilder.DropColumn(
                name: "StatusHRManagerSignature",
                table: "ExtraWork_tb");
        }
    }
}
