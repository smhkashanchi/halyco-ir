﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e11 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SeoStaticPage_tb");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SeoStaticPage_tb",
                columns: table => new
                {
                    SeoPage_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MetaDescription = table.Column<string>(maxLength: 500, nullable: true),
                    MetaKeyword = table.Column<string>(maxLength: 300, nullable: true),
                    MetaOther = table.Column<string>(maxLength: 500, nullable: true),
                    MetaTitle = table.Column<string>(maxLength: 200, nullable: true),
                    PageId = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SeoStaticPage_tb", x => x.SeoPage_ID);
                });
        }
    }
}
