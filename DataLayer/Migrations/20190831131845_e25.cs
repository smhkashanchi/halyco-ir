﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e25 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<byte>(
                name: "loanReason",
                table: "loan_tb",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HRManagerReason",
                table: "loan_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PersonalCode_HRmanager",
                table: "loan_tb",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "StatusHRManagerSignature",
                table: "loan_tb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "loanReasonDescription",
                table: "loan_tb",
                maxLength: 100,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_loan_tb_PersonalCode_HRmanager",
                table: "loan_tb",
                column: "PersonalCode_HRmanager");

            migrationBuilder.AddForeignKey(
                name: "FK_loan_tb_Employee_tb_PersonalCode_HRmanager",
                table: "loan_tb",
                column: "PersonalCode_HRmanager",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_loan_tb_Employee_tb_PersonalCode_HRmanager",
                table: "loan_tb");

            migrationBuilder.DropIndex(
                name: "IX_loan_tb_PersonalCode_HRmanager",
                table: "loan_tb");

            migrationBuilder.DropColumn(
                name: "HRManagerReason",
                table: "loan_tb");

            migrationBuilder.DropColumn(
                name: "PersonalCode_HRmanager",
                table: "loan_tb");

            migrationBuilder.DropColumn(
                name: "StatusHRManagerSignature",
                table: "loan_tb");

            migrationBuilder.DropColumn(
                name: "loanReasonDescription",
                table: "loan_tb");

            migrationBuilder.AlterColumn<string>(
                name: "loanReason",
                table: "loan_tb",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(byte));
        }
    }
}
