﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e23 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "loan_tb",
                columns: table => new
                {
                    loanID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PersonalCode = table.Column<string>(nullable: false),
                    loanType = table.Column<string>(maxLength: 50, nullable: false),
                    loanReason = table.Column<string>(maxLength: 100, nullable: true),
                    amount = table.Column<int>(nullable: false),
                    Description = table.Column<string>(maxLength: 1000, nullable: true),
                    PersonalCode_superior = table.Column<string>(nullable: true),
                    SuperiorReason = table.Column<string>(maxLength: 500, nullable: true),
                    StatusSuperiorSignature = table.Column<bool>(nullable: true),
                    PersonalCode_manager = table.Column<string>(nullable: true),
                    ManagerReason = table.Column<string>(maxLength: 500, nullable: true),
                    StatusManagerSignature = table.Column<bool>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_loan_tb", x => x.loanID);
                    table.ForeignKey(
                        name: "FK_loan_tb_Employee_tb_PersonalCode",
                        column: x => x.PersonalCode,
                        principalTable: "Employee_tb",
                        principalColumn: "PersonalCode",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_loan_tb_Employee_tb_PersonalCode_manager",
                        column: x => x.PersonalCode_manager,
                        principalTable: "Employee_tb",
                        principalColumn: "PersonalCode",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_loan_tb_Employee_tb_PersonalCode_superior",
                        column: x => x.PersonalCode_superior,
                        principalTable: "Employee_tb",
                        principalColumn: "PersonalCode",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_loan_tb_PersonalCode",
                table: "loan_tb",
                column: "PersonalCode");

            migrationBuilder.CreateIndex(
                name: "IX_loan_tb_PersonalCode_manager",
                table: "loan_tb",
                column: "PersonalCode_manager");

            migrationBuilder.CreateIndex(
                name: "IX_loan_tb_PersonalCode_superior",
                table: "loan_tb",
                column: "PersonalCode_superior");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "loan_tb");
        }
    }
}
