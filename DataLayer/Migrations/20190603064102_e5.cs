﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "SY_Morkhasi_Saat",
                table: "EmployeeSalary",
                nullable: true,
                oldClrType: typeof(float));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<float>(
                name: "SY_Morkhasi_Saat",
                table: "EmployeeSalary",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
