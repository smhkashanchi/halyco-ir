﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class EditRoleInNewVacationSystem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SendFinalSmsToUser",
                table: "AspNetRoles",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "SendSmsToParent",
                table: "AspNetRoles",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoles_SendFinalSmsToUser",
                table: "AspNetRoles",
                column: "SendFinalSmsToUser");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetRoles_AspNetRoles_SendFinalSmsToUser",
                table: "AspNetRoles",
                column: "SendFinalSmsToUser",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetRoles_AspNetRoles_SendFinalSmsToUser",
                table: "AspNetRoles");

            migrationBuilder.DropIndex(
                name: "IX_AspNetRoles_SendFinalSmsToUser",
                table: "AspNetRoles");

            migrationBuilder.DropColumn(
                name: "SendFinalSmsToUser",
                table: "AspNetRoles");

            migrationBuilder.DropColumn(
                name: "SendSmsToParent",
                table: "AspNetRoles");
        }
    }
}
