﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e32 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FR_padash",
                table: "EmployeeSalary");

            migrationBuilder.AlterColumn<int>(
                name: "FR_padash__",
                table: "EmployeeSalary",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "FR_padash__",
                table: "EmployeeSalary",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "FR_padash",
                table: "EmployeeSalary",
                nullable: false,
                defaultValue: 0);
        }
    }
}
