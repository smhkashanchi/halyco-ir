﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class newMission : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Mission_tb_Employee_tb_PersonalCode",
                table: "Mission_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_Mission_tb_Employee_tb_PersonalCode_HRmanager",
                table: "Mission_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_Mission_tb_Employee_tb_PersonalCode_manager",
                table: "Mission_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_Mission_tb_Employee_tb_PersonalCode_superior",
                table: "Mission_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_Vacation_tb_AspNetUsers_PersonalCodeFirst",
                table: "Vacation_tb");

            migrationBuilder.DropIndex(
                name: "IX_Mission_tb_PersonalCode_HRmanager",
                table: "Mission_tb");

            migrationBuilder.DropIndex(
                name: "IX_Mission_tb_PersonalCode_manager",
                table: "Mission_tb");

            migrationBuilder.DropIndex(
                name: "IX_Mission_tb_PersonalCode_superior",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "HRManagerDateTime",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "HRManagerReason",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "ManagerDateTime",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "ManagerReason",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "PersonalCode_HRmanager",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "PersonalCode_manager",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "PersonalCode_superior",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "StatusHRManagerSignature",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "StatusManagerSignature",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "SuperiorDateTime",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "SuperiorReason",
                table: "Mission_tb");

            migrationBuilder.RenameColumn(
                name: "StatusSuperiorSignature",
                table: "Mission_tb",
                newName: "FinalStatus");

            migrationBuilder.RenameColumn(
                name: "PersonalCode",
                table: "Mission_tb",
                newName: "PersonalCodeFirst");

            migrationBuilder.RenameIndex(
                name: "IX_Mission_tb_PersonalCode",
                table: "Mission_tb",
                newName: "IX_Mission_tb_PersonalCodeFirst");

            migrationBuilder.AlterColumn<string>(
                name: "PersonalCodeFirst",
                table: "Vacation_tb",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.CreateTable(
                name: "MissionSignature_tb",
                columns: table => new
                {
                    MissionSignature_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Mission_ID = table.Column<int>(nullable: false),
                    PersonalCodeSignaturer = table.Column<string>(maxLength: 450, nullable: false),
                    SignatureStatus = table.Column<bool>(nullable: true),
                    SignatureReason = table.Column<string>(maxLength: 1000, nullable: true),
                    SignatureDateTime = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MissionSignature_tb", x => x.MissionSignature_ID);
                    table.ForeignKey(
                        name: "FK_MissionSignature_tb_Mission_tb_Mission_ID",
                        column: x => x.Mission_ID,
                        principalTable: "Mission_tb",
                        principalColumn: "Mission_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MissionSignature_tb_AspNetUsers_PersonalCodeSignaturer",
                        column: x => x.PersonalCodeSignaturer,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MissionSignature_tb_Mission_ID",
                table: "MissionSignature_tb",
                column: "Mission_ID");

            migrationBuilder.CreateIndex(
                name: "IX_MissionSignature_tb_PersonalCodeSignaturer",
                table: "MissionSignature_tb",
                column: "PersonalCodeSignaturer");

            migrationBuilder.AddForeignKey(
                name: "FK_Mission_tb_Employee_tb_PersonalCodeFirst",
                table: "Mission_tb",
                column: "PersonalCodeFirst",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCodeFirst",
                table: "Vacation_tb",
                column: "PersonalCodeFirst",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Mission_tb_Employee_tb_PersonalCodeFirst",
                table: "Mission_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCodeFirst",
                table: "Vacation_tb");

            migrationBuilder.DropTable(
                name: "MissionSignature_tb");

            migrationBuilder.RenameColumn(
                name: "PersonalCodeFirst",
                table: "Mission_tb",
                newName: "PersonalCode");

            migrationBuilder.RenameColumn(
                name: "FinalStatus",
                table: "Mission_tb",
                newName: "StatusSuperiorSignature");

            migrationBuilder.RenameIndex(
                name: "IX_Mission_tb_PersonalCodeFirst",
                table: "Mission_tb",
                newName: "IX_Mission_tb_PersonalCode");

            migrationBuilder.AlterColumn<string>(
                name: "PersonalCodeFirst",
                table: "Vacation_tb",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<DateTime>(
                name: "HRManagerDateTime",
                table: "Mission_tb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HRManagerReason",
                table: "Mission_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ManagerDateTime",
                table: "Mission_tb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ManagerReason",
                table: "Mission_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PersonalCode_HRmanager",
                table: "Mission_tb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PersonalCode_manager",
                table: "Mission_tb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PersonalCode_superior",
                table: "Mission_tb",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "StatusHRManagerSignature",
                table: "Mission_tb",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "StatusManagerSignature",
                table: "Mission_tb",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "SuperiorDateTime",
                table: "Mission_tb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SuperiorReason",
                table: "Mission_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Mission_tb_PersonalCode_HRmanager",
                table: "Mission_tb",
                column: "PersonalCode_HRmanager");

            migrationBuilder.CreateIndex(
                name: "IX_Mission_tb_PersonalCode_manager",
                table: "Mission_tb",
                column: "PersonalCode_manager");

            migrationBuilder.CreateIndex(
                name: "IX_Mission_tb_PersonalCode_superior",
                table: "Mission_tb",
                column: "PersonalCode_superior");

            migrationBuilder.AddForeignKey(
                name: "FK_Mission_tb_Employee_tb_PersonalCode",
                table: "Mission_tb",
                column: "PersonalCode",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Mission_tb_Employee_tb_PersonalCode_HRmanager",
                table: "Mission_tb",
                column: "PersonalCode_HRmanager",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Mission_tb_Employee_tb_PersonalCode_manager",
                table: "Mission_tb",
                column: "PersonalCode_manager",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Mission_tb_Employee_tb_PersonalCode_superior",
                table: "Mission_tb",
                column: "PersonalCode_superior",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Vacation_tb_AspNetUsers_PersonalCodeFirst",
                table: "Vacation_tb",
                column: "PersonalCodeFirst",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
