﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "FR_MandehMorakhasi",
                table: "EmployeeSalary",
                nullable: true,
                oldClrType: typeof(float));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<float>(
                name: "FR_MandehMorakhasi",
                table: "EmployeeSalary",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
