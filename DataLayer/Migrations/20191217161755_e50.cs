﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e50 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "HRManagerDateTime",
                table: "ExtraWork_tb",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "ManagerDateTime",
                table: "ExtraWork_tb",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "SuperiorDateTime",
                table: "ExtraWork_tb",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HRManagerDateTime",
                table: "ExtraWork_tb");

            migrationBuilder.DropColumn(
                name: "ManagerDateTime",
                table: "ExtraWork_tb");

            migrationBuilder.DropColumn(
                name: "SuperiorDateTime",
                table: "ExtraWork_tb");
        }
    }
}
