﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e43 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vacation_tb_Employee_tb_AdministrativeAffairsSignature",
                table: "Vacation_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_Vacation_tb_Employee_tb_AlternateSignature",
                table: "Vacation_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCode_3",
                table: "Vacation_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_Vacation_tb_Employee_tb_UnitsSupervisorSignature",
                table: "Vacation_tb");

            migrationBuilder.DropIndex(
                name: "IX_Vacation_tb_AdministrativeAffairsSignature",
                table: "Vacation_tb");

            migrationBuilder.DropIndex(
                name: "IX_Vacation_tb_AlternateSignature",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "AdministrativeAffairsSignature",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "StatusAdministrativeAffairsSignature",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "StatusUnitsSupervisorSignature",
                table: "Vacation_tb");

            migrationBuilder.RenameColumn(
                name: "VecationType",
                table: "Vacation_tb",
                newName: "VacationType");

            migrationBuilder.RenameColumn(
                name: "UnitsSupervisorSignature",
                table: "Vacation_tb",
                newName: "PersonalCode_superior");

            migrationBuilder.RenameColumn(
                name: "UnitsSupervisorReason",
                table: "Vacation_tb",
                newName: "SuperiorReason");

            migrationBuilder.RenameColumn(
                name: "Section",
                table: "Vacation_tb",
                newName: "VacationReason");

            migrationBuilder.RenameColumn(
                name: "PersonalCode_3",
                table: "Vacation_tb",
                newName: "PersonalCode");

            migrationBuilder.RenameColumn(
                name: "DayVecationStartDate",
                table: "Vacation_tb",
                newName: "PersonalCode_manager");

            migrationBuilder.RenameColumn(
                name: "DayVecationEndDate",
                table: "Vacation_tb",
                newName: "PersonalCodeSecond");

            migrationBuilder.RenameColumn(
                name: "ApplicantSignature",
                table: "Vacation_tb",
                newName: "DayVacationStartDate");

            migrationBuilder.RenameColumn(
                name: "AlternateSignature",
                table: "Vacation_tb",
                newName: "DayVacationEndDate");

            migrationBuilder.RenameColumn(
                name: "AdministrativeAffairsReason",
                table: "Vacation_tb",
                newName: "ManagerReason");

            migrationBuilder.RenameColumn(
                name: "Vecation_ID",
                table: "Vacation_tb",
                newName: "Vacation_ID");

            migrationBuilder.RenameIndex(
                name: "IX_Vacation_tb_UnitsSupervisorSignature",
                table: "Vacation_tb",
                newName: "IX_Vacation_tb_PersonalCode_superior");

            migrationBuilder.RenameIndex(
                name: "IX_Vacation_tb_PersonalCode_3",
                table: "Vacation_tb",
                newName: "IX_Vacation_tb_PersonalCode");

            migrationBuilder.AlterColumn<bool>(
                name: "StatusAlternateSignature",
                table: "Vacation_tb",
                nullable: true,
                oldClrType: typeof(byte));

            migrationBuilder.AlterColumn<string>(
                name: "PersonalCode_manager",
                table: "Vacation_tb",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PersonalCodeSecond",
                table: "Vacation_tb",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DayVacationEndDate",
                table: "Vacation_tb",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "StatusManagerSignature",
                table: "Vacation_tb",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "StatusSuperiorSignature",
                table: "Vacation_tb",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Vacation_tb_PersonalCodeSecond",
                table: "Vacation_tb",
                column: "PersonalCodeSecond");

            migrationBuilder.CreateIndex(
                name: "IX_Vacation_tb_PersonalCode_manager",
                table: "Vacation_tb",
                column: "PersonalCode_manager");

            migrationBuilder.AddForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCode",
                table: "Vacation_tb",
                column: "PersonalCode",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCodeSecond",
                table: "Vacation_tb",
                column: "PersonalCodeSecond",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCode_manager",
                table: "Vacation_tb",
                column: "PersonalCode_manager",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCode_superior",
                table: "Vacation_tb",
                column: "PersonalCode_superior",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCode",
                table: "Vacation_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCodeSecond",
                table: "Vacation_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCode_manager",
                table: "Vacation_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCode_superior",
                table: "Vacation_tb");

            migrationBuilder.DropIndex(
                name: "IX_Vacation_tb_PersonalCodeSecond",
                table: "Vacation_tb");

            migrationBuilder.DropIndex(
                name: "IX_Vacation_tb_PersonalCode_manager",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "StatusManagerSignature",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "StatusSuperiorSignature",
                table: "Vacation_tb");

            migrationBuilder.RenameColumn(
                name: "VacationType",
                table: "Vacation_tb",
                newName: "VecationType");

            migrationBuilder.RenameColumn(
                name: "VacationReason",
                table: "Vacation_tb",
                newName: "Section");

            migrationBuilder.RenameColumn(
                name: "SuperiorReason",
                table: "Vacation_tb",
                newName: "UnitsSupervisorReason");

            migrationBuilder.RenameColumn(
                name: "PersonalCode_superior",
                table: "Vacation_tb",
                newName: "UnitsSupervisorSignature");

            migrationBuilder.RenameColumn(
                name: "PersonalCode_manager",
                table: "Vacation_tb",
                newName: "DayVecationStartDate");

            migrationBuilder.RenameColumn(
                name: "PersonalCodeSecond",
                table: "Vacation_tb",
                newName: "DayVecationEndDate");

            migrationBuilder.RenameColumn(
                name: "PersonalCode",
                table: "Vacation_tb",
                newName: "PersonalCode_3");

            migrationBuilder.RenameColumn(
                name: "ManagerReason",
                table: "Vacation_tb",
                newName: "AdministrativeAffairsReason");

            migrationBuilder.RenameColumn(
                name: "DayVacationStartDate",
                table: "Vacation_tb",
                newName: "ApplicantSignature");

            migrationBuilder.RenameColumn(
                name: "DayVacationEndDate",
                table: "Vacation_tb",
                newName: "AlternateSignature");

            migrationBuilder.RenameColumn(
                name: "Vacation_ID",
                table: "Vacation_tb",
                newName: "Vecation_ID");

            migrationBuilder.RenameIndex(
                name: "IX_Vacation_tb_PersonalCode_superior",
                table: "Vacation_tb",
                newName: "IX_Vacation_tb_UnitsSupervisorSignature");

            migrationBuilder.RenameIndex(
                name: "IX_Vacation_tb_PersonalCode",
                table: "Vacation_tb",
                newName: "IX_Vacation_tb_PersonalCode_3");

            migrationBuilder.AlterColumn<byte>(
                name: "StatusAlternateSignature",
                table: "Vacation_tb",
                nullable: false,
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DayVecationStartDate",
                table: "Vacation_tb",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DayVecationEndDate",
                table: "Vacation_tb",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AlternateSignature",
                table: "Vacation_tb",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AdministrativeAffairsSignature",
                table: "Vacation_tb",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "StatusAdministrativeAffairsSignature",
                table: "Vacation_tb",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "StatusUnitsSupervisorSignature",
                table: "Vacation_tb",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_Vacation_tb_AdministrativeAffairsSignature",
                table: "Vacation_tb",
                column: "AdministrativeAffairsSignature");

            migrationBuilder.CreateIndex(
                name: "IX_Vacation_tb_AlternateSignature",
                table: "Vacation_tb",
                column: "AlternateSignature");

            migrationBuilder.AddForeignKey(
                name: "FK_Vacation_tb_Employee_tb_AdministrativeAffairsSignature",
                table: "Vacation_tb",
                column: "AdministrativeAffairsSignature",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Vacation_tb_Employee_tb_AlternateSignature",
                table: "Vacation_tb",
                column: "AlternateSignature",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCode_3",
                table: "Vacation_tb",
                column: "PersonalCode_3",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Vacation_tb_Employee_tb_UnitsSupervisorSignature",
                table: "Vacation_tb",
                column: "UnitsSupervisorSignature",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
