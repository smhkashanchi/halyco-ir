﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e21 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ExtraWork_tb",
                columns: table => new
                {
                    ExtraWorkID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PersonalCode = table.Column<string>(nullable: false),
                    StartDate = table.Column<string>(nullable: false),
                    EndDate = table.Column<string>(nullable: false),
                    StartTime = table.Column<string>(maxLength: 10, nullable: false),
                    EndTime = table.Column<string>(maxLength: 10, nullable: false),
                    ExtraWorktime = table.Column<string>(maxLength: 500, nullable: true),
                    PersonalCode_superior = table.Column<string>(nullable: true),
                    SuperiorReason = table.Column<string>(maxLength: 500, nullable: true),
                    StatusSuperiorSignature = table.Column<bool>(nullable: true),
                    PersonalCode_manager = table.Column<string>(nullable: true),
                    ManagerReason = table.Column<string>(maxLength: 500, nullable: true),
                    StatusManagerSignature = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExtraWork_tb", x => x.ExtraWorkID);
                    table.ForeignKey(
                        name: "FK_ExtraWork_tb_Employee_tb_PersonalCode",
                        column: x => x.PersonalCode,
                        principalTable: "Employee_tb",
                        principalColumn: "PersonalCode",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExtraWork_tb_Employee_tb_PersonalCode_manager",
                        column: x => x.PersonalCode_manager,
                        principalTable: "Employee_tb",
                        principalColumn: "PersonalCode",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExtraWork_tb_Employee_tb_PersonalCode_superior",
                        column: x => x.PersonalCode_superior,
                        principalTable: "Employee_tb",
                        principalColumn: "PersonalCode",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExtraWork_tb_PersonalCode",
                table: "ExtraWork_tb",
                column: "PersonalCode");

            migrationBuilder.CreateIndex(
                name: "IX_ExtraWork_tb_PersonalCode_manager",
                table: "ExtraWork_tb",
                column: "PersonalCode_manager");

            migrationBuilder.CreateIndex(
                name: "IX_ExtraWork_tb_PersonalCode_superior",
                table: "ExtraWork_tb",
                column: "PersonalCode_superior");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExtraWork_tb");
        }
    }
}
