﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class Editvacation1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "AlternateDateTime",
                table: "Vacation_tb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AlternateSignatureReason",
                table: "Vacation_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PersonalCodeSecond",
                table: "Vacation_tb",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "StatusAlternateSignature",
                table: "Vacation_tb",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Vacation_tb_PersonalCodeSecond",
                table: "Vacation_tb",
                column: "PersonalCodeSecond");

            migrationBuilder.AddForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCodeSecond",
                table: "Vacation_tb",
                column: "PersonalCodeSecond",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vacation_tb_Employee_tb_PersonalCodeSecond",
                table: "Vacation_tb");

            migrationBuilder.DropIndex(
                name: "IX_Vacation_tb_PersonalCodeSecond",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "AlternateDateTime",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "AlternateSignatureReason",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "PersonalCodeSecond",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "StatusAlternateSignature",
                table: "Vacation_tb");
        }
    }
}
