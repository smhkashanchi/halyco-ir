﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FR_MandehMorakhasi",
                table: "EmployeeSalary");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<float>(
                name: "FR_MandehMorakhasi",
                table: "EmployeeSalary",
                nullable: false,
                defaultValue: 0f);
        }
    }
}
