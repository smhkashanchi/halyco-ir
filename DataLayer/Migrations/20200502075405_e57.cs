﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e57 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FR_mamoriat_vizhe__",
                table: "EmployeeSalary",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "FR_tafavot_tatbigh__",
                table: "EmployeeSalary",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FR_mamoriat_vizhe__",
                table: "EmployeeSalary");

            migrationBuilder.DropColumn(
                name: "FR_tafavot_tatbigh__",
                table: "EmployeeSalary");
        }
    }
}
