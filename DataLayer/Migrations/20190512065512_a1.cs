﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class a1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ActivityField_tb",
                columns: table => new
                {
                    ActivityFieldID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivityFieldTitle = table.Column<string>(maxLength: 50, nullable: false),
                    ActivityFieldDescription = table.Column<string>(maxLength: 500, nullable: false),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivityField_tb", x => x.ActivityFieldID);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Task = table.Column<string>(maxLength: 100, nullable: true),
                    Responsibilty = table.Column<string>(maxLength: 100, nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Role_Status = table.Column<byte>(nullable: false),
                    Role_Type = table.Column<byte>(nullable: false),
                    Access = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FullName = table.Column<string>(maxLength: 200, nullable: false),
                    UserPhoto = table.Column<string>(nullable: true),
                    Mobile_1 = table.Column<string>(maxLength: 12, nullable: true),
                    Mobile_2 = table.Column<string>(maxLength: 12, nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    RegisterDate = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Catalog_tb",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(maxLength: 100, nullable: false),
                    Files = table.Column<string>(maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Catalog_tb", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "CoWorker_tb",
                columns: table => new
                {
                    CoWorker_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(maxLength: 100, nullable: true),
                    Link = table.Column<string>(maxLength: 100, nullable: true),
                    Photo = table.Column<string>(maxLength: 100, nullable: false),
                    Type = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoWorker_tb", x => x.CoWorker_ID);
                });

            migrationBuilder.CreateTable(
                name: "Emp_ActivityItems_tb",
                columns: table => new
                {
                    Item_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Emp_ActivityItems_tb", x => x.Item_ID);
                });

            migrationBuilder.CreateTable(
                name: "Emp_BaseInformation_tb",
                columns: table => new
                {
                    Employment_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(maxLength: 50, nullable: false),
                    LastName = table.Column<string>(maxLength: 150, nullable: false),
                    FatherName = table.Column<string>(maxLength: 50, nullable: false),
                    Id_Cer = table.Column<string>(maxLength: 20, nullable: false),
                    BirthDate = table.Column<string>(maxLength: 15, nullable: false),
                    NationalCode = table.Column<string>(maxLength: 10, nullable: false),
                    TarikhSodorShenasname = table.Column<string>(maxLength: 15, nullable: false),
                    MahaleSodorShenasname = table.Column<string>(maxLength: 50, nullable: false),
                    BirthPlace = table.Column<string>(maxLength: 100, nullable: false),
                    Religion = table.Column<string>(maxLength: 30, nullable: false),
                    MaritalStatus = table.Column<string>(maxLength: 10, nullable: false),
                    MaritalDescription = table.Column<string>(maxLength: 100, nullable: false),
                    ChildrenDescription = table.Column<string>(maxLength: 100, nullable: false),
                    IsHealth = table.Column<bool>(nullable: false),
                    HealthDescription = table.Column<string>(maxLength: 100, nullable: true),
                    Duty = table.Column<bool>(nullable: false),
                    ExemptReason = table.Column<string>(maxLength: 100, nullable: true),
                    Image = table.Column<string>(maxLength: 100, nullable: true),
                    IsCriminalRecord = table.Column<bool>(nullable: false),
                    CriminalRecordReason = table.Column<string>(maxLength: 200, nullable: false),
                    IsSmoking = table.Column<bool>(nullable: false),
                    SmokingReason = table.Column<string>(maxLength: 200, nullable: false),
                    EnglishRead = table.Column<byte>(nullable: false),
                    EnglishWrite = table.Column<byte>(nullable: false),
                    EnglishSpeake = table.Column<byte>(nullable: false),
                    SelectedJob = table.Column<string>(maxLength: 300, nullable: true),
                    TypeCooperation = table.Column<byte>(nullable: false),
                    TimeCooperation = table.Column<string>(maxLength: 200, nullable: true),
                    IsOvertime = table.Column<bool>(nullable: false),
                    OvertimeHours = table.Column<short>(nullable: false),
                    HolidayIsWork = table.Column<bool>(nullable: false),
                    IsMissionInternal = table.Column<bool>(nullable: false),
                    IsMissionExternal = table.Column<bool>(nullable: false),
                    IsInsuranceHistory = table.Column<bool>(nullable: false),
                    InsuranceDescription = table.Column<string>(maxLength: 100, nullable: true),
                    MethodIntroduction = table.Column<string>(maxLength: 300, nullable: true),
                    IsSpecialPhysicalItems = table.Column<bool>(nullable: false),
                    SpecialPhysicalItemsDescription = table.Column<string>(maxLength: 500, nullable: true),
                    IsWorking = table.Column<bool>(nullable: false),
                    WorkGuarantee = table.Column<string>(maxLength: 100, nullable: true),
                    ExpectedSalary = table.Column<string>(maxLength: 50, nullable: true),
                    SalaryType = table.Column<byte>(nullable: false),
                    IsHeadFamily = table.Column<bool>(nullable: false),
                    HomeType = table.Column<byte>(nullable: false),
                    HomeDescription = table.Column<string>(maxLength: 200, nullable: false),
                    Address = table.Column<string>(maxLength: 400, nullable: false),
                    Tell = table.Column<string>(maxLength: 15, nullable: false),
                    Mobile = table.Column<string>(maxLength: 15, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Emp_BaseInformation_tb", x => x.Employment_ID);
                });

            migrationBuilder.CreateTable(
                name: "Emp_ComputerSkillItems_tb",
                columns: table => new
                {
                    Item_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Emp_ComputerSkillItems_tb", x => x.Item_ID);
                });

            migrationBuilder.CreateTable(
                name: "HalycoManager",
                columns: table => new
                {
                    HManager_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ManagerName = table.Column<string>(maxLength: 100, nullable: false),
                    ManagerPost = table.Column<string>(maxLength: 100, nullable: false),
                    ManagerEmail = table.Column<string>(maxLength: 100, nullable: true),
                    ManagerPhoto = table.Column<string>(maxLength: 200, nullable: false),
                    ManagerType = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HalycoManager", x => x.HManager_ID);
                });

            migrationBuilder.CreateTable(
                name: "Language_tb",
                columns: table => new
                {
                    Lang_ID = table.Column<string>(type: "varchar(8)", nullable: false),
                    Lang_Name = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Language_tb", x => x.Lang_ID);
                });

            migrationBuilder.CreateTable(
                name: "MainProposal_tb",
                columns: table => new
                {
                    Proposal_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(maxLength: 200, nullable: false),
                    ProposalDate = table.Column<string>(maxLength: 15, nullable: true),
                    ProposalDescription = table.Column<string>(maxLength: 900, nullable: false),
                    CurrentStatus = table.Column<string>(maxLength: 100, nullable: true),
                    ImplementMethod = table.Column<string>(maxLength: 500, nullable: true),
                    Possibilities = table.Column<string>(maxLength: 200, nullable: true),
                    FinancialSavings = table.Column<string>(maxLength: 10, nullable: true),
                    ImplementPrice = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MainProposal_tb", x => x.Proposal_ID);
                });

            migrationBuilder.CreateTable(
                name: "MainSuppliers_tb",
                columns: table => new
                {
                    Suppliers_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SuppliersType = table.Column<bool>(nullable: false),
                    FullName = table.Column<string>(maxLength: 100, nullable: false),
                    Brand = table.Column<string>(maxLength: 50, nullable: true),
                    CentralOfficeTell = table.Column<string>(maxLength: 15, nullable: true),
                    SalesTell = table.Column<string>(maxLength: 15, nullable: true),
                    FactoyTell = table.Column<string>(maxLength: 15, nullable: true),
                    CentralOfficFax = table.Column<string>(maxLength: 20, nullable: true),
                    SalesFax = table.Column<string>(maxLength: 20, nullable: true),
                    FactoryFax = table.Column<string>(maxLength: 20, nullable: true),
                    CentralOfficePostalCode = table.Column<string>(maxLength: 15, nullable: true),
                    SalesPostalCode = table.Column<string>(maxLength: 15, nullable: true),
                    FactoyPostalCode = table.Column<string>(maxLength: 15, nullable: true),
                    PhoneNumber = table.Column<string>(maxLength: 15, nullable: true),
                    Address = table.Column<string>(maxLength: 200, nullable: true),
                    Email = table.Column<string>(maxLength: 50, nullable: true),
                    Website = table.Column<string>(maxLength: 30, nullable: true),
                    Affordability = table.Column<string>(maxLength: 50, nullable: true),
                    PersonnelNumber = table.Column<short>(nullable: false),
                    TecknecianNumber = table.Column<short>(nullable: false),
                    ExpertsNumber = table.Column<short>(nullable: false),
                    QualityControlNumber = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MainSuppliers_tb", x => x.Suppliers_ID);
                });

            migrationBuilder.CreateTable(
                name: "MainSurvey_tb",
                columns: table => new
                {
                    MainSurvey_ID = table.Column<byte>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SurveyType = table.Column<byte>(nullable: false),
                    Title = table.Column<string>(maxLength: 100, nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MainSurvey_tb", x => x.MainSurvey_ID);
                });

            migrationBuilder.CreateTable(
                name: "OfficePost_tb",
                columns: table => new
                {
                    OfficePostID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    OfficePostTitle = table.Column<string>(maxLength: 50, nullable: false),
                    OfficePostRoles = table.Column<string>(maxLength: 500, nullable: false),
                    OfficePostResponsibility = table.Column<string>(maxLength: 500, nullable: false),
                    ParentId = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OfficePost_tb", x => x.OfficePostID);
                    table.ForeignKey(
                        name: "FK_OfficePost_tb_OfficePost_tb_ParentId",
                        column: x => x.ParentId,
                        principalTable: "OfficePost_tb",
                        principalColumn: "OfficePostID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OfficeUnit_tb",
                columns: table => new
                {
                    OfficeUnitID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    OfficeUnitTitle = table.Column<string>(maxLength: 50, nullable: false),
                    OfficeUnitTasks = table.Column<string>(maxLength: 500, nullable: true),
                    OfficeUnitResponsibility = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OfficeUnit_tb", x => x.OfficeUnitID);
                });

            migrationBuilder.CreateTable(
                name: "SeoPage_tb",
                columns: table => new
                {
                    SeoPage_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PageId = table.Column<int>(nullable: false),
                    UrlPage = table.Column<string>(maxLength: 200, nullable: false),
                    MetaTitle = table.Column<string>(maxLength: 200, nullable: false),
                    MetaKeyword = table.Column<string>(maxLength: 300, nullable: true),
                    MetaDescription = table.Column<string>(maxLength: 500, nullable: true),
                    MetaOther = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SeoPage_tb", x => x.SeoPage_ID);
                });

            migrationBuilder.CreateTable(
                name: "SeoPageProduct_tb",
                columns: table => new
                {
                    SeoPage_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PageId = table.Column<int>(nullable: false),
                    UrlPage = table.Column<string>(maxLength: 300, nullable: true),
                    MetaTitle = table.Column<string>(maxLength: 200, nullable: true),
                    MetaKeyword = table.Column<string>(maxLength: 300, nullable: true),
                    MetaDescription = table.Column<string>(maxLength: 500, nullable: true),
                    MetaOther = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SeoPageProduct_tb", x => x.SeoPage_ID);
                });

            migrationBuilder.CreateTable(
                name: "SeoStaticPage_tb",
                columns: table => new
                {
                    SeoPage_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PageId = table.Column<short>(nullable: false),
                    MetaTitle = table.Column<string>(maxLength: 200, nullable: true),
                    MetaKeyword = table.Column<string>(maxLength: 300, nullable: true),
                    MetaDescription = table.Column<string>(maxLength: 500, nullable: true),
                    MetaOther = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SeoStaticPage_tb", x => x.SeoPage_ID);
                });

            migrationBuilder.CreateTable(
                name: "SMS_tb",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MobileNumber = table.Column<string>(maxLength: 15, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SMS_tb", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "SocialNetworks_tb",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(maxLength: 50, nullable: true),
                    Link = table.Column<string>(maxLength: 200, nullable: false),
                    Type = table.Column<byte>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SocialNetworks_tb", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "SuppliersFileType_tb",
                columns: table => new
                {
                    FileType_ID = table.Column<byte>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuppliersFileType_tb", x => x.FileType_ID);
                });

            migrationBuilder.CreateTable(
                name: "SurveyAnswerType_tb",
                columns: table => new
                {
                    AnswerType_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(maxLength: 300, nullable: false),
                    OptionTitle1 = table.Column<string>(maxLength: 50, nullable: false),
                    OptionTitle2 = table.Column<string>(maxLength: 50, nullable: false),
                    OptionTitle3 = table.Column<string>(maxLength: 50, nullable: false),
                    OptionTitle4 = table.Column<string>(maxLength: 50, nullable: false),
                    OptionTitle5 = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SurveyAnswerType_tb", x => x.AnswerType_ID);
                });

            migrationBuilder.CreateTable(
                name: "Activity_tb",
                columns: table => new
                {
                    Activity_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivityTitle = table.Column<string>(maxLength: 50, nullable: false),
                    ActivityDescription = table.Column<string>(maxLength: 500, nullable: false),
                    ActivityFieldID = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activity_tb", x => x.Activity_ID);
                    table.ForeignKey(
                        name: "FK_Activity_tb_ActivityField_tb_ActivityFieldID",
                        column: x => x.ActivityFieldID,
                        principalTable: "ActivityField_tb",
                        principalColumn: "ActivityFieldID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Customer_tb",
                columns: table => new
                {
                    Customer_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserIdentity_Id = table.Column<string>(maxLength: 450, nullable: true),
                    BirthDate = table.Column<DateTime>(nullable: true),
                    NationalCode = table.Column<string>(maxLength: 20, nullable: true),
                    Jender = table.Column<bool>(nullable: false),
                    IsNewsLetter = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    ActiveCode = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer_tb", x => x.Customer_ID);
                    table.ForeignKey(
                        name: "FK_Customer_tb_AspNetUsers_UserIdentity_Id",
                        column: x => x.UserIdentity_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Emp_Activity_tb",
                columns: table => new
                {
                    Employement_Id = table.Column<int>(nullable: false),
                    ItemId = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Emp_Activity_tb", x => new { x.Employement_Id, x.ItemId });
                    table.ForeignKey(
                        name: "FK_Emp_Activity_tb_Emp_BaseInformation_tb_Employement_Id",
                        column: x => x.Employement_Id,
                        principalTable: "Emp_BaseInformation_tb",
                        principalColumn: "Employment_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Emp_Activity_tb_Emp_ActivityItems_tb_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Emp_ActivityItems_tb",
                        principalColumn: "Item_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Emp_Documents_tb",
                columns: table => new
                {
                    Doc_ID = table.Column<int>(nullable: false),
                    Employement_Id = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: true),
                    FileName = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Emp_Documents_tb", x => x.Doc_ID);
                    table.ForeignKey(
                        name: "FK_Emp_Documents_tb_Emp_BaseInformation_tb_Employement_Id",
                        column: x => x.Employement_Id,
                        principalTable: "Emp_BaseInformation_tb",
                        principalColumn: "Employment_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Emp_EducationalRecords_tb",
                columns: table => new
                {
                    Edu_ID = table.Column<int>(nullable: false),
                    Employment_Id = table.Column<int>(nullable: false),
                    Evidence = table.Column<string>(maxLength: 30, nullable: false),
                    Field = table.Column<string>(maxLength: 60, nullable: false),
                    Average = table.Column<string>(maxLength: 6, nullable: false),
                    EndDate = table.Column<string>(maxLength: 15, nullable: true),
                    UniversityType = table.Column<string>(maxLength: 30, nullable: false),
                    InstitutionName = table.Column<string>(maxLength: 60, nullable: false),
                    City_Country = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Emp_EducationalRecords_tb", x => x.Edu_ID);
                    table.ForeignKey(
                        name: "FK_Emp_EducationalRecords_tb_Emp_BaseInformation_tb_Employment_Id",
                        column: x => x.Employment_Id,
                        principalTable: "Emp_BaseInformation_tb",
                        principalColumn: "Employment_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Emp_Guarantor_tb",
                columns: table => new
                {
                    Guarantor_ID = table.Column<int>(nullable: false),
                    Employement_Id = table.Column<int>(nullable: false),
                    FullName = table.Column<string>(maxLength: 150, nullable: false),
                    Relation = table.Column<string>(maxLength: 80, nullable: false),
                    Job = table.Column<string>(maxLength: 100, nullable: false),
                    Mobile = table.Column<string>(maxLength: 15, nullable: false),
                    WorkPlace = table.Column<string>(maxLength: 400, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Emp_Guarantor_tb", x => x.Guarantor_ID);
                    table.ForeignKey(
                        name: "FK_Emp_Guarantor_tb_Emp_BaseInformation_tb_Employement_Id",
                        column: x => x.Employement_Id,
                        principalTable: "Emp_BaseInformation_tb",
                        principalColumn: "Employment_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Emp_TraningCourse_tb",
                columns: table => new
                {
                    TraningCourse_ID = table.Column<int>(nullable: false),
                    Employement_Id = table.Column<int>(nullable: false),
                    TraningName = table.Column<string>(maxLength: 100, nullable: false),
                    OrganizationName = table.Column<string>(maxLength: 100, nullable: true),
                    TraningPeroid = table.Column<string>(maxLength: 30, nullable: false),
                    StartDate = table.Column<string>(maxLength: 15, nullable: true),
                    Description = table.Column<string>(maxLength: 300, nullable: true),
                    IsEvidence = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Emp_TraningCourse_tb", x => x.TraningCourse_ID);
                    table.ForeignKey(
                        name: "FK_Emp_TraningCourse_tb_Emp_BaseInformation_tb_Employement_Id",
                        column: x => x.Employement_Id,
                        principalTable: "Emp_BaseInformation_tb",
                        principalColumn: "Employment_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Emp_UnderSupervisor_tb",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false),
                    Employement_Id = table.Column<int>(nullable: false),
                    FullName = table.Column<string>(maxLength: 150, nullable: false),
                    Jender = table.Column<string>(maxLength: 10, nullable: false),
                    Relation = table.Column<string>(maxLength: 100, nullable: false),
                    BirthDate = table.Column<string>(maxLength: 15, nullable: false),
                    LevelEducation = table.Column<string>(maxLength: 30, nullable: false),
                    Job = table.Column<string>(maxLength: 150, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Emp_UnderSupervisor_tb", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Emp_UnderSupervisor_tb_Emp_BaseInformation_tb_Employement_Id",
                        column: x => x.Employement_Id,
                        principalTable: "Emp_BaseInformation_tb",
                        principalColumn: "Employment_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Emp_WorkExperience_tb",
                columns: table => new
                {
                    WorkExp_ID = table.Column<int>(nullable: false),
                    Employment_Id = table.Column<int>(nullable: false),
                    OrganizationName = table.Column<string>(maxLength: 100, nullable: false),
                    Post = table.Column<string>(maxLength: 100, nullable: false),
                    AssistPeriod = table.Column<string>(maxLength: 20, nullable: false),
                    AssistCutDate = table.Column<string>(maxLength: 15, nullable: true),
                    Tell = table.Column<string>(maxLength: 15, nullable: true),
                    AverageSalary = table.Column<string>(maxLength: 20, nullable: true),
                    AssistCutReason = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Emp_WorkExperience_tb", x => x.WorkExp_ID);
                    table.ForeignKey(
                        name: "FK_Emp_WorkExperience_tb_Emp_BaseInformation_tb_Employment_Id",
                        column: x => x.Employment_Id,
                        principalTable: "Emp_BaseInformation_tb",
                        principalColumn: "Employment_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Emp_ComputerSkills_tb",
                columns: table => new
                {
                    Employement_Id = table.Column<int>(nullable: false),
                    ItemId = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Emp_ComputerSkills_tb", x => new { x.Employement_Id, x.ItemId });
                    table.ForeignKey(
                        name: "FK_Emp_ComputerSkills_tb_Emp_BaseInformation_tb_Employement_Id",
                        column: x => x.Employement_Id,
                        principalTable: "Emp_BaseInformation_tb",
                        principalColumn: "Employment_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Emp_ComputerSkills_tb_Emp_ComputerSkillItems_tb_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Emp_ComputerSkillItems_tb",
                        principalColumn: "Item_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AVFGroup_tb",
                columns: table => new
                {
                    AVFGroup_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AVFGroupTitle = table.Column<string>(maxLength: 150, nullable: false),
                    Lang = table.Column<string>(type: "varchar(8)", nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    ParentId = table.Column<short>(nullable: true),
                    AVFGroupImage = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AVFGroup_tb", x => x.AVFGroup_ID);
                    table.ForeignKey(
                        name: "FK_AVFGroup_tb_Language_tb_Lang",
                        column: x => x.Lang,
                        principalTable: "Language_tb",
                        principalColumn: "Lang_ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AVFGroup_tb_AVFGroup_tb_ParentId",
                        column: x => x.ParentId,
                        principalTable: "AVFGroup_tb",
                        principalColumn: "AVFGroup_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Color_tb",
                columns: table => new
                {
                    Color_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ColorTitle = table.Column<string>(maxLength: 50, nullable: false),
                    ColorCode = table.Column<string>(maxLength: 50, nullable: true),
                    Lang = table.Column<string>(type: "varchar(8)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Color_tb", x => x.Color_ID);
                    table.ForeignKey(
                        name: "FK_Color_tb_Language_tb_Lang",
                        column: x => x.Lang,
                        principalTable: "Language_tb",
                        principalColumn: "Lang_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ConnectionGroup_tb",
                columns: table => new
                {
                    ConnectionGroup_ID = table.Column<byte>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ConnectionGroupTitle = table.Column<string>(maxLength: 50, nullable: false),
                    Lang = table.Column<string>(type: "varchar(8)", nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConnectionGroup_tb", x => x.ConnectionGroup_ID);
                    table.ForeignKey(
                        name: "FK_ConnectionGroup_tb_Language_tb_Lang",
                        column: x => x.Lang,
                        principalTable: "Language_tb",
                        principalColumn: "Lang_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContactUsMessageGroup_tb",
                columns: table => new
                {
                    CMG_ID = table.Column<byte>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CMGName = table.Column<string>(maxLength: 100, nullable: false),
                    Lang = table.Column<string>(type: "varchar(8)", nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactUsMessageGroup_tb", x => x.CMG_ID);
                    table.ForeignKey(
                        name: "FK_ContactUsMessageGroup_tb_Language_tb_Lang",
                        column: x => x.Lang,
                        principalTable: "Language_tb",
                        principalColumn: "Lang_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContentsGroup_tb",
                columns: table => new
                {
                    ContentGroup_ID = table.Column<short>(nullable: false),
                    ContentGroupName = table.Column<string>(maxLength: 100, nullable: false),
                    Lang = table.Column<string>(type: "varchar(8)", nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    ParentID = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentsGroup_tb", x => x.ContentGroup_ID);
                    table.ForeignKey(
                        name: "FK_ContentsGroup_tb_Language_tb_Lang",
                        column: x => x.Lang,
                        principalTable: "Language_tb",
                        principalColumn: "Lang_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Country_tb",
                columns: table => new
                {
                    Country_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CountryName = table.Column<string>(maxLength: 50, nullable: false),
                    CountryCode = table.Column<string>(type: "varchar(10)", nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    Lang = table.Column<string>(type: "varchar(8)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Country_tb", x => x.Country_ID);
                    table.ForeignKey(
                        name: "FK_Country_tb_Language_tb_Lang",
                        column: x => x.Lang,
                        principalTable: "Language_tb",
                        principalColumn: "Lang_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FAQ_tb",
                columns: table => new
                {
                    FAQ_ID = table.Column<byte>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FAQuestion = table.Column<string>(maxLength: 500, nullable: false),
                    FAQAnswer = table.Column<string>(nullable: false),
                    Lnag = table.Column<string>(type: "varchar(8)", nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    TestField = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FAQ_tb", x => x.FAQ_ID);
                    table.ForeignKey(
                        name: "FK_FAQ_tb_Language_tb_Lnag",
                        column: x => x.Lnag,
                        principalTable: "Language_tb",
                        principalColumn: "Lang_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GalleryGroup_tb",
                columns: table => new
                {
                    GalleryGroup_ID = table.Column<byte>(nullable: false),
                    GalleryGroupTitle = table.Column<string>(maxLength: 150, nullable: false),
                    Lang = table.Column<string>(type: "varchar(8)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GalleryGroup_tb", x => x.GalleryGroup_ID);
                    table.ForeignKey(
                        name: "FK_GalleryGroup_tb_Language_tb_Lang",
                        column: x => x.Lang,
                        principalTable: "Language_tb",
                        principalColumn: "Lang_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Garanty_tb",
                columns: table => new
                {
                    Garanty_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    GarantyName = table.Column<string>(maxLength: 150, nullable: false),
                    Icon = table.Column<string>(maxLength: 100, nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Lang = table.Column<string>(type: "varchar(8)", nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Garanty_tb", x => x.Garanty_ID);
                    table.ForeignKey(
                        name: "FK_Garanty_tb_Language_tb_Lang",
                        column: x => x.Lang,
                        principalTable: "Language_tb",
                        principalColumn: "Lang_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "NewsLetterGroup_tb",
                columns: table => new
                {
                    Group_ID = table.Column<byte>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    Lang = table.Column<string>(type: "varchar(8)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewsLetterGroup_tb", x => x.Group_ID);
                    table.ForeignKey(
                        name: "FK_NewsLetterGroup_tb_Language_tb_Lang",
                        column: x => x.Lang,
                        principalTable: "Language_tb",
                        principalColumn: "Lang_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Off_tb",
                columns: table => new
                {
                    Off_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    OffTitle = table.Column<string>(maxLength: 200, nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    Price = table.Column<byte>(nullable: false),
                    Lang = table.Column<string>(type: "varchar(8)", nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Off_tb", x => x.Off_ID);
                    table.ForeignKey(
                        name: "FK_Off_tb_Language_tb_Lang",
                        column: x => x.Lang,
                        principalTable: "Language_tb",
                        principalColumn: "Lang_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Producer_tb",
                columns: table => new
                {
                    Producer_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProducerTitle = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    Icon = table.Column<string>(maxLength: 100, nullable: true),
                    Lang = table.Column<string>(type: "varchar(8)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Producer_tb", x => x.Producer_ID);
                    table.ForeignKey(
                        name: "FK_Producer_tb_Language_tb_Lang",
                        column: x => x.Lang,
                        principalTable: "Language_tb",
                        principalColumn: "Lang_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProjectGroup_tb",
                columns: table => new
                {
                    ProjectGroup_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(maxLength: 300, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    GroupImage = table.Column<string>(maxLength: 100, nullable: false),
                    Lang = table.Column<string>(type: "varchar(8)", nullable: true),
                    ParentId = table.Column<short>(nullable: true),
                    IsParent = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectGroup_tb", x => x.ProjectGroup_ID);
                    table.ForeignKey(
                        name: "FK_ProjectGroup_tb_Language_tb_Lang",
                        column: x => x.Lang,
                        principalTable: "Language_tb",
                        principalColumn: "Lang_ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProjectGroup_tb_ProjectGroup_tb_ParentId",
                        column: x => x.ParentId,
                        principalTable: "ProjectGroup_tb",
                        principalColumn: "ProjectGroup_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SendType_tb",
                columns: table => new
                {
                    SendType_Id = table.Column<byte>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SendType_Title = table.Column<string>(nullable: true),
                    SendType_Description = table.Column<string>(nullable: true),
                    SendType_Type = table.Column<byte>(nullable: false),
                    SendType_Price = table.Column<int>(nullable: false),
                    Lang_ID = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SendType_tb", x => x.SendType_Id);
                    table.ForeignKey(
                        name: "FK_SendType_tb_Language_tb_Lang_ID",
                        column: x => x.Lang_ID,
                        principalTable: "Language_tb",
                        principalColumn: "Lang_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SlideShowGroup_tb",
                columns: table => new
                {
                    SlideShowGroupID = table.Column<byte>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SSGName = table.Column<string>(maxLength: 150, nullable: false),
                    Lnag = table.Column<string>(type: "varchar(8)", nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SlideShowGroup_tb", x => x.SlideShowGroupID);
                    table.ForeignKey(
                        name: "FK_SlideShowGroup_tb_Language_tb_Lnag",
                        column: x => x.Lnag,
                        principalTable: "Language_tb",
                        principalColumn: "Lang_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Unit_tb",
                columns: table => new
                {
                    Unit_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UnitTitle = table.Column<string>(maxLength: 50, nullable: false),
                    Lang = table.Column<string>(type: "varchar(8)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Unit_tb", x => x.Unit_ID);
                    table.ForeignKey(
                        name: "FK_Unit_tb_Language_tb_Lang",
                        column: x => x.Lang,
                        principalTable: "Language_tb",
                        principalColumn: "Lang_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProposalFiles_tb",
                columns: table => new
                {
                    P_File_ID = table.Column<short>(nullable: false),
                    ProposalId = table.Column<short>(nullable: false),
                    P_FileName = table.Column<string>(maxLength: 60, nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProposalFiles_tb", x => x.P_File_ID);
                    table.ForeignKey(
                        name: "FK_ProposalFiles_tb_MainProposal_tb_ProposalId",
                        column: x => x.ProposalId,
                        principalTable: "MainProposal_tb",
                        principalColumn: "Proposal_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SuppliersCertificates_tb",
                columns: table => new
                {
                    Certificate_ID = table.Column<int>(nullable: false),
                    SupplierId = table.Column<short>(nullable: false),
                    Title = table.Column<string>(maxLength: 200, nullable: false),
                    Type = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuppliersCertificates_tb", x => x.Certificate_ID);
                    table.ForeignKey(
                        name: "FK_SuppliersCertificates_tb_MainSuppliers_tb_SupplierId",
                        column: x => x.SupplierId,
                        principalTable: "MainSuppliers_tb",
                        principalColumn: "Suppliers_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SupplierShareholders_tb",
                columns: table => new
                {
                    ShareHolder_ID = table.Column<short>(nullable: false),
                    Suppliers_ID = table.Column<short>(nullable: false),
                    FullName = table.Column<string>(maxLength: 100, nullable: false),
                    Type = table.Column<bool>(nullable: false),
                    PercentageShare = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupplierShareholders_tb", x => x.ShareHolder_ID);
                    table.ForeignKey(
                        name: "FK_SupplierShareholders_tb_MainSuppliers_tb_Suppliers_ID",
                        column: x => x.Suppliers_ID,
                        principalTable: "MainSuppliers_tb",
                        principalColumn: "Suppliers_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SuppliersResume_tb",
                columns: table => new
                {
                    Resume_ID = table.Column<int>(nullable: false),
                    SupplierId = table.Column<short>(nullable: false),
                    Title = table.Column<string>(maxLength: 100, nullable: false),
                    Employer = table.Column<string>(maxLength: 100, nullable: false),
                    ResumeDescription = table.Column<string>(maxLength: 500, nullable: true),
                    CollaborationPeriod = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuppliersResume_tb", x => x.Resume_ID);
                    table.ForeignKey(
                        name: "FK_SuppliersResume_tb_MainSuppliers_tb_SupplierId",
                        column: x => x.SupplierId,
                        principalTable: "MainSuppliers_tb",
                        principalColumn: "Suppliers_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SurvayQuestionGroup_tb",
                columns: table => new
                {
                    SurveyGroup_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MainSurveyId = table.Column<byte>(nullable: false),
                    Title = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SurvayQuestionGroup_tb", x => x.SurveyGroup_ID);
                    table.ForeignKey(
                        name: "FK_SurvayQuestionGroup_tb_MainSurvey_tb_MainSurveyId",
                        column: x => x.MainSurveyId,
                        principalTable: "MainSurvey_tb",
                        principalColumn: "MainSurvey_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SurveyCustomerOtherData_tb",
                columns: table => new
                {
                    C_OtherData_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MainSurveyId = table.Column<byte>(nullable: false),
                    FullName = table.Column<string>(maxLength: 100, nullable: false),
                    OfficePost = table.Column<string>(maxLength: 50, nullable: true),
                    PhoneNumber = table.Column<string>(maxLength: 15, nullable: false),
                    CompeletFromDate = table.Column<DateTime>(nullable: false),
                    Proposal = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SurveyCustomerOtherData_tb", x => x.C_OtherData_ID);
                    table.ForeignKey(
                        name: "FK_SurveyCustomerOtherData_tb_MainSurvey_tb_MainSurveyId",
                        column: x => x.MainSurveyId,
                        principalTable: "MainSurvey_tb",
                        principalColumn: "MainSurvey_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SurveySuppliersOtherData_tb",
                columns: table => new
                {
                    S_OtherData_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MainSurveyId = table.Column<byte>(nullable: false),
                    FullName = table.Column<string>(maxLength: 400, nullable: false),
                    Company = table.Column<string>(maxLength: 50, nullable: true),
                    Proposal = table.Column<string>(maxLength: 450, nullable: true),
                    CompleteFormDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SurveySuppliersOtherData_tb", x => x.S_OtherData_ID);
                    table.ForeignKey(
                        name: "FK_SurveySuppliersOtherData_tb_MainSurvey_tb_MainSurveyId",
                        column: x => x.MainSurveyId,
                        principalTable: "MainSurvey_tb",
                        principalColumn: "MainSurvey_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Employee_tb",
                columns: table => new
                {
                    PersonalCode = table.Column<string>(maxLength: 10, nullable: false),
                    UserIdentity_Id = table.Column<string>(maxLength: 450, nullable: true),
                    NationalCode = table.Column<string>(maxLength: 10, nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    OfficePostId = table.Column<short>(nullable: false),
                    OfficeUnitId = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee_tb", x => x.PersonalCode);
                    table.ForeignKey(
                        name: "FK_Employee_tb_OfficePost_tb_OfficePostId",
                        column: x => x.OfficePostId,
                        principalTable: "OfficePost_tb",
                        principalColumn: "OfficePostID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Employee_tb_OfficeUnit_tb_OfficeUnitId",
                        column: x => x.OfficeUnitId,
                        principalTable: "OfficeUnit_tb",
                        principalColumn: "OfficeUnitID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Employee_tb_AspNetUsers_UserIdentity_Id",
                        column: x => x.UserIdentity_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SuppliersFiles_tb",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false),
                    FileTypeId = table.Column<byte>(nullable: false),
                    SupplierId = table.Column<short>(nullable: false),
                    FileNames = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuppliersFiles_tb", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SuppliersFiles_tb_SuppliersFileType_tb_FileTypeId",
                        column: x => x.FileTypeId,
                        principalTable: "SuppliersFileType_tb",
                        principalColumn: "FileType_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SuppliersFiles_tb_MainSuppliers_tb_SupplierId",
                        column: x => x.SupplierId,
                        principalTable: "MainSuppliers_tb",
                        principalColumn: "Suppliers_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SuppliersActivityFields_tb",
                columns: table => new
                {
                    S_ActivityFields_ID = table.Column<short>(nullable: false),
                    ActivityId = table.Column<short>(nullable: false),
                    SupplyCapacity = table.Column<int>(nullable: false),
                    SupplierId = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuppliersActivityFields_tb", x => x.S_ActivityFields_ID);
                    table.ForeignKey(
                        name: "FK_SuppliersActivityFields_tb_Activity_tb_ActivityId",
                        column: x => x.ActivityId,
                        principalTable: "Activity_tb",
                        principalColumn: "Activity_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SuppliersActivityFields_tb_MainSuppliers_tb_SupplierId",
                        column: x => x.SupplierId,
                        principalTable: "MainSuppliers_tb",
                        principalColumn: "Suppliers_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AVF_tb",
                columns: table => new
                {
                    AVF_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AVFGroupId = table.Column<short>(nullable: false),
                    AVFTitle = table.Column<string>(maxLength: 200, nullable: false),
                    FileOrLink = table.Column<bool>(nullable: false),
                    FileNameOrLink = table.Column<string>(maxLength: 2000, nullable: false),
                    AVFSize = table.Column<string>(maxLength: 50, nullable: true),
                    AVFDownloadCount = table.Column<short>(nullable: true),
                    FileType = table.Column<string>(maxLength: 50, nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    VideoImage = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AVF_tb", x => x.AVF_ID);
                    table.ForeignKey(
                        name: "FK_AVF_tb_AVFGroup_tb_AVFGroupId",
                        column: x => x.AVFGroupId,
                        principalTable: "AVFGroup_tb",
                        principalColumn: "AVFGroup_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Connection_tb",
                columns: table => new
                {
                    Connection_ID = table.Column<byte>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ConnectionGroupId = table.Column<byte>(nullable: false),
                    ConnectionName = table.Column<string>(maxLength: 100, nullable: false),
                    ConnectionValue = table.Column<string>(maxLength: 500, nullable: false),
                    CType = table.Column<bool>(nullable: false),
                    CImage = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Connection_tb", x => x.Connection_ID);
                    table.ForeignKey(
                        name: "FK_Connection_tb_ConnectionGroup_tb_ConnectionGroupId",
                        column: x => x.ConnectionGroupId,
                        principalTable: "ConnectionGroup_tb",
                        principalColumn: "ConnectionGroup_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ContactUsMessage_tb",
                columns: table => new
                {
                    ContactusMessage_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CMGId = table.Column<byte>(nullable: false),
                    SenderName = table.Column<string>(maxLength: 150, nullable: true),
                    SenderEmail = table.Column<string>(maxLength: 200, nullable: false),
                    SenderMobile = table.Column<string>(maxLength: 15, nullable: true),
                    MessageText = table.Column<string>(maxLength: 2000, nullable: false),
                    MessageAnswer = table.Column<string>(maxLength: 2000, nullable: true),
                    IsNew = table.Column<bool>(nullable: false),
                    SendDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactUsMessage_tb", x => x.ContactusMessage_ID);
                    table.ForeignKey(
                        name: "FK_ContactUsMessage_tb_ContactUsMessageGroup_tb_CMGId",
                        column: x => x.CMGId,
                        principalTable: "ContactUsMessageGroup_tb",
                        principalColumn: "CMG_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Contents_tb",
                columns: table => new
                {
                    Content_ID = table.Column<int>(nullable: false),
                    ContentGroupId = table.Column<short>(nullable: false),
                    ContentTitle = table.Column<string>(maxLength: 200, nullable: false),
                    ContentSummary = table.Column<string>(maxLength: 1500, nullable: false),
                    ContentText = table.Column<string>(nullable: false),
                    ContentImage = table.Column<string>(maxLength: 150, nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contents_tb", x => x.Content_ID);
                    table.ForeignKey(
                        name: "FK_Contents_tb_ContentsGroup_tb_ContentGroupId",
                        column: x => x.ContentGroupId,
                        principalTable: "ContentsGroup_tb",
                        principalColumn: "ContentGroup_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "State_tb",
                columns: table => new
                {
                    State_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StateName = table.Column<string>(maxLength: 50, nullable: false),
                    StateCode = table.Column<string>(type: "varchar(10)", nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    CountryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_State_tb", x => x.State_ID);
                    table.ForeignKey(
                        name: "FK_State_tb_Country_tb_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country_tb",
                        principalColumn: "Country_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Gallery_tb",
                columns: table => new
                {
                    Gallery_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    GalleryGroupId = table.Column<byte>(nullable: false),
                    GalleryTitle = table.Column<string>(maxLength: 200, nullable: false),
                    GalleryImage = table.Column<string>(maxLength: 150, nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gallery_tb", x => x.Gallery_ID);
                    table.ForeignKey(
                        name: "FK_Gallery_tb_GalleryGroup_tb_GalleryGroupId",
                        column: x => x.GalleryGroupId,
                        principalTable: "GalleryGroup_tb",
                        principalColumn: "GalleryGroup_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "NewsLetter_tb",
                columns: table => new
                {
                    NewsLetter_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    GroupId = table.Column<byte>(nullable: false),
                    FullName = table.Column<string>(maxLength: 150, nullable: true),
                    Email = table.Column<string>(maxLength: 150, nullable: false),
                    Mobile = table.Column<string>(maxLength: 15, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewsLetter_tb", x => x.NewsLetter_ID);
                    table.ForeignKey(
                        name: "FK_NewsLetter_tb_NewsLetterGroup_tb_GroupId",
                        column: x => x.GroupId,
                        principalTable: "NewsLetterGroup_tb",
                        principalColumn: "Group_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductGroup_tb",
                columns: table => new
                {
                    ProductGroup_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductGroupTitle = table.Column<string>(maxLength: 300, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    ProductGroupImage = table.Column<string>(maxLength: 100, nullable: false),
                    OffId = table.Column<short>(nullable: true),
                    Lang = table.Column<string>(type: "varchar(8)", nullable: true),
                    ParentId = table.Column<short>(nullable: true),
                    IsParent = table.Column<bool>(nullable: false),
                    PicLargeWidth = table.Column<short>(nullable: true),
                    PicSmallWidth = table.Column<short>(nullable: true),
                    PicLargeHeight = table.Column<short>(nullable: true),
                    PicSmallHeight = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductGroup_tb", x => x.ProductGroup_ID);
                    table.ForeignKey(
                        name: "FK_ProductGroup_tb_Language_tb_Lang",
                        column: x => x.Lang,
                        principalTable: "Language_tb",
                        principalColumn: "Lang_ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductGroup_tb_Off_tb_OffId",
                        column: x => x.OffId,
                        principalTable: "Off_tb",
                        principalColumn: "Off_ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductGroup_tb_ProductGroup_tb_ParentId",
                        column: x => x.ParentId,
                        principalTable: "ProductGroup_tb",
                        principalColumn: "ProductGroup_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SlideShow_tb",
                columns: table => new
                {
                    SlideShowID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SSGId = table.Column<byte>(type: "tinyint", nullable: false),
                    SSPic = table.Column<string>(maxLength: 150, nullable: false),
                    SSTitle = table.Column<string>(maxLength: 150, nullable: true),
                    SSText = table.Column<string>(maxLength: 700, nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    Link = table.Column<string>(maxLength: 300, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SlideShow_tb", x => x.SlideShowID);
                    table.ForeignKey(
                        name: "FK_SlideShow_tb_SlideShowGroup_tb_SSGId",
                        column: x => x.SSGId,
                        principalTable: "SlideShowGroup_tb",
                        principalColumn: "SlideShowGroupID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SurveyQuestion_tb",
                columns: table => new
                {
                    Question_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    QuestionGroupId = table.Column<short>(nullable: false),
                    AnswerTypeId = table.Column<short>(nullable: false),
                    Question = table.Column<string>(maxLength: 500, nullable: false),
                    ReplyCount = table.Column<int>(nullable: false),
                    OptionVoteCount1 = table.Column<int>(nullable: false),
                    OptionVoteCount2 = table.Column<int>(nullable: false),
                    OptionVoteCount3 = table.Column<int>(nullable: false),
                    OptionVoteCount4 = table.Column<int>(nullable: false),
                    OptionVoteCount5 = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SurveyQuestion_tb", x => x.Question_ID);
                    table.ForeignKey(
                        name: "FK_SurveyQuestion_tb_SurveyAnswerType_tb_AnswerTypeId",
                        column: x => x.AnswerTypeId,
                        principalTable: "SurveyAnswerType_tb",
                        principalColumn: "AnswerType_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SurveyQuestion_tb_SurvayQuestionGroup_tb_QuestionGroupId",
                        column: x => x.QuestionGroupId,
                        principalTable: "SurvayQuestionGroup_tb",
                        principalColumn: "SurveyGroup_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Employe_Post",
                columns: table => new
                {
                    Employ_Post_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PersonalCode = table.Column<string>(nullable: true),
                    PostId = table.Column<short>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employe_Post", x => x.Employ_Post_ID);
                    table.ForeignKey(
                        name: "FK_Employe_Post_Employee_tb_PersonalCode",
                        column: x => x.PersonalCode,
                        principalTable: "Employee_tb",
                        principalColumn: "PersonalCode",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employe_Post_OfficePost_tb_PostId",
                        column: x => x.PostId,
                        principalTable: "OfficePost_tb",
                        principalColumn: "OfficePostID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeSalary",
                columns: table => new
                {
                    Salary_ID = table.Column<string>(maxLength: 50, nullable: false),
                    PersonalCode = table.Column<string>(maxLength: 10, nullable: true),
                    SalMah = table.Column<string>(maxLength: 10, nullable: true),
                    NameMahaleKhedmatHokmi = table.Column<string>(maxLength: 100, nullable: true),
                    SP_Khales_Pardakhti = table.Column<int>(nullable: false),
                    FR_Ayab_Zahab = table.Column<int>(nullable: false),
                    FR_Bime_Takmili = table.Column<int>(nullable: false),
                    FR_DastmozdRoozaneh = table.Column<int>(nullable: false),
                    FR_FOGHOLADE = table.Column<int>(nullable: false),
                    FR_Hag_Jazb = table.Column<int>(nullable: false),
                    FR_HAG_MASKAN = table.Column<int>(nullable: false),
                    FR_HAG_MASULEYAT = table.Column<int>(nullable: false),
                    FR_HAG_OLAD = table.Column<int>(nullable: false),
                    FR_Hogug_Payeh = table.Column<int>(nullable: false),
                    FR_Sayer_Mazaya = table.Column<int>(nullable: false),
                    SP_Ezafe_Kar = table.Column<int>(nullable: false),
                    SP_Hag_Bimeh_Karmand = table.Column<int>(nullable: false),
                    SP_Jame_Ezafat = table.Column<int>(nullable: false),
                    SP_Jame_Kosurat = table.Column<int>(nullable: false),
                    SP_Maleyat = table.Column<int>(nullable: false),
                    SP_MashMul_Bimeh = table.Column<int>(nullable: false),
                    SP_Mashmul_Maleyat = table.Column<int>(nullable: false),
                    SY_KE_VamGhest1 = table.Column<int>(nullable: false),
                    SY_KE_VamMandeh1 = table.Column<int>(nullable: false),
                    SY_Saat_AzafeKar = table.Column<short>(nullable: false),
                    SY_sayer_Kosurat = table.Column<int>(nullable: false),
                    SY_Tadad_Ruz_Kar = table.Column<short>(nullable: false),
                    ZR_FOGHOLADE_Special = table.Column<int>(nullable: false),
                    SY_saat_nobat_kari = table.Column<int>(nullable: false),
                    FR_moavaghe = table.Column<int>(nullable: false),
                    FR_mosaede = table.Column<int>(nullable: false),
                    FR_padash = table.Column<int>(nullable: false),
                    FR_sayer_ezafat = table.Column<int>(nullable: false),
                    SP_mablagh_nobatkari = table.Column<int>(nullable: false),
                    SP_Malyat_hoghogh = table.Column<int>(nullable: false),
                    FR_bon_kargari = table.Column<int>(nullable: false),
                    FR_mozd_sanavat = table.Column<int>(nullable: false),
                    SP_mablagh_MAMOREYAT = table.Column<int>(nullable: false),
                    FR_Karane = table.Column<int>(nullable: false),
                    FR_mamoriat = table.Column<int>(nullable: false),
                    FR_Eydi = table.Column<int>(nullable: false),
                    FR_m_maliat_EYDI = table.Column<int>(nullable: false),
                    FR_Date_StartKar = table.Column<string>(maxLength: 15, nullable: true),
                    FR_MandehMorakhasi = table.Column<float>(nullable: false),
                    SY_GHaybat_Ruz = table.Column<short>(nullable: false),
                    SY_Morkhasi_Ruz = table.Column<short>(nullable: false),
                    SY_Morkhasi_Saat = table.Column<float>(nullable: false),
                    FR_Bimeh_mashin = table.Column<int>(nullable: false),
                    FR_Sandogh_Z_V = table.Column<int>(nullable: false),
                    SY_KE_VamGhest2 = table.Column<int>(nullable: false),
                    SY_KE_NameVam1 = table.Column<string>(maxLength: 100, nullable: true),
                    SY_KE_NameVam2 = table.Column<string>(maxLength: 100, nullable: true),
                    SY_KE_NameVam3 = table.Column<string>(maxLength: 100, nullable: true),
                    SY_KE_NameVam4 = table.Column<string>(maxLength: 100, nullable: true),
                    SY_KE_NameVam5 = table.Column<string>(nullable: true),
                    SY_KE_VamGhest3 = table.Column<int>(nullable: false),
                    SY_KE_VamGhest4 = table.Column<int>(nullable: false),
                    SY_KE_VamGhest5 = table.Column<int>(nullable: false),
                    SY_KE_VamMandeh2 = table.Column<int>(nullable: false),
                    SY_KE_VamMandeh3 = table.Column<int>(nullable: false),
                    SY_KE_VamMandeh4 = table.Column<int>(nullable: false),
                    SY_KE_VamMandeh5 = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeSalary", x => x.Salary_ID);
                    table.ForeignKey(
                        name: "FK_EmployeeSalary_Employee_tb_PersonalCode",
                        column: x => x.PersonalCode,
                        principalTable: "Employee_tb",
                        principalColumn: "PersonalCode",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Mission_tb",
                columns: table => new
                {
                    Mission_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MissionStart = table.Column<string>(nullable: false),
                    MissionEnd = table.Column<string>(nullable: false),
                    MissionType = table.Column<byte>(nullable: false),
                    Reason = table.Column<string>(maxLength: 250, nullable: false),
                    Followers = table.Column<string>(maxLength: 200, nullable: true),
                    Destination = table.Column<string>(maxLength: 200, nullable: true),
                    Issue = table.Column<string>(maxLength: 900, nullable: false),
                    IssueDate = table.Column<string>(nullable: true),
                    PersonalCode_1 = table.Column<string>(nullable: false),
                    ApplicantSignature = table.Column<string>(nullable: true),
                    ManagerSignature_1 = table.Column<string>(maxLength: 10, nullable: true),
                    HumanResourcesSignature_1 = table.Column<string>(maxLength: 10, nullable: true),
                    Manager_Status = table.Column<bool>(nullable: false),
                    Human_Status = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mission_tb", x => x.Mission_ID);
                    table.ForeignKey(
                        name: "FK_Mission_tb_Employee_tb_ApplicantSignature",
                        column: x => x.ApplicantSignature,
                        principalTable: "Employee_tb",
                        principalColumn: "PersonalCode",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Mission_tb_Employee_tb_PersonalCode_1",
                        column: x => x.PersonalCode_1,
                        principalTable: "Employee_tb",
                        principalColumn: "PersonalCode",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProposalProviders_tb",
                columns: table => new
                {
                    PP_ID = table.Column<int>(nullable: false),
                    PersonalCode = table.Column<string>(maxLength: 10, nullable: false),
                    ProposalId = table.Column<short>(nullable: false),
                    UnitId = table.Column<short>(nullable: false),
                    PostId = table.Column<short>(nullable: false),
                    PhoneNumber = table.Column<string>(maxLength: 12, nullable: true),
                    OfficeUnitID = table.Column<short>(nullable: true),
                    OfficePostID = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProposalProviders_tb", x => x.PP_ID);
                    table.ForeignKey(
                        name: "FK_ProposalProviders_tb_OfficePost_tb_OfficePostID",
                        column: x => x.OfficePostID,
                        principalTable: "OfficePost_tb",
                        principalColumn: "OfficePostID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProposalProviders_tb_OfficeUnit_tb_OfficeUnitID",
                        column: x => x.OfficeUnitID,
                        principalTable: "OfficeUnit_tb",
                        principalColumn: "OfficeUnitID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProposalProviders_tb_Employee_tb_PersonalCode",
                        column: x => x.PersonalCode,
                        principalTable: "Employee_tb",
                        principalColumn: "PersonalCode",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProposalProviders_tb_MainProposal_tb_ProposalId",
                        column: x => x.ProposalId,
                        principalTable: "MainProposal_tb",
                        principalColumn: "Proposal_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SurveyEmployeesOtherData_tb",
                columns: table => new
                {
                    SE_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PersonalCode = table.Column<string>(nullable: true),
                    MainSurveyId = table.Column<byte>(nullable: false),
                    CompleteFormDate = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SurveyEmployeesOtherData_tb", x => x.SE_ID);
                    table.ForeignKey(
                        name: "FK_SurveyEmployeesOtherData_tb_MainSurvey_tb_MainSurveyId",
                        column: x => x.MainSurveyId,
                        principalTable: "MainSurvey_tb",
                        principalColumn: "MainSurvey_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SurveyEmployeesOtherData_tb_Employee_tb_PersonalCode",
                        column: x => x.PersonalCode,
                        principalTable: "Employee_tb",
                        principalColumn: "PersonalCode",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Vacation_tb",
                columns: table => new
                {
                    Vecation_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Section = table.Column<string>(maxLength: 100, nullable: true),
                    VecationType = table.Column<bool>(nullable: false),
                    DateVacationTime = table.Column<DateTime>(nullable: true),
                    StartTime = table.Column<string>(maxLength: 10, nullable: true),
                    EndTime = table.Column<string>(maxLength: 10, nullable: true),
                    DayVecationStartDate = table.Column<DateTime>(nullable: true),
                    DayVecationEndDate = table.Column<DateTime>(nullable: true),
                    Duration = table.Column<byte>(nullable: true),
                    PersonalCode_3 = table.Column<string>(nullable: false),
                    ApplicantSignature = table.Column<string>(maxLength: 10, nullable: true),
                    AlternateSignature = table.Column<string>(maxLength: 10, nullable: true),
                    UnitsSupervisorSignature = table.Column<string>(maxLength: 10, nullable: true),
                    StatusUnitsSupervisorSignature = table.Column<bool>(nullable: false),
                    AdministrativeAffairsSignature = table.Column<string>(maxLength: 10, nullable: true),
                    StatusAdministrativeAffairsSignature = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vacation_tb", x => x.Vecation_ID);
                    table.ForeignKey(
                        name: "FK_Vacation_tb_Employee_tb_PersonalCode_3",
                        column: x => x.PersonalCode_3,
                        principalTable: "Employee_tb",
                        principalColumn: "PersonalCode",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ContentFiles_tb",
                columns: table => new
                {
                    AvfId = table.Column<int>(nullable: false),
                    ContentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentFiles_tb", x => new { x.AvfId, x.ContentId });
                    table.ForeignKey(
                        name: "FK_ContentFiles_tb_AVF_tb_AvfId",
                        column: x => x.AvfId,
                        principalTable: "AVF_tb",
                        principalColumn: "AVF_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContentFiles_tb_Contents_tb_ContentId",
                        column: x => x.ContentId,
                        principalTable: "Contents_tb",
                        principalColumn: "Content_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ContentsComment_tb",
                columns: table => new
                {
                    Comment_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ContentId = table.Column<int>(nullable: true),
                    ContentGroupId = table.Column<short>(nullable: false),
                    Comment = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 50, nullable: false),
                    Email = table.Column<string>(maxLength: 50, nullable: false),
                    IsNew = table.Column<bool>(nullable: false),
                    CommentDate = table.Column<DateTime>(nullable: false),
                    PositiveScore = table.Column<byte>(nullable: true),
                    NegativeScore = table.Column<byte>(nullable: true),
                    IsConfirmed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentsComment_tb", x => x.Comment_ID);
                    table.ForeignKey(
                        name: "FK_ContentsComment_tb_ContentsGroup_tb_ContentGroupId",
                        column: x => x.ContentGroupId,
                        principalTable: "ContentsGroup_tb",
                        principalColumn: "ContentGroup_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContentsComment_tb_Contents_tb_ContentId",
                        column: x => x.ContentId,
                        principalTable: "Contents_tb",
                        principalColumn: "Content_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContentsTag_tb",
                columns: table => new
                {
                    Tag_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ContentId = table.Column<int>(nullable: false),
                    TagText = table.Column<string>(maxLength: 1000, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentsTag_tb", x => x.Tag_ID);
                    table.ForeignKey(
                        name: "FK_ContentsTag_tb_Contents_tb_ContentId",
                        column: x => x.ContentId,
                        principalTable: "Contents_tb",
                        principalColumn: "Content_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ContentsVotes_tb",
                columns: table => new
                {
                    ContentVotes_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ContentId = table.Column<int>(nullable: false),
                    CVoteCount = table.Column<short>(nullable: false),
                    CSumVotes = table.Column<short>(nullable: false),
                    CAvgVote = table.Column<float>(nullable: false),
                    CVisitCount = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentsVotes_tb", x => x.ContentVotes_ID);
                    table.ForeignKey(
                        name: "FK_ContentsVotes_tb_Contents_tb_ContentId",
                        column: x => x.ContentId,
                        principalTable: "Contents_tb",
                        principalColumn: "Content_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "City_tb",
                columns: table => new
                {
                    City_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CityName = table.Column<string>(maxLength: 50, nullable: false),
                    CityCode = table.Column<string>(type: "varchar(10)", nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    State_ID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_City_tb", x => x.City_ID);
                    table.ForeignKey(
                        name: "FK_City_tb_State_tb_State_ID",
                        column: x => x.State_ID,
                        principalTable: "State_tb",
                        principalColumn: "State_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ContentsGallery_tb",
                columns: table => new
                {
                    GalleryId = table.Column<short>(nullable: false),
                    ContentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentsGallery_tb", x => new { x.GalleryId, x.ContentId });
                    table.UniqueConstraint("AK_ContentsGallery_tb_ContentId_GalleryId", x => new { x.ContentId, x.GalleryId });
                    table.ForeignKey(
                        name: "FK_ContentsGallery_tb_Contents_tb_ContentId",
                        column: x => x.ContentId,
                        principalTable: "Contents_tb",
                        principalColumn: "Content_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContentsGallery_tb_Gallery_tb_GalleryId",
                        column: x => x.GalleryId,
                        principalTable: "Gallery_tb",
                        principalColumn: "Gallery_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GalleryPicture_tb",
                columns: table => new
                {
                    GalleryId = table.Column<short>(nullable: false),
                    GalleryPicTitle = table.Column<string>(maxLength: 150, nullable: true),
                    ImageNames = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GalleryPicture_tb", x => x.ImageNames);
                    table.ForeignKey(
                        name: "FK_GalleryPicture_tb_Gallery_tb_GalleryId",
                        column: x => x.GalleryId,
                        principalTable: "Gallery_tb",
                        principalColumn: "Gallery_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MainProduct_tb",
                columns: table => new
                {
                    Product_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductImage = table.Column<string>(maxLength: 150, nullable: true),
                    GalleryId = table.Column<short>(nullable: true),
                    ProductCode = table.Column<string>(maxLength: 100, nullable: true),
                    IsPopular = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    IsExsit = table.Column<bool>(nullable: false),
                    ExistCount = table.Column<int>(nullable: true),
                    IsDecor = table.Column<bool>(nullable: false),
                    SaledCount = table.Column<int>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    ProductCatalog = table.Column<string>(maxLength: 150, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MainProduct_tb", x => x.Product_ID);
                    table.ForeignKey(
                        name: "FK_MainProduct_tb_Gallery_tb_GalleryId",
                        column: x => x.GalleryId,
                        principalTable: "Gallery_tb",
                        principalColumn: "Gallery_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Projects_tb",
                columns: table => new
                {
                    Project_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Image = table.Column<string>(maxLength: 150, nullable: true),
                    GalleryId = table.Column<short>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    IsPopular = table.Column<bool>(nullable: false),
                    Title = table.Column<string>(maxLength: 500, nullable: false),
                    GroupId = table.Column<short>(nullable: true),
                    Description = table.Column<string>(nullable: false),
                    MetaTitle = table.Column<string>(nullable: true),
                    MetaKeyword = table.Column<string>(maxLength: 500, nullable: true),
                    MetaDescription = table.Column<string>(maxLength: 500, nullable: true),
                    MetaOther = table.Column<string>(maxLength: 500, nullable: true),
                    ProducerId = table.Column<short>(nullable: true),
                    Tags = table.Column<string>(maxLength: 400, nullable: true),
                    Slug = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects_tb", x => x.Project_ID);
                    table.ForeignKey(
                        name: "FK_Projects_tb_Gallery_tb_GalleryId",
                        column: x => x.GalleryId,
                        principalTable: "Gallery_tb",
                        principalColumn: "Gallery_ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Projects_tb_ProjectGroup_tb_GroupId",
                        column: x => x.GroupId,
                        principalTable: "ProjectGroup_tb",
                        principalColumn: "ProjectGroup_ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Projects_tb_Producer_tb_ProducerId",
                        column: x => x.ProducerId,
                        principalTable: "Producer_tb",
                        principalColumn: "Producer_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Feature_tb",
                columns: table => new
                {
                    Feature_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductGroupId = table.Column<short>(nullable: true),
                    FeatureTitle = table.Column<string>(maxLength: 200, nullable: false),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Feature_tb", x => x.Feature_ID);
                    table.ForeignKey(
                        name: "FK_Feature_tb_ProductGroup_tb_ProductGroupId",
                        column: x => x.ProductGroupId,
                        principalTable: "ProductGroup_tb",
                        principalColumn: "ProductGroup_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SurveyQuestionReply_tb",
                columns: table => new
                {
                    QuestionId = table.Column<short>(nullable: false),
                    SelectedReply = table.Column<byte>(nullable: false),
                    ExtraInformationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SurveyQuestionReply_tb", x => new { x.ExtraInformationId, x.QuestionId });
                    table.ForeignKey(
                        name: "FK_SurveyQuestionReply_tb_SurveyQuestion_tb_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "SurveyQuestion_tb",
                        principalColumn: "Question_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CustomerAddress_tb",
                columns: table => new
                {
                    CustomerAddress_Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Address = table.Column<string>(nullable: false),
                    PostalCode = table.Column<string>(nullable: false),
                    Phone = table.Column<string>(nullable: false),
                    Customer_Id = table.Column<int>(nullable: false),
                    Country_Id = table.Column<int>(nullable: false),
                    State_Id = table.Column<int>(nullable: false),
                    City_Id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerAddress_tb", x => x.CustomerAddress_Id);
                    table.ForeignKey(
                        name: "FK_CustomerAddress_tb_City_tb_City_Id",
                        column: x => x.City_Id,
                        principalTable: "City_tb",
                        principalColumn: "City_ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CustomerAddress_tb_Country_tb_Country_Id",
                        column: x => x.Country_Id,
                        principalTable: "Country_tb",
                        principalColumn: "Country_ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CustomerAddress_tb_Customer_tb_Customer_Id",
                        column: x => x.Customer_Id,
                        principalTable: "Customer_tb",
                        principalColumn: "Customer_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomerAddress_tb_State_tb_State_Id",
                        column: x => x.State_Id,
                        principalTable: "State_tb",
                        principalColumn: "State_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProductInfo_tb",
                columns: table => new
                {
                    ProductInfo_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductId = table.Column<int>(nullable: false),
                    ProductTitle = table.Column<string>(maxLength: 500, nullable: false),
                    ProductSummary = table.Column<string>(maxLength: 100, nullable: true),
                    ProductGroupId = table.Column<short>(nullable: true),
                    Description = table.Column<string>(nullable: false),
                    UnitId = table.Column<short>(nullable: true),
                    MetaTitle = table.Column<string>(maxLength: 500, nullable: true),
                    MetaKeyword = table.Column<string>(maxLength: 500, nullable: true),
                    MetaDescription = table.Column<string>(maxLength: 500, nullable: true),
                    MetaOther = table.Column<string>(maxLength: 500, nullable: true),
                    OffId = table.Column<short>(nullable: true),
                    ProducerId = table.Column<short>(nullable: true),
                    Tags = table.Column<string>(maxLength: 400, nullable: true),
                    Slug = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductInfo_tb", x => x.ProductInfo_ID);
                    table.ForeignKey(
                        name: "FK_ProductInfo_tb_Off_tb_OffId",
                        column: x => x.OffId,
                        principalTable: "Off_tb",
                        principalColumn: "Off_ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductInfo_tb_Producer_tb_ProducerId",
                        column: x => x.ProducerId,
                        principalTable: "Producer_tb",
                        principalColumn: "Producer_ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductInfo_tb_ProductGroup_tb_ProductGroupId",
                        column: x => x.ProductGroupId,
                        principalTable: "ProductGroup_tb",
                        principalColumn: "ProductGroup_ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductInfo_tb_MainProduct_tb_ProductId",
                        column: x => x.ProductId,
                        principalTable: "MainProduct_tb",
                        principalColumn: "Product_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductInfo_tb_Unit_tb_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Unit_tb",
                        principalColumn: "Unit_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProjectsAdvantage_tb",
                columns: table => new
                {
                    Advantage_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductInfoId = table.Column<int>(nullable: false),
                    AdvantageTitle = table.Column<string>(maxLength: 500, nullable: false),
                    AdvantageType = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectsAdvantage_tb", x => x.Advantage_ID);
                    table.ForeignKey(
                        name: "FK_ProjectsAdvantage_tb_Projects_tb_ProductInfoId",
                        column: x => x.ProductInfoId,
                        principalTable: "Projects_tb",
                        principalColumn: "Project_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProjectVideo_tb",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false),
                    ProjectId = table.Column<int>(nullable: false),
                    VideoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectVideo_tb", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ProjectVideo_tb_Projects_tb_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects_tb",
                        principalColumn: "Project_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProjectVideo_tb_AVF_tb_VideoId",
                        column: x => x.VideoId,
                        principalTable: "AVF_tb",
                        principalColumn: "AVF_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RateOption_tb",
                columns: table => new
                {
                    RateOption_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProjectId = table.Column<int>(nullable: false),
                    RatorTotal = table.Column<int>(nullable: false),
                    RateTotal = table.Column<int>(nullable: false),
                    RateAverage = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RateOption_tb", x => x.RateOption_ID);
                    table.ForeignKey(
                        name: "FK_RateOption_tb_Projects_tb_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects_tb",
                        principalColumn: "Project_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FeatureReply_tb",
                columns: table => new
                {
                    FeatureReply_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FeatureId = table.Column<int>(nullable: false),
                    FeatureReplyText = table.Column<string>(maxLength: 200, nullable: false),
                    IsDefault = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeatureReply_tb", x => x.FeatureReply_ID);
                    table.ForeignKey(
                        name: "FK_FeatureReply_tb_Feature_tb_FeatureId",
                        column: x => x.FeatureId,
                        principalTable: "Feature_tb",
                        principalColumn: "Feature_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Factor",
                columns: table => new
                {
                    Factor_Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Factor_Date = table.Column<string>(nullable: true),
                    TotalPrice = table.Column<int>(nullable: false),
                    Order_Status = table.Column<byte>(nullable: false),
                    PayMent_Status = table.Column<byte>(nullable: false),
                    Factor_Lang = table.Column<string>(nullable: true),
                    Bank_Code = table.Column<int>(nullable: false),
                    Payment_Type = table.Column<byte>(nullable: false),
                    Customer_Id = table.Column<int>(nullable: false),
                    CustomerAddress_Id = table.Column<int>(nullable: false),
                    Lang_ID = table.Column<string>(nullable: true),
                    SendType_ID = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Factor", x => x.Factor_Id);
                    table.ForeignKey(
                        name: "FK_Factor_CustomerAddress_tb_CustomerAddress_Id",
                        column: x => x.CustomerAddress_Id,
                        principalTable: "CustomerAddress_tb",
                        principalColumn: "CustomerAddress_Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Factor_Customer_tb_Customer_Id",
                        column: x => x.Customer_Id,
                        principalTable: "Customer_tb",
                        principalColumn: "Customer_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Factor_Language_tb_Lang_ID",
                        column: x => x.Lang_ID,
                        principalTable: "Language_tb",
                        principalColumn: "Lang_ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Factor_SendType_tb_SendType_ID",
                        column: x => x.SendType_ID,
                        principalTable: "SendType_tb",
                        principalColumn: "SendType_Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CustomerComplaintMaster_tb",
                columns: table => new
                {
                    ComplaintID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductId = table.Column<int>(nullable: false),
                    FullName = table.Column<string>(maxLength: 100, nullable: false),
                    CompanyName = table.Column<string>(maxLength: 30, nullable: true),
                    Address = table.Column<string>(maxLength: 200, nullable: true),
                    Tell = table.Column<string>(maxLength: 15, nullable: true),
                    PhoneNumber = table.Column<string>(maxLength: 15, nullable: false),
                    Fax = table.Column<string>(maxLength: 20, nullable: true),
                    Email = table.Column<string>(maxLength: 100, nullable: true),
                    AgencyName = table.Column<string>(maxLength: 50, nullable: true),
                    OccurrenceDate = table.Column<string>(maxLength: 25, nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Request = table.Column<string>(maxLength: 2000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerComplaintMaster_tb", x => x.ComplaintID);
                    table.ForeignKey(
                        name: "FK_CustomerComplaintMaster_tb_ProductInfo_tb_ProductId",
                        column: x => x.ProductId,
                        principalTable: "ProductInfo_tb",
                        principalColumn: "ProductInfo_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CustomerSurveyProducts_tb",
                columns: table => new
                {
                    ProductId = table.Column<int>(nullable: false),
                    CustomerSurveyId = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerSurveyProducts_tb", x => new { x.ProductId, x.CustomerSurveyId });
                    table.ForeignKey(
                        name: "FK_CustomerSurveyProducts_tb_SurveyCustomerOtherData_tb_CustomerSurveyId",
                        column: x => x.CustomerSurveyId,
                        principalTable: "SurveyCustomerOtherData_tb",
                        principalColumn: "C_OtherData_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomerSurveyProducts_tb_ProductInfo_tb_ProductId",
                        column: x => x.ProductId,
                        principalTable: "ProductInfo_tb",
                        principalColumn: "ProductInfo_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductAdvantage_tb",
                columns: table => new
                {
                    Advantage_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductInfoId = table.Column<int>(nullable: false),
                    AdvantageTitle = table.Column<string>(maxLength: 500, nullable: false),
                    AdvantageType = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductAdvantage_tb", x => x.Advantage_ID);
                    table.ForeignKey(
                        name: "FK_ProductAdvantage_tb_ProductInfo_tb_ProductInfoId",
                        column: x => x.ProductInfoId,
                        principalTable: "ProductInfo_tb",
                        principalColumn: "ProductInfo_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductColor_tb",
                columns: table => new
                {
                    ProductColor_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductId = table.Column<int>(nullable: false),
                    ColorId = table.Column<short>(nullable: false),
                    ProductPrice = table.Column<int>(nullable: true),
                    ProductCount = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductColor_tb", x => x.ProductColor_ID);
                    table.ForeignKey(
                        name: "FK_ProductColor_tb_Color_tb_ColorId",
                        column: x => x.ColorId,
                        principalTable: "Color_tb",
                        principalColumn: "Color_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductColor_tb_ProductInfo_tb_ProductId",
                        column: x => x.ProductId,
                        principalTable: "ProductInfo_tb",
                        principalColumn: "ProductInfo_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductComment_tb",
                columns: table => new
                {
                    Comment_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductInforId = table.Column<int>(nullable: true),
                    ProductGroupId = table.Column<short>(nullable: false),
                    Comment = table.Column<string>(nullable: false),
                    CommentReply = table.Column<string>(nullable: true),
                    UserName = table.Column<string>(maxLength: 50, nullable: false),
                    Email = table.Column<string>(maxLength: 50, nullable: false),
                    IsNew = table.Column<bool>(nullable: false),
                    CommentDate = table.Column<DateTime>(nullable: false),
                    PositiveScore = table.Column<byte>(nullable: true),
                    NegativeScore = table.Column<byte>(nullable: true),
                    IsConfirmed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductComment_tb", x => x.Comment_ID);
                    table.ForeignKey(
                        name: "FK_ProductComment_tb_ProductGroup_tb_ProductGroupId",
                        column: x => x.ProductGroupId,
                        principalTable: "ProductGroup_tb",
                        principalColumn: "ProductGroup_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductComment_tb_ProductInfo_tb_ProductInforId",
                        column: x => x.ProductInforId,
                        principalTable: "ProductInfo_tb",
                        principalColumn: "ProductInfo_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProductGaranty_tb",
                columns: table => new
                {
                    ProductInfoId = table.Column<int>(nullable: false),
                    GarantyId = table.Column<short>(nullable: false),
                    GarantyPrice = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductGaranty_tb", x => new { x.ProductInfoId, x.GarantyId });
                    table.UniqueConstraint("AK_ProductGaranty_tb_GarantyId_ProductInfoId", x => new { x.GarantyId, x.ProductInfoId });
                    table.ForeignKey(
                        name: "FK_ProductGaranty_tb_Garanty_tb_GarantyId",
                        column: x => x.GarantyId,
                        principalTable: "Garanty_tb",
                        principalColumn: "Garanty_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductGaranty_tb_ProductInfo_tb_ProductInfoId",
                        column: x => x.ProductInfoId,
                        principalTable: "ProductInfo_tb",
                        principalColumn: "ProductInfo_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SimilarProduct_tb",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductID = table.Column<int>(nullable: true),
                    SimilarProduct_ID = table.Column<int>(nullable: true),
                    ProductInfo_ID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SimilarProduct_tb", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SimilarProduct_tb_ProductInfo_tb_ProductInfo_ID",
                        column: x => x.ProductInfo_ID,
                        principalTable: "ProductInfo_tb",
                        principalColumn: "ProductInfo_ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SimilarProduct_tb_ProductInfo_tb_SimilarProduct_ID",
                        column: x => x.SimilarProduct_ID,
                        principalTable: "ProductInfo_tb",
                        principalColumn: "ProductInfo_ID",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "ProductFeature_tb",
                columns: table => new
                {
                    ProductInfoId = table.Column<int>(nullable: false),
                    FeatureReplyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductFeature_tb", x => new { x.ProductInfoId, x.FeatureReplyId });
                    table.UniqueConstraint("AK_ProductFeature_tb_FeatureReplyId_ProductInfoId", x => new { x.FeatureReplyId, x.ProductInfoId });
                    table.ForeignKey(
                        name: "FK_ProductFeature_tb_FeatureReply_tb_FeatureReplyId",
                        column: x => x.FeatureReplyId,
                        principalTable: "FeatureReply_tb",
                        principalColumn: "FeatureReply_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductFeature_tb_ProductInfo_tb_ProductInfoId",
                        column: x => x.ProductInfoId,
                        principalTable: "ProductInfo_tb",
                        principalColumn: "ProductInfo_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FactorDetails",
                columns: table => new
                {
                    FactorDetails_Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Count = table.Column<short>(nullable: false),
                    FinalPrice = table.Column<int>(nullable: false),
                    Garanty_Id = table.Column<short>(nullable: true),
                    ProductInfo_Id = table.Column<int>(nullable: false),
                    Factor_Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FactorDetails", x => x.FactorDetails_Id);
                    table.ForeignKey(
                        name: "FK_FactorDetails_Factor_Factor_Id",
                        column: x => x.Factor_Id,
                        principalTable: "Factor",
                        principalColumn: "Factor_Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FactorDetails_Garanty_tb_Garanty_Id",
                        column: x => x.Garanty_Id,
                        principalTable: "Garanty_tb",
                        principalColumn: "Garanty_ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FactorDetails_ProductInfo_tb_ProductInfo_Id",
                        column: x => x.ProductInfo_Id,
                        principalTable: "ProductInfo_tb",
                        principalColumn: "ProductInfo_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CustomerComplaintFiles_tb",
                columns: table => new
                {
                    CC_File_ID = table.Column<short>(nullable: false),
                    ComplaintId = table.Column<short>(nullable: false),
                    FileNames = table.Column<string>(maxLength: 100, nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerComplaintFiles_tb", x => x.CC_File_ID);
                    table.ForeignKey(
                        name: "FK_CustomerComplaintFiles_tb_CustomerComplaintMaster_tb_ComplaintId",
                        column: x => x.ComplaintId,
                        principalTable: "CustomerComplaintMaster_tb",
                        principalColumn: "ComplaintID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Activity_tb_ActivityFieldID",
                table: "Activity_tb",
                column: "ActivityFieldID");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AVF_tb_AVFGroupId",
                table: "AVF_tb",
                column: "AVFGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_AVFGroup_tb_Lang",
                table: "AVFGroup_tb",
                column: "Lang");

            migrationBuilder.CreateIndex(
                name: "IX_AVFGroup_tb_ParentId",
                table: "AVFGroup_tb",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_City_tb_State_ID",
                table: "City_tb",
                column: "State_ID");

            migrationBuilder.CreateIndex(
                name: "IX_Color_tb_Lang",
                table: "Color_tb",
                column: "Lang");

            migrationBuilder.CreateIndex(
                name: "IX_Connection_tb_ConnectionGroupId",
                table: "Connection_tb",
                column: "ConnectionGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_ConnectionGroup_tb_Lang",
                table: "ConnectionGroup_tb",
                column: "Lang");

            migrationBuilder.CreateIndex(
                name: "IX_ContactUsMessage_tb_CMGId",
                table: "ContactUsMessage_tb",
                column: "CMGId");

            migrationBuilder.CreateIndex(
                name: "IX_ContactUsMessageGroup_tb_Lang",
                table: "ContactUsMessageGroup_tb",
                column: "Lang");

            migrationBuilder.CreateIndex(
                name: "IX_ContentFiles_tb_ContentId",
                table: "ContentFiles_tb",
                column: "ContentId");

            migrationBuilder.CreateIndex(
                name: "IX_Contents_tb_ContentGroupId",
                table: "Contents_tb",
                column: "ContentGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_ContentsComment_tb_ContentGroupId",
                table: "ContentsComment_tb",
                column: "ContentGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_ContentsComment_tb_ContentId",
                table: "ContentsComment_tb",
                column: "ContentId");

            migrationBuilder.CreateIndex(
                name: "IX_ContentsGroup_tb_Lang",
                table: "ContentsGroup_tb",
                column: "Lang");

            migrationBuilder.CreateIndex(
                name: "IX_ContentsTag_tb_ContentId",
                table: "ContentsTag_tb",
                column: "ContentId");

            migrationBuilder.CreateIndex(
                name: "IX_ContentsVotes_tb_ContentId",
                table: "ContentsVotes_tb",
                column: "ContentId");

            migrationBuilder.CreateIndex(
                name: "IX_Country_tb_Lang",
                table: "Country_tb",
                column: "Lang");

            migrationBuilder.CreateIndex(
                name: "IX_Customer_tb_UserIdentity_Id",
                table: "Customer_tb",
                column: "UserIdentity_Id",
                unique: true,
                filter: "[UserIdentity_Id] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerAddress_tb_City_Id",
                table: "CustomerAddress_tb",
                column: "City_Id");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerAddress_tb_Country_Id",
                table: "CustomerAddress_tb",
                column: "Country_Id");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerAddress_tb_Customer_Id",
                table: "CustomerAddress_tb",
                column: "Customer_Id");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerAddress_tb_State_Id",
                table: "CustomerAddress_tb",
                column: "State_Id");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerComplaintFiles_tb_ComplaintId",
                table: "CustomerComplaintFiles_tb",
                column: "ComplaintId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerComplaintMaster_tb_ProductId",
                table: "CustomerComplaintMaster_tb",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerSurveyProducts_tb_CustomerSurveyId",
                table: "CustomerSurveyProducts_tb",
                column: "CustomerSurveyId");

            migrationBuilder.CreateIndex(
                name: "IX_Emp_Activity_tb_ItemId",
                table: "Emp_Activity_tb",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_Emp_ComputerSkills_tb_ItemId",
                table: "Emp_ComputerSkills_tb",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_Emp_Documents_tb_Employement_Id",
                table: "Emp_Documents_tb",
                column: "Employement_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Emp_EducationalRecords_tb_Employment_Id",
                table: "Emp_EducationalRecords_tb",
                column: "Employment_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Emp_Guarantor_tb_Employement_Id",
                table: "Emp_Guarantor_tb",
                column: "Employement_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Emp_TraningCourse_tb_Employement_Id",
                table: "Emp_TraningCourse_tb",
                column: "Employement_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Emp_UnderSupervisor_tb_Employement_Id",
                table: "Emp_UnderSupervisor_tb",
                column: "Employement_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Emp_WorkExperience_tb_Employment_Id",
                table: "Emp_WorkExperience_tb",
                column: "Employment_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Employe_Post_PersonalCode",
                table: "Employe_Post",
                column: "PersonalCode");

            migrationBuilder.CreateIndex(
                name: "IX_Employe_Post_PostId",
                table: "Employe_Post",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_tb_OfficePostId",
                table: "Employee_tb",
                column: "OfficePostId");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_tb_OfficeUnitId",
                table: "Employee_tb",
                column: "OfficeUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_tb_UserIdentity_Id",
                table: "Employee_tb",
                column: "UserIdentity_Id",
                unique: true,
                filter: "[UserIdentity_Id] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeSalary_PersonalCode",
                table: "EmployeeSalary",
                column: "PersonalCode");

            migrationBuilder.CreateIndex(
                name: "IX_Factor_CustomerAddress_Id",
                table: "Factor",
                column: "CustomerAddress_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Factor_Customer_Id",
                table: "Factor",
                column: "Customer_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Factor_Lang_ID",
                table: "Factor",
                column: "Lang_ID");

            migrationBuilder.CreateIndex(
                name: "IX_Factor_SendType_ID",
                table: "Factor",
                column: "SendType_ID");

            migrationBuilder.CreateIndex(
                name: "IX_FactorDetails_Factor_Id",
                table: "FactorDetails",
                column: "Factor_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FactorDetails_Garanty_Id",
                table: "FactorDetails",
                column: "Garanty_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FactorDetails_ProductInfo_Id",
                table: "FactorDetails",
                column: "ProductInfo_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FAQ_tb_Lnag",
                table: "FAQ_tb",
                column: "Lnag");

            migrationBuilder.CreateIndex(
                name: "IX_Feature_tb_ProductGroupId",
                table: "Feature_tb",
                column: "ProductGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_FeatureReply_tb_FeatureId",
                table: "FeatureReply_tb",
                column: "FeatureId");

            migrationBuilder.CreateIndex(
                name: "IX_Gallery_tb_GalleryGroupId",
                table: "Gallery_tb",
                column: "GalleryGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_GalleryGroup_tb_Lang",
                table: "GalleryGroup_tb",
                column: "Lang");

            migrationBuilder.CreateIndex(
                name: "IX_GalleryPicture_tb_GalleryId",
                table: "GalleryPicture_tb",
                column: "GalleryId");

            migrationBuilder.CreateIndex(
                name: "IX_Garanty_tb_Lang",
                table: "Garanty_tb",
                column: "Lang");

            migrationBuilder.CreateIndex(
                name: "IX_MainProduct_tb_GalleryId",
                table: "MainProduct_tb",
                column: "GalleryId");

            migrationBuilder.CreateIndex(
                name: "IX_Mission_tb_ApplicantSignature",
                table: "Mission_tb",
                column: "ApplicantSignature");

            migrationBuilder.CreateIndex(
                name: "IX_Mission_tb_PersonalCode_1",
                table: "Mission_tb",
                column: "PersonalCode_1");

            migrationBuilder.CreateIndex(
                name: "IX_NewsLetter_tb_GroupId",
                table: "NewsLetter_tb",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_NewsLetterGroup_tb_Lang",
                table: "NewsLetterGroup_tb",
                column: "Lang");

            migrationBuilder.CreateIndex(
                name: "IX_Off_tb_Lang",
                table: "Off_tb",
                column: "Lang");

            migrationBuilder.CreateIndex(
                name: "IX_OfficePost_tb_ParentId",
                table: "OfficePost_tb",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_Producer_tb_Lang",
                table: "Producer_tb",
                column: "Lang");

            migrationBuilder.CreateIndex(
                name: "IX_ProductAdvantage_tb_ProductInfoId",
                table: "ProductAdvantage_tb",
                column: "ProductInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductColor_tb_ColorId",
                table: "ProductColor_tb",
                column: "ColorId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductColor_tb_ProductId",
                table: "ProductColor_tb",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductComment_tb_ProductGroupId",
                table: "ProductComment_tb",
                column: "ProductGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductComment_tb_ProductInforId",
                table: "ProductComment_tb",
                column: "ProductInforId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductGroup_tb_Lang",
                table: "ProductGroup_tb",
                column: "Lang");

            migrationBuilder.CreateIndex(
                name: "IX_ProductGroup_tb_OffId",
                table: "ProductGroup_tb",
                column: "OffId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductGroup_tb_ParentId",
                table: "ProductGroup_tb",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInfo_tb_OffId",
                table: "ProductInfo_tb",
                column: "OffId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInfo_tb_ProducerId",
                table: "ProductInfo_tb",
                column: "ProducerId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInfo_tb_ProductGroupId",
                table: "ProductInfo_tb",
                column: "ProductGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInfo_tb_ProductId",
                table: "ProductInfo_tb",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInfo_tb_UnitId",
                table: "ProductInfo_tb",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectGroup_tb_Lang",
                table: "ProjectGroup_tb",
                column: "Lang");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectGroup_tb_ParentId",
                table: "ProjectGroup_tb",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_tb_GalleryId",
                table: "Projects_tb",
                column: "GalleryId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_tb_GroupId",
                table: "Projects_tb",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_tb_ProducerId",
                table: "Projects_tb",
                column: "ProducerId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectsAdvantage_tb_ProductInfoId",
                table: "ProjectsAdvantage_tb",
                column: "ProductInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectVideo_tb_ProjectId",
                table: "ProjectVideo_tb",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectVideo_tb_VideoId",
                table: "ProjectVideo_tb",
                column: "VideoId");

            migrationBuilder.CreateIndex(
                name: "IX_ProposalFiles_tb_ProposalId",
                table: "ProposalFiles_tb",
                column: "ProposalId");

            migrationBuilder.CreateIndex(
                name: "IX_ProposalProviders_tb_OfficePostID",
                table: "ProposalProviders_tb",
                column: "OfficePostID");

            migrationBuilder.CreateIndex(
                name: "IX_ProposalProviders_tb_OfficeUnitID",
                table: "ProposalProviders_tb",
                column: "OfficeUnitID");

            migrationBuilder.CreateIndex(
                name: "IX_ProposalProviders_tb_PersonalCode",
                table: "ProposalProviders_tb",
                column: "PersonalCode");

            migrationBuilder.CreateIndex(
                name: "IX_ProposalProviders_tb_ProposalId",
                table: "ProposalProviders_tb",
                column: "ProposalId");

            migrationBuilder.CreateIndex(
                name: "IX_RateOption_tb_ProjectId",
                table: "RateOption_tb",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_SendType_tb_Lang_ID",
                table: "SendType_tb",
                column: "Lang_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SimilarProduct_tb_ProductInfo_ID",
                table: "SimilarProduct_tb",
                column: "ProductInfo_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SimilarProduct_tb_SimilarProduct_ID",
                table: "SimilarProduct_tb",
                column: "SimilarProduct_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SlideShow_tb_SSGId",
                table: "SlideShow_tb",
                column: "SSGId");

            migrationBuilder.CreateIndex(
                name: "IX_SlideShowGroup_tb_Lnag",
                table: "SlideShowGroup_tb",
                column: "Lnag");

            migrationBuilder.CreateIndex(
                name: "IX_State_tb_CountryId",
                table: "State_tb",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_SuppliersActivityFields_tb_ActivityId",
                table: "SuppliersActivityFields_tb",
                column: "ActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_SuppliersActivityFields_tb_SupplierId",
                table: "SuppliersActivityFields_tb",
                column: "SupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_SuppliersCertificates_tb_SupplierId",
                table: "SuppliersCertificates_tb",
                column: "SupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_SuppliersFiles_tb_FileTypeId",
                table: "SuppliersFiles_tb",
                column: "FileTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_SuppliersFiles_tb_SupplierId",
                table: "SuppliersFiles_tb",
                column: "SupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_SupplierShareholders_tb_Suppliers_ID",
                table: "SupplierShareholders_tb",
                column: "Suppliers_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SuppliersResume_tb_SupplierId",
                table: "SuppliersResume_tb",
                column: "SupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_SurvayQuestionGroup_tb_MainSurveyId",
                table: "SurvayQuestionGroup_tb",
                column: "MainSurveyId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyCustomerOtherData_tb_MainSurveyId",
                table: "SurveyCustomerOtherData_tb",
                column: "MainSurveyId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyEmployeesOtherData_tb_MainSurveyId",
                table: "SurveyEmployeesOtherData_tb",
                column: "MainSurveyId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyEmployeesOtherData_tb_PersonalCode",
                table: "SurveyEmployeesOtherData_tb",
                column: "PersonalCode");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyQuestion_tb_AnswerTypeId",
                table: "SurveyQuestion_tb",
                column: "AnswerTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyQuestion_tb_QuestionGroupId",
                table: "SurveyQuestion_tb",
                column: "QuestionGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyQuestionReply_tb_QuestionId",
                table: "SurveyQuestionReply_tb",
                column: "QuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveySuppliersOtherData_tb_MainSurveyId",
                table: "SurveySuppliersOtherData_tb",
                column: "MainSurveyId");

            migrationBuilder.CreateIndex(
                name: "IX_Unit_tb_Lang",
                table: "Unit_tb",
                column: "Lang");

            migrationBuilder.CreateIndex(
                name: "IX_Vacation_tb_PersonalCode_3",
                table: "Vacation_tb",
                column: "PersonalCode_3");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Catalog_tb");

            migrationBuilder.DropTable(
                name: "Connection_tb");

            migrationBuilder.DropTable(
                name: "ContactUsMessage_tb");

            migrationBuilder.DropTable(
                name: "ContentFiles_tb");

            migrationBuilder.DropTable(
                name: "ContentsComment_tb");

            migrationBuilder.DropTable(
                name: "ContentsGallery_tb");

            migrationBuilder.DropTable(
                name: "ContentsTag_tb");

            migrationBuilder.DropTable(
                name: "ContentsVotes_tb");

            migrationBuilder.DropTable(
                name: "CoWorker_tb");

            migrationBuilder.DropTable(
                name: "CustomerComplaintFiles_tb");

            migrationBuilder.DropTable(
                name: "CustomerSurveyProducts_tb");

            migrationBuilder.DropTable(
                name: "Emp_Activity_tb");

            migrationBuilder.DropTable(
                name: "Emp_ComputerSkills_tb");

            migrationBuilder.DropTable(
                name: "Emp_Documents_tb");

            migrationBuilder.DropTable(
                name: "Emp_EducationalRecords_tb");

            migrationBuilder.DropTable(
                name: "Emp_Guarantor_tb");

            migrationBuilder.DropTable(
                name: "Emp_TraningCourse_tb");

            migrationBuilder.DropTable(
                name: "Emp_UnderSupervisor_tb");

            migrationBuilder.DropTable(
                name: "Emp_WorkExperience_tb");

            migrationBuilder.DropTable(
                name: "Employe_Post");

            migrationBuilder.DropTable(
                name: "EmployeeSalary");

            migrationBuilder.DropTable(
                name: "FactorDetails");

            migrationBuilder.DropTable(
                name: "FAQ_tb");

            migrationBuilder.DropTable(
                name: "GalleryPicture_tb");

            migrationBuilder.DropTable(
                name: "HalycoManager");

            migrationBuilder.DropTable(
                name: "Mission_tb");

            migrationBuilder.DropTable(
                name: "NewsLetter_tb");

            migrationBuilder.DropTable(
                name: "ProductAdvantage_tb");

            migrationBuilder.DropTable(
                name: "ProductColor_tb");

            migrationBuilder.DropTable(
                name: "ProductComment_tb");

            migrationBuilder.DropTable(
                name: "ProductFeature_tb");

            migrationBuilder.DropTable(
                name: "ProductGaranty_tb");

            migrationBuilder.DropTable(
                name: "ProjectsAdvantage_tb");

            migrationBuilder.DropTable(
                name: "ProjectVideo_tb");

            migrationBuilder.DropTable(
                name: "ProposalFiles_tb");

            migrationBuilder.DropTable(
                name: "ProposalProviders_tb");

            migrationBuilder.DropTable(
                name: "RateOption_tb");

            migrationBuilder.DropTable(
                name: "SeoPage_tb");

            migrationBuilder.DropTable(
                name: "SeoPageProduct_tb");

            migrationBuilder.DropTable(
                name: "SeoStaticPage_tb");

            migrationBuilder.DropTable(
                name: "SimilarProduct_tb");

            migrationBuilder.DropTable(
                name: "SlideShow_tb");

            migrationBuilder.DropTable(
                name: "SMS_tb");

            migrationBuilder.DropTable(
                name: "SocialNetworks_tb");

            migrationBuilder.DropTable(
                name: "SuppliersActivityFields_tb");

            migrationBuilder.DropTable(
                name: "SuppliersCertificates_tb");

            migrationBuilder.DropTable(
                name: "SuppliersFiles_tb");

            migrationBuilder.DropTable(
                name: "SupplierShareholders_tb");

            migrationBuilder.DropTable(
                name: "SuppliersResume_tb");

            migrationBuilder.DropTable(
                name: "SurveyEmployeesOtherData_tb");

            migrationBuilder.DropTable(
                name: "SurveyQuestionReply_tb");

            migrationBuilder.DropTable(
                name: "SurveySuppliersOtherData_tb");

            migrationBuilder.DropTable(
                name: "Vacation_tb");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "ConnectionGroup_tb");

            migrationBuilder.DropTable(
                name: "ContactUsMessageGroup_tb");

            migrationBuilder.DropTable(
                name: "Contents_tb");

            migrationBuilder.DropTable(
                name: "CustomerComplaintMaster_tb");

            migrationBuilder.DropTable(
                name: "SurveyCustomerOtherData_tb");

            migrationBuilder.DropTable(
                name: "Emp_ActivityItems_tb");

            migrationBuilder.DropTable(
                name: "Emp_ComputerSkillItems_tb");

            migrationBuilder.DropTable(
                name: "Emp_BaseInformation_tb");

            migrationBuilder.DropTable(
                name: "Factor");

            migrationBuilder.DropTable(
                name: "NewsLetterGroup_tb");

            migrationBuilder.DropTable(
                name: "Color_tb");

            migrationBuilder.DropTable(
                name: "FeatureReply_tb");

            migrationBuilder.DropTable(
                name: "Garanty_tb");

            migrationBuilder.DropTable(
                name: "AVF_tb");

            migrationBuilder.DropTable(
                name: "MainProposal_tb");

            migrationBuilder.DropTable(
                name: "Projects_tb");

            migrationBuilder.DropTable(
                name: "SlideShowGroup_tb");

            migrationBuilder.DropTable(
                name: "Activity_tb");

            migrationBuilder.DropTable(
                name: "SuppliersFileType_tb");

            migrationBuilder.DropTable(
                name: "MainSuppliers_tb");

            migrationBuilder.DropTable(
                name: "SurveyQuestion_tb");

            migrationBuilder.DropTable(
                name: "Employee_tb");

            migrationBuilder.DropTable(
                name: "ContentsGroup_tb");

            migrationBuilder.DropTable(
                name: "ProductInfo_tb");

            migrationBuilder.DropTable(
                name: "CustomerAddress_tb");

            migrationBuilder.DropTable(
                name: "SendType_tb");

            migrationBuilder.DropTable(
                name: "Feature_tb");

            migrationBuilder.DropTable(
                name: "AVFGroup_tb");

            migrationBuilder.DropTable(
                name: "ProjectGroup_tb");

            migrationBuilder.DropTable(
                name: "ActivityField_tb");

            migrationBuilder.DropTable(
                name: "SurveyAnswerType_tb");

            migrationBuilder.DropTable(
                name: "SurvayQuestionGroup_tb");

            migrationBuilder.DropTable(
                name: "OfficePost_tb");

            migrationBuilder.DropTable(
                name: "OfficeUnit_tb");

            migrationBuilder.DropTable(
                name: "Producer_tb");

            migrationBuilder.DropTable(
                name: "MainProduct_tb");

            migrationBuilder.DropTable(
                name: "Unit_tb");

            migrationBuilder.DropTable(
                name: "City_tb");

            migrationBuilder.DropTable(
                name: "Customer_tb");

            migrationBuilder.DropTable(
                name: "ProductGroup_tb");

            migrationBuilder.DropTable(
                name: "MainSurvey_tb");

            migrationBuilder.DropTable(
                name: "Gallery_tb");

            migrationBuilder.DropTable(
                name: "State_tb");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Off_tb");

            migrationBuilder.DropTable(
                name: "GalleryGroup_tb");

            migrationBuilder.DropTable(
                name: "Country_tb");

            migrationBuilder.DropTable(
                name: "Language_tb");
        }
    }
}
