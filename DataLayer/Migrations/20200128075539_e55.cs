﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e55 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SuppliersFiles_tb_SuppliersFileType_tb_FileTypeId",
                table: "SuppliersFiles_tb");

            migrationBuilder.DropTable(
                name: "SuppliersFileType_tb");

            migrationBuilder.DropIndex(
                name: "IX_SuppliersFiles_tb_FileTypeId",
                table: "SuppliersFiles_tb");

            migrationBuilder.DropColumn(
                name: "FileTypeId",
                table: "SuppliersFiles_tb");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte>(
                name: "FileTypeId",
                table: "SuppliersFiles_tb",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.CreateTable(
                name: "SuppliersFileType_tb",
                columns: table => new
                {
                    FileType_ID = table.Column<byte>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuppliersFileType_tb", x => x.FileType_ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SuppliersFiles_tb_FileTypeId",
                table: "SuppliersFiles_tb",
                column: "FileTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_SuppliersFiles_tb_SuppliersFileType_tb_FileTypeId",
                table: "SuppliersFiles_tb",
                column: "FileTypeId",
                principalTable: "SuppliersFileType_tb",
                principalColumn: "FileType_ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
