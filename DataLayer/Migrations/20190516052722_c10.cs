﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class c10 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MainPageSeo_tb",
                columns: table => new
                {
                    SeoPage_ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PageId = table.Column<int>(nullable: false),
                    MetaTitle = table.Column<string>(maxLength: 200, nullable: true),
                    MetaKeyword = table.Column<string>(maxLength: 300, nullable: true),
                    MetaDescription = table.Column<string>(maxLength: 500, nullable: true),
                    MetaOther = table.Column<string>(maxLength: 500, nullable: true),
                    PageType = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MainPageSeo_tb", x => x.SeoPage_ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MainPageSeo_tb");
        }
    }
}
