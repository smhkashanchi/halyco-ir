﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class EditShiftChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_shiftChange_tb_Employee_tb_PersonalCode_HRmanager",
                table: "shiftChange_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_shiftChange_tb_Employee_tb_PersonalCode_manager",
                table: "shiftChange_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_shiftChange_tb_Employee_tb_PersonalCode_superior",
                table: "shiftChange_tb");

            migrationBuilder.DropIndex(
                name: "IX_shiftChange_tb_PersonalCode_HRmanager",
                table: "shiftChange_tb");

            migrationBuilder.DropIndex(
                name: "IX_shiftChange_tb_PersonalCode_manager",
                table: "shiftChange_tb");

            migrationBuilder.DropIndex(
                name: "IX_shiftChange_tb_PersonalCode_superior",
                table: "shiftChange_tb");

            migrationBuilder.DropColumn(
                name: "HRManagerDateTime",
                table: "shiftChange_tb");

            migrationBuilder.DropColumn(
                name: "HRManagerReason",
                table: "shiftChange_tb");

            migrationBuilder.DropColumn(
                name: "ManagerDateTime",
                table: "shiftChange_tb");

            migrationBuilder.DropColumn(
                name: "ManagerReason",
                table: "shiftChange_tb");

            migrationBuilder.DropColumn(
                name: "PersonalCode_HRmanager",
                table: "shiftChange_tb");

            migrationBuilder.DropColumn(
                name: "PersonalCode_manager",
                table: "shiftChange_tb");

            migrationBuilder.DropColumn(
                name: "PersonalCode_superior",
                table: "shiftChange_tb");

            migrationBuilder.DropColumn(
                name: "StatusHRManagerSignature",
                table: "shiftChange_tb");

            migrationBuilder.DropColumn(
                name: "StatusManagerSignature",
                table: "shiftChange_tb");

            migrationBuilder.DropColumn(
                name: "SuperiorDateTime",
                table: "shiftChange_tb");

            migrationBuilder.DropColumn(
                name: "SuperiorReason",
                table: "shiftChange_tb");

            migrationBuilder.RenameColumn(
                name: "StatusSuperiorSignature",
                table: "shiftChange_tb",
                newName: "FinalStatus");

            migrationBuilder.AlterColumn<DateTime>(
                name: "AlternateDateTime",
                table: "shiftChange_tb",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "shiftChangeSignature_tb",
                columns: table => new
                {
                    ShiftChangeSignature_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ShiftChange_ID = table.Column<int>(nullable: false),
                    PersonalCodeSignaturer = table.Column<string>(maxLength: 450, nullable: false),
                    SignatureStatus = table.Column<bool>(nullable: true),
                    SignatureReason = table.Column<string>(maxLength: 1000, nullable: true),
                    SignatureDateTime = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_shiftChangeSignature_tb", x => x.ShiftChangeSignature_ID);
                    table.ForeignKey(
                        name: "FK_shiftChangeSignature_tb_AspNetUsers_PersonalCodeSignaturer",
                        column: x => x.PersonalCodeSignaturer,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_shiftChangeSignature_tb_shiftChange_tb_ShiftChange_ID",
                        column: x => x.ShiftChange_ID,
                        principalTable: "shiftChange_tb",
                        principalColumn: "ShiftChange_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_shiftChangeSignature_tb_PersonalCodeSignaturer",
                table: "shiftChangeSignature_tb",
                column: "PersonalCodeSignaturer");

            migrationBuilder.CreateIndex(
                name: "IX_shiftChangeSignature_tb_ShiftChange_ID",
                table: "shiftChangeSignature_tb",
                column: "ShiftChange_ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "shiftChangeSignature_tb");

            migrationBuilder.RenameColumn(
                name: "FinalStatus",
                table: "shiftChange_tb",
                newName: "StatusSuperiorSignature");

            migrationBuilder.AlterColumn<DateTime>(
                name: "AlternateDateTime",
                table: "shiftChange_tb",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AddColumn<DateTime>(
                name: "HRManagerDateTime",
                table: "shiftChange_tb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HRManagerReason",
                table: "shiftChange_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ManagerDateTime",
                table: "shiftChange_tb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ManagerReason",
                table: "shiftChange_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PersonalCode_HRmanager",
                table: "shiftChange_tb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PersonalCode_manager",
                table: "shiftChange_tb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PersonalCode_superior",
                table: "shiftChange_tb",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "StatusHRManagerSignature",
                table: "shiftChange_tb",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "StatusManagerSignature",
                table: "shiftChange_tb",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "SuperiorDateTime",
                table: "shiftChange_tb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SuperiorReason",
                table: "shiftChange_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_shiftChange_tb_PersonalCode_HRmanager",
                table: "shiftChange_tb",
                column: "PersonalCode_HRmanager");

            migrationBuilder.CreateIndex(
                name: "IX_shiftChange_tb_PersonalCode_manager",
                table: "shiftChange_tb",
                column: "PersonalCode_manager");

            migrationBuilder.CreateIndex(
                name: "IX_shiftChange_tb_PersonalCode_superior",
                table: "shiftChange_tb",
                column: "PersonalCode_superior");

            migrationBuilder.AddForeignKey(
                name: "FK_shiftChange_tb_Employee_tb_PersonalCode_HRmanager",
                table: "shiftChange_tb",
                column: "PersonalCode_HRmanager",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_shiftChange_tb_Employee_tb_PersonalCode_manager",
                table: "shiftChange_tb",
                column: "PersonalCode_manager",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_shiftChange_tb_Employee_tb_PersonalCode_superior",
                table: "shiftChange_tb",
                column: "PersonalCode_superior",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
