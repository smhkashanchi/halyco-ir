﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e29 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "ApplicantSignature",
                table: "Vacation_tb",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 10,
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Vacation_tb_AdministrativeAffairsSignature",
                table: "Vacation_tb",
                column: "AdministrativeAffairsSignature");

            migrationBuilder.CreateIndex(
                name: "IX_Vacation_tb_AlternateSignature",
                table: "Vacation_tb",
                column: "AlternateSignature");

            migrationBuilder.CreateIndex(
                name: "IX_Vacation_tb_UnitsSupervisorSignature",
                table: "Vacation_tb",
                column: "UnitsSupervisorSignature");

            migrationBuilder.AddForeignKey(
                name: "FK_Vacation_tb_Employee_tb_AdministrativeAffairsSignature",
                table: "Vacation_tb",
                column: "AdministrativeAffairsSignature",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Vacation_tb_Employee_tb_AlternateSignature",
                table: "Vacation_tb",
                column: "AlternateSignature",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Vacation_tb_Employee_tb_UnitsSupervisorSignature",
                table: "Vacation_tb",
                column: "UnitsSupervisorSignature",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vacation_tb_Employee_tb_AdministrativeAffairsSignature",
                table: "Vacation_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_Vacation_tb_Employee_tb_AlternateSignature",
                table: "Vacation_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_Vacation_tb_Employee_tb_UnitsSupervisorSignature",
                table: "Vacation_tb");

            migrationBuilder.DropIndex(
                name: "IX_Vacation_tb_AdministrativeAffairsSignature",
                table: "Vacation_tb");

            migrationBuilder.DropIndex(
                name: "IX_Vacation_tb_AlternateSignature",
                table: "Vacation_tb");

            migrationBuilder.DropIndex(
                name: "IX_Vacation_tb_UnitsSupervisorSignature",
                table: "Vacation_tb");

            migrationBuilder.AlterColumn<string>(
                name: "ApplicantSignature",
                table: "Vacation_tb",
                maxLength: 10,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
