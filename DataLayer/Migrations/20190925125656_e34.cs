﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e34 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_shiftChange_tb_OfficeUnit_tb_UnitID_2",
                table: "shiftChange_tb");

            migrationBuilder.DropIndex(
                name: "IX_shiftChange_tb_UnitID_2",
                table: "shiftChange_tb");

            migrationBuilder.DropColumn(
                name: "UnitID_2",
                table: "shiftChange_tb");

            migrationBuilder.AddColumn<string>(
                name: "AlternateSignatureReason",
                table: "shiftChange_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "StatusAlternateSignature",
                table: "shiftChange_tb",
                nullable: false,
                defaultValue: (byte)0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AlternateSignatureReason",
                table: "shiftChange_tb");

            migrationBuilder.DropColumn(
                name: "StatusAlternateSignature",
                table: "shiftChange_tb");

            migrationBuilder.AddColumn<short>(
                name: "UnitID_2",
                table: "shiftChange_tb",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.CreateIndex(
                name: "IX_shiftChange_tb_UnitID_2",
                table: "shiftChange_tb",
                column: "UnitID_2");

            migrationBuilder.AddForeignKey(
                name: "FK_shiftChange_tb_OfficeUnit_tb_UnitID_2",
                table: "shiftChange_tb",
                column: "UnitID_2",
                principalTable: "OfficeUnit_tb",
                principalColumn: "OfficeUnitID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
