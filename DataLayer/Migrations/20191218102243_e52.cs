﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e52 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "AlternateDateTime",
                table: "Vacation_tb",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "HRManagerDateTime",
                table: "Vacation_tb",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ManagerDateTime",
                table: "Vacation_tb",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "SuperiorDateTime",
                table: "Vacation_tb",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "AlternateDateTime",
                table: "shiftChange_tb",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "HRManagerDateTime",
                table: "shiftChange_tb",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ManagerDateTime",
                table: "shiftChange_tb",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "SuperiorDateTime",
                table: "shiftChange_tb",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "HRManagerDateTime",
                table: "loan_tb",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ManagerDateTime",
                table: "loan_tb",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "SuperiorDateTime",
                table: "loan_tb",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AlternateDateTime",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "HRManagerDateTime",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "ManagerDateTime",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "SuperiorDateTime",
                table: "Vacation_tb");

            migrationBuilder.DropColumn(
                name: "AlternateDateTime",
                table: "shiftChange_tb");

            migrationBuilder.DropColumn(
                name: "HRManagerDateTime",
                table: "shiftChange_tb");

            migrationBuilder.DropColumn(
                name: "ManagerDateTime",
                table: "shiftChange_tb");

            migrationBuilder.DropColumn(
                name: "SuperiorDateTime",
                table: "shiftChange_tb");

            migrationBuilder.DropColumn(
                name: "HRManagerDateTime",
                table: "loan_tb");

            migrationBuilder.DropColumn(
                name: "ManagerDateTime",
                table: "loan_tb");

            migrationBuilder.DropColumn(
                name: "SuperiorDateTime",
                table: "loan_tb");
        }
    }
}
