﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e41 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_shiftChange_tb_Employee_tb_PersonalCodeSecond",
                table: "shiftChange_tb");

            migrationBuilder.AlterColumn<string>(
                name: "PersonalCodeSecond",
                table: "shiftChange_tb",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddForeignKey(
                name: "FK_shiftChange_tb_Employee_tb_PersonalCodeSecond",
                table: "shiftChange_tb",
                column: "PersonalCodeSecond",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_shiftChange_tb_Employee_tb_PersonalCodeSecond",
                table: "shiftChange_tb");

            migrationBuilder.AlterColumn<string>(
                name: "PersonalCodeSecond",
                table: "shiftChange_tb",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_shiftChange_tb_Employee_tb_PersonalCodeSecond",
                table: "shiftChange_tb",
                column: "PersonalCodeSecond",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
