﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e58 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "SY_KE_VamMandeh5",
                table: "EmployeeSalary",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<long>(
                name: "SY_KE_VamMandeh4",
                table: "EmployeeSalary",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<long>(
                name: "SY_KE_VamMandeh3",
                table: "EmployeeSalary",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<long>(
                name: "SY_KE_VamMandeh2",
                table: "EmployeeSalary",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<long>(
                name: "SY_KE_VamMandeh1",
                table: "EmployeeSalary",
                nullable: false,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "SY_KE_VamMandeh5",
                table: "EmployeeSalary",
                nullable: false,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<int>(
                name: "SY_KE_VamMandeh4",
                table: "EmployeeSalary",
                nullable: false,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<int>(
                name: "SY_KE_VamMandeh3",
                table: "EmployeeSalary",
                nullable: false,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<int>(
                name: "SY_KE_VamMandeh2",
                table: "EmployeeSalary",
                nullable: false,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<int>(
                name: "SY_KE_VamMandeh1",
                table: "EmployeeSalary",
                nullable: false,
                oldClrType: typeof(long));
        }
    }
}
