﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e19 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "shiftChange_tb",
                columns: table => new
                {
                    ShiftChange_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PersonalCodeFirst = table.Column<string>(nullable: false),
                    PersonalCodeSecond = table.Column<string>(nullable: false),
                    UnitID_1 = table.Column<short>(nullable: false),
                    UnitID_2 = table.Column<short>(nullable: false),
                    ShiftName1 = table.Column<string>(maxLength: 50, nullable: false),
                    ShiftName2 = table.Column<string>(maxLength: 50, nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    shiftDate = table.Column<string>(maxLength: 50, nullable: false),
                    shiftReason = table.Column<string>(maxLength: 500, nullable: true),
                    PersonalCode_superior = table.Column<string>(nullable: false),
                    SuperiorReason = table.Column<string>(maxLength: 500, nullable: true),
                    StatusSuperiorSignature = table.Column<bool>(nullable: false),
                    PersonalCode_manager = table.Column<string>(nullable: false),
                    ManagerReason = table.Column<string>(maxLength: 500, nullable: true),
                    StatusManagerSignature = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_shiftChange_tb", x => x.ShiftChange_ID);
                    table.ForeignKey(
                        name: "FK_shiftChange_tb_Employee_tb_PersonalCodeFirst",
                        column: x => x.PersonalCodeFirst,
                        principalTable: "Employee_tb",
                        principalColumn: "PersonalCode",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_shiftChange_tb_Employee_tb_PersonalCodeSecond",
                        column: x => x.PersonalCodeSecond,
                        principalTable: "Employee_tb",
                        principalColumn: "PersonalCode",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_shiftChange_tb_Employee_tb_PersonalCode_manager",
                        column: x => x.PersonalCode_manager,
                        principalTable: "Employee_tb",
                        principalColumn: "PersonalCode",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_shiftChange_tb_Employee_tb_PersonalCode_superior",
                        column: x => x.PersonalCode_superior,
                        principalTable: "Employee_tb",
                        principalColumn: "PersonalCode",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_shiftChange_tb_OfficeUnit_tb_UnitID_1",
                        column: x => x.UnitID_1,
                        principalTable: "OfficeUnit_tb",
                        principalColumn: "OfficeUnitID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_shiftChange_tb_OfficeUnit_tb_UnitID_2",
                        column: x => x.UnitID_2,
                        principalTable: "OfficeUnit_tb",
                        principalColumn: "OfficeUnitID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_shiftChange_tb_PersonalCodeFirst",
                table: "shiftChange_tb",
                column: "PersonalCodeFirst");

            migrationBuilder.CreateIndex(
                name: "IX_shiftChange_tb_PersonalCodeSecond",
                table: "shiftChange_tb",
                column: "PersonalCodeSecond");

            migrationBuilder.CreateIndex(
                name: "IX_shiftChange_tb_PersonalCode_manager",
                table: "shiftChange_tb",
                column: "PersonalCode_manager");

            migrationBuilder.CreateIndex(
                name: "IX_shiftChange_tb_PersonalCode_superior",
                table: "shiftChange_tb",
                column: "PersonalCode_superior");

            migrationBuilder.CreateIndex(
                name: "IX_shiftChange_tb_UnitID_1",
                table: "shiftChange_tb",
                column: "UnitID_1");

            migrationBuilder.CreateIndex(
                name: "IX_shiftChange_tb_UnitID_2",
                table: "shiftChange_tb",
                column: "UnitID_2");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "shiftChange_tb");
        }
    }
}
