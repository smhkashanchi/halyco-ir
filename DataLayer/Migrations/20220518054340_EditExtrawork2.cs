﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class EditExtrawork2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExtraWork_tb_Employee_tb_PersonalCode",
                table: "ExtraWork_tb");

            migrationBuilder.DropIndex(
                name: "IX_ExtraWork_tb_PersonalCode",
                table: "ExtraWork_tb");

            migrationBuilder.DropColumn(
                name: "PersonalCode",
                table: "ExtraWork_tb");

            migrationBuilder.AlterColumn<string>(
                name: "PersonalCodeFirst",
                table: "ExtraWork_tb",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ExtraWork_tb_PersonalCodeFirst",
                table: "ExtraWork_tb",
                column: "PersonalCodeFirst");

            migrationBuilder.AddForeignKey(
                name: "FK_ExtraWork_tb_Employee_tb_PersonalCodeFirst",
                table: "ExtraWork_tb",
                column: "PersonalCodeFirst",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExtraWork_tb_Employee_tb_PersonalCodeFirst",
                table: "ExtraWork_tb");

            migrationBuilder.DropIndex(
                name: "IX_ExtraWork_tb_PersonalCodeFirst",
                table: "ExtraWork_tb");

            migrationBuilder.AlterColumn<string>(
                name: "PersonalCodeFirst",
                table: "ExtraWork_tb",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<string>(
                name: "PersonalCode",
                table: "ExtraWork_tb",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_ExtraWork_tb_PersonalCode",
                table: "ExtraWork_tb",
                column: "PersonalCode");

            migrationBuilder.AddForeignKey(
                name: "FK_ExtraWork_tb_Employee_tb_PersonalCode",
                table: "ExtraWork_tb",
                column: "PersonalCode",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
