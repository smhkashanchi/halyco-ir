﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e53 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "EndTime",
                table: "Mission_tb",
                maxLength: 10,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StartTime",
                table: "Mission_tb",
                maxLength: 10,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EndTime",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "StartTime",
                table: "Mission_tb");
        }
    }
}
