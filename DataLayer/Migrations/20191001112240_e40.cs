﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class e40 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Mission_tb_Employee_tb_ApplicantSignature",
                table: "Mission_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_Mission_tb_Employee_tb_PersonalCode_1",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "HumanResourcesSignature_1",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "Human_Status",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "Issue",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "ManagerSignature_1",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "Manager_Status",
                table: "Mission_tb");

            migrationBuilder.RenameColumn(
                name: "PersonalCode_1",
                table: "Mission_tb",
                newName: "PersonalCode");

            migrationBuilder.RenameColumn(
                name: "IssueDate",
                table: "Mission_tb",
                newName: "PersonalCode_superior");

            migrationBuilder.RenameColumn(
                name: "ApplicantSignature",
                table: "Mission_tb",
                newName: "PersonalCode_manager");

            migrationBuilder.RenameIndex(
                name: "IX_Mission_tb_PersonalCode_1",
                table: "Mission_tb",
                newName: "IX_Mission_tb_PersonalCode");

            migrationBuilder.RenameIndex(
                name: "IX_Mission_tb_ApplicantSignature",
                table: "Mission_tb",
                newName: "IX_Mission_tb_PersonalCode_manager");

            migrationBuilder.AlterColumn<string>(
                name: "Reason",
                table: "Mission_tb",
                maxLength: 1000,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 250);

            migrationBuilder.AlterColumn<string>(
                name: "Destination",
                table: "Mission_tb",
                maxLength: 500,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 200,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PersonalCode_superior",
                table: "Mission_tb",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HRManagerReason",
                table: "Mission_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ManagerReason",
                table: "Mission_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MissionSubject",
                table: "Mission_tb",
                maxLength: 500,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "PersonalCode_HRmanager",
                table: "Mission_tb",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "StatusHRManagerSignature",
                table: "Mission_tb",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "StatusManagerSignature",
                table: "Mission_tb",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "StatusSuperiorSignature",
                table: "Mission_tb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SuperiorReason",
                table: "Mission_tb",
                maxLength: 500,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Mission_tb_PersonalCode_HRmanager",
                table: "Mission_tb",
                column: "PersonalCode_HRmanager");

            migrationBuilder.CreateIndex(
                name: "IX_Mission_tb_PersonalCode_superior",
                table: "Mission_tb",
                column: "PersonalCode_superior");

            migrationBuilder.AddForeignKey(
                name: "FK_Mission_tb_Employee_tb_PersonalCode",
                table: "Mission_tb",
                column: "PersonalCode",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Mission_tb_Employee_tb_PersonalCode_HRmanager",
                table: "Mission_tb",
                column: "PersonalCode_HRmanager",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Mission_tb_Employee_tb_PersonalCode_manager",
                table: "Mission_tb",
                column: "PersonalCode_manager",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Mission_tb_Employee_tb_PersonalCode_superior",
                table: "Mission_tb",
                column: "PersonalCode_superior",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Mission_tb_Employee_tb_PersonalCode",
                table: "Mission_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_Mission_tb_Employee_tb_PersonalCode_HRmanager",
                table: "Mission_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_Mission_tb_Employee_tb_PersonalCode_manager",
                table: "Mission_tb");

            migrationBuilder.DropForeignKey(
                name: "FK_Mission_tb_Employee_tb_PersonalCode_superior",
                table: "Mission_tb");

            migrationBuilder.DropIndex(
                name: "IX_Mission_tb_PersonalCode_HRmanager",
                table: "Mission_tb");

            migrationBuilder.DropIndex(
                name: "IX_Mission_tb_PersonalCode_superior",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "HRManagerReason",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "ManagerReason",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "MissionSubject",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "PersonalCode_HRmanager",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "StatusHRManagerSignature",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "StatusManagerSignature",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "StatusSuperiorSignature",
                table: "Mission_tb");

            migrationBuilder.DropColumn(
                name: "SuperiorReason",
                table: "Mission_tb");

            migrationBuilder.RenameColumn(
                name: "PersonalCode_superior",
                table: "Mission_tb",
                newName: "IssueDate");

            migrationBuilder.RenameColumn(
                name: "PersonalCode_manager",
                table: "Mission_tb",
                newName: "ApplicantSignature");

            migrationBuilder.RenameColumn(
                name: "PersonalCode",
                table: "Mission_tb",
                newName: "PersonalCode_1");

            migrationBuilder.RenameIndex(
                name: "IX_Mission_tb_PersonalCode_manager",
                table: "Mission_tb",
                newName: "IX_Mission_tb_ApplicantSignature");

            migrationBuilder.RenameIndex(
                name: "IX_Mission_tb_PersonalCode",
                table: "Mission_tb",
                newName: "IX_Mission_tb_PersonalCode_1");

            migrationBuilder.AlterColumn<string>(
                name: "Reason",
                table: "Mission_tb",
                maxLength: 250,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 1000);

            migrationBuilder.AlterColumn<string>(
                name: "Destination",
                table: "Mission_tb",
                maxLength: 200,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 500,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "IssueDate",
                table: "Mission_tb",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HumanResourcesSignature_1",
                table: "Mission_tb",
                maxLength: 10,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Human_Status",
                table: "Mission_tb",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Issue",
                table: "Mission_tb",
                maxLength: 900,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ManagerSignature_1",
                table: "Mission_tb",
                maxLength: 10,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Manager_Status",
                table: "Mission_tb",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddForeignKey(
                name: "FK_Mission_tb_Employee_tb_ApplicantSignature",
                table: "Mission_tb",
                column: "ApplicantSignature",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Mission_tb_Employee_tb_PersonalCode_1",
                table: "Mission_tb",
                column: "PersonalCode_1",
                principalTable: "Employee_tb",
                principalColumn: "PersonalCode",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
