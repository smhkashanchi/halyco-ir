﻿    using DataLayer;
using DomainClasses;
using DomainClasses.Connection;
using DomainClasses.Customer;
using DomainClasses.Language;
using DomainClasses.Role;
using DomainClasses.SlideShow;
using DomainClasses.User;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.NewsLetters;
using DomainClasses.Contents;
using DomainClasses.Product;
using DomainClasses.AVF;

namespace DataLayer
{
    public class DbContextSeedData
    {
        public static string User_Id { get; set; }
        public static void SeedData
(UserManager<ApplicationUser> userManager,
RoleManager<ApplicationRole> roleManager, ApplicationDbContext coText
            )
        {
            SeedLanguage(coText);
            SeedRoles(roleManager);
            SeedUsers(userManager);
            SeedCompanyUser(coText);
            SeedConnectionGroup(coText);
            SeedSlideShowGroup(coText);
            SeedNewsLetterGroup(coText);
            SeedColor(coText);
            SeedAboutUs(coText);
            SeedAVFGruop(coText);
        }

        public static void SeedUsers
        (UserManager<ApplicationUser> userManager)
        {
            var user1 = userManager.FindByIdAsync("Admin@yahoo.com").Result;
            var roles = userManager.GetUsersInRoleAsync("Admin");
            if (roles.Result.Count == 0)
            {
                var user = new ApplicationUser()
                {
                    UserName = "admin@yahoo.com",
                    Email = "admin@yahoo.com",
                    EmailConfirmed = true,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    FullName = "مهدی الهی",
                    Mobile_1 = "09104609932",
                    Mobile_2 = null,
                    UserPhoto = "/UploadFile/UsersPhoto/no-profile.jpg",
                    PhoneNumber = "03155301817"

                };
                IdentityResult result = userManager.CreateAsync
                (user, "User@1").Result;

                if (result.Succeeded)
                {
                    User_Id = user.Id;
                    userManager.AddToRoleAsync(user, "Admin").Wait();

                }
            }

        }
        public static void SeedRoles
(RoleManager<ApplicationRole> roleManager)
        {

            if (!roleManager.RoleExistsAsync
                   ("Admin").Result)
            {
                ApplicationRole role = new ApplicationRole();
                role.Name = "Admin";
                role.Access = "[{'Id':':Account','Name':'Account','DisplayName':null,'AreaName':null,'Actions':[{'Id':':Account: Logout','Name':'Logout','DisplayName':null,'ControllerId':':Account'}]},{'Id':':CmsFeatures','Name':'CmsFeatures','DisplayName':null,'AreaName':null,'Actions':[{'Id':':CmsFeatures: Index','Name':'Index','DisplayName':null,'ControllerId':':CmsFeatures'},{'Id':':CmsFeatures: Details','Name':'Details','DisplayName':null,'ControllerId':':CmsFeatures'},{'Id':':CmsFeatures: Create','Name':'Create','DisplayName':null,'ControllerId':':CmsFeatures'},{'Id':':CmsFeatures: Edit','Name':'Edit','DisplayName':null,'ControllerId':':CmsFeatures'},{'Id':':CmsFeatures: Delete','Name':'Delete','DisplayName':null,'ControllerId':':CmsFeatures'}]},{'Id':':CompanyUsers','Name':'CompanyUsers','DisplayName':null,'AreaName':null,'Actions':[{'Id':':CompanyUsers: Index','Name':'Index','DisplayName':null,'ControllerId':':CompanyUsers'},{'Id':':CompanyUsers: Details','Name':'Details','DisplayName':null,'ControllerId':':CompanyUsers'},{'Id':':CompanyUsers: Create','Name':'Create','DisplayName':null,'ControllerId':':CompanyUsers'},{'Id':':CompanyUsers: Edit','Name':'Edit','DisplayName':null,'ControllerId':':CompanyUsers'},{'Id':':CompanyUsers: Delete','Name':'Delete','DisplayName':null,'ControllerId':':CompanyUsers'}]},{'Id':':Customers','Name':'Customers','DisplayName':null,'AreaName':null,'Actions':[{'Id':':Customers: Index','Name':'Index','DisplayName':null,'ControllerId':':Customers'},{'Id':':Customers: Details','Name':'Details','DisplayName':null,'ControllerId':':Customers'},{'Id':':Customers: Create','Name':'Create','DisplayName':null,'ControllerId':':Customers'},{'Id':':Customers: Edit','Name':'Edit','DisplayName':null,'ControllerId':':Customers'},{'Id':':Customers: Delete','Name':'Delete','DisplayName':null,'ControllerId':':Customers'}]},{'Id':':Departments','Name':'Departments','DisplayName':null,'AreaName':null,'Actions':[{'Id':':Departments: Index','Name':'Index','DisplayName':null,'ControllerId':':Departments'},{'Id':':Departments: Details','Name':'Details','DisplayName':null,'ControllerId':':Departments'},{'Id':':Departments: Create','Name':'Create','DisplayName':null,'ControllerId':':Departments'},{'Id':':Departments: Edit','Name':'Edit','DisplayName':null,'ControllerId':':Departments'},{'Id':':Departments: Delete','Name':'Delete','DisplayName':null,'ControllerId':':Departments'},{'Id':':Departments: DeleteMultiSelect','Name':'DeleteMultiSelect','DisplayName':null,'ControllerId':':Departments'}]},{'Id':':DepartmentUsers','Name':'DepartmentUsers','DisplayName':null,'AreaName':null,'Actions':[{'Id':':DepartmentUsers: Index','Name':'Index','DisplayName':null,'ControllerId':':DepartmentUsers'},{'Id':':DepartmentUsers: Create','Name':'Create','DisplayName':null,'ControllerId':':DepartmentUsers'},{'Id':':DepartmentUsers: Delete','Name':'Delete','DisplayName':null,'ControllerId':':DepartmentUsers'}]},{'Id':':Home','Name':'Home','DisplayName':null,'AreaName':null,'Actions':[{'Id':':Home: Index','Name':'Index','DisplayName':null,'ControllerId':':Home'}]},{'Id':':Manage','Name':'Manage','DisplayName':null,'AreaName':null,'Actions':[{'Id':':Manage: Index','Name':'Index','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: SendVerificationEmail','Name':'SendVerificationEmail','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: ChangePassword','Name':'ChangePassword','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: ChangeUserName','Name':'ChangeUserName','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: SetPassword','Name':'SetPassword','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: ExternalLogins','Name':'ExternalLogins','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: LinkLogin','Name':'LinkLogin','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: LinkLoginCallback','Name':'LinkLoginCallback','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: RemoveLogin','Name':'RemoveLogin','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: TwoFactorAuthentication','Name':'TwoFactorAuthentication','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: Disable2faWarning','Name':'Disable2faWarning','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: Disable2fa','Name':'Disable2fa','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: EnableAuthenticator','Name':'EnableAuthenticator','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: ResetAuthenticatorWarning','Name':'ResetAuthenticatorWarning','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: ResetAuthenticator','Name':'ResetAuthenticator','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: GenerateRecoveryCodes','Name':'GenerateRecoveryCodes','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: UploadUserPhoto','Name':'UploadUserPhoto','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: RemoveUserPhoto','Name':'RemoveUserPhoto','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: DisplayUserPhoto','Name':'DisplayUserPhoto','DisplayName':null,'ControllerId':':Manage'}]},{'Id':':ProjectDescriptions','Name':'ProjectDescriptions','DisplayName':null,'AreaName':null,'Actions':[{'Id':':ProjectDescriptions: Index','Name':'Index','DisplayName':null,'ControllerId':':ProjectDescriptions'},{'Id':':ProjectDescriptions: Create','Name':'Create','DisplayName':null,'ControllerId':':ProjectDescriptions'}]},{'Id':':ProjectFeatures','Name':'ProjectFeatures','DisplayName':null,'AreaName':null,'Actions':[{'Id':':ProjectFeatures: Index','Name':'Index','DisplayName':null,'ControllerId':':ProjectFeatures'},{'Id':':ProjectFeatures: Create','Name':'Create','DisplayName':null,'ControllerId':':ProjectFeatures'},{'Id':':ProjectFeatures: Delete','Name':'Delete','DisplayName':null,'ControllerId':':ProjectFeatures'}]},{'Id':':ProjectFiles','Name':'ProjectFiles','DisplayName':null,'AreaName':null,'Actions':[{'Id':':ProjectFiles: Index','Name':'Index','DisplayName':null,'ControllerId':':ProjectFiles'},{'Id':':ProjectFiles: Create','Name':'Create','DisplayName':null,'ControllerId':':ProjectFiles'},{'Id':':ProjectFiles: DownloadFile','Name':'DownloadFile','DisplayName':null,'ControllerId':':ProjectFiles'}]},{'Id':':Projects','Name':'Projects','DisplayName':null,'AreaName':null,'Actions':[{'Id':':Projects: Index','Name':'Index','DisplayName':null,'ControllerId':':Projects'},{'Id':':Projects: Details','Name':'Details','DisplayName':null,'ControllerId':':Projects'},{'Id':':Projects: Create','Name':'Create','DisplayName':null,'ControllerId':':Projects'},{'Id':':Projects: Edit','Name':'Edit','DisplayName':null,'ControllerId':':Projects'},{'Id':':Projects: Delete','Name':'Delete','DisplayName':null,'ControllerId':':Projects'}]},{'Id':':Roles','Name':'Roles','DisplayName':null,'AreaName':null,'Actions':[{'Id':':Roles: Index','Name':'Index','DisplayName':null,'ControllerId':':Roles'},{'Id':':Roles: Details','Name':'Details','DisplayName':null,'ControllerId':':Roles'},{'Id':':Roles: Create','Name':'Create','DisplayName':null,'ControllerId':':Roles'},{'Id':':Roles: Edit','Name':'Edit','DisplayName':null,'ControllerId':':Roles'},{'Id':':Roles: Delete','Name':'Delete','DisplayName':null,'ControllerId':':Roles'},{'Id':':Roles: DeleteMultiSelect','Name':'DeleteMultiSelect','DisplayName':null,'ControllerId':':Roles'},{'Id':':Roles: Permission','Name':'Permission','DisplayName':null,'ControllerId':':Roles'},{'Id':':Roles: AddRoleCustomer','Name':'AddRoleCustomer','DisplayName':null,'ControllerId':':Roles'},{'Id':':Roles: AddRole','Name':'AddRole','DisplayName':null,'ControllerId':':Roles'}]},{'Id':':Teams','Name':'Teams','DisplayName':null,'AreaName':null,'Actions':[{'Id':':Teams: Index','Name':'Index','DisplayName':null,'ControllerId':':Teams'},{'Id':':Teams: Details','Name':'Details','DisplayName':null,'ControllerId':':Teams'},{'Id':':Teams: Create','Name':'Create','DisplayName':null,'ControllerId':':Teams'},{'Id':':Teams: Edit','Name':'Edit','DisplayName':null,'ControllerId':':Teams'},{'Id':':Teams: Delete','Name':'Delete','DisplayName':null,'ControllerId':':Teams'}]},{'Id':':TicketDetails','Name':'TicketDetails','DisplayName':null,'AreaName':null,'Actions':[{'Id':':TicketDetails: Index','Name':'Index','DisplayName':null,'ControllerId':':TicketDetails'},{'Id':':TicketDetails: Create','Name':'Create','DisplayName':null,'ControllerId':':TicketDetails'},{'Id':':TicketDetails: DownloadFile','Name':'DownloadFile','DisplayName':null,'ControllerId':':TicketDetails'}]},{'Id':':TicketMasters','Name':'TicketMasters','DisplayName':null,'AreaName':null,'Actions':[{'Id':':TicketMasters: Index','Name':'Index','DisplayName':null,'ControllerId':':TicketMasters'},{'Id':':TicketMasters: Details','Name':'Details','DisplayName':null,'ControllerId':':TicketMasters'},{'Id':':TicketMasters: Create','Name':'Create','DisplayName':null,'ControllerId':':TicketMasters'},{'Id':':TicketMasters: Edit','Name':'Edit','DisplayName':null,'ControllerId':':TicketMasters'},{'Id':':TicketMasters: Delete','Name':'Delete','DisplayName':null,'ControllerId':':TicketMasters'},{'Id':':TicketMasters: ReferralTicket','Name':'ReferralTicket','DisplayName':null,'ControllerId':':TicketMasters'},{'Id':':TicketMasters: CloseTicket','Name':'CloseTicket','DisplayName':null,'ControllerId':':TicketMasters'}]},{'Id':':UserTeams','Name':'UserTeams','DisplayName':null,'AreaName':null,'Actions':[{'Id':':UserTeams: Index','Name':'Index','DisplayName':null,'ControllerId':':UserTeams'},{'Id':':UserTeams: Create','Name':'Create','DisplayName':null,'ControllerId':':UserTeams'},{'Id':':UserTeams: Delete','Name':'Delete','DisplayName':null,'ControllerId':':UserTeams'}]}]"; role.Role_Status = 1;
                role.Description = "دسترسی به کلیه امور";
                role.Role_Type = 1;
                IdentityResult roleResult = roleManager.
                CreateAsync(role).Result;
            }
           

            if (!roleManager.RoleExistsAsync
 ("Customer").Result)
            {
                ApplicationRole role = new ApplicationRole();
                role.Name = "Customer";
                role.Access = "[{'Id':':Account','Name':'Account','DisplayName':null,'AreaName':null,'Actions':[{'Id':':Account: Logout','Name':'Logout','DisplayName':null,'ControllerId':':Account'}]},{'Id':':Home','Name':'Home','DisplayName':null,'AreaName':null,'Actions':[{'Id':':Home: Index','Name':'Index','DisplayName':null,'ControllerId':':Home'}]},{'Id':':Manage','Name':'Manage','DisplayName':null,'AreaName':null,'Actions':[{'Id':':Manage: Index','Name':'Index','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: SendVerificationEmail','Name':'SendVerificationEmail','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: ChangePassword','Name':'ChangePassword','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: ChangeUserName','Name':'ChangeUserName','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: SetPassword','Name':'SetPassword','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: ExternalLogins','Name':'ExternalLogins','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: LinkLogin','Name':'LinkLogin','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: LinkLoginCallback','Name':'LinkLoginCallback','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: RemoveLogin','Name':'RemoveLogin','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: TwoFactorAuthentication','Name':'TwoFactorAuthentication','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: Disable2faWarning','Name':'Disable2faWarning','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: Disable2fa','Name':'Disable2fa','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: EnableAuthenticator','Name':'EnableAuthenticator','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: ResetAuthenticatorWarning','Name':'ResetAuthenticatorWarning','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: ResetAuthenticator','Name':'ResetAuthenticator','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: GenerateRecoveryCodes','Name':'GenerateRecoveryCodes','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: UploadUserPhoto','Name':'UploadUserPhoto','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: RemoveUserPhoto','Name':'RemoveUserPhoto','DisplayName':null,'ControllerId':':Manage'},{'Id':':Manage: DisplayUserPhoto','Name':'DisplayUserPhoto','DisplayName':null,'ControllerId':':Manage'}]},{'Id':':ProjectFeatures','Name':'ProjectFeatures','DisplayName':null,'AreaName':null,'Actions':[{'Id':':ProjectFeatures: Index','Name':'Index','DisplayName':null,'ControllerId':':ProjectFeatures'},{'Id':':ProjectFeatures: Create','Name':'Create','DisplayName':null,'ControllerId':':ProjectFeatures'}]},{'Id':':ProjectFiles','Name':'ProjectFiles','DisplayName':null,'AreaName':null,'Actions':[{'Id':':ProjectFiles: Index','Name':'Index','DisplayName':null,'ControllerId':':ProjectFiles'},{'Id':':ProjectFiles: Create','Name':'Create','DisplayName':null,'ControllerId':':ProjectFiles'},{'Id':':ProjectFiles: DownloadFile','Name':'DownloadFile','DisplayName':null,'ControllerId':':ProjectFiles'}]},{'Id':':Projects','Name':'Projects','DisplayName':null,'AreaName':null,'Actions':[{'Id':':Projects: Index','Name':'Index','DisplayName':null,'ControllerId':':Projects'},{'Id':':Projects: Details','Name':'Details','DisplayName':null,'ControllerId':':Projects'}]},{'Id':':TicketDetails','Name':'TicketDetails','DisplayName':null,'AreaName':null,'Actions':[{'Id':':TicketDetails: Index','Name':'Index','DisplayName':null,'ControllerId':':TicketDetails'},{'Id':':TicketDetails: Create','Name':'Create','DisplayName':null,'ControllerId':':TicketDetails'},{'Id':':TicketDetails: DownloadFile','Name':'DownloadFile','DisplayName':null,'ControllerId':':TicketDetails'}]},{'Id':':TicketMasters','Name':'TicketMasters','DisplayName':null,'AreaName':null,'Actions':[{'Id':':TicketMasters: Index','Name':'Index','DisplayName':null,'ControllerId':':TicketMasters'},{'Id':':TicketMasters: Create','Name':'Create','DisplayName':null,'ControllerId':':TicketMasters'},{'Id':':TicketMasters: CloseTicket','Name':'CloseTicket','DisplayName':null,'ControllerId':':TicketMasters'}]}]"; role.Role_Type = 1;
                role.Role_Status = 1;
                role.Description = "مشتریان";
                IdentityResult roleResult = roleManager.
                CreateAsync(role).Result;
            }
         

        }

        public static void SeedCompanyUser(ApplicationDbContext coText)
        {

            if (!coText.Customer_tb.Any())
            {
                var Customer = new Customer
                {
                    UserIdentity_Id = User_Id,
                    IsActive=true,
                    Jender=false,
                    IsNewsLetter=false

                };
                coText.Add(Customer);
            }

            coText.SaveChanges();
        }
        public static void SeedConnectionGroup(ApplicationDbContext coText)
        {

            if (!coText.ConnectionGroup_tb.Any())
            {
                var lang = coText.Language_tb.Where(x => x.Lang_ID == "fa").FirstOrDefault();
                var connectionGroup = new ConnectionGroup
                {
                    ConnectionGroupTitle = " گروه پیش فرض",
                    IsActive=true,
                    Lang="fa",
                    Language=lang
                    
                };
                coText.Add(connectionGroup);
            }

            coText.SaveChanges();
        }
        public static void SeedLanguage(ApplicationDbContext coText)
        {

            if (!coText.Language_tb.Any())
            {
                var language = new Language
                {
                    IsActive=true,
                    Lang_Name="فارسی",
                    Lang_ID="fa"

                };
                coText.Add(language);
            }

            coText.SaveChanges();
        }

        public static void SeedSlideShowGroup(ApplicationDbContext coText)
        {

            if(!coText.SlideShowGroup_tb.Any())
            {
                var language = coText.Language_tb.Where(l => l.Lang_ID=="fa").FirstOrDefault();
                var slideShowGroup = new SlideShowGroup
                {
                    SSGName="گروه پیش فرض اسلایدشو",
                    IsActive=true,
                    Lnag="fa",
                    Language=language
                };
                coText.Add(slideShowGroup);
            }

            coText.SaveChanges();
        }

        public static void SeedNewsLetterGroup(ApplicationDbContext coText)
        {

            if (!coText.NewsLetterGroup_tb.Any())
            {
                var language = coText.Language_tb.Where(l => l.Lang_ID == "fa").FirstOrDefault();
               
                var newsLetterGroup = new NewsLetterGroup
                {
                    IsActive = true,
                    Lang = "fa",
                    Title = "گروه پیش فرض خبرنامه",
                    Language=language
                };
                coText.Add(newsLetterGroup);
            }

            coText.SaveChanges();
        }
        public static void SeedContentGroup(ApplicationDbContext coText)
        {

            if (!coText.ContentsGroup_tb.Any())
            {
                var language = coText.Language_tb.Where(l => l.Lang_ID == "fa").FirstOrDefault();

                var contentsGroup = new ContentsGroup
                {
                    IsActive = true,
                    Lang = "fa",
                    ContentGroupName = "گروه پیش فرض خبرنامه",
                    Language = language
                };
                coText.Add(contentsGroup);
            }

            coText.SaveChanges();
        }
        public static void SeedColor(ApplicationDbContext coText)
        {

            if (!coText.Color_tb.Any())
            {
                var language = coText.Language_tb.Where(l => l.Lang_ID == "fa").FirstOrDefault();

                var color = new Color
                {
                    Color_ID = 0,
                    Lang = "fa",
                    ColorTitle = "بدون رنگ",
                    Language = language
                };
                coText.Add(color);
            }

            coText.SaveChanges();
        }

        public static void SeedAboutUs(ApplicationDbContext coText)
        {

            if (!coText.Contents_tb.Any())
            {

                var aboutUs = new Contents
                {
                    Content_ID = -1,
                    ContentGroupId = -1,
                    ContentTitle = "درباره ما",
                    ContentSummary = "خلاصه درباره ما",
                    ContentText = "متن اصلی درباره ما",
                    CreateDate = DateTime.Now,
                    IsActive = true
                };
                coText.Add(aboutUs);
            }
            coText.SaveChanges();
        }

        public static void SeedAVFGruop(ApplicationDbContext coText)
        {
            if (!coText.AVFGroup_tb.Any())
            {
                var language = coText.Language_tb.Where(l => l.Lang_ID == "fa").FirstOrDefault();

                var AVFG = new AVFGroup
                {
                    AVFGroup_ID = 1,
                    ParentId = null,
                    AVFGroupTitle = "ریشه",
                    IsActive = true,
                    Lang = "fa"
                };
                coText.Add(AVFG);
            }

            coText.SaveChanges();
        }


    }


}


