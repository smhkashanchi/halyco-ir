﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses;
using DomainClasses.Role;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using DomainClasses.User;
using DomainClasses.Customer;
using DomainClasses.Cart;

namespace DataLayer
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser,ApplicationRole,string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<DomainClasses.Language.Language> Language_tb { get; set; }

        public DbSet<DomainClasses.Customer.Customer> Customer_tb { get; set; }
        public DbSet<DomainClasses.Customer.CustomerAddress> CustomerAddress_tb { get; set; }
        public DbSet<DomainClasses.Cart.Factor> Factor_tb { get; set; }
        public DbSet<DomainClasses.Cart.Factor> FactorDetails_tb { get; set; }
        public DbSet<DomainClasses.Cart.SendType> SendType_tb { get; set; }

        public DbSet<DomainClasses.SlideShow.SlideShowGroup> SlideShowGroup_tb { get; set; }
        public DbSet<DomainClasses.SlideShow.SlideShow> SlideShow_tb { get; set; }
        public DbSet<DomainClasses.FAQ.FAQ> FAQ_tb { get; set; }

        public DbSet<DomainClasses.ContactUsMessage.ContactusMessageGroup> ContactUsMessageGroup_tb { get; set; }
        public DbSet<DomainClasses.ContactUsMessage.ContactusMessage> ContactUsMessage_tb { get; set; }

        public DbSet<DomainClasses.Contents.ContentsGroup> ContentsGroup_tb { get; set; }
        public DbSet<DomainClasses.Contents.Contents> Contents_tb { get; set; }
        public DbSet<DomainClasses.Contents.ContentsTags> ContentsTag_tb { get; set; }
        public DbSet<DomainClasses.Contents.ContentsGallery> ContentsGallery_tb { get; set; }
        public DbSet<DomainClasses.Contents.ContentFiles> ContentFiles_tb { get; set; }
        public DbSet<DomainClasses.Contents.ContentsComments> ContentsComment_tb { get; set; }
        public DbSet<DomainClasses.Contents.ContentsVotes> ContentsVotes_tb { get; set; }

        public DbSet<DomainClasses.Gallery.Gallery> Gallery_tb { get; set; }
        public DbSet<DomainClasses.Gallery.GalleryGroup> GalleryGroup_tb { get; set; }
        public DbSet<DomainClasses.Gallery.GalleryPicture> GalleryPicture_tb { get; set; }

        public DbSet<DomainClasses.Product.Off> Off_tb { get; set; }
        public DbSet<DomainClasses.Product.ProductsGroup> ProductGroup_tb { get; set; }
        public DbSet<DomainClasses.Product.MainProduct> MainProduct_tb { get; set; }
        public DbSet<DomainClasses.Product.ProductInfo> ProductInfo_tb { get; set; }
        public DbSet<DomainClasses.Product.Unit> Unit_tb { get; set; }
        public DbSet<DomainClasses.Product.Producer> Producer_tb { get; set; }
        public DbSet<DomainClasses.Product.Color> Color_tb { get; set; }
        public DbSet<DomainClasses.Product.Garanty> Garanty_tb { get; set; }
        public DbSet<DomainClasses.Product.Feature> Feature_tb { get; set; }
        public DbSet<DomainClasses.Product.FeatureReply> FeatureReply_tb { get; set; }
        public DbSet<DomainClasses.Product.ProductFeature> ProductFeature_tb { get; set; }
        public DbSet<DomainClasses.Product.ProductColor> ProductColor_tb { get; set; }
        public DbSet<DomainClasses.Product.ProductGaranty> ProductGaranty_tb { get; set; }
        public DbSet<DomainClasses.Product.ProductAdvantage> ProductAdvantage_tb { get; set; }
        public DbSet<DomainClasses.Product.ProductComment> ProductComment_tb { get; set; }
        public DbSet<DomainClasses.Product.SimilarProduct> SimilarProduct_tb { get; set; }


        public DbSet<DomainClasses.Connection.ConnectionGroup> ConnectionGroup_tb { get; set; }
        public DbSet<DomainClasses.Connection.Connection> Connection_tb { get; set; }
        public DbSet<DomainClasses.Connection.Map> Map_tb { get; set; }


        public DbSet<DomainClasses.AVF.AVFGroup> AVFGroup_tb { get; set; }
        public DbSet<DomainClasses.AVF.AVF> AVF_tb { get; set; }

        public DbSet<DomainClasses.Region.Country> Country_tb { get; set; }
        public DbSet<DomainClasses.Region.State> State_tb { get; set; }
        public DbSet<DomainClasses.Region.City> City_tb { get; set; }

        public DbSet<DomainClasses.Supporter.CoWorker> CoWorker_tb { get; set; }

        public DbSet<DomainClasses.SocialNetwork.SocialNetworks> SocialNetworks_tb { get; set; }
        public DbSet<DomainClasses.NewsLetters.NewsLetterGroup> NewsLetterGroup_tb { get; set; }
        public DbSet<DomainClasses.NewsLetters.NewsLetter> NewsLetter_tb { get; set; }
        public DbSet<DomainClasses.SMS_Subscription.Persons> SMS_tb { get; set; }

        public DbSet<DomainClasses.Project.ProjectGroup> ProjectGroup_tb { get; set; }
        public DbSet<DomainClasses.Project.Projects> Projects_tb { get; set; }
        public DbSet<DomainClasses.Project.ProjectsAdvantage> ProjectsAdvantage_tb { get; set; }
        public DbSet<DomainClasses.Project.RateOption> RateOption_tb { get; set; }
        public DbSet<DomainClasses.Project.ProjectVideo> ProjectVideo_tb { get; set; }


        public DbSet<DomainClasses.Catalog.Catalog> Catalog_tb { get; set; }

        public DbSet<DomainClasses.Seo.SeoPage> SeoPage_tb { get; set; }
        public DbSet<DomainClasses.Seo.SeoPageProduct> SeoPageProduct_tb { get; set; }
        public DbSet<DomainClasses.Seo.MainPageSeo> MainPageSeo_tb { get; set; }

        public DbSet<DomainClasses.Manager.HalycoManager> HalycoManager { get; set; }

        public DbSet<DomainClasses.Employees.Employee> Employee_tb { get; set; }
        public DbSet<DomainClasses.Employees.EmployeeSalary> EmployeeSalary { get; set; }
        public DbSet<DomainClasses.Employees.Employe_Post> Employe_Post { get; set; }
        public DbSet<DomainClasses.Employees.Mission> Mission_tb { get; set; }
        public DbSet<DomainClasses.Employees.Vacation> Vacation_tb { get; set; }
        public DbSet<DomainClasses.Employees.VacationSignature> VacationSignature_tb { get; set; }
        public DbSet<DomainClasses.Employees.MissionSignature> MissionSignature_tb { get; set; }


        public DbSet<DomainClasses.BasicInformation.Activity> Activity_tb { get; set; }
        public DbSet<DomainClasses.BasicInformation.ActivityField> ActivityField_tb { get; set; }
        public DbSet<DomainClasses.BasicInformation.OfficePost> OfficePost_tb { get; set; }
        public DbSet<DomainClasses.BasicInformation.OfficeUnit> OfficeUnit_tb { get; set; }

        public DbSet<DomainClasses.CustomerComplaint.CustomerComplaintMaster> CustomerComplaintMaster_tb { get; set; }
        public DbSet<DomainClasses.CustomerComplaint.CustomerComplaintFiles> CustomerComplaintFiles_tb { get; set; }

        public DbSet<DomainClasses.Proposal.MainProposal> MainProposal_tb { get; set; }
        public DbSet<DomainClasses.Proposal.ProposalProviders> ProposalProviders_tb { get; set; }
        public DbSet<DomainClasses.Proposal.ProposalFiles> ProposalFiles_tb { get; set; }

        public DbSet<DomainClasses.Suppliers.MainSuppliers> MainSuppliers_tb { get; set; }
        public DbSet<DomainClasses.Suppliers.SuppliersActivityFields> SuppliersActivityFields_tb { get; set; }
        public DbSet<DomainClasses.Suppliers.SuppliersCertificates> SuppliersCertificates_tb { get; set; }
        public DbSet<DomainClasses.Suppliers.SuppliersFiles> SuppliersFiles_tb { get; set; }
        public DbSet<DomainClasses.Suppliers.SupplierShareholders> SupplierShareholders_tb { get; set; }
        public DbSet<DomainClasses.Suppliers.SuppliersResume> SuppliersResume_tb { get; set; }

        public DbSet<DomainClasses.Survey.MainSurvey> MainSurvey_tb { get; set; }
        public DbSet<DomainClasses.Survey.SurvayQuestionGroup> SurvayQuestionGroup_tb { get; set; }
        public DbSet<DomainClasses.Survey.SurveyAnswerType> SurveyAnswerType_tb { get; set; }
        public DbSet<DomainClasses.Survey.SurveyCustomerOtherData> SurveyCustomerOtherData_tb { get; set; }
        public DbSet<DomainClasses.Survey.CustomerSurveyProducts> CustomerSurveyProducts_tb { get; set; }
        public DbSet<DomainClasses.Survey.SurveyEmployeesOtherData> SurveyEmployeesOtherData_tb { get; set; }
        public DbSet<DomainClasses.Survey.SurveyQuestion> SurveyQuestion_tb { get; set; }
        public DbSet<DomainClasses.Survey.SurveyQuestionReply> SurveyQuestionReply_tb { get; set; }
        public DbSet<DomainClasses.Survey.SurveySuppliersOtherData> SurveySuppliersOtherData_tb { get; set; }


        public DbSet<DomainClasses.Employement.Emp_BaseInformation> Emp_BaseInformation_tb { get; set; }
        public DbSet<DomainClasses.Employement.Emp_Activity> Emp_Activity_tb { get; set; }
        public DbSet<DomainClasses.Employement.Emp_ActivityItems> Emp_ActivityItems_tb { get; set; }
        public DbSet<DomainClasses.Employement.Emp_ComputerSkillItems> Emp_ComputerSkillItems_tb { get; set; }
        public DbSet<DomainClasses.Employement.Emp_ComputerSkills> Emp_ComputerSkills_tb { get; set; }
        public DbSet<DomainClasses.Employement.Emp_Documents> Emp_Documents_tb { get; set; }
        public DbSet<DomainClasses.Employement.Emp_EducationalRecords> Emp_EducationalRecords_tb { get; set; }
        public DbSet<DomainClasses.Employement.Emp_Guarantor> Emp_Guarantor_tb { get; set; }
        public DbSet<DomainClasses.Employement.Emp_TraningCourse> Emp_TraningCourse_tb { get; set; }
        public DbSet<DomainClasses.Employement.Emp_UnderSupervisor> Emp_UnderSupervisor_tb { get; set; }
        public DbSet<DomainClasses.Employement.Emp_WorkExperience> Emp_WorkExperience_tb { get; set; }

        public DbSet<DomainClasses.Employees.ShiftChange> shiftChange_tb{ get; set; }
        public DbSet<DomainClasses.Employees.ShiftChangeSignature> shiftChangeSignature_tb { get; set; }
        public DbSet<DomainClasses.Employees.ExtraWork> ExtraWork_tb { get; set; }
        public DbSet<DomainClasses.Employees.ExtraworkSignature> ExtraWorkSignature_tb { get; set; }
        public DbSet<DomainClasses.Employees.Loan> loan_tb { get; set; }




        protected override void OnModelCreating(ModelBuilder builder)
        {
            //builder.Seed();
            //برای داشتن 2 یا چند کلید در جدول از دستور زیر استفاده میکنیم
            builder.Entity<DomainClasses.Contents.ContentsGallery>()
                .HasKey(x => new { x.GalleryId, x.ContentId });

            builder.Entity<DomainClasses.Contents.ContentFiles>()
               .HasKey(x => new { x.AvfId, x.ContentId });

            builder.Entity<DomainClasses.Survey.SurveyQuestionReply>()
               .HasKey(x => new { x.ExtraInformationId, x.QuestionId });

            //Emp_ComputerSkills Table
            builder.Entity<DomainClasses.Employement.Emp_ComputerSkills>()
                .HasKey(x => new { x.Employement_Id, x.ItemId });

            //Emp_Activity Table
            builder.Entity<DomainClasses.Employement.Emp_Activity>()
                .HasKey(x => new { x.Employement_Id, x.ItemId });

            //ProductFaeture Table
            builder.Entity<DomainClasses.Product.ProductFeature>()
                .HasKey(x => new { x.ProductInfoId, x.FeatureReplyId });

            //CustomerSurveyProducts Table
            builder.Entity<DomainClasses.Survey.CustomerSurveyProducts>()
                .HasKey(x => new { x.ProductId, x.CustomerSurveyId });

            // ProductGaranty Table
            builder.Entity<DomainClasses.Product.ProductGaranty>()
                .HasKey(x => new { x.ProductInfoId, x.GarantyId });

           // builder.Entity<DomainClasses.Proposal.ProposalProviders>().HasKey(x => new { x.PersonalCode, x.ProposalId });

            //builder.Entity<DomainClasses.Survey.SurveyEmployeesOtherData>().HasKey(x => new { x.MainSurveyId, x.PersonalCode });

            //SimilarProduct Table
            builder.Entity<DomainClasses.Product.SimilarProduct>()
                .HasOne(x => x.SimilarProducts).WithMany(x => x.SimilarProduct).HasForeignKey(x => x.SimilarProduct_ID)
                .OnDelete(DeleteBehavior.SetNull);

            builder.Entity<CustomerAddress>().HasOne(e => e.City)
  .WithMany(x => x.CustomerAddresses).Metadata.DeleteBehavior = DeleteBehavior.Restrict;

            builder.Entity<Factor>().HasOne(e => e.CustomerAddress)
 .WithMany(x => x.Factor).Metadata.DeleteBehavior = DeleteBehavior.Restrict;

            builder.Entity<CustomerAddress>().HasOne(e => e.State)
.WithMany(x => x.CustomerAddresses).Metadata.DeleteBehavior = DeleteBehavior.Restrict;
            builder.Entity<CustomerAddress>().HasOne(e => e.Country)
.WithMany(x => x.CustomerAddresses).Metadata.DeleteBehavior = DeleteBehavior.Restrict;


            //VideoProduct Table
            //builder.Entity<DomainClasses.Project.ProjectVideo>()
            //    .HasOne(x => x.AVF).WithMany(x => x.ProjectVideos).HasForeignKey(x => x.VideoId)
            //    .OnDelete(DeleteBehavior.SetNull);

            builder.Entity<DomainClasses.Project.ProjectVideo>()
                .HasOne(x => x.Projects)
                .WithMany(x => x.ProjectVideos)
                .HasForeignKey(x => x.ProjectId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<DomainClasses.Project.ProjectVideo>()
                .HasOne(x => x.AVF)
                .WithMany(x => x.ProjectVideos)
                .HasForeignKey(x => x.VideoId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
            //builder.Entity<DomainClasses.Product.SimilarProduct>()
            //.HasKey(x => new { x.SimilarProduct_ID, x.ProductID });


            base.OnModelCreating(builder);

            //


        }

    }
}
