﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace itsaco_RefrencenetCoreCMS.Models.ManageViewModels
{
    public class IndexViewModel
    {
        [Display(Name = "نام کاربری")]
        [Required(ErrorMessage = "{0} را وارد کنید")]
        [StringLength(100, ErrorMessage = "{0} حداکثر باید 100 کاراکتر باشد")]
        public string Username { get; set; }

        public bool IsEmailConfirmed { get; set; }
        [StringLength(100, ErrorMessage = "{0} حداکثر باید 100 کاراکتر باشد")]
        [Required(ErrorMessage = "{0} را وارد کنید")]
        [EmailAddress(ErrorMessage = "{0} نامعتبر نمی باشد")]
        [Display(Name = "ایمیل")]
        public string Email { get; set; }

        [Required(ErrorMessage = "{0} را وارد کنید")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$", ErrorMessage = "شماره {0} وارد شده مجاز نمی باشد")]
        [Phone]
        [Display(Name = "تلفن ثابت")]
        public string PhoneNumber { get; set; }

        [Display(Name = "نام و نام خانوادگی")]
        [Required(ErrorMessage = "{0} را وارد کنید")]
        [StringLength(100, ErrorMessage = "{0} حداکثر باید 100 کاراکتر باشد")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "{0} را وارد کنید")]
        [RegularExpression(@"^\(?([0-9]{4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "شماره تلفن همراه وارد شده مجاز نمی باشد")]
        [Display(Name = " 1 تلفن همراه")]
        [StringLength(11)]
        public string Mobile_1 { get; set; }
        [RegularExpression(@"^\(?([0-9]{4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "شماره تلفن همراه وارد شده مجاز نمی باشد")]

        [Display(Name = "2 تلفن همراه")]
        [StringLength(11)]
        public string Mobile_2 { get; set; }

      
    }
}
