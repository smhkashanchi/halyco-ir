﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace itsaco_RefrencenetCoreCMS.Models.AccountViewModels
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "{0} خود را وارد کنید")]
        [EmailAddress]
        [Display(Name = "ایمیل")]
        public string Email { get; set; }

        [Required(ErrorMessage = "{0} خود را وارد کنید")]
        [StringLength(100, ErrorMessage = "رمز عبور شما باید حداقل 6 کاراکتر باشد", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "رمز عبور")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "تکرار رمز عبور")]
        [Compare("Password", ErrorMessage = "{0} یکسان نیست")]
        public string ConfirmPassword { get; set; }
        [Display(Name = "نام و نام خانوادگی")]

        [Required(ErrorMessage = "{0} خود را وارد کنید")]
        [StringLength(100, ErrorMessage = "{0} حداکثر باید 100 کاراکتر باشد")]
        public string FullName { get; set; }

        
   
        
    }
}
