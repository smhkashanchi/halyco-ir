﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace itsaco_RefrencenetCoreCMS.Models.AccountViewModels
{
    public class ResetPasswordViewModel
    {
        [Display(Name = "ایمیل")]
        [Required(ErrorMessage = "ایمیل خود را وارد کنید")]
        [EmailAddress(ErrorMessage = "ایمیل وارد شده معتبر نمی باشد")]
        public string Email { get; set; }

        [Required(ErrorMessage = "رمز عبور خود را وارد کنید")]
        [Display(Name = " رمز عبور")]

        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "تائید رمز عبور")]
        [Compare("Password", ErrorMessage = "رمز عبور یکسان نیست")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }
}
