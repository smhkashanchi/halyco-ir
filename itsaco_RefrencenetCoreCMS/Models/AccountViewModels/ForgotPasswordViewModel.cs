﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace itsaco_RefrencenetCoreCMS.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
       [Display(Name ="ایمیل")]
        [Required(ErrorMessage ="ایمیل خود را وارد کنید")]
        [EmailAddress(ErrorMessage ="ایمیل وارد شده معتبر نمی باشد")]
        public string Email { get; set; }
    }
}
