﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace itsaco_RefrencenetCoreCMS.Models.AccountViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = " {0} را وارد کنید")]
        [Display(Name = "نام کاربری")]
        public string Email { get; set; }
        [Display(Name = "رمز عبور")]
        [Required(ErrorMessage = "رمز عبور خود را وارد کنید")]

        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "مرا به خاطر بسپار")]
        public bool RememberMe { get; set; }
        public string LoginAdmin { get; set; }

        [Required]
        [StringLength(4)]
        [NotMapped]
        public string CaptchaCode { get; set; }
    }
}
