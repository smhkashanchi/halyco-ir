﻿using DomainClasses.Language;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Services.Repositories;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace AspCorePanelProject.Areas.Admin.ViewComponents
{
    public class ShowAllLanguage:ViewComponent
    {
        //این کلاس که از ویو کامپوننت ارث بری میکنید
        public IGenericRepository<Language> _langRepository;
        public ShowAllLanguage(IGenericRepository<Language> langRepository)
        {
            _langRepository = langRepository;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var langs = await _langRepository.GetAllAsync(x => x.IsActive == true);
            ViewBag.Languages = new SelectList(await _langRepository.GetAllAsync(x => x.IsActive == true), "Lang_ID", "Lang_Name", CultureInfo.CurrentCulture.Name);
            return await Task.FromResult((IViewComponentResult)View("ShowAllLanguage", langs));
        }
    }
}
