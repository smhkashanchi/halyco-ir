﻿using DomainClasses;
using DomainClasses.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ViewModels.UsersViewModels;

namespace itsaco_RefrencenetCoreCMS.Area.Admin.ViewComponents
{
    public class UserPhotoAdminViewComponent : ViewComponent
    {
        private readonly UserManager<ApplicationUser> _userManager;
        public UserPhotoAdminViewComponent(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }
        [TempData]
        public string StatusMessage { get; set; }
        public async Task<IViewComponentResult> InvokeAsync()
        {


            UserPhotoViewModel viewModel = new UserPhotoViewModel();
            var user = await _userManager.GetUserAsync(UserClaimsPrincipal);
            if (user.UserPhoto == null || user.UserPhoto == "/UploadFile/UsersPhoto/no-profile.jpg")
            {

                viewModel.UserPhoto_Address = "/UploadFile/UsersPhoto/no-profile.jpg";
                viewModel.HasUserPhoto = false;
                return View(viewModel);
               
            }
            else
            {

                viewModel.UserPhoto_Address = user.UserPhoto;
                viewModel.HasUserPhoto = true;
                return View(viewModel);

            }
        }
        
    }
}
