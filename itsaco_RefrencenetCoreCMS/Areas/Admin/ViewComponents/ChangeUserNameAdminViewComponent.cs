﻿using DomainClasses;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using ViewModels;
using System.Threading.Tasks;
using ViewModels.Manage;
using DomainClasses.User;

namespace itsaco_RefrencenetCoreCMS.Area.Admin.ViewComponents
{

    public class ChangeUserNameAdminViewComponent : ViewComponent
    {
        private readonly UserManager<ApplicationUser> _userManager;
        public ChangeUserNameAdminViewComponent(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }
        [TempData]
        public string StatusMessage { get; set; }
        public async Task<IViewComponentResult> InvokeAsync()
        {


             var user = await _userManager.GetUserAsync(UserClaimsPrincipal);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(UserClaimsPrincipal)}'.");
            }

            var model = new ChangeUserNameViewModel ();
            return await Task.FromResult((IViewComponentResult)View("Default",model));

        }
    }
}
