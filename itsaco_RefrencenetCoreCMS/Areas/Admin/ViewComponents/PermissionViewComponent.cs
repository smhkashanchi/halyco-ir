﻿using DomainClasses;
using DomainClasses.FilterAuthorization;
using DomainClasses.Role;
using DomainClasses.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Services.FilterAuthorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModels.Role;

namespace Project_Management.ViewComponents
{
    public class PermissionViewComponent : ViewComponent
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;

        private readonly IMvcControllerDiscovery _mvcControllerDiscovery;
        private readonly SignInManager<ApplicationUser> _signInManager;


        public PermissionViewComponent(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IMvcControllerDiscovery mvcControllerDiscovery,
            RoleManager<ApplicationRole> roleManager
           )
        {
            _roleManager = roleManager;
            _mvcControllerDiscovery = mvcControllerDiscovery;
            _userManager = userManager;
            _signInManager = signInManager;

        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var user = await _userManager.GetUserAsync(UserClaimsPrincipal);
            var roles = await _userManager.GetRolesAsync(user);
            var viewModel = new PermissionViewModel();
            List<object> listDes = new List<object>();

            foreach (var item in roles)
            {
                var role = await _roleManager.FindByNameAsync(item);
                if (role.Access != null)
                {
                    var des = JsonConvert.DeserializeObject(role.Access);
                    listDes.Add(des);
                }
            }

            



            IEnumerable<MvcControllerInfo> listcontroller = new List<MvcControllerInfo>();
            List<MvcControllerInfo> Allcontroller = new List<MvcControllerInfo>();
            foreach (var item in listDes)
            {
                listcontroller = JsonConvert.DeserializeObject<IEnumerable<MvcControllerInfo>>(item.ToString());
                foreach (var controller in listcontroller)
                {
                    Allcontroller.Add(controller);
                }
            }
            List<MvcControllerInfo> MainControllers = new List<MvcControllerInfo>();
            List<MvcControllerInfo> ProductControllers = new List<MvcControllerInfo>();
            List<MvcControllerInfo> ContentControllers = new List<MvcControllerInfo>();
            List<MvcControllerInfo> ConnectionControllers = new List<MvcControllerInfo>();
            List<MvcControllerInfo> BaseInformationControllers = new List<MvcControllerInfo>();
            List<MvcControllerInfo> EmployeeControllers = new List<MvcControllerInfo>();
            List<MvcControllerInfo> EmployeeCoworkerControllers = new List<MvcControllerInfo>();
            List<MvcControllerInfo> SurveyControllers = new List<MvcControllerInfo>();
            List<MvcControllerInfo> EmployementControllers = new List<MvcControllerInfo>();
            List<MvcControllerInfo> EmployementRequestMySectionControllers = new List<MvcControllerInfo>();
            List<MvcControllerInfo> EmployementRequestMyUnitControllers = new List<MvcControllerInfo>();
            List<MvcControllerInfo> EmployementRequestMyCompanyControllers = new List<MvcControllerInfo>();
            List<MvcControllerInfo> MultiMediaControllers = new List<MvcControllerInfo>();
            List<MvcControllerInfo> AccessControlControllers = new List<MvcControllerInfo>();
            List<MvcControllerInfo> HalycoControllers = new List<MvcControllerInfo>();
            List<MvcControllerInfo> SalaryControllers = new List<MvcControllerInfo>();
            List<MvcControllerInfo> ProposalControllers = new List<MvcControllerInfo>();
            List<MvcControllerInfo> SuppliersControllers = new List<MvcControllerInfo>();
            List<MvcControllerInfo> CustomerControllers = new List<MvcControllerInfo>();

            foreach (var controller in Allcontroller)
            {
                List<MvcActionInfo> MainActions = new List<MvcActionInfo>();

                if (!ControllerDictionary.HideController(controller.Name))
                {
                    if (MainControllers.Any(x => x.Name == controller.Name)) continue;

                    //Have Same Controllers
                    if (Allcontroller.Any(x => x.Name == controller.Name))
                    {
                        var listsameControllers = Allcontroller.Where(x => x.Name == controller.Name).ToList();
                        foreach (var sameControllers in listsameControllers)
                        {
                            foreach (var action in sameControllers.Actions)
                            {
                                if (!MainActions.Any(x => x.Name == action.Name))
                                { MainActions.Add(action); }
                            }
                        }
                        controller.Actions = MainActions;
                        MainControllers.Add(controller);

                    }

                    else
                    {
                        MainControllers.Add(controller);
                    }
                }
            }
            //Seprate
          
            foreach (var item in MainControllers.ToList())
            {
                if (ControllerDictionary.IsSubMenu_Product(item.Name))
                {
                    ProductControllers.Add(item);
                    MainControllers.Remove(item);
                }
                else if (ControllerDictionary.IsSubMenu_Content(item.Name))
                {
                    ContentControllers.Add(item);
                    MainControllers.Remove(item);
                }
                else if (ControllerDictionary.IsSubMenu_Connention(item.Name))
                {
                    ConnectionControllers.Add(item);
                    MainControllers.Remove(item);
                }
                else if (ControllerDictionary.IsSubMenu_BaseInformation(item.Name))
                {
                    BaseInformationControllers.Add(item);
                    MainControllers.Remove(item);
                }
                else if (ControllerDictionary.IsSubMenu_Employee(item.Name))
                {
                    EmployeeControllers.Add(item);
                    MainControllers.Remove(item);
                }
                else if (ControllerDictionary.IsSubMenu_EmployeeCoworker(item.Name))
                {
                    EmployeeCoworkerControllers.Add(item);
                    MainControllers.Remove(item);
                }
                else if (ControllerDictionary.IsSubMenu_Survey(item.Name))
                {
                    SurveyControllers.Add(item);
                    MainControllers.Remove(item);
                }
                else if (ControllerDictionary.IsSubMenu_Employement(item.Name))
                {
                    EmployementControllers.Add(item);
                    MainControllers.Remove(item);
                }
                else if (ControllerDictionary.IsSubMenu_EmployementRequestMySection(item.Name))
                {
                    EmployementRequestMySectionControllers.Add(item);
                    MainControllers.Remove(item);
                }
                else if (ControllerDictionary.IsSubMenu_EmployementRequestMyUnit(item.Name))
                {
                    EmployementRequestMyUnitControllers.Add(item);
                    MainControllers.Remove(item);
                }
                else if (ControllerDictionary.IsSubMenu_EmployementRequestMyCompany(item.Name))
                {
                    EmployementRequestMyCompanyControllers.Add(item);
                    MainControllers.Remove(item);
                }
                else if (ControllerDictionary.IsSubMenu_MultiMedia(item.Name))
                {
                    MultiMediaControllers.Add(item);
                    MainControllers.Remove(item);
                }
                else if (ControllerDictionary.IsSubMenu_AccessControl(item.Name))
                {
                    AccessControlControllers.Add(item);
                    MainControllers.Remove(item);
                }
                else if (ControllerDictionary.IsSubMenu_Halyco(item.Name))
                {
                    HalycoControllers.Add(item);
                    MainControllers.Remove(item);
                }
                else if (ControllerDictionary.IsSubMenu_Salary(item.Name))
                {
                    SalaryControllers.Add(item);
                    MainControllers.Remove(item);
                }
                else if (ControllerDictionary.IsSubMenu_Proposal(item.Name))
                {
                    ProposalControllers.Add(item);
                    MainControllers.Remove(item);
                }
                else if (ControllerDictionary.IsSubMenu_Suppliers(item.Name))
                {
                    SuppliersControllers.Add(item);
                    MainControllers.Remove(item);
                }
                else if (ControllerDictionary.IsSubMenu_Customer(item.Name))
                {
                    CustomerControllers.Add(item);
                    MainControllers.Remove(item);
                }
            }

            //
            viewModel.ConnectionControllers = ConnectionControllers;
            viewModel.ContentControllers = ContentControllers;
            viewModel.ProductControllers = ProductControllers;
            viewModel.BaseInformationControllers = BaseInformationControllers;
            viewModel.UserFullName = user.FullName;
            viewModel.SelectedControllers = MainControllers;
            viewModel.EmployeeControllers = EmployeeControllers;
            viewModel.SurveyControllers = SurveyControllers;
            viewModel.EmployementControllers = EmployementControllers;
            viewModel.EmployeeCoworkerControllers = EmployeeCoworkerControllers;
            viewModel.EmployementRequestMySectionControllers = EmployementRequestMySectionControllers;
            viewModel.EmployementRequestMyUnitControllers = EmployementRequestMyUnitControllers;
            viewModel.EmployementRequestMyCompanyControllers = EmployementRequestMyCompanyControllers;
            viewModel.MultiMediaControllers = MultiMediaControllers;
            viewModel.AccessControlControllers = AccessControlControllers;
            viewModel.HalycoControllers = HalycoControllers;
            viewModel.SalaryControllers = SalaryControllers;
            viewModel.ProposalControllers = ProposalControllers;
            viewModel.SuppliersControllers = SuppliersControllers;
            viewModel.CustomerControllers = CustomerControllers;
            return View(viewModel);
        }
    }
}
