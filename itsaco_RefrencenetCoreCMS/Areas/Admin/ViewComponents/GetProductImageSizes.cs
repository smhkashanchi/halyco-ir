﻿using DomainClasses.Product;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspCorePanelProject.Areas.Admin.ViewComponents
{
    public class GetProductImageSizes:ViewComponent
    {
        public IGenericRepository<ProductsGroup> _productsGroupRepository;
        public GetProductImageSizes(IGenericRepository<ProductsGroup> productsGroupRepository)
        {
            _productsGroupRepository = productsGroupRepository;
        }
        public async Task<IViewComponentResult> InvokeAsync(short Id)
        {
            var group =await _productsGroupRepository.GetByIdAsync((short)Id);
            return await Task.FromResult((IViewComponentResult)View("GetProductImageSizes", group));
        }
    }
}
