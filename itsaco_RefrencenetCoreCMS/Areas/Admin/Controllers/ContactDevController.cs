﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services;
using Services.Repositories;
using ViewModels;

namespace AspCorePanelProject.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]
    [DisplayName("ارتباط با توسعه دهنده")]
    public class ContactDevController : Controller
    {
        private static IHttpContextAccessor HttpContextAccessor;
        public IEmailSender _emailsender;
        public IViewRender _viewRender;
        public ContactDevController(IEmailSender emailsender, IViewRender viewRender, IHttpContextAccessor httpContextAccessor)
        {
            _emailsender = emailsender;
            _viewRender = viewRender;
            HttpContextAccessor = httpContextAccessor;
        }
        private static Uri GetAbsoluteUri()
        {
            var request = HttpContextAccessor.HttpContext.Request;
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = request.Scheme;
            uriBuilder.Host = request.Host.Host;
            uriBuilder.Path = request.Path.ToString();
            uriBuilder.Query = request.QueryString.ToString();
            return uriBuilder.Uri;
        }
    [DisplayName("ارسال پیام ")]
    [ActionName("SendMessage")]
        public IActionResult Index()
        {
            return View("Index");
        }
        [HttpPost]
        public async Task<JsonResult> SendMessage(ContactWithDeveloperVM person, IFormFile atFile)
        {
            string filePath = "";
            if (person != null)
            {
                if (atFile != null)
                {
                    string fileName = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(atFile.FileName);
                    filePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/EmailFiles", fileName);
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await atFile.CopyToAsync(stream);
                    }

                }
                //// گرفتن یو آر ال صفحه
                ///
                Uri url = GetAbsoluteUri();
                person.PageUrl = url.AbsoluteUri.ToString();

                /////////////Send Email
                /////برای ارسال ایمیل ابتدا دیپندنسی های اون رو داخل فایل اپ ستینگ ست کرده
                ///بعد از اون با ریپوزیتوری و سرویس بصورت اسنکرون پیاده سازی کرده
                ///
                if (filePath != "")
                {
                    string body = await _viewRender.RenderToStringAsync("ManageEmail/EmailBody", person);
                    await _emailsender.SendEmailWithFileAsync("elahi2mahdi@yahoo.com", "بخش ارتباط با توسعه دهنده", body, filePath);
                }
                else
                {
                    //SendMail.Send(person.Email.Trim(), "amirhosseink.7795@gmail.com", "بخش ارتباط با برنامه نویس", person.MessageText, person.Name,filePath);
                    var body = await _viewRender.RenderToStringAsync("ManageEmail/MailBody", person).ConfigureAwait(false);
                    await _emailsender.SendEmailAsync("elahi2mahdi@yahoo.com", "بخش ارتباط با توسعه دهنده", body);
                }
                return Json(Ok());
            }
            return Json("NOK");

        }
    }
}