﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Employees;
using DomainClasses.Role;
using DomainClasses.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("وام")]
    public class LoanController : Controller
    {
        public UserManager<ApplicationUser> _userManager;
        public RoleManager<ApplicationRole> _roleManager;
        public IGenericRepository<Employee> _empgenericRepository;
        public IGenericRepository<ApplicationRole> _ApplicationRoleRepository;
        public IGenericRepository<ApplicationUser> _userRepository;
        public IGenericRepository<Loan> _LoanRepository;
        public static byte SupervionLevel;
        public LoanController(UserManager<ApplicationUser> userManager, IGenericRepository<Employee> empgenericRepository, IGenericRepository<ApplicationRole> ApplicationRoleRepository, RoleManager<ApplicationRole> roleManager, IGenericRepository<ApplicationUser> userRepository, IGenericRepository<Loan> LoanRepository)
        {
            _userManager = userManager;
            _empgenericRepository = empgenericRepository;
            _ApplicationRoleRepository = ApplicationRoleRepository;
            _roleManager = roleManager;
            _userRepository = userRepository;
            _LoanRepository = LoanRepository;
        }

        [DisplayName("صفحه نخست")]
        public IActionResult Index()
        {
            return View();
        }

        [DisplayName("ثبت وام")]
        public async Task<IActionResult> Create()
        {
            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                where: x => x.UserIdentity_Id == user.Id
                );
            if (user != null && employee.FirstOrDefault() != null)
            {
                ViewBag.FullName = user.FullName;
                ViewBag.PersonalCode = employee.FirstOrDefault().PersonalCode;
                SupervionLevel = employee.FirstOrDefault().SupervisionLevel.Value;
            }
            return PartialView();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Loan loan)
        {
            loan.CreateDate = DateTime.Now;
            if (SupervionLevel == 2)
            {
                loan.StatusSuperiorSignature = true;
            }
            await _LoanRepository.AddAsync(loan);
            await _LoanRepository.SaveChangesAsync();

            var loan2 = await checkSpecialState(loan);
            _LoanRepository.Update(loan2);
            await _LoanRepository.SaveChangesAsync();

            return Json(new { status = "ok" });
        }

        [NonAction]
        ///برای حالت های خاص مثل اینکه درخواست های روسا توسط رئیس اداری به صورت مستقیم بررسی شود.
        public async Task<Loan> checkSpecialState(Loan loan)
        {
            var temp = await _LoanRepository.GetWithIncludeAsync<Loan>(
                selector: x => x,
                where: x => x.loanID == loan.loanID,
                include: x => x.Include(s => s.Employee_lo_1)
                );
            Loan temploan = temp.FirstOrDefault();
            var user = await _userManager.FindByIdAsync(temploan.Employee_lo_1.UserIdentity_Id);
            var userRoles = await _userManager.GetRolesAsync(user);
            if (userRoles.Contains("رئیس برنامه ریزی و توسعه و بهبود") ||
                userRoles.Contains("رئیس تولید") ||
                userRoles.Contains("رئیس فنی") ||
                userRoles.Contains("رئیس فروش") ||
                userRoles.Contains("رئیس حسابداری") ||
                userRoles.Contains("مدیر عامل") ||
                userRoles.Contains("رئیس بازرگانی خارجی") ||
                userRoles.Contains("مدیر بهره برداری") ||
                userRoles.Contains("رئیس دفتر مدیر عامل")
                )
                temploan.StatusManagerSignature = true;

            return temploan;
        }

        [DisplayName("لیست درخواست های وام")]
        public async Task<IActionResult> ViewAll()
        {
            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                where: x => x.UserIdentity_Id == user.Id
                );

            var ExraWorks = await _LoanRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.Employee_lo_3).ThenInclude(s => s.ApplicationUser).Include(s => s.Employee_lo_4).ThenInclude(s => s.ApplicationUser).Include(s => s.Employee_lo_5).ThenInclude(s => s.ApplicationUser),
                where: x => x.PersonalCode == employee.FirstOrDefault().PersonalCode,
                orderBy: x => x.OrderByDescending(s => s.loanID));
            return PartialView(ExraWorks);
        }

        [DisplayName("ویرایش وام")]
        public async Task<IActionResult> Edit(int id)
        {
            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.OfficeUnit),
                where: x => x.UserIdentity_Id == user.Id
                );
            if (user != null && employee.FirstOrDefault() != null)
            {
                ViewBag.FullName = user.FullName;
                ViewBag.PersonalCode = employee.FirstOrDefault().PersonalCode;
                SupervionLevel = employee.FirstOrDefault().SupervisionLevel.Value;
                ViewBag.SupervionLevel = SupervionLevel;
            }

            var loan = await _LoanRepository.GetAsync(x => x.loanID == id);
            return PartialView(loan);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Loan loan)
        {
            _LoanRepository.Update(loan);
            await _LoanRepository.SaveChangesAsync();
            return Json(new { status = "ok" });
        }

        [DisplayName("حذف")]
        public async Task<IActionResult> Delete(int id)
        {
            var Loan = await _LoanRepository.GetByIdAsync(id);
            if (Loan != null)
            {
                _LoanRepository.Delete(Loan);
                await _LoanRepository.SaveChangesAsync();
                return Json("ok");
            }
            return Json("Nok");
        }

    }
}