﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Product;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;

namespace AspCorePanelProject.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]
    [DisplayName("گارانتی")]
    public class GarantyController : Controller
    {
        public IGenericRepository<Garanty> _garantyRepository;
        public GarantyController(IGenericRepository<Garanty> garantyRepository)
        {
            _garantyRepository = garantyRepository;
        }
        [DisplayName("صفحه نخست")]

        public IActionResult Index()
        {
            return View();
        }
        [DisplayName("لیست گارانتی ها")]

        public async Task<IActionResult> ViewAll(string id)
        {
            return PartialView(await _garantyRepository.GetAllAsync(x => x.Lang == id));
        }
        [DisplayName("افزودن ")]

        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Garanty garanty, IFormFile imgGaranty)
        {
            if (garanty != null)
            {
                if (imgGaranty != null)
                {
                    if (!Directory.Exists("wwwroot/Files/Garanty"))
                    {
                        Directory.CreateDirectory("wwwroot/Files/Garanty");
                    }
                    garanty.Icon = imgGaranty.FileName.Substring(0, imgGaranty.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(imgGaranty.FileName);
                    string filePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Garanty", garanty.Icon);
                    //string filePathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Contents/Thumb", contents.ContentImage);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await imgGaranty.CopyToAsync(stream);
                    }
                }


                await _garantyRepository.AddAsync(garanty);
                await _garantyRepository.SaveChangesAsync();
                return Json(garanty);
            }
            return Json("nall");
        }
        [DisplayName("ویرایش ")]

        public async Task<IActionResult> Edit(Int16 id)
        {
            return View(await _garantyRepository.GetByIdAsync(id));
        }
        [HttpPost]
        public async Task<IActionResult> Edit(Garanty garanty, IFormFile imgGaranty)
        {
            if (garanty != null)
            {
                if (imgGaranty != null)
                {
                    if (garanty.Icon != null)
                    {
                        string oldfilePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Garanty", garanty.Icon);
                        await DeleteFilesInProj(oldfilePath);
                    }

                    garanty.Icon = imgGaranty.FileName.Substring(0, imgGaranty.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(imgGaranty.FileName);
                    string filePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Garanty", garanty.Icon);
                    //string filePathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Contents/Thumb", contents.ContentImage);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await imgGaranty.CopyToAsync(stream);
                    }
                }
                _garantyRepository.Update(garanty);
                await _garantyRepository.SaveChangesAsync();
                return Json(garanty);
            }
            return Json("nall");
        }

        [DisplayName("حذف ")]

        public async Task<IActionResult> Delete(Int16 id)
        {
            var garanty = await _garantyRepository.GetByIdAsync(id);
            if (garanty != null)
            {

                if (garanty.Icon != null)
                {
                    string oldfilePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Garanty", garanty.Icon);
                    await DeleteFilesInProj(oldfilePath);
                }
                _garantyRepository.Delete(garanty);
                await _garantyRepository.SaveChangesAsync();
                return Json(garanty);

            }
            return Json("no");
        }
        public async Task<IActionResult> DeleteFilesInProj(string path, string pathThumb = "")
        {
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            if (System.IO.File.Exists(pathThumb))
            {
                System.IO.File.Delete(pathThumb);
            }
            return Json(Ok());
        }
    }
}