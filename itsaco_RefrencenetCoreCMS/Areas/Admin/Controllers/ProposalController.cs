﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.BasicInformation;
using DomainClasses.Employees;
using DomainClasses.Proposal;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName(" پیشنهاد ها")]
    public class ProposalController : Controller
    {
        public IGenericRepository<OfficeUnit> _officeUnitRepository;
        public IGenericRepository<OfficePost> _officePostRepository;
        public IGenericRepository<Employee> _employeeRepository;
        public IGenericRepository<MainProposal> _mainProposalRepository;
        public IGenericRepository<ProposalFiles> _proposalFilesRepository;
        public IGenericRepository<ProposalProviders> _proposalProvidersRepository;
        public ProposalController(IGenericRepository<Employee> employeeRepository,IGenericRepository<ProposalProviders> proposalProvidersRepository,IGenericRepository<ProposalFiles> proposalFilesRepository,IGenericRepository<MainProposal> mainProposalRepository,IGenericRepository<OfficeUnit> officeUnitRepository, IGenericRepository<OfficePost> officePostRepository)
        {
            _officeUnitRepository = officeUnitRepository;
            _officePostRepository = officePostRepository;
            _mainProposalRepository = mainProposalRepository;
            _proposalFilesRepository = proposalFilesRepository;
            _proposalProvidersRepository = proposalProvidersRepository;
            _employeeRepository = employeeRepository;
        }
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> ProposalOffer()
        {
            ViewBag.AllUnits = new SelectList(await _officeUnitRepository.GetAllAsync(), "OfficeUnitID", "OfficeUnitTitle");
            ViewBag.AllPosts = new SelectList(await _officePostRepository.GetAllAsync(), "OfficePostID", "OfficePostTitle");

            return PartialView();
        }
        public IActionResult MainProposal()
        {
            return PartialView();
        }

        public IActionResult FileProposal()
        {
            return PartialView();
        }
        [HttpPost]
        public async Task<IActionResult> DocumentUpload(IFormFile docFile)
        {
            if (!Directory.Exists("wwwroot/Files/Proposal/temp"))
            {
                Directory.CreateDirectory("wwwroot/Files/Proposal/temp");
            }

            //await RemoveTempFiles();
            string fileName = "";
            Random rnd = new Random();

            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Proposal/temp");

            fileName = rnd.Next(1, 99999).ToString() + docFile.FileName.Replace(" ", "_").Replace("(", "").Replace(")", "");
            using (var stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
            {
                docFile.CopyTo(stream);
            }
            return Json(new { status = "ok", fn = fileName });
        }
        public async Task<IActionResult> CheckIsExsistEmployee(string id)
        {
            if(await _employeeRepository.Exists(x => x.PersonalCode == id.Trim()))
            {
                var emp = await _employeeRepository.GetWithIncludeAsync(
                    selector:x=>x,
                    include:x=>x.Include(s=>s.ApplicationUser),
                    where: x => x.PersonalCode == id.Trim()
                    );

                return Json(new { name = emp.FirstOrDefault().ApplicationUser.FullName});
            }
            return Json("no_exsist");
        }
        [HttpPost]
        public async Task<IActionResult> ProposalRegister(MainProposal mainProposal, List<string> docTitle, List<string> docFile, List<string> units, List<string> posts, List<string> personalCodes, List<string> phones)
        {
            ProposalFiles pf = new ProposalFiles();
            ProposalProviders pp = new ProposalProviders();
            try
            {
                await _mainProposalRepository.AddAsync(mainProposal);
                await _mainProposalRepository.SaveChangesAsync();

                Int16 ProposalId = mainProposal.Proposal_ID;

                ///Document
                ///
                for (int i = 0; i < docFile.Count; i++)
                {
                    var documents = await _proposalFilesRepository.GetAllAsync();
                    Int16 zero = 0;
                    Int16 DocumentId = documents.LastOrDefault() != null ? documents.LastOrDefault().P_File_ID : zero;

                    pf.P_File_ID = (short)(DocumentId + 1);

                    pf.P_FileName = docFile[i];
                    if (docTitle[i] != "")
                    {
                        pf.Title = docTitle[i];
                    }
                    pf.ProposalId = ProposalId;

                    await _proposalFilesRepository.AddAsync(pf);
                    await _proposalFilesRepository.SaveChangesAsync();
                }
                ///Provider
                ///
                for (int i = 0; i < personalCodes.Count; i++)
                {
                    var providers = await _proposalProvidersRepository.GetAllAsync();
                    int zero = 0;
                    int ProviderId = providers.LastOrDefault() != null ? providers.LastOrDefault().PP_ID : zero;

                    pp.PP_ID = ProviderId + 1;

                    pp.PersonalCode = personalCodes[i];
                    if (phones[i] != "")
                    {
                        pp.PhoneNumber = phones[i];
                    }
                    pp.UnitId = Convert.ToInt16(units[i]);
                    pp.PostId = Convert.ToInt16(posts[i]);
                    pp.ProposalId = ProposalId;

                    await _proposalProvidersRepository.AddAsync(pp);
                    await _proposalProvidersRepository.SaveChangesAsync();
                }
                await SaveToMainFolder();
            }
            catch (Exception err)
            {

                throw;
            }

            return Json("ok");
        }
        public async Task<IActionResult> SaveToMainFolder()
        {
            string fileName = "";
            string destFile = "";
            string sourcePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Proposal/temp");
            string targetPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Proposal");
            if (System.IO.Directory.Exists(sourcePath))
            {
                string[] files = System.IO.Directory.GetFiles(sourcePath);

                // Copy the files. 

                foreach (string file in files)
                {
                    fileName = System.IO.Path.GetFileName(file);
                    destFile = System.IO.Path.Combine(targetPath, fileName);     //ایجاد مسیر و نام فایل برای ذخیره عکس اصلی
                    System.IO.File.Copy(file, destFile, true);                   //ذخیره بدون تغییر سایز عکس در مسیر اصلی

                    System.IO.File.Delete(file);
                }

                //await RemoveTempFiles();
            }
            return Ok();
        }
        public IActionResult DeleteDocumentUpload(string fn)
        {
            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Proposal/temp");
            string myFile = Path.Combine(path, fn);
            if (System.IO.File.Exists(myFile))
            {
                System.IO.File.Delete(myFile);
            }
            return Json("ok");
        }


    }
}