﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Survey;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("گروه سوالات نظرسنجی")]
    public class SurveyQuestionGroupController : Controller
    {
        public IGenericRepository<SurvayQuestionGroup> _survayQuestionGroupRepository;
        public IGenericRepository<MainSurvey> _mainSurveyRepository;
        public IGenericRepository<SurveyQuestion> _surveyQuestionRepository;
        public SurveyQuestionGroupController(IGenericRepository<SurveyQuestion> surveyQuestionRepository,IGenericRepository<SurvayQuestionGroup> survayQuestionGroupRepository, IGenericRepository<MainSurvey> mainSurveyRepository)
        {
            _mainSurveyRepository = mainSurveyRepository;
            _survayQuestionGroupRepository = survayQuestionGroupRepository;
            _surveyQuestionRepository = surveyQuestionRepository;
        }
        public IActionResult Index()
        {
            return View();
        }
        [DisplayName("لیست نظرسنجی های اصلی")]
        public async Task<JsonResult> GetAllMainSurvey()
        {
            return Json(await _mainSurveyRepository.GetAllAsync(null, x => x.OrderByDescending(s => s.MainSurvey_ID)));
        }
        [DisplayName("لیست گروه سوالات نظرسنجی")]
        public async Task<IActionResult> ViewAll(int id)
        {
            return PartialView(await _survayQuestionGroupRepository.GetAllAsync(x => x.MainSurveyId == id, x => x.OrderByDescending(s => s.SurveyGroup_ID)));
        }
        [DisplayName("افزودن گروه سوالات نظرسنجی")]
        public async Task<IActionResult> Create()
        {
            ViewBag.MainSurvey = new SelectList(await _mainSurveyRepository.GetAllAsync(), "MainSurvey_ID", "Title");
            return PartialView();
        }
        [HttpPost]
        public async Task<IActionResult> Create(SurvayQuestionGroup survayQuestionGroup)
        {
            await _survayQuestionGroupRepository.AddAsync(survayQuestionGroup);
            await _survayQuestionGroupRepository.SaveChangesAsync();
            return Json(new { status = "ok", msurveyId = survayQuestionGroup.MainSurveyId });
        }
        [DisplayName("ویرایش گروه سوالات نظرسنجی")]
        public async Task<IActionResult> Edit(int id)
        {
            var sqGroup = await _survayQuestionGroupRepository.GetAsync(x => x.SurveyGroup_ID == id);
            ViewBag.MainSurvey = new SelectList(await _mainSurveyRepository.GetAllAsync(), "MainSurvey_ID", "Title",sqGroup.MainSurveyId);
            return PartialView(sqGroup);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(SurvayQuestionGroup survayQuestionGroup)
        {
             _survayQuestionGroupRepository.Update(survayQuestionGroup);
            await _survayQuestionGroupRepository.SaveChangesAsync();
            return Json(new { status = "ok", msurveyId = survayQuestionGroup.MainSurveyId });
        }
        [DisplayName("حذف گروه سوالات نظرسنجی")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!await _surveyQuestionRepository.Exists(x => x.QuestionGroupId == id))
            {
                var quesGroupSurvey = await _survayQuestionGroupRepository.GetAsync(x => x.SurveyGroup_ID == id);
                if (quesGroupSurvey != null)
                {
                    _survayQuestionGroupRepository.Delete(quesGroupSurvey);
                    await _survayQuestionGroupRepository.SaveChangesAsync();
                    return Json("ok");
                }
                return Json("nall");
            }
            return Json("ex_ques");
        }
    }
}