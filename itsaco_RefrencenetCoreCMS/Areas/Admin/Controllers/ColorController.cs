﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Product;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories;

namespace AspCorePanelProject.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]
    [DisplayName("رنگ")]


    public class ColorController : Controller
    {
        public IGenericRepository<Color> _colorRepository;
        public ColorController(IGenericRepository<Color> colorRepository)
        {
            _colorRepository = colorRepository;
        }
        [DisplayName("صفحه نخست")]
        public IActionResult Index()
        {
            return View();
        }
        [DisplayName("لیست رنگ ها")]
        public async Task<IActionResult> ViewAll(string id)
        {
            var list = await _colorRepository.GetAllAsync(x => x.Lang == id);
            return PartialView(list.OrderByDescending(x => x.ColorTitle));
        }
        [DisplayName("افزودن")]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Color color)
        {
            if (ModelState.IsValid)
            {
                if (!await _colorRepository.Exists(x=>x.ColorCode==color.ColorCode.Trim()))
                {
                    await _colorRepository.AddAsync(color);
                    await _colorRepository.SaveChangesAsync();
                    return Json(color);
                }
                return Json("repeate");
            }
            //return Json("nall");
            return View(color);
        }
        [DisplayName("ویرایش")]

        public async Task<IActionResult> Edit(short id)
        {
            return View( await _colorRepository.GetByIdAsync(id));
        }
        [HttpPost]
        public async Task<IActionResult> Edit(Color color)
        {
            if (color != null)
            {

                _colorRepository.Update(color);
                await _colorRepository.SaveChangesAsync();
                return Json(color);

            }
            return View(color);
        }
        [DisplayName("حذف")]
        public async Task<IActionResult> Delete(short id)
        {
            var color = await _colorRepository.GetByIdAsync(id);
            if (color != null)
            {
                _colorRepository.Delete(color);
                await _colorRepository.SaveChangesAsync();
                return Json(color);
            }
            return Json("Nok");
        }
    }
}