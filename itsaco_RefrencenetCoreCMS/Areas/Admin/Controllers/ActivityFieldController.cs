﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DataLayer;
using DomainClasses.BasicInformation;
using System.ComponentModel;
using Microsoft.AspNetCore.Authorization;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("Admin")]
    [DisplayName("زمینه فعالیت")]
    public class ActivityFieldController : Controller
    {
        public IGenericRepository<ActivityField> _ActivityFieldRepository;

        public ActivityFieldController(IGenericRepository<ActivityField> ActivityFieldRepository)
        {
            _ActivityFieldRepository = ActivityFieldRepository;
        }

        [DisplayName("صفحه نخست ")]
        public IActionResult Index()
        {
            return View();
        }
        [DisplayName("لیست زمینه های فعالیت ")]
        public async Task<IActionResult> ViewAll(string id)
        {
            return PartialView(await _ActivityFieldRepository.GetAllAsync(null, x => x.OrderByDescending(s => s.ActivityFieldID)));
        }
        [DisplayName("افزودن  ")]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(ActivityField activityField)
        {
            if (activityField != null)
            {
                await _ActivityFieldRepository.AddAsync(activityField);
                await _ActivityFieldRepository.SaveChangesAsync();
                return Json(activityField);
            }
            return View(activityField);
        }
        [DisplayName("ویرایش  ")]
        public async Task<IActionResult> Edit(short id)
        {
            return View(await _ActivityFieldRepository.GetByIdAsync(id));
        }
        [HttpPost]
        public async Task<IActionResult> Edit(ActivityField activityField)
        {
            if (activityField != null)
            {
                _ActivityFieldRepository.Update(activityField);
                await _ActivityFieldRepository.SaveChangesAsync();
                return Json(activityField);
            }
            return View(activityField);
        }
        public async Task<IActionResult> Delete(short id)
        {
            var activityField = await _ActivityFieldRepository.GetByIdAsync(id);
            if (activityField != null)
            {
                _ActivityFieldRepository.Delete(activityField);
                await _ActivityFieldRepository.SaveChangesAsync();
                return Json(new { status = "ok", title = activityField.ActivityFieldTitle });
            }
            return Json("null");
        }
    }
}