﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Survey;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("نوع جواب نظرسنجی")]
    public class SurveyAnswerTypeController : Controller
    {

        public IGenericRepository<SurveyAnswerType> _surveyAnswerTypeRepository;
        public IGenericRepository<SurveyQuestion> _surveyQuestionRepository;
        public SurveyAnswerTypeController(IGenericRepository<SurveyAnswerType> surveyAnswerTypeRepository, IGenericRepository<SurveyQuestion> surveyQuestionRepository)
        {
            _surveyAnswerTypeRepository =surveyAnswerTypeRepository;
            _surveyQuestionRepository = surveyQuestionRepository;
        }
        public IActionResult Index()
        {
            return View();
        }
        [DisplayName("نمایش نوع جواب ها")]
        public async Task<IActionResult> ViewAll()
        {
            return PartialView(await _surveyAnswerTypeRepository.GetAllAsync());
        }
        [DisplayName("افزودن نوع جواب")]
        public IActionResult Create()
        {
            return PartialView();
        }
        [HttpPost]
        public async Task<IActionResult> Create(SurveyAnswerType surveyAnswerType)
        {
            try
            {
                await _surveyAnswerTypeRepository.AddAsync(surveyAnswerType);
                await _surveyAnswerTypeRepository.SaveChangesAsync();
                return Json("ok");
            }
            catch (Exception err)
            {
                throw err;
            }
            
        }

        [DisplayName("ویرایش نوع جواب")]
        public async Task<IActionResult> Edit(int id)
        {
            return PartialView(await _surveyAnswerTypeRepository.GetAsync(x=>x.AnswerType_ID==id));
        }
        [HttpPost]
        public async Task<IActionResult> Edit(SurveyAnswerType surveyAnswerType)
        {
            try
            {
                 _surveyAnswerTypeRepository.Update(surveyAnswerType);
                await _surveyAnswerTypeRepository.SaveChangesAsync();
                return Json("ok");
            }
            catch (Exception err)
            {
                throw err;
            }

        }
        [DisplayName("حذف نوع جواب نظرسنجی")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!await _surveyQuestionRepository.Exists(x => x.AnswerTypeId == id))
            {
                var answerType = await _surveyAnswerTypeRepository.GetAsync(x => x.AnswerType_ID == id);
                if (answerType != null)
                {
                    _surveyAnswerTypeRepository.Delete(answerType);
                    await _surveyAnswerTypeRepository.SaveChangesAsync();
                    return Json("ok");
                }
                return Json("nall");
            }
            return Json("ex_ques");
        }
    }
}