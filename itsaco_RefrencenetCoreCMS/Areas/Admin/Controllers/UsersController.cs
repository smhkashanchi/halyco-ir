﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DataLayer;
using DomainClasses.Role;
using Services.Repositories;
using DomainClasses.User;
using Microsoft.AspNetCore.Identity;
using ViewModels.UsersViewModels;
using Utilities.DatePersion;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel;
using Microsoft.AspNetCore.Http;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    [DisplayName("مدیریت کاربران ")]
    public class UsersController : Controller
    {
        IGenericRepository<ApplicationUser> _userRepository;
        UserManager<ApplicationUser> _userManager;

        public UsersController(IGenericRepository<ApplicationUser> userRepository,
            UserManager<ApplicationUser> userManager)
        {
            _userRepository = userRepository;
            _userManager = userManager;
        }
        [DisplayName("صفحه نخست")]

        // GET: Admin/Users
        public async Task<IActionResult> Index()
        {

            return View();
        }
        [DisplayName("لیست کاربران")]

        public async Task<ActionResult> ViewAll(string id)
        {
            return PartialView(await _userManager.GetUsersInRoleAsync("SubAdmin"));
        }
      
        [DisplayName("افزودن")]

        // GET: Admin/Users/Create
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(UserViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
               
                if (await _userRepository.Exists(f => f.Email == viewModel.Email) == true)
                {
                    return Json(new { message = "ایمیل قبلا برای فرد دیگری ثبت شده.", res = false });
                }
                else if (await _userRepository.Exists(f => f.UserName == viewModel.UserName) == true)
                {
                    return Json(new { message = "نام کاربری قبلا برای فرد دیگری ثبت شده.", res = false });
                }
                var user = new ApplicationUser
                {
                    UserName = viewModel.UserName,
                    Email = viewModel.Email,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    EmailConfirmed = true,
                    FullName = viewModel.FullName,
                    Mobile_1 = viewModel.Mobile_1,
                    RegisterDate = ConvertDate.GetDatePersion(),
                    Mobile_2 = viewModel.Mobile_2,
                    UserPhoto = "/Files/UsersPhoto/no-profile.jpg",
                    PhoneNumber = viewModel.PhoneNumber,
                };

                var result = await _userManager.CreateAsync(user, viewModel.Password);
                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, "SubAdmin");
                    return Json(user);
                }
                else
                {
                    return Json(new { message = "رمز عبور شما باید  دارای  عدد و حروف کوچک و بزرگ انگلیسی باشد ", res = false });
                }

            }
            return View(viewModel);
        }
        [DisplayName("ویرایش")]

        // GET: Admin/Users/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userRepository.GetAsync(x => x.Id == id);
            var viewModel = new UserEditViewModel()
            {
                User_Id = user.Id,
                Mobile_1 = user.Mobile_1,
                Mobile_2 = user.Mobile_2,
                FullName = user.FullName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                UserName = user.UserName
            };

            if (user == null)
            {
                return NotFound();
            }
            return View(viewModel);
        }

        // POST: Admin/Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, UserEditViewModel viewModel)
        {

            if (ModelState.IsValid)
            {

               if (await _userRepository.Exists(f => f.Email == viewModel.Email && f.Id != viewModel.User_Id) == true)
                {
                    return Json(new { message = "ایمیل قبلا برای فرد دیگری ثبت شده", res = false });
                }
                else if (await _userRepository.Exists(f => f.UserName == viewModel.UserName && f.Id != viewModel.User_Id) == true)
                {
                    return Json(new { message = "نام کاربری قبلا برای فرد دیگری ثبت شده", res = false });
                }

                var user = await _userRepository.GetAsync(x => x.Id == id);

                user.UserName = viewModel.UserName;
                user.PhoneNumber = viewModel.PhoneNumber;
                user.FullName = viewModel.FullName;
                user.Mobile_1 = viewModel.Mobile_1;
                user.Mobile_2 = viewModel.Mobile_2;
                user.Email = viewModel.Email;

                try
                {
                    await _userManager.SetEmailAsync(user, user.Email);
                    await _userManager.SetUserNameAsync(user, user.UserName);
                    user.EmailConfirmed = true;
                    await _userManager.UpdateAsync(user);

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await ApplicationRoleExists(viewModel.User_Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return Json(new { res=true,model=user });
            }
            return View(viewModel);
        }
        [DisplayName("حذف")]

        //// GET: Admin/Users/Delete/5
        //public async Task<IActionResult> Delete(string id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }


        //    var user = await _userManager.FindByIdAsync(id);
        //    await _userManager.DeleteAsync(user);

        //    if (user == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(user);
        //}

        //// POST: Admin/Users/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(string id)
        //{
        //    var applicationRole = await _context.Roles.FindAsync(id);
        //    _context.Roles.Remove(applicationRole);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction(nameof(Index));
        //}
        [NonAction]
        private Task<bool> ApplicationRoleExists(string id)
        {
            return  _userRepository.Exists(e => e.Id == id);
        }

        [DisplayName("تغییر رمز عبور کاربر توسط مدیر")]
        [HttpGet]
        public async Task<IActionResult> changeEmployeePassword(string id) {
            var user =await _userRepository.GetByIdAsync(id);
            ViewBag.fullName = user.FullName;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> changeEmployeePassword(string id,string currentAdminPassword,string newEmployeePassword)
        {
            string pass = HttpContext.Session.GetString("currentUserPass");
            if (pass == currentAdminPassword)
            {
                var user = await _userRepository.GetByIdAsync(id);
                var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                await _userManager.ResetPasswordAsync(user, token, newEmployeePassword);
                return Json(user);
            }
            else
            {
                return Json("adminPasswordIncorrect");
            }
            
        }
    }
}
