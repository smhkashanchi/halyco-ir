﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Suppliers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("تامین کنندگان")]
    public class SuppliersController : Controller
    {
        public IGenericRepository<MainSuppliers> _mainSuppliersRepository;
        public IGenericRepository<SupplierShareholders> _supplierShareholdersRepository;
        public IGenericRepository<SuppliersActivityFields> _suppliersActivityFieldsRepository;
        public IGenericRepository<SuppliersResume> _suppliersResumeRepository;
        public IGenericRepository<SuppliersCertificates> _suppliersCertificatesRepository;
        public IGenericRepository<SuppliersFiles> _suppliersFilesRepository;
        public SuppliersController(IGenericRepository<SuppliersFiles> suppliersFilesRepository,IGenericRepository<SuppliersCertificates> suppliersCertificatesRepository,IGenericRepository<SuppliersResume> suppliersResumeRepository,IGenericRepository<SuppliersActivityFields> suppliersActivityFieldsRepository,IGenericRepository<MainSuppliers> mainSuppliersRepository, IGenericRepository<SupplierShareholders> supplierShareholdersRepository)
        {
            _mainSuppliersRepository = mainSuppliersRepository;
            _supplierShareholdersRepository = supplierShareholdersRepository;
            _suppliersActivityFieldsRepository = suppliersActivityFieldsRepository;
            _suppliersResumeRepository = suppliersResumeRepository;
            _suppliersCertificatesRepository = suppliersCertificatesRepository;
            _suppliersFilesRepository = suppliersFilesRepository;
        }
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> ViewAll()
        {
            var suppliers = await _mainSuppliersRepository.GetAllAsync();
            return PartialView(suppliers.OrderByDescending(x=>x.Suppliers_ID));
        }
        public async Task<IActionResult> ViewDetails(int id)
        {
            var thisSupplier = await _mainSuppliersRepository.GetAsync(x => x.Suppliers_ID == id);
            return PartialView(thisSupplier);
        }
        public async Task<IActionResult> ViewShareholderSupplier(int id)
        {
            return PartialView(await _supplierShareholdersRepository.GetAllAsync(x => x.Suppliers_ID == id));
        }
        public async Task<IActionResult> ViewActivitySupplier(int id)
        {
            return PartialView(await _suppliersActivityFieldsRepository.GetWithIncludeAsync(
                selector:x=>x,
                include:x=>x.Include(s=>s.Activity).ThenInclude(s=>s.ActivityField),
                where:x=>x.SupplierId==id
                ));
        }
        public async Task<IActionResult> ViewResumeSupplier(int id)
        {
            return PartialView(await _suppliersResumeRepository.GetAllAsync(x => x.SupplierId == id));
        }
        public async Task<IActionResult> ViewCertificateSupplier(int id)
        {
            return PartialView(await _suppliersCertificatesRepository.GetAllAsync(x => x.SupplierId == id));
        }
        public async Task<IActionResult> ViewDocumentsSupplier(int id)
        {
            return PartialView(await _suppliersFilesRepository.GetWithIncludeAsync(
                selector: x => x,
                //include: x => x.Include(s => s.SuppliersFileType),
                where: x => x.SupplierId == id
                ));
        }
    }
}