﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.CustomerComplaint;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("شکایات مشتریان")]
    public class ComplaintController : Controller
    {
        public IGenericRepository<CustomerComplaintMaster> _customerComplaintMasterRepository;
        public IGenericRepository<CustomerComplaintFiles> _customerComplaintFilesRepository;
        public ComplaintController(IGenericRepository<CustomerComplaintMaster> customerComplaintMasterRepository, IGenericRepository<CustomerComplaintFiles> customerComplaintFilesRepository)
        {
            _customerComplaintMasterRepository = customerComplaintMasterRepository;
            _customerComplaintFilesRepository = customerComplaintFilesRepository;
        }
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> ViewAll()
        {
            var complaints = await _customerComplaintMasterRepository.GetWithIncludeAsync
                (
                selector:x=>x,
                include:x=>x.Include(s=>s.ProductInfo)
                );
            return PartialView(complaints.OrderByDescending(x => x.ComplaintID));
        }
        public async Task<IActionResult> ViewDetails(int id)
        {
            var thisComplaint = await _customerComplaintMasterRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.ProductInfo),
                where:x=>x.ComplaintID==id
                );
            return PartialView(thisComplaint.FirstOrDefault());
        }
        public async Task<IActionResult> ViewDocumentsComplaint(int id)
        {
            return PartialView(await _customerComplaintFilesRepository.GetAllAsync(x=>x.ComplaintId==id));
        }
    }
}