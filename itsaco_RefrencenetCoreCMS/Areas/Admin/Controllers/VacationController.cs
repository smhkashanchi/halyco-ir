﻿using DomainClasses.Role;
using DomainClasses.User;
using DomainClasses.Employees;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services;
using Services.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Utilities;
using ViewModels;
using static Utilities.FunctionResult;
using System.Linq.Dynamic.Core;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("مرخصی")]
    public class VacationController : Controller
    {
        public UserManager<ApplicationUser> _userManager;
        public RoleManager<ApplicationRole> _roleManager;
        public IGenericRepository<ApplicationRole> _ApplicationRoleRepository;
        public IGenericRepository<ApplicationUser> _userRepository;
        public IGenericRepository<Vacation> _VacationRepository;
        public IGenericRepository<VacationSignature> _VacationSignatureRepository;
        public IGenericRepository<Employee> _empgenericRepository;
        public ISMSSender _smsSender;
        public string SendFinalSmsToUser_UserId = ""; //شناسه کاربری که تاییدیه نهایی مرخصی را می دهد

        public VacationController(UserManager<ApplicationUser> userManager, IGenericRepository<ApplicationRole> ApplicationRoleRepository, RoleManager<ApplicationRole> roleManager, IGenericRepository<ApplicationUser> userRepository, IGenericRepository<Vacation> VacationRepository, IGenericRepository<VacationSignature> VacationSignatureRepository, ISMSSender smsSender, IGenericRepository<Employee> empgenericRepository)
        {
            _userManager = userManager;
            _ApplicationRoleRepository = ApplicationRoleRepository;
            _roleManager = roleManager;
            _userRepository = userRepository;
            _VacationRepository = VacationRepository;
            _VacationSignatureRepository = VacationSignatureRepository;
            _smsSender = smsSender;
            _empgenericRepository = empgenericRepository;
        }

        [DisplayName("صفحه نخست")]
        public async Task<IActionResult> Index()
        {
            var user = await _userRepository.GetAsync(x => x.UserName == User.Identity.Name);
            ViewBag.UserIdentity_Id = user.Id;
            ViewBag.Fullname = user.FullName;
            return View();
        }

        [DisplayName("افزودن مرخصی")]
        public async Task<IActionResult> Create()
        {
            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.OfficeUnit),
                where: x => x.UserIdentity_Id == user.Id
                );
            ViewBag.FullName = user.FullName;
            ViewBag.personalCode = employee.FirstOrDefault().PersonalCode;
            ViewBag.HasAlternate = employee.FirstOrDefault().HasAlternateEmployee.Value;

            var coworkerList = await _empgenericRepository.GetWithIncludeAsync(
              selector: x => x,
              where: x => x.Job == employee.FirstOrDefault().Job && x.UserIdentity_Id != employee.FirstOrDefault().UserIdentity_Id && x.IsActive == true,
              include: x => x.Include(s => s.ApplicationUser)
              );
            SelectList coworkerEmployeelist = new SelectList(coworkerList, "PersonalCode", "ApplicationUser.FullName");
            ViewBag.coworkerEmployeelist = coworkerEmployeelist;

            return PartialView();
        }

        [HttpPost]
        public async Task<simpleFunctionResult> Create(Vacation vacation, string sdate, string edate, string vtdate)
        {
            try
            {

                //بدست آورن رکورد یوزر من
                var user = await _userManager.GetUserAsync(User);
                //بدست آوردن لیست  نام نقش های من
                var myRole = await _userManager.GetRolesAsync(user);
                //حذف نقش کارمند از لیست نقش های من
                myRole.Remove("Employee");
                //بدست آوردن نقش من
                var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());
                var FinalUserRole = await _ApplicationRoleRepository.GetAsync(x => x.Id == mr.SendFinalSmsToUser);
                var FinalUsers = await _userManager.GetUsersInRoleAsync(FinalUserRole.Name);
                var FinalUser = FinalUsers.FirstOrDefault();
                SendFinalSmsToUser_UserId = FinalUser.Id;
                //بدست آوردن نقش  پدر من
                var parentRole = await _ApplicationRoleRepository.GetAsync(x => x.Id == mr.ParentRoleId);
                //بدست آوردن لیست یوزر پدرهای من
                var parentUsers = await _userManager.GetUsersInRoleAsync(parentRole.Name);
                //بدست آوردن شماره موبایل اولین(تنهاترین) پدر من
                string userMobile = parentUsers.FirstOrDefault().Mobile_1;
                //بدست آوردن نام من
                string userName = user.FullName;
                DateTime dtStart=DateTime.MinValue, dtEnd=DateTime.MinValue;
                System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                if (sdate != null)
                {
                    dtStart = dtDateTime.AddSeconds(double.Parse(sdate)).ToLocalTime();
                    vacation.DayVacationStartDate = dtStart.ToString("MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                    if(dtStart.Date<DateTime.Now.Date)
                    {
                        return new simpleFunctionResult { Status = false, message = "تاریخ شروع مرخصی روزانه نمی تواند امروز یا قبل از آن باشد" };
                    }
                }
                else
                {
                    vacation.DayVacationStartDate = null;
                }

                if (edate != null)
                {
                    dtEnd = dtDateTime.AddSeconds(double.Parse(edate)).ToLocalTime();
                    vacation.DayVacationEndDate = dtEnd.ToString("MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                    if (dtStart.Date > dtEnd.Date)
                    {
                        return new simpleFunctionResult { Status = false, message = "تاریخ پایان مرخصی روزانه نمی تواند قبل از تاریخ شروع آن باشد" };
                    }
                }
                else
                {
                    vacation.DayVacationEndDate = null;
                }

                if (vtdate != null)
                {
                    DateTime dtVacationDateTime = dtDateTime.AddSeconds(double.Parse(vtdate)).ToLocalTime();
                    vacation.DateVacationTime = dtVacationDateTime.ToString("MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                }
                else
                {
                    vacation.DateVacationTime = null;
                }

                vacation.CreateDate = DateTime.Now;

                if (vacation.PersonalCodeSecond == null)
                {
                    vacation.StatusAlternateSignature = true;
                }

                await _VacationRepository.AddAsync(vacation);
                try
                {
                    await _VacationRepository.SaveChangesAsync();

                    simpleFunctionResult result = await createVacationSigniture(vacation.Vacation_ID, user.Id);
                    if (!result.Status)
                    {
                        return new simpleFunctionResult { Status = result.Status, message = result.message };
                    }

                    string vacationType;
                    string Duration;
                    if (vacation.VacationType)
                    {
                        vacationType = "روزانه";
                        Duration =  vacation.DayVacationStartDate.ToShortPersian() + " تا " + vacation.DayVacationEndDate.ToShortPersian();
                    }
                    else
                    {
                        vacationType = "ساعتی";
                        Duration = "از ساعت " + vacation.StartTime + " تا " + vacation.EndTime + " " + vacation.DateVacationTime.ToShortPersian();
                    }
                    if(vacation.PersonalCodeSecond!=null)
                    {
                        var Employee = await _empgenericRepository.GetByIdAsync(vacation.PersonalCodeSecond);
                        var User = await _userRepository.GetByIdAsync(Employee.UserIdentity_Id);
                        var alternateEmployee =await _empgenericRepository.GetByIdAsync(vacation.PersonalCodeSecond);
                        var alternateUser = await _userRepository.GetByIdAsync(alternateEmployee.UserIdentity_Id);
                        await _smsSender.SendSubmitvacationMessageForCoworkerSMSAsync(alternateUser.Mobile_1, User.FullName, alternateUser.FullName, Duration);
                    }
                    else
                    {
                        await _smsSender.SendSubmitvacationMessageSMSAsync(userMobile, userName, Duration, vacationType);
                    }
                    
                    return new simpleFunctionResult { Status = true, message = "ok" };
                }
                catch (Exception err)
                {
                    return new simpleFunctionResult { Status = false, message = err.Message + " *** " + err.InnerException };
                }

            }
            catch (Exception err)
            {
                return new simpleFunctionResult { Status = false, message = err.Message + " *** " + err.InnerException };
            }
        }

        /// <summary>
        /// متد ایجاد رکوردهای امضا مرخصی
        /// </summary>
        /// <param name="Vacation_ID">شناسه مرخصی</param>
        /// <param name="PersonalCodeSignaturer">شناسه درخواست کننده مرخصی</param>
        /// <returns></returns>
        [NonAction]
        public async Task<simpleFunctionResult> createVacationSigniture(int Vacation_ID, string PersonalCodeSignaturer)
        {
            try
            {
                //بدست آورن رکورد یوزر من
                var user = await _userRepository.GetByIdAsync(PersonalCodeSignaturer);
                //بدست آوردن لیست  نام نقش های من
                var myRole = await _userManager.GetRolesAsync(user);
                //حذف نقش کارمند از لیست نقش های من
                myRole.Remove("Employee");
                //بدست آوردن نقش من
                var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());
                if (mr.ParentRoleId != null)
                {
                    //بدست آوردن نقش  پدر من
                    var parentRole = await _ApplicationRoleRepository.GetAsync(x => x.Id == mr.ParentRoleId);
                    //بدست آوردن لیست یوزر پدرهای من
                    var parentUsers = await _userManager.GetUsersInRoleAsync(parentRole.Name);
               
                    //ساخت رکورد امضا مرخصی برای نقش پدر یوزر جاری و ذخیره آن
                    VacationSignature vs = new VacationSignature
                    {
                        Vacation_ID = Vacation_ID,
                        PersonalCodeSignaturer = parentUsers.FirstOrDefault().Id
                    };
                    await _VacationSignatureRepository.AddAsync(vs);
                    await _VacationSignatureRepository.SaveChangesAsync();

                    //اگر شناسه پدر یوزر جاری متفاوت از شناسه تایید کننده نهایی بود این تابع به صورت بازگشتی اجرا شود
                    if (parentUsers.FirstOrDefault().Id != SendFinalSmsToUser_UserId)
                    {
                        simpleFunctionResult result = await createVacationSigniture(Vacation_ID, parentUsers.FirstOrDefault().Id);
                        if (!result.Status)
                        {
                            return new simpleFunctionResult { Status = result.Status, message = result.message };
                        }
                    }
                }
                return new simpleFunctionResult { Status = true, message = "ok" };
            }
            catch (Exception err)
            {
                return new simpleFunctionResult { Status = false, message = err.Message + " *** " + err.InnerException };
            }
        }

        [DisplayName("مشاهده لیست درخواست ها")]
        public async Task<IActionResult> viewAll()
        {
            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                where: x => x.UserIdentity_Id == user.Id
                );

            IEnumerable<VacationSuperiorViewAllViewModel> vacation = new List<VacationSuperiorViewAllViewModel>();
            //vacation = await _VacationRepository.GetWithIncludeAsync(
            //    selector: x => x,
            //    //include: x => x.Include(s => s.Employee_V_1).Include(s => s.Employee_V_2).Include(s => s.Employee_V_3).Include(s => s.Employee_V_4).Include(s => s.Employee_V_5),
            //    where: x => x.PersonalCodeFirst == user.Id,
            //    orderBy: x => x.OrderByDescending(s => s.Vacation_ID)
            //    );

            vacation = await _VacationRepository.GetWithIncludeAsync(
                        selector: x => new VacationSuperiorViewAllViewModel
                        {
                            CreateDate = (x.CreateDate != null ? x.CreateDate.ToShamsiWithTime() : ""),
                            DateVacationTime = (x.DateVacationTime != null ? x.DateVacationTime.ToShortPersian() : ""),
                            DayVacationEndDate = (x.DayVacationEndDate != null ? x.DayVacationEndDate.ToShortPersian() : ""),
                            DayVacationStartDate = (x.DayVacationStartDate != null ? x.DayVacationStartDate.ToShortPersian() : ""),
                            EndTime = x.EndTime,
                            FinalStatus = x.FinalStatus,
                            FullName = x.Employee_V_1.ApplicationUser.FullName,
                            StartTime = x.StartTime,
                            VacationType = x.VacationType,
                            Vacation_ID = x.Vacation_ID
                        },
                        include: x => x.Include(s => s.Employee_V_1),
                        where: x => x.PersonalCodeFirst == employee.FirstOrDefault().PersonalCode,
                        orderBy: x => x.OrderByDescending(s => s.Vacation_ID)
                        );


            #region jQuery_DataTable_Ajax_Code
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;
            if (!(string.IsNullOrEmpty(sortColumn)))
            {
                vacation = vacation.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection).ToList();
            }
            if (!string.IsNullOrEmpty(searchValue))
            {
                vacation = vacation.Where(m => m.FullName.Contains(searchValue) || m.CreateDate.Contains(searchValue)
                || (!string.IsNullOrEmpty(m.StartTime) ? m.StartTime.Contains(searchValue) : false)
                || (!string.IsNullOrEmpty(m.EndTime) ? m.EndTime.Contains(searchValue) : false)
                || m.DateVacationTime.Contains(searchValue)
                || m.DayVacationStartDate.Contains(searchValue)
                || m.DayVacationEndDate.Contains(searchValue)
                ).ToList();

            }
            recordsTotal = vacation.Count();
            var data = vacation.Skip(skip).Take(pageSize).ToList();
            var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
            #endregion
            return new JsonResult(jsonData);


            //return PartialView(vacation.OrderByDescending(x => x.CreateDate).Take(50));
        }

        [DisplayName("ویرایش مرخصی")]
        public async Task<IActionResult> Edit(int? id)
        {
            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                  (
                  selector: x => x,
                  include: x => x.Include(s => s.OfficeUnit),
                  where: x => x.UserIdentity_Id == user.Id
                  );
            if (user != null && employee.FirstOrDefault() != null)
            {
                ViewBag.FullName = user.FullName;
                ViewBag.PersonalCode = employee.FirstOrDefault().PersonalCode;
            }
            ViewBag.HasAlternate = employee.FirstOrDefault().HasAlternateEmployee.Value;

            var vacation = await _VacationRepository.GetWithIncludeAsync(
                selector: x => x,
                where: x => x.Vacation_ID == id,
                include: x => x.Include(s => s.Employee_V_1)
                );

            VacationSuperiorViewModel vsvm = new VacationSuperiorViewModel();
            vsvm.vacation = vacation.FirstOrDefault();
            var vacationSignatures = await _VacationSignatureRepository.GetWithIncludeAsync(
                selector: x => x,
                where: x => x.Vacation_ID == id,
                include: x => x.Include(s => s.applicationUser),
                orderBy: x => x.OrderBy(s => s.VacationSignature_ID)
                );
            vsvm.vacationSignatures = vacationSignatures.ToList();

            var coworkerList = await _empgenericRepository.GetWithIncludeAsync(
            selector: x => x,
            where: x => x.Job == employee.FirstOrDefault().Job && x.UserIdentity_Id != employee.FirstOrDefault().UserIdentity_Id && x.IsActive == true,
            include: x => x.Include(s => s.ApplicationUser)
            );
            SelectList coworkerEmployeelist = new SelectList(coworkerList, "PersonalCode", "ApplicationUser.FullName");
            ViewBag.coworkerEmployeelist = coworkerEmployeelist;


            ViewBag.firstVSStatus = vacationSignatures.FirstOrDefault().SignatureStatus;
            return PartialView(vsvm);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Vacation vacation, string sdate, string edate, string vtdate)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            var user = await _userManager.GetUserAsync(User);
            var myRole = await _userManager.GetRolesAsync(user);
            myRole.Remove("Employee");
            var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());
            var parentRole = await _ApplicationRoleRepository.GetAsync(x => x.Id == mr.ParentRoleId);
            var parentUsers = await _userManager.GetUsersInRoleAsync(parentRole.Name);

            string userMobile = parentUsers.FirstOrDefault().Mobile_1;
            string userName = user.FullName;

            DateTime dtStart = DateTime.MinValue, dtEnd = DateTime.MinValue;
            if (sdate != null)
            {
                dtStart = dtDateTime.AddSeconds(double.Parse(sdate)).ToLocalTime();
                vacation.DayVacationStartDate = dtStart.ToString("MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                if (dtStart.Date < DateTime.Now.Date)
                {
                    return Json(new{ status = false, message = "تاریخ شروع مرخصی روزانه نمی تواند امروز یا قبل از آن باشد" });
                }
            }
            else
            {
                vacation.DayVacationStartDate = null;
            }

            if (edate != null)
            {
                dtEnd = dtDateTime.AddSeconds(double.Parse(edate)).ToLocalTime();
                vacation.DayVacationEndDate = dtEnd.ToString("MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                if (dtStart.Date > dtEnd.Date)
                {
                    return Json(new { status = false, message = "تاریخ پایان مرخصی روزانه نمی تواند قبل از تاریخ شروع آن باشد" });
                }
            }
            else
            {
                vacation.DayVacationEndDate = null;
            }

            if (vtdate != null)
            {
                DateTime dtVacationDateTime = dtDateTime.AddSeconds(double.Parse(vtdate)).ToLocalTime();
                vacation.DateVacationTime = dtVacationDateTime.ToString("MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            else
            {
                vacation.DateVacationTime = null;
            }

            var Vacations = await _VacationRepository.GetWithIncludeAsync
                (
                selector: x => x,
                where: x => x.Vacation_ID == vacation.Vacation_ID);
            var ThisVacation = Vacations.FirstOrDefault();
            ThisVacation.VacationType = vacation.VacationType;
            ThisVacation.DayVacationStartDate = vacation.DayVacationStartDate;
            ThisVacation.DayVacationEndDate = vacation.DayVacationEndDate;
            ThisVacation.Duration = vacation.Duration;
            ThisVacation.DateVacationTime = vacation.DateVacationTime;
            ThisVacation.StartTime = vacation.StartTime;
            ThisVacation.EndTime = vacation.EndTime;
            ThisVacation.VacationReason = vacation.VacationReason;

            _VacationRepository.Update(ThisVacation);
            await _VacationRepository.SaveChangesAsync();

            string vacationType;
            string Duration;
            if (vacation.VacationType)
            {
                vacationType = "روزانه";
                Duration = "از تاریخ " + vacation.DayVacationStartDate.ToShortPersian() + " تا " + vacation.DayVacationEndDate.ToShortPersian();
            }
            else
            {
                vacationType = "ساعتی";
                Duration = "از ساعت " + vacation.StartTime + " تا " + vacation.EndTime + " تاریخ " + vacation.DateVacationTime.ToShortPersian();
            }
            await _smsSender.SendSubmitvacationMessageSMSAsync(userMobile, userName, Duration, vacationType);
            return Json(new { status = true});
        }

        [DisplayName("حذف")]
        public async Task<IActionResult> Delete(int id)
        {
            var vacationSignatureList = await _VacationSignatureRepository.GetAllAsync(x => x.Vacation_ID == id, orderBy: x => x.OrderBy(s => s.VacationSignature_ID));
            if (vacationSignatureList.FirstOrDefault().SignatureStatus != null)
            {
                return Json("notAllowed");
            }
            var vacation = await _VacationRepository.GetByIdAsync(id);
            if (vacation != null)
            {
                _VacationRepository.Delete(vacation);
                await _VacationRepository.SaveChangesAsync();
                return Json("ok");
            }
            return Json("Nok");
        }

    }
}
