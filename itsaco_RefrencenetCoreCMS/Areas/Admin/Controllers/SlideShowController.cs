﻿using DomainClasses.SlideShow;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Services.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using Utilities;

namespace AspCorePanelProject.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]
    [DisplayName("اسلایدشو")]

    public class SlideShowController : Controller
    {
        public List<SelectListItem> ssgList { get; set; }

        public IGenericRepository<SlideShowGroup> _slideShowGroupRepository;
        public IGenericRepository<SlideShow> _slideShowRepository;

        public SlideShowController(IGenericRepository<SlideShowGroup> slideShowGroupRepository, IGenericRepository<SlideShow> slideShowRepository)
        {
            _slideShowGroupRepository = slideShowGroupRepository;
            _slideShowRepository = slideShowRepository;
        }
        [DisplayName("صفحه نخست")]

        public IActionResult Index()
        {


            return View();
        }
        [DisplayName("لیست  گروه اسلایدشوها")]

        public async Task<ActionResult> ViewAll(string id)
        {
            //return PartialView(_slideShowGroupRepository.GetAllSlideShowGroupByLang(id));
            var sg = await _slideShowGroupRepository.GetAllAsync(x => x.Lnag == id);
            return PartialView(sg);
        }
        [DisplayName("افزودن گروه اسلایدشو")]

        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(SlideShowGroup slideShowGroup, string lang)
        {
            if (slideShowGroup != null)
            {
                // slideShowGroup.Lnag = lang;
                await _slideShowGroupRepository.AddAsync(slideShowGroup);
                await _slideShowGroupRepository.SaveChangesAsync();
                return Json(slideShowGroup.Lnag);
            }
            return View(slideShowGroup);
        }
        [DisplayName("ویرایش  گروه اسلایدشو")]


        public async Task<IActionResult> Edit(int id)
        {
            return View(await _slideShowGroupRepository.GetByIdAsync(id));
        }
        [HttpPost]
        public async Task<IActionResult> Edit(SlideShowGroup slideShowGroup)
        {
            if (slideShowGroup != null)
            {
                _slideShowGroupRepository.Update(slideShowGroup);
                await _slideShowGroupRepository.SaveChangesAsync();
                return Json(slideShowGroup.Lnag);
            }
            return Json("NOK");
        }
        [DisplayName("حذف  گروه اسلایدشو")]

        public async Task<JsonResult> Delete(int id)
        {
            var ssg = await _slideShowGroupRepository.GetByIdAsync(id);
            if (ssg != null)
            {
                if (_slideShowRepository.GetAllAsync(x => x.SSGId == id) == null)
                {
                    _slideShowGroupRepository.Delete(ssg);
                    await _slideShowGroupRepository.SaveChangesAsync();
                    return Json(ssg);
                }
                return Json("NOK");

            }
            return Json(Ok());
        }


        ///از اینجا به بعد مربوط به بخش اسلایدشو می باشد
        ///

        public async Task<IActionResult> ViewAllSSG(string id)
        {
            //جهت اضافه کردن یک آیتم به اول لیست داده شده به دراپ داون لیست از تکه کد زیر استفاده میشود
            if (id == null) { id = CultureInfo.CurrentCulture.Name; }
            ssgList = new List<SelectListItem>();
            //ssgList.Add(new SelectListItem
            //{
            //    Text="انتخاب کنید ...",
            //    Value="0"
            //});
            foreach (var item in await _slideShowGroupRepository.GetAllAsync(x => x.Lnag == id))
            {
                ssgList.Add(new SelectListItem
                {
                    Text = item.SSGName,
                    Value = item.SlideShowGroupID.ToString()
                });
            }


            ViewBag.allGroups = ssgList;
            return Json(ssgList);
        }
        [DisplayName("لیست اسلایدشوها")]

        public async Task<IActionResult> SlideShowViewAll(int id)
        {
            return PartialView(await _slideShowRepository.GetAllAsync(x => x.SSGId == id));
        }
        [DisplayName("افزودن اسلایدشو")]


        public async Task<IActionResult> CreateSlideShow()
        {
            ViewBag.allGroups = new SelectList(await _slideShowGroupRepository.GetAllAsync(), "SlideShowGroupID", "SSGName");
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateSlideShow(SlideShow slideShow, IFormFile imgSlideShow)
        {
            if (slideShow != null)
            {
                if (imgSlideShow != null)
                {
                    if (!Directory.Exists("wwwroot/Files/SlideShow/Temp"))
                    {
                        Directory.CreateDirectory("wwwroot/Files/SlideShow/Temp");
                    }
                    slideShow.SSPic = imgSlideShow.FileName.Substring(0, imgSlideShow.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(imgSlideShow.FileName);
                    string Temppath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SlideShow/Temp", slideShow.SSPic);
                    string path = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SlideShow", slideShow.SSPic);
                    string resizedPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SlideShow/Thumb", slideShow.SSPic);

                    using (var stream = new FileStream(Temppath, FileMode.Create))
                    {
                        imgSlideShow.CopyTo(stream);
                        stream.Dispose();
                    }
                    //
                    ImageManipulate imageManipulate = new ImageManipulate();
                    imageManipulate.resizeImage2(Temppath, path, 825, 278);
                    imageManipulate.resizeImage2(Temppath, resizedPath, 120, 90);
                    System.IO.File.Delete(Temppath);
                }

                await _slideShowRepository.AddAsync(slideShow);
                await _slideShowRepository.SaveChangesAsync();
                return Json(slideShow.SSGId);
                //}
                //else
                //{
                //    return Json("NoImage");
                //}
            }
            return View(slideShow);
        }

        [DisplayName("ویرایش اسلایدشو")]
        public async Task<IActionResult> EditSlideShow(int id)
        {
            var slideShow = await _slideShowRepository.GetByIdAsync(id);
            ViewBag.allGroups = new SelectList(await _slideShowGroupRepository.GetAllAsync(), "SlideShowGroupID", "SSGName", slideShow.SSGId);
            return View(slideShow);
        }
        [HttpPost]
        public async Task<IActionResult> EditSlideShow(SlideShow slideShow, IFormFile imgSliderUpload)
        {
            if (imgSliderUpload != null)
            {
                string oldPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SlideShow", slideShow.SSPic);
                string oldPathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SlideShow/Thumb", slideShow.SSPic);
                DeleteFilesInProj(oldPath, oldPathThumb);

                slideShow.SSPic = imgSliderUpload.FileName.Substring(0, imgSliderUpload.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(imgSliderUpload.FileName);
                string Temppath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SlideShow/Temp", slideShow.SSPic);
                string path = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SlideShow", slideShow.SSPic);
                string resizedPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SlideShow/Thumb", slideShow.SSPic);

                using (var stream = new FileStream(Temppath, FileMode.Create))
                {
                    imgSliderUpload.CopyTo(stream);
                    stream.Dispose();
                }
                ImageManipulate imageManipulate = new ImageManipulate();
                imageManipulate.resizeImage2(Temppath, path, 825, 278);
                imageManipulate.resizeImage2(Temppath, resizedPath, 120, 90);
                System.IO.File.Delete(Temppath);

            }
            _slideShowRepository.Update(slideShow);
            await _slideShowRepository.SaveChangesAsync();
            return Json(slideShow.SSGId);
        }

        [DisplayName("حذف اسلایدشو ")]

        public async Task<JsonResult> checkboxSelected(string values)
        {
            if (values != "")
            {
                string[] splitedValues = values.Split('&');
                for (int i = 0; i < splitedValues.Length - 1; i++)
                {
                    var slider = await _slideShowRepository.GetByIdAsync(Int32.Parse(splitedValues[i]));
                    await DeleteFiles(slider.SSPic);
                    _slideShowRepository.DeleteById(Int32.Parse(splitedValues[i]));
                    await _slideShowRepository.SaveChangesAsync();

                }
                return Json(Ok());
            }
            return Json("NOK");
        }
        ///
        ///
        [NonAction]
        public async Task<IActionResult> DeleteFiles(string fileName)
        {
            string oldPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SlideShow", fileName);
            string oldPathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SlideShow/Thumb", fileName);
            DeleteFilesInProj(oldPath, oldPathThumb);

            return Json(Ok());
        }
       
        [NonAction]
        public void DeleteFilesInProj(string path, string pathThumb)
        {///برای حذف بصورت آسینک
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            if (System.IO.File.Exists(pathThumb))
            {
                System.IO.File.Delete(pathThumb);
            }
            //return Json(Ok());
        }
    }
}