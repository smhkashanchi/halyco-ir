﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Contents;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;

namespace MeshkatEnergy.Areas.Admin.Controllers
{
       [Authorize(Roles="Admin")]
    [Area("admin")]

    public class MaterialController : Controller
    {
        public IGenericRepository<Contents> _contentsRepository;
        public MaterialController(IGenericRepository<Contents> contentsRepository)
        {
            _contentsRepository = contentsRepository;
        }
        public async Task<IActionResult> Index()
        {
            var genralContent =await _contentsRepository.GetByIdAsync(-2);
            ViewBag.genralContent = genralContent;
            return View();
        }
        public async Task<IActionResult> ViewAllAboutUs(int id)
        {
            var aboutUs =await _contentsRepository.GetWithIncludeAsync<Contents>(
                selector : x=>x,
                where : x=>x.ContentGroupId==id && x.IsActive && x.contentsGroup.IsActive,
                include :x=>x.Include(s=>s.contentsGroup)
                );
            return PartialView(aboutUs);
        }
        public async Task<IActionResult> Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Contents contents, IFormFile img)
        {
            if (contents != null)
            {
                if (img != null)
                {
                    contents.ContentImage = img.FileName.Substring(0, img.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(img.FileName);
                    string filePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Material", contents.ContentImage);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await img.CopyToAsync(stream);
                    }
                }

                //contents.ContentGroupId = 3;
                contents.CreateDate = DateTime.Now;
              await  _contentsRepository.AddAsync(contents);
               await _contentsRepository.SaveChangesAsync();
                return Json("ok");
            }
            return Json("nall");
        }
        public async Task<IActionResult> Edit(int id)
        {
            return View(await _contentsRepository.GetByIdAsync(id));
        }
        [HttpPost]
        public async Task<IActionResult> Edit(Contents contents, IFormFile img)
        {
            if (contents != null)
            {
                if (img != null)
                {
                    if (contents.ContentImage != null)
                    {
                        string filePathOld = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Material", contents.ContentImage);
                        if (System.IO.File.Exists(filePathOld))
                        {
                            System.IO.File.Delete(filePathOld);
                        }
                    }
                    contents.ContentImage = img.FileName.Substring(0, img.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(img.FileName);
                    string filePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Material", contents.ContentImage);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await img.CopyToAsync(stream);
                    }
                }

                //contents.ContentGroupId = 3;
                contents.CreateDate = DateTime.Now;
                _contentsRepository.Update(contents);
              await  _contentsRepository.SaveChangesAsync();
                return Json("ok");
            }
            return Json("nall");
        }
    }
}