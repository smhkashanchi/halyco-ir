﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Employees;
using DomainClasses.Role;
using DomainClasses.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

using Services;
using Services.Repositories;

using Utilities;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("مرخصی همکاران")]
    public class VacationCoworkernewController : Controller
    {
        public UserManager<ApplicationUser> _userManager;
        public RoleManager<ApplicationRole> _roleManager;
        public IGenericRepository<Employee> _empgenericRepository;
        public IGenericRepository<ApplicationRole> _ApplicationRoleRepository;
        public IGenericRepository<ApplicationUser> _userRepository;
        public IGenericRepository<Vacation> _VacationRepository;
        public static byte SupervionLevel;
        public ISMSSender _smsSender;

        public VacationCoworkernewController(UserManager<ApplicationUser> userManager, IGenericRepository<Employee> empgenericRepository, IGenericRepository<ApplicationRole> ApplicationRoleRepository, RoleManager<ApplicationRole> roleManager, IGenericRepository<ApplicationUser> userRepository, IGenericRepository<Vacation> VacationRepository, ISMSSender smsSender)
        {
            _userManager = userManager;
            _empgenericRepository = empgenericRepository;
            _ApplicationRoleRepository = ApplicationRoleRepository;
            _roleManager = roleManager;
            _userRepository = userRepository;
            _VacationRepository = VacationRepository;
            _smsSender = smsSender;
        }

        [DisplayName("صفحه نخست")]
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.OfficeUnit),
                where: x => x.UserIdentity_Id == user.Id
                );
            if (user != null && employee.FirstOrDefault() != null)
            {
                ViewBag.FullName = user.FullName;
                ViewBag.PersonalCode = employee.FirstOrDefault().PersonalCode;
            }
            return View();
        }

        [DisplayName("لیست مرخصی ها")]
        public async Task<IActionResult> ViewAll(string id,string filterList)
        {
            var vacations = await _VacationRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.Employee_V_1).ThenInclude(s => s.ApplicationUser).Include(s => s.Employee_V_2).ThenInclude(s => s.ApplicationUser),
                where: x => x.PersonalCodeSecond == id,
                orderBy: x => x.OrderByDescending(s => s.Vacation_ID)
                );
            //filter 
            if (filterList == "-1")
            {
                vacations = vacations.Where(x => x.StatusAlternateSignature == null).ToList();
            }
            else if (filterList == "1")
            {
                vacations = vacations.Where(x => x.StatusAlternateSignature == true).ToList();
            }
            else if (filterList == "0")
            {
                vacations = vacations.Where(x => x.StatusAlternateSignature == false).ToList();
            }
            return PartialView(vacations);
        }

        [DisplayName("ویرایش مرخصی")]
        public async Task<IActionResult> Edit(int id)
        {
            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.OfficeUnit),
                where: x => x.UserIdentity_Id == user.Id
                );
            if (user != null && employee.FirstOrDefault() != null)
            {
                ViewBag.FullName = user.FullName;
                ViewBag.PersonalCode = employee.FirstOrDefault().PersonalCode;
                ViewBag.UnitName = employee.FirstOrDefault().OfficeUnit.OfficeUnitTitle;
                ViewBag.UnitId = employee.FirstOrDefault().OfficeUnit.OfficeUnitID;
                string alternateClass = "";
                if (employee.FirstOrDefault().HasAlternateEmployee == false)
                {
                    alternateClass = "hidden";
                }
                ViewBag.alternateClass = alternateClass;

                var userRolesList = await _userManager.GetRolesAsync(user);
                userRolesList.Remove("Employee");
                var y = await _userManager.GetUsersInRoleAsync(userRolesList[0]);

                //y.Remove(user);
                List<Employee> coworkerList = new List<Employee>();
                foreach (var item in y)
                {
                    var emp = await _userRepository.GetWithIncludeAsync(
                         selector: x => x,
                         include: x => x.Include(s => s.Employee),
                         where: x => x.Id == item.Id                         
                         );
                    coworkerList.Add(emp.FirstOrDefault().Employee);
                }
                SelectList coworkerEmployeelist = new SelectList(coworkerList, "PersonalCode", "ApplicationUser.FullName");
                ViewBag.coworkerEmployeelist = coworkerEmployeelist;
            }

            var ThisVacation = await _VacationRepository.GetWithIncludeAsync(
                selector:x=>x,
                where:x => x.Vacation_ID == id,
                include:x=>x.Include(s=>s.Employee_V_1).ThenInclude(s=>s.ApplicationUser)
                );
            SupervionLevel = ThisVacation.FirstOrDefault().Employee_V_1.ApplicationUser.Employee.SupervisionLevel.Value;
            ViewBag.SupervionLevel = SupervionLevel;
            return PartialView(ThisVacation.FirstOrDefault());
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Vacation vacation)
        {
            var ThisVacation = await _VacationRepository.GetByIdAsync(vacation.Vacation_ID);
            ThisVacation.StatusAlternateSignature = vacation.StatusAlternateSignature;
            ThisVacation.AlternateSignatureReason = vacation.AlternateSignatureReason;
            //if (SupervionLevel == 2)
            //{
            //    ThisVacation.StatusSuperiorSignature = true;
            //}
            ThisVacation.AlternateDateTime = DateTime.Now;
            _VacationRepository.Update(ThisVacation);
            await _VacationRepository.SaveChangesAsync();
            if(vacation.StatusAlternateSignature!=null && vacation.StatusAlternateSignature==true)
            {
                //بدست آورن رکورد یوزر کارمند درخواست دهنده
                var RequesterEmployee = await _empgenericRepository.GetByIdAsync(ThisVacation.PersonalCodeFirst);
                var RequesterUser = await _userManager.FindByIdAsync(RequesterEmployee.UserIdentity_Id);
                //var user = await _userManager.GetUserAsync(User);
                //بدست آوردن لیست  نام نقش های کارمند درخواست دهنده
                var myRole = await _userManager.GetRolesAsync(RequesterUser);
                //حذف نقش کارمند از لیست نقش های من
                myRole.Remove("Employee");
                //بدست آوردن نقش من
                var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());
                //بدست آوردن نقش  پدر من
                var parentRole = await _ApplicationRoleRepository.GetAsync(x => x.Id == mr.ParentRoleId);
                //بدست آوردن لیست یوزر پدرهای من
                var parentUsers = await _userManager.GetUsersInRoleAsync(parentRole.Name);
                //بدست آوردن شماره موبایل اولین(تنهاترین) پدر من
                string userMobile = parentUsers.FirstOrDefault().Mobile_1;
                //بدست آوردن نام من
                string userName = RequesterUser.FullName;

                string vacationType;
                string Duration;
                if (ThisVacation.VacationType)
                {
                    vacationType = "روزانه";
                    Duration = "از تاریخ " + ThisVacation.DayVacationStartDate.ToShortPersian() + " تا " + ThisVacation.DayVacationEndDate.ToShortPersian();
                }
                else
                {
                    vacationType = "ساعتی";
                    Duration = "از ساعت " + ThisVacation.StartTime + " تا " + ThisVacation.EndTime + " تاریخ " + ThisVacation.DateVacationTime.ToShortPersian();
                }

                await _smsSender.SendSubmitvacationMessageSMSAsync(userMobile, userName, Duration, vacationType);
            }
            else
            {
                string vacationType;
                if (ThisVacation.VacationType)
                {
                    vacationType = "روزانه";
                }
                else
                {
                    vacationType = "ساعتی";
                }

                var alternateEmployee = await _empgenericRepository.GetByIdAsync(ThisVacation.PersonalCodeSecond);
                var alternateUser = await _userRepository.GetByIdAsync(alternateEmployee.UserIdentity_Id);

                var Employee = await _empgenericRepository.GetByIdAsync(ThisVacation.PersonalCodeFirst);
                var User = await _userRepository.GetByIdAsync(Employee.UserIdentity_Id);
               
                await _smsSender.SendRefuseVacationByCoworkerMessageSMSAsync(User.Mobile_1, alternateUser.FullName, vacationType);

            }
            return Json(new { status = "ok" });
        }
    }
}