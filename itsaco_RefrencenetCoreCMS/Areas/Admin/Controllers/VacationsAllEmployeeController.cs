﻿using DomainClasses.Role;
using DomainClasses.User;
using DomainClasses.Employees;
using System.Linq.Dynamic.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services;
using Services.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Utilities;
using ViewModels;
using static Utilities.FunctionResult;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("لیست مرخصی های همه کارمندان")]
    public class VacationsAllEmployeeController : Controller
    {
        public UserManager<ApplicationUser> _userManager;
        public RoleManager<ApplicationRole> _roleManager;
        public IGenericRepository<ApplicationRole> _ApplicationRoleRepository;
        public IGenericRepository<ApplicationUser> _userRepository;
        public IGenericRepository<Vacation> _VacationRepository;
        public IGenericRepository<VacationSignature> _VacationSignatureRepository;
        public IGenericRepository<Employee> _empgenericRepository;
        public ISMSSender _smsSender;

        public VacationsAllEmployeeController(UserManager<ApplicationUser> userManager, IGenericRepository<ApplicationRole> ApplicationRoleRepository, RoleManager<ApplicationRole> roleManager, IGenericRepository<ApplicationUser> userRepository, IGenericRepository<Vacation> VacationRepository, IGenericRepository<VacationSignature> VacationSignatureRepository, ISMSSender smsSender, IGenericRepository<Employee> empgenericRepository)
        {
            _userManager = userManager;
            _ApplicationRoleRepository = ApplicationRoleRepository;
            _roleManager = roleManager;
            _userRepository = userRepository;
            _VacationRepository = VacationRepository;
            _VacationSignatureRepository = VacationSignatureRepository;
            _smsSender = smsSender;
            _empgenericRepository = empgenericRepository;
        }

        [DisplayName("صفحه نخست")]
        public async Task<IActionResult> Index()
        {
            var user = await _userRepository.GetAsync(x => x.UserName == User.Identity.Name);
            ViewBag.UserIdentity_Id = user.Id;
            ViewBag.Fullname = user.FullName;
            return View();
        }


        [DisplayName("مشاهده لیست درخواست ها")]
        [HttpPost]
        public async Task<IActionResult> viewAll()
        {
            try
            {
                ////بدست آورن رکورد یوزر من
                //var user = await _userManager.GetUserAsync(User);
                ////بدست آوردن لیست  نام نقش های من
                //var myRole = await _userManager.GetRolesAsync(user);
                ////حذف نقش کارمند از لیست نقش های من
                //myRole.Remove("کارمند");
                ////بدست آوردن نقش من
                //var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());
                ////بدست آوردن لیست تمام نقش های زیرمجموعه نقش من
                //List<ApplicationRole> allMyChildrenRoles = new List<ApplicationRole>();
                //allMyChildrenRoles = await getChildrenRoles(mr, allMyChildrenRoles);
                ////بدست آوردن لیست تمام کارمندان نقش های زیرمجموعه نقش من
                //List<ApplicationUser> AllMyEmployees = new List<ApplicationUser>();
                //foreach (var item in allMyChildrenRoles)
                //{
                //    var TempEmployeeList = await _userManager.GetUsersInRoleAsync(item.Name);
                //    AllMyEmployees.AddRange(TempEmployeeList);
                //}
                ////بدست آوردن لیست نقش هایی که مدیر پشتیبانی می تواند درخواست های آنها را ببیند
                //var TempRoles = await _ApplicationRoleRepository.GetAllAsync(x => x.IsAgentOfManager);
                //foreach (var item in TempRoles)
                //{
                //    var TempEmployeeList = await _userManager.GetUsersInRoleAsync(item.Name);
                //    AllMyEmployees.AddRange(TempEmployeeList);
                //}
                ////بدست آوردن تمام درخواست های مرخصی کارمندان زیرمجموعه نقش من
                //IEnumerable<VacationSuperiorViewAllViewModel> AllMyEmployeeVacations = new List<VacationSuperiorViewAllViewModel>();
                //foreach (var item in AllMyEmployees)
                //{
                    var AllMyEmployeeVacations = await _VacationRepository.GetWithIncludeAsync(
                        selector: x => new VacationSuperiorViewAllViewModel
                        {
                            CreateDate = (x.CreateDate != null ? x.CreateDate.ToShamsiWithTime() : ""),
                            DateVacationTime = (x.DateVacationTime != null ? x.DateVacationTime.ToShortPersian() : ""),
                            DayVacationEndDate = (x.DayVacationEndDate != null ? x.DayVacationEndDate.ToShortPersian() : ""),
                            DayVacationStartDate = (x.DayVacationStartDate != null ? x.DayVacationStartDate.ToShortPersian() : ""),
                            EndTime = x.EndTime,
                            FinalStatus = x.FinalStatus,
                            FullName = x.Employee_V_1.ApplicationUser.FullName,
                            StartTime = x.StartTime,
                            VacationType = x.VacationType,
                            Vacation_ID = x.Vacation_ID
                        },
                        include: x => x.Include(s => s.Employee_V_1)
                        //,where: x => x.Employee_V_1.ApplicationUser.Id == item.Id && (x.StatusAlternateSignature.HasValue && x.StatusAlternateSignature.Value)
                        ,orderBy:x=>x.OrderByDescending(s=>s.Vacation_ID)
                        );
                //    AllMyEmployeeVacations = (AllMyEmployeeVacations ?? Enumerable.Empty<VacationSuperiorViewAllViewModel>()).Concat(tempVacationList ?? Enumerable.Empty<VacationSuperiorViewAllViewModel>()); //amending `<string>` to the appropriate type

                //}
                //AllMyEmployeeVacations = AllMyEmployeeVacations.OrderByDescending(x => x.Vacation_ID).ToList();

                #region jQuery_DataTable_Ajax_Code
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    AllMyEmployeeVacations = AllMyEmployeeVacations.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection).ToList();
                }
                //if (searchValue == "ساعتی") { searchValue = "0"; } else if (searchValue == "1") { searchValue = "true"; }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    AllMyEmployeeVacations = AllMyEmployeeVacations.Where(m => m.FullName.Contains(searchValue) || m.CreateDate.Contains(searchValue)
                    || (!string.IsNullOrEmpty(m.StartTime) ? m.StartTime.Contains(searchValue) : false)
                    || (!string.IsNullOrEmpty(m.EndTime) ? m.EndTime.Contains(searchValue) : false)
                    || m.DateVacationTime.Contains(searchValue)
                    || m.DayVacationStartDate.Contains(searchValue)
                    || m.DayVacationEndDate.Contains(searchValue)
                    //|| m.VacationType.ToString().Contains(searchValue)
                    ).ToList();

                }
                recordsTotal = AllMyEmployeeVacations.Count();
                var data = AllMyEmployeeVacations.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                #endregion
                return new JsonResult(jsonData);


            }
            catch (Exception err)
            {

                throw;
            }
            //return PartialView(AllMyEmployeeVacations.Take(100));
        }

        /// <summary>
        /// متد بازگشتی جهت بدست آوردن لیست تمامی نقش های اولاد یک نقش
        /// </summary>
        /// <param name="role">نقش جاری</param>
        /// <param name="childrenRoleList">لیست نقش های اولاد</param>
        /// <returns>لیست نقش های اولاد آپدیت شده</returns>
        [NonAction]
        public async Task<List<ApplicationRole>> getChildrenRoles(ApplicationRole role, List<ApplicationRole> childrenRoleList)
        {
            var tempList = await _ApplicationRoleRepository.GetAllAsync(x => x.ParentRoleId == role.Id || x.SendFinalSmsToUser == role.Id);

            //foreach (var item in tempList)
            //{
            //    var result = await getChildrenRoles(item, childrenRoleList);

            //}
            childrenRoleList.AddRange(tempList);
            return childrenRoleList;
        }

        [DisplayName("ویرایش مرخصی")]
        public async Task<IActionResult> Edit(int? id)
        {
            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                where: x => x.UserIdentity_Id == user.Id
                );
            if (user != null && employee.FirstOrDefault() != null)
            {
                ViewBag.FullName = user.FullName;
                ViewBag.PersonalCode = user.Id;
            }

            var vacation = await _VacationRepository.GetWithIncludeAsync(
                selector: x => x,
                where: x => x.Vacation_ID == id,
                include: x => x.Include(s => s.Employee_V_1).ThenInclude(s => s.ApplicationUser).Include(s => s.Employee_V_2).ThenInclude(s => s.ApplicationUser));

            ViewBag.HasAlternate = employee.FirstOrDefault().HasAlternateEmployee.Value;

            var vacationSignatureList = await _VacationSignatureRepository.GetAllAsync(x => x.Vacation_ID == id, orderBy: x => x.OrderBy(s => s.VacationSignature_ID));
            //ViewBag.firstVSStatus = vacationSignatureList.FirstOrDefault().SignatureStatus;
            VacationSuperiorViewModel vsvm = new VacationSuperiorViewModel();
            vsvm.vacation = vacation.FirstOrDefault();
            var vacationSignatures = await _VacationSignatureRepository.GetWithIncludeAsync(
                selector: x => x,
                where: x => x.Vacation_ID == id,
                include: x => x.Include(s => s.applicationUser),
                orderBy: x => x.OrderBy(s => s.VacationSignature_ID)
                );
            List<VacationSignature> VSList = vacationSignatures.OrderByDescending(x => x.VacationSignature_ID).ToList();

            bool flag = false;
            vsvm.ThisUserCanSignature = true;
            foreach (var item in VSList)
            {
                if (flag)
                {
                    vsvm.ThisUserCanSignature = item.SignatureStatus;
                    break;
                }
                if (item.applicationUser.Id == user.Id)
                {
                    flag = true;
                }

            }

            vsvm.vacationSignatures = vacationSignatures.ToList();
            vsvm.CurrentSuperiorVacationSignatures = vacationSignatures.FirstOrDefault(x => x.PersonalCodeSignaturer == user.Id);

            #region برای مدیر پشتیبانی
            //بدست آوردن لیست  نام نقش های من
            var myRole = await _userManager.GetRolesAsync(user);
            //حذف نقش کارمند از لیست نقش های من
            myRole.Remove("Employee");
            //بدست آوردن نقش من
            var ThisUserRole = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());


            var RequesterUser = await _userRepository.GetAsync(x => x.Id == vacation.FirstOrDefault().Employee_V_1.ApplicationUser.Id);
            //بدست آوردن لیست  نام نقش های من
            var RequesterRole = await _userManager.GetRolesAsync(RequesterUser);
            //حذف نقش کارمند از لیست نقش های من
            RequesterRole.Remove("Employee");
            //بدست آوردن نقش من
            var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == RequesterRole.FirstOrDefault());
            if (mr.IsAgentOfManager && mr.SendFinalSmsToUser == "116ce37b-efd3-4bb8-a42b-9027e85c7dd6" && vacation.FirstOrDefault().FinalStatus != true && ThisUserRole.Id == "a2c5ff4a-64b0-4381-9cc2-e65596ebb271")//اگر نقش کاربر درخواست دهنده امکان مدیر پشتیبانی را فعال کرده باشد
            {
                ViewBag.IsAgentOfManager = true;
            }
            #endregion

            return PartialView(vsvm);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(VacationSignature vacationSignature)
        {
            //ذخیره سازی امضای مافوق کنونی
            var user = await _userManager.GetUserAsync(User);
            vacationSignature.SignatureDateTime = DateTime.Now;
            vacationSignature.PersonalCodeSignaturer = user.Id;
            _VacationSignatureRepository.Update(vacationSignature);
            await _VacationSignatureRepository.SaveChangesAsync();

            var vacation = await _VacationRepository.GetWithIncludeAsync(
                    selector: x => x,
                    where: x => x.Vacation_ID == vacationSignature.Vacation_ID,
                 include: x => x.Include(s => s.Employee_V_1).ThenInclude(s => s.ApplicationUser));

            if (vacationSignature.SignatureStatus == false)
            {

                vacation.FirstOrDefault().FinalStatus = vacationSignature.SignatureStatus.Value;
                _VacationRepository.Update(vacation.FirstOrDefault());
                await _VacationRepository.SaveChangesAsync();

                string vacationStatusString = "";
                if (vacationSignature.SignatureStatus.Value)
                {
                    vacationStatusString = "تایید";
                }
                else
                {
                    vacationStatusString = "رد";
                }
                //ارسال پیامک نتیجه درخواست مرخصی به کارمند درخواست کننده
                await _smsSender.SendfinalVacationAcceptSMSAsync(vacation.FirstOrDefault().Employee_V_1.ApplicationUser.Mobile_1, vacation.FirstOrDefault().Employee_V_1.ApplicationUser.FullName, vacationSignature.SignatureDateTime.ToShamsiWithTime(), vacationStatusString);
                //ارسال پیامک نتیجه درخواست مرخصی به مدیر منابع انسانی
                // await _smsSender.finalVacationAcceptHRManagerSMSAsync(parentUsers.FirstOrDefault().Mobile_1, vacation2.Employee_V_1.FullName, vacationSignature.SignatureDateTime.ToShamsiWithTime(), vacationStatusString);
            }

            var myRole = await _userManager.GetRolesAsync(user);
            myRole.Remove("Employee");
            var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());
            var parentRole = await _ApplicationRoleRepository.GetAsync(x => x.Id == mr.ParentRoleId);
            if (parentRole != null)
            {
                var parentUsers = await _userManager.GetUsersInRoleAsync(parentRole.Name);
                //تا اینجا
                // بررسی شود که آیا باید به مافوق بعدی پیامک بره یا اینکه به سطح مدیر پروژه رسیدیم و باید وضعیت خود مرخصی مشخص بشه و پیامک نتیجه نهایی به کارمند ارسال بشه

                //بررسی شود که آیا باید به مافوق بعدی پیامک برود
                if (mr.SendSmsToParent)
                {
                    string userMobile = parentUsers.FirstOrDefault().Mobile_1;
                    string userName = vacation.FirstOrDefault().Employee_V_1.ApplicationUser.FullName;

                    string vacationType;
                    string Duration;
                    if (vacation.FirstOrDefault().VacationType)
                    {
                        vacationType = "روزانه";
                        Duration = vacation.FirstOrDefault().DayVacationStartDate.ToShortPersian() + " تا " + vacation.FirstOrDefault().DayVacationEndDate.ToShortPersian();
                    }
                    else
                    {
                        vacationType = "ساعتی";
                        Duration = "از ساعت " + vacation.FirstOrDefault().StartTime + " تا " + vacation.FirstOrDefault().EndTime + " تاریخ " + vacation.FirstOrDefault().DateVacationTime.ToShortPersian();
                    }
                    await _smsSender.SendSubmitvacationMessageSMSAsync(userMobile, userName, Duration, vacationType);

                }
            }
            //بدست آوردن نقش کارمند درخواست کننده مرخصی
            var vacationRequesterRole = await _userManager.GetRolesAsync(vacation.FirstOrDefault().Employee_V_1.ApplicationUser);
            vacationRequesterRole.Remove("Employee");
            var vrr = await _ApplicationRoleRepository.GetAsync(x => x.Name == vacationRequesterRole.FirstOrDefault());
            //اگر شناسه نقشی که وضعیت نهایی مرخصی را مشخص می کند با شناسه مافوق جاری یکسان باشد
            if (vrr.SendFinalSmsToUser == mr.Id)
            {
                vacation.FirstOrDefault().FinalStatus = vacationSignature.SignatureStatus.Value;
                _VacationRepository.Update(vacation.FirstOrDefault());
                await _VacationRepository.SaveChangesAsync();

                string vacationStatusString = "";
                if (vacationSignature.SignatureStatus.Value)
                {
                    vacationStatusString = "تایید";
                }
                else
                {
                    vacationStatusString = "رد";
                }

                string vacationType;
                if (vacation.FirstOrDefault().VacationType)
                {
                    vacationType = "روزانه";
                }
                else
                {
                    vacationType = "ساعتی";
                }

                //ارسال پیامک نتیجه درخواست مرخصی به کارمند درخواست کننده
                await _smsSender.SendfinalVacationAcceptSMSAsync(vacation.FirstOrDefault().Employee_V_1.ApplicationUser.Mobile_1, vacation.FirstOrDefault().Employee_V_1.ApplicationUser.FullName, vacationSignature.SignatureDateTime.ToShamsiWithTime(), vacationStatusString);
                //ارسال پیامک نتیجه درخواست به مدیر تایید کننده نهایی که فعلا نیازی نیست چون خودش میدونه چه کرده
                //await _smsSender.finalVacationAcceptHRManagerSMSAsync(parentUsers.FirstOrDefault().Mobile_1, vacation.FirstOrDefault().Employee_V_1.ApplicationUser.FullName, vacationSignature.SignatureDateTime.ToShamsiWithTime(), vacationSignature.SignatureStatus.Value,vacationType);

            }
            return Json(new { status = "ok" });
        }



    }
}
