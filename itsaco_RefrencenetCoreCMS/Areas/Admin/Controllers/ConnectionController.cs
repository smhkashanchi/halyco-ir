﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Connection;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;

namespace AspCorePanelProject.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]
    [DisplayName("راه های ارتباطی")]

    public class ConnectionController : Controller
    {
        public List<SelectListItem> CnGroup;
        public List<SelectListItem> typeList;
        public IGenericRepository<ConnectionGroup> _connectionGroupRepository;
        public IGenericRepository<Connection> _connectionRepository;
        public IGenericRepository<Map> _mapRepository;
        public ConnectionController(IGenericRepository<Map> mapRepository, IGenericRepository<ConnectionGroup> connectionGroupRepository, IGenericRepository<Connection> connectionRepository)
        {
            _connectionGroupRepository = connectionGroupRepository;
            _connectionRepository = connectionRepository;
            _mapRepository = mapRepository;
        }
        [DisplayName("صفحه نخست")]
        public IActionResult Index()
        {
            return View();
        }
        [DisplayName("لیست گروه های ارتباطی")]
        public async Task<IActionResult> ViewAll(string id)
        {
            var group = await _connectionGroupRepository.GetAllAsync(x => x.Lang == id, x => x.OrderByDescending(s => s.ConnectionGroup_ID));
            return PartialView(group);
        }
        [DisplayName("افزودن گروه")]
        public IActionResult CreateGroup()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateGroup(ConnectionGroup connectionGroup)
        {
            if (connectionGroup != null)
            {
                await _connectionGroupRepository.AddAsync(connectionGroup);
                await _connectionGroupRepository.SaveChangesAsync();

                return Json(connectionGroup);
            }
            return Json("nall");
        }
        [DisplayName("ویرایش گروه")]

        public async Task<IActionResult> editGroup(int id)
        {
            return View(await _connectionGroupRepository.GetByIdAsync(id));
        }
        [HttpPost]
        public async Task<IActionResult> editGroup(ConnectionGroup connectionGroup)
        {
            if (connectionGroup != null)
            {
                _connectionGroupRepository.Update(connectionGroup);
                await _connectionGroupRepository.SaveChangesAsync();

                return Json(connectionGroup);
            }
            return Json("nall");
        }
        [DisplayName("حذف گروه")]

        public async Task<IActionResult> DeleteGroup(int id)
        {
            var group = await _connectionGroupRepository.GetByIdAsync(id);
            if (group != null)
            {
                if (_connectionRepository.GetAllAsync(x => x.ConnectionGroupId == id) == null)
                {
                    _connectionGroupRepository.Delete(group);
                    await _connectionGroupRepository.SaveChangesAsync();
                    return Json(group);
                }
                return Json("repeate");
            }
            return Json("nall");
        }
        ///////////////////////////// بخش راههای ارتباطی
        ///
        [DisplayName("لیست راه های ارتباطی")]

        public async Task<IActionResult> ViewAllConnection(int id)
        {
            var lis2 = await _connectionRepository.GetAllAsync(x => x.ConnectionGroupId == id,
               s => s.OrderByDescending(x => x.Connection_ID));

            var list = await _connectionRepository.GetWithIncludeAsync<Connection>(selector: x => x,
                where: x => x.ConnectionGroupId == id,
               orderBy: s => s.OrderByDescending(x => x.Connection_ID)
               , include: x => x.Include(s => s.ConnectionGroup));

            return PartialView(list);
        }

        public async Task<IActionResult> ViewAllCnGroupList(string id)
        {
            CnGroup = new List<SelectListItem>();

            foreach (var item in await _connectionGroupRepository.GetAllAsync(x => x.Lang == id, x => x.OrderByDescending(s => s.ConnectionGroup_ID)))
            {
                CnGroup.Add(new SelectListItem
                {
                    Text = item.ConnectionGroupTitle,
                    Value = item.ConnectionGroup_ID.ToString()
                });
            }
            return Json(CnGroup);
        }
        [DisplayName("افزودن")]

        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Connection connection)
        {
            if (connection != null)
            {
                await _connectionRepository.AddAsync(connection);
                await _connectionRepository.SaveChangesAsync();
                return Json(connection);
            }
            return Json(connection);
        }
        [DisplayName("ویرایش")]

        public async Task<IActionResult> Edit(byte id)
        {
            var connection = await _connectionRepository.GetByIdAsync(id);
            typeList = new List<SelectListItem>();
            if (connection.CType == true)
            {
                typeList.Add(new SelectListItem
                {
                    Text = "نقشه",
                    Value = "0",
                });
                typeList.Add(new SelectListItem
                {
                    Text = "متنی",
                    Value = "1",
                    Selected = true
                });
            }
            else
            {
                typeList.Add(new SelectListItem
                {
                    Text = "نقشه",
                    Value = "0",
                    Selected = true
                });
                typeList.Add(new SelectListItem
                {
                    Text = "متنی",
                    Value = "1",

                });
            }
            ViewBag.cnType = typeList;
            return View(connection);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(Connection connection)
        {
            if (connection != null)
            {
                _connectionRepository.Update(connection);
                await _connectionRepository.SaveChangesAsync();
                return Json(connection);
            }
            return Json(connection);
        }
        [DisplayName("حذف")]
        public async Task<IActionResult> DeleteConnection(byte id)
        {
            var connection = await _connectionRepository.GetByIdAsync(id);
            if (connection != null)
            {
                _connectionRepository.Delete(connection);
                await _connectionRepository.SaveChangesAsync();
                return Json(connection);
            }
            return Json("Nok");
        }
        [DisplayName("نقشه")]
        //[Route("/Admin/Map")]
        public async Task<IActionResult> Map(int? id=1)
        {
            var mapInfo = await _mapRepository.GetAsync(x=>x.Type==id);
            return View(mapInfo);
        }
        [HttpPost]
        public async Task<IActionResult> InsertOrUpdateMap(Map map)
        {
            try
            {
                var mapInfo = await _mapRepository.GetAsync(x=>x.Type==map.Type);
                if (mapInfo==null)
                {
                    await _mapRepository.AddAsync(map);
                    await _mapRepository.SaveChangesAsync();
                    return Json("add");
                }
                else
                {
                    mapInfo.Latitude = map.Latitude;
                    mapInfo.longitude = map.longitude;
                    _mapRepository.Update(mapInfo);
                    await _mapRepository.SaveChangesAsync();
                    return Json("update");
                }
            }
            catch (Exception err)
            {

                throw;
            }
        }
        [DisplayName("گرفتن اطلاعات آدرس")]
        public async Task<IActionResult> GetLocationStatus(int id)
        {
            var thisLocation = await _mapRepository.GetAsync(x => x.Type == id);
            if (thisLocation != null)
            {
                return Json(new { lat = thisLocation.Latitude, longi = thisLocation.longitude });
            }
            return Json(new { lat = "33.997243", longi = "51.434958" });
        }
    }
}