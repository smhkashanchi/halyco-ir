﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Product;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Services.Repositories;
using ViewModels;

namespace AspCorePanelProject.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]
    [DisplayName("ویژگی ها")]
    public class FeaturesController : Controller
    {
        public List<SelectListItem> featureList;
        public IGenericRepository<Feature> _featureRepository;
        public IGenericRepository<ProductsGroup> _productsGroupRepository;
        public IGenericRepository<FeatureReply> _featureReplyRepository;
        public FeaturesController(IGenericRepository<Feature> featureRepository
            , IGenericRepository<ProductsGroup> productsGroupRepository,
            IGenericRepository<FeatureReply> featureReplyRepository)
        {
            _featureRepository = featureRepository;
            _productsGroupRepository = productsGroupRepository;
            _featureReplyRepository = featureReplyRepository;

        }
        [DisplayName("صفحه نخست")]
        public IActionResult Index()
        {
            return View();
        }
        public async Task<string> createTreeView(Int16 pgId, string lang)
        {
            return JsonConvert.SerializeObject(await ViewInTreeView(pgId, lang));
        }
        [NonAction]
        public async Task<List<Node>> ViewInTreeView(Int16 PGId, string lang)//گرفتن گروه مطالب بصورت جیسون و فرستادن به تابع ساخت تری ویو
        {
            List<Node> TNodes = new List<Node>();
            List<Node> TNodes2 = new List<Node>();
            // var groups;

            var productGroupData = await _productsGroupRepository.GetByIdAsync(PGId);

            var groups = await _productsGroupRepository.GetAllAsync(x => x.ParentId == PGId);
            foreach (var item in groups)
            {
                var node = await ViewInTreeView(item.ProductGroup_ID, lang);
                TNodes2.Add(node.FirstOrDefault());
            }


            TNodes.Add(new Node()
            {
                text = productGroupData.ProductGroupTitle,
                href = "#" + productGroupData.ProductGroupTitle,
                tags = PGId.ToString(),
                nodes = TNodes2
            });

            return TNodes;
        }
        [DisplayName("لیست ویژگی ها")]
        public async Task<IActionResult> ViewAllFeatures(int id)
        {
            return PartialView(await _featureRepository.GetWithIncludeAsync<Feature>(
                selector: x => x,
                include: x => x.Include(s => s.ProductsGroup),
                where: x => x.ProductGroupId == id));
        }
        [DisplayName("افرودن ویژگی")]
        public IActionResult CreateFeature()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateFeature(Feature feature)
        {
            if (feature != null)
            {
                await _featureRepository.AddAsync(feature);
                await _featureRepository.SaveChangesAsync();
                return Json(feature);
            }
            return View();
        }
        [DisplayName("ویرایش ویژگی ")]
        public async Task<IActionResult> EditFeature(int id)
        {
            return View(await _featureRepository.GetByIdAsync(id));
        }
        [HttpPost]
        public async Task<IActionResult> EditFeature(Feature feature)
        {
            if (feature != null)
            {
                _featureRepository.Update(feature);
                await _featureRepository.SaveChangesAsync();
                return Json(feature);
            }
            return View();
        }
        [DisplayName("حذف ویژگی ")]

        public async Task<IActionResult> DeleteFeature(int id)
        {
            var feature = await _featureRepository.GetByIdAsync(id);
            if (feature != null)
            {
                _featureRepository.Delete(feature);
                await _featureRepository.SaveChangesAsync();
                return Json(feature);
            }
            return Json("no");
        }

        ////////////////////////////////// بخش جواب ویژگی
        ///
        [DisplayName("لیست جواب ویژگی ها ")]
        public async Task<IActionResult> ViewAllFeatureReply(int id)
        {/// زمانی که از داخل دراپ داون یک ویژگی انتخاب شود جواب های ان ویژگی نمایش داده میشود
            return PartialView(await _featureReplyRepository.GetWithIncludeAsync<FeatureReply>
                (selector: x => x,
                where: x => x.FeatureId == id,
                include: x => x.Include(s => s.Feature)));
        }

        public async Task<JsonResult> GetAllFeatureByProductGroupId(int id)
        {/// زمانی که روی گروه محصولات کلیک شود این مقدار جیسون به دراپ داون داده میشود

            featureList = new List<SelectListItem>();
            featureList.Add(new SelectListItem
            {
                Text = "انتخاب کنید ...",
                Value = "0"
            });
            foreach (var item in await _featureRepository.GetWithIncludeAsync<Feature>(
                selector: x => x,
                include: x => x.Include(s => s.ProductsGroup),
                where: x => x.ProductGroupId == id))
            {
                featureList.Add(new SelectListItem
                {
                    Text = item.FeatureTitle,
                    Value = item.Feature_ID.ToString()
                });
            }
            return Json(featureList);
        }
        [DisplayName("افزودن جواب ویژگی ")]
        public async Task<IActionResult> CreateFeatureReply(int id)
        {
            ViewBag.allFeatures = new SelectList(await _featureRepository.GetWithIncludeAsync<Feature>(
                selector: x => x,
                include: x => x.Include(s => s.ProductsGroup),
                where: x => x.ProductGroupId == id), "Feature_ID", "FeatureTitle");
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateFeatureReply(FeatureReply featureReply)
        {
            if (featureReply != null)
            {
                await _featureReplyRepository.AddAsync(featureReply);
                await _featureReplyRepository.SaveChangesAsync();
                return Json(featureReply);
            }
            return Json("no");
        }
        [DisplayName("ویرایش جواب ویژگی ")]
        public async Task<IActionResult> EditFeatureReply(int id)
        {
            //var products = await _productInfoRepository.GetWithIncludeAsync<ProductInfo>
            //   (
            //   selector: x => x,
            //   include: s => s.Include(x => x.MainProduct).Include(x => x.ProductsGroup).Include(x => x.Off).Include(x => x.Unit).Include(x => x.Producer),
            //   where: x => x.ProductGroupId == id
            //   );

            var AllfeatureReply = await _featureReplyRepository.GetWithIncludeAsync<FeatureReply>(
                selector: x => x,
                include: y => y.Include(x => x.Feature),
                where: z => z.FeatureReply_ID == id
                );
            var feature = AllfeatureReply.FirstOrDefault();
            //ViewBag.valueF = featureReply2.  .Feature.Feature_ID;
            ViewBag.textF = feature.Feature.FeatureTitle;
            ViewBag.Feature_Id = feature.FeatureId;
            //ViewBag.allFeatures = new SelectList(featureReply.Feature., "Feature_ID", "FeatureTitle",featureReply.FeatureId);
            return View(feature);
        }
        [HttpPost]
        public async Task<IActionResult> EditFeatureReply(FeatureReply featureReply)
        {
            if (featureReply != null)
            {
                _featureReplyRepository.Update(featureReply);
                await _featureReplyRepository.SaveChangesAsync();
                return Json(featureReply);
            }
            return Json("no");
        }
        [DisplayName(" حذف جواب ویژگی")]

        public async Task checkboxSelected(string values)
        {
            if (values != "")
            {
                string[] splitedValues = values.Split('&');
                for (int i = 0; i < splitedValues.Length - 1; i++)
                {

                    _featureReplyRepository.DeleteById(Int32.Parse(splitedValues[i]));
                    await _featureReplyRepository.SaveChangesAsync();

                }
            }
        }

        public async Task<IActionResult> SetIsDefault(int id, int featureId)
        {
            if (id != 0)
            {
                var frs = await _featureReplyRepository.GetWithIncludeAsync<FeatureReply>
                (selector: x => x,
                where: x => x.FeatureId == id,
                include: x => x.Include(s => s.Feature));
                var fr = await _featureReplyRepository.GetByIdAsync(id);
                foreach (var item in frs)
                {
                    item.IsDefault = false;
                    _featureReplyRepository.Update(item);
                }
                await _featureReplyRepository.SaveChangesAsync();

                fr.IsDefault = true;
                _featureReplyRepository.Update(fr);
                await _featureReplyRepository.SaveChangesAsync();
                return Json("ok");
            }
            return Json("no");
        }

    }
}