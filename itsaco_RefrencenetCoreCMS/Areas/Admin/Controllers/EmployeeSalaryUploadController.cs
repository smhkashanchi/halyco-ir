﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Employees;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("آپلود فیش حقوقی کارمندان")]
    public class EmployeeSalaryUploadController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        public IGenericRepository<EmployeeSalary> _employeeSalary;
        public EmployeeSalaryUploadController(IHostingEnvironment hostingEnvironment, IGenericRepository<EmployeeSalary> employeeSalary)
        {
            _hostingEnvironment = hostingEnvironment;
            _employeeSalary = employeeSalary;
        }
        public IActionResult Index()
        {
            return View();
        }
        //[HttpPost]
        //public async Task<IActionResult> SalaryUpload(IFormFile myFile)
        //{
        //    int rowError=0;
        //    try
        //    {
        //        Int16 zeroInt16 = 0;
        //        string rootFolder = _hostingEnvironment.WebRootPath;
        //        string fileName = myFile.FileName.Substring(0, myFile.FileName.IndexOf(".")) + "_"+DateTime.Now.ToString("yyyyMMddHHmmss") + System.IO.Path.GetExtension(myFile.FileName);
        //        string filePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/EmployeeSalary", fileName);
        //        FileInfo file = new FileInfo(Path.Combine(rootFolder,"Files/EmployeeSalary", fileName));

        //        using (var stream = new FileStream(filePath, FileMode.Create))
        //        {
        //            myFile.CopyTo(stream);

        //        }
        //        using (ExcelPackage package = new ExcelPackage(file))
        //        {
        //            ExcelWorksheet workSheet = package.Workbook.Worksheets.FirstOrDefault();//["Cnn"];
        //            int totalRows = workSheet.Dimension.Rows;

        //            List<EmployeeSalary> employeeSalaryList = new List<EmployeeSalary>();

        //            for (int i = 2; i <= totalRows; i++)
        //            {
        //                rowError = i;
        //                EmployeeSalary es = new EmployeeSalary();
        //                es.Salary_ID = Guid.NewGuid().ToString();
        //                es.PersonalCode = workSheet.Cells[i, 1].Value.ToString();
        //                es.SalMah = workSheet.Cells[i, 3].Value.ToString() != null ? workSheet.Cells[i, 3].Value.ToString() : "";
        //                es.NameMahaleKhedmatHokmi = workSheet.Cells[i, 4].Value.ToString() != null ? workSheet.Cells[i, 4].Value.ToString() : "";
        //                es.SP_Khales_Pardakhti = workSheet.Cells[i, 5].Value != null ? Convert.ToInt32(workSheet.Cells[i, 5].Value) : 0;
        //                es.FR_Ayab_Zahab = workSheet.Cells[i, 6].Value != null ? Convert.ToInt32(workSheet.Cells[i, 6].Value) : 0;
        //                es.FR_Bime_Takmili = workSheet.Cells[i, 7].Value != null ? Convert.ToInt32(workSheet.Cells[i, 7].Value) : 0;
        //                es.FR_DastmozdRoozaneh = workSheet.Cells[i, 8].Value != null ? Convert.ToInt32(workSheet.Cells[i, 8].Value) : 0;
        //                es.FR_FOGHOLADE = workSheet.Cells[i, 9].Value != null ? Convert.ToInt32(workSheet.Cells[i, 9].Value) : 0;
        //                es.FR_Hag_Jazb = workSheet.Cells[i, 10].Value != null ? Convert.ToInt32(workSheet.Cells[i, 10].Value) : 0;
        //                es.FR_HAG_MASKAN = workSheet.Cells[i, 11].Value != null ? Convert.ToInt32(workSheet.Cells[i, 11].Value) : 0;
        //                es.FR_HAG_MASULEYAT = workSheet.Cells[i, 12].Value != null ? Convert.ToInt32(workSheet.Cells[i, 12].Value) : 0;
        //                es.FR_HAG_OLAD = workSheet.Cells[i, 13].Value != null ? Convert.ToInt32(workSheet.Cells[i, 13].Value) : 0;
        //                es.FR_Hogug_Payeh = workSheet.Cells[i, 14].Value != null ? Convert.ToInt32(workSheet.Cells[i, 14].Value) : 0;
        //                es.FR_Sayer_Mazaya = workSheet.Cells[i, 15].Value != null ? Convert.ToInt32(workSheet.Cells[i, 15].Value) : 0;
        //                es.SP_Ezafe_Kar = workSheet.Cells[i, 16].Value != null ? Convert.ToInt32(workSheet.Cells[i, 16].Value) : 0;
        //                es.SP_Hag_Bimeh_Karmand = workSheet.Cells[i, 17].Value != null ? Convert.ToInt32(workSheet.Cells[i, 17].Value) : 0;
        //                es.SP_Jame_Ezafat = workSheet.Cells[i, 18].Value != null ? Convert.ToInt32(workSheet.Cells[i, 18].Value) : 0;
        //                es.SP_Jame_Kosurat = workSheet.Cells[i, 19].Value != null ? Convert.ToInt32(workSheet.Cells[i, 19].Value) : 0;
        //                es.SP_Maleyat = workSheet.Cells[i, 20].Value != null ? Convert.ToInt32(workSheet.Cells[i, 20].Value) : 0;
        //                es.SP_MashMul_Bimeh = workSheet.Cells[i, 21].Value != null ? Convert.ToInt32(workSheet.Cells[i, 21].Value) : 0;
        //                es.SP_Mashmul_Maleyat = workSheet.Cells[i, 22].Value != null ? Convert.ToInt32(workSheet.Cells[i, 22].Value) : 0;
        //                es.SY_KE_VamGhest1 = workSheet.Cells[i, 23].Value != null ? Convert.ToInt32(workSheet.Cells[i, 23].Value) : 0;
        //                es.SY_KE_VamMandeh1 = workSheet.Cells[i, 24].Value != null ? Convert.ToInt32(workSheet.Cells[i, 24].Value) : 0;
        //                es.SY_Saat_AzafeKar = workSheet.Cells[i, 25].Value != null ? Convert.ToInt16(workSheet.Cells[i, 25].Value) : zeroInt16;
        //                es.SY_sayer_Kosurat = workSheet.Cells[i, 26].Value != null ? Convert.ToInt32(workSheet.Cells[i, 26].Value) : zeroInt16;
        //                es.SY_Tadad_Ruz_Kar = workSheet.Cells[i, 27].Value != null ? Convert.ToInt16(workSheet.Cells[i, 27].Value) : zeroInt16;
        //                es.ZR_FOGHOLADE_Special = workSheet.Cells[i, 28].Value != null ? Convert.ToInt32(workSheet.Cells[i, 28].Value) : 0;
        //                es.SY_saat_nobat_kari = workSheet.Cells[i, 29].Value != null ? Convert.ToInt32(workSheet.Cells[i, 29].Value) : 0;
        //                es.FR_moavaghe = workSheet.Cells[i, 30].Value != null ? Convert.ToInt32(workSheet.Cells[i, 30].Value) : 0;
        //                es.FR_mosaede = workSheet.Cells[i, 31].Value != null ? Convert.ToInt32(workSheet.Cells[i, 31].Value) : 0;
        //                es.FR_padash__ = workSheet.Cells[i, 32].Value != null ? Convert.ToInt32(workSheet.Cells[i, 32].Value) : 0;
        //                es.FR_sayer_ezafat = workSheet.Cells[i, 33].Value != null ? Convert.ToInt32(workSheet.Cells[i, 33].Value) : 0;
        //                es.SP_mablagh_nobatkari = workSheet.Cells[i, 34].Value != null ? Convert.ToInt32(workSheet.Cells[i, 34].Value) : 0;
        //                es.SP_Malyat_hoghogh = workSheet.Cells[i, 35].Value != null ? Convert.ToInt32(workSheet.Cells[i, 35].Value) : 0;
        //                es.FR_bon_kargari = workSheet.Cells[i, 36].Value != null ? Convert.ToInt32(workSheet.Cells[i, 36].Value) : 0;
        //                es.FR_mozd_sanavat = workSheet.Cells[i, 37].Value != null ? Convert.ToInt32(workSheet.Cells[i, 37].Value) : 0;
        //                es.SP_mablagh_MAMOREYAT = workSheet.Cells[i, 38].Value != null ? Convert.ToInt32(workSheet.Cells[i, 38].Value) : 0;
        //                es.FR_Karane = workSheet.Cells[i, 39].Value != null ? Convert.ToInt32(workSheet.Cells[i, 39].Value) : 0;
        //                es.FR_mamoriat = workSheet.Cells[i, 40].Value != null ? Convert.ToInt32(workSheet.Cells[i, 40].Value) : 0;
        //                es.FR_Eydi = workSheet.Cells[i, 41].Value != null ? Convert.ToInt32(workSheet.Cells[i, 41].Value) : 0;
        //                es.FR_m_maliat_EYDI = workSheet.Cells[i, 42].Value != null ? Convert.ToInt32(workSheet.Cells[i, 42].Value) : 0;
        //                es.FR_Date_StartKar = workSheet.Cells[i, 43].Value != null ? workSheet.Cells[i, 43].Value.ToString() : "";
        //                es.FR_MandehMorakhasi = workSheet.Cells[i, 44].Value != null ? workSheet.Cells[i, 44].Value.ToString() : "0";
        //                es.SY_GHaybat_Ruz = workSheet.Cells[i, 45].Value != null ? Convert.ToInt16(workSheet.Cells[i, 45].Value) : zeroInt16;
        //                es.SY_Morkhasi_Ruz = workSheet.Cells[i, 46].Value != null ? Convert.ToInt16(workSheet.Cells[i, 46].Value) : zeroInt16;
        //                es.SY_Morkhasi_Saat = workSheet.Cells[i, 47].Value != null ? workSheet.Cells[i, 44].Value.ToString() : "0";
        //                es.FR_Bimeh_mashin = workSheet.Cells[i, 48].Value != null ? Convert.ToInt32(workSheet.Cells[i, 48].Value) : 0;
        //                es.FR_Sandogh_Z_V = workSheet.Cells[i, 49].Value != null ? Convert.ToInt32(workSheet.Cells[i, 49].Value) : 0;
        //                es.SY_KE_VamGhest2 = workSheet.Cells[i, 50].Value != null ? Convert.ToInt32(workSheet.Cells[i, 50].Value) : 0;
        //                es.SY_KE_NameVam1 = workSheet.Cells[i, 51].Value != null ? workSheet.Cells[i, 51].Value.ToString() : "";
        //                es.SY_KE_NameVam2 = workSheet.Cells[i, 52].Value != null ? workSheet.Cells[i, 52].Value.ToString() : "";
        //                es.SY_KE_NameVam3 = workSheet.Cells[i, 53].Value != null ? workSheet.Cells[i, 53].Value.ToString() : "";
        //                es.SY_KE_NameVam4 = workSheet.Cells[i, 54].Value != null ? workSheet.Cells[i, 54].Value.ToString() : "";
        //                es.SY_KE_NameVam5 = workSheet.Cells[i, 55].Value != null ? workSheet.Cells[i, 55].Value.ToString() : "";
        //                es.SY_KE_VamGhest3 = workSheet.Cells[i, 56].Value != null ? Convert.ToInt32(workSheet.Cells[i, 56].Value) : 0;
        //                es.SY_KE_VamGhest4 = workSheet.Cells[i, 57].Value != null ? Convert.ToInt32(workSheet.Cells[i, 57].Value) : 0;
        //                es.SY_KE_VamGhest5 = workSheet.Cells[i, 58].Value != null ? Convert.ToInt32(workSheet.Cells[i, 58].Value) : 0;
        //                es.SY_KE_VamMandeh2 = workSheet.Cells[i, 59].Value != null ? Convert.ToInt32(workSheet.Cells[i, 59].Value) : 0;
        //                es.SY_KE_VamMandeh3 = workSheet.Cells[i, 60].Value != null ? Convert.ToInt32(workSheet.Cells[i, 60].Value) : 0;
        //                es.SY_KE_VamMandeh4 = workSheet.Cells[i, 61].Value != null ? Convert.ToInt32(workSheet.Cells[i, 61].Value) : 0;
        //                es.SY_KE_VamMandeh5 = workSheet.Cells[i, 62].Value != null ? Convert.ToInt32(workSheet.Cells[i, 62].Value) : 0;
        //                es.FR_mamoriat_vizhe__ = workSheet.Cells[i, 63].Value != null ? Convert.ToInt32(workSheet.Cells[i, 63].Value) : 0;
        //                es.FR_tafavot_tatbigh__ = workSheet.Cells[i, 64].Value != null ? Convert.ToInt32(workSheet.Cells[i, 64].Value) : 0;
                        

        //                employeeSalaryList.Add(es);
        //            }
        //            await _employeeSalary.AddAsync(employeeSalaryList);
        //            await _employeeSalary.SaveChangesAsync();
                   
        //            return Json("ok");
        //        }
                
        //    }
        //    catch (Exception err)
        //    {
        //        return Json(err.Message +" *** Error Row: "+rowError.ToString()+" *** "+ err.InnerException.ToString());
        //        throw;
        //    }
        //}

        [HttpPost]
        public async Task<IActionResult> SalaryUpload(IFormFile myFile)
        {
            int rowError = 0;
            try
            {
                Int16 zeroInt16 = 0;
                string rootFolder = _hostingEnvironment.WebRootPath;
                string fileName = myFile.FileName.Substring(0, myFile.FileName.IndexOf(".")) + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + System.IO.Path.GetExtension(myFile.FileName);
                string filePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/EmployeeSalary", fileName);
                FileInfo file = new FileInfo(Path.Combine(rootFolder, "Files/EmployeeSalary", fileName));

                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    myFile.CopyTo(stream);

                }
                using (ExcelPackage package = new ExcelPackage(file))
                {
                    ExcelWorksheet workSheet = package.Workbook.Worksheets.FirstOrDefault();//["Cnn"];
                    int totalRows = workSheet.Dimension.Rows;

                    List<EmployeeSalary> employeeSalaryList = new List<EmployeeSalary>();

                    for (int i = 2; i <= totalRows; i++)
                    {
                        rowError = i;
                        EmployeeSalary es = new EmployeeSalary();
                        es.Salary_ID = Guid.NewGuid().ToString();
                        es.PersonalCode = workSheet.Cells[i, 1].Value.ToString();
                        es.SalMah = workSheet.Cells[i, 3].Value.ToString() != null ? workSheet.Cells[i, 3].Value.ToString() : "";
                        es.NameMahaleKhedmatHokmi = workSheet.Cells[i, 4].Value.ToString() != null ? workSheet.Cells[i, 4].Value.ToString() : "";
                        es.SP_Khales_Pardakhti = workSheet.Cells[i, 5].Value != null ? Convert.ToInt32(workSheet.Cells[i, 5].Value) : 0;
                        es.FR_Ayab_Zahab = workSheet.Cells[i, 6].Value != null ? Convert.ToInt32(workSheet.Cells[i, 6].Value) : 0;
                        es.FR_Bime_Takmili = workSheet.Cells[i, 7].Value != null ? Convert.ToInt32(workSheet.Cells[i, 7].Value) : 0;
                        es.FR_DastmozdRoozaneh = workSheet.Cells[i, 8].Value != null ? Convert.ToInt32(workSheet.Cells[i, 8].Value) : 0;
                        es.FR_FOGHOLADE = workSheet.Cells[i, 9].Value != null ? Convert.ToInt32(workSheet.Cells[i, 9].Value) : 0;
                        es.FR_Hag_Jazb = workSheet.Cells[i, 10].Value != null ? Convert.ToInt32(workSheet.Cells[i, 10].Value) : 0;
                        es.FR_HAG_MASKAN = workSheet.Cells[i, 11].Value != null ? Convert.ToInt32(workSheet.Cells[i, 11].Value) : 0;
                        es.FR_HAG_MASULEYAT = workSheet.Cells[i, 12].Value != null ? Convert.ToInt32(workSheet.Cells[i, 12].Value) : 0;
                        es.FR_HAG_OLAD = workSheet.Cells[i, 13].Value != null ? Convert.ToInt32(workSheet.Cells[i, 13].Value) : 0;
                        es.FR_Hogug_Payeh = workSheet.Cells[i, 14].Value != null ? Convert.ToInt32(workSheet.Cells[i, 14].Value) : 0;
                        es.FR_Sayer_Mazaya = workSheet.Cells[i, 15].Value != null ? Convert.ToInt32(workSheet.Cells[i, 15].Value) : 0;
                        es.SP_Ezafe_Kar = workSheet.Cells[i, 16].Value != null ? Convert.ToInt32(workSheet.Cells[i, 16].Value) : 0;
                        es.SP_Hag_Bimeh_Karmand = workSheet.Cells[i, 17].Value != null ? Convert.ToInt32(workSheet.Cells[i, 17].Value) : 0;
                        es.SP_Jame_Ezafat = workSheet.Cells[i, 18].Value != null ? Convert.ToInt32(workSheet.Cells[i, 18].Value) : 0;
                        es.SP_Jame_Kosurat = workSheet.Cells[i, 19].Value != null ? Convert.ToInt32(workSheet.Cells[i, 19].Value) : 0;
                        es.SP_Maleyat = workSheet.Cells[i, 20].Value != null ? Convert.ToInt32(workSheet.Cells[i, 20].Value) : 0;
                        es.SP_MashMul_Bimeh = workSheet.Cells[i, 21].Value != null ? Convert.ToInt32(workSheet.Cells[i, 21].Value) : 0;
                        es.SP_Mashmul_Maleyat = workSheet.Cells[i, 22].Value != null ? Convert.ToInt32(workSheet.Cells[i, 22].Value) : 0;
                        es.SY_KE_VamGhest1 = workSheet.Cells[i, 23].Value != null ? Convert.ToInt32(workSheet.Cells[i, 23].Value) : 0;
                        es.SY_KE_VamMandeh1 = workSheet.Cells[i, 24].Value != null ? Convert.ToInt64(workSheet.Cells[i, 24].Value) : 0;
                        es.SY_Saat_AzafeKar = workSheet.Cells[i, 25].Value != null ? Convert.ToInt16(workSheet.Cells[i, 25].Value) : zeroInt16;
                        es.SY_sayer_Kosurat = workSheet.Cells[i, 26].Value != null ? Convert.ToInt32(workSheet.Cells[i, 26].Value) : zeroInt16;
                        es.SY_Tadad_Ruz_Kar = workSheet.Cells[i, 27].Value != null ? Convert.ToInt16(workSheet.Cells[i, 27].Value) : zeroInt16;
                        es.ZR_FOGHOLADE_Special = workSheet.Cells[i, 28].Value != null ? Convert.ToInt32(workSheet.Cells[i, 28].Value) : 0;
                        es.SY_saat_nobat_kari = workSheet.Cells[i, 29].Value != null ? Convert.ToInt32(workSheet.Cells[i, 29].Value) : 0;
                        es.FR_moavaghe = workSheet.Cells[i, 30].Value != null ? Convert.ToInt32(workSheet.Cells[i, 30].Value) : 0;
                        es.FR_mosaede = workSheet.Cells[i, 31].Value != null ? Convert.ToInt32(workSheet.Cells[i, 31].Value) : 0;
                        es.FR_padash__ = workSheet.Cells[i, 32].Value != null ? Convert.ToInt32(workSheet.Cells[i, 32].Value) : 0;
                        es.FR_sayer_ezafat = workSheet.Cells[i, 33].Value != null ? Convert.ToInt32(workSheet.Cells[i, 33].Value) : 0;
                        es.SP_mablagh_nobatkari = workSheet.Cells[i, 34].Value != null ? Convert.ToInt32(workSheet.Cells[i, 34].Value) : 0;
                        es.SP_Malyat_hoghogh = workSheet.Cells[i, 35].Value != null ? Convert.ToInt32(workSheet.Cells[i, 35].Value) : 0;
                        es.FR_bon_kargari = workSheet.Cells[i, 36].Value != null ? Convert.ToInt32(workSheet.Cells[i, 36].Value) : 0;
                        es.FR_mozd_sanavat = workSheet.Cells[i, 37].Value != null ? Convert.ToInt32(workSheet.Cells[i, 37].Value) : 0;
                        es.SP_mablagh_MAMOREYAT = workSheet.Cells[i, 38].Value != null ? Convert.ToInt32(workSheet.Cells[i, 38].Value) : 0;
                        es.FR_Karane = workSheet.Cells[i, 39].Value != null ? Convert.ToInt32(workSheet.Cells[i, 39].Value) : 0;
                        es.FR_mamoriat = workSheet.Cells[i, 40].Value != null ? Convert.ToInt32(workSheet.Cells[i, 40].Value) : 0;
                        es.FR_Eydi = workSheet.Cells[i, 41].Value != null ? Convert.ToInt32(workSheet.Cells[i, 41].Value) : 0;
                        es.FR_m_maliat_EYDI = workSheet.Cells[i, 42].Value != null ? Convert.ToInt32(workSheet.Cells[i, 42].Value) : 0;
                        es.FR_Date_StartKar = workSheet.Cells[i, 43].Value != null ? workSheet.Cells[i, 43].Value.ToString() : "";
                        es.FR_MandehMorakhasi = workSheet.Cells[i, 44].Value != null ? workSheet.Cells[i, 44].Value.ToString() : "0";
                        es.SY_GHaybat_Ruz = workSheet.Cells[i, 45].Value != null ? Convert.ToInt16(workSheet.Cells[i, 45].Value) : zeroInt16;
                        es.SY_Morkhasi_Ruz = workSheet.Cells[i, 46].Value != null ? Convert.ToInt16(workSheet.Cells[i, 46].Value) : zeroInt16;
                        es.SY_Morkhasi_Saat = workSheet.Cells[i, 47].Value != null ? workSheet.Cells[i, 44].Value.ToString() : "0";
                        es.FR_Bimeh_mashin = workSheet.Cells[i, 48].Value != null ? Convert.ToInt32(workSheet.Cells[i, 48].Value) : 0;
                        es.FR_Sandogh_Z_V = workSheet.Cells[i, 49].Value != null ? Convert.ToInt32(workSheet.Cells[i, 49].Value) : 0;
                        es.SY_KE_VamGhest2 = workSheet.Cells[i, 50].Value != null ? Convert.ToInt32(workSheet.Cells[i, 50].Value) : 0;
                        es.SY_KE_NameVam1 = workSheet.Cells[i, 51].Value != null ? workSheet.Cells[i, 51].Value.ToString() : "";
                        es.SY_KE_NameVam2 = workSheet.Cells[i, 52].Value != null ? workSheet.Cells[i, 52].Value.ToString() : "";
                        es.SY_KE_NameVam3 = workSheet.Cells[i, 53].Value != null ? workSheet.Cells[i, 53].Value.ToString() : "";
                        es.SY_KE_NameVam4 = workSheet.Cells[i, 54].Value != null ? workSheet.Cells[i, 54].Value.ToString() : "";
                        es.SY_KE_NameVam5 = workSheet.Cells[i, 55].Value != null ? workSheet.Cells[i, 55].Value.ToString() : "";
                        es.SY_KE_VamGhest3 = workSheet.Cells[i, 56].Value != null ? Convert.ToInt32(workSheet.Cells[i, 56].Value) : 0;
                        es.SY_KE_VamGhest4 = workSheet.Cells[i, 57].Value != null ? Convert.ToInt32(workSheet.Cells[i, 57].Value) : 0;
                        es.SY_KE_VamGhest5 = workSheet.Cells[i, 58].Value != null ? Convert.ToInt32(workSheet.Cells[i, 58].Value) : 0;
                        es.SY_KE_VamMandeh2 = workSheet.Cells[i, 59].Value != null ? Convert.ToInt64(workSheet.Cells[i, 59].Value) : 0;
                        es.SY_KE_VamMandeh3 = workSheet.Cells[i, 60].Value != null ? Convert.ToInt64(workSheet.Cells[i, 60].Value) : 0;
                        es.SY_KE_VamMandeh4 = workSheet.Cells[i, 61].Value != null ? Convert.ToInt64(workSheet.Cells[i, 61].Value) : 0;
                        es.SY_KE_VamMandeh5 = workSheet.Cells[i, 62].Value != null ? Convert.ToInt64(workSheet.Cells[i, 62].Value) : 0;
                        es.FR_mamoriat_vizhe__ = workSheet.Cells[i, 63].Value != null ? Convert.ToInt32(workSheet.Cells[i, 63].Value) : 0;
                        es.FR_tafavot_tatbigh__ = workSheet.Cells[i, 64].Value != null ? Convert.ToInt32(workSheet.Cells[i, 64].Value) : 0;

                        await _employeeSalary.AddAsync(es);
                        await _employeeSalary.SaveChangesAsync();
                    }
                   

                    return Json("ok");
                }

            }
            catch (Exception err)
            {
                return Json(err.Message + " *** Error Row: " + rowError.ToString() + " *** " + err.InnerException.ToString());
                throw;
            }
        }


    }
}