﻿using DomainClasses.AVF;
using DomainClasses.Gallery;
using DomainClasses.Project;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Services.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ViewModels;

namespace MeshkatEnergy.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]
    [DisplayName("پروژه ها")]
    public class ProjectsController : Controller
    {
        public List<SelectListItem> VideoList;
        public IGenericRepository<ProjectGroup> _projectGroupRepository;
        public IGenericRepository<Projects> _projectsRepository;
        public IGenericRepository<Gallery> _galleryRepository;
        public IGenericRepository<ProjectsAdvantage> _projectsAdvantageRepository;
        public IGenericRepository<RateOption> _rateOptionRepository;
        public IGenericRepository<AVFGroup> _aVFGroupRepository;
        public IGenericRepository<AVF> _aVFRepository;
        public IGenericRepository<ProjectVideo> _projectVideoRepository;
        public ProjectsController(IGenericRepository<ProjectGroup> projectGroupRepository, IGenericRepository<Projects> projectsRepository, IGenericRepository<Gallery> galleryRepository
           , IGenericRepository<ProjectVideo> projectVideoRepository, IGenericRepository<ProjectsAdvantage> projectsAdvantageRepository, IGenericRepository<RateOption> rateOptionRepository
            , IGenericRepository<AVFGroup> aVFGroupRepository, IGenericRepository<AVF> aVFRepository)
        {
            _projectGroupRepository = projectGroupRepository;
            _projectsRepository = projectsRepository;
            _galleryRepository = galleryRepository;
            _projectsAdvantageRepository = projectsAdvantageRepository;
            _rateOptionRepository = rateOptionRepository;
            _aVFGroupRepository = aVFGroupRepository;
            _aVFRepository = aVFRepository;
            _projectVideoRepository = projectVideoRepository;
        }
        [DisplayName("صفحه نخست")]

        public IActionResult Index()
        {
            return View();
        }
        public async Task<string> createTreeView(short pgId, string lang)
        {
            return JsonConvert.SerializeObject(await ViewInTreeView(pgId, lang));
        }
        [NonAction]

        public async Task<List<Node>> ViewInTreeView(short PGId, string lang)//گرفتن گروه مطالب بصورت جیسون و فرستادن به تابع ساخت تری ویو
        {
            List<Node> TNodes = new List<Node>();
            List<Node> TNodes2 = new List<Node>();
            // var groups;

            var projectGroupData = await _projectGroupRepository.GetByIdAsync(PGId);

            var groups = await _projectGroupRepository.GetAllAsync(x => x.ParentId == PGId);
            foreach (var item in groups)
            {
                var node = await ViewInTreeView(item.ProjectGroup_ID, lang);
                TNodes2.Add(node.FirstOrDefault());
            }


            TNodes.Add(new Node()
            {
                text = projectGroupData.Title,
                href = "#" + projectGroupData.Title,
                tags = PGId.ToString(),
                nodes = TNodes2
            });

            return TNodes;
        }
        [DisplayName("افزودن گروه پروژه")]

        public IActionResult CreateProjectsGroup()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateProjectsGroup(ProjectGroup projectGroup, IFormFile pgImage)
        {
            if (!Directory.Exists("wwwroot/Files/ProjectsGroup"))
            {
                Directory.CreateDirectory("wwwroot/Files/ProjectsGroup");
            }
            if (!Directory.Exists("wwwroot/Files/ProjectsGroup/Thumb"))
            {
                Directory.CreateDirectory("wwwroot/Files/ProjectsGroup/Thumb");
            }
            if (projectGroup != null)
            {
                if (pgImage != null)
                {
                    projectGroup.GroupImage = pgImage.FileName.Substring(0, pgImage.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(pgImage.FileName);
                    string filePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/ProjectsGroup", projectGroup.GroupImage);
                    string filePathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/ProjectsGroup/Thumb", projectGroup.GroupImage);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await pgImage.CopyToAsync(stream);
                    }
                    //ImageManipulate img = new ImageManipulate();
                    //img.resizeImage(filePath, filePathThumb);

                    await _projectGroupRepository.AddAsync(projectGroup);
                    await _projectGroupRepository.SaveChangesAsync();
                    return Json(projectGroup);
                }
            }
            return View(projectGroup);
        }
        [DisplayName("نمایش  جزئیات گروه پروژه")]

        public async Task<IActionResult> ViewProjectGroup(Int16 id)
        {
            return PartialView(await _projectGroupRepository.GetByIdAsync(id));
        }
        [DisplayName("ویرایش گروه پروژه")]

        public async Task<IActionResult> EditProjectsGroup(short id)
        {
            return View(await _projectGroupRepository.GetByIdAsync(id));
        }
        [HttpPost]
        public async Task<IActionResult> EditProjectsGroup(ProjectGroup projectGroup, IFormFile pgImage)
        {
            if (projectGroup != null)
            {
                if (pgImage != null)
                {
                    if (projectGroup.GroupImage != null)
                    {
                        string oldFilePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/ProjectsGroup", projectGroup.GroupImage);
                        string oldFilePathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/ProjectsGroup/Thumb", projectGroup.GroupImage);
                        await DeleteFilesInProj(oldFilePath, oldFilePathThumb);
                    }


                    projectGroup.GroupImage = pgImage.FileName.Substring(0, pgImage.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(pgImage.FileName);
                    string filePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/ProjectsGroup", projectGroup.GroupImage);
                    string filePathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/ProjectsGroup/Thumb", projectGroup.GroupImage);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await pgImage.CopyToAsync(stream);
                    }
                    //ImageManipulate img = new ImageManipulate();
                    //img.resizeImage(filePath, filePathThumb);


                }
                _projectGroupRepository.Update(projectGroup);
                await _projectGroupRepository.SaveChangesAsync();
                return Json(projectGroup);
            }
            return View(projectGroup);
        }
        [DisplayName("حذف گروه پروژه")]


        public async Task<IActionResult> DeleteProjectGroup(short id)
        {
            var group = await _projectGroupRepository.GetByIdAsync(id);
            if (group != null)
            {
                var childs = await _projectGroupRepository.GetAllAsync(x => x.ParentId == group.ProjectGroup_ID);
                if (childs.Count() == 0) //IsChilde
                {
                    var products = await _projectsRepository.GetAllAsync(x => x.GroupId == group.ProjectGroup_ID);
                    if (products.Count() == 0) //IsProduct
                    {
                        if (group.GroupImage != null)
                        {
                            string filePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/ProjectsGroup", group.GroupImage);
                            string filePathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/ProjectsGroup/Thumb", group.GroupImage);
                            await DeleteFilesInProj(filePath, filePathThumb);
                        }
                        _projectGroupRepository.Delete(group);
                        await _projectGroupRepository.SaveChangesAsync();
                        return Json(group);
                    }
                    return Json("isProjects");
                }
                return Json("NOK");
            }
            return Json(group);
        }

        ///////////////////////////////////////// بخش پروژه ها
        ///  
        [DisplayName("لیست پروژه ها")]

        public async Task<ActionResult> ViewAllProjects(int id)
        {
            var projects = await _projectsRepository.GetAllAsync(x => x.GroupId == id);
            if (projects != null)
            {
                return PartialView(projects);
            }
            return View(projects);
        }
        [DisplayName("افزودن پروژه ")]

        public async Task<IActionResult> CreateProjects()
        {
            ViewBag.allGallery = new SelectList(await _galleryRepository.GetAllAsync(), "Gallery_ID", "GalleryTitle");
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateProjects(Projects projects, IFormFile imgProject, IFormFile video)
        {
            if (!Directory.Exists("wwwroot/Files/Projects/"))
            {
                Directory.CreateDirectory("wwwroot/Files/Projects/");
            }
            if (!Directory.Exists("wwwroot/Files/Projects/Thumb"))
            {
                Directory.CreateDirectory("wwwroot/Files/Projects/Thumb");
            }
            if (projects != null)
            {

                if (imgProject != null)
                {
                    projects.Image = imgProject.FileName.Substring(0, imgProject.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(imgProject.FileName);
                    string path = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Projects/", projects.Image);
                    string pathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Projects/Thumb", projects.Image);


                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await imgProject.CopyToAsync(stream);
                    }
                    //ImageManipulate img = new ImageManipulate();
                    //img.resizeImage2(path, pathThumb,303,273);
                }
                else {
                    projects.Image = "noimage.png";
                }
                projects.CreateDate = DateTime.Now;
                await _projectsRepository.AddAsync(projects);
                await _projectsRepository.SaveChangesAsync();
                return Json(projects);
            }
            return Json("nall");
        }
        [DisplayName("ویرایش پروژه ")]


        public async Task<IActionResult> EditProjects(int id)
        {
            var project = await _projectsRepository.GetByIdAsync(id);
            ViewBag.allGallery = new SelectList(await _galleryRepository.GetAllAsync(), "Gallery_ID", "GalleryTitle", project.GalleryId);
            return View(project);
        }
        [HttpPost]
        public async Task<IActionResult> EditProjects(Projects projects, IFormFile imgProject, IFormFile video)
        {
            if (projects != null)
            {

                if (imgProject != null)
                {
                    if (projects.Image != null)
                    {
                        string oldcatalogpath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Projects", projects.Image);
                        string oldcatalogpathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Projects/Thumb", projects.Image);
                        await DeleteFilesInProj(oldcatalogpath, oldcatalogpathThumb);
                    }
                    projects.Image = imgProject.FileName.Substring(0, imgProject.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(imgProject.FileName);
                    string path = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Projects/", projects.Image);
                    string pathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Projects/Thumb", projects.Image);


                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await imgProject.CopyToAsync(stream);
                    }
                    //ImageManipulate img = new ImageManipulate();
                    //img.resizeImage(path, pathThumb);
                }
                _projectsRepository.Update(projects);
                await _projectsRepository.SaveChangesAsync();
                return Json(projects);
            }
            return Json("nall");
        }
        [DisplayName("حذف پروژه ")]

        public async Task<IActionResult> checkboxSelectedProjects(string values)
        {
            if (values != "")
            {
                string[] splitedValues = values.Split('&');
                for (int i = 0; i < splitedValues.Length - 1; i++)
                {
                    var project = await _projectsRepository.GetByIdAsync(Int32.Parse(splitedValues[i]));
                    if (project.Image != null)
                    {
                        string oldcatalogpath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Projects", project.Image);
                        string oldcatalogpathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Projects/Thumb", project.Image);
                        await DeleteFilesInProj(oldcatalogpath, oldcatalogpathThumb);
                    }


                    _projectsRepository.Delete(project);
                    await _projectsRepository.SaveChangesAsync();
                }
                return Json("ok");
            }
            return Json("no");
        }
        ////////////////////////////////////////// بخش قابلیت های پروژه
        /// 
        [DisplayName("لیست قابلیت ها")]

        public async Task<IActionResult> ViewAllAdvantage(int id)
        {
            //var products = await _productInfoRepository.GetWithIncludeAsync<ProductInfo>
            //   (
            //   selector: x => x,
            //   include: s => s.Include(x => x.MainProduct).Include(x => x.ProductsGroup).Include(x => x.Off).Include(x => x.Unit).Include(x => x.Producer),
            //   where: x => x.ProductGroupId == id
            //   );
            var projects = await _projectsRepository.GetByIdAsync(id);
            ViewBag.projectsTitle = projects.Title;
            ViewBag.projectsId = projects.Project_ID;
            var advantage = await _projectsAdvantageRepository.GetAllAsync(x => x.ProductInfoId == id);
            return PartialView(advantage);
        }
        [DisplayName("افزودن قابلیت ")]

        public IActionResult CreateAdvantage()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateAdvantage(ProjectsAdvantage projectsAdvantage)
        {
            if (projectsAdvantage != null)
            {
                await _projectsAdvantageRepository.AddAsync(projectsAdvantage);
                await _projectsAdvantageRepository.SaveChangesAsync();
                return Json(projectsAdvantage);
            }
            return Json("nall");
        }
        [DisplayName("ویرایش قابلیت ")]

        public async Task<IActionResult> EditAdvantage(short id)
        {
            return View(await _projectsAdvantageRepository.GetByIdAsync(id));
        }
        [HttpPost]
        public async Task<IActionResult> EditAdvantage(ProjectsAdvantage projectsAdvantage)
        {
            if (projectsAdvantage != null)
            {
                _projectsAdvantageRepository.Update(projectsAdvantage);
                await _projectsAdvantageRepository.SaveChangesAsync();
                return Json(projectsAdvantage);
            }
            return Json("nall");
        }
        [DisplayName(" حذف قابلیت پروژه")]

        public async Task<IActionResult> checkboxSelectedProjectsAdvantage(string values)
        {
            if (values != "")
            {
                string[] splitedValues = values.Split('&');
                for (int i = 0; i < splitedValues.Length - 1; i++)
                {
                    _projectsAdvantageRepository.DeleteById(Int16.Parse(splitedValues[i]));
                    await _projectsAdvantageRepository.SaveChangesAsync();
                }
                return Json("ok");
            }
            return Json("no");
        }
        ////////////////////////////////////////// بخش امتیازات محصول
        /////
        ///   
        [DisplayName("نمایش امتیاز پروژه")]
        public async Task<IActionResult> ViewRateOptionDetails(int id)
        {
            var RateOption = await _rateOptionRepository.GetWithIncludeAsync<RateOption>
                (
                  selector: x => x,
                  where: x => x.ProjectId == id
                );
            return PartialView(RateOption.FirstOrDefault());
        }
        [NonAction]
        public async Task<IActionResult> DeleteFilesInProj(string path, string pathThumb)
        {
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            if (System.IO.File.Exists(pathThumb))
            {
                System.IO.File.Delete(pathThumb);
            }
            return Json(Ok());
        }
        ////////////////////////////////////////////// بخش ویدئو محصولات
        ///
        public async Task<string> createTreeViewVideo(short pgId, string lang)
        {
            var s = await ViewInTreeViewVideo(pgId, lang);
            return JsonConvert.SerializeObject(s);
        }
        [NonAction]
        public async Task<List<Node>> ViewInTreeViewVideo(short gId, string lang)
        {
            List<Node> TNodes = new List<Node>();
            List<Node> TNodes2 = new List<Node>();
            // var groups;

            var GroupData = await _aVFGroupRepository.GetByIdAsync(gId);

            var groups = await _aVFGroupRepository.GetAllAsync(x => x.ParentId == gId, x => x.OrderByDescending(s => s.AVFGroupTitle));
            foreach (var item in groups)
            {
                var node = await ViewInTreeViewVideo(item.AVFGroup_ID, lang);
                TNodes2.Add(node.FirstOrDefault());
            }


            TNodes.Add(new Node()
            {
                text = GroupData.AVFGroupTitle,
                href = "#" + GroupData.AVFGroupTitle,
                tags = gId.ToString(),
                nodes = TNodes2
            });

            return TNodes;
        }
        [DisplayName("نمایش ویدئو های پروژه")]

        public async Task<IActionResult> ViewAllVideo(int id)
        {
            if (id != 0)
            {
                var product = await _projectsRepository.GetByIdAsync(id);
                ViewBag.productTitle = product.Title;
                var video = await _projectVideoRepository.GetWithIncludeAsync<ProjectVideo>
                    (
                    selector: x => x,
                    include: x => x.Include(z => z.AVF).Include(z => z.Projects),
                    where: x => x.ProjectId == id
                    );
                return PartialView(video);
            }
            return View();
        }
        public async Task<JsonResult> GetAllVideoByIdInProject(int id)
        {
            VideoList = new List<SelectListItem>();
            VideoList.Add(new SelectListItem
            {
                Text = "انتخاب کنید ...",
                Value = "0"
            });
            foreach (var item in await _aVFRepository.GetAllAsync(x => x.AVFGroupId == id))
            {
                VideoList.Add(new SelectListItem
                {
                    Text = item.AVFTitle,
                    Value = item.AVF_ID.ToString()
                });
            }


            return Json(VideoList);
        }
        [DisplayName("افزودن ویدئو پروژه")]

        [HttpPost]
        public async Task<IActionResult> CreateProjectVideo(ProjectVideo projectVideo, string[] VideoIds)
        {
            if (projectVideo != null)
            {

                for (int i = 0; i < VideoIds.Length; i++)
                {
                    ///if (!_projectsRepository.IsExistProjectVideo(projectVideo.ProjectId.Value, Int32.Parse(VideoIds[i])))
                    ///{
                    var isExist = await _projectVideoRepository.GetAsync(x => x.ProjectId == projectVideo.ProjectId && x.VideoId == Int32.Parse(VideoIds[i]));
                    var allProjects = await _projectVideoRepository.GetAllAsync();
                    if (isExist == null)
                    {
                        int Id = 0;
                        if (allProjects.Count() > 0)
                        {
                            Id = allProjects.LastOrDefault().ID;
                        }
                        projectVideo.ID = Id + 1;
                        projectVideo.VideoId = Int32.Parse(VideoIds[i]);
                        await _projectVideoRepository.AddAsync(projectVideo);
                        await _projectVideoRepository.SaveChangesAsync();
                    }
                    else
                    {
                        return Json(new { status = "repeate" });
                    }
                }
                return Json(new { status = true, projectId = projectVideo.ProjectId });

            }
            return Json("nall");
        }
        [DisplayName("حذف ویدئو پروژه")]


        public async Task<IActionResult> checkboxSelectedProjectsVideo(string values)
        {
            if (values != "")
            {
                string[] splitedValues = values.Split('&');
                for (int i = 0; i < splitedValues.Length - 1; i++)
                {
                    _projectVideoRepository.DeleteById(Int32.Parse(splitedValues[i]));
                    await _projectVideoRepository.SaveChangesAsync();
                }
                return Json("ok");
            }
            return Json("no");
        }
    }
}