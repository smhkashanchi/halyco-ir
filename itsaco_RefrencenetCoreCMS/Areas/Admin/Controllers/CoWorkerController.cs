﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Supporter;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories;
using Utilities;

namespace MeshkatEnergy.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]
    [DisplayName("گواهینامه ها و افتخارات")]
    public class CoWorkerController : Controller
    {
        public IGenericRepository<CoWorker> _coWorkerRepository;
        public CoWorkerController(IGenericRepository<CoWorker> coWorkerRepository)
        {
            _coWorkerRepository = coWorkerRepository;
        }

        [DisplayName("صفحه نخست")]
        public IActionResult Index()
        {
            return View();
        }

        [DisplayName("لسیت گواهینامه ها و افتخارات")]
        public async Task<IActionResult> ViewAll()
        {
            return PartialView( await _coWorkerRepository.GetAllAsync(null,x=>x.OrderByDescending(s=>s.CoWorker_ID)));
        }

        [DisplayName("افزودن")]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(CoWorker coWorker,IFormFile photo)
        {
            if (!Directory.Exists("wwwroot/Files/Supporter/Temp"))
            {
                Directory.CreateDirectory("wwwroot/Files/Supporter/Temp");
            }
            if (!Directory.Exists("wwwroot/Files/Supporter"))
            {
                Directory.CreateDirectory("wwwroot/Files/Supporter");
            }
            if (coWorker != null)
            {
                if (photo != null)
                {
                    coWorker.Photo = photo.FileName.Substring(0, photo.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(photo.FileName);
                    string TempPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Supporter/Temp", coWorker.Photo);
                    string photoPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Supporter", coWorker.Photo);

                    using (var stream = new FileStream(TempPath, FileMode.Create))
                    {
                        await photo.CopyToAsync(stream);
                    }
                    ImageManipulate imageManipulate = new ImageManipulate();
                    imageManipulate.resizeImage2(TempPath, photoPath, 135, 90);
                   // System.IO.File.Delete(TempPath);
                }
                await _coWorkerRepository.AddAsync(coWorker);
                await _coWorkerRepository.SaveChangesAsync();
                return Json(coWorker);
            }
            return Json("nalll");
        }

        [DisplayName("ویرایش")]
        public async Task<IActionResult> Edit(Int16 id)
        {
            return View(await _coWorkerRepository.GetByIdAsync(id));
        }

        [HttpPost]
        public async Task<IActionResult> Edit(CoWorker coWorker,IFormFile photo)
        {
            if (!Directory.Exists("wwwroot/Files/Supporter/Temp"))
            {
                Directory.CreateDirectory("wwwroot/Files/Supporter/Temp");
            }
            if (!Directory.Exists("wwwroot/Files/Supporter"))
            {
                Directory.CreateDirectory("wwwroot/Files/Supporter");
            }
            if (coWorker != null)
            {
                if (photo != null)
                {
                    if (coWorker.Photo != null)
                    {
                        string photoPathOld = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Supporter", coWorker.Photo);
                        await DeleteFilesInProj(photoPathOld);

                        string TempphotoPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Supporter/Temp", coWorker.Photo);
                        await DeleteFilesInProj(TempphotoPath);
                    }
                    coWorker.Photo = photo.FileName.Substring(0, photo.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(photo.FileName);
                    string TempPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Supporter/Temp", coWorker.Photo);
                    string photoPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Supporter", coWorker.Photo);

                    using (var stream=new FileStream(TempPath, FileMode.Create))
                    {
                        await photo.CopyToAsync(stream);
                    }
                    ImageManipulate imageManipulate = new ImageManipulate();
                    imageManipulate.resizeImage2(TempPath, photoPath, 135, 90);
                   // System.IO.File.Delete(TempPath);
                }
                _coWorkerRepository.Update(coWorker);
                await _coWorkerRepository.SaveChangesAsync();
                return Json(coWorker);
            }
            return Json("nalll");
        }

        [DisplayName("حذف")]
        public async Task<IActionResult> Delete(Int16 id)
        {
            var coworker = await _coWorkerRepository.GetByIdAsync(id);
            if (coworker != null)
            {
                string photoPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Supporter", coworker.Photo);
                await DeleteFilesInProj(photoPath);

                string TempphotoPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Supporter/Temp", coworker.Photo);
                await DeleteFilesInProj(TempphotoPath);

                _coWorkerRepository.Delete(coworker);
                await _coWorkerRepository.SaveChangesAsync();
                return Json(coworker);
            }
            return Json("nall");
        }

        [NonAction]
        public async Task<IActionResult> DeleteFilesInProj(string path)
        {
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
           
            return Json(Ok());
        }
    }
}