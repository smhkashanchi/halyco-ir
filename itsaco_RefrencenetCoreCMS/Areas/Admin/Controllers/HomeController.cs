﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace AspCorePanelProject.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]
    [DisplayName("خانه")]

    public class HomeController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        public HomeController(SignInManager<ApplicationUser> signInManager)
        {
            _signInManager = signInManager;
        }
        [DisplayName("صفحه نخست")]

        public IActionResult Index()
        {
            return View();
        }
       
    }
}