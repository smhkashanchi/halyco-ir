﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.SocialNetwork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Services.Repositories;
using Utilities;

namespace CodeNagarProject.Areas.Admin.Controllers
{
    [Area("admin")]
       [Authorize]
       [DisplayName("شبکه های اجتماعی")]
    public class SocialNetworkController : Controller
    {
        StatusDictionary statusDictionary = new StatusDictionary();
        public IGenericRepository<SocialNetworks> _socialNetworkRepository;
        public SocialNetworkController(IGenericRepository<SocialNetworks> socialNetworkRepository)
        {
            _socialNetworkRepository = socialNetworkRepository;
        }
        [DisplayName("صفحه نخست")]

        public IActionResult Index()
        {
            return View();
        }
        [DisplayName("لیست شبکه های اجتماعی")]

        public async Task<ActionResult> ViewAll()
        {
            return PartialView(await _socialNetworkRepository.GetAllAsync());
        }
        [DisplayName("افزودن")]

        public IActionResult Create()
        {
            ViewData["Types"] = new SelectList(statusDictionary.SocialNetwork_Type, "Key", "Value");
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(SocialNetworks socialNetworks)
        {
            if (socialNetworks != null)
            {
                await _socialNetworkRepository.AddAsync(socialNetworks);
                await _socialNetworkRepository.SaveChangesAsync();
                return Json(socialNetworks);
            }
            ViewData["Types"] = new SelectList(statusDictionary.SocialNetwork_Type, "Key", "Value");
            return Json("nall");
        }
        [DisplayName("ویرایش")]

        public async Task<IActionResult> Edit(byte id)
        {
            ViewData["Types"] = new SelectList(statusDictionary.SocialNetwork_Type, "Key", "Value");
            var socialNet =await _socialNetworkRepository.GetByIdAsync(id);
            ViewBag.type = socialNet.Type;
            return View(socialNet);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(SocialNetworks socialNetworks)
        {
            if (socialNetworks != null)
            {
                _socialNetworkRepository.Update(socialNetworks);
               await _socialNetworkRepository.SaveChangesAsync();
                return Json(socialNetworks);
            }
            ViewData["Types"] = new SelectList(statusDictionary.SocialNetwork_Type, "Key", "Value");
            return Json("nall");
        }
        [DisplayName("حذف")]

        public async Task<IActionResult> Delete(byte id)
        {
            var socialNet =await _socialNetworkRepository.GetByIdAsync(id);
            if (socialNet != null)
            {

                _socialNetworkRepository.Delete(socialNet);
                await _socialNetworkRepository.SaveChangesAsync();
                return Json(socialNet);
            }
            return Json("nall");
        }
    }
}