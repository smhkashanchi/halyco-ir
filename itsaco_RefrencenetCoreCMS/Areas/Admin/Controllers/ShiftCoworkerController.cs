﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Employees;
using DomainClasses.Role;
using DomainClasses.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

using Services;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    /// <summary>
    /// درخواست شیفت همکاران(مخصوص جایگزین شیفت)
    /// </summary>
    [Area("Admin")]
    [Authorize]
    [DisplayName("درخواست تعویض شیفت همکاران")]
    public class ShiftCoworkerController : Controller
    {
        public UserManager<ApplicationUser> _userManager;
        public RoleManager<ApplicationRole> _roleManager;
        public IGenericRepository<Employee> _empgenericRepository;
        public IGenericRepository<ApplicationRole> _ApplicationRoleRepository;
        public IGenericRepository<ApplicationUser> _userRepository;
        public IGenericRepository<ShiftChange> _ShiftChangeRepository;
        public ISMSSender _smsSender;
        public static byte SupervionLevel;
        public ShiftCoworkerController(UserManager<ApplicationUser> userManager, IGenericRepository<Employee> empgenericRepository, IGenericRepository<Vacation> vacationRepository, IGenericRepository<ApplicationRole> ApplicationRoleRepository, RoleManager<ApplicationRole> roleManager, IGenericRepository<ApplicationUser> userRepository, IGenericRepository<ShiftChange> ShiftChangeRepository,ISMSSender smsSender)
        {
            _userManager = userManager;
            _empgenericRepository = empgenericRepository;
            _ApplicationRoleRepository = ApplicationRoleRepository;
            _roleManager = roleManager;
            _userRepository = userRepository;
            _ShiftChangeRepository = ShiftChangeRepository;
            _smsSender = smsSender;
        }

        [DisplayName("صفحه نخست")]
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.OfficeUnit),
                where: x => x.UserIdentity_Id == user.Id
                );
            if (user != null && employee.FirstOrDefault() != null)
            {
                ViewBag.FullName = user.FullName;
                ViewBag.PersonalCode = employee.FirstOrDefault().PersonalCode;
            }
            return View();
        }

        [DisplayName("لیست تعویض شیفت ها")]
        public async Task<IActionResult> ViewAll(string id,string filterList)
        {
            var vacations = await _ShiftChangeRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.Employee_Shc_1).ThenInclude(s => s.ApplicationUser).Include(s => s.Employee_Shc_2).ThenInclude(s => s.ApplicationUser),
                where: x => x.PersonalCodeSecond == id,
                 orderBy: x => x.OrderByDescending(s => s.ShiftChange_ID)
                );
            //filter 
            if (filterList == "-1")
            {
                vacations = vacations.Where(x => x.StatusAlternateSignature == null).ToList();
            }
            else if (filterList == "1")
            {
                vacations = vacations.Where(x => x.StatusAlternateSignature == true).ToList();
            }
            else if (filterList == "0")
            {
                vacations = vacations.Where(x => x.StatusAlternateSignature == false).ToList();
            }
            return PartialView(vacations);
        }

        [DisplayName("ویرایش مرخصی")]
        public async Task<IActionResult> Edit(int id)
        {
            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.OfficeUnit),
                where: x => x.UserIdentity_Id == user.Id
                );
            if (user != null && employee.FirstOrDefault() != null)
            {
                ViewBag.FullName = user.FullName;
                ViewBag.PersonalCode = employee.FirstOrDefault().PersonalCode;
                ViewBag.UnitName = employee.FirstOrDefault().OfficeUnit.OfficeUnitTitle;
                ViewBag.UnitId = employee.FirstOrDefault().OfficeUnit.OfficeUnitID;
                string alternateClass = "";
                if (employee.FirstOrDefault().HasAlternateEmployee == false)
                {
                    alternateClass = "hidden";
                }
                ViewBag.alternateClass = alternateClass;

                var userRolesList = await _userManager.GetRolesAsync(user);
                userRolesList.Remove("Employee");
                var y = await _userManager.GetUsersInRoleAsync(userRolesList[0]);

                //y.Remove(user);
                List<Employee> coworkerList = new List<Employee>();
                foreach (var item in y)
                {
                    var emp = await _userRepository.GetWithIncludeAsync(
                         selector: x => x,
                         include: x => x.Include(s => s.Employee),
                         where: x => x.Id == item.Id
                         );
                    coworkerList.Add(emp.FirstOrDefault().Employee);
                }
                SelectList coworkerEmployeelist = new SelectList(coworkerList, "PersonalCode", "ApplicationUser.FullName");
                ViewBag.coworkerEmployeelist = coworkerEmployeelist;
            }

            var thisShift = await _ShiftChangeRepository.GetWithIncludeAsync(
                selector:x=>x,
                include:x=>x.Include(s=>s.Employee_Shc_1).ThenInclude(s=>s.ApplicationUser),
                where:x => x.ShiftChange_ID == id
                );
            SupervionLevel = thisShift.FirstOrDefault().Employee_Shc_1.ApplicationUser.Employee.SupervisionLevel.Value;
            ViewBag.SupervionLevel = SupervionLevel;
            return PartialView(thisShift.FirstOrDefault());
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ShiftChange ShiftChange)
        {
            var ThisShift = await _ShiftChangeRepository.GetByIdAsync(ShiftChange.ShiftChange_ID);
            ThisShift.StatusAlternateSignature = ShiftChange.StatusAlternateSignature;
            ThisShift.AlternateSignatureReason = ShiftChange.AlternateSignatureReason;
          
            ThisShift.AlternateDateTime = DateTime.Now;
            _ShiftChangeRepository.Update(ThisShift);
            await _ShiftChangeRepository.SaveChangesAsync();

            //بدست آورن رکورد یوزر کارمند درخواست دهنده
            var RequesterEmployee = await _empgenericRepository.GetByIdAsync(ThisShift.PersonalCodeFirst);
            var RequesterUser = await _userManager.FindByIdAsync(RequesterEmployee.UserIdentity_Id);
            //var user = await _userManager.GetUserAsync(User);
            //بدست آوردن لیست  نام نقش های کارمند درخواست دهنده
            var myRole = await _userManager.GetRolesAsync(RequesterUser);
            //حذف نقش کارمند از لیست نقش های من
            myRole.Remove("Employee");
            //بدست آوردن نقش من
            var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());
            //بدست آوردن نقش  پدر من
            var parentRole = await _ApplicationRoleRepository.GetAsync(x => x.Id == mr.ParentRoleId);
            //بدست آوردن لیست یوزر پدرهای من
            var parentUsers = await _userManager.GetUsersInRoleAsync(parentRole.Name);
            //بدست آوردن شماره موبایل اولین(تنهاترین) پدر من
            string userMobile = parentUsers.FirstOrDefault().Mobile_1;
            //بدست آوردن نام من
            string userName = RequesterUser.FullName;
            if (ShiftChange.StatusAlternateSignature != null && ShiftChange.StatusAlternateSignature == true)
            {
                await _smsSender.SendSubmitShiftChangeMessageSMSAsync(userMobile, userName, ThisShift.shiftDate);
            }
            else { 

                var alternateEmployee = await _empgenericRepository.GetByIdAsync(ThisShift.PersonalCodeSecond);
            var alternateUser = await _userRepository.GetByIdAsync(alternateEmployee.UserIdentity_Id);

            var Employee = await _empgenericRepository.GetByIdAsync(ThisShift.PersonalCodeFirst);
            var User = await _userRepository.GetByIdAsync(Employee.UserIdentity_Id);

            await _smsSender.SendRefuseShiftChangeByCoworkerMessageSMSAsync(User.Mobile_1, alternateUser.FullName,ThisShift.shiftDate);
            }
            return Json(new { status = "ok" });
        }
    }
}