﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using DomainClasses.Employees;
using DomainClasses.Role;
using DomainClasses.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services;
using Services.Repositories;
using Utilities;
using ViewModels;
using static Utilities.FunctionResult;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("ماموریت")]
    public class MissionSPController : Controller
    {
        public UserManager<ApplicationUser> _userManager;
        public RoleManager<ApplicationRole> _roleManager;
        public IGenericRepository<Employee> _empgenericRepository;
        public IGenericRepository<ApplicationRole> _ApplicationRoleRepository;
        public IGenericRepository<ApplicationUser> _userRepository;
        public IGenericRepository<Mission> _MissionRepository;
        public IGenericRepository<MissionSignature> _MissionSignatureRepository;
        public ISMSSender _smsSender;
        public string SendFinalSmsToUser_UserId = ""; //شناسه کاربری که تاییدیه نهایی مرخصی را می دهد
        public static byte SupervionLevel;
        public MissionSPController(UserManager<ApplicationUser> userManager, IGenericRepository<Employee> empgenericRepository, IGenericRepository<ApplicationRole> ApplicationRoleRepository, RoleManager<ApplicationRole> roleManager, IGenericRepository<ApplicationUser> userRepository, IGenericRepository<Mission> MissionRepository, ISMSSender smsSender, IGenericRepository<MissionSignature> MissionSignatureRepository)
        {
            _userManager = userManager;
            _empgenericRepository = empgenericRepository;
            _ApplicationRoleRepository = ApplicationRoleRepository;
            _roleManager = roleManager;
            _userRepository = userRepository;
            _MissionRepository = MissionRepository;
            _smsSender = smsSender;
            _MissionSignatureRepository = MissionSignatureRepository;
        }

        [DisplayName("صفحه نخست")]
        public IActionResult Index()
        {
            return View();
        }

        [DisplayName("ثبت ماموریت")]
        public async Task<IActionResult> Create()
        {
            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                where: x => x.UserIdentity_Id == user.Id
                );
            if (user != null && employee.FirstOrDefault() != null)
            {
                ViewBag.FullName = user.FullName;
                ViewBag.PersonalCode = employee.FirstOrDefault().PersonalCode;
                SupervionLevel = employee.FirstOrDefault().SupervisionLevel.Value;
            }
            return PartialView();
        }

        [HttpPost]
        public async Task<simpleFunctionResult> Create(Mission mission, string sdate, string edate)
        {
            try
            {
                //بدست آورن رکورد یوزر من
                var user = await _userManager.GetUserAsync(User);
                //بدست آوردن لیست  نام نقش های من
                var myRole = await _userManager.GetRolesAsync(user);
                //حذف نقش کارمند از لیست نقش های من
                myRole.Remove("Employee");
                //بدست آوردن نقش من
                var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());
                var FinalUserRole = await _ApplicationRoleRepository.GetAsync(x => x.Id == mr.SendFinalSmsToUser);
                var FinalUsers = await _userManager.GetUsersInRoleAsync(FinalUserRole.Name);
                var FinalUser = FinalUsers.FirstOrDefault();
                SendFinalSmsToUser_UserId = FinalUser.Id;
                //بدست آوردن نقش  پدر من
                var parentRole = await _ApplicationRoleRepository.GetAsync(x => x.Id == mr.ParentRoleId);
                //بدست آوردن لیست یوزر پدرهای من
                var parentUsers = await _userManager.GetUsersInRoleAsync(parentRole.Name);
                //بدست آوردن شماره موبایل اولین(تنهاترین) پدر من
                string userMobile = parentUsers.FirstOrDefault().Mobile_1;
                //بدست آوردن نام من
                string userName = user.FullName;

                System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                DateTime dtStart = dtDateTime.AddSeconds(double.Parse(sdate)).ToLocalTime();
                DateTime dtEnd = dtDateTime.AddSeconds(double.Parse(edate)).ToLocalTime();
                if (dtStart.Date <= DateTime.Now.Date && mission.type)
                {
                    return new simpleFunctionResult { Status = false, message = "تاریخ شروع ماموریت روزانه نمی تواند امروز یا قبل از آن باشد" };
                }
                if (dtStart.Date > dtEnd.Date && mission.type)
                {
                    return new simpleFunctionResult { Status = false, message = "تاریخ پایان ماموریت روزانه نمی تواند قبل از تاریخ شروع آن باشد" };
                }
                mission.MissionStart = dtStart.ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                mission.MissionEnd = dtEnd.ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                mission.CreateDate = DateTime.Now;
                //if (SupervionLevel == 2)
                //{
                //    mission.StatusSuperiorSignature = true;
                //}
                await _MissionRepository.AddAsync(mission);
                await _MissionRepository.SaveChangesAsync();

                //var mission2 = await checkSpecialState(mission);
                //_MissionRepository.Update(mission2);
                //await _MissionRepository.SaveChangesAsync();

                #region عملیات_ویژه
            

                simpleFunctionResult result = await createMissionSigniture(mission.Mission_ID, user.Id);
                if (result.Status)
                {
                    return new simpleFunctionResult { Status = result.Status, message = result.message };
                }

                #endregion
                string Duration;
               
                    Duration = "از تاریخ " + mission.MissionStart.ToShortPersian() + " تا " + mission.MissionEnd.ToShortPersian();
                await _smsSender.SendSubmitMissionMessageSMSAsync(userMobile, userName, Duration);
                return new simpleFunctionResult { Status = true, message = "ok" };
            }
            catch (Exception err)
            {
                return new simpleFunctionResult { Status = false, message = err.Message + " *** " + err.InnerException };
            }
        }

        /// <summary>
        /// متد ایجاد رکوردهای امضا ماموریت
        /// </summary>
        /// <param name="Vacation_ID">شناسه مرخصی</param>
        /// <param name="PersonalCodeSignaturer">شناسه درخواست کننده مرخصی</param>
        /// <returns></returns>
        [NonAction]
        public async Task<simpleFunctionResult> createMissionSigniture(int Mission_ID, string PersonalCodeSignaturer)
        {
            try
            {
                //بدست آورن رکورد یوزر من
                var user = await _userRepository.GetByIdAsync(PersonalCodeSignaturer);
                //بدست آوردن لیست  نام نقش های من
                var myRole = await _userManager.GetRolesAsync(user);
                //حذف نقش کارمند از لیست نقش های من
                myRole.Remove("Employee");
                //بدست آوردن نقش من
                var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());
                if (mr.ParentRoleId != null)
                {
                    //بدست آوردن نقش  پدر من
                    var parentRole = await _ApplicationRoleRepository.GetAsync(x => x.Id == mr.ParentRoleId);
                    //بدست آوردن لیست یوزر پدرهای من
                    var parentUsers = await _userManager.GetUsersInRoleAsync(parentRole.Name);
                    //ساخت رکورد امضا مرخصی برای نقش پدر یوزر جاری و ذخیره آن
                    MissionSignature vs = new MissionSignature
                    {
                        Mission_ID = Mission_ID,
                        PersonalCodeSignaturer = parentUsers.FirstOrDefault().Id
                    };
                    await _MissionSignatureRepository.AddAsync(vs);
                    await _MissionSignatureRepository.SaveChangesAsync();

                    //اگر شناسه پدر یوزر جاری متفاوت از شناسه تایید کننده نهایی بود این تابع به صورت بازگشتی اجرا شود
                    if (parentUsers.FirstOrDefault().Id != SendFinalSmsToUser_UserId)
                    {
                        simpleFunctionResult result = await createMissionSigniture(Mission_ID, parentUsers.FirstOrDefault().Id);
                        if (!result.Status)
                        {
                            return new simpleFunctionResult { Status = result.Status, message = result.message };
                        }
                    }
                }
                return new simpleFunctionResult { Status = true, message = "ok" };
            }
            catch (Exception err)
            {
                return new simpleFunctionResult { Status = false, message = err.Message + " *** " + err.InnerException };
            }
        }


        //[NonAction]
        /////برای حالت های خاص مثل اینکه درخواست های روسا توسط رئیس اداری به صورت مستقیم بررسی شود.
        //public async Task<Mission> checkSpecialState(Mission mission)
        //{
        //    var temp = await _MissionRepository.GetWithIncludeAsync<Mission>(
        //        selector: x => x,
        //        where: x => x.Mission_ID == mission.Mission_ID,
        //        include: x => x.Include(s => s.Employee_M_1)
        //        );
        //    Mission tempmission = temp.FirstOrDefault();
        //    var user = await _userManager.FindByIdAsync(tempmission.Employee_M_1.UserIdentity_Id);
        //    var userRoles = await _userManager.GetRolesAsync(user);
        //    if (userRoles.Contains("رئیس برنامه ریزی و توسعه و بهبود") ||
        //        userRoles.Contains("رئیس تولید") ||
        //        userRoles.Contains("رئیس فنی") ||
        //        userRoles.Contains("رئیس فروش") ||
        //        userRoles.Contains("رئیس حسابداری") ||
        //        userRoles.Contains("مدیر عامل") ||
        //        userRoles.Contains("رئیس بازرگانی خارجی") ||
        //        userRoles.Contains("مدیر بهره برداری") ||
        //        userRoles.Contains("رئیس دفتر مدیر عامل")
        //        )
        //        tempmission.StatusManagerSignature = true;

        //    return tempmission;
        //}

        [DisplayName("لیست درخواست های ماموریت")]
        public async Task<IActionResult> ViewAll()
        {
            try
            {

            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.OfficeUnit),
                where: x => x.UserIdentity_Id == user.Id
                );

            IEnumerable<MissionSuperiorViewAllViewModel> mission = new List<MissionSuperiorViewAllViewModel>();
            mission = await _MissionRepository.GetWithIncludeAsync(
                       selector: x => new MissionSuperiorViewAllViewModel
                       {
                           CreateDate = (x.CreateDate != null ? x.CreateDate.ToShamsiWithTime() : ""),
                           MissionStart = (x.MissionStart != null ? x.MissionStart.ToShortPersian2() : ""),
                           MissionEnd = (x.MissionEnd != null ? x.MissionEnd.ToShortPersian2() : ""),
                           StartTime = x.StartTime,
                           EndTime = x.EndTime,
                           Mission_ID = x.Mission_ID,
                           FinalStatus=x.FinalStatus,
                           FullName = x.Employee_M_1.ApplicationUser.FullName,
                           MissionSubject=x.MissionSubject,
                           type=x.type
                       },
                       include: x => x.Include(s => s.Employee_M_1).ThenInclude(s=>s.ApplicationUser),
                       where: x => x.PersonalCodeFirst == employee.FirstOrDefault().PersonalCode,
                       orderBy: x => x.OrderByDescending(s => s.Mission_ID)
                       );

            #region jQuery_DataTable_Ajax_Code
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;
            if (!(string.IsNullOrEmpty(sortColumn)))
            {
                mission = mission.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection).ToList();
            }
            if (!string.IsNullOrEmpty(searchValue))
            {
                mission = mission.Where(m => m.FullName.Contains(searchValue) || m.CreateDate.Contains(searchValue)
                || (!string.IsNullOrEmpty(m.StartTime) ? m.StartTime.Contains(searchValue) : false)
                || (!string.IsNullOrEmpty(m.StartTime) ? m.StartTime.Contains(searchValue) : false)
                || m.MissionStart.Contains(searchValue)
                || m.MissionEnd.Contains(searchValue)
                || m.MissionSubject.Contains(searchValue)
                ).ToList();

            }
            recordsTotal = mission.Count();
            var data = mission.Skip(skip).Take(pageSize).ToList();
            var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
            #endregion
            return new JsonResult(jsonData);

            }
            catch (Exception err)
            {

                throw;
            }
        }

        [DisplayName("ویرایش ماموریت")]
        public async Task<IActionResult> Edit(int id)
        {
            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.OfficeUnit),
                where: x => x.UserIdentity_Id == user.Id
                );
            if (user != null && employee.FirstOrDefault() != null)
            {
                ViewBag.FullName = user.FullName;
                ViewBag.PersonalCode = employee.FirstOrDefault().PersonalCode;
                SupervionLevel = employee.FirstOrDefault().SupervisionLevel.Value;
                ViewBag.SupervionLevel = SupervionLevel;
            }

            var mission = await _MissionRepository.GetWithIncludeAsync(
                selector: x => x,
                where: x => x.Mission_ID == id,
                include: x => x.Include(s => s.Employee_M_1).ThenInclude(s=>s.ApplicationUser)
                );


            MissionSuperiorViewModel msvm = new MissionSuperiorViewModel();
            msvm.mission = mission.FirstOrDefault();
            var missionSignature = await _MissionSignatureRepository.GetWithIncludeAsync(
                selector: x => x,
                where: x => x.Mission_ID == id,
                include: x => x.Include(s => s.applicationUser),
                orderBy: x => x.OrderBy(s => s.MissionSignature_ID)
                );
            msvm.missionSignature = missionSignature.ToList();

            ViewBag.firstVSStatus = missionSignature.FirstOrDefault().SignatureStatus;

            return PartialView(msvm);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Mission mission, string sdate, string edate)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            DateTime dtStart = dtDateTime.AddSeconds(double.Parse(sdate)).ToLocalTime();
            DateTime dtEnd = dtDateTime.AddSeconds(double.Parse(edate)).ToLocalTime();
            if (dtStart.Date <= DateTime.Now.Date && mission.type)
            {
                return Json(new { status = false, message = "تاریخ شروع ماموریت روزانه نمی تواند امروز یا قبل از آن باشد" });
            }
            if (dtStart.Date > dtEnd.Date && mission.type)
            {
                return Json(new { status = false, message = "تاریخ پایان ماموریت روزانه نمی تواند قبل از تاریخ شروع آن باشد" });
            }
            mission.MissionStart = dtStart.ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            mission.MissionEnd = dtEnd.ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);

            var ThisMission = await _MissionRepository.GetByIdAsync(mission.Mission_ID);
            ThisMission.MissionSubject = mission.MissionSubject;
            ThisMission.MissionStart = mission.MissionStart;
            ThisMission.MissionEnd = mission.MissionEnd;
            ThisMission.Destination = mission.Destination;
            ThisMission.MissionType = mission.MissionType;
            ThisMission.Reason = mission.Reason;
            ThisMission.Followers = mission.Followers;


            _MissionRepository.Update(ThisMission);
            await _MissionRepository.SaveChangesAsync();

            var user = await _userManager.GetUserAsync(User);
            var myRole = await _userManager.GetRolesAsync(user);
            myRole.Remove("Employee");
            var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());
            var parentRole = await _ApplicationRoleRepository.GetAsync(x => x.Id == mr.ParentRoleId);
            var parentUsers = await _userManager.GetUsersInRoleAsync(parentRole.Name);

            string userMobile = parentUsers.FirstOrDefault().Mobile_1;
            string userName = user.FullName;

            string Duration;
            Duration = "از تاریخ " + mission.MissionStart.ToShortPersian() + " تا " + mission.MissionEnd.ToShortPersian();
            
            await _smsSender.SendSubmitMissionMessageSMSAsync(userMobile, userName, Duration);
            
            return Json(new { status = true });
        }

        [DisplayName("حذف")]
        public async Task<IActionResult> Delete(int id)
        {
            var MissionSignatureList = await _MissionSignatureRepository.GetAllAsync(x => x.Mission_ID == id, orderBy: x => x.OrderBy(s => s.MissionSignature_ID));
            if (MissionSignatureList.FirstOrDefault().SignatureStatus != null)
            {
                return Json("notAllowed");
            }
            var Mission = await _MissionRepository.GetByIdAsync(id);
            if (Mission != null)
            {
                _MissionRepository.Delete(Mission);
                await _MissionRepository.SaveChangesAsync();
                return Json("ok");
            }
            return Json("Nok");
        }

    }
}