﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.AVF;
using DomainClasses.Contents;
using DomainClasses.Gallery;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Services.Repositories;
using Utilities;
using ViewModels;

namespace AspCorePanelProject.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]
    [DisplayName("اخبار و مطالب آموزشی")]

    public class ContentsController : Controller
    {
        public List<SelectListItem> ggList { get; set; }
        public List<SelectListItem> gList { get; set; }
        public List<SelectListItem> avfList { get; set; }
        public IGenericRepository<ContentsGroup> _contentsGroupRepository;
        public IGenericRepository<Contents> _contentsRepository;
        public IGenericRepository<ContentsGallery> _contentsGalleryRepository;
        public IGenericRepository<Gallery> _galleryRepository;
        public IGenericRepository<ContentsTags> _contentsTagsRepository;
        public IGenericRepository<GalleryGroup> _galleryGroupRepository;
        public IGenericRepository<ContentsComments> _contentsCommentRepository;
        public IGenericRepository<AVF> _avfRepository;
        public IGenericRepository<ContentFiles> _contentFilesRepository;
        public IHostingEnvironment _hostingEnvironment;
        public ContentsController(
            IGenericRepository<ContentFiles> contentFilesRepository,
            IHostingEnvironment hostingEnvironment,
            IGenericRepository<ContentsGroup> contentsGroupRepository,
            IGenericRepository<Contents> contentsRepository,
            IGenericRepository<ContentsGallery> contentsGalleryRepository,
            IGenericRepository<Gallery> galleryRepository,
            IGenericRepository<ContentsTags> contentsTagsRepository,
            IGenericRepository<GalleryGroup> galleryGroupRepository,
            IGenericRepository<ContentsComments> contentsCommentRepository,
            IGenericRepository<AVF> avfRepository)
        {
            _avfRepository = avfRepository;
            _hostingEnvironment = hostingEnvironment;
            _contentsGroupRepository = contentsGroupRepository;
            _contentsRepository = contentsRepository;
            _contentsGalleryRepository = contentsGalleryRepository;
            _galleryRepository = galleryRepository;
            _contentsTagsRepository = contentsTagsRepository;
            _galleryGroupRepository = galleryGroupRepository;
            _contentsCommentRepository = contentsCommentRepository;
            _contentFilesRepository = contentFilesRepository;
        }
        [DisplayName("صفحه نخست")]

        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        [Route("file_upload")]
        public IActionResult UploadImage(IFormFile upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            if (upload.Length <= 0) return null;

            var fileName = Guid.NewGuid() + Path.GetExtension(upload.FileName).ToLower();



            var path = Path.Combine(
                _hostingEnvironment.WebRootPath, "CkImages",
                fileName);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                upload.CopyTo(stream);

            }



            var url = $"{"/CkImages/"}{fileName}";


            return Json(new { uploaded = true, url });
        }

        [DisplayName("لیست گروه مطالب")]
        public async Task<IActionResult> ViewAllContentsGroup(int id)
        {
            return PartialView(await _contentsGroupRepository.GetAllAsync(x => x.ParentID == id, x => x.OrderByDescending(s => s.ContentGroupName)));
        }
        public async Task<IActionResult> ViewCnGroupData(short id)//برای نمایش جزئیات گروه در لیست پایین صفحه
        {
            var cngroup = await _contentsGroupRepository.GetByIdAsync(id);
            return PartialView(cngroup);
        }
        public async Task<string> createTreeView(short pgId, string lang)
        {
            return JsonConvert.SerializeObject(await ViewInTreeView(pgId, lang));
        }
        [NonAction]

        public async Task<List<Node>> ViewInTreeView(short PGId, string lang)//گرفتن گروه مطالب بصورت جیسون و فرستادن به تابع ساخت تری ویو
        {
            List<Node> TNodes = new List<Node>();
            List<Node> TNodes2 = new List<Node>();
            // var groups;

            var GroupData = await _contentsGroupRepository.GetByIdAsync(PGId);

            var groups = await _contentsGroupRepository.GetAllAsync(x => x.ParentID == PGId, x => x.OrderByDescending(s => s.ContentGroupName));
            foreach (var item in groups)
            {
                var node = await ViewInTreeView(item.ContentGroup_ID, lang);
                TNodes2.Add(node.FirstOrDefault());
            }


            TNodes.Add(new Node()
            {
                text = " " + GroupData.ContentGroupName,
                href = "#" + GroupData.ContentGroupName,
                tags = PGId.ToString(),
                nodes = TNodes2
            });

            return TNodes;
        }

        //public string GetAllContentsGroupForTreeView(int parentId, string treeViewElement)
        //{
        //    string tves = treeViewElement;
        //    var contentsGroup = _contentsGroupRepository.GetAllContentsGroupByParentId(parentId);
        //    if (contentsGroup.Count() > 0)
        //    {
        //        tves += "<ul>";
        //        foreach (var item in contentsGroup)
        //        {

        //            tves += "<li id='cng_" + item.ContentGroup_ID + "'>" +
        //                "<span class='contentsGroup' id='" + item.ContentGroup_ID + "'>" + item.ContentGroupName + "</span>   (" + item.ContentGroup_ID + ")       " +
        //                "    <i title='مشاهده جزئیات' class='fa fa-fw fa-list-alt text-success mouser' onclick='GetContentsGroupById(" + item.ContentGroup_ID + ")'></i>" +
        //                "<i title='ویرایش' class='fa fa-fw fa-edit text-info mouser' onclick='openModalForEditContentsGroup(" + item.ContentGroup_ID + ")'></i>" +
        //                "<i title='حذف' class='fa fa-fw fa-trash text-danger mouser' onclick='DeleteContentsGroup(" + item.ContentGroup_ID + ")'></i>";
        //            tves = GetAllContentsGroupForTreeView(item.ContentGroup_ID, tves);
        //            tves += "</li>";

        //        }
        //        tves += "</ul>";
        //    }
        //    return tves;
        //}

        //برای تری ویو هایی که بدون عملیات چندگانه هستند
        //public string GetAllContentsGroupForTreeViewWithoutOpt(int parentId, string treeViewElement)
        //{
        //    string tves = treeViewElement;
        //    var contentsGroup = _contentsGroupRepository.GetAllContentsGroupByParentId(parentId);
        //    if (contentsGroup.Count() > 0)
        //    {
        //        tves += "<ul>";
        //        foreach (var item in contentsGroup)
        //        {

        //            tves += "<li>" +
        //                "<span class='contentsGroup' id='" + item.ContentGroup_ID + "'>" + item.ContentGroupName + "</span>   (" + item.ContentGroup_ID + ")";

        //            tves = GetAllContentsGroupForTreeViewWithoutOpt(item.ContentGroup_ID, tves);
        //            tves += "</li>";

        //        }
        //        tves += "</ul>";
        //    }
        //    return tves;
        //}
        [DisplayName("افزودن گروه مطالب")]
        public async Task<IActionResult> CreateContentsGroup()
        {
            var max = await _contentsGroupRepository.GetAllAsync();
            if (max.Count() == 0)
            {
                ViewBag.maxId = 1;
            }
            else
            {
                ViewBag.maxId = max.LastOrDefault().ContentGroup_ID + 1;
            }
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateContentsGroup(ContentsGroup contentsGroup)
        {
            if (contentsGroup != null)
            {
                if (!await _contentsGroupRepository.Exists(x => x.ContentGroup_ID == contentsGroup.ContentGroup_ID))
                {
                    await _contentsGroupRepository.AddAsync(contentsGroup);
                    await _contentsGroupRepository.SaveChangesAsync();
                    return Json(contentsGroup);
                }
                var max = await _contentsGroupRepository.GetAllAsync();
                object[] obj = new object[2];
                obj[0] = "repeate";
                obj[1] = max.LastOrDefault().ContentGroup_ID;
                return Json(obj);
            }
            return View(contentsGroup);
        }
        [DisplayName("ویرایش گروه مطالب")]
        public async Task<IActionResult> EditContentsGroup(short id)
        {
            if (id == 0)
            {
                return NotFound();
            }
            return View(await _contentsGroupRepository.GetByIdAsync(id));
        }
        [HttpPost]
        public async Task<IActionResult> EditContentsGroup(ContentsGroup contentsGroup)
        {
            if (await _contentsGroupRepository.Exists(x => x.ContentGroup_ID == contentsGroup.ContentGroup_ID))
            {
                _contentsGroupRepository.Update(contentsGroup);
                await _contentsGroupRepository.SaveChangesAsync();
                return Json(contentsGroup);
            }
            return View();
        }
        [DisplayName("حذف گروه مطالب")]
        public async Task<IActionResult> DeleteContentsGroup(short id)
        {
            var cnGroup = await _contentsGroupRepository.GetByIdAsync(id);
            if (cnGroup != null)
            {
                if (!await _contentsRepository.Exists(x => x.Content_ID == id))
                {
                    _contentsGroupRepository.Delete(cnGroup);
                    await _contentsGroupRepository.SaveChangesAsync();
                    return Json(cnGroup);
                }
                return Json("NOK");
            }
            return NotFound();
        }
        [DisplayName("لیست مطالب")]
        public async Task<IActionResult> ViewAllContents(int id)
        {
            return PartialView(await _contentsRepository.GetAllAsync(x => x.ContentGroupId == id, x => x.OrderByDescending(s => s.Content_ID)));
        }
        [DisplayName("افزودن مطلب")]
        public async Task<IActionResult> CreateContents(string id)
        {
            ViewBag.allGroups = new SelectList(await _contentsGroupRepository.GetAllAsync(x => x.Lang == id, x => x.OrderByDescending(s => s.ContentGroup_ID)), "ContentsGroup_ID", "ContentsGroupName");

            var max = await _contentsRepository.GetAllAsync();
            if (max.Count() == 0)
            {
                ViewBag.maxId = 1;
            }
            else
            {
                ViewBag.maxId = max.LastOrDefault().Content_ID + 1;
            }
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateContents(Contents contents, IFormFile ContentImage)
        {
            if (!Directory.Exists("wwwroot/Files/Contents/Temp"))
            {
                Directory.CreateDirectory("wwwroot/Files/Contents/Temp");
            }
            if (!Directory.Exists("wwwroot/Files/Contents"))
            {
                Directory.CreateDirectory("wwwroot/Files/Contents");
            }
            if (!Directory.Exists("wwwroot/Files/Contents/Thumb"))
            {
                Directory.CreateDirectory("wwwroot/Files/Contents/Thumb");
            }
            if (!await _contentsRepository.Exists(x => x.Content_ID == contents.Content_ID))
            {
                if (ContentImage != null)
                {
                    contents.ContentImage = ContentImage.FileName.Substring(0, ContentImage.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(ContentImage.FileName);
                    string TempPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Contents/Temp", contents.ContentImage);
                    string filePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Contents", contents.ContentImage);
                    string filePathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Contents/Thumb", contents.ContentImage);

                    using (var stream = new FileStream(TempPath, FileMode.Create))
                    {
                        await ContentImage.CopyToAsync(stream);
                    }

                    ImageManipulate imageManipulate = new ImageManipulate();
                    imageManipulate.resizeImage2(TempPath, filePath, 250, 180);
                    imageManipulate.resizeImage2(TempPath, filePathThumb, 250, 180);
                    System.IO.File.Delete(TempPath);

                    contents.CreateDate = DateTime.Now;
                    await _contentsRepository.AddAsync(contents);
                    await _contentsRepository.SaveChangesAsync();
                    return Json(contents);
                }
            }
            var max = await _contentsRepository.GetAllAsync();
            object[] obj = new object[2];
            obj[0] = "repeate";
            obj[1] = max.LastOrDefault().Content_ID;
            return Json(obj);
        }
        [DisplayName("ویرایش مطلب")]
        public async Task<IActionResult> EditContents(int id)
        {
            var content = await _contentsRepository.GetByIdAsync(id);
            return View(content);
        }
        [HttpPost]
        public async Task<IActionResult> EditContents(Contents contents, IFormFile NewContentImage)
        {
            if (NewContentImage != null)
            {
                string oldFilePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Contents", contents.ContentImage);
                string oldFilePathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Contents/Thumb", contents.ContentImage);
                await DeleteFilesInProj(oldFilePath, oldFilePathThumb);

                contents.ContentImage = NewContentImage.FileName.Substring(0, NewContentImage.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(NewContentImage.FileName);
                string TempPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Contents/Temp", contents.ContentImage);
                string filePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Contents", contents.ContentImage);
                string filePathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Contents/Thumb", contents.ContentImage);

                using (var stream = new FileStream(TempPath, FileMode.Create))
                {
                    await NewContentImage.CopyToAsync(stream);
                }

                ImageManipulate imageManipulate = new ImageManipulate();
                imageManipulate.resizeImage2(TempPath, filePath, 250, 180);
                imageManipulate.resizeImage2(TempPath, filePathThumb, 250, 180);
                System.IO.File.Delete(TempPath);
            }
            _contentsRepository.Update(contents);
            await _contentsRepository.SaveChangesAsync();
            return Json(contents);
        }
        [DisplayName("حذف مطلب")]
        public async Task<IActionResult> DeleteContents(int id)
        {
            var content = await _contentsRepository.GetByIdAsync(id);
            if (content != null)
            {
                string filePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Contents", content.ContentImage);
                string filePathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Contents/Thumb", content.ContentImage);
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                    System.IO.File.Delete(filePathThumb);
                }
                _contentsRepository.Delete(content);
                await _contentsRepository.SaveChangesAsync();
                return Json(content.ContentTitle);
            }
            return View(content);
        }
        [NonAction]
        public async Task<IActionResult> DeleteFilesInProj(string path, string pathThumb)
        {
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            if (System.IO.File.Exists(pathThumb))
            {
                System.IO.File.Delete(pathThumb);
            }
            return Json(Ok());
        }

        //////////// مربوط به بخش گالری مطالب
        [DisplayName("لیست گالری مطالب")]
        public async Task<IActionResult> ViewAllContentsGallery(int id)
        {
            if (id != 0)
            {
                var content = await _contentsRepository.GetByIdAsync(id);
                ViewBag.contentTitle = content.ContentTitle;
                ViewBag.contentId = content.Content_ID;

                var cnGallery = await _contentsGalleryRepository.GetWithIncludeAsync<ContentsGallery>(
                    selector: x => x,
                    where: x => x.ContentId == id,
                    include: x => x.Include(s => s.contents).Include(s => s.Gallery));

                return PartialView(cnGallery);
            }
            return View();
        }
        [DisplayName("افزودن گالری مطالب")]
        public async Task<IActionResult> CreateContentsGallery(string id)
        {
            //ggList = new List<SelectListItem>();
            //ggList.Add(new SelectListItem
            //{
            //    Text = "انتخاب کنید ...",
            //    Value = "0"
            //});
            //foreach (var item in _galleryGroupRepository.GetAllGalleryGroupByLang(id))
            //{
            //    ggList.Add(new SelectListItem
            //    {
            //        Text = item.GalleryGroupTitle,
            //        Value = item.GalleryGroup_ID.ToString()
            //    });
            //}

            //ViewBag.allGalleryGroup = ggList;
            return View();
        }
        public async Task<JsonResult> GetAllGalleryByGroupId(int id) //برای نمایش گالری براساس گروه در دراپ داون صفحه کانتنت
        {
            if (id != 0)
            {
                gList = new List<SelectListItem>();
                foreach (var item in await _galleryRepository.GetWithIncludeAsync<Gallery>(
                    selector: x => x,
                    where: x => x.GalleryGroupId == id,
                   include: x => x.Include(s => s.GalleryGroup)))
                {
                    gList.Add(new SelectListItem
                    {
                        Text = item.GalleryTitle,
                        Value = item.Gallery_ID.ToString()
                    });
                }

                return Json(gList);
            }
            return Json(Ok());
        }
        [HttpPost]
        public async Task<IActionResult> CreateContentsGallery(ContentsGallery contentsGallery)
        {
            if (ModelState.IsValid)
            {
                if (!await _contentsGalleryRepository.Exists(x => x.ContentId == contentsGallery.ContentId && x.GalleryId == contentsGallery.GalleryId))
                {
                    await _contentsGalleryRepository.AddAsync(contentsGallery);
                    await _contentsGalleryRepository.SaveChangesAsync();
                    return Json(contentsGallery.ContentId);
                }
                return Json("repeate");
            }
            return View();
        }
        [DisplayName("حذف گالری مطالب")]

        public async Task checkboxSelected(string values)
        {
            if (values != "")
            {
                string[] splitedValues = values.Split('&');
                for (int i = 0; i < splitedValues.Length - 1; i++)
                {
                    var conntentGallery = await _contentsGalleryRepository.GetAsync(x => x.GalleryId == Int16.Parse(splitedValues[i]));
                    _contentsGalleryRepository.Delete(conntentGallery);
                    await _contentsGalleryRepository.SaveChangesAsync();
                }
            }
        }

        //////////// مربوط به بخش فایل مطالب
        [DisplayName("لیست فایل مطالب")]
        public async Task<IActionResult> ViewAllContentsFiles(int id)
        {
            if (id != 0)
            {
                var content = await _contentsRepository.GetByIdAsync(id);
                ViewBag.contentTitle = content.ContentTitle;
                ViewBag.contentId = content.Content_ID;

                var cnFiles = await _contentFilesRepository.GetWithIncludeAsync(
                    selector: x => x,
                    where: x => x.ContentId == id,
                    include: x => x.Include(s => s.contents).Include(s => s.AVF));

                return PartialView(cnFiles);
            }
            return View();
        }
        [DisplayName("افزودن فایل مطالب")]
        public IActionResult CreateContentFiles()
        {

            return View();
        }
        public async Task<JsonResult> GetAllFilesByGroupId(int id)
        {
            if (id != 0)
            {
                avfList = new List<SelectListItem>();
                foreach (var item in await _avfRepository.GetWithIncludeAsync<AVF>(
                    selector: x => x,
                    where: x => x.AVFGroupId == id)) 
                   //include: x => x.Include(s => s.AVFGroup)))
                {
                    avfList.Add(new SelectListItem
                    {
                        Text = item.AVFTitle,
                        Value = item.AVF_ID.ToString()
                    });
                }

                return Json(avfList);
            }
            return Json(Ok());
        }
        [HttpPost]
        public async Task<IActionResult> CreateContentFiles(ContentFiles contentFiles)
        {

            if (!await _contentFilesRepository.Exists(x => x.ContentId == contentFiles.ContentId && x.AvfId == contentFiles.AvfId))
            {
                await _contentFilesRepository.AddAsync(contentFiles);
                await _contentFilesRepository.SaveChangesAsync();
                return Json(contentFiles.ContentId);
            }
            return Json("repeate");

        }
        [DisplayName("حذف فایل مطالب")]

        public async Task checkboxSelectedContentFile(string values,int id)
        {
            if (values != "")
            {
                string[] splitedValues = values.Split('&');
                for (int i = 0; i < splitedValues.Length - 1; i++)
                {
                    var conntentFile = await _contentFilesRepository.GetAsync(x => x.AvfId == Int16.Parse(splitedValues[i]) && x.ContentId==id);
                    _contentFilesRepository.Delete(conntentFile);
                    await _contentFilesRepository.SaveChangesAsync();
                }
            }
        }

        //////////// مربوط به بخش کلمات کلیدی مطالب
        [DisplayName("افزودن/ویرایش کلمات کلیدی")]
        public async Task<IActionResult> CreateOrEditContentsTag(int id, ContentsTags contentsTags)
        {
            if (!await _contentsTagsRepository.Exists(x => x.ContentId == id)) //اگر داخل این جدول کلمه کلیدی برای این مطلب ثبت نشده بود
            {
                //add
                return Json("add");
            }
            //Update
            return Json("update");
        }
        public IActionResult CreateContentsTag(int id)
        {
            ViewBag.ContentId = id;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateContentsTag(ContentsTags contentsTags)
        {
            if (ModelState.IsValid)
            {
                await _contentsTagsRepository.AddAsync(contentsTags);
                await _contentsTagsRepository.SaveChangesAsync();
                return Json(contentsTags.ContentId);
            }
            return View();
        }
        public async Task<IActionResult> EditContentsTag(int id)
        {
            var contentsTag = await _contentsTagsRepository.GetWithIncludeAsync<ContentsTags>(
                selector: x => x,
                where: x => x.ContentId == id,
                include: x => x.Include(s => s.contents));

            if (contentsTag != null)
            {
                return View(contentsTag.FirstOrDefault());
            }
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> EditContentsTag(ContentsTags contentsTags)
        {
            if (ModelState.IsValid)
            {
                _contentsTagsRepository.Update(contentsTags);
                await _contentsTagsRepository.SaveChangesAsync();
                return Json(contentsTags.ContentId);
            }
            return View();
        }
        [DisplayName("حذف کلمات کلیدی")]

        public async Task DeleteContentsTag(int id)
        {
            if (id != 0)
            {
                if (await _contentsTagsRepository.Exists(x => x.ContentId == id))
                {
                    _contentsTagsRepository.DeleteById(id);
                    await _contentsTagsRepository.SaveChangesAsync();
                }
            }
        }

        //////////// مربوط به بخش کامنت های مطالب
        ///
        [DisplayName("لیست نظرات محصول")]
        public async Task<IActionResult> ViewAllComments(int id)
        {
            if (id != 0)
            {
                var content = await _contentsRepository.GetByIdAsync(id);
                ViewBag.contentTitle = content.ContentTitle;

                var comments = await _contentsCommentRepository.GetWithIncludeAsync<ContentsComments>(
                    selector: x => x,
                    where: x => x.ContentId == id,
                    include: x => x.Include(s => s.Contents));

                return PartialView(comments);
            }
            return View();
        }
        [DisplayName("حذف نظرات محصول")]

        public async Task checkboxSelectedComments(string values)
        {
            if (values != "")
            {
                string[] splitedValues = values.Split('&');
                for (int i = 0; i < splitedValues.Length - 1; i++)
                {

                    _contentsCommentRepository.DeleteById(Int32.Parse(splitedValues[i]));
                    await _contentsGalleryRepository.SaveChangesAsync();
                }
            }
        }
        [DisplayName("ویرایش نظرات محصول")]

        public async Task<IActionResult> EditComment(int id)
        {
            var comment = await _contentsCommentRepository.GetByIdAsync(id);
            if (comment.IsNew)
            {
                comment.IsNew = false;
                _contentsCommentRepository.Update(comment);
                await _contentsCommentRepository.SaveChangesAsync();
            }
            return View(comment);
        }
        [HttpPost]
        public async Task<IActionResult> EditComment(ContentsComments contentsComments)
        {
            if (contentsComments != null)
            {
                _contentsCommentRepository.Update(contentsComments);
                await _contentsCommentRepository.SaveChangesAsync();
                return Json(contentsComments);
            }
            return View();
        }
    }
}