﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DataLayer;
using DomainClasses.Employees;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel;
using Microsoft.AspNetCore.Identity;
using DomainClasses.User;
using DomainClasses.Role;
using Services.Repositories;
using Services;
using static Utilities.FunctionResult;
using ViewModels;
using Utilities;
using System.Linq.Dynamic.Core;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    [DisplayName("تعویض شیفت")]
    public class ShiftChangeController : Controller
    {
        public UserManager<ApplicationUser> _userManager;
        public RoleManager<ApplicationRole> _roleManager;
        public IGenericRepository<Employee> _empgenericRepository;
        public IGenericRepository<ApplicationRole> _ApplicationRoleRepository;
        public IGenericRepository<ApplicationUser> _userRepository;
        public IGenericRepository<ShiftChange> _ShiftChangeRepository;
        public IGenericRepository<ShiftChangeSignature> _ShiftChangeSignatureRepository;
        public ISMSSender _smsSender;
        public string SendFinalSmsToUser_UserId = ""; //شناسه کاربری که تاییدیه نهایی مرخصی را می دهد

        public ShiftChangeController(UserManager<ApplicationUser> userManager, IGenericRepository<Employee> empgenericRepository, IGenericRepository<ShiftChange> shiftChangeRepository, IGenericRepository<ApplicationRole> ApplicationRoleRepository, RoleManager<ApplicationRole> roleManager, IGenericRepository<ApplicationUser> userRepository, IGenericRepository<ShiftChange> ShiftChangeRepository, ISMSSender smsSender, IGenericRepository<ShiftChangeSignature> ShiftChangeSignatureRepository)
        {
            _userManager = userManager;
            _empgenericRepository = empgenericRepository;
            _ApplicationRoleRepository = ApplicationRoleRepository;
            _roleManager = roleManager;
            _userRepository = userRepository;
            _ShiftChangeRepository = ShiftChangeRepository;
            _smsSender = smsSender;
            _ShiftChangeSignatureRepository = ShiftChangeSignatureRepository;
        }

        [DisplayName("صفحه نخست")]
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.OfficeUnit),
                where: x => x.UserIdentity_Id == user.Id
                );
            ViewBag.HasShift = employee.FirstOrDefault().HasShift.Value;
            ViewBag.HasAlternate = employee.FirstOrDefault().HasAlternateEmployee.Value;
            return View();
        }

        [DisplayName("افزودن شیفت")]
        public async Task<IActionResult> Create()
        {
            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.OfficeUnit),
                where: x => x.UserIdentity_Id == user.Id
                );
            ViewBag.FullName = user.FullName;
            ViewBag.personalCode = employee.FirstOrDefault().PersonalCode;
            ViewBag.UnitName = employee.FirstOrDefault().OfficeUnit.OfficeUnitTitle;
            ViewBag.UnitId = employee.FirstOrDefault().OfficeUnit.OfficeUnitID;
            ViewBag.HasAlternate = employee.FirstOrDefault().HasAlternateEmployee.Value;

            var coworkerList = await _empgenericRepository.GetWithIncludeAsync(
               selector: x => x,
               where: x => x.Job == employee.FirstOrDefault().Job && x.UserIdentity_Id != employee.FirstOrDefault().UserIdentity_Id && x.IsActive == true,
               include: x => x.Include(s => s.ApplicationUser)
               );
            SelectList coworkerEmployeelist = new SelectList(coworkerList, "PersonalCode", "ApplicationUser.FullName");
            ViewBag.coworkerEmployeelist = coworkerEmployeelist;

            return View();
        }

        [HttpPost]
        public async Task<simpleFunctionResult> Create(ShiftChange ShiftChange, string sdate)
        {
            try
            {

                //بدست آورن رکورد یوزر من
                var user = await _userManager.GetUserAsync(User);
                //بدست آوردن لیست  نام نقش های من
                var myRole = await _userManager.GetRolesAsync(user);
                //حذف نقش کارمند از لیست نقش های من
                myRole.Remove("Employee");
                //بدست آوردن نقش من
                var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());
                var FinalUserRole = await _ApplicationRoleRepository.GetAsync(x => x.Id == mr.SendFinalSmsToUser);
                var FinalUsers = await _userManager.GetUsersInRoleAsync(FinalUserRole.Name);
                var FinalUser = FinalUsers.FirstOrDefault();
                SendFinalSmsToUser_UserId = FinalUser.Id;
                //بدست آوردن نقش  پدر من
                var parentRole = await _ApplicationRoleRepository.GetAsync(x => x.Id == mr.ParentRoleId);
                //بدست آوردن لیست یوزر پدرهای من
                var parentUsers = await _userManager.GetUsersInRoleAsync(parentRole.Name);
                //بدست آوردن شماره موبایل اولین(تنهاترین) پدر من
                string userMobile = parentUsers.FirstOrDefault().Mobile_1;
                //بدست آوردن نام من
                string userName = user.FullName;

                System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                DateTime dtStart = dtDateTime.AddSeconds(double.Parse(sdate)).ToLocalTime();
                ShiftChange.shiftDate = dtStart.ToString("MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                ShiftChange.CreateDate = DateTime.Now;
                if (ShiftChange.PersonalCodeSecond == null)
                {
                    ShiftChange.StatusAlternateSignature = true;
                }
                await _ShiftChangeRepository.AddAsync(ShiftChange);
                try
                {
                    await _ShiftChangeRepository.SaveChangesAsync();

                    //var ShiftChange2 = await checkSpecialState(ShiftChange);
                    //_ShiftChangeRepository.Update(ShiftChange2);
                    //await _ShiftChangeRepository.SaveChangesAsync();
                    simpleFunctionResult result = await createShiftChageSigniture(ShiftChange.ShiftChange_ID, user.Id);
                    if (!result.Status)
                    {
                        return new simpleFunctionResult { Status = result.Status, message = result.message };
                    }

                    var alternateEmployee = await _empgenericRepository.GetByIdAsync(ShiftChange.PersonalCodeSecond);
                    var alternateUser = await _userRepository.GetByIdAsync(alternateEmployee.UserIdentity_Id);

                    await _smsSender.SendSubmitshiftChangeForCoworkerMessageSMSAsync(alternateUser.Mobile_1, userName, alternateUser.FullName, ShiftChange.shiftDate);

                    return new simpleFunctionResult { Status = true, message = "ok" };
                }
                catch (Exception err)
                {
                    return new simpleFunctionResult { Status = false, message = err.Message + " *** " + err.InnerException };
                }
            }
            catch (Exception err)
            {
                return new simpleFunctionResult { Status = false, message = err.Message + " *** " + err.InnerException };
            }
        }

        /// <summary>
        /// متد ایجاد رکوردهای امضا مرخصی
        /// </summary>
        /// <param name="ShiftChange_ID">شناسه مرخصی</param>
        /// <param name="PersonalCodeSignaturer">شناسه درخواست کننده مرخصی</param>
        /// <returns></returns>
        [NonAction]
        public async Task<simpleFunctionResult> createShiftChageSigniture(int ShiftChange_ID, string PersonalCodeSignaturer)
        {
            try
            {
                //بدست آورن رکورد یوزر من
                var user = await _userRepository.GetByIdAsync(PersonalCodeSignaturer);
                //بدست آوردن لیست  نام نقش های من
                var myRole = await _userManager.GetRolesAsync(user);
                //حذف نقش کارمند از لیست نقش های من
                myRole.Remove("Employee");
                //بدست آوردن نقش من
                var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());
                if (mr.ParentRoleId != null)
                {
                    //بدست آوردن نقش  پدر من
                    var parentRole = await _ApplicationRoleRepository.GetAsync(x => x.Id == mr.ParentRoleId);
                    //بدست آوردن لیست یوزر پدرهای من
                    var parentUsers = await _userManager.GetUsersInRoleAsync(parentRole.Name);
                    //ساخت رکورد امضا مرخصی برای نقش پدر یوزر جاری و ذخیره آن
                    ShiftChangeSignature scs = new ShiftChangeSignature()
                    {
                        ShiftChange_ID = ShiftChange_ID,
                        PersonalCodeSignaturer = parentUsers.FirstOrDefault().Id
                    };
                    await _ShiftChangeSignatureRepository.AddAsync(scs);
                    await _ShiftChangeSignatureRepository.SaveChangesAsync();

                    //اگر شناسه پدر یوزر جاری متفاوت از شناسه تایید کننده نهایی بود این تابع به صورت بازگشتی اجرا شود
                    if (parentUsers.FirstOrDefault().Id != SendFinalSmsToUser_UserId)
                    {
                        simpleFunctionResult result = await createShiftChageSigniture(ShiftChange_ID, parentUsers.FirstOrDefault().Id);
                        if (!result.Status)
                        {
                            return new simpleFunctionResult { Status = result.Status, message = result.message };
                        }
                    }
                }
                return new simpleFunctionResult { Status = true, message = "ok" };
            }
            catch (Exception err)
            {
                return new simpleFunctionResult { Status = false, message = err.Message + " *** " + err.InnerException };
            }
        }


        //[NonAction]
        /////برای حالت های خاص مثل اینکه درخواست های روسا توسط رئیس اداری به صورت مستقیم بررسی شود.
        //public async Task<ShiftChange> checkSpecialState(ShiftChange ShiftChange)
        //{
        //    var temp = await _ShiftChangeRepository.GetWithIncludeAsync<ShiftChange>(
        //        selector: x => x,
        //        where: x => x.ShiftChange_ID == ShiftChange.ShiftChange_ID,
        //        include: x => x.Include(s => s.Employee_Shc_1)
        //        );
        //    ShiftChange tempShiftChange = temp.FirstOrDefault();
        //    var user = await _userManager.FindByIdAsync(tempShiftChange.Employee_Shc_1.UserIdentity_Id);
        //    var userRoles = await _userManager.GetRolesAsync(user);
        //    if (userRoles.Contains("رئیس برنامه ریزی و توسعه و بهبود") ||
        //        userRoles.Contains("رئیس تولید") ||
        //        userRoles.Contains("رئیس فنی") ||
        //        userRoles.Contains("رئیس فروش") ||
        //        userRoles.Contains("رئیس حسابداری") ||
        //        userRoles.Contains("مدیر عامل") ||
        //        userRoles.Contains("رئیس بازرگانی خارجی") ||
        //        userRoles.Contains("مدیر بهره برداری") ||
        //        userRoles.Contains("رئیس دفتر مدیر عامل")
        //        )
        //        tempShiftChange.StatusManagerSignature = true;

        //    return tempShiftChange;
        //}

        [DisplayName("ویرایش شیفت")]
        public async Task<IActionResult> Edit(int? id)
        {
            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.OfficeUnit),
                where: x => x.UserIdentity_Id == user.Id
                );
            if (user != null && employee.FirstOrDefault() != null)
            {
                ViewBag.FullName = user.FullName;
                ViewBag.PersonalCode = employee.FirstOrDefault().PersonalCode;
            }
            ViewBag.UnitName = employee.FirstOrDefault().OfficeUnit.OfficeUnitTitle;
            ViewBag.HasAlternate = employee.FirstOrDefault().HasAlternateEmployee.Value;

            var coworkerList = await _empgenericRepository.GetWithIncludeAsync(
              selector: x => x,
              where: x => x.Job == employee.FirstOrDefault().Job && x.UserIdentity_Id != employee.FirstOrDefault().UserIdentity_Id && x.IsActive == true,
              include: x => x.Include(s => s.ApplicationUser)
              );
            SelectList coworkerEmployeelist = new SelectList(coworkerList, "PersonalCode", "ApplicationUser.FullName");
            ViewBag.coworkerEmployeelist = coworkerEmployeelist;


            var ShiftChange = await _ShiftChangeRepository.GetWithIncludeAsync(
                selector: x => x,
                where: x => x.ShiftChange_ID == id,
                include: x => x.Include(s => s.Employee_Shc_1).Include(s => s.Employee_Shc_2)
                );
            ShiftChangeSuperiorViewModel scsvm = new ShiftChangeSuperiorViewModel();
            scsvm.shiftChange = ShiftChange.FirstOrDefault();
            var shiftChangeSignatures = await _ShiftChangeSignatureRepository.GetWithIncludeAsync(
                selector: x => x,
                where: x => x.ShiftChange_ID == id,
                include: x => x.Include(s => s.applicationUser),
                orderBy: x => x.OrderBy(s => s.ShiftChangeSignature_ID)
                );
            scsvm.shiftChangeSignatures = shiftChangeSignatures.ToList();


            ViewBag.firstVSStatus = shiftChangeSignatures.FirstOrDefault().SignatureStatus;
            return PartialView(scsvm);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ShiftChange ShiftChange, string sdate)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                var myRole = await _userManager.GetRolesAsync(user);
                myRole.Remove("Employee");
                var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());
                var parentRole = await _ApplicationRoleRepository.GetAsync(x => x.Id == mr.ParentRoleId);
                var parentUsers = await _userManager.GetUsersInRoleAsync(parentRole.Name);

                string userMobile = parentUsers.FirstOrDefault().Mobile_1;
                string userName = user.FullName;


                System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                DateTime dtStart = dtDateTime.AddSeconds(double.Parse(sdate)).ToLocalTime();
                ShiftChange.shiftDate = dtStart.ToString("MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                ShiftChange.CreateDate = DateTime.Now;
                _ShiftChangeRepository.Update(ShiftChange);
                await _ShiftChangeRepository.SaveChangesAsync();

                var alternateEmployee = await _empgenericRepository.GetByIdAsync(ShiftChange.PersonalCodeSecond);
                var alternateUser = await _userRepository.GetByIdAsync(alternateEmployee.UserIdentity_Id);

                await _smsSender.SendSubmitshiftChangeForCoworkerMessageSMSAsync(alternateUser.Mobile_1, userName, alternateUser.FullName, ShiftChange.shiftDate);
                return Json(new { status = "ok" });
            }
            catch (Exception err)
            {

                throw;
            }

        }



        [DisplayName("مشاهده لیست درخواست های تعویض شیفت")]
        public async Task<IActionResult> viewAll()
        {
            try
            {

            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.OfficeUnit),
                where: x => x.UserIdentity_Id == user.Id
                );

            //var ShiftChanges = await _ShiftChangeRepository.GetWithIncludeAsync(
            //    selector: x => x,
            //    include: x => x.Include(s => s.Employee_Shc_1).ThenInclude(s => s.ApplicationUser).Include(s => s.Employee_Shc_2).ThenInclude(s => s.ApplicationUser),
            //    where: x => x.PersonalCodeFirst == employee.FirstOrDefault().PersonalCode,
            //    orderBy: x => x.OrderByDescending(s => s.ShiftChange_ID));

            IEnumerable<ShiftChangeSuperiorViewAllViewModel> scsvavm = new List<ShiftChangeSuperiorViewAllViewModel>();
            scsvavm = await _ShiftChangeRepository.GetWithIncludeAsync(
                      selector: x => new ShiftChangeSuperiorViewAllViewModel
                      {
                          CreateDate = x.CreateDate != null ? x.CreateDate.ToShamsiFullDateTime() : "",
                          shiftDate=x.shiftDate.ToShortPersian(),
                          FinalStatus = x.FinalStatus,
                          AlternateFullName = x.Employee_Shc_2.ApplicationUser.FullName,
                          ShiftChange_ID = x.ShiftChange_ID
                      },
                      include: x => x.Include(s => s.Employee_Shc_1),
                      where: x => x.PersonalCodeFirst == employee.FirstOrDefault().PersonalCode,
                      orderBy: x => x.OrderByDescending(s => s.ShiftChange_ID)
                      );


            #region jQuery_DataTable_Ajax_Code
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;
            if (!(string.IsNullOrEmpty(sortColumn)))
            {
                scsvavm = scsvavm.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection).ToList();
            }
            if (!string.IsNullOrEmpty(searchValue))
            {
                scsvavm = scsvavm.Where(m => m.FullName.Contains(searchValue) || m.CreateDate.Contains(searchValue)
                || m.shiftDate.Contains(searchValue)
                ).ToList();

            }
            recordsTotal = scsvavm.Count();
            var data = scsvavm.Skip(skip).Take(pageSize).ToList();
            var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
            #endregion
            return new JsonResult(jsonData);

            }
            catch (Exception err)
            {

                throw;
            }
        }

        [DisplayName("حذف")]
        public async Task<IActionResult> Delete(int id)
        {
            var shiftChangeSignatureList = await _ShiftChangeSignatureRepository.GetAllAsync(x => x.ShiftChange_ID == id, orderBy: x => x.OrderBy(s => s.ShiftChangeSignature_ID));
            if (shiftChangeSignatureList.FirstOrDefault().SignatureStatus != null)
            {
                return Json("notAllowed");
            }

            var ShiftChanges = await _ShiftChangeRepository.GetByIdAsync(id);
            if (ShiftChanges != null)
            {
                _ShiftChangeRepository.Delete(ShiftChanges);
                await _ShiftChangeRepository.SaveChangesAsync();
                return Json("ok");
            }
            return Json("Nok");
        }
    }
}
