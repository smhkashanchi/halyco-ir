﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Employees;
using DomainClasses.Role;
using DomainClasses.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("درخواست وام(مافوق)")]
    public class LoanSuperiorController : Controller
    {
        public UserManager<ApplicationUser> _userManager;
        public RoleManager<ApplicationRole> _roleManager;
        public IGenericRepository<Employee> _empgenericRepository;
        public IGenericRepository<ApplicationRole> _ApplicationRoleRepository;
        public IGenericRepository<ApplicationUser> _userRepository;
        public IGenericRepository<Loan> _LoanRepository;
        public static byte SupervionLevel;
        public LoanSuperiorController(UserManager<ApplicationUser> userManager, IGenericRepository<Employee> empgenericRepository, IGenericRepository<ApplicationRole> ApplicationRoleRepository, RoleManager<ApplicationRole> roleManager, IGenericRepository<ApplicationUser> userRepository, IGenericRepository<Loan> LoanRepository)
        {
            _userManager = userManager;
            _empgenericRepository = empgenericRepository;
            _ApplicationRoleRepository = ApplicationRoleRepository;
            _roleManager = roleManager;
            _userRepository = userRepository;
            _LoanRepository = LoanRepository;
        }

        [DisplayName("صفحه نخست")]
        public IActionResult Index()
        {
            return View();
        }


        [DisplayName("لیست درخواست های وام")]
        public async Task<IActionResult> ViewAll(string id, string LoanPrice_txt, string ApplicationUserFullName_txt, string SuperiorName_txt, string managerName_txt,
            string HRManagerName_txt, string StartCreateDate, string EndCreateDate)
        {
            DateTime dtStart = new DateTime(0), dtEnd = new DateTime();
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            if (StartCreateDate != null)
            {
                dtStart = dtDateTime.AddSeconds(double.Parse(StartCreateDate)).Date;
            }
            else
            {
                //تاریخ روز صفر
            }

            if (EndCreateDate != null)
            {
                dtEnd = dtDateTime.AddSeconds(double.Parse(EndCreateDate)).Date;
                dtEnd = dtEnd.AddDays(1);
            }
            else                         //چون از ساعت 12 نیمه شب تاریخ میخوره برای اینکه درخواست های روز جاری هم شامل بشه به علاوه 1 می کنیم
            {
                dtEnd = DateTime.Now.AddDays(1);          //تاریخ جاری
            }
            var user = await _userManager.GetUserAsync(User);
           
            var myRole = await _userManager.GetRolesAsync(user);
            myRole.Remove("Employee");
            var myEmployeeRole = await _ApplicationRoleRepository.GetAllAsync(x => x.ParentRole.Name == myRole.FirstOrDefault());
            var myEmployees = new List<ApplicationUser>();
            foreach (var item in myEmployeeRole)
            {
                myEmployees.AddRange(await _userManager.GetUsersInRoleAsync(item.Name));
            }
            var AllLoanSuperior = new List<Loan>();
            foreach (var item in myEmployees)
            {
                var loans = await _LoanRepository.GetWithIncludeAsync
                    (
                    selector: x => x,
                    include: x => x.Include(s => s.Employee_lo_1).Include(s => s.Employee_lo_3).ThenInclude(s => s.ApplicationUser).Include(s => s.Employee_lo_4).ThenInclude(s => s.ApplicationUser).Include(s => s.Employee_lo_5).ThenInclude(s => s.ApplicationUser),
                    where: x => x.PersonalCode == item.UserName
                    
                );
                if (loans.Count() > 0)
                {
                    if (loans.FirstOrDefault().StatusSuperiorSignature == true && loans.FirstOrDefault().PersonalCode_superior == null)
                    {
                        // Do nothing
                    }
                    else
                    {
                        AllLoanSuperior.AddRange(loans);
                    }
                }
               
            }
            AllLoanSuperior=AllLoanSuperior.OrderByDescending(x => x.loanID).ToList();
            //filter 
            if (id == "-1")
            {
                AllLoanSuperior = AllLoanSuperior.Where(x => x.StatusSuperiorSignature == null).ToList();
            }
            else if (id == "1")
            {
                AllLoanSuperior = AllLoanSuperior.Where(x => x.StatusSuperiorSignature == true).ToList();
            }
            else if (id == "0")
            {
                AllLoanSuperior = AllLoanSuperior.Where(x => x.StatusSuperiorSignature == false).ToList();
            }
            AllLoanSuperior = AllLoanSuperior.Where(x => x.CreateDate >= dtStart && x.CreateDate <= dtEnd).ToList();
            List<Loan> M = new List<Loan>();
            foreach (var item in AllLoanSuperior)
            {
                try
                {
                    //if (CreateDate_txt != null && item.CreateDate == Convert.ToDateTime(CreateDate_txt))
                    //{
                    //    AV.Add(item);
                    //}
                    if (LoanPrice_txt != null && item.amount == Convert.ToInt32(LoanPrice_txt))
                    {
                        M.Add(item);
                    }
                    if (ApplicationUserFullName_txt != null && item.Employee_lo_1 != null && item.Employee_lo_1.ApplicationUser.FullName.Contains(ApplicationUserFullName_txt))
                    {
                        M.Add(item);
                    }
                    //else if (DayVacationStartDate_txt != null && item.DayVacationStartDate!=null && item.DayVacationStartDate.Contains(DayVacationStartDate_txt))
                    //{
                    //    AV.Add(item);
                    //}
                    //else if (DayVacationEndDate_txt != null && item.DayVacationEndDate!=null && item.DayVacationEndDate.Contains(DayVacationEndDate_txt))
                    //{
                    //    AV.Add(item);
                    //}

                    else if (SuperiorName_txt != null && item.Employee_lo_3 != null && item.Employee_lo_3.ApplicationUser.FullName.Contains(SuperiorName_txt))
                    {
                        M.Add(item);
                    }
                    else if (managerName_txt != null && item.Employee_lo_4 != null && item.Employee_lo_4.ApplicationUser.FullName.Contains(managerName_txt))
                    {
                        M.Add(item);
                    }
                    else if (HRManagerName_txt != null && item.Employee_lo_5 != null && item.Employee_lo_5.ApplicationUser.FullName.Contains(HRManagerName_txt))
                    {
                        M.Add(item);
                    }
                }
                catch (Exception err)
                {
                    continue;
                }
            }
            
            if (M.Count > 0)
            {
                return PartialView(M.AsEnumerable());
            }
            else
            {
                return PartialView(AllLoanSuperior.AsEnumerable());
            }
        }

        [DisplayName("ویرایش وام")]
        public async Task<IActionResult> Edit(int id)
        {
            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.OfficeUnit),
                where: x => x.UserIdentity_Id == user.Id
                );
            if (user != null && employee.FirstOrDefault() != null)
            {
                ViewBag.FullName = user.FullName;
                ViewBag.PersonalCode = employee.FirstOrDefault().PersonalCode;
            }

            var loan = await _LoanRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.Employee_lo_1).ThenInclude(s => s.ApplicationUser),
                where: x => x.loanID == id);
            SupervionLevel = loan.LastOrDefault().Employee_lo_1.ApplicationUser.Employee.SupervisionLevel.Value;
            ViewBag.SupervionLevel = SupervionLevel;

            return PartialView(loan.FirstOrDefault());
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Loan loan)
        {
            var ThisLoan = await _LoanRepository.GetByIdAsync(loan.loanID);
            ThisLoan.PersonalCode_superior = loan.PersonalCode_superior;
            ThisLoan.StatusSuperiorSignature = loan.StatusSuperiorSignature;
            ThisLoan.SuperiorReason = loan.SuperiorReason;
            if (SupervionLevel == 1)
            {
                ThisLoan.StatusManagerSignature = loan.StatusSuperiorSignature;
            }
            ThisLoan.SuperiorDateTime = DateTime.Now;
            _LoanRepository.Update(ThisLoan);
            await _LoanRepository.SaveChangesAsync();
            return Json(new { status = "ok" });
        }
    }
}