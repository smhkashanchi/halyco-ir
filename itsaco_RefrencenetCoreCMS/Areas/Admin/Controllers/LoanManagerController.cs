﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DataLayer;
using DomainClasses.Employees;
using System.ComponentModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using DomainClasses.User;
using DomainClasses.Role;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("درخواست وام(مدیر)")]
    public class LoanManagerController : Controller
    {
        public UserManager<ApplicationUser> _userManager;
        public RoleManager<ApplicationRole> _roleManager;
        public IGenericRepository<Employee> _empgenericRepository;
        public IGenericRepository<ApplicationRole> _ApplicationRoleRepository;
        public IGenericRepository<ApplicationUser> _userRepository;
        public IGenericRepository<Loan> _LoanRepository;
        public static byte SupervionLevel;
        public LoanManagerController(UserManager<ApplicationUser> userManager, IGenericRepository<Employee> empgenericRepository, IGenericRepository<ApplicationRole> ApplicationRoleRepository, RoleManager<ApplicationRole> roleManager, IGenericRepository<ApplicationUser> userRepository, IGenericRepository<Loan> LoanRepository)
        {
            _userManager = userManager;
            _empgenericRepository = empgenericRepository;
            _ApplicationRoleRepository = ApplicationRoleRepository;
            _roleManager = roleManager;
            _userRepository = userRepository;
            _LoanRepository = LoanRepository;
        }

        [DisplayName("صفحه نخست")]
        public IActionResult Index()
        {
            return View();
        }


        [DisplayName("لیست درخواست های وام")]
        public async Task<IActionResult> ViewAll(string id, string LoanPrice_txt, string ApplicationUserFullName_txt, string SuperiorName_txt, string managerName_txt,
            string HRManagerName_txt, string StartCreateDate, string EndCreateDate)
        {
            DateTime dtStart = new DateTime(0), dtEnd = new DateTime();
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            if (StartCreateDate != null)
            {
                dtStart = dtDateTime.AddSeconds(double.Parse(StartCreateDate)).Date;
            }
            else
            {
                //تاریخ روز صفر
            }

            if (EndCreateDate != null)
            {
                dtEnd = dtDateTime.AddSeconds(double.Parse(EndCreateDate)).Date;
                dtEnd = dtEnd.AddDays(1);
            }
            else                         //چون از ساعت 12 نیمه شب تاریخ میخوره برای اینکه درخواست های روز جاری هم شامل بشه به علاوه 1 می کنیم
            {
                dtEnd = DateTime.Now.AddDays(1);          //تاریخ جاری
            }
            var user = await _userManager.GetUserAsync(User);
            var myRole = await _userManager.GetRolesAsync(user);
            myRole.Remove("Employee");
            //نقش هایی که نقش من پدر آنها هست
            var myEmployeeRoles = await _ApplicationRoleRepository.GetAllAsync(x => x.ParentRole.Name == myRole.FirstOrDefault());
            var RolesOfMyEmployeeOfMyEmployeeRole = new List<ApplicationRole>();
            foreach (var item in myEmployeeRoles)
            {
                RolesOfMyEmployeeOfMyEmployeeRole.AddRange(await _ApplicationRoleRepository.GetAllAsync(x => x.ParentRole.Name == item.Name));
            }
            var myEmployeeOfMyEmployee = new List<ApplicationUser>();//کارمندانی که نقش فرزند نقش مدیر را دارند
            foreach (var item in myEmployeeRoles)
            {
                myEmployeeOfMyEmployee.AddRange(await _userManager.GetUsersInRoleAsync(item.Name));
            }
            foreach (var item in RolesOfMyEmployeeOfMyEmployeeRole)
            {
                myEmployeeOfMyEmployee.AddRange(await _userManager.GetUsersInRoleAsync(item.Name));
            }
            var AllLoanM = new List<Loan>();
            foreach (var item in myEmployeeOfMyEmployee)
            {
                var loan = await _LoanRepository.GetWithIncludeAsync
                    (
                    selector: x => x,
                   include: x => x.Include(s => s.Employee_lo_1).ThenInclude(s => s.ApplicationUser).Include(s => s.Employee_lo_3).ThenInclude(s => s.ApplicationUser).Include(s => s.Employee_lo_4).ThenInclude(s => s.ApplicationUser).Include(s => s.Employee_lo_5).ThenInclude(s => s.ApplicationUser),
                    where: x => x.PersonalCode == item.NormalizedUserName && ( x.StatusSuperiorSignature == true)//(x.PersonalCode_superior != null &&)
                );
                if (loan.Count() > 0)
                {
                    if (loan.FirstOrDefault().StatusManagerSignature == true && loan.FirstOrDefault().PersonalCode_manager == null)
                    {
                        // Do nothing
                    }
                    else
                    {
                        AllLoanM.AddRange(loan);
                    }
                }
               
            }
            AllLoanM=AllLoanM.OrderByDescending(x => x.loanID).ToList();
            //filter 
            if (id == "-1")
            {
                AllLoanM = AllLoanM.Where(x => x.StatusManagerSignature == null).ToList();
            }
            else if (id == "1")
            {
                AllLoanM = AllLoanM.Where(x => x.StatusManagerSignature == true).ToList();
            }
            else if (id == "0")
            {
                AllLoanM = AllLoanM.Where(x => x.StatusManagerSignature == false).ToList();
            }
            AllLoanM = AllLoanM.Where(x => x.CreateDate >= dtStart && x.CreateDate <= dtEnd).ToList();
            List<Loan> M = new List<Loan>();
            foreach (var item in AllLoanM)
            {
                try
                {
                    //if (CreateDate_txt != null && item.CreateDate == Convert.ToDateTime(CreateDate_txt))
                    //{
                    //    AV.Add(item);
                    //}
                    if (LoanPrice_txt != null && item.amount == Convert.ToInt32(LoanPrice_txt))
                    {
                        M.Add(item);
                    }
                    if (ApplicationUserFullName_txt != null && item.Employee_lo_1 != null && item.Employee_lo_1.ApplicationUser.FullName.Contains(ApplicationUserFullName_txt))
                    {
                        M.Add(item);
                    }
                    //else if (DayVacationStartDate_txt != null && item.DayVacationStartDate!=null && item.DayVacationStartDate.Contains(DayVacationStartDate_txt))
                    //{
                    //    AV.Add(item);
                    //}
                    //else if (DayVacationEndDate_txt != null && item.DayVacationEndDate!=null && item.DayVacationEndDate.Contains(DayVacationEndDate_txt))
                    //{
                    //    AV.Add(item);
                    //}

                    else if (SuperiorName_txt != null && item.Employee_lo_3 != null && item.Employee_lo_3.ApplicationUser.FullName.Contains(SuperiorName_txt))
                    {
                        M.Add(item);
                    }
                    else if (managerName_txt != null && item.Employee_lo_4 != null && item.Employee_lo_4.ApplicationUser.FullName.Contains(managerName_txt))
                    {
                        M.Add(item);
                    }
                    else if (HRManagerName_txt != null && item.Employee_lo_5 != null && item.Employee_lo_5.ApplicationUser.FullName.Contains(HRManagerName_txt))
                    {
                        M.Add(item);
                    }
                }
                catch (Exception err)
                {
                    continue;
                }
            }
            
            if (M.Count > 0)
            {
                return PartialView(M.AsEnumerable());
            }
            else
            {
                return PartialView(AllLoanM.AsEnumerable());
            }
        }

        [DisplayName("ویرایش وام")]
        public async Task<IActionResult> Edit(int id)
        {
            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.OfficeUnit),
                where: x => x.UserIdentity_Id == user.Id
                );
            if (user != null && employee.FirstOrDefault() != null)
            {
                ViewBag.FullName = user.FullName;
                ViewBag.PersonalCode = employee.FirstOrDefault().PersonalCode;
            }

            var loan = await _LoanRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.Employee_lo_1).ThenInclude(s => s.ApplicationUser),
                where: x => x.loanID == id);

            SupervionLevel = loan.FirstOrDefault().Employee_lo_1.ApplicationUser.Employee.SupervisionLevel.Value;
            byte thisKarmandLevel = Convert.ToByte(loan.LastOrDefault().Employee_lo_1.ApplicationUser.Employee.SupervisionLevel);
            ViewBag.SupervionLevel = SupervionLevel;
            //برای اینکه فقط پدر هر نقش بتونه فرزندانش را ویرایش کند (ویژه آقای صدیق)
            string thisKarmandUserId = loan.FirstOrDefault().Employee_lo_1.ApplicationUser.Id;
            var myRoles = await _userManager.GetRolesAsync(loan.FirstOrDefault().Employee_lo_1.ApplicationUser);
            myRoles.Remove("Employee");
            var myRole = await _roleManager.FindByNameAsync(myRoles.FirstOrDefault());
            var myParentRole = await _ApplicationRoleRepository.GetAllAsync(x => x.Id == myRole.ParentRoleId);
            IEnumerable<ApplicationUser> parentUser;
            string parentUserId = "";
            if (thisKarmandLevel == 1)
            {
                parentUser = await _userManager.GetUsersInRoleAsync(myParentRole.FirstOrDefault().Name);
            }
            else
            {
                parentUser = await _userManager.GetUsersInRoleAsync(myParentRole.FirstOrDefault().Name);
                parentUserId = parentUser.FirstOrDefault().Id;
                if (user.Id != parentUserId)
                {
                    var myGrandParentRole = await _ApplicationRoleRepository.GetAllAsync(x => x.Id == myParentRole.FirstOrDefault().ParentRoleId);
                    parentUser = await _userManager.GetUsersInRoleAsync(myGrandParentRole.FirstOrDefault().Name);
                }
            }
            
            parentUserId = parentUser.FirstOrDefault().Id;
            if (user.Id == parentUserId)                   //|| user.Id==grandParentUserId
            { ViewBag.CanEdit = true; }
            else { ViewBag.CanEdit = false; }


            return PartialView(loan.FirstOrDefault());
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Loan loan)
        {
            var ThisLoan = await _LoanRepository.GetByIdAsync(loan.loanID);
            ThisLoan.PersonalCode_manager = loan.PersonalCode_manager;
            ThisLoan.StatusManagerSignature = loan.StatusManagerSignature;
            ThisLoan.ManagerReason = loan.ManagerReason;
            //if (SupervionLevel == 2)
            //{
            //    ThisLoan.StatusSuperiorSignature = loan.StatusManagerSignature;
            //}
            ThisLoan.ManagerDateTime = DateTime.Now;
            _LoanRepository.Update(ThisLoan);
            await _LoanRepository.SaveChangesAsync();
            return Json(new { status = "ok" });
        }
    }
}