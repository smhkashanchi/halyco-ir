﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Employement;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("فرم استخدام")]
    public class EmployementsController : Controller
    {
        public IGenericRepository<Emp_BaseInformation> _baseInformationRepository;
        public IGenericRepository<Emp_ComputerSkillItems> _computerSkillItemsRepository;
        public IGenericRepository<Emp_ActivityItems> _activityItemsRepository;
        public IGenericRepository<Emp_EducationalRecords> _educationalRecordsRepository;
        public IGenericRepository<Emp_WorkExperience> _workExperienceRepository;
        public IGenericRepository<Emp_TraningCourse> _traningCourseRepository;
        public IGenericRepository<Emp_Guarantor> _guarantorRepository;
        public IGenericRepository<Emp_UnderSupervisor> _underSupervisorRepository;
        public IGenericRepository<Emp_ComputerSkills> _computerSkillsRepository;
        public IGenericRepository<Emp_Activity> _activityRepository;
        public IGenericRepository<Emp_Documents> _documentsRepository;
        public EmployementsController(IGenericRepository<Emp_Documents> documentsRepository, IGenericRepository<Emp_Activity> activityRepository, IGenericRepository<Emp_ComputerSkills> computerSkillsRepository, IGenericRepository<Emp_UnderSupervisor> underSupervisorRepository, IGenericRepository<Emp_Guarantor> guarantorRepository, IGenericRepository<Emp_TraningCourse> traningCourseRepository, IGenericRepository<Emp_WorkExperience> workExperienceRepository, IGenericRepository<Emp_EducationalRecords> educationalRecordsRepository, IGenericRepository<Emp_BaseInformation> baseInformationRepository, IGenericRepository<Emp_ComputerSkillItems> computerSkillItemsRepository, IGenericRepository<Emp_ActivityItems> activityItemsRepository)
        {
            _baseInformationRepository = baseInformationRepository;
            _computerSkillItemsRepository = computerSkillItemsRepository;
            _activityItemsRepository = activityItemsRepository;
            _educationalRecordsRepository = educationalRecordsRepository;
            _workExperienceRepository = workExperienceRepository;
            _guarantorRepository = guarantorRepository;
            _traningCourseRepository = traningCourseRepository;
            _underSupervisorRepository = underSupervisorRepository;
            _computerSkillsRepository = computerSkillsRepository;
            _activityRepository = activityRepository;
            _documentsRepository = documentsRepository;
        }
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> ViewAll()
        {
            return PartialView(await _baseInformationRepository.GetAllAsync());
        }
        public async Task<IActionResult> ViewDetails(int id)
        {
            ViewBag.SkillComputer = await _computerSkillsRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.Emp_ComputerSkillItems),
                where: x => x.Employement_Id == id);

            ViewBag.Activity = await _activityRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.Emp_ActivityItems),
                where: x => x.Employement_Id == id);

            return PartialView(await _baseInformationRepository.GetAsync(x => x.Employment_ID == id));
        }
        public async Task<IActionResult> ViewDetailsEducational(int id)
        {
            return PartialView(await _educationalRecordsRepository.GetAllAsync(x => x.Employment_Id == id));
        }
        public async Task<IActionResult> ViewDetailsWorkExperience(int id)
        {
            return PartialView(await _workExperienceRepository.GetAllAsync(x => x.Employment_Id == id));
        }
        public async Task<IActionResult> ViewDetailsTraningCourses(int id)
        {
            return PartialView(await _traningCourseRepository.GetAllAsync(x => x.Employement_Id == id));
        }
        public async Task<IActionResult> ViewDetailsGrauntor(int id)
        {
            return PartialView(await _guarantorRepository.GetAllAsync(x => x.Employement_Id == id));
        }
        public async Task<IActionResult> ViewDetailsUnderSuppervisor(int id)
        {
            return PartialView(await _underSupervisorRepository.GetAllAsync(x => x.Employement_Id == id));
        }
        public async Task<IActionResult> ViewDetailsDocuments(int id)
        {
            return PartialView(await _documentsRepository.GetAllAsync(x => x.Employement_Id == id));
        }
        public async Task<IActionResult> DeleteEmployement(int id)
        {
            var thisEmployement = await _baseInformationRepository.GetAsync(x => x.Employment_ID == id);
            if (thisEmployement != null)
            {
                _baseInformationRepository.Delete(thisEmployement);
                await _baseInformationRepository.SaveChangesAsync();
                return Json("ok");
            }
            return Json("nok");
        }
    }
}