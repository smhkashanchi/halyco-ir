﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.FAQ;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories;

namespace AspCorePanelProject.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]
    [DisplayName("سوالات متداول ")]

    public class FAQController : Controller
    {
        public IGenericRepository<FAQ> _fAQRepository;
        public FAQController(IGenericRepository<FAQ> fAQRepository)
        {
            _fAQRepository = fAQRepository;
        }
        [DisplayName("صفحه نخست ")]

        public IActionResult Index()
        {
            return View();
        }
        [DisplayName("لیست سوالات متداول ")]
        public async Task<IActionResult> ViewAll(string id)
        {
            return PartialView(await _fAQRepository.GetAllAsync(x => x.Lnag == id, x => x.OrderByDescending(s => s.FAQ_ID)));
        }
        [DisplayName("افزودن  ")]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(FAQ fAQ)
        {
            if (fAQ != null)
            {
                await _fAQRepository.AddAsync(fAQ);
                await _fAQRepository.SaveChangesAsync();
                return Json(fAQ.Lnag);
            }
            return View(fAQ);
        }
        [DisplayName("ویرایش")]
        public async Task<IActionResult> Edit(byte id)
        {
            if (id == 0)
            {
                return NotFound();
            }
            return View(await _fAQRepository.GetByIdAsync(id));
        }
        [HttpPost]
        public async Task<IActionResult> Edit(FAQ fAQ)
        {
            _fAQRepository.Update(fAQ);
            await _fAQRepository.SaveChangesAsync();
            return Json(fAQ.Lnag);
        }

        [DisplayName("حذف")]

        public async Task checkboxSelected(string values)
        {
            if (values != "")
            {
                string[] splitedValues = values.Split('&');
                for (int i = 0; i < splitedValues.Length - 1; i++)
                {

                    _fAQRepository.DeleteById(Byte.Parse(splitedValues[i]));
                    await _fAQRepository.SaveChangesAsync();

                }
            }
        }
    }
}