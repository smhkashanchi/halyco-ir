﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Language;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories;

namespace AspCorePanelProject.Areas.Admin.Controllers
{
    [Area("admin")]
       [Authorize]
    [DisplayName("زبان")]

    public class LanguageController : Controller
    {
        public IGenericRepository<Language> _langRepository;
        public LanguageController(IGenericRepository<Language> langRepository)
        {
            _langRepository = langRepository;
        }
        [DisplayName("صفحه نخست")]

        public IActionResult Index()
        {
            return View();
        }
        [DisplayName("لیست زبان ها")]

        public async Task<IActionResult> ViewAll()
        {
            return PartialView(await _langRepository.GetAllAsync());
        }

        public async Task<JsonResult> GetAllLangForLayoutPage()
        {
            return Json(await _langRepository.GetAllAsync(x=>x.IsActive,x=>x.OrderByDescending(s=>s.Lang_Name)));
        }
        [DisplayName("افزودن  ")]

        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Language language)
        {
         
            if (language != null)
            {
                if (!await _langRepository.Exists(x=>x.Lang_ID==language.Lang_ID))
                {
                   await _langRepository.AddAsync(language);
                   await  _langRepository.SaveChangesAsync();
                    return Json(language);
                }
                return Json("repeate");
            }
            return View(language);
        }
        [DisplayName("ویرایش  ")]


        public async Task<IActionResult> Edit(string id)
        {
            return View(await _langRepository.GetByIdAsync(id));
        }
        [HttpPost]
        public async Task<IActionResult> Edit(Language language)
        {
                    _langRepository.Update(language);
                    await _langRepository.SaveChangesAsync();
                    return Json(language);
        }

        [AllowAnonymous]
        public IActionResult ChangeLanguage(string culture)
        {
            Response.Cookies.Append(CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions() { Expires = DateTimeOffset.UtcNow.AddYears(1) });

            return Redirect(Request.Headers["Referer"].ToString());
        }
    }
}