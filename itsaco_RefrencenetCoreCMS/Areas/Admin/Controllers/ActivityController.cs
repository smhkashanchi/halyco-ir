﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DataLayer;
using DomainClasses.BasicInformation;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("Admin")]
    [DisplayName("فعالیت")]
    public class ActivityController : Controller
    {
        public IGenericRepository<Activity> _ActivityRepository;
        public IGenericRepository<ActivityField> _ActivityFieldRepository;
        

        public ActivityController(IGenericRepository<Activity> ActivityRepository, IGenericRepository<ActivityField> ActivityFieldRepository)
        {
            _ActivityRepository = ActivityRepository;
            _ActivityFieldRepository = ActivityFieldRepository;
        }

        [DisplayName("صفحه نخست ")]
        public async Task<IActionResult> Index()
        {
            var selectList = new SelectList(await _ActivityFieldRepository.GetAllAsync(x => x.IsActive == true), "ActivityFieldID", "ActivityFieldTitle");
            ViewBag.ActivtyFields = selectList;
            return View();
        }
        [DisplayName("لیست فعالیت ها ")]
        public async Task<IActionResult> ViewAll(string id)
        {
            if (id == "")
            {
                return View();
            }
            else
            { 
            return PartialView(await _ActivityRepository.GetAllAsync(x=>x.ActivityFieldID==Convert.ToInt16(id), x => x.OrderByDescending(s => s.Activity_ID)));
            }
        }
        [DisplayName("افزودن  ")]
        public async Task<IActionResult> Create()
        {

            var selectList = new SelectList(await _ActivityFieldRepository.GetAllAsync(x => x.IsActive == true), "ActivityFieldID", "ActivityFieldTitle");
            ViewBag.ActivtyFields = selectList;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Activity activity)
        {
            if (activity != null)
            {
                await _ActivityRepository.AddAsync(activity);
                await _ActivityRepository.SaveChangesAsync();
                return Json(activity);
            }
            return View(activity);
        }
        [DisplayName("ویرایش  ")]
        public async Task<IActionResult> Edit(short id)
        {

            var selectList = new SelectList(await _ActivityFieldRepository.GetAllAsync(x => x.IsActive == true), "ActivityFieldID", "ActivityFieldTitle");
            ViewBag.ActivtyFields = selectList;
            return View(await _ActivityRepository.GetByIdAsync(id));
        }
        [HttpPost]
        public async Task<IActionResult> Edit(Activity activity)
        {
            if (activity != null)
            {
                _ActivityRepository.Update(activity);
                await _ActivityRepository.SaveChangesAsync();
                return Json(activity);
            }
            return View(activity);
        }
        public async Task<IActionResult> Delete(short id)
        {
            var activity = await _ActivityRepository.GetByIdAsync(id);
            if (activity != null)
            {
                _ActivityRepository.Delete(activity);
                await _ActivityRepository.SaveChangesAsync();
                return Json(new { status = "ok", title = activity.ActivityTitle });
            }
            return Json("null");
        }
    }
}