﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Product;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Services.Repositories;

namespace AspCorePanelProject.Areas.Admin.Controllers
{
    [Area("admin")]
       [Authorize]
    [DisplayName("تخفیف")]

    public class OffController : Controller
    {
        public List<SelectListItem> offList;
        public IGenericRepository<Off> _offRepository;
        public OffController(IGenericRepository<Off> offRepository)
        {
            _offRepository = offRepository;
        }
        [DisplayName("صفحه نخست")]

        public IActionResult Index()
        {
            return View();
        }
        [DisplayName("لیست تخفیف ها")]

        public async Task<IActionResult> ViewAll(string id)
        {
            return PartialView(await _offRepository.GetAllAsync(x => x.Lang == id));
        }
        [DisplayName("افزودن  ")]

        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Off off, string sdate, string eDate)
        {
            if (off != null)
            {
                System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                DateTime dtStart = dtDateTime.AddSeconds(double.Parse(sdate)).ToLocalTime();
                DateTime dtEnd = dtDateTime.AddSeconds(double.Parse(eDate)).ToLocalTime();
                off.StartDate = dtStart;
                off.EndDate = dtEnd;
                if(off.StartDate>=off.EndDate)
                {
                    return Json(new { res = true });

                }
                await _offRepository.AddAsync(off);
                await _offRepository.SaveChangesAsync();
                return Json(off);
            }
            return View(off);
        }
        [DisplayName("ویرایش  ")]

        public async Task<IActionResult> Edit(Int16 id)
        {
            return View( await _offRepository.GetByIdAsync(id));
        }
        [HttpPost]
        public async Task<IActionResult> Edit(Off off, string sdate, string eDate)
        {
            if (off != null)
            {
                System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                DateTime dtStart = dtDateTime.AddSeconds(double.Parse(sdate)).ToLocalTime();
                DateTime dtEnd = dtDateTime.AddSeconds(double.Parse(eDate)).ToLocalTime();
                off.StartDate = dtStart;
                off.EndDate = dtEnd;
                if (off.StartDate >= off.EndDate)
                {
                    return Json(new { res = true });

                }
                _offRepository.Update(off);
                await _offRepository.SaveChangesAsync();
                return Json(off);
            }
            return View(off);
        }
        [DisplayName("حذف  ")]

        public async Task<JsonResult> DeleteOff(Int16 id)
        {
            var off = await _offRepository.GetByIdAsync(id);
            if (off != null)
            {
                _offRepository.Delete(off);
               await _offRepository.SaveChangesAsync();
                return Json(off);
            }
            return Json("Nok");
        }
        public async Task<JsonResult> ViewAllOffList(string id)
        {
            offList = new List<SelectListItem>();
            offList.Add(new SelectListItem
            {
                Text = "انتخاب کنید ...",
                Value = "0"
            });
            foreach (var item in await _offRepository.GetAllAsync(x=>x.Lang==id))
            {
                offList.Add(new SelectListItem
                {
                    Text = item.OffTitle,
                    Value = item.Off_ID.ToString()
                });
            }
            return Json(offList);
        }
    }
}