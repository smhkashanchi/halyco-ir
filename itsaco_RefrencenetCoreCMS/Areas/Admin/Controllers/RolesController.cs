﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DataLayer;
using DomainClasses.Role;
using Microsoft.AspNetCore.Identity;
using Services.Repositories;
using DomainClasses.User;
using ViewModels.Role;
using System.ComponentModel;
using Services.FilterAuthorization;
using Newtonsoft.Json;
using DomainClasses.FilterAuthorization;
using Microsoft.AspNetCore.Authorization;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Area("Admin")]
    [DisplayName("مدیریت نقش ها")]
    [Authorize]
    public class RolesController : Controller
    {
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IGenericRepository<ApplicationRole> _genericRepository;
        private readonly IGenericRepository<ApplicationUser> _user_genericRepository;
        private readonly IGenericRepository<ApplicationRole> _role_genericRepository;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMvcControllerDiscovery _mvcControllerDiscovery;

        public RolesController(IGenericRepository<ApplicationRole> genericRepository,
              RoleManager<ApplicationRole> roleManager,
              UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager,
              IGenericRepository<ApplicationUser> user_genericRepository,
              IGenericRepository<ApplicationRole> role_genericRepository,
              IMvcControllerDiscovery mvcControllerDiscovery)
        {
            _roleManager = roleManager;
            _genericRepository = genericRepository;
            _userManager = userManager;
            _signInManager = signInManager;
            _user_genericRepository = user_genericRepository;
            _role_genericRepository = role_genericRepository;
            _mvcControllerDiscovery = mvcControllerDiscovery;
        }

        //[BreadCrumb(Title = "نقش ها", Order = 1)]
        //[DisplayName("مشاهده لیست نقش ها")]
        [DisplayName("صفحه نخست")]
        public async Task<IActionResult> Index()
        {
            return View();
        }
        [DisplayName("لیست نقش ها")]

        public async Task<ActionResult> ViewAll(string id)
        {
            return PartialView(await _role_genericRepository.GetWithIncludeAsync(
                selector:x=>x,
                include:x=>x.Include(s=>s.ParentRole)
                ));
        }
        //[DisplayName("جزئیات")]

        // GET: Roles/Details/5
        //public async Task<IActionResult> Details(string id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var applicationRole = await _genericRepository.GetByIdAsync(id);

        //    MainRoleViewModel vmRole = new MainRoleViewModel()
        //    {
        //        Title = applicationRole.Name,
        //        Role_Status = applicationRole.Role_Status,
        //        Description = applicationRole.Description,
        //        Responsibilty = applicationRole.Responsibilty,
        //        Task = applicationRole.Task,
        //        Role_Type = applicationRole.Role_Type
        //    };

        //    if (applicationRole == null)
        //    {
        //        return NotFound();
        //    }

        //    return PartialView(vmRole);
        //}
        //[DisplayName("افزودن")]

        // GET: Roles/Create
        [DisplayName("افزودن")]

        public async Task<IActionResult> Create()
        {
            var allRoles =new SelectList( await _role_genericRepository.GetAllAsync(), "Id", "Name");
            ViewBag.allRoles = allRoles;
            return PartialView();
        }

        // POST: Roles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(MainRoleViewModel applicationRole)
        {

            if (ModelState.IsValid)
            {
                if (await _genericRepository.Exists(f => f.Name == applicationRole.Title))
                {
                    return Json(new { message = "عنوان قبلا برای نقش دیگری ثبت شده.", return_Status = false });

                }
                ApplicationRole Role = new ApplicationRole()
                {
                    Name = applicationRole.Title,
                    Role_Status = 1,
                    Description = applicationRole.Description,
                    Responsibilty = applicationRole.Responsibilty,
                    Task = applicationRole.Task,
                    Role_Type = 1,
                    ParentRoleId = applicationRole.ParentRole,
                    SendSmsToParent = applicationRole.SendSmsToParent,
                    SendFinalSmsToUser = applicationRole.SendFinalSmsToUser,
                    IsAgentOfManager=applicationRole.IsAgentOfManager,
                    Access =
            "[{'Id':'admin:Home','Name':'Home','DisplayName':null,'AreaName':'admin','Actions':[{'Id':'admin:Home:Index','Name':'Index','DisplayName':null,'ControllerId':'admin:Home'}]},{'Id':'Admin:Account','Name':'Account','DisplayName':null,'AreaName':'Admin','Actions':[{'Id':'Admin:Account:Logout','Name':'Logout','DisplayName':null,'ControllerId':'Admin:Account'}]},{'Id':'admin:Manage','Name':'Manage','DisplayName':null,'AreaName':'admin','Actions':[{'Id':'admin:Manage:Index','Name':'Index','DisplayName':null,'ControllerId':'admin:Manage'},{'Id':'admin:Manage:SendVerificationEmail','Name':'SendVerificationEmail','DisplayName':null,'ControllerId':'admin:Manage'},{'Id':'admin:Manage:ChangePassword','Name':'ChangePassword','DisplayName':null,'ControllerId':'admin:Manage'},{'Id':'admin:Manage:ChangeUserName','Name':'ChangeUserName','DisplayName':null,'ControllerId':'admin:Manage'},{'Id':'admin:Manage:SetPassword','Name':'SetPassword','DisplayName':null,'ControllerId':'admin:Manage'},{'Id':'admin:Manage:ExternalLogins','Name':'ExternalLogins','DisplayName':null,'ControllerId':'admin:Manage'},{'Id':'admin:Manage:LinkLogin','Name':'LinkLogin','DisplayName':null,'ControllerId':'admin:Manage'},{'Id':'admin:Manage:LinkLoginCallback','Name':'LinkLoginCallback','DisplayName':null,'ControllerId':'admin:Manage'},{'Id':'admin:Manage:RemoveLogin','Name':'RemoveLogin','DisplayName':null,'ControllerId':'admin:Manage'},{'Id':'admin:Manage:TwoFactorAuthentication','Name':'TwoFactorAuthentication','DisplayName':null,'ControllerId':'admin:Manage'},{'Id':'admin:Manage:Disable2faWarning','Name':'Disable2faWarning','DisplayName':null,'ControllerId':'admin:Manage'},{'Id':'admin:Manage:Disable2fa','Name':'Disable2fa','DisplayName':null,'ControllerId':'admin:Manage'},{'Id':'admin:Manage:EnableAuthenticator','Name':'EnableAuthenticator','DisplayName':null,'ControllerId':'admin:Manage'},{'Id':'admin:Manage:ResetAuthenticatorWarning','Name':'ResetAuthenticatorWarning','DisplayName':null,'ControllerId':'admin:Manage'},{'Id':'admin:Manage:ResetAuthenticator','Name':'ResetAuthenticator','DisplayName':null,'ControllerId':'admin:Manage'},{'Id':'admin:Manage:GenerateRecoveryCodes','Name':'GenerateRecoveryCodes','DisplayName':null,'ControllerId':'admin:Manage'},{'Id':'admin:Manage:UploadUserPhoto','Name':'UploadUserPhoto','DisplayName':null,'ControllerId':'admin:Manage'},{'Id':'admin:Manage:RemoveUserPhoto','Name':'RemoveUserPhoto','DisplayName':null,'ControllerId':'admin:Manage'},{'Id':'admin:Manage:DisplayUserPhoto','Name':'DisplayUserPhoto','DisplayName':null,'ControllerId':'admin:Manage'}]}]"
                };
                var role = await _roleManager.CreateAsync(Role);
                return Json(role);


            }

            return Json(applicationRole);
        }

        //[DisplayName("ویرایش")]

        // GET: Roles/Edit/5
        [DisplayName("ویرایش")]

        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var allRoles = new SelectList(await _role_genericRepository.GetAllAsync(), "Id", "Name");
            ViewBag.allRoles = allRoles;

            var applicationRole = await _roleManager.FindByIdAsync(id);

            if (applicationRole == null)
            {
                return NotFound();
            }
            MainRoleViewModel vmRole = new MainRoleViewModel()
            {
                Id = applicationRole.Id,
                Title = applicationRole.Name,
                Role_Status = applicationRole.Role_Status,
                Description = applicationRole.Description,
                Responsibilty = applicationRole.Responsibilty,
                Task = applicationRole.Task,
                Role_Type = applicationRole.Role_Type,
                ParentRole = applicationRole.ParentRoleId,
                Access = applicationRole.Access,
                 SendFinalSmsToUser = applicationRole.SendFinalSmsToUser,
                SendSmsToParent = applicationRole.SendSmsToParent,
                IsAgentOfManager=applicationRole.IsAgentOfManager
            };


            if (applicationRole == null)
            {
                return NotFound();
            }
            return PartialView(vmRole);
        }

        // POST: Roles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, MainRoleViewModel vmRole)
        {
            if (id != vmRole.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (await _genericRepository.Exists(f => f.Name == vmRole.Title && f.Id != vmRole.Id) == true)
                {
                    return Json(new { message = "این نقش قبلا با این نام ثبت شده", res = false });
                }
                var role = await _roleManager.FindByIdAsync(vmRole.Id);

                role.Name = vmRole.Title;
                role.Task = vmRole.Task;
                role.Responsibilty = vmRole.Responsibilty;
                role.Role_Status = 1;
                role.Access = vmRole.Access;
                role.Role_Type = 1;
                role.ParentRoleId = vmRole.ParentRole;
                role.SendFinalSmsToUser = vmRole.SendFinalSmsToUser;
                role.SendSmsToParent = vmRole.SendSmsToParent;
                role.IsAgentOfManager = vmRole.IsAgentOfManager;
                try
                {
                   await _roleManager.UpdateAsync(role);
                    //_genericRepository.Update(role);
                   // await _genericRepository.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await ApplicationRoleExists(vmRole.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return Json(new  {res=true,model= role });
            }
            return Json(vmRole);
        }
        [DisplayName("حذف")]

        //[DisplayName("حذف")]

        // GET: Roles/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationRole = await _genericRepository.GetByIdAsync(id);
            var list = await _userManager.GetUsersInRoleAsync(applicationRole.Name);
            if (list.Count > 0)
            {
                return Json(new { return_status = false, message = "این نقش برای " + list.Count + " کاربر ثبت شده" });

            }
            if (applicationRole == null)
            {
                return NotFound();
            }
            _genericRepository.Delete(applicationRole);
            await _genericRepository.SaveChangesAsync();
            return Json(new { return_status = true, Message = applicationRole.Id.ToString() });

        }

        // POST: Roles/Delete/5
        [HttpPost, ActionName("Delete")]
        
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationRole = await _roleManager.FindByIdAsync(id);
            var list = await _userManager.GetUsersInRoleAsync(applicationRole.Name);
            if (list.Count > 0)
            {
                return Json(new { res = false, message = "این نقش برای " + list.Count + " کاربر ثبت شده" });

            }
            if (applicationRole == null)
            {
                return NotFound();
            }
            _genericRepository.Delete(applicationRole);
            await _genericRepository.SaveChangesAsync();
            return Json(new { res = true, Message = applicationRole.Name });
        }
        [NonAction]
        private async Task<bool> ApplicationRoleExists(string id)
        {
            return await _genericRepository.Exists(x => x.Id == id);
        }
        [DisplayName("افزودن نقش به کاربران")]

        public async Task<ActionResult> AddRole(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
                return NotFound();
            var userRoles = await _userManager.GetRolesAsync(user);
            var userViewModel = new UserRolesViewModel
            {
                UserId = user.Id,
                UserName = user.UserName
                ,
                Roles = userRoles


            };
            var roles = await _roleManager.Roles.Where(x => x.Name != "Customer").ToListAsync();
            ViewData["Roles"] = roles;
            ViewData["IsCustomer"] = false;

            return PartialView(userViewModel);
        }
        // POST: Access/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddRole(UserRolesViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                ViewData["Roles"] = await _roleManager.Roles.ToListAsync();
                return View(viewModel);
            }

            var user = await _userManager.FindByIdAsync(viewModel.UserId);
            if (user == null)
            {
                ModelState.AddModelError("", "User not found");
                ViewData["Roles"] = await _roleManager.Roles.ToListAsync();
                return View();
            }

            var userRoles = await _userManager.GetRolesAsync(user);
            await _userManager.RemoveFromRolesAsync(user, userRoles);
            var userRoles2 = await _userManager.GetRolesAsync(user);
            await _userManager.AddToRolesAsync(user, viewModel.Roles);
            var userRoles3 = await _userManager.GetRolesAsync(user);
            var x ="";
            return Json(new { res = true });

        }

        [HttpGet]
        [DisplayName("مشاهده دسترسی ها ")]
        public async Task<ActionResult> Permission(string id)
        {

            ViewData["Controllers"] = _mvcControllerDiscovery.GetControllers();

            var role = await _roleManager.FindByIdAsync(id);
            if (role == null)
                return NotFound();
            PermissionViewModel permissionViewModel = new PermissionViewModel();
            if (role.Access != null)
            {
                permissionViewModel.Name = role.Name;
                permissionViewModel.SelectedControllers = JsonConvert.DeserializeObject<IEnumerable<MvcControllerInfo>>(role.Access);

            }
            else
            {
                permissionViewModel.Name = role.Name;
            }


            ViewData["Role_Name"] = role.Name;
            ViewData["Role_Id"] = role.Id;
            ViewData["Controllers"] = _mvcControllerDiscovery.GetControllers();

            return PartialView(permissionViewModel);


        }

        // POST: Role/Create
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Permission(PermissionViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                ViewData["Controllers"] = _mvcControllerDiscovery.GetControllers();
                return View(viewModel);
            }

            var role = await _genericRepository.GetByIdAsync(viewModel.Name);

            if (viewModel.SelectedControllers != null && viewModel.SelectedControllers.Any())
            {
                foreach (var controller in viewModel.SelectedControllers)
                    foreach (var action in controller.Actions)
                        action.ControllerId = controller.Id;

                var accessJson = JsonConvert.SerializeObject(viewModel.SelectedControllers);
                role.Access = accessJson;
            }
            else
            {
                role.Access = null;
            }
            _genericRepository.Update(role);
            await _genericRepository.SaveChangesAsync();
            //if (result.Succeeded)
            //    return RedirectToAction(nameof(Index));

            //foreach (var error in result.Errors)
            //    ModelState.AddModelError("", error.Description);

            //ViewData["Controllers"] = _mvcControllerDiscovery.GetControllers();

            return Json(new { return_status = true });


        }
    }
}
