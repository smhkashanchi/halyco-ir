﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Employement;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("فعالیت های فرد استخدام")]
    public class ActivityOrgItemsController : Controller
    {
        public IGenericRepository<Emp_ActivityItems> _activityItemsRepository;
        public ActivityOrgItemsController(IGenericRepository<Emp_ActivityItems> activityItemsRepository)
        {
            _activityItemsRepository = activityItemsRepository;
        }
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> ViewAll()
        {
            return PartialView(await _activityItemsRepository.GetAllAsync());
        }
       
        public IActionResult Create()
        {
            return PartialView();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Emp_ActivityItems emp_ActivityItems)
        {
            await _activityItemsRepository.AddAsync(emp_ActivityItems);
            await _activityItemsRepository.SaveChangesAsync();
            return Json("ok");
        }
        public async Task<IActionResult> Edit(int id)
        {
            return PartialView(await _activityItemsRepository.GetAsync(x => x.Item_ID == id));
        }
        [HttpPost]
        public async Task<IActionResult> Edit(Emp_ActivityItems emp_ActivityItems)
        {
            _activityItemsRepository.Update(emp_ActivityItems);
            await _activityItemsRepository.SaveChangesAsync();
            return Json("ok");
        }
        public async Task<IActionResult> Delete(int id)
        {
            var thisSkill = await _activityItemsRepository.GetAsync(x => x.Item_ID == id);
            if (thisSkill != null)
            {
                _activityItemsRepository.Delete(thisSkill);
                await _activityItemsRepository.SaveChangesAsync();
                return Json("ok");
            }
            return Json("invalid");
        }
    }
}