﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.BasicInformation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Services.Repositories;
using ViewModels;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]
    [DisplayName("سمت")]
    public class OfficePostController : Controller
    {
        public IGenericRepository<OfficePost> _officePost;
        public OfficePostController(IGenericRepository<OfficePost> officePost)
        {
            _officePost = officePost;
        }
        [DisplayName("صفحه نخست ")]
        public IActionResult Index()
        {
            return View();
        }
        [DisplayName("لیست سمت های اداری ")]
        public async Task<IActionResult> ViewAll(string id)
        {
            return PartialView(await _officePost.GetAllAsync(null, x => x.OrderByDescending(s => s.OfficePostID)));
        }
        public async Task<string> createTreeView(short pgId, string lang)
        {
            return JsonConvert.SerializeObject(await ViewInTreeView(pgId, lang));
        }
        [NonAction]

        public async Task<List<Node>> ViewInTreeView(short PGId, string lang)
        {
            List<Node> TNodes = new List<Node>();
            List<Node> TNodes2 = new List<Node>();
            // var groups;

            var GroupData = await _officePost.GetByIdAsync(PGId);

            var groups = await _officePost.GetAllAsync(x => x.ParentId == PGId, x => x.OrderByDescending(s => s.OfficePostTitle));
            foreach (var item in groups)
            {
                var node = await ViewInTreeView(item.OfficePostID, lang);
                TNodes2.Add(node.FirstOrDefault());
            }


            TNodes.Add(new Node()
            {
                text = " " + GroupData.OfficePostTitle,
                href = "#" + GroupData.OfficePostTitle,
                tags = PGId.ToString(),
                nodes = TNodes2
            });

            return TNodes;
        }

        [DisplayName("جزئیات سمت")]
        public async Task<IActionResult> ViewOfficePostData(Int16 id)
        {
            return PartialView(await _officePost.GetAsync(x => x.OfficePostID == id));
        }
        [DisplayName("افزودن  ")]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(OfficePost officePost)
        {
            if (officePost != null)
            {
                await _officePost.AddAsync(officePost);
                await _officePost.SaveChangesAsync();
                return Json(new { status="ok", title = officePost.OfficePostTitle, parentId=officePost.OfficePostID});
            }
            return View(officePost);
        }
        [DisplayName("ویرایش  ")]
        public async Task<IActionResult> Edit(short id)
        {
            return View(await _officePost.GetByIdAsync(id));
        }
        [HttpPost]
        public async Task<IActionResult> Edit(OfficePost officePost)
        {
            if (officePost != null)
            {
                 _officePost.Update(officePost);
                await _officePost.SaveChangesAsync();
                return Json(new { status = "ok",title = officePost.OfficePostTitle, parentId = officePost.OfficePostID });
            }
            return View(officePost);
        }
        public async Task<IActionResult> Delete(short id)
        {
            var officePost = await _officePost.GetByIdAsync(id);
            if (officePost != null)
            {
                _officePost.Delete(officePost);
                await _officePost.SaveChangesAsync();
                return Json(new { status = "ok", title = officePost.OfficePostTitle, parentId = officePost.OfficePostID });
            }
            return Json("null");
        }
    }
}