﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Survey;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("سوالات نظرسنجی")]
    public class SurveyQuestionController : Controller
    {
        public IGenericRepository<SurvayQuestionGroup> _survayQuestionGroupRepository;
        public IGenericRepository<SurveyQuestionReply> _surveyQuestionReplyRepository;
        public IGenericRepository<SurveyQuestion> _surveyQuestionRepository;
        public IGenericRepository<SurveyAnswerType> _surveyAnswerTypeRepository;
        public SurveyQuestionController(IGenericRepository<SurveyQuestionReply> surveyQuestionReplyRepository,IGenericRepository<SurveyAnswerType> surveyAnswerTypeRepository,IGenericRepository<SurveyQuestion> surveyQuestionRepository, IGenericRepository<SurvayQuestionGroup> survayQuestionGroupRepository, IGenericRepository<MainSurvey> mainSurveyRepository)
        {
            _surveyQuestionReplyRepository = surveyQuestionReplyRepository;
            _survayQuestionGroupRepository = survayQuestionGroupRepository;
            _surveyQuestionRepository = surveyQuestionRepository;
            _surveyAnswerTypeRepository = surveyAnswerTypeRepository;
        }
       
        public IActionResult Index()
        {
            return View();
        }

        [DisplayName("لیست گروه سوالات نظرسنجی")]
        public async Task<JsonResult> GetAllSurveyQuestionGroup(int id)
        {
            return Json(await _survayQuestionGroupRepository.GetAllAsync(x=>x.MainSurveyId==id));
        }
        [DisplayName("لیست سوالات نظرسنجی")]
        public async Task<IActionResult> ViewAll(int id)
        {
            var questions = await _surveyQuestionRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.SurveyAnswerType),
                where: x => x.QuestionGroupId == id
                );
            return PartialView(questions);
        }
        [DisplayName("افزودن سوال نظرسنجی")]
        public async Task<IActionResult> Create(int id)
        {
            if (id == 0)
            {
                ViewBag.QuestionGroup = new SelectList(await _survayQuestionGroupRepository.GetAllAsync(), "SurveyGroup_ID", "Title");
            }
            else
            {
                ViewBag.QuestionGroup = new SelectList(await _survayQuestionGroupRepository.GetAllAsync(x => x.MainSurveyId == id), "SurveyGroup_ID", "Title");
            }
            ViewBag.QuestionAnswerType = new SelectList(await _surveyAnswerTypeRepository.GetAllAsync(), "AnswerType_ID", "Title");
            return PartialView();
        }
        [HttpPost]
        public async Task<IActionResult> Create(SurveyQuestion surveyQuestion)
        {
            surveyQuestion.ReplyCount = 0;
            surveyQuestion.OptionVoteCount1 = 0;
            surveyQuestion.OptionVoteCount2 = 0;
            surveyQuestion.OptionVoteCount3 = 0;
            surveyQuestion.OptionVoteCount4 = 0;
            surveyQuestion.OptionVoteCount5 = 0;
            await _surveyQuestionRepository.AddAsync(surveyQuestion);
            await _surveyQuestionRepository.SaveChangesAsync();
            return Json(new { status= "ok" ,groupId=surveyQuestion.QuestionGroupId});
        }

        [DisplayName("افزودن سوال نظرسنجی")]
        public async Task<IActionResult> Edit(int id)
        {
            var surveyQuestion = await _surveyQuestionRepository.GetAsync(x => x.Question_ID == id);
            ViewBag.QuestionGroup = new SelectList(await _survayQuestionGroupRepository.GetAllAsync(), "SurveyGroup_ID", "Title", surveyQuestion.QuestionGroupId);
            ViewBag.QuestionAnswerType = new SelectList(await _surveyAnswerTypeRepository.GetAllAsync(), "AnswerType_ID", "Title",surveyQuestion.AnswerTypeId);
            return PartialView(surveyQuestion);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(SurveyQuestion surveyQuestion)
        {
            _surveyQuestionRepository.Update(surveyQuestion);
            await _surveyQuestionRepository.SaveChangesAsync();
            return Json(new { status = "ok", groupId = surveyQuestion.QuestionGroupId });
        }
        [DisplayName("حذف سوال نظرسنجی")]
        public async Task<IActionResult> Delete(int id)
        {
            //if (!await _surveyQuestionReplyRepository.Exists(x => x.QuestionId == id))
            //{
                var question = await _surveyQuestionRepository.GetAsync(x => x.Question_ID == id);
                if (question != null)
                {
                    _surveyQuestionRepository.Delete(question);
                    await _surveyQuestionRepository.SaveChangesAsync();
                    return Json("ok");
                }
                return Json("nall");
            //}
            //return Json("ex_ques_reply");
        }
    }
}