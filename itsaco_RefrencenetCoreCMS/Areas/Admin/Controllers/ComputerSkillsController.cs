﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Employement;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("مهارت های کامپیوتری")]
    public class ComputerSkillsController : Controller
    {
        public IGenericRepository<Emp_ComputerSkillItems> _computerSkillItemsRepository;
        public ComputerSkillsController(IGenericRepository<Emp_ComputerSkillItems> computerSkillItemsRepository)
        {
            _computerSkillItemsRepository = computerSkillItemsRepository;
        }
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> ViewAll()
        {
            return PartialView(await _computerSkillItemsRepository.GetAllAsync());
        }
        public IActionResult Create()
        {
            return PartialView();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Emp_ComputerSkillItems emp_ComputerSkillItems)
        {
            await _computerSkillItemsRepository.AddAsync(emp_ComputerSkillItems);
            await _computerSkillItemsRepository.SaveChangesAsync();
            return Json("ok");
        }
        public async Task<IActionResult> Edit(int id)
        {
            return PartialView(await _computerSkillItemsRepository.GetAsync(x=>x.Item_ID==id));
        }
        [HttpPost]
        public async Task<IActionResult> Edit(Emp_ComputerSkillItems emp_ComputerSkillItems)
        {
             _computerSkillItemsRepository.Update(emp_ComputerSkillItems);
            await _computerSkillItemsRepository.SaveChangesAsync();
            return Json("ok");
        }
        public async Task<IActionResult> Delete(int id)
        {
            var thisSkill = await _computerSkillItemsRepository.GetAsync(x => x.Item_ID == id);
            if (thisSkill != null)
            {
                _computerSkillItemsRepository.Delete(thisSkill);
                await _computerSkillItemsRepository.SaveChangesAsync();
                return Json("ok");
            }
            return Json("invalid");
        }
    }
}