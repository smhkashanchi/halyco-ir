﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DataLayer;
using DomainClasses.Manager;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel;
using Services.Repositories;
using Microsoft.AspNetCore.Http;
using System.IO;
using Utilities;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]
    [DisplayName("اعضای هیئت مدیره و مدیران اجرایی")]
    public class HalycoManagersController : Controller
    {
        public IGenericRepository<HalycoManager> _HalycoManagerRepository;

        public HalycoManagersController(IGenericRepository<HalycoManager> HalycoManagerRepository)
        {
            _HalycoManagerRepository = HalycoManagerRepository;
        }

        [DisplayName("صفحه نخست")]

        public IActionResult Index()
        {
            return View();
        }

        [DisplayName("لسیت اعضای هیئت مدیره و مدیران اجرایی")]

        public async Task<IActionResult> ViewAll(bool id=true)
        {
            return PartialView(await _HalycoManagerRepository.GetAllAsync(x=>x.ManagerType==id, x => x.OrderByDescending(s => s.HManager_ID)));
        }

        [DisplayName("افزودن")]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(HalycoManager halycoManager, IFormFile ManagerPhoto)
        {
            if (!Directory.Exists("wwwroot/Files/Manager"))
            {
                Directory.CreateDirectory("wwwroot/Files/Manager");
            }
            if (!Directory.Exists("wwwroot/Files/Manager/Temp"))
            {
                Directory.CreateDirectory("wwwroot/Files/Manager/Temp");
            }
            if (halycoManager != null)
            {
                if (ManagerPhoto != null)
                {
                    halycoManager.ManagerPhoto = ManagerPhoto.FileName.Substring(0, ManagerPhoto.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(ManagerPhoto.FileName);
                    string TempPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Manager/Temp", halycoManager.ManagerPhoto);
                    string photoPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Manager", halycoManager.ManagerPhoto);

                    using (var stream = new FileStream(TempPath, FileMode.Create))
                    {
                        await ManagerPhoto.CopyToAsync(stream);
                    }
                    ImageManipulate imageManipulate = new ImageManipulate();
                    imageManipulate.resizeImage2(TempPath, photoPath, 90, 90);
                    System.IO.File.Delete(TempPath);
                }
                await _HalycoManagerRepository.AddAsync(halycoManager);
                await _HalycoManagerRepository.SaveChangesAsync();
                return Json(halycoManager);
            }
            return Json("nalll");
        }
        [DisplayName("ویرایش")]

        public async Task<IActionResult> Edit(Int16 id)
        {
            return View(await _HalycoManagerRepository.GetByIdAsync(id));
        }
        [HttpPost]
        public async Task<IActionResult> Edit(HalycoManager halycoManager, IFormFile photo)
        {
            if (halycoManager != null)
            {
                if (photo != null)
                {
                    if (halycoManager.ManagerPhoto != null)
                    {
                        string photoPathOld = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Manager", halycoManager.ManagerPhoto);
                        await DeleteFilesInProj(photoPathOld);
                    }
                    halycoManager.ManagerPhoto = photo.FileName.Substring(0, photo.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(photo.FileName);
                    string TempPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Manager/Temp", halycoManager.ManagerPhoto);
                    string photoPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Manager", halycoManager.ManagerPhoto);

                    using (var stream = new FileStream(TempPath, FileMode.Create))
                    {
                        await photo.CopyToAsync(stream);
                    }
                    ImageManipulate imageManipulate = new ImageManipulate();
                    imageManipulate.resizeImage2(TempPath, photoPath, 90, 90);
                    System.IO.File.Delete(TempPath);
                }
                _HalycoManagerRepository.Update(halycoManager);
                await _HalycoManagerRepository.SaveChangesAsync();
                return Json(halycoManager);
            }
            return Json("nalll");
        }
        [DisplayName("حذف")]

        public async Task<IActionResult> Delete(Int16 id)
        {
            var halycoManager = await _HalycoManagerRepository.GetByIdAsync(id);
            if (halycoManager != null)
            {
                string photoPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Manager", halycoManager.ManagerPhoto);
                await DeleteFilesInProj(photoPath);

                _HalycoManagerRepository.Delete(halycoManager);
                await _HalycoManagerRepository.SaveChangesAsync();
                return Json(halycoManager);
            }
            return Json("nall");
        }
        [NonAction]
        public async Task<IActionResult> DeleteFilesInProj(string path)
        {
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }

            return Json(Ok());
        }
    }
}