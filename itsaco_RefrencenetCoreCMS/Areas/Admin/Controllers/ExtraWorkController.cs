﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Employees;
using DomainClasses.Role;
using DomainClasses.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services;
using Services.Repositories;
using System.Linq.Dynamic.Core;
using Utilities;
using ViewModels;
using static Utilities.FunctionResult;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("اضافه کاری")]
    public class ExtraWorkController : Controller
    {
        public UserManager<ApplicationUser> _userManager;
        public RoleManager<ApplicationRole> _roleManager;
        public IGenericRepository<Employee> _empgenericRepository;
        public IGenericRepository<ApplicationRole> _ApplicationRoleRepository;
        public IGenericRepository<ApplicationUser> _userRepository;
        public IGenericRepository<ExtraWork> _ExtraWorkRepository;
        public IGenericRepository<ExtraworkSignature> _ExtraWorkSignatureRepository;
        public static byte SupervionLevel;
        public ISMSSender _smsSender;
        public string SendFinalSmsToUser_UserId = ""; //شناسه کاربری که تاییدیه نهایی مرخصی را می دهد

        public ExtraWorkController(UserManager<ApplicationUser> userManager, IGenericRepository<Employee> empgenericRepository, IGenericRepository<ApplicationRole> ApplicationRoleRepository, RoleManager<ApplicationRole> roleManager, IGenericRepository<ApplicationUser> userRepository, IGenericRepository<ExtraWork> ExtraWorkRepository, IGenericRepository<ExtraworkSignature> ExtraWorkSignatureRepository, ISMSSender smsSender)
        {
            _userManager = userManager;
            _empgenericRepository = empgenericRepository;
            _ApplicationRoleRepository = ApplicationRoleRepository;
            _roleManager = roleManager;
            _userRepository = userRepository;
            _ExtraWorkRepository = ExtraWorkRepository;
            _ExtraWorkSignatureRepository = ExtraWorkSignatureRepository;
            _smsSender = smsSender;
        }

        [DisplayName("صفحه نخست")]
        public async Task<IActionResult> Index()
        {

            return View();
        }

        [DisplayName("ثبت اضافه کاری")]
        public async Task<IActionResult> Create()
        {
            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.OfficeUnit),
                where: x => x.UserIdentity_Id == user.Id
                );
            if (user != null && employee.FirstOrDefault() != null)
            {
                ViewBag.FullName = user.FullName;
                ViewBag.PersonalCode = employee.FirstOrDefault().PersonalCode;
                SupervionLevel = employee.FirstOrDefault().SupervisionLevel.Value;
            }
            return PartialView();
        }


        [HttpPost]
        public async Task<simpleFunctionResult> Create(ExtraWork ExtraWork, string sdate, string edate)
        {
            try
            {

                //بدست آورن رکورد یوزر من
                var user = await _userManager.GetUserAsync(User);
                //بدست آوردن لیست  نام نقش های من
                var myRole = await _userManager.GetRolesAsync(user);
                //حذف نقش کارمند از لیست نقش های من
                myRole.Remove("Employee");
                //بدست آوردن نقش من
                var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());
                var FinalUserRole = await _ApplicationRoleRepository.GetAsync(x => x.Id == mr.SendFinalSmsToUser);
                var FinalUsers = await _userManager.GetUsersInRoleAsync(FinalUserRole.Name);
                var FinalUser = FinalUsers.FirstOrDefault();
                SendFinalSmsToUser_UserId = FinalUser.Id;
                //بدست آوردن نقش  پدر من
                var parentRole = await _ApplicationRoleRepository.GetAsync(x => x.Id == mr.ParentRoleId);
                //بدست آوردن لیست یوزر پدرهای من
                var parentUsers = await _userManager.GetUsersInRoleAsync(parentRole.Name);
                //بدست آوردن شماره موبایل اولین(تنهاترین) پدر من
                string userMobile = parentUsers.FirstOrDefault().Mobile_1;
                //بدست آوردن نام من
                string userName = user.FullName;

                System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                DateTime dtStart = dtDateTime.AddSeconds(double.Parse(sdate)).ToLocalTime();
                DateTime dtEnd = dtDateTime.AddSeconds(double.Parse(edate)).ToLocalTime();
                ExtraWork.StartDate = dtStart.ToString("MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                ExtraWork.EndDate = dtEnd.ToString("MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                ExtraWork.CreateDate = DateTime.Now;
                //if (SupervionLevel == 2)
                //{
                //    ExtraWork.StatusSuperiorSignature = true;
                //}
                await _ExtraWorkRepository.AddAsync(ExtraWork);
                await _ExtraWorkRepository.SaveChangesAsync();

                simpleFunctionResult result = await createVacationSigniture(ExtraWork.ExtraWorkID, user.Id);
                if (!result.Status)
                {
                    return new simpleFunctionResult { Status = result.Status, message = result.message };
                }


                //var ExtraWork2 = await checkSpecialState(ExtraWork);
                //_ExtraWorkRepository.Update(ExtraWork2);
                //await _ExtraWorkRepository.SaveChangesAsync();

                await _smsSender.SendSubmitExtraWorkMessageSMSAsync(userMobile, userName, ExtraWork.StartDate.ToShortPersian());
                return new simpleFunctionResult { Status = true, message = "ok" };
            }
            catch (Exception err)
            {
                return new simpleFunctionResult { Status = false, message = err.Message + " *** " + err.InnerException };
            }
        }

        /// <summary>
        /// متد ایجاد رکوردهای امضا اضافه کاری
        /// </summary>
        /// <param name="Vacation_ID">شناسه اضافه کاری</param>
        /// <param name="PersonalCodeSignaturer">شناسه درخواست کننده اضافه کاری</param>
        /// <returns></returns>
        [NonAction]
        public async Task<simpleFunctionResult> createVacationSigniture(int Extrawork_ID, string PersonalCodeSignaturer)
        {
            try
            {
                //بدست آورن رکورد یوزر من
                var user = await _userRepository.GetByIdAsync(PersonalCodeSignaturer);
                //بدست آوردن لیست  نام نقش های من
                var myRole = await _userManager.GetRolesAsync(user);
                //حذف نقش کارمند از لیست نقش های من
                myRole.Remove("Employee");
                //بدست آوردن نقش من
                var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());
                if (mr.ParentRoleId != null)
                {
                    //بدست آوردن نقش  پدر من
                    var parentRole = await _ApplicationRoleRepository.GetAsync(x => x.Id == mr.ParentRoleId);
                    //بدست آوردن لیست یوزر پدرهای من
                    var parentUsers = await _userManager.GetUsersInRoleAsync(parentRole.Name);
                    //ساخت رکورد امضا مرخصی برای نقش پدر یوزر جاری و ذخیره آن
                    ExtraworkSignature ew = new ExtraworkSignature
                    {
                        ExtraWork_ID = Extrawork_ID,
                        PersonalCodeSignaturer = parentUsers.FirstOrDefault().Id
                    };
                    await _ExtraWorkSignatureRepository.AddAsync(ew);
                    await _ExtraWorkSignatureRepository.SaveChangesAsync();

                    //اگر شناسه پدر یوزر جاری متفاوت از شناسه تایید کننده نهایی بود این تابع به صورت بازگشتی اجرا شود
                    if (parentUsers.FirstOrDefault().Id != SendFinalSmsToUser_UserId)
                    {
                        simpleFunctionResult result = await createVacationSigniture(Extrawork_ID, parentUsers.FirstOrDefault().Id);
                        if (!result.Status)
                        {
                            return new simpleFunctionResult { Status = result.Status, message = result.message };
                        }
                    }
                }
                
                return new simpleFunctionResult { Status = true, message = "ok" };
            }
            catch (Exception err)
            {
                return new simpleFunctionResult { Status = false, message = err.Message + " *** " + err.InnerException };
            }
        }


        //[NonAction]
        /////برای حالت های خاص مثل اینکه درخواست های روسا توسط رئیس اداری به صورت مستقیم بررسی شود.
        //public async Task<ExtraWork> checkSpecialState(ExtraWork ExtraWork)
        //{
        //    var temp = await _ExtraWorkRepository.GetWithIncludeAsync<ExtraWork>(
        //        selector: x => x,
        //        where: x => x.ExtraWorkID == ExtraWork.ExtraWorkID,
        //        include: x => x.Include(s => s.Employee_Ext_1)
        //        );
        //    ExtraWork tempExtraWork = temp.FirstOrDefault();
        //    var user = await _userManager.FindByIdAsync(tempExtraWork.Employee_Ext_1.UserIdentity_Id);
        //    var userRoles = await _userManager.GetRolesAsync(user);
        //    if (userRoles.Contains("رئیس برنامه ریزی و توسعه و بهبود") ||
        //        userRoles.Contains("رئیس تولید") ||
        //        userRoles.Contains("رئیس فنی") ||
        //        userRoles.Contains("رئیس فروش") ||
        //        userRoles.Contains("رئیس حسابداری") ||
        //        userRoles.Contains("مدیر عامل") ||
        //        userRoles.Contains("رئیس بازرگانی خارجی") ||
        //        userRoles.Contains("مدیر بهره برداری") ||
        //        userRoles.Contains("رئیس دفتر مدیر عامل")
        //        )
        //        tempExtraWork.StatusManagerSignature = true;

        //    return tempExtraWork;
        //}

        [DisplayName("لیست درخواست های اضافه کاری")]
        public async Task<IActionResult> ViewAll()
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.OfficeUnit),
                where: x => x.UserIdentity_Id == user.Id
                );

            IEnumerable<ExtraworkSuperiorViewAllViewModel> extrawork = new List<ExtraworkSuperiorViewAllViewModel>();

            //var ExraWorks = await _ExtraWorkRepository.GetWithIncludeAsync(
            //    selector: x => x,
            //    include: x => x.Include(s => s.Employee_Ext_1).ThenInclude(s => s.ApplicationUser),
            //    where: x => x.PersonalCodeFirst == employee.FirstOrDefault().PersonalCode,
            //    orderBy: x => x.OrderByDescending(s => s.ExtraWorkID));
            extrawork = await _ExtraWorkRepository.GetWithIncludeAsync(
                       selector: x => new ExtraworkSuperiorViewAllViewModel
                       {
                           CreateDate = (x.CreateDate != null ? x.CreateDate.ToShamsiWithTime() : ""),
                           EndDate = x.EndDate,
                           FinalStatus = x.FinalStatus,
                           FullName = x.Employee_Ext_1.ApplicationUser.FullName,
                           StartDate = x.StartDate,
                           ExtraWork_ID = x.ExtraWorkID
                       },
                       include: x => x.Include(s => s.Employee_Ext_1).ThenInclude(s=>s.ApplicationUser),
                       where: x => x.Employee_Ext_1.ApplicationUser.Id == user.Id,
                       orderBy:x=>x.OrderByDescending(s=>s.ExtraWorkID)
                       );

            #region jQuery_DataTable_Ajax_Code
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;
            if (!(string.IsNullOrEmpty(sortColumn)))
            {
                extrawork = extrawork.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection).ToList();
            }
            if (!string.IsNullOrEmpty(searchValue))
            {
                extrawork = extrawork.Where(m => m.FullName.Contains(searchValue) || m.CreateDate.Contains(searchValue)
                //|| (!string.IsNullOrEmpty(m.StartTime) ? m.StartTime.Contains(searchValue) : false)
                //|| (!string.IsNullOrEmpty(m.EndTime) ? m.EndTime.Contains(searchValue) : false)
                //|| m.DateVacationTime.Contains(searchValue)
                //|| m.StartDate.Contains(searchValue)
                //|| m.EndDate.Contains(searchValue)
                ).ToList();

            }
            recordsTotal = extrawork.Count();
            var data = extrawork.Skip(skip).Take(pageSize).ToList();
            var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
            #endregion
            return new JsonResult(jsonData);
            }
            catch (Exception err)
            {

                throw;
            }
        }


        [DisplayName("ویرایش اضافه کاری")]
        public async Task<IActionResult> Edit(int id)
        {
            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.OfficeUnit),
                where: x => x.UserIdentity_Id == user.Id
                );
            if (user != null && employee.FirstOrDefault() != null)
            {
                ViewBag.FullName = user.FullName;
                ViewBag.PersonalCode = employee.FirstOrDefault().PersonalCode;
                SupervionLevel = employee.FirstOrDefault().SupervisionLevel.Value;
                ViewBag.SupervionLevel = SupervionLevel;
            }

            var ExtraWork = await _ExtraWorkRepository.GetWithIncludeAsync(
                selector:x=>x,
                where:x => x.ExtraWorkID == id,
                include:x=>x.Include(s=>s.Employee_Ext_1).ThenInclude(s=>s.ApplicationUser)
                );
            ExtraworkSuperiorViewModel esvm = new ExtraworkSuperiorViewModel();
            esvm.extraWork = ExtraWork.FirstOrDefault();
            var vacationSignatures = await _ExtraWorkSignatureRepository.GetWithIncludeAsync(
                selector: x => x,
                where: x => x.ExtraWork_ID == id,
                include: x => x.Include(s => s.applicationUser),
                orderBy: x => x.OrderBy(s => s.ExtraworkSignature_ID)
                );
            esvm.extraworkSignatures = vacationSignatures.ToList();


            ViewBag.firstVSStatus = vacationSignatures.FirstOrDefault().SignatureStatus;

            return PartialView(esvm);
        }


        [HttpPost]
        public async Task<IActionResult> Edit(ExtraWork ExtraWork, string sdate, string edate)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            DateTime dtStart = dtDateTime.AddSeconds(double.Parse(sdate)).ToLocalTime();
            DateTime dtEnd = dtDateTime.AddSeconds(double.Parse(edate)).ToLocalTime();
            ExtraWork.StartDate = dtStart.ToString("MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            ExtraWork.EndDate = dtEnd.ToString("MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

            var ThisExtraWork = await _ExtraWorkRepository.GetByIdAsync(ExtraWork.ExtraWorkID);
            //ThisExtraWork.PersonalCode = ExtraWork.PersonalCode;
            ThisExtraWork.StartDate = ExtraWork.StartDate;
            ThisExtraWork.EndDate = ExtraWork.EndDate;
            ThisExtraWork.StartTime = ExtraWork.StartTime;
            ThisExtraWork.EndTime = ExtraWork.EndTime;
            ThisExtraWork.ExtraWorktime = ExtraWork.ExtraWorktime;
            //ThisExtraWork.CreateDate = ExtraWork.CreateDate;
            //ThisExtraWork.StatusSuperiorSignature = ExtraWork.StatusSuperiorSignature;
            //ThisExtraWork.StatusManagerSignature = ExtraWork.StatusManagerSignature;

            _ExtraWorkRepository.Update(ThisExtraWork);
            await _ExtraWorkRepository.SaveChangesAsync();

            var user = await _userManager.GetUserAsync(User);
            var myRole = await _userManager.GetRolesAsync(user);
            myRole.Remove("Employee");
            var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());
            var parentRole = await _ApplicationRoleRepository.GetAsync(x => x.Id == mr.ParentRoleId);
            var parentUsers = await _userManager.GetUsersInRoleAsync(parentRole.Name);

            string userMobile = parentUsers.FirstOrDefault().Mobile_1;
            string userName = user.FullName;

            await _smsSender.SendSubmitExtraWorkMessageSMSAsync(userMobile, userName, ExtraWork.StartDate.ToShortPersian());

            return Json(new { status = "ok" });
        }

        [DisplayName("حذف")]
        public async Task<IActionResult> Delete(int id)
        {
            var vacationSignatureList = await _ExtraWorkSignatureRepository.GetAllAsync(x => x.ExtraWork_ID == id, orderBy: x => x.OrderBy(s => s.ExtraworkSignature_ID));
            if (vacationSignatureList.FirstOrDefault().SignatureStatus != null)
            {
                return Json("notAllowed");
            }
            var ExtraWork = await _ExtraWorkRepository.GetByIdAsync(id);
            if (ExtraWork != null)
            {
                _ExtraWorkRepository.Delete(ExtraWork);
                await _ExtraWorkRepository.SaveChangesAsync();
                return Json("ok");
            }
            return Json("Nok");
        }

    }
}