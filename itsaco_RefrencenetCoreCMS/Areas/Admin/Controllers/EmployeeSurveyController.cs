﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

using DomainClasses.Employees;
using DomainClasses.Survey;
using DomainClasses.User;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using Services.Repositories;

using ViewModels;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("فرم نظرسنجی کارکنان")]
    public class EmployeeSurveyController : Controller
    {
        public static Int16 OtherData_ID;
        public UserManager<ApplicationUser> _userManager;
        public IGenericRepository<Employee> _empgenericRepository;
        public IGenericRepository<MainSurvey> _mainSurveyRepository;
        public IGenericRepository<SurvayQuestionGroup> _survayQuestionGroupRepository;
        public IGenericRepository<SurveyQuestion> _survayQuestionRepository;
        public IGenericRepository<SurveySuppliersOtherData> _surveySuppliersOtherDataRepository;
        public IGenericRepository<SurveyCustomerOtherData> _surveyCustomerOtherDataRepository;
        public IGenericRepository<SurveyEmployeesOtherData> _surveyEmployeesOtherDataRepository;
        public IGenericRepository<SurveyQuestionReply> _surveyQuestionReplyRepository;
        public EmployeeSurveyController(IGenericRepository<SurveyEmployeesOtherData> surveyEmployeesOtherDataRepository, UserManager<ApplicationUser> userManager, IGenericRepository<Employee> empgenericRepository, IGenericRepository<SurveyCustomerOtherData> surveyCustomerOtherDataRepository, IGenericRepository<SurveyQuestion> survayQuestionRepository, IGenericRepository<MainSurvey> mainSurveyRepository, IGenericRepository<SurvayQuestionGroup> survayQuestionGroupRepository, IGenericRepository<SurveySuppliersOtherData> surveySuppliersOtherDataRepository, IGenericRepository<SurveyQuestionReply> surveyQuestionReplyRepository)
        {
            _mainSurveyRepository = mainSurveyRepository;
            _survayQuestionGroupRepository = survayQuestionGroupRepository;
            _survayQuestionRepository = survayQuestionRepository;
            _surveySuppliersOtherDataRepository = surveySuppliersOtherDataRepository;
            _surveyQuestionReplyRepository = surveyQuestionReplyRepository;
            _surveyCustomerOtherDataRepository = surveyCustomerOtherDataRepository;
            _userManager = userManager;
            _empgenericRepository = empgenericRepository;
            _surveyEmployeesOtherDataRepository = surveyEmployeesOtherDataRepository;
        }
        public async Task<IActionResult> Index()
        {
            var mainSurvey = await _mainSurveyRepository.GetAsync(x => x.SurveyType == 2 && x.IsActive, x => x.OrderByDescending(s => s.MainSurvey_ID));
            if (mainSurvey != null)
            {
                ViewData["EmployeeSurvey"] = mainSurvey.Title;
            }
            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.OfficeUnit),
                where: x => x.UserIdentity_Id == user.Id
                );
            if (user != null && employee.FirstOrDefault() != null)
            {
                ViewBag.FullName = user.FullName;
                ViewBag.PersonalCode = employee.FirstOrDefault().PersonalCode;
            }
            return View();
        }

        public async Task<IActionResult> ViewEmployeeSurvey()
        {
            SurveyVM survey = new SurveyVM();
            var mainSurvey = await _mainSurveyRepository.GetAsync(x => x.SurveyType == 2 && x.IsActive, x => x.OrderByDescending(s => s.MainSurvey_ID));

            IEnumerable<SurvayQuestionGroup> quesGroup = await _survayQuestionGroupRepository.GetAllAsync(x => x.MainSurveyId == mainSurvey.MainSurvey_ID);
            survey.SurvayQuestionGroup = quesGroup;

            IEnumerable<SurveyQuestion> questions = await _survayQuestionRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.SurveyAnswerType).Include(s => s.SurvayQuestionGroup),
                where: x => x.SurvayQuestionGroup.MainSurveyId == quesGroup.FirstOrDefault().MainSurveyId
                );

            survey.SurveyQuestions = questions;

            //questions.

            return PartialView(survey);
            //survey.
        }
        [HttpPost]
        public async Task<IActionResult> SurveySubmit(SurveyEmployeesOtherData surveyEmployeesOtherData)
        {
            try
            {
                var EmployeeSurvey = await _surveyEmployeesOtherDataRepository.GetAllAsync(x => x.MainSurveyId == surveyEmployeesOtherData.MainSurveyId && x.PersonalCode == surveyEmployeesOtherData.PersonalCode);
                if (EmployeeSurvey.Any())
                { return Json("Repeated"); }
                surveyEmployeesOtherData.CompleteFormDate = DateTime.Now;
                await _surveyEmployeesOtherDataRepository.AddAsync(surveyEmployeesOtherData);
                await _surveyEmployeesOtherDataRepository.SaveChangesAsync();

                OtherData_ID = surveyEmployeesOtherData.SE_ID;
                return Json("ok");
            }
            catch (Exception err)
            {
                throw err;
            }

        }

        [HttpPost]
        public async Task<IActionResult> GetSurveyData(List<string> questionId, List<string> selectedReply)
        {
            try
            {
                SurveyQuestionReply sqr = new SurveyQuestionReply();
                for (int i = 0; i < questionId.Count; i++)
                {

                    sqr.QuestionId = Convert.ToInt16(questionId[i]);

                    sqr.SelectedReply = Convert.ToByte(selectedReply[i]);
                    sqr.ExtraInformationId = OtherData_ID;

                    await _surveyQuestionReplyRepository.AddAsync(sqr);
                    await _surveyQuestionReplyRepository.SaveChangesAsync();


                    ////
                    ///
                    var thisQuestion = await _survayQuestionRepository.GetAsync(x => x.Question_ID == Convert.ToInt16(questionId[i]));

                    thisQuestion.ReplyCount += 1;
                    switch (selectedReply[i])
                    {
                        case "1":
                            thisQuestion.OptionVoteCount5 += 1;
                            break;
                        case "2":
                            thisQuestion.OptionVoteCount4 += 1;
                            break;
                        case "3":
                            thisQuestion.OptionVoteCount3 += 1;
                            break;
                        case "4":
                            thisQuestion.OptionVoteCount2 += 1;
                            break;
                        case "5":
                            thisQuestion.OptionVoteCount1 += 1;
                            break;
                        default:
                            break;
                    }

                    _survayQuestionRepository.Update(thisQuestion);
                    await _survayQuestionRepository.SaveChangesAsync();
                }
                return Json("ok");
            }
            catch (Exception err)
            {

                throw err;
            }
        }
    }
}