﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Product;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Services.Repositories;

namespace AspCorePanelProject.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]
    [DisplayName("تولید کننده ها")]

    public class ProducerController : Controller
    {
        public List<SelectListItem> producerList;
        public IGenericRepository<Producer> _producerRepository;
        public ProducerController(IGenericRepository<Producer> producerRepository)
        {
            _producerRepository = producerRepository;
        }
        [DisplayName("صفحه نخست")]

        public IActionResult Index()
        {
            return View();
        }
        [DisplayName("لیست تولید کننده ها")]

        public async Task<IActionResult> ViewAll(string id)
        {
            return PartialView(await _producerRepository.GetAllAsync(x => x.Lang == id));
        }
        [DisplayName("افزودن  ")]

        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Producer producer, IFormFile producerImage)
        {
            if (producer != null)
            {
                if (producerImage != null)
                {
                    if (!Directory.Exists("wwwroot/Files/Producer")) {
                        Directory.CreateDirectory("wwwroot/Files/Producer");
                    }
                    producer.Icon = producerImage.FileName.Substring(0, producerImage.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(producerImage.FileName);
                    string filePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Producer", producer.Icon);
                    //string filePathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Contents/Thumb", contents.ContentImage);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await producerImage.CopyToAsync(stream);
                    }
                }
                await _producerRepository.AddAsync(producer);
                await _producerRepository.SaveChangesAsync();
                return Json(producer);
            }
            return Json("no");
        }
        [DisplayName("ویرایش  ")]

        public async Task<IActionResult> Edit(Int16 id)
        {
            return View(await _producerRepository.GetByIdAsync(id));
        }
        [HttpPost]
        public async Task<IActionResult> Edit(Producer producer, IFormFile producerImage)
        {
            if (producer != null)
            {
                if (producerImage != null)
                {
                    if (producer.Icon != null)
                    {
                        string oldfilePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Producer", producer.Icon);
                        await DeleteFilesInProj(oldfilePath);
                    }

                    producer.Icon = producerImage.FileName.Substring(0, producerImage.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(producerImage.FileName);
                    string filePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Producer", producer.Icon);
                    //string filePathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Contents/Thumb", contents.ContentImage);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await producerImage.CopyToAsync(stream);
                    }
                }
                _producerRepository.Update(producer);
                await _producerRepository.SaveChangesAsync();
                return Json(producer);
            }
            return Json("no");
        }
        [DisplayName("حذف  ")]

        public async Task<IActionResult> Delete(Int16 id)
        {
            var producer = await _producerRepository.GetByIdAsync(id);
            if (producer != null)
            {
                if (producer.Icon != null)
                {
                    string oldfilePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Producer", producer.Icon);
                    await DeleteFilesInProj(oldfilePath);
                }
                _producerRepository.Delete(producer);
                await _producerRepository.SaveChangesAsync();
                return Json(producer);
            }
            return Json("no");
        }

        public async Task<IActionResult> DeleteFilesInProj(string path, string pathThumb = "")
        {
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            if (System.IO.File.Exists(pathThumb))
            {
                System.IO.File.Delete(pathThumb);
            }
            return Json(Ok());
        }

        public async Task<JsonResult> ViewAllProducerList(string id)
        {
            producerList = new List<SelectListItem>();
            producerList.Add(new SelectListItem
            {
                Text = "انتخاب کنید ...",
                Value = "0"
            });
            foreach (var item in await _producerRepository.GetAllAsync(x => x.Lang == id))
            {
                producerList.Add(new SelectListItem
                {
                    Text = item.ProducerTitle,
                    Value = item.Producer_ID.ToString()
                });
            }
            return Json(producerList);
        }
    }
}