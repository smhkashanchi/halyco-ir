﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.BasicInformation;
using DomainClasses.Employees;
using DomainClasses.Proposal;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("لیست پیشنهاد ها (دسترسی برای مدیر)")]
    public class adminProposalsController : Controller
    {
        public IGenericRepository<OfficeUnit> _officeUnitRepository;
        public IGenericRepository<OfficePost> _officePostRepository;
        public IGenericRepository<Employee> _employeeRepository;
        public IGenericRepository<MainProposal> _mainProposalRepository;
        public IGenericRepository<ProposalFiles> _proposalFilesRepository;
        public IGenericRepository<ProposalProviders> _proposalProvidersRepository;

        public adminProposalsController(IGenericRepository<Employee> employeeRepository, IGenericRepository<ProposalProviders> proposalProvidersRepository, IGenericRepository<ProposalFiles> proposalFilesRepository, IGenericRepository<MainProposal> mainProposalRepository, IGenericRepository<OfficeUnit> officeUnitRepository, IGenericRepository<OfficePost> officePostRepository)
        {
            _officeUnitRepository = officeUnitRepository;
            _officePostRepository = officePostRepository;
            _mainProposalRepository = mainProposalRepository;
            _proposalFilesRepository = proposalFilesRepository;
            _proposalProvidersRepository = proposalProvidersRepository;
            _employeeRepository = employeeRepository;
        }

        [DisplayName("صفحه لیست پیشنهادها")]
        public IActionResult Index()
        {
            return View();
        }

        ////-------------------------------------------------------------لیست ارائه پیشنهاد ها
        //[DisplayName("صفحه لیست پیشنهادها (دسترسی برای مدیر)")]
        //public IActionResult Proposals()
        //{
        //    return View();
        //}
        [DisplayName("نمایش پیشنهادها")]
        public async Task<IActionResult> ViewAll()
        {
            return PartialView(await _mainProposalRepository.GetAllAsync(null, x => x.OrderByDescending(s => s.Proposal_ID)));
        }

        [DisplayName("نمایش جزئیات ارائه کننده پیشنهاد")]
        public async Task<IActionResult> ViewDetailsProposalProvider(int id)
        {
            var proposal = await _proposalProvidersRepository.GetWithIncludeAsync
                (selector: x => x,
                include: x => x.Include(s => s.OfficeUnit).Include(s => s.OfficePost).Include(s => s.Employee).ThenInclude(s => s.ApplicationUser),
                where: x => x.ProposalId == id
                );
            return PartialView(proposal);
        }

        [DisplayName("نمایش جزئیات پیشنهادها")]
        public async Task<IActionResult> ViewDetailsMainProposal(int id)
        {
            var proposal = await _mainProposalRepository.GetAsync(x => x.Proposal_ID == id);
            return PartialView(proposal);
        }
        [DisplayName("نمایش فایل پیشنهاد ها")]
        public async Task<IActionResult> ViewDetailsProposalFiles(int id)
        {
            var proposal = await _proposalFilesRepository.GetAllAsync(x => x.ProposalId == id);
            return PartialView(proposal);
        }


    }
}