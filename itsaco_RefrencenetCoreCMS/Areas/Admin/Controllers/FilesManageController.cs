﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.AVF;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Services.Repositories;
using Utilities;
using ViewModels;

namespace AspCorePanelProject.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]
    [DisplayName("دانلودها و ویدئوها")]
    public class FilesManageController : Controller
    {
        public List<SelectListItem> typeList;
        public List<SelectListItem> downloadGroupList;
        public Dictionary<string, string> fileTypeList;
        public IGenericRepository<AVFGroup> _aVFGroupRepository;
        public IGenericRepository<AVF> _aVFRepository;
        public FilesManageController(IGenericRepository<AVFGroup> aVFGroupRepository, IGenericRepository<AVF> aVFRepository)
        {
            _aVFGroupRepository = aVFGroupRepository;
            _aVFRepository = aVFRepository;
        }
        [DisplayName("صفحه نخست")]

        public IActionResult Index()
        {
            return View();
        }
        [DisplayName("لیست گروه ها")]

        public async Task<IActionResult> ViewAll(short id)
        {
            return PartialView(await _aVFGroupRepository.GetByIdAsync(id));
        }
        [DisplayName("نمایش جزئیات گروه")]

        public async Task<IActionResult> ViewGroupData(short id)
        {
            return PartialView(await _aVFGroupRepository.GetByIdAsync(id));
        }

        public async Task<string> createTreeView(short pgId, string lang)
        {
            return JsonConvert.SerializeObject(await ViewInTreeView(pgId, lang));
        }
        [NonAction]
        public async Task<List<Node>> ViewInTreeView(short gId, string lang)
        {
            List<Node> TNodes = new List<Node>();
            List<Node> TNodes2 = new List<Node>();
            // var groups;

            var GroupData = await _aVFGroupRepository.GetByIdAsync(gId);

            var groups = await _aVFGroupRepository.GetAllAsync(x => x.ParentId == gId, x => x.OrderByDescending(s => s.AVFGroupTitle));
            foreach (var item in groups)
            {
                var node = await ViewInTreeView(item.AVFGroup_ID, lang);
                TNodes2.Add(node.FirstOrDefault());
            }


            TNodes.Add(new Node()
            {
                text = GroupData.AVFGroupTitle,
                href = "#" + GroupData.AVFGroupTitle,
                tags = gId.ToString(),
                nodes = TNodes2
            });

            return TNodes;
        }
        public async Task<JsonResult> GetAllGroupsByLang(string id)
        {
            downloadGroupList = new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text = "انتخاب کنید ...",
                    Value = "0"
                }
            };
            foreach (var item in await _aVFGroupRepository.GetAllAsync(x => x.Lang == id && x.ParentId!=null, x => x.OrderByDescending(s => s.AVFGroup_ID)))
            {
                downloadGroupList.Add(new SelectListItem
                {
                    Text = item.AVFGroupTitle,
                    Value = item.AVFGroup_ID.ToString()
                });
            }


            return Json(downloadGroupList);
        }
        [DisplayName("افزودن گروه ")]

        public IActionResult CreateGroup()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateGroup(AVFGroup aVFGroup, IFormFile AVFGroupImage)
        {
            if (!Directory.Exists("wwwroot/Files/FileManage/AVFGroupImage"))
            {
                Directory.CreateDirectory("wwwroot/Files/FileManage/AVFGroupImage");
            }
            if (!Directory.Exists("wwwroot/Files/FileManage/AVFGroupImage/Temp"))
            {
                Directory.CreateDirectory("wwwroot/Files/FileManage/AVFGroupImage/Temp");
            }
            if (aVFGroup != null)
            {
                if (AVFGroupImage != null)
                {
                    aVFGroup.AVFGroupImage = AVFGroupImage.FileName.Substring(0, AVFGroupImage.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(AVFGroupImage.FileName);
                    string TempPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/FileManage/AVFGroupImage/Temp", aVFGroup.AVFGroupImage);
                    string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/FileManage/AVFGroupImage", aVFGroup.AVFGroupImage);

                    using (var stream = new FileStream(TempPath, FileMode.Create))
                    {
                        AVFGroupImage.CopyTo(stream);
                    }
                    ImageManipulate imageManipulate = new ImageManipulate();
                    imageManipulate.resizeImage2(TempPath, path, 260, 140);
                    System.IO.File.Delete(TempPath);
                }
                var list = await _aVFGroupRepository.GetAllAsync();
                if (list.Count() > 0 && aVFGroup.ParentId == null)
                {
                    return Json(new { res = false });
                }
                await _aVFGroupRepository.AddAsync(aVFGroup);
                await _aVFGroupRepository.SaveChangesAsync();
                return Json(new { model = aVFGroup, res = true });
            }
            return View();
        }
        [DisplayName("ویرایش گروه ")]

        public async Task<IActionResult> EditGroup(short id)
        {
            return View(await _aVFGroupRepository.GetByIdAsync(id));
        }

        [HttpPost]
        public async Task<IActionResult> EditGroup(AVFGroup aVFGroup, IFormFile AVFGroupPic)
        {
            if (aVFGroup != null)
            {
                if (AVFGroupPic != null)
                {
                    string oldPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/FileManage/AVFGroupImage", aVFGroup.AVFGroupImage);
                    System.IO.File.Delete(oldPath);

                    aVFGroup.AVFGroupImage = AVFGroupPic.FileName.Substring(0, AVFGroupPic.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(AVFGroupPic.FileName);
                    string TempPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/FileManage/AVFGroupImage/Temp", aVFGroup.AVFGroupImage);
                    string savepath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/FileManage/AVFGroupImage", aVFGroup.AVFGroupImage);

                    using (var stream = new FileStream(TempPath, FileMode.Create))
                    {
                        AVFGroupPic.CopyTo(stream);
                    }
                    //
                    ImageManipulate imageManipulate = new ImageManipulate();
                    imageManipulate.resizeImage2(TempPath, savepath, 260, 140);
                    System.IO.File.Delete(TempPath);
                }
                _aVFGroupRepository.Update(aVFGroup);
                await _aVFGroupRepository.SaveChangesAsync();
                return Json(aVFGroup.AVFGroup_ID);
            }
            return View();
        }
        [DisplayName("حذف گروه ")]

        public async Task<IActionResult> DeleteGroup(short id)
        {
            var group = await _aVFGroupRepository.GetByIdAsync(id);
            if (group != null)
            {
               
                var avfg = await _aVFRepository.GetAsync(x => x.AVFGroupId == id);
                if (avfg == null)
                {
                    if (group.AVFGroupImage != null)
                    {
                        string Path = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/FileManage/AVFGroupImage", group.AVFGroupImage);
                        System.IO.File.Delete(Path);
                    }
                    _aVFGroupRepository.Delete(group);
                    await _aVFGroupRepository.SaveChangesAsync();
                    return Json(group);
                }
                return Json("NOK");
            }
            return Json("no");
        }
        ////////////////////// بخش فایل ها
        ///
        [DisplayName("لیست فایل ها")]

        public async Task<IActionResult> ViewAllFiles(int id)
        {
            return PartialView(await _aVFRepository.GetAllAsync(x => x.AVFGroupId == id, x => x.OrderByDescending(s => s.AVF_ID)));
        }
        [DisplayName("افزودن فایل ")]

        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(AVF aVF, IFormFile file, IFormFile vidoImg)
        {
            if (!Directory.Exists("wwwroot/Files/FileManage"))
            {
                Directory.CreateDirectory("wwwroot/Files/FileManage");
            }
            if (!Directory.Exists("wwwroot/Files/FileManage/Image"))
            {
                Directory.CreateDirectory("wwwroot/Files/FileManage/Image");
            }
            if (!Directory.Exists("wwwroot/Files/FileManage/Image/Thumb"))
            {
                Directory.CreateDirectory("wwwroot/Files/FileManage/Image/Thumb");
            }
            if (aVF != null)
            {
                if (file != null)
                {
                    aVF.FileNameOrLink = file.FileName.Substring(0, file.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                    string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/FileManage", aVF.FileNameOrLink);

                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }
                }

                if (vidoImg != null)
                {
                    aVF.VideoImage = vidoImg.FileName.Substring(0, vidoImg.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(vidoImg.FileName);
                    string path2 = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/FileManage/Image", aVF.VideoImage);
                    string thumbpath2 = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/FileManage/Image/Thumb", aVF.VideoImage);

                    using (var stream = new FileStream(path2, FileMode.Create))
                    {
                        await vidoImg.CopyToAsync(stream);
                    }
                    ImageManipulate imageManipulate = new ImageManipulate();
                    imageManipulate.resizeImage2(path2, thumbpath2, 260, 200);
                    System.IO.File.Delete(path2);
                }
                aVF.CreateDate = DateTime.Now;
                await _aVFRepository.AddAsync(aVF);
                await _aVFRepository.SaveChangesAsync();
                return Json(aVF);

            }
            return View();
        }
        [DisplayName("ویرایش فایل ")]

        public async Task<IActionResult> Edit(int id)
        {
            var file = await _aVFRepository.GetByIdAsync(id);
            typeList = new List<SelectListItem>();
            if (file.FileOrLink == true)
            {
                typeList.Add(new SelectListItem
                {
                    Text = "لینک",
                    Value = "0",
                });
                typeList.Add(new SelectListItem
                {
                    Text = "فایل",
                    Value = "1",
                    Selected = true
                });
            }
            else
            {
                typeList.Add(new SelectListItem
                {
                    Text = "لینک",
                    Value = "0",
                    Selected = true
                });
                typeList.Add(new SelectListItem
                {
                    Text = "فایل",
                    Value = "1",

                });
            }
            ViewBag.type = typeList;
            //fileTypeList.Add("فایل", "فایل");
            //fileTypeList.Add("فیلم", "فیلم");
            //fileTypeList.Add("صوت", "صوت");
            //ViewBag.allType = new SelectList(fileTypeList, "Key", "Value", 1);
            return View(file);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(AVF aVF, IFormFile file, IFormFile videoImg)
        {
            if (aVF != null)
            {
                if (file != null)
                {
                    if (aVF.FileOrLink == true && aVF.FileNameOrLink != null)
                    {
                        string oldFilePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/FileManage", aVF.FileNameOrLink);
                        await DeleteFilesInProj(oldFilePath);

                        aVF.FileNameOrLink = file.FileName.Substring(0, file.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/FileManage", aVF.FileNameOrLink);

                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            await file.CopyToAsync(stream);
                        }
                    }
                }
                ////
                ///
                if (videoImg != null)
                {
                    if (aVF.VideoImage != null)
                    {
                        string oldpath2 = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/FileManage/Image", aVF.VideoImage);
                        string oldthumbpath2 = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/FileManage/Image/Thumb", aVF.VideoImage);
                        await DeleteFilesInProj2(oldpath2, oldthumbpath2);
                    }

                    aVF.VideoImage = videoImg.FileName.Substring(0, videoImg.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(videoImg.FileName);
                    string path2 = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/FileManage/Image", aVF.VideoImage);
                    string thumbpath2 = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/FileManage/Image/Thumb", aVF.VideoImage);

                    using (var stream = new FileStream(path2, FileMode.Create))
                    {
                        await videoImg.CopyToAsync(stream);
                    }
                    ImageManipulate imageManipulate = new ImageManipulate();
                    imageManipulate.resizeImage2(path2, thumbpath2, 260, 200);
                    System.IO.File.Delete(path2);
                }

                _aVFRepository.Update(aVF);
                await _aVFRepository.SaveChangesAsync();
                return Json(aVF);
            }
            return View();
        }
        [DisplayName("حذف فایل ")]

        public async Task<IActionResult> DeleteAVF(int id)
        {
            var avf = await _aVFRepository.GetByIdAsync(id);
            if (avf != null)
            {
                //if (!_aVFGroupRepository.IsSubGroup(id))
                //{'
                if (avf.VideoImage != null)
                {
                    string oldpath2 = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/FileManage/Image", avf.VideoImage);
                    string oldthumbpath2 = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/FileManage/Image/Thumb", avf.VideoImage);
                    await DeleteFilesInProj2(oldpath2, oldthumbpath2);

                    string filePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/FileManage", avf.FileNameOrLink);
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }
                 
                }
                _aVFRepository.Delete(avf);
                await _aVFRepository.SaveChangesAsync();
                return Json(avf);
                //}
            }
            return Json("nall");
        }
        public async Task<IActionResult> DeleteFilesInProj(string path)
        {
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            return Json(Ok());
        }
        public async Task<IActionResult> DeleteFilesInProj2(string path, string thumbPath)
        {
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            if (System.IO.File.Exists(thumbPath))
            {
                System.IO.File.Delete(thumbPath);
            }
            return Json(Ok());
        }
    }
}