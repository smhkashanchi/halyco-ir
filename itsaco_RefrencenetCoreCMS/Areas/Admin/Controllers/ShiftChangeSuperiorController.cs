﻿using DomainClasses.Role;
using DomainClasses.User;
using DomainClasses.Employees;
using System.Linq.Dynamic.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services;
using Services.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Utilities;
using ViewModels;
using static Utilities.FunctionResult;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("درخواست تعویض شیفت کارمندان")]
    public class ShiftChangeSuperiorController : Controller
    {
        public UserManager<ApplicationUser> _userManager;
        public RoleManager<ApplicationRole> _roleManager;
        public IGenericRepository<ApplicationRole> _ApplicationRoleRepository;
        public IGenericRepository<ApplicationUser> _userRepository;
        public IGenericRepository<ShiftChange> _ShiftChangeRepository;
        public IGenericRepository<ShiftChangeSignature> _ShiftChangeSignatureRepository;
        public IGenericRepository<Employee> _empgenericRepository;
        public ISMSSender _smsSender;

        public ShiftChangeSuperiorController(UserManager<ApplicationUser> userManager, IGenericRepository<ApplicationRole> ApplicationRoleRepository, RoleManager<ApplicationRole> roleManager, IGenericRepository<ApplicationUser> userRepository, IGenericRepository<ShiftChange> ShiftChangeRepository, IGenericRepository<ShiftChangeSignature> ShiftChangeSignatureRepository, ISMSSender smsSender, IGenericRepository<Employee> empgenericRepository)
        {
            _userManager = userManager;
            _ApplicationRoleRepository = ApplicationRoleRepository;
            _roleManager = roleManager;
            _userRepository = userRepository;
            _ShiftChangeRepository = ShiftChangeRepository;
            _ShiftChangeSignatureRepository = ShiftChangeSignatureRepository;
            _smsSender = smsSender;
            _empgenericRepository = empgenericRepository;
        }

        [DisplayName("صفحه نخست")]
        public async Task<IActionResult> Index()
        {
            var user = await _userRepository.GetAsync(x => x.UserName == User.Identity.Name);
            ViewBag.UserIdentity_Id = user.Id;
            ViewBag.Fullname = user.FullName;
            return View();
        }


        [DisplayName("مشاهده لیست درخواست ها")]
        [HttpPost]
        public async Task<IActionResult> viewAll()
        {
            try
            {

                //بدست آورن رکورد یوزر من
                var user = await _userManager.GetUserAsync(User);
                //بدست آوردن لیست  نام نقش های من
                var myRole = await _userManager.GetRolesAsync(user);
                //حذف نقش کارمند از لیست نقش های من
                myRole.Remove("Employee");
                //بدست آوردن نقش من
                var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());
                //بدست آوردن لیست تمام نقش های زیرمجموعه نقش من
                List<ApplicationRole> allMyChildrenRoles = new List<ApplicationRole>();
                allMyChildrenRoles = await getChildrenRoles(mr, allMyChildrenRoles);
                //بدست آوردن لیست تمام کارمندان نقش های زیرمجموعه نقش من
                List<ApplicationUser> AllMyEmployees = new List<ApplicationUser>();
                foreach (var item in allMyChildrenRoles)
                {
                    var TempEmployeeList = await _userManager.GetUsersInRoleAsync(item.Name);
                    AllMyEmployees.AddRange(TempEmployeeList);
                }
                //بدست آوردن لیست نقش هایی که مدیر پشتیبانی می تواند درخواست های آنها را ببیند
                if (mr.Id == "a2c5ff4a-64b0-4381-9cc2-e65596ebb271")
                {
                    var TempRoles = await _ApplicationRoleRepository.GetAllAsync(x => x.IsAgentOfManager);
                    foreach (var item in TempRoles)
                    {
                        var TempEmployeeList = await _userManager.GetUsersInRoleAsync(item.Name);
                        AllMyEmployees.AddRange(TempEmployeeList);
                    }
                }
                //بدست آوردن تمام درخواست های مرخصی کارمندان زیرمجموعه نقش من
                IEnumerable<ShiftChangeSuperiorViewAllViewModel> AllMyEmployeeShiftChanges = new List<ShiftChangeSuperiorViewAllViewModel>();
                foreach (var item in AllMyEmployees)
                {
                    var tempShiftChangeList = await _ShiftChangeRepository.GetWithIncludeAsync(
                        selector: x => new ShiftChangeSuperiorViewAllViewModel
                        {
                            CreateDate = (x.CreateDate != null ? x.CreateDate.ToShamsiWithTime() : ""),
                            FinalStatus = x.FinalStatus,
                            FullName = x.Employee_Shc_1.ApplicationUser.FullName,
                            shiftDate = x.shiftDate.ToShortPersian(),
                            ShiftChange_ID = x.ShiftChange_ID
                        },
                        include: x => x.Include(s => s.Employee_Shc_1),
                        where: x => x.Employee_Shc_1.ApplicationUser.Id == item.Id && (x.StatusAlternateSignature.HasValue && x.StatusAlternateSignature.Value)
                        );
                    AllMyEmployeeShiftChanges = (AllMyEmployeeShiftChanges ?? Enumerable.Empty<ShiftChangeSuperiorViewAllViewModel>()).Concat(tempShiftChangeList ?? Enumerable.Empty<ShiftChangeSuperiorViewAllViewModel>()); //amending `<string>` to the appropriate type

                }
                AllMyEmployeeShiftChanges = AllMyEmployeeShiftChanges.OrderByDescending(x => x.ShiftChange_ID).ToList();

                #region jQuery_DataTable_Ajax_Code
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    AllMyEmployeeShiftChanges = AllMyEmployeeShiftChanges.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection).ToList();
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    AllMyEmployeeShiftChanges = AllMyEmployeeShiftChanges.Where(m => m.FullName.Contains(searchValue) || m.CreateDate.Contains(searchValue) || m.shiftDate.Contains(searchValue)
                    ).ToList();

                }
                recordsTotal = AllMyEmployeeShiftChanges.Count();
                var data = AllMyEmployeeShiftChanges.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                #endregion
                return new JsonResult(jsonData);


            }
            catch (Exception err)
            {

                throw;
            }
            //return PartialView(AllMyEmployeeShiftChanges.Take(100));
        }

        /// <summary>
        /// متد بازگشتی جهت بدست آوردن لیست تمامی نقش های اولاد یک نقش
        /// </summary>
        /// <param name="role">نقش جاری</param>
        /// <param name="childrenRoleList">لیست نقش های اولاد</param>
        /// <returns>لیست نقش های اولاد آپدیت شده</returns>
        [NonAction]
        public async Task<List<ApplicationRole>> getChildrenRoles(ApplicationRole role, List<ApplicationRole> childrenRoleList)
        {
            var tempList = await _ApplicationRoleRepository.GetAllAsync(x => x.ParentRoleId == role.Id || x.SendFinalSmsToUser == role.Id);

            //foreach (var item in tempList)
            //{
            //    var result = await getChildrenRoles(item, childrenRoleList);

            //}
            childrenRoleList.AddRange(tempList);
            return childrenRoleList;
        }

        [DisplayName("ویرایش تعویض شیفت")]
        public async Task<IActionResult> Edit(int? id)
        {
            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.OfficeUnit),
                where: x => x.UserIdentity_Id == user.Id
                );
            if (user != null && employee.FirstOrDefault() != null)
            {
                ViewBag.FullName = user.FullName;
                ViewBag.PersonalCode = employee.FirstOrDefault().PersonalCode;
            }
           

            var shiftChange = await _ShiftChangeRepository.GetWithIncludeAsync(
                selector: x => x,
                where: x => x.ShiftChange_ID == id,
                include: x => x.Include(s => s.Employee_Shc_1).ThenInclude(s => s.ApplicationUser).Include(s => s.Employee_Shc_2).ThenInclude(s => s.ApplicationUser));

            ViewBag.HasAlternate = shiftChange.FirstOrDefault().Employee_Shc_1.HasAlternateEmployee.Value;

            var shiftChangeSignatureList = await _ShiftChangeSignatureRepository.GetAllAsync(x => x.ShiftChange_ID == id, orderBy: x => x.OrderBy(s => s.ShiftChangeSignature_ID));
            //ViewBag.firstVSStatus = shiftChangeSignatureList.FirstOrDefault().SignatureStatus;
            ShiftChangeSuperiorViewModel scsvm = new ShiftChangeSuperiorViewModel();
            scsvm.shiftChange = shiftChange.FirstOrDefault();
            var shiftChangeSignatures = await _ShiftChangeSignatureRepository.GetWithIncludeAsync(
                selector: x => x,
                where: x => x.ShiftChange_ID == id,
                include: x => x.Include(s => s.applicationUser),
                orderBy: x => x.OrderBy(s => s.ShiftChangeSignature_ID)
                );
            List<ShiftChangeSignature> VSList = shiftChangeSignatures.OrderByDescending(x => x.ShiftChangeSignature_ID).ToList();

            scsvm.ThisUserCanSignature = true;
            foreach (var item in VSList)
            {
                if (item.applicationUser.Id == user.Id)
                {
                    scsvm.ThisUserCanSignature = (item.SignatureStatus == null ? true : item.SignatureStatus);
                }
            }

            scsvm.shiftChangeSignatures = shiftChangeSignatures.ToList();
            scsvm.CurrentSuperiorShiftChangeSignatures = shiftChangeSignatures.FirstOrDefault(x => x.PersonalCodeSignaturer == user.Id);

            #region برای مدیر پشتیبانی
            //بدست آوردن لیست  نام نقش های من
            var myRole = await _userManager.GetRolesAsync(user);
            //حذف نقش کارمند از لیست نقش های من
            myRole.Remove("Employee");
            //بدست آوردن نقش من
            var ThisUserRole = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());


            var RequesterUser = await _userRepository.GetAsync(x => x.Id == shiftChange.FirstOrDefault().Employee_Shc_1.ApplicationUser.Id);
            //بدست آوردن لیست  نام نقش های من
            var RequesterRole = await _userManager.GetRolesAsync(RequesterUser);
            //حذف نقش کارمند از لیست نقش های من
            RequesterRole.Remove("Employee");
            //بدست آوردن نقش من
            var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == RequesterRole.FirstOrDefault());
            if (mr.IsAgentOfManager && mr.SendFinalSmsToUser == "116ce37b-efd3-4bb8-a42b-9027e85c7dd6" && shiftChange.FirstOrDefault().FinalStatus != true && ThisUserRole.Id == "a2c5ff4a-64b0-4381-9cc2-e65596ebb271")//اگر نقش کاربر درخواست دهنده امکان مدیر پشتیبانی را فعال کرده باشد
            {
                ViewBag.IsAgentOfManager = true;
            }
            #endregion

            return PartialView(scsvm);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ShiftChangeSignature shiftChangeSignature)
        {
            //ذخیره سازی امضای مافوق کنونی
            var user = await _userManager.GetUserAsync(User);
            shiftChangeSignature.SignatureDateTime = DateTime.Now;
            shiftChangeSignature.PersonalCodeSignaturer = user.Id;
            _ShiftChangeSignatureRepository.Update(shiftChangeSignature);
            await _ShiftChangeSignatureRepository.SaveChangesAsync();

            var shiftChange = await _ShiftChangeRepository.GetWithIncludeAsync(
                selector: x => x,
                where: x => x.ShiftChange_ID == shiftChangeSignature.ShiftChange_ID,
             include: x => x.Include(s => s.Employee_Shc_1).ThenInclude(s => s.ApplicationUser));

            if (shiftChangeSignature.SignatureStatus == false)
            {
                shiftChange.FirstOrDefault().FinalStatus = shiftChangeSignature.SignatureStatus.Value;
                _ShiftChangeRepository.Update(shiftChange.FirstOrDefault());
                await _ShiftChangeRepository.SaveChangesAsync();

                string shiftChangeStatusString = "";
                if (shiftChangeSignature.SignatureStatus.Value)
                {
                    shiftChangeStatusString = "تایید";
                }
                else
                {
                    shiftChangeStatusString = "رد";
                }
                //ارسال پیامک نتیجه درخواست تعویض شیفت به کارمند درخواست کننده
                await _smsSender.SendfinalShiftChangeAcceptSMSAsync(shiftChange.FirstOrDefault().Employee_Shc_1.ApplicationUser.Mobile_1, shiftChange.FirstOrDefault().Employee_Shc_1.ApplicationUser.FullName, shiftChangeSignature.SignatureDateTime.ToShamsiWithTime(), shiftChangeStatusString);
                //ارسال پیامک نتیجه درخواست تعویض شیفت به مدیر منابع انسانی
                //await _smsSender.finalShiftChangeAcceptHRManagerSMSAsync(parentUsers.FirstOrDefault().Mobile_1, shiftChange2.Employee_V_1.FullName, shiftChangeSignature.SignatureDateTime.ToShamsiWithTime(), shiftChangeStatusString);
            }

            var myRole = await _userManager.GetRolesAsync(user);
            myRole.Remove("Employee");
            var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());
            var parentRole = await _ApplicationRoleRepository.GetAsync(x => x.Id == mr.ParentRoleId);
            if (parentRole != null)
            {
                var parentUsers = await _userManager.GetUsersInRoleAsync(parentRole.Name);
                //تا اینجا
                // بررسی شود که آیا باید به مافوق بعدی پیامک بره یا اینکه به سطح مدیر پروژه رسیدیم و باید وضعیت خود مرخصی مشخص بشه و پیامک نتیجه نهایی به کارمند ارسال بشه

                //بررسی شود که آیا باید به مافوق بعدی پیامک برود
                if (mr.SendSmsToParent)
                {
                    string userMobile = parentUsers.FirstOrDefault().Mobile_1;
                    string userName = shiftChange.FirstOrDefault().Employee_Shc_1.ApplicationUser.FullName;

                    await _smsSender.SendSubmitShiftChangeMessageSMSAsync(userMobile, userName, shiftChange.FirstOrDefault().shiftDate);
                }
            }
            //بدست آوردن نقش کارمند درخواست کننده مرخصی
            var shiftChangeRequesterRole = await _userManager.GetRolesAsync(shiftChange.FirstOrDefault().Employee_Shc_1.ApplicationUser);
            shiftChangeRequesterRole.Remove("کارمند");
            var vrr = await _ApplicationRoleRepository.GetAsync(x => x.Name == shiftChangeRequesterRole.FirstOrDefault());
            //اگر شناسه نقشی که وضعیت نهایی مرخصی را مشخص می کند با شناسه مافوق جاری یکسان باشد
            //یا برای نقش درخواست دهنده امکان تایید توسط مدیر پشتیبانی فعال بوده و کاربر جاری مدیر پشتیبانی باشد
            if (vrr.SendFinalSmsToUser == mr.Id || (vrr.IsAgentOfManager && mr.Id == "a2c5ff4a-64b0-4381-9cc2-e65596ebb271"))
            {
                shiftChange.FirstOrDefault().FinalStatus = shiftChangeSignature.SignatureStatus.Value;
                _ShiftChangeRepository.Update(shiftChange.FirstOrDefault());
                await _ShiftChangeRepository.SaveChangesAsync();

                string shiftChangeStatusString = "";
                if (shiftChangeSignature.SignatureStatus.Value)
                {
                    shiftChangeStatusString = "تایید";
                }
                else
                {
                    shiftChangeStatusString = "رد";
                }
                //ارسال پیامک نتیجه درخواست مرخصی به کارمند درخواست کننده
                await _smsSender.SendfinalShiftChangeAcceptSMSAsync(shiftChange.FirstOrDefault().Employee_Shc_1.ApplicationUser.Mobile_1, shiftChange.FirstOrDefault().Employee_Shc_1.ApplicationUser.FullName, shiftChangeSignature.SignatureDateTime.ToShamsiWithTime(), shiftChangeStatusString);
                //ارسال پیامک نتیجه درخواست به مدیر تایید کننده نهایی که فعلا نیازی نیست چون خودش میدونه چه کرده
                //await _smsSender.finalShiftChangeAcceptHRManagerSMSAsync(parentUsers.FirstOrDefault().Mobile_1, shiftChange.FirstOrDefault().Employee_Shc_1.ApplicationUser.FullName, shiftChangeSignature.SignatureDateTime.ToShamsiWithTime(), shiftChangeSignature.SignatureStatus.Value);
            }
            return Json(new { status = "ok" });
        }



    }
}
