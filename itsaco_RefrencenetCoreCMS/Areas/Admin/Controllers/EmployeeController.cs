﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.BasicInformation;
using DomainClasses.Employees;
using DomainClasses.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;
using Utilities;
using ViewModels;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]
    [DisplayName("کارکنان ")]

    public class EmployeeController : Controller
    {
        private UserManager<ApplicationUser> _userManager;
        public IGenericRepository<Employee> _employeeRepository;
        public IGenericRepository<EmployeeSalary> _employeeSalaryRepository;
        public IGenericRepository<OfficeUnit> _officeUnitRepository;
        public IGenericRepository<OfficePost> _officePostRepository;
        public readonly IGenericRepository<ApplicationUser> _user_genericRepository;
        public EmployeeController(IGenericRepository<Employee> employeeRepository, IGenericRepository<EmployeeSalary> employeeSalaryRepository, UserManager<ApplicationUser> userManager, IGenericRepository<OfficeUnit> officeUnitRepository, IGenericRepository<OfficePost> officePostRepository, IGenericRepository<ApplicationUser> user_genericRepository)
        {
            _employeeRepository = employeeRepository;
            _userManager = userManager;
            _officeUnitRepository = officeUnitRepository;
            _officePostRepository = officePostRepository;
            _user_genericRepository = user_genericRepository;
            _employeeSalaryRepository = employeeSalaryRepository;
        }

        public IActionResult Index()
        {
            return View();
        }
        [DisplayName("لیست کارکنان")]
        public async Task<IActionResult> ViewAll()
        {
            var employ = await _employeeRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.ApplicationUser).Include(s => s.OfficePost).Include(s => s.OfficeUnit)
                );

            var evm =new List<EmployeeViewModel>() ;
            foreach(var item in employ)
            {
                var rn = await _userManager.GetRolesAsync(item.ApplicationUser);
                rn.Remove("Employee");
                rn.Remove("SubAdmin");
                rn.Remove("Admin");
                rn.Remove("Customer");
                var r = new EmployeeViewModel { eemployee = item, RolesName = rn.FirstOrDefault()};
                evm.Add(r);
            }
            return PartialView(evm.AsEnumerable());
        }
        [DisplayName("افزودن کارمند")]
        public async Task<IActionResult> CreateEmployee()
        {
            ViewBag.AllUnits = new SelectList(await _officeUnitRepository.GetAllAsync(), "OfficeUnitID", "OfficeUnitTitle");
            ViewBag.AllPosts = new SelectList(await _officePostRepository.GetAllAsync(), "OfficePostID", "OfficePostTitle");
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateEmployee(EmployeeVM employee, IFormFile imgUser,IFormFile respons)
        {
            if (!Directory.Exists("wwwroot/Files/Employee"))
            {
                Directory.CreateDirectory("wwwroot/Files/Employee");
            }
            if (!Directory.Exists("wwwroot/Files/Employee/Thumb"))
            {
                Directory.CreateDirectory("wwwroot/Files/Employee/Thumb");
            }
            if (!Directory.Exists("wwwroot/Files/Employee/Responsibilty"))
            {
                Directory.CreateDirectory("wwwroot/Files/Employee/Responsibilty");
            }

            try
            {
                if (employee != null)
                {
                    if (imgUser != null)
                    {
                        employee.PersonPic = imgUser.FileName.Substring(0, imgUser.FileName.IndexOf(".")) + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + System.IO.Path.GetExtension(imgUser.FileName);
                        string path = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Employee/", employee.PersonPic);
                        string pathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Employee/Thumb", employee.PersonPic);

                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            await imgUser.CopyToAsync(stream);
                        }

                        ImageManipulate img = new ImageManipulate();
                        img.resizeImage2(path, pathThumb, 200, 150);
                        System.IO.File.Delete(path);
                    }
                    if (respons != null)
                    {
                        employee.Responsiblity = respons.FileName.Substring(0, respons.FileName.IndexOf(".")) + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + System.IO.Path.GetExtension(respons.FileName);
                        string path = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Employee/Responsibilty", employee.Responsiblity);

                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            await respons.CopyToAsync(stream);
                        }
                    }

                    var user = new ApplicationUser
                    {
                        FullName = employee.FullName,
                        UserName = employee.PersonalCode,
                        UserPhoto = employee.PersonPic,
                        Mobile_1 = employee.Mobile,
                        PhoneNumber = employee.Tel,
                        SecurityStamp = Guid.NewGuid().ToString()
                    };
                    var result = await _userManager.CreateAsync(user, employee.PersonPassword);
                    if (result.Succeeded)
                    {
                        await _userManager.AddToRoleAsync(user, "Employee");
                        var thisUser = await _userManager.GetUserIdAsync(user);
                        Employee em = new Employee()
                        {
                            UserIdentity_Id = thisUser,
                            PersonalCode = employee.PersonalCode,
                            NationalCode = employee.NationalCode,
                            OfficeUnitId = employee.OfficeUnitId,
                            OfficePostId = employee.OfficePostId,
                            IsActive = employee.IsActive,
                            responsibility = employee.Responsiblity,
                            HasShift = employee.HasShift,
                            ShiftName = employee.ShiftName,
                            HasAlternateEmployee = employee.HasAlternateEmployee,
                            SupervisionLevel = employee.SupervisionLevel,
                            Job = employee.Job
                        };
                        await _employeeRepository.AddAsync(em);
                        await _employeeRepository.SaveChangesAsync();
                        return Json("ok");
                    }
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("All", error.Description);
                        if (error.Code == "DuplicateUserName")
                        {
                            return Json("repeate");
                        }
                        if(error.Code== "PasswordTooShort")
                        {
                            return Json("pass_short");
                        }
                    }
                }

            }
            catch (Exception err)
            {
                return Json(err.Message);
                throw;
            }
            return Json("nall");
        }


        [DisplayName("ویرایش کارمند")]
        public async Task<IActionResult> EditEmployee(string id)
        {
            var employe = await _employeeRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.OfficeUnit).Include(s => s.OfficePost).Include(s => s.ApplicationUser),
                where: x => x.PersonalCode == id
                );
            ViewBag.AllUnits = new SelectList(await _officeUnitRepository.GetAllAsync(), "OfficeUnitID", "OfficeUnitTitle", employe.FirstOrDefault().OfficeUnitId);
            ViewBag.AllPosts = new SelectList(await _officePostRepository.GetAllAsync(), "OfficePostID", "OfficePostTitle", employe.FirstOrDefault().OfficePostId);

            EmployeeEditVM emVM = new EmployeeEditVM()
            {
                FullName = employe.FirstOrDefault().ApplicationUser.FullName,
                Mobile = employe.FirstOrDefault().ApplicationUser.Mobile_1,
                Tel = employe.FirstOrDefault().ApplicationUser.PhoneNumber,
                // PersonPassword = employe.FirstOrDefault().ApplicationUser.PasswordHash,
                //PersonRePassword = employe.FirstOrDefault().ApplicationUser.PasswordHash,
                PersonPic = employe.FirstOrDefault().ApplicationUser.UserPhoto,
                PersonalCode = employe.FirstOrDefault().PersonalCode,
                UserIdentity_Id = employe.FirstOrDefault().UserIdentity_Id,
                NationalCode = employe.FirstOrDefault().NationalCode,
                OfficePostId = employe.FirstOrDefault().OfficePostId,
                OfficeUnitId = employe.FirstOrDefault().OfficeUnitId,
                IsActive = employe.FirstOrDefault().IsActive,
                Responsiblity = employe.FirstOrDefault().responsibility,
                ShiftName = (employe.FirstOrDefault().ShiftName == null ? "A" : employe.FirstOrDefault().ShiftName),
                HasShift = (employe.FirstOrDefault().HasShift == null ? false: (bool)employe.FirstOrDefault().HasShift),
                HasAlternateEmployee = (employe.FirstOrDefault().HasAlternateEmployee == null ? false: (bool)employe.FirstOrDefault().HasAlternateEmployee),
                SupervisionLevel=employe.FirstOrDefault().SupervisionLevel,
                Job=employe.FirstOrDefault().Job
                
            };
            return View(emVM);
        }
        [HttpPost]
        public async Task<IActionResult> EditEmployee(EmployeeEditVM employee, IFormFile imgUser, IFormFile respons)
        {
            try
            {
                if (employee != null)
                {
                    if (imgUser != null)  //اگر عکس جدید آپلود شده باشد
                    {
                        if (employee.PersonPic != null)   //اگر قبلا کاربر عکس داشته آن عکس حذف می شود
                        {
                            string oldImage = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Employee", employee.PersonPic);
                            string oldImageThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Employee/Thumb", employee.PersonPic);
                            await DeleteFilesInProj2(oldImage, oldImageThumb);
                        }
                       
                        //فرایند ذخیره سازی عکس جدید
                        employee.PersonPic = imgUser.FileName.Substring(0, imgUser.FileName.IndexOf(".")) + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + System.IO.Path.GetExtension(imgUser.FileName);
                        string path = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Employee/", employee.PersonPic);
                        string pathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Employee/Thumb", employee.PersonPic);

                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            await imgUser.CopyToAsync(stream);
                        }

                        ImageManipulate img = new ImageManipulate();
                        img.resizeImage2(path, pathThumb, 200, 150);
                        System.IO.File.Delete(path);
                    }
                    if (respons != null)
                    {
                        if (employee.Responsiblity != null)
                        {
                            string oldFile = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Employee/Responsibilty", employee.Responsiblity);
                            if (System.IO.File.Exists(oldFile))
                            {
                                System.IO.File.Delete(oldFile);
                            }
                        }

                        employee.Responsiblity = respons.FileName.Substring(0, respons.FileName.IndexOf(".")) + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + System.IO.Path.GetExtension(respons.FileName);
                        string path = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Employee/Responsibilty/", employee.Responsiblity);

                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            await respons.CopyToAsync(stream);
                        }
                    }
                    //گرفتن اطلاعات کاربر از جداول خود دات نت کور
                    var users = await _user_genericRepository.GetWithIncludeAsync(
                        selector:f=>f,
                        where:f => f.Id == employee.UserIdentity_Id,
                        include: s => s.Include(f => f.Employee)
                    );

                    var user = users.FirstOrDefault();

                    user.FullName = employee.FullName;
                    user.UserName = employee.PersonalCode;
                    user.UserPhoto = employee.PersonPic;
                    user.Mobile_1 = employee.Mobile;
                    user.PhoneNumber = employee.Tel;
                    user.Employee.PersonalCode = employee.PersonalCode;
                    user.Employee.NationalCode = employee.NationalCode;
                    user.Employee.OfficeUnitId = employee.OfficeUnitId;
                    user.Employee.OfficePostId = employee.OfficePostId;
                    user.Employee.IsActive = employee.IsActive;
                    user.Employee.responsibility = employee.Responsiblity;
                    user.Employee.HasShift = employee.HasShift;
                    user.Employee.HasAlternateEmployee = employee.HasAlternateEmployee;
                    user.Employee.ShiftName = employee.ShiftName;
                    user.Employee.SupervisionLevel = employee.SupervisionLevel;
                    user.Employee.Job = employee.Job;
                    
                    //if (employee.PersonPassword != null)         //اگر کاربر پسورد جدید وارد کرده بود
                    //{                                              //ابتدا پسورد کاربر خالی می شود
                    //    user.PasswordHash = null;
                    //}
                 
                    var result = await _userManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        //var thisUser = await _userManager.GetUserIdAsync(user);
                        //await _userManager.AddPasswordAsync(user, employee.PersonPassword);           //اینجا با این تابع پسورد جدید جای گذاری می شود
                        _employeeRepository.Update(user.Employee);
                        await _employeeRepository.SaveChangesAsync();
                        return Json("ok");
                    }
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("All", error.Description);
                        if (error.Code == "DuplicateUserName")
                        {
                            return Json("repeate");
                        }
                    }
                }

            }
            catch (Exception err)
            {
                return Json(err.Message);
                throw;
            }
            return Json("nall");
        }
        [DisplayName("حذف")]
        public async Task<IActionResult> Delete(string id)
        {
            try
            {
                var employee = await _employeeRepository.GetWithIncludeAsync(selector: x => x,
                include: x => x.Include(s => s.ApplicationUser),
                where: x => x.PersonalCode == id);


                if (employee.FirstOrDefault() != null)
                {
                    if(!await _employeeSalaryRepository.Exists(x => x.PersonalCode == employee.FirstOrDefault().PersonalCode))
                    {
                        //ApplicationUser thisUser = new ApplicationUser() { Id = employee.FirstOrDefault().UserIdentity_Id };
                        if (employee.FirstOrDefault().ApplicationUser.UserPhoto != null)
                        {
                            string oldImage = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Employee", employee.FirstOrDefault().ApplicationUser.UserPhoto);
                            string oldImageThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Employee/Thumb", employee.FirstOrDefault().ApplicationUser.UserPhoto);
                            await DeleteFilesInProj2(oldImage, oldImageThumb);
                        }
                        _employeeRepository.Delete(employee.FirstOrDefault());
                        await _employeeRepository.SaveChangesAsync();

                        //_user_genericRepository.Detach(thisUser);
                        //await _employeeRepository.SaveChangesAsync();
                        await _userManager.DeleteAsync(employee.FirstOrDefault().ApplicationUser);
                        return Json("ok");
                    }
                    return Json("emp_salary");
                }
            }
            catch (Exception err)
            {

                throw;
            }
            return Json("Nok");
        }
        [NonAction]
        public async Task<IActionResult> DeleteFilesInProj2(string path, string path2)
        {
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            if (System.IO.File.Exists(path2))
            {
                System.IO.File.Delete(path2);
            }
            return Json(Ok());
        }
    }
}