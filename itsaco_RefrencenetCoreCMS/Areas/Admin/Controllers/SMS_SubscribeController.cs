﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.SMS_Subscription;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Services.Repositories;
using Utilities;

namespace CodeNagarProject.Areas.Admin.Controllers
{
    [Area("admin")]
       [Authorize]
       [DisplayName("اشتراک پیامکی")]
    public class SMS_SubscribeController : Controller
    {
        public List<SelectListItem> mobileOfMembersList;
        IGenericRepository<Persons> _sMSSubscribeRepository;

        public SMS_SubscribeController(IGenericRepository<Persons> sMSSubscribeRepository)
        {
            _sMSSubscribeRepository = sMSSubscribeRepository;
        }
        [DisplayName("صفحه نخست")]

        public IActionResult Index()
        {
            return View();
        }
        public async Task<JsonResult> ViewAllMemberOfSms()
        {
            mobileOfMembersList = new List<SelectListItem>();

            foreach (var item in await _sMSSubscribeRepository.GetAllAsync())
            {
                mobileOfMembersList.Add(new SelectListItem
                {
                    Text = item.MobileNumber,
                    Value = item.ID.ToString()
                });
            }
            return Json(mobileOfMembersList);
        }
        [HttpPost]
        public async Task<IActionResult> SendSMS(string mobiles, string userName, string password, string message, string from)
        {
            try
            {
                SendMultiSMS sms = new SendMultiSMS();
                mobiles = mobiles.Remove(mobiles.LastIndexOf(";"), 1);
                string[] mobile = mobiles.Split(";");
                sms.Config(mobile, userName, password, message, from);
                return Json("ok");
            }
            catch (Exception)
            {
                return Json("nok");
            }
        }
    }
}