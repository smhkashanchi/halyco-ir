﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Seo;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories;

namespace CodeNagarProject.Areas.Admin.Controllers
{
    [DisplayName("سئو")]
    [Area("admin")]
    [Authorize]
    public class SeoPageController : Controller
    {
        public static int PageId = 0;
        public static byte PageType = 0;
        public IGenericRepository<MainPageSeo> _seoPageRepository;
        public SeoPageController(IGenericRepository<MainPageSeo> seoPageRepository)
        {
            _seoPageRepository = seoPageRepository;
        }
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> ViewAll()
        {
            var pages = await _seoPageRepository.GetAllAsync();
            return PartialView(pages);
        }
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(MainPageSeo seoPage)
        {
            if (seoPage != null)
            {
                //if (!_seoPageRepository.IsExist(seoPage.UrlPage))
                //{
                try
                {
                    seoPage.PageId = PageId;
                    seoPage.PageType = PageType;
                    await _seoPageRepository.AddAsync(seoPage);
                    await _seoPageRepository.SaveChangesAsync();
                    return Json(seoPage);
                }
                catch (Exception err)
                {

                    throw;
                }
                //}
                //return Json("repeate");
            }
            return Json("nall");
        }
        public async Task<IActionResult> Edit(int id, byte type)
        {
            var seoPage = await _seoPageRepository.GetAsync(x => x.PageId == id && x.PageType == PageType);
            return View(seoPage);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(MainPageSeo seoPage)
        {
            try
            {
                if (seoPage.MetaDescription == null && seoPage.MetaTitle == null && seoPage.MetaOther == null && seoPage.MetaKeyword == null)
                {
                    var thisseopage = await _seoPageRepository.GetAsync(x => x.PageId == PageId && x.PageType == PageType);
                    _seoPageRepository.Delete(thisseopage);
                    await _seoPageRepository.SaveChangesAsync();
                    return Json("nall");
                }
                seoPage.PageId = PageId;
                seoPage.PageType = PageType;
                _seoPageRepository.Update(seoPage);
                await _seoPageRepository.SaveChangesAsync();
                return Json(seoPage);
            }
            catch (Exception err)
            {

                throw;
            }
        }
        public async Task<IActionResult> checkboxSelected(string values)
        {
            if (values != "")
            {
                string[] splitedValues = values.Split('&');
                for (int i = 0; i < splitedValues.Length - 1; i++)
                {
                    _seoPageRepository.DeleteById(Int32.Parse(splitedValues[i]));
                    await _seoPageRepository.SaveChangesAsync();
                }
                return Json("ok");
            }
            return Json("nall");
        }
        public async Task<IActionResult> CreateOrEditContentsSeo(int id, byte type)
        {
            PageId = id;
            PageType = type;
            var seo = await _seoPageRepository.GetAsync(s => s.PageId == id && s.PageType == type);
            if (seo == null) //اگر داخل این جدول کلمه کلیدی برای این مطلب ثبت نشده بود
            {
                //add
                return Json("add");
            }
            //Update
            return Json("update");
        }
    }
}