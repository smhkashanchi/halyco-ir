﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Catalog;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories;

namespace CodeNagarProject.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]
    [DisplayName("گاتالوک ")]

    public class CatalogController : Controller
    {
        public IGenericRepository<Catalog> _catalogRepository;
        public CatalogController(IGenericRepository<Catalog> catalogRepository)
        {
            _catalogRepository = catalogRepository;
        }
        [DisplayName("صفحه نخست ")]

        public async Task<IActionResult> Index()
        {
            var catalog =await _catalogRepository.GetAllAsync();
            ViewBag.Catalog = catalog.OrderByDescending(x=>x.ID);
            return View();
        }
        [DisplayName("لیست کاتالوگ ها ")]

        public async Task<IActionResult> ViewAllCatalog()
        {
            var list = await _catalogRepository.GetAllAsync();
            return PartialView(list.OrderByDescending(x=>x.ID));
        }
        [DisplayName("افزودن کاتالوگ  ")]

        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Catalog catalog,IFormFile thisFile)
        {
            if (!Directory.Exists("wwwroot/Files/Catalog"))
            {
                Directory.CreateDirectory("wwwroot/Files/Catalog");
            }
            if (thisFile != null)
            {
                catalog.Files = thisFile.FileName.Substring(0, thisFile.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(thisFile.FileName);
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Catalog", catalog.Files);

                using (var stream = new FileStream(path, FileMode.Create))
                {
                    thisFile.CopyTo(stream);
                }
                await _catalogRepository.AddAsync(catalog);
                await _catalogRepository.SaveChangesAsync();
                return Json("ok");
            }
            return Json("nall");
        }
        [DisplayName("حذف کاتالوگ  ")]

        public async Task<IActionResult> Delete(byte id)
        {
            var catalog =await _catalogRepository.GetByIdAsync(id);
            if (catalog != null)
            {
                string photoPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Catalog", catalog.Files);
                await DeleteFilesInProj(photoPath);

                _catalogRepository.Delete(catalog);
                 await _catalogRepository.SaveChangesAsync();
                return Json(catalog);
            }
            return Json("nall");
        }

        public async Task<IActionResult> DeleteFilesInProj(string path)
        {
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }

            return Json(Ok());
        }

    }
}