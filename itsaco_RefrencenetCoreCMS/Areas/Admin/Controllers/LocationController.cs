﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Region;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Services.Repositories;

namespace AspCorePanelProject.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]
    [DisplayName("شهر و استان")]

    public class LocationController : Controller
    {
        public List<SelectListItem> countryList;
        public List<SelectListItem> stateList;
        public IGenericRepository<Country> _countryRepository;
        public IGenericRepository<State> _stateRepository;
        public IGenericRepository<City> _cityRepository;

        public LocationController(IGenericRepository<Country> countryRepository
            , IGenericRepository<State> stateRepository, IGenericRepository<City> cityRepository)
        {
            _countryRepository = countryRepository;
            _stateRepository = stateRepository;
            _cityRepository = cityRepository;
        }
        [DisplayName("صفحه نخست")]
        public IActionResult Index()
        {
            return View();
        }
        [DisplayName("لیست کشورها")]
        public async Task<IActionResult> ViewAll(string id)
        {
            return PartialView(await _countryRepository.GetAllAsync(x => x.Lang == id, x => x.OrderByDescending(s => s.Country_ID)));
        }
        [DisplayName("افزودن کشور")]
        public IActionResult CreateCountry()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateCountry(Country country)
        {
            if (country != null)
            {
                if (!await _countryRepository.Exists(x => x.CountryName == country.CountryName))
                {
                    await _countryRepository.AddAsync(country);
                    await _countryRepository.SaveChangesAsync();
                    return Json(country);
                }
                return Json("repeate");
            }
            return Json("nall");
        }
        [DisplayName("ویرایش کشور")]

        public async Task<IActionResult> EditCountry(int id)
        {
            return View(await _countryRepository.GetByIdAsync(id));
        }
        [HttpPost]
        public async Task<IActionResult> EditCountry(Country country)
        {
            if (country != null)
            {
                _countryRepository.Update(country);
                await _countryRepository.SaveChangesAsync();
                return Json(country);
            }
            return Json("nall");
        }
        [DisplayName("حذف کشور")]

        public async Task<IActionResult> DeleteCountry(int id)
        {
            var country = await _countryRepository.GetByIdAsync(id);
            if (country != null)
            {
                if (await _countryRepository.GetByIdAsync(country.Country_ID) != null)
                {
                    _countryRepository.Delete(country);
                    await _countryRepository.SaveChangesAsync();
                    return Json(country);
                }
                return Json("NOK");
            }
            return Json("Nok");
        }
        /////////////////////////////////////// بخش استان
        ///
        public async Task<JsonResult> GetAllCountry(string id)
        {
            countryList = new List<SelectListItem>();
            countryList.Add(new SelectListItem
            {
                Text = "انتخاب کنید ...",
                Value = "0"
            });
            foreach (var item in await _countryRepository.GetAllAsync(x => x.Lang == id, x => x.OrderByDescending(s => s.Country_ID)))
            {
                countryList.Add(new SelectListItem
                {
                    Text = item.CountryName,
                    Value = item.Country_ID.ToString()
                });
            }
            return Json(countryList);
        }
        [DisplayName("لیست استان ها")]

        public async Task<IActionResult> ViewAllState(int id)
        {
            return PartialView(await _stateRepository.GetAllAsync(x => x.CountryId == id, x => x.OrderByDescending(s => s.State_ID)));
        }
        [DisplayName("افزودن استان ")]

        public IActionResult CreateState()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateState(State state)
        {
            if (state != null)
            {
                if (!await _stateRepository.Exists(x => x.StateName.Equals(state.StateName)))
                {
                    await _stateRepository.AddAsync(state);
                    await _stateRepository.SaveChangesAsync();
                    return Json(state);
                }
                return Json("repeate");
            }
            return Json("nall");
        }
        [DisplayName("ویرایش استان ")]

        public async Task<IActionResult> EditState(int id, string lang)
        {
            var state = await _stateRepository.GetByIdAsync(id);
            ViewBag.allCountry = new SelectList(await _countryRepository.GetAllAsync(x => x.Lang == lang, x => x.OrderByDescending(s => s.Country_ID)), "Country_ID", "CountryName", state.CountryId);
            return View(state);
        }
        [HttpPost]
        public async Task<IActionResult> EditState(State state)
        {
            if (state != null)
            {
                _stateRepository.Update(state);
                await _stateRepository.SaveChangesAsync();
                return Json(state);
            }
            return Json("nall");
        }
        [DisplayName("حذف استان ")]

        public async Task<IActionResult> DeleteState(int id)
        {
            var state = await _stateRepository.GetByIdAsync(id);
            if (state != null)
            {
                if (await _cityRepository.GetAsync(x => x.State_ID == id) == null)
                {
                    _stateRepository.Delete(state);
                    await _stateRepository.SaveChangesAsync();
                    return Json(state);
                }
                return Json("NOK");
            }
            return Json("Nok");
        }

        /////////////////////////////////////// بخش شهرها
        ///
        ///
        public async Task<JsonResult> GetAllState(int id)
        {
            stateList = new List<SelectListItem>();
            stateList.Add(new SelectListItem
            {
                Text = "انتخاب کنید ...",
                Value = "0"
            });
            foreach (var item in await _stateRepository.GetAllAsync(x => x.CountryId == id, x => x.OrderByDescending(s => s.State_ID)))
            {
                stateList.Add(new SelectListItem
                {
                    Text = item.StateName,
                    Value = item.State_ID.ToString()
                });
            }
            return Json(stateList);
        }
        [DisplayName("لیست شهر ها")]

        public async Task<IActionResult> ViewAllCity(int id)
        {
            return PartialView(await _cityRepository.GetAllAsync(x => x.State_ID == id, x => x.OrderByDescending(s => s.CityName)));
        }
        [DisplayName("افزودن شهر ")]

        public IActionResult CreateCity()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateCity(City city)
        {
            if (city != null)
            {
                if (!await _cityRepository.Exists(x => x.CityName == city.CityName))
                {
                    await _cityRepository.AddAsync(city);
                    await _cityRepository.SaveChangesAsync();
                    return Json(city);
                }
                return Json("repeate");
            }
            return Json("nall");
        }
        [DisplayName("ویرایش شهر ")]

        public async Task<IActionResult> EditCity(int id)
        {
            var city = await _cityRepository.GetByIdAsync(id);
            ViewBag.allState = new SelectList(await _stateRepository.GetAllAsync(null, x => x.OrderByDescending(s => s.State_ID)), "State_ID", "StateName", city.State_ID);
            return View(city);
        }
        [HttpPost]
        public async Task<IActionResult> EditCity(City city)
        {
            if (city != null)
            {
                _cityRepository.Update(city);
                await _cityRepository.SaveChangesAsync();
                return Json(city);
            }
            return Json("nall");
        }
        [DisplayName("حذف شهر ")]

        public async Task<IActionResult> DeleteCity(int id)
        {
            var city = await _cityRepository.GetByIdAsync(id);
            if (city != null)
            {

                _cityRepository.Delete(city);
                await _cityRepository.SaveChangesAsync();
                return Json(new { cityName = city.CityName, cityId = city.City_ID, State_ID = city.State_ID });
            }
            return Json("Nok");
        }
    }
}