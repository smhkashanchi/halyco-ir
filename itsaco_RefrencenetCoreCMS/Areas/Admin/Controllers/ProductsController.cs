﻿using DomainClasses.Gallery;
using DomainClasses.Language;
using DomainClasses.Product;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Services;
using Services.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Utilities;
using ViewModels;

namespace AspCorePanelProject.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]
    [DisplayName("محصولات")]

    public class ProductsController : Controller
    {
        public static int ProductId = 0;
        public static int ProductIdForDictionary = 0;
        public List<SelectListItem> colorList;
        public List<SelectListItem> featureReplyList;
        public List<SelectListItem> advantageList;
        public IGenericRepository<ProductsGroup> _productsGroupRepository;
        public IGenericRepository<Off> _offRepository;
        public IGenericRepository<MainProduct> _mainProductsRepository;
        public IGenericRepository<ProductInfo> _productInfoRepository;
        public IGenericRepository<Gallery> _galleryRepository;
        public IGenericRepository<Unit> _unitRepository;
        public IGenericRepository<Language> _langRepository;
        public IGenericRepository<ProductColor> _productColorReporsitory;
        public IGenericRepository<Color> _colorRepository;
        public IGenericRepository<ProductFeature> _productFeaturesRepository;
        public IGenericRepository<Feature> _featureRepository;
        public IGenericRepository<FeatureReply> _featureReplyRepository;
        public IGenericRepository<ProductGaranty> _productGarntyRepository;
        public IGenericRepository<Garanty> _garantyRepository;
        public IGenericRepository<ProductAdvantage> _advantageRepository;
        public IGenericRepository<Producer> _producerRepository;
        public IGenericRepository<ProductComment> _productCommentRepository;
        public IGenericRepository<SimilarProduct> _similarProductsRepository;
        public IEmailSender _emailRepository;
        public ProductsController(IGenericRepository<ProductsGroup> productsGroupRepository, IGenericRepository<Off> offRepository, IGenericRepository<MainProduct> mainProductsRepository,
                                  IGenericRepository<Gallery> galleryRepository, IGenericRepository<Unit> unitRepository, IGenericRepository<Language> langRepository,
                                  IGenericRepository<ProductColor> productColorReporsitory, IGenericRepository<Color> colorRepository,
                                  IGenericRepository<ProductFeature> productFeaturesRepository,
                                  IGenericRepository<Feature> featureRepository,
                                  IGenericRepository<FeatureReply> featureReplyRepository,
                                  IGenericRepository<ProductGaranty> productGarntyRepository,
                                  IGenericRepository<Garanty> garantyRepository,
                                  IGenericRepository<ProductAdvantage> advantageRepository,
                                  IGenericRepository<Producer> producerRepository,
                                  IGenericRepository<ProductComment> productCommentRepository
                                  , IEmailSender emailRepository
                                   , IGenericRepository<SimilarProduct> similarProductsRepository,
                                  IGenericRepository<ProductInfo> productInfoRepository)
        {
            _productsGroupRepository = productsGroupRepository;
            _productInfoRepository = productInfoRepository;
            _offRepository = offRepository;
            _mainProductsRepository = mainProductsRepository;
            _galleryRepository = galleryRepository;
            _unitRepository = unitRepository;
            _langRepository = langRepository;
            _productColorReporsitory = productColorReporsitory;
            _colorRepository = colorRepository;
            _productFeaturesRepository = productFeaturesRepository;
            _featureRepository = featureRepository;
            _featureReplyRepository = featureReplyRepository;
            _productGarntyRepository = productGarntyRepository;
            _garantyRepository = garantyRepository;
            _advantageRepository = advantageRepository;
            _producerRepository = producerRepository;
            _productCommentRepository = productCommentRepository;
            _emailRepository = emailRepository;
            _similarProductsRepository = similarProductsRepository;
        }
        [DisplayName("صفحه نخست")]

        public IActionResult Index()
        {
            return View();
        }
        //public List<ChildNode> ViewInTreeViewChild(int GroupId)
        //{
        //    List<ChildNode> TChild=new List<ChildNode>(); ;
        //    foreach (var item2 in _productsGroupRepository.GetAllProductsGroupByParentId(GroupId))
        //    {
        //        TChild.Add(new ChildNode()
        //        {
        //            text = item2.ProductGroupTitle,
        //            href = "#" + item2.ProductGroupTitle,
        //            tags = item2.ProductGroup_ID.ToString() //برای ذخیره آی دی در اتریبیوت دیتا تایتل 
        //        });
        //    }
        //    return TChild;
        //}

        public async Task<string> createTreeView(short pgId, string lang)
        {
            var list = await ViewInTreeView(pgId, lang);
            return JsonConvert.SerializeObject(list);
        }
        [NonAction]
        public async Task<List<Node>> ViewInTreeView(short PGId, string lang)//گرفتن گروه مطالب بصورت جیسون و فرستادن به تابع ساخت تری ویو
        {
            List<Node> TNodes = new List<Node>();
            List<Node> TNodes2 = new List<Node>();

            var productGroupData = await _productsGroupRepository.GetByIdAsync(PGId);

            var groups = await _productsGroupRepository.GetAllAsync(x => x.ParentId == PGId);
            foreach (var item in groups)
            {
                var node = await ViewInTreeView(item.ProductGroup_ID, lang);
                TNodes2.Add(node.FirstOrDefault());
            }


            TNodes.Add(new Node()
            {
                text = productGroupData.ProductGroupTitle,
                href = "#" + productGroupData.ProductGroupTitle,
                tags = PGId.ToString(),
                nodes = TNodes2
            });

            return TNodes;
        }
        [DisplayName("افزودن گروه محصول")]

        public async Task<IActionResult> CreateProductsGroup(string id)
        {
            ViewBag.allOff = new SelectList(await _offRepository.GetAllAsync(x => x.Lang == id), "Off_ID", "OffTitle");
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateProductsGroup(ProductsGroup productsGroup, IFormFile pgImage)
        {
            if (!Directory.Exists("wwwroot/Files/ProductsGroup"))
            {
                Directory.CreateDirectory("wwwroot/Files/ProductsGroup");
            }
            if (!Directory.Exists("wwwroot/Files/ProductsGroup/Thumb"))
            {
                Directory.CreateDirectory("wwwroot/Files/ProductsGroup/Thumb");
            }
            if (productsGroup.ParentId == null)
            {
                return Json(new { res = false });
            }
            if (productsGroup != null)
            {
                if (pgImage != null)
                {
                    productsGroup.ProductGroupImage = pgImage.FileName.Substring(0, pgImage.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(pgImage.FileName);
                    string filePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/ProductsGroup", productsGroup.ProductGroupImage);
                    string filePathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/ProductsGroup/Thumb", productsGroup.ProductGroupImage);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await pgImage.CopyToAsync(stream);
                    }
                    //ImageManipulate img = new ImageManipulate();
                    //img.resizeImage(filePath, filePathThumb);

                    await _productsGroupRepository.AddAsync(productsGroup);
                    await _productsGroupRepository.SaveChangesAsync();
                    return Json(new { productsGroup=productsGroup,res=true });
                }
            }
            return View(productsGroup);
        }
        [DisplayName("نمایش جزئیات گروه")]


        public async Task<IActionResult> ViewProductGroup(short id)
        {
            return PartialView(await _productsGroupRepository.GetByIdAsync(id));
        }
        [DisplayName("ویرایش گروه محصول")]


        public async Task<IActionResult> EditProductsGroup(short id)
        {
            var group = await _productsGroupRepository.GetByIdAsync(id);
            if (group.OffId != null)
            {
                var off = await _offRepository.GetByIdAsync(group.OffId.Value);
                ViewBag.off = new SelectList(await _offRepository.GetAllAsync(x => x.Lang == group.Lang), "Off_ID", "OffTitle", off.Off_ID);
            }
            return View(group);
        }

        [HttpPost]
        public async Task<IActionResult> EditProductsGroup(ProductsGroup productsGroup, IFormFile pgImage)
        {
            if (productsGroup != null)
            {
                if (pgImage != null)
                {
                    string oldFilePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/ProductsGroup", productsGroup.ProductGroupImage);
                    string oldFilePathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/ProductsGroup/Thumb", productsGroup.ProductGroupImage);
                    await DeleteFilesInProj(oldFilePath, oldFilePathThumb);

                    productsGroup.ProductGroupImage = pgImage.FileName.Substring(0, pgImage.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(productsGroup.ProductGroupImage);
                    string filePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/ProductsGroup", productsGroup.ProductGroupImage);
                    string filePathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/ProductsGroup/Thumb", productsGroup.ProductGroupImage);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await pgImage.CopyToAsync(stream);
                    }
                    //ImageManipulate img = new ImageManipulate();
                    //img.resizeImage(filePath, filePathThumb);
                }
                _productsGroupRepository.Update(productsGroup);
                await _productsGroupRepository.SaveChangesAsync();
                return Json(productsGroup);

            }
            return View(productsGroup);
        }
        [DisplayName("حذف گروه محصول")]


        public async Task<IActionResult> DeleteProductsGroup(short id)
        {
            var group = await _productsGroupRepository.GetByIdAsync(id);
            if (group != null)
            {
                var list = await _productsGroupRepository.GetAllAsync(x => x.ParentId == group.ProductGroup_ID);
                if (list.Count()==0) //IsChilde
                {
                    var products =await _productInfoRepository.GetAllAsync(x => x.ProductGroupId == group.ProductGroup_ID);
                    if (list.Count()==0) //IsProduct
                    {
                        string filePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/ProductsGroup", group.ProductGroupImage);
                        string filePathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/ProductsGroup/Thumb", group.ProductGroupImage);
                        //await DeleteFilesInProj(filePath, filePathThumb);
                        if (System.IO.File.Exists(filePath))
                        {
                            System.IO.File.Delete(filePath);
                        }
                        _productsGroupRepository.Delete(group);
                        await _productsGroupRepository.SaveChangesAsync();
                        return Json(group);
                    }
                    return Json("isProduct");
                }
                return Json("NOK");
            }
            return Json(group);
        }
        [NonAction]
        public async Task<IActionResult> DeleteFilesInProj(string path, string pathThumb)
        {
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            if (System.IO.File.Exists(pathThumb))
            {
                System.IO.File.Delete(pathThumb);
            }
            return Json(Ok());
        }
        [NonAction]
        public async Task<IActionResult> DeleteFilesInProj2(string path)
        {
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            return Json(Ok());
        }
        ///////////////////////////////////////////////////// بخش محصولات
        ///
        [DisplayName("نمایش محصولات ")]

        public async Task<ActionResult> ViewAllProducts(int id)
        {
            var products = await _productInfoRepository.GetWithIncludeAsync<ProductInfo>
                (
                selector: x => x,
                include: s => s.Include(x => x.MainProduct).Include(x => x.ProductsGroup).Include(x => x.Off).Include(x=> x.Unit).Include(x => x.Producer),
                where: x => x.ProductGroupId == id,
                orderBy: x => x.OrderByDescending(a => a.ProductId)
                );
            if (products != null)
            {
                return PartialView(products);
            }
            return View(products);
        }
        [DisplayName("نمایش محصولات  برای بخش محصولات مشابه")]
        public async Task<ActionResult> ViewAllProductsForSimilar(int id)
        {
            var products = await _productInfoRepository.GetWithIncludeAsync<ProductInfo>
                (where: x=>x.ProductGroupId==id,
                include:x=>x.Include(s=>s.MainProduct).Include(s=>s.ProductsGroup),
                selector:x=>x);
            if (products != null)
            {
                return PartialView(products);
            }
            return View(products);
        }
        [DisplayName("افزودن محصول ")]
        public async Task<IActionResult> CreateMainProduct(string id)
        {
            ViewBag.allLang = new SelectList(await _langRepository.GetAllAsync(x=>x.IsActive), "Lang_ID", "Lang_Name");
            ViewBag.allGallery = new SelectList(await _galleryRepository.GetAllAsync(), "Gallery_ID", "GalleryTitle");
            return View();
        }
        public IActionResult GetProductImageSizesViewComponent(int id)
        { /// برای فراخوانی ویو کامپوننت در ویو بصورت آیجکس این متد نوشته شده
            return ViewComponent("GetProductImageSizes", new { Id = id });
        }
        [HttpPost]

        public async Task<IActionResult> CreateMainProduct(MainProductVM mainProduct, IFormFile imgProduct, IFormFile pCatalog, string id)
        {
            if (!Directory.Exists("wwwroot/Files/Products/Catalog"))
            {
                Directory.CreateDirectory("wwwroot/Files/Products/Catalog");
            }
            if (!Directory.Exists("wwwroot/Files/Products"))
            {
                Directory.CreateDirectory("wwwroot/Files/Products");
            }
            if (!Directory.Exists("wwwroot/Files/Products/Thumb"))
            {
                Directory.CreateDirectory("wwwroot/Files/Products/Thumb");
            }
            if (mainProduct != null)
            {
                if (imgProduct != null)
                {
                    if (pCatalog != null)
                    {
                        mainProduct.ProductCatalog = pCatalog.FileName.Substring(0, pCatalog.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(pCatalog.FileName);
                        string catalogpath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Products/Catalog", mainProduct.ProductCatalog);

                        using (var stream = new FileStream(catalogpath, FileMode.Create))
                        {
                            await pCatalog.CopyToAsync(stream);
                        }
                    }



                    mainProduct.ProductImage = imgProduct.FileName.Substring(0, imgProduct.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(imgProduct.FileName);
                    string path = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Products/", mainProduct.ProductImage);
                    string pathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Products/Thumb", mainProduct.ProductImage);


                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await imgProduct.CopyToAsync(stream);
                    }
                    ImageManipulate img = new ImageManipulate();
                    img.resizeImage2(path, pathThumb, 340, 120);

                    mainProduct.CreateDate = DateTime.Now;
                    MainProduct mp = new MainProduct();
                    mp.CreateDate = mainProduct.CreateDate;
                    mp.ExistCount = mainProduct.ExistCount;
                    mp.GalleryId = mainProduct.GalleryId;
                    mp.IsActive = mainProduct.IsActive;
                    mp.IsDecor = mainProduct.IsDecor;
                    mp.IsExsit = mainProduct.IsExsit;
                    mp.IsPopular = mainProduct.IsPopular;
                    mp.ProductCode = mainProduct.ProductCode;
                    mp.ProductImage = mainProduct.ProductImage;
                    mp.ProductCatalog = mainProduct.ProductCatalog;
                    mp.Product_ID = mainProduct.Product_ID;
                    mp.SaledCount = mainProduct.SaledCount;
                    await _mainProductsRepository.AddAsync(mp);
                    await _mainProductsRepository.SaveChangesAsync();
                    ProductId = mp.Product_ID;

                    return Json("ok");
                }
                return Json("noImage");
                //return View(mainProduct);
            }
            return View(mainProduct);
        }

        //public IActionResult resizeImage(int smallPicWidth,int smallPicHeight)
        //{
        //    ImageManipulate img = new ImageManipulate();
        //    img.resizeImage2(path, pathThumb, smallPicWidth, smallPicHeight);
        //}
        public async Task<IActionResult> CreateProduct(string id)
        {
            var units = await _unitRepository.GetAllAsync(x => x.Lang == id);
            ViewBag.allUnit = new SelectList(units, "Unit_ID", "UnitTitle");

            ViewBag.allOff = new SelectList(await _offRepository.GetAllAsync(x => x.Lang == id), "Off_ID", "OffTitle");
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateProduct(ProductInfo productInfo)
        {
            if (productInfo != null)
            {
                if (ProductId != 0)
                {
                    if (productInfo.UnitId == 0)
                        productInfo.UnitId = null;
                    if (productInfo.OffId == 0)
                        productInfo.OffId = null;
                    if (productInfo.ProducerId == 0)
                        productInfo.ProducerId = null;

                    productInfo.ProductId = ProductId;
                    await _productInfoRepository.AddAsync(productInfo);
                    await _mainProductsRepository.SaveChangesAsync();
                    return Json(productInfo);
                }
                return Json("NoId");
            }
            return Json("no");
        }
        [DisplayName("ویرایش محصول ")]

        public async Task<IActionResult> EditMainProduct(int id, string lang)
        {
            ViewBag.allLang = new SelectList(await _langRepository.GetAllAsync(x => x.IsActive), "Lang_ID", "Lang_Name");
            var main_product = await _productInfoRepository.GetWithIncludeAsync<ProductInfo>(
                 selector: x => x,
                where: x => x.ProductInfo_ID == id,
                include: x => x.Include(s => s.MainProduct).ThenInclude(s=>s.Gallery));

            var galleries = await _galleryRepository.GetAllAsync();
           /// ViewBag.allGallery = new SelectList(galleries, "Gallery_ID", "GalleryTitle", main_product.GalleryId);
            return View(main_product.FirstOrDefault());
        }
        [HttpPost]
        public async Task<IActionResult> EditMainProduct(MainProduct mainProduct, IFormFile imgProduct, IFormFile pCatalog)
        {
            if (mainProduct != null)
            {
                if (imgProduct != null)
                {
                    if (pCatalog != null)
                    {
                        if (mainProduct.ProductCatalog != null)
                        {
                            string oldcatalogpath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Products/Catalog", mainProduct.ProductCatalog);
                            await DeleteFilesInProj2(oldcatalogpath);
                        }

                        mainProduct.ProductCatalog = pCatalog.FileName.Substring(0, pCatalog.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(pCatalog.FileName);
                        string catalogpath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Products/Catalog", mainProduct.ProductCatalog);
                        using (var stream = new FileStream(catalogpath, FileMode.Create))
                        {
                            await pCatalog.CopyToAsync(stream);
                        }
                    }

                    if (mainProduct.ProductImage != null)
                    {
                        string oldpath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Products/", mainProduct.ProductImage);
                        string oldpathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Products/Thumb", mainProduct.ProductImage);
                        await DeleteFilesInProj(oldpath, oldpathThumb);
                    }


                    mainProduct.ProductImage = imgProduct.FileName.Substring(0, imgProduct.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(imgProduct.FileName);
                    string path = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Products/", mainProduct.ProductImage);
                    string pathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Products/Thumb", mainProduct.ProductImage);

                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await imgProduct.CopyToAsync(stream);
                    }
                    ImageManipulate img = new ImageManipulate();
                    img.resizeImage2(path, pathThumb, 340, 120);
                }
                _mainProductsRepository.Update(mainProduct);
                await _mainProductsRepository.SaveChangesAsync();
                return Json("ok");
            }
            return Json("no");
        }
        //public IActionResult EditProduct(string lang,int id)
        //{
        //    //var product = _mainProductsRepository.GetProductById(id);
        //    //var units = _unitRepository.GetAllUnitByLang(lang);
        //    //ViewBag.allUnit = new SelectList(units, "Unit_ID", "UnitTitle");
        //    ViewBag.allLang = new SelectList(_langRepository.GetAllLanguageIsActive(), "Lang_ID", "Lang_Name");
        //    //ViewBag.allOff = new SelectList(_offRepository.GetAllOffByLang(lang), "Off_ID", "OffTitle");
        //    return View();
        //}
        public async Task<IActionResult> EditProduct(int id, string lang)
        {
            //
            ProductIdForDictionary = id;
            if (ProductIdForDictionary != 0)
            {
                var products = await _productInfoRepository.GetWithIncludeAsync<ProductInfo>
                    (
                    selector: x => x,
                    include: x => x.Include(z => z.ProductsGroup).Include(c => c.MainProduct),
                    where: x => x.ProductId == id
                    );
                var products2 = products.ToDictionary(x => x.ProductsGroup.Lang, x => x.ProductInfo_ID);
                //var products = _mainProductsRepository.GetAllProductsByProductId(ProductIdForDictionary);
                var infoId = 0;
                ProductInfo product = null;
                if (products2.ContainsKey(lang))
                {
                    infoId = products2[lang];
                    var GetProductByLanguage = await _productInfoRepository.GetWithIncludeAsync<ProductInfo> ////GetProductByLanguage
                         (
                          selector: x => x,
                          include: x => x.Include(xx => xx.MainProduct).Include(xxx => xxx.ProductsGroup).Include(xxxx => xxxx.Off).Include(xxxxx => xxxxx.Unit).Include(xxxxxx => xxxxxx.Producer),
                          where: x => x.ProductInfo_ID == infoId && x.ProductsGroup.Lang == lang
                         );
                    product = GetProductByLanguage.FirstOrDefault();
                    ViewBag.InfoId = infoId;
                    ViewBag.GroupId = product.ProductGroupId;

                    var units = await _unitRepository.GetAllAsync(x => x.Lang == lang);
                    ViewBag.allUnit = new SelectList(units, "Unit_ID", "UnitTitle", product.UnitId);
                    var producer = await _producerRepository.GetAllAsync(x => x.Lang == lang);
                    ViewBag.allProducer = new SelectList(producer, "Producer_ID", "ProducerTitle", product.ProducerId);

                    ViewBag.allOff = new SelectList(await _offRepository.GetAllAsync(x => x.Lang == lang), "Off_ID", "OffTitle", product.OffId);
                    return View(product);
                }
                else
                {
                    ProductId = ProductIdForDictionary;
                    return Json("noSet");
                }
            }
            return Json("nall");

        }
        [HttpPost]
        public async Task<IActionResult> EditProduct(ProductInfo productInfo)
        {
            if (productInfo != null)
            {

                _productInfoRepository.Update(productInfo);
                await _mainProductsRepository.SaveChangesAsync();
                return Json(productInfo);

            }
            return Json("no");
        }
        [DisplayName("حذف محصول ")]

        public async Task<IActionResult> checkboxSelected(string values)
        {
            if (values != "")
            {
                string[] splitedValues = values.Split('&');
                for (int i = 0; i < splitedValues.Length - 1; i++)
                {
                    var productsWithId = await _productInfoRepository.GetWithIncludeAsync<ProductInfo>
                        (
                        selector: x => x,
                        include: x => x.Include(z => z.MainProduct),
                        where: x => x.ProductInfo_ID == Int32.Parse(splitedValues[i])
                        );

                    var product = productsWithId.FirstOrDefault();
                    var mainProductWithId =await _productInfoRepository.GetAllAsync(x => x.ProductId == product.ProductId);
                    if (mainProductWithId.Count()>=1)
                    {
                        if (product.MainProduct.ProductCatalog != null)
                        {
                            string oldcatalogpath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Products/Catalog", product.MainProduct.ProductCatalog);
                            await DeleteFilesInProj2(oldcatalogpath);
                        }
                        string path = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Products/", product.MainProduct.ProductImage);
                        string pathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Products/Thumb", product.MainProduct.ProductImage);

                        await DeleteFilesInProj(path, pathThumb);

                        int mainId = product.ProductId;
                        //MainProduct mp = new MainProduct() { Product_ID = product.ProductId };
                        _productInfoRepository.Delete(product);
                        _mainProductsRepository.DeleteById(mainId);
                    }
                    else
                    {
                        _productInfoRepository.Delete(product);
                    }
                    //_mainProductsRepository.DeleteProduct(Int32.Parse(splitedValues[i]));
                    await _mainProductsRepository.SaveChangesAsync();
                    return Json("ok");
                }
            }
            return Json(Ok());
        }

        /////////////////////////////// بخش رنگ محصولاتs
        ///
        [DisplayName("نمایش رنگ های محصول ")]
        public async Task<ActionResult> ViewAllProductColor(int id)
        {
            if (id != 0)
            {
                var productColors = await _productColorReporsitory.GetWithIncludeAsync<ProductColor>(where: x => x.ProductId == id,
                    selector: x => x,
                    include:x=>x.Include(s=>s.Color));

                var product = await _productInfoRepository.GetWithIncludeAsync<ProductInfo> //GetProductByInfoId
                       (
                       selector: x => x,
                       include: x => x.Include(z => z.MainProduct),
                       where: x => x.ProductInfo_ID ==id
                       );

                ViewBag.productTitle = product.FirstOrDefault().ProductTitle;
                ViewBag.productId = product.FirstOrDefault().ProductInfo_ID;
                return PartialView(productColors);
            }
            return Json("nall");
        }

        public async Task<ActionResult> GetAllColorForAdd(string id)
        {
            colorList = new List<SelectListItem>();
            //colorList.Add(new SelectListItem
            //{
            //    Text = "انتخاب کنید ...",
            //    Value = "0"
            //});
            foreach (var item in await _colorRepository.GetAllAsync(x => x.Lang == id))
            {
                colorList.Add(new SelectListItem
                {
                    Text = item.ColorTitle,
                    Value = item.Color_ID.ToString()
                });
            }
            return Json(colorList);
        }
        [DisplayName("افزودن رنگ به محصول ")]

        public async Task<IActionResult> CreatePoductColor(string id)
        {
            ViewBag.allColor = new SelectList(await _colorRepository.GetAllAsync(x => x.Lang == id), "Color_ID", "ColorTitle");
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreatePoductColor(ProductColor productColor)
        {
            if (productColor.ProductId != 0)
            {
                await _productColorReporsitory.AddAsync(productColor);
                await _productColorReporsitory.SaveChangesAsync();
                return Json(productColor);
            }
            return Json("nall");
        }
        [DisplayName("حذف رنگ های محصول ")]

        public async Task<IActionResult> checkboxSelectedProductColor(string values)
        {
            if (values != "")
            {
                string[] splitedValues = values.Split('&');
                for (int i = 0; i < splitedValues.Length - 1; i++)
                {

                    _productColorReporsitory.DeleteById(Int32.Parse(splitedValues[i]));
                    await _productColorReporsitory.SaveChangesAsync();
                }
                return Json("ok");
            }
            return Json("no");
        }


        //////////////////////////////////////// بخش ویژگی محصولات
        ///
        [DisplayName("نمایش ویرگی های محصول ")]

        public async Task<IActionResult> ViewAllProductFeaturesByProductId(int id)
        {
            var products = await _productInfoRepository.GetWithIncludeAsync<ProductInfo>(
                selector: x => x,
                where: x => x.ProductInfo_ID == id,
                include: x => x.Include(s => s.MainProduct)
                );

            var product = products.FirstOrDefault();
            ViewBag.productTitle = product.ProductTitle;
            ViewBag.productId = product.ProductInfo_ID;
            var productFeatures =await _productFeaturesRepository.GetWithIncludeAsync<ProductFeature>(
                where: x=>x.ProductInfoId==id,
                selector:x=>x,
                include:x=>x.Include(s=>s.FeatureReply).ThenInclude(s=>s.Feature).Include(s=>s.ProductInfo));

            return PartialView(productFeatures);
        }
        //[NonAction]

        //public async Task<IActionResult> CreateProductFeature(int id)
        //{
        //    var product = await _productInfoRepository.GetWithIncludeAsync<ProductInfo> //GetProductByInfoId
        //                (
        //                selector: x => x,
        //                include: x => x.Include(z => z.MainProduct),
        //                where: x => x.ProductInfo_ID == id
        //                );

        //    int groupId = product.FirstOrDefault().ProductGroupId.Value;
        //    ViewBag.features = new SelectList(await _featureRepository.GetAllAsync(x => x.ProductGroupId == id), "Feature_ID", "FeatureTitle");



        //    return View();
        //}

        //public async Task<ActionResult> GetAllFeatureReplyByFeatureId(int id)
        //{
        //    featureReplyList = new List<SelectListItem>();
        //    //featureReplyList.Add(new SelectListItem
        //    //{
        //    //    Text = "انتخاب کنید ...",
        //    //    Value = "0"
        //    //});
        //    foreach (var item in await _featureReplyRepository.GetAllAsync(x => x.FeatureId == id))
        //    {
        //        featureReplyList.Add(new SelectListItem
        //        {
        //            Text = item.FeatureReplyText,
        //            Value = item.FeatureReply_ID.ToString()
        //        });
        //    }
        //    return Json(featureReplyList);
        //}
        [DisplayName("افزودن ویرگی به محصول ")]
        [ActionName("CreateProductFeature")]
        public async Task<IActionResult> AddProductFeatures(int id)
        {
            var product = await _productInfoRepository.GetWithIncludeAsync<ProductInfo> //GetProductByInfoId
                        (
                        selector: x => x,
                        include: x => x.Include(z => z.MainProduct),
                        where: x => x.ProductInfo_ID == id
                        );

            var Features = await _featureRepository.GetWithIncludeAsync<Feature>(
                where: x => x.ProductGroupId == product.FirstOrDefault().ProductGroupId.Value,
                selector: x => x,
                include: x => x.Include(s => s.FeatureReplies));

            return PartialView("AddProductFeatures", Features);
        }

        [HttpPost]
        public async Task<IActionResult> CreateProductFeature(ProductFeature productFeature)
        {
            if (productFeature != null)
            {
                if (await _productFeaturesRepository.Exists(x => x.ProductInfoId == productFeature.ProductInfoId && x.FeatureReplyId == productFeature.FeatureReplyId))
                    return Json("repeate");

                await _productFeaturesRepository.AddAsync(productFeature);
                await _productFeaturesRepository.SaveChangesAsync();
                return Json(productFeature);
            }
            return Json("nall");
        }
        [DisplayName("حذف ویژگی های محصول ")]

        public async Task<IActionResult> checkboxSelectedProductFeature(string values, string featureReply)
        {
            if (values != "")
            {
                string[] splitedValues = values.Split('&');
                string[] splitedValuesFeature = featureReply.Split('&');
                for (int i = 0; i < splitedValues.Length - 1; i++)
                {
                    var productFeature = await _productFeaturesRepository.GetAsync(s => s.FeatureReplyId == Int32.Parse(splitedValuesFeature[i]) && s.ProductInfoId == Int32.Parse(splitedValues[i]));
                   _productFeaturesRepository.Delete(productFeature);
                    await _productFeaturesRepository.SaveChangesAsync();
                }
                return Json("ok");
            }
            return Json("no");
        }
        /////////////////////////////////////////////// بخش گارانتی محصولات
        ///
        [DisplayName("نمایش گارانتی های محصول ")]
        public async Task<ActionResult> ViewAllProductGaranty(int id)
        {
            var product = await _productInfoRepository.GetWithIncludeAsync<ProductInfo> //GetProductByInfoId
                       (
                       selector: x => x,
                       include: x => x.Include(z => z.MainProduct),
                       where: x => x.ProductInfo_ID == id
                       );
            ViewBag.productTitle = product.FirstOrDefault().ProductTitle;
            ViewBag.productId = product.FirstOrDefault().ProductInfo_ID;
            var productGaranty = await _productGarntyRepository.GetWithIncludeAsync<ProductGaranty>(
                where : x => x.ProductInfoId == id,
                selector:x=>x,
                include:x=>x.Include(s=>s.ProductInfo).Include(s=>s.Garanty));
            return PartialView(productGaranty);
        }
        [DisplayName("افزودن گارانتی به محصول ")]
        public async Task<IActionResult> CreateProductGaranty(string id)
        {
            ViewBag.allGaranty = new SelectList(await _garantyRepository.GetAllAsync(x => x.Lang == id), "Garanty_ID", "GarantyName");
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateProductGaranty(ProductGaranty productGaranty)
        {
            if (productGaranty != null)
            {
                await _productGarntyRepository.AddAsync(productGaranty);
                await _productGarntyRepository.SaveChangesAsync();
                return Json(productGaranty);
            }
            return Json("nall");
        }
        [DisplayName("حذف گارانتی های محصول ")]

        public async Task<IActionResult> checkboxSelectedProductGaranty(string values, string garantyID)
        {
            if (values != "")
            {
                string[] splitedValues = values.Split('&');
                string[] splitedValuesGaranty = garantyID.Split('&');
                for (int i = 0; i < splitedValues.Length - 1; i++)
                {
                    var pg = await _productGarntyRepository.GetWithIncludeAsync<ProductGaranty>
                        (selector: x => x,
                        where: x => x.GarantyId == Int16.Parse(splitedValuesGaranty[i]) && x.ProductInfoId == Int32.Parse(splitedValues[i])  
                        , include: x => x.Include(s => s.ProductInfo)
                        );

                   _productGarntyRepository.Delete(pg.FirstOrDefault());
                    await _productGarntyRepository.SaveChangesAsync();
                }
                return Json("ok");
            }
            return Json("no");
        }

        /////////////////////////////////////////////// بخش مزایا / معایب محصولات
        ///
        [DisplayName("نمایش مزایا/معایب محصول  ")]
        public async Task<IActionResult> ViewAllAdvantage(int id)
        {
            var product = await _productInfoRepository.GetWithIncludeAsync<ProductInfo> //GetProductByInfoId
                        (
                        selector: x => x,
                        include: x => x.Include(z => z.MainProduct),
                        where: x => x.ProductInfo_ID == id
                        );
            ViewBag.productTitle = product.FirstOrDefault().ProductTitle;
            ViewBag.productId = product.FirstOrDefault().ProductInfo_ID;
            var advantage = await _advantageRepository.GetAllAsync(x => x.ProductInfoId == id);
            return PartialView(advantage);
        }
        [DisplayName("افزودن مزایا/معایب به محصول  ")]

        public IActionResult CreateAdvantage()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateAdvantage(ProductAdvantage productAdvantage)
        {
            if (productAdvantage != null)
            {
                await _advantageRepository.AddAsync(productAdvantage);
                await _advantageRepository.SaveChangesAsync();
                return Json(productAdvantage);
            }
            return Json("nall");
        }
        [DisplayName("ویرایش مزایا/معایب به محصول  ")]

        public async Task<IActionResult> EditAdvantage(short id)
        {
            var advan = await _advantageRepository.GetByIdAsync(id);
            advantageList = new List<SelectListItem>();
            if (advan.AdvantageType == true)
            {
                advantageList.Add(new SelectListItem
                {
                    Text = "عیب",
                    Value = "0",
                });
                advantageList.Add(new SelectListItem
                {
                    Text = "مزیت",
                    Value = "1",
                    Selected = true
                });
            }
            else
            {
                advantageList.Add(new SelectListItem
                {
                    Text = "عیب",
                    Value = "0",
                    Selected = true
                });
                advantageList.Add(new SelectListItem
                {
                    Text = "مزیت",
                    Value = "1",

                });
            }
            ViewBag.advanType = advantageList;
            return View(advan);
        }
        [HttpPost]
        public async Task<IActionResult> EditAdvantage(ProductAdvantage productAdvantage)
        {
            if (productAdvantage != null)
            {
                _advantageRepository.Update(productAdvantage);
                await _advantageRepository.SaveChangesAsync();
                return Json(productAdvantage);
            }
            return Json("nall");
        }
        [DisplayName("حذف مزایا/معایب  محصول ")]

        public async Task<IActionResult> checkboxSelectedProductAdvantage(string values)
        {
            if (values != "")
            {
                string[] splitedValues = values.Split('&');
                for (int i = 0; i < splitedValues.Length - 1; i++)
                {
                    _advantageRepository.DeleteById(Int16.Parse(splitedValues[i]));
                    await _advantageRepository.SaveChangesAsync();
                }
                return Json("ok");
            }
            return Json("no");
        }

        /////////////////////////////// مربوط به بخش نظرات محصول
        ///
        [DisplayName("نمایش نظرات محصول ")]

        public async Task<IActionResult> ViewAllComments(int id)
        {
            if (id != 0)
            {
                var product = await _productInfoRepository.GetWithIncludeAsync<ProductInfo> //GetProductByInfoId
                        (
                        selector: x => x,
                        include: x => x.Include(z => z.MainProduct),
                        where: x => x.ProductInfo_ID == id
                        );
                ViewBag.productTitle = product.FirstOrDefault().ProductTitle;
                var comments =await _productCommentRepository.GetAllAsync(x => x.ProductInforId == id);
                return PartialView(comments);
            }
            return View();
        }
        [DisplayName("ویرایش نظرات محصول ")]

        public async Task<IActionResult> EditComment(int id)
        {
            var comment = await _productCommentRepository.GetByIdAsync(id);
            if (comment.IsNew)
            {
                comment.IsNew = false;
                _productCommentRepository.Update(comment);
                await _productCommentRepository.SaveChangesAsync();
            }
            return View(comment);
        }
        [HttpPost]
        public async Task<IActionResult> EditComment(ProductComment productComment)
        {
            if (productComment != null)
            {
                _productCommentRepository.Update(productComment);
                await _productCommentRepository.SaveChangesAsync();
                //await _emailRepository.SendEmail("amirhosseink.7795@gmail.com", productComment.Email, "پاسخ نظرات سایت", productComment.CommentReply);
                return Json(productComment);
            }
            return View();
        }
        [DisplayName("حذف نظرات محصول ")]

        public async Task<IActionResult> checkboxSelectedComments(string values)
        {
            if (values != "")
            {
                string[] splitedValues = values.Split('&');
                for (int i = 0; i < splitedValues.Length - 1; i++)
                {

                    _productCommentRepository.DeleteById(Int32.Parse(splitedValues[i]));
                    await _productCommentRepository.SaveChangesAsync();
                }
            }
            return Json("no");
        }
        ///////////////////////////////////////////// مربوط به بخش محصولات مشابه
        ///
        [DisplayName("نمایش محصولات مشابه")]

        public async Task<IActionResult> ViewAllSimilarProducts(int id)
        {
            var product = await _productInfoRepository.GetWithIncludeAsync<ProductInfo> //GetProductByInfoId
                       (
                       selector: x => x,
                       include: x => x.Include(z => z.MainProduct),
                       where: x => x.ProductInfo_ID == id
                       );
            ViewBag.productTitle = product.FirstOrDefault().ProductTitle;
            ViewBag.productId = product.FirstOrDefault().ProductInfo_ID;

            var similar =await _similarProductsRepository.GetWithIncludeAsync<SimilarProduct>( where: x=> x.ProductID == id,
                selector:x=>x,
                include:x=>x.Include(s=>s.ProductInfo).Include(s=>s.SimilarProducts).ThenInclude(s=>s.MainProduct));
            return PartialView(similar);
        }
        [HttpPost]
        [DisplayName("افزودن محصول مشابه")]

        public async Task<IActionResult> CreateSimilarProduct(SimilarProduct similarProduct)
        {
            if (similarProduct != null)
            {
                var IsExists = await _similarProductsRepository.Exists(x => x.SimilarProduct_ID == similarProduct.SimilarProduct_ID &&
                  x.ProductID == similarProduct.ProductID);
                if (IsExists)
                    return Json("repeate");

                if (similarProduct.ProductID==similarProduct.SimilarProduct_ID)
                    return Json("error");

                

                await _similarProductsRepository.AddAsync(similarProduct);
                    await _similarProductsRepository.SaveChangesAsync();
                    return Json(similarProduct);
              
            }
            return Json("nall");
        }
        [DisplayName("حذف محصول مشابه")]

        public async Task<IActionResult> DeleteSimilarProduct(int id)
        {
            _similarProductsRepository.DeleteById(id);
            await _similarProductsRepository.SaveChangesAsync();
            return Json("ok");
        }
    }
}