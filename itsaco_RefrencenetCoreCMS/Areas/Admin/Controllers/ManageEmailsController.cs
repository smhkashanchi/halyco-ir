﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AspCorePanelProject.Areas.Admin.Controllers
{
    [Area("admin")]

    public class ManageEmailsController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult ContactWithDev()
        {
            return PartialView();
        }
    }
}