﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Contents;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;

namespace MeshkatEnergy.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("درباره ما")]
    public class AboutUsController : Controller
    {
        public IGenericRepository<Contents> _contentsRepository;
        public AboutUsController(IGenericRepository<Contents> contentsRepository)
        {
            _contentsRepository = contentsRepository;
        }
        [DisplayName("صفحه نخست")]

        public async Task<IActionResult> Index()
        {
            var aboutUs = await _contentsRepository.GetByIdAsync(-1);
            ViewBag.aboutUs = aboutUs;
            return View();
        }
        [DisplayName("لیست درباره ما")]
        public async Task<ActionResult> ViewAll(string id)
        {
            if (CultureInfo.CurrentCulture.Name == "fa-IR")
            {
                var aboutUs = await _contentsRepository.GetAllAsync(x => x.ContentGroupId == -1);
                return PartialView(aboutUs);
            }
            else if (CultureInfo.CurrentCulture.Name == "en-US")
            {
                var aboutUs = await _contentsRepository.GetAllAsync(x => x.ContentGroupId == -2);
                return PartialView(aboutUs);
            }
            return PartialView();
        }
        [DisplayName("افزودن")]

        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Contents contents, IFormFile img)
        {
            if (contents != null)
            {
                if (img != null)
                {
                    contents.ContentImage = img.FileName.Substring(0, img.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(img.FileName);
                    string filePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/AboutUs", contents.ContentImage);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await img.CopyToAsync(stream);
                    }
                }


                contents.ContentGroupId = -1;
                contents.Content_ID = -1;
                contents.ContentImage = "";
                contents.CreateDate = DateTime.Now;

                await _contentsRepository.AddAsync(contents);
                await _contentsRepository.SaveChangesAsync();

                return Json("ok");
            }
            return Json("nall");
        }
        [DisplayName("ویرایش")]

        public async Task<IActionResult> Edit(int id)
        {
            return View(await _contentsRepository.GetByIdAsync(id));
        }
        [HttpPost]
        public async Task<IActionResult> Edit(Contents contents, IFormFile img)
        {
            if (contents != null)
            {
                if (img != null)
                {
                    if (contents.ContentImage != null)
                    {
                        string filePathOld = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/AboutUs", contents.ContentImage);
                        if (System.IO.File.Exists(filePathOld))
                        {
                            System.IO.File.Delete(filePathOld);
                        }
                    }
                    contents.ContentImage = img.FileName.Substring(0, img.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(img.FileName);
                    string filePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/AboutUs", contents.ContentImage);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await img.CopyToAsync(stream);
                    }
                }

                //contents.ContentGroupId = 6;
                contents.ContentImage = "";
                contents.CreateDate = DateTime.Now;

                _contentsRepository.Update(contents);
                await _contentsRepository.SaveChangesAsync();

                return Json("ok");
            }
            return Json("nall");
        }
        [NonAction]

        public async Task<IActionResult> DeleteFilesInProj(string path)
        {
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }

            return Json(Ok());
        }

    }

}