﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Seo;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories;

namespace CodeNagarProject.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]

    public class SeoPageProductController : Controller
    {
        public IGenericRepository<SeoPageProduct> _seoPageProjectRepository;
        public SeoPageProductController(IGenericRepository<SeoPageProduct> seoPageProjectRepository)
        {
            _seoPageProjectRepository = seoPageProjectRepository;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(SeoPageProduct seoPageProject)
        {
            if (seoPageProject != null)
            {
                //if (!_seoPageRepository.IsExist(seoPage.UrlPage))
                //{
                await _seoPageProjectRepository.AddAsync(seoPageProject);
               await _seoPageProjectRepository.SaveChangesAsync();
                return Json(seoPageProject);
                //}
                //return Json("repeate");
            }
            return Json("nall");
        }
        public async Task<IActionResult> Edit(int id)
        {
            var seoPage =await _seoPageProjectRepository.GetByIdAsync(id);
            return View(seoPage);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(SeoPageProduct seoPageProject)
        {
            if (seoPageProject.MetaDescription == null && seoPageProject.MetaTitle == null && seoPageProject.MetaKeyword == null && seoPageProject.MetaOther == null)
            {
                _seoPageProjectRepository.DeleteById(seoPageProject.PageId);
               await _seoPageProjectRepository.SaveChangesAsync();
                return Json("nall");
            }
            _seoPageProjectRepository.Update(seoPageProject);
           await _seoPageProjectRepository.SaveChangesAsync();
            return Json(seoPageProject);
        }
        public async Task<IActionResult> checkboxSelected(string values)
        {
            if (values != "")
            {
                string[] splitedValues = values.Split('&');
                for (int i = 0; i < splitedValues.Length - 1; i++)
                {
                    _seoPageProjectRepository.DeleteById(Int32.Parse(splitedValues[i]));
                  await  _seoPageProjectRepository.SaveChangesAsync();
                }
                return Json("ok");
            }
            return Json("nall");
        }
        public async Task<IActionResult> CreateOrEditProjectSeo(int id)
        {
            var seo =await _seoPageProjectRepository.GetByIdAsync(id);
            if (seo==null) //
            {
                //add
                return Json("add");
            }
            //Update
            return Json("update");
        }
    }
}