﻿using DomainClasses.Role;
using DomainClasses.User;
using DomainClasses.Employees;
using System.Linq.Dynamic.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services;
using Services.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Utilities;
using ViewModels;
using static Utilities.FunctionResult;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("لیست ماموریت های همه کارمندان")]
    public class MissionsAllEmployeesController : Controller
    {
        public UserManager<ApplicationUser> _userManager;
        public RoleManager<ApplicationRole> _roleManager;
        public IGenericRepository<ApplicationRole> _ApplicationRoleRepository;
        public IGenericRepository<ApplicationUser> _userRepository;
        public IGenericRepository<Mission> _MissionRepository;
        public IGenericRepository<MissionSignature> _MissionSignatureRepository;
        public ISMSSender _smsSender;

        public MissionsAllEmployeesController(UserManager<ApplicationUser> userManager, IGenericRepository<ApplicationRole> ApplicationRoleRepository, RoleManager<ApplicationRole> roleManager, IGenericRepository<ApplicationUser> userRepository, IGenericRepository<Mission> MissionRepository, IGenericRepository<MissionSignature> MissionSignatureRepository, ISMSSender smsSender)
        {
            _userManager = userManager;
            _ApplicationRoleRepository = ApplicationRoleRepository;
            _roleManager = roleManager;
            _userRepository = userRepository;
            _MissionRepository = MissionRepository;
            _MissionSignatureRepository = MissionSignatureRepository;
            _smsSender = smsSender;
        }

        [DisplayName("صفحه نخست")]
        public async Task<IActionResult> Index()
        {
            var user = await _userRepository.GetAsync(x => x.UserName == User.Identity.Name);
            ViewBag.UserIdentity_Id = user.Id;
            ViewBag.Fullname = user.FullName;
            return View();
        }


        [DisplayName("مشاهده لیست درخواست ها")]
        [HttpPost]
        public async Task<IActionResult> viewAll()
        {
            try
            {

                ////بدست آورن رکورد یوزر من
                //var user = await _userManager.GetUserAsync(User);
                ////بدست آوردن لیست  نام نقش های من
                //var myRole = await _userManager.GetRolesAsync(user);
                ////حذف نقش کارمند از لیست نقش های من
                //myRole.Remove("کارمند");
                ////بدست آوردن نقش من
                //var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());
                ////بدست آوردن لیست تمام نقش های زیرمجموعه نقش من
                //List<ApplicationRole> allMyChildrenRoles = new List<ApplicationRole>();
                //allMyChildrenRoles = await getChildrenRoles(mr, allMyChildrenRoles);
                ////بدست آوردن لیست تمام کارمندان نقش های زیرمجموعه نقش من
                //List<ApplicationUser> AllMyEmployees = new List<ApplicationUser>();
                //foreach (var item in allMyChildrenRoles)
                //{
                //    var TempEmployeeList = await _userManager.GetUsersInRoleAsync(item.Name);
                //    AllMyEmployees.AddRange(TempEmployeeList);
                //}
                ////بدست آوردن لیست نقش هایی که مدیر پشتیبانی می تواند درخواست های آنها را ببیند
                //var TempRoles = await _ApplicationRoleRepository.GetAllAsync(x => x.IsAgentOfManager);
                //foreach (var item in TempRoles)
                //{
                //    var TempEmployeeList = await _userManager.GetUsersInRoleAsync(item.Name);
                //    AllMyEmployees.AddRange(TempEmployeeList);
                //}
                ////بدست آوردن تمام درخواست های ماموریت کارمندان زیرمجموعه نقش من
                //IEnumerable<MissionSuperiorViewAllViewModel> AllMyEmployeeMissions = new List<MissionSuperiorViewAllViewModel>();
                //foreach (var item in AllMyEmployees)
                //{
                    var AllMyEmployeeMissions = await _MissionRepository.GetWithIncludeAsync(
                        selector: x => new MissionSuperiorViewAllViewModel
                        {
                            CreateDate = (x.CreateDate != null ? x.CreateDate.ToShamsiWithTime() : ""),
                            MissionStart = (x.MissionStart != null ? x.MissionStart.ToShortPersian2() : ""),
                            MissionEnd = (x.MissionEnd != null ? x.MissionEnd.ToShortPersian2() : ""),
                            MissionSubject = x.MissionSubject,
                            EndTime = x.EndTime,
                            FinalStatus = x.FinalStatus,
                            FullName = x.Employee_M_1.ApplicationUser.FullName,
                            StartTime = x.StartTime,
                            Mission_ID = x.Mission_ID
                        },
                        include: x => x.Include(s => s.Employee_M_1)
                        //,where: x => x.Employee_M_1.ApplicationUser.Id == item.Id
                        ,orderBy:x=>x.OrderByDescending(s=>s.Mission_ID)
                        );
                //    AllMyEmployeeMissions = (AllMyEmployeeMissions ?? Enumerable.Empty<MissionSuperiorViewAllViewModel>()).Concat(tempMissionList ?? Enumerable.Empty<MissionSuperiorViewAllViewModel>()); //amending `<string>` to the appropriate type

                //}
                //AllMyEmployeeMissions = AllMyEmployeeMissions.OrderByDescending(x => x.Mission_ID).ToList();

                #region jQuery_DataTable_Ajax_Code
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    AllMyEmployeeMissions = AllMyEmployeeMissions.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection).ToList();
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    AllMyEmployeeMissions = AllMyEmployeeMissions.Where(m => m.FullName.Contains(searchValue) || m.CreateDate.Contains(searchValue)
                    || (!string.IsNullOrEmpty(m.StartTime) ? m.StartTime.Contains(searchValue) : false)
                    || (!string.IsNullOrEmpty(m.EndTime) ? m.EndTime.Contains(searchValue) : false)
                    || m.MissionStart.Contains(searchValue)
                    || m.MissionEnd.Contains(searchValue)
                    || m.MissionSubject.Contains(searchValue)
                    ).ToList();

                }
                recordsTotal = AllMyEmployeeMissions.Count();
                var data = AllMyEmployeeMissions.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                #endregion
                return new JsonResult(jsonData);


            }
            catch (Exception err)
            {

                throw;
            }
            //return PartialView(AllMyEmployeeVacations.Take(100));
        }

        /// <summary>
        /// متد بازگشتی جهت بدست آوردن لیست تمامی نقش های اولاد یک نقش
        /// </summary>
        /// <param name="role">نقش جاری</param>
        /// <param name="childrenRoleList">لیست نقش های اولاد</param>
        /// <returns>لیست نقش های اولاد آپدیت شده</returns>
        [NonAction]
        public async Task<List<ApplicationRole>> getChildrenRoles(ApplicationRole role, List<ApplicationRole> childrenRoleList)
        {
            var tempList = await _ApplicationRoleRepository.GetAllAsync(x => x.ParentRoleId == role.Id || x.SendFinalSmsToUser == role.Id);

            //foreach (var item in tempList)
            //{
            //    var result = await getChildrenRoles(item, childrenRoleList);

            //}
            childrenRoleList.AddRange(tempList);
            return childrenRoleList;
        }

        [DisplayName("ویرایش ماموریت")]
        public async Task<IActionResult> Edit(int? id)
        {
            var user = await _userManager.GetUserAsync(User);
            var employee = await _userRepository.GetWithIncludeAsync
                (
                selector: x => x,
                where: x => x.Id == user.Id
                );
            if (user != null && employee.FirstOrDefault() != null)
            {
                ViewBag.FullName = user.FullName;
                ViewBag.PersonalCode = user.Id;
            }

            var mission = await _MissionRepository.GetWithIncludeAsync(
                selector: x => x,
                where: x => x.Mission_ID == id,
                include: x => x.Include(s => s.Employee_M_1).ThenInclude(s => s.ApplicationUser));
            var missionSignatureList = await _MissionSignatureRepository.GetAllAsync(x => x.Mission_ID == id, orderBy: x => x.OrderBy(s => s.MissionSignature_ID));
            //ViewBag.firstVSStatus = vacationSignatureList.FirstOrDefault().SignatureStatus;
            MissionSuperiorViewModel msvm = new MissionSuperiorViewModel();
            msvm.mission = mission.FirstOrDefault();
            var missionSignatures = await _MissionSignatureRepository.GetWithIncludeAsync(
                selector: x => x,
                where: x => x.Mission_ID == id,
                include: x => x.Include(s => s.applicationUser),
                orderBy: x => x.OrderBy(s => s.MissionSignature_ID)
                );
            List<MissionSignature> MSList = missionSignatures.OrderByDescending(x => x.MissionSignature_ID).ToList();

            bool flag = false;
            msvm.ThisUserCanSignature = true;
            foreach (var item in MSList)
            {
                if (flag)
                {
                    msvm.ThisUserCanSignature = item.SignatureStatus;
                    break;
                }
                if (item.applicationUser.Id == user.Id)
                {
                    flag = true;
                }

            }

            msvm.missionSignature = missionSignatures.ToList();
            msvm.CurrentSuperiorMissionSignature = missionSignatures.FirstOrDefault(x => x.PersonalCodeSignaturer == user.Id);

            #region برای مدیر پشتیبانی
            //بدست آوردن لیست  نام نقش های من
            var myRole = await _userManager.GetRolesAsync(user);
            //حذف نقش کارمند از لیست نقش های من
            myRole.Remove("Employee");
            //بدست آوردن نقش من
            var ThisUserRole = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());


            var RequesterUser = await _userRepository.GetAsync(x => x.Id == mission.FirstOrDefault().Employee_M_1.ApplicationUser.Id);
            //بدست آوردن لیست  نام نقش های من
            var RequesterRole = await _userManager.GetRolesAsync(RequesterUser);
            //حذف نقش کارمند از لیست نقش های من
            RequesterRole.Remove("Employee");
            //بدست آوردن نقش من
            var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == RequesterRole.FirstOrDefault());
            if (mr.IsAgentOfManager && mr.SendFinalSmsToUser == "116ce37b-efd3-4bb8-a42b-9027e85c7dd6" && mission.FirstOrDefault().FinalStatus != true && ThisUserRole.Id == "a2c5ff4a-64b0-4381-9cc2-e65596ebb271")//اگر نقش کاربر درخواست دهنده امکان مدیر پشتیبانی را فعال کرده باشد
            {
                ViewBag.IsAgentOfManager = true;
            }
            #endregion

            return PartialView(msvm);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(MissionSignature missionSignature)
        {
            //ذخیره سازی امضای مافوق کنونی
            var user = await _userManager.GetUserAsync(User);
            missionSignature.SignatureDateTime = DateTime.Now;
            missionSignature.PersonalCodeSignaturer = user.Id;
            _MissionSignatureRepository.Update(missionSignature);
            await _MissionSignatureRepository.SaveChangesAsync();

            var mission = await _MissionRepository.GetWithIncludeAsync(
              selector: x => x,
              where: x => x.Mission_ID == missionSignature.Mission_ID,
           include: x => x.Include(s => s.Employee_M_1).ThenInclude(s => s.ApplicationUser));

            if (missionSignature.SignatureStatus == false)
            {
                mission.FirstOrDefault().FinalStatus = missionSignature.SignatureStatus.Value;
                _MissionRepository.Update(mission.FirstOrDefault());
                await _MissionRepository.SaveChangesAsync();

                string missionStatusString = "";
                if (missionSignature.SignatureStatus.Value)
                {
                    missionStatusString = "تایید";
                }
                else
                {
                    missionStatusString = "رد";
                }
                //ارسال پیامک نتیجه درخواست مرخصی به کارمند درخواست کننده
                await _smsSender.SendfinalMissionAcceptSMSAsync(mission.FirstOrDefault().Employee_M_1.ApplicationUser.Mobile_1, mission.FirstOrDefault().Employee_M_1.ApplicationUser.FullName, missionSignature.SignatureDateTime.ToShamsiWithTime(), missionStatusString);
                //ارسال پیامک نتیجه درخواست مرخصی به مدیر منابع انسانی
                // await _smsSender.finalVacationAcceptHRManagerSMSAsync(parentUsers.FirstOrDefault().Mobile_1, vacation2.Employee_V_1.FullName, vacationSignature.SignatureDateTime.ToShamsiWithTime(), vacationStatusString);
            }

            var myRole = await _userManager.GetRolesAsync(user);
            myRole.Remove("Employee");
            var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());
            var parentRole = await _ApplicationRoleRepository.GetAsync(x => x.Id == mr.ParentRoleId);
            if (parentRole != null)
            {
                var parentUsers = await _userManager.GetUsersInRoleAsync(parentRole.Name);
                //تا اینجا
                // بررسی شود که آیا باید به مافوق بعدی پیامک بره یا اینکه به سطح مدیر پروژه رسیدیم و باید وضعیت خود مرخصی مشخص بشه و پیامک نتیجه نهایی به کارمند ارسال بشه

                //بررسی شود که آیا باید به مافوق بعدی پیامک برود
                if (mr.SendSmsToParent)
                {
                    string userMobile = parentUsers.FirstOrDefault().Mobile_1;
                    string userName = mission.FirstOrDefault().Employee_M_1.ApplicationUser.FullName;

                    string Duration;
                    Duration = "از تاریخ " + mission.FirstOrDefault().MissionStart.ToShortPersian() + " تا " + mission.FirstOrDefault().MissionEnd.ToShortPersian();

                    await _smsSender.SendSubmitMissionMessageSMSAsync(userMobile, userName, Duration);

                }
            }
            //بدست آوردن نقش کارمند درخواست کننده مرخصی
            var missonRequesterRole = await _userManager.GetRolesAsync(mission.FirstOrDefault().Employee_M_1.ApplicationUser);
            missonRequesterRole.Remove("Employee");
            var vrr = await _ApplicationRoleRepository.GetAsync(x => x.Name == missonRequesterRole.FirstOrDefault());
            //اگر شناسه نقشی که وضعیت نهایی مرخصی را مشخص می کند با شناسه مافوق جاری یکسان باشد
            if (vrr.SendFinalSmsToUser == mr.Id)
            {
                mission.FirstOrDefault().FinalStatus = missionSignature.SignatureStatus.Value;
                _MissionRepository.Update(mission.FirstOrDefault());
                await _MissionRepository.SaveChangesAsync();

                string vacationStatusString = "";
                if (missionSignature.SignatureStatus.Value)
                {
                    vacationStatusString = "تایید";
                }
                else
                {
                    vacationStatusString = "رد";
                }
                //ارسال پیامک نتیجه درخواست مرخصی به کارمند درخواست کننده
                await _smsSender.SendfinalMissionAcceptSMSAsync(mission.FirstOrDefault().Employee_M_1.ApplicationUser.Mobile_1, mission.FirstOrDefault().Employee_M_1.ApplicationUser.FullName, missionSignature.SignatureDateTime.ToShamsiWithTime(), vacationStatusString);
                //ارسال پیامک نتیجه درخواست به مدیر تایید کننده نهایی که فعلا نیازی نیست چون خودش میدونه چه کرده
                // await _smsSender.finalMissionAcceptHRManagerSMSAsync(parentUsers.FirstOrDefault().Mobile_1, mission.FirstOrDefault().Employee_M_1.ApplicationUser.FullName, missionSignature.SignatureDateTime.ToShamsiWithTime(), missionSignature.SignatureStatus.Value);
            }
            return Json(new { status = "ok" });
        }



    }
}
