﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Employees;
using DomainClasses.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("فیش حقوقی کارمندان")]
    public class EmployeeSalaryController : Controller
    {
        public UserManager<ApplicationUser> _userManager;
        public IGenericRepository<Employee> _empgenericRepository;
        public IGenericRepository<EmployeeSalary> _employeeSalary;
        public EmployeeSalaryController(IGenericRepository<EmployeeSalary> employeeSalary, UserManager<ApplicationUser> userManager, IGenericRepository<Employee> empgenericRepository)
        {
            _employeeSalary = employeeSalary;
            _userManager = userManager;
            _empgenericRepository = empgenericRepository;
        }
        public IActionResult Index()
        {
            return View();
        }
        [DisplayName("نمایش فیش حقوقی کارمندان")]
        public async Task<IActionResult> ViewAll()
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                var employee = await _empgenericRepository.GetWithIncludeAsync
                    (
                    selector: x => x,
                    include: x => x.Include(s => s.OfficeUnit),
                    where: x => x.UserIdentity_Id == user.Id
                    );

                var employeeSalary = await _employeeSalary.GetWithIncludeAsync(
                    selector: x => x,
                    include: x => x.Include(s => s.Employee).ThenInclude(s => s.ApplicationUser),
                    where: x => x.PersonalCode == employee.FirstOrDefault().PersonalCode
                    );
                ViewBag.employeeSalary = employeeSalary;
                var w = employeeSalary.OrderByDescending(x => x.SalMah);
                return PartialView(w);
            }
            catch (Exception err)
            {

                throw;
            }
        }
        [DisplayName("جزئیات فیش حقوقی")]
        public async Task<IActionResult> ViewDetails(string id)
        {
            var thisEmployeeSalary = await _employeeSalary.GetWithIncludeAsync(
                    selector: x => x,
                    include: x => x.Include(s => s.Employee).ThenInclude(s => s.ApplicationUser),
                    where: x => x.Salary_ID == id
                    );
            return PartialView(thisEmployeeSalary.FirstOrDefault());
        }
    }
}