﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.ContactUsMessage;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Services;
using Services.Repositories;

namespace AspCorePanelProject.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]
    [DisplayName("پیام های ارتباطی")]

    public class ContactUsMessageController : Controller
    {
        public List<SelectListItem> cmGroups;
        public IEmailSender _emailRepository;
        public IGenericRepository<ContactusMessageGroup> _contactusMsgGroupRepository;
        public IGenericRepository<ContactusMessage> _contactusMsgRepository;
        public ContactUsMessageController(IGenericRepository<ContactusMessageGroup> contactusMsgGroupRepository,
            IGenericRepository<ContactusMessage> contactusMsgRepository, IEmailSender emailRepository)
        {
            _contactusMsgGroupRepository = contactusMsgGroupRepository;
            _contactusMsgRepository = contactusMsgRepository;
            _emailRepository = emailRepository;
        }
        [DisplayName("صفحه نخست")]
        public IActionResult Index(string id)
        {

            return View();
        }
        [DisplayName("لیست گروه های ارتباطی")]

        public async Task<IActionResult> ViewAll(string id)
        {

            return PartialView(await _contactusMsgGroupRepository.GetAllAsync(x => x.Lang == id, x => x.OrderByDescending(s => s.CMG_ID)));
        }
        [DisplayName("افرودن گروه")]

        public IActionResult CreateCMG()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateCMG(ContactusMessageGroup contactusMessageGroup)
        {
            if (contactusMessageGroup != null)
            {
                await _contactusMsgGroupRepository.AddAsync(contactusMessageGroup);
                await _contactusMsgGroupRepository.SaveChangesAsync();
                return Json(contactusMessageGroup);
            }
            return View(contactusMessageGroup);
        }
        [DisplayName(" ویرایش گروه")]

        public async Task<IActionResult> EditCMG(byte id)
        {
            if (id == 0)
            {
                return NotFound();
            }
            return View(await _contactusMsgGroupRepository.GetByIdAsync(id));
        }
        [HttpPost]
        public async Task<IActionResult> EditCMG(ContactusMessageGroup contactusMessageGroup)
        {
            _contactusMsgGroupRepository.Update(contactusMessageGroup);
            await _contactusMsgGroupRepository.SaveChangesAsync();
            return Json(contactusMessageGroup);
        }
        [DisplayName("حذف گروه")]

        public async Task<IActionResult> DeleteCMG(byte id)
        {
            var CMG = await _contactusMsgGroupRepository.GetByIdAsync(id);
            if (CMG != null)
            {
                var list = await _contactusMsgRepository.GetAllAsync(x => x.CMGId == id);
                if (list.Count() == 0)
                {
                    _contactusMsgGroupRepository.Delete(CMG);
                    await _contactusMsgGroupRepository.SaveChangesAsync();
                    return Json(CMG);
                }
                return Json("NOK");
            }
            return Json(CMG);
        }
      
        // از اینجا به بعد مربوط به بخش پیام ها می باشد
        public async Task<IActionResult> ViewAllCMG(string id)
        {
            //جهت اضافه کردن یک آیتم به اول لیست داده شده به دراپ داون لیست از تکه کد زیر استفاده میشود
            cmGroups = new List<SelectListItem>();
            cmGroups.Add(new SelectListItem
            {
                Text = "انتخاب کنید ...",
                Value = "0"
            });
            foreach (var item in await _contactusMsgGroupRepository.GetAllAsync(x => x.Lang == id, x => x.OrderByDescending(s => s.CMG_ID)))
            {
                cmGroups.Add(new SelectListItem
                {
                    Text = item.CMGName,
                    Value = item.CMG_ID.ToString()
                });
            }
            ViewBag.allGroups = cmGroups;
            return Json(cmGroups);
        }
        [DisplayName("لیست پیام های ارتباطی")]

        public async Task<IActionResult> ViewAllCM(int id)
        {
            return PartialView(await _contactusMsgRepository.GetAllAsync(x => x.CMGId == id, x => x.OrderByDescending(s => s.ContactusMessage_ID)));
        }
        [DisplayName("ارسال پاسخ به پیام")]

        public async Task<IActionResult> EditCM(int id)
        {
            if (id == 0)
            {
                return NotFound();
            }
            //ContactusMessage cm = new ContactusMessage() { ContactusMessage_ID = id, IsNew = false };
            //_contactusMsgRepository.UpdateContactusMessage(cm);
            //_contactusMsgRepository.Save();
            return View(await _contactusMsgRepository.GetByIdAsync(id));
        }
        [HttpPost]
        public async Task<IActionResult> EditCM(ContactusMessage contactusMessage)
        {
            _contactusMsgRepository.Update(contactusMessage);
            await _contactusMsgRepository.SaveChangesAsync();
            /////////////Send Email
            /////برای ارسال ایمیل ابتدا دیپندنسی های اون رو داخل فایل اپ ستینگ ست کرده
            ///بعد از اون با ریپوزیتوری و سرویس بصورت اسنکرون پیاده سازی کرده
            await _emailRepository.SendEmailAsync(contactusMessage.SenderEmail, "پاسخ پیام ارتباطی", contactusMessage.MessageAnswer);
            ///

            return Json(contactusMessage);
        }

        [DisplayName("حذف پیام های ارتباطی")]

        public void checkboxSelected(string values)
        {
            if (values != "")
            {
                string[] splitedValues = values.Split('&');
                for (int i = 0; i < splitedValues.Length - 1; i++)
                {

                    _contactusMsgRepository.DeleteById(Int32.Parse(splitedValues[i]));
                    _contactusMsgRepository.SaveChangesAsync();

                }
            }
        }
    }
}