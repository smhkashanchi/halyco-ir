﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.BasicInformation;
using DomainClasses.Employees;
using DomainClasses.Role;
using DomainClasses.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]
    [DisplayName("مرخصی همکاران")]
    public class altVacationRequestController : Controller
    {
        public UserManager<ApplicationUser> _userManager;
        public RoleManager<ApplicationRole> _roleManager;
        public IGenericRepository<Employee> _empgenericRepository;
        public IGenericRepository<Vacation> _vacationRepository;
        public IGenericRepository<OfficePost> _officePostRepository;
        public IGenericRepository<ApplicationRole> _ApplicationRoleRepository;
        public IGenericRepository<ApplicationUser> _userRepository;

        public altVacationRequestController(IGenericRepository<OfficePost> officePostRepository, UserManager<ApplicationUser> userManager, IGenericRepository<Employee> empgenericRepository, IGenericRepository<Vacation> vacationRepository, IGenericRepository<ApplicationRole> ApplicationRoleRepository, RoleManager<ApplicationRole> roleManager, IGenericRepository<ApplicationUser> userRepository)
        {
            _userManager = userManager;
            _empgenericRepository = empgenericRepository;
            _vacationRepository = vacationRepository;
            _officePostRepository = officePostRepository;
            _ApplicationRoleRepository = ApplicationRoleRepository;
            _roleManager = roleManager;
            _userRepository = userRepository;
        }
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.OfficeUnit),
                where: x => x.UserIdentity_Id == user.Id
                );
            if (user != null && employee.FirstOrDefault() != null)
            {
                ViewBag.FullName = user.FullName;
                ViewBag.PersonalCode = employee.FirstOrDefault().PersonalCode;
            }
                return View();
        }

        //[DisplayName("لیست مرخصی ها")]
        //public async Task<IActionResult> ViewAll(string id)
        //{
        //    var vacations = await _vacationRepository.GetWithIncludeAsync
        //        (
        //        selector: x => x,
        //        include: x => x.Include(s => s.Employee_Vec_1).ThenInclude(s => s.ApplicationUser).Include(s => s.Employee_vac_2).ThenInclude(s => s.ApplicationUser).Include(s => s.Employee_vac_3).ThenInclude(s => s.ApplicationUser).Include(s => s.Employee_vac_4).ThenInclude(s => s.ApplicationUser).Include(s => s.Employee_vac_5).ThenInclude(s => s.ApplicationUser),
        //        where: x => x.AlternateSignature == id
        //        );
        //    return PartialView(vacations);
        //}

        //[DisplayName("ویرایش مرخصی")]
        //public async Task<IActionResult> EditAltVacation(int id)
        //{
        //    var user = await _userManager.GetUserAsync(User);
        //    var employee = await _empgenericRepository.GetWithIncludeAsync
        //        (
        //        selector: x => x,
        //        include: x => x.Include(s => s.OfficeUnit),
        //        where: x => x.UserIdentity_Id == user.Id
        //        );
        //    if (user != null && employee.FirstOrDefault() != null)
        //    {
        //        ViewBag.FullName = user.FullName;
        //        ViewBag.PersonalCode = employee.FirstOrDefault().PersonalCode;
        //        ViewBag.Unit = employee.FirstOrDefault().OfficeUnit.OfficeUnitTitle;
        //        string alternateClass = "";
        //        if (employee.FirstOrDefault().HasAlternateEmployee == false)
        //        {
        //            alternateClass = "hidden";
        //        }
        //        ViewBag.alternateClass = alternateClass;

        //        var userRolesList = await _userManager.GetRolesAsync(user);
        //        userRolesList.Remove("Employee");
        //        var y = await _userManager.GetUsersInRoleAsync(userRolesList[0]);

        //        //y.Remove(user);
        //        List<Employee> coworkerList = new List<Employee>();
        //        foreach (var item in y)
        //        {
        //            var emp = await _userRepository.GetWithIncludeAsync(
        //                 selector: x => x,
        //                 include: x => x.Include(s => s.Employee),
        //                 where: x => x.Id == item.Id
        //                 );
        //            coworkerList.Add(emp.FirstOrDefault().Employee);
        //        }
        //        SelectList coworkerEmployeelist = new SelectList(coworkerList, "PersonalCode", "ApplicationUser.FullName");
        //        ViewBag.coworkerEmployeelist = coworkerEmployeelist;
        //    }

        //    var thisVacation = await _vacationRepository.GetAsync(x => x.Vecation_ID == id);
        //    return PartialView(thisVacation);
        //}

        //[DisplayName("ویرایش مرخصی")]
        //[HttpPost]
        //public async Task<IActionResult> EditAltVacation(Vacation vacation, string sdate, string eDate, string timeDate, int st)//
        //{
        //    System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
        //    if (st == 1)
        //    {
        //        DateTime dtStart = dtDateTime.AddSeconds(double.Parse(sdate)).ToLocalTime();
        //        DateTime dtEnd = dtDateTime.AddSeconds(double.Parse(eDate)).ToLocalTime();
        //        vacation.DayVecationStartDate = dtStart.ToString("MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
        //        vacation.DayVecationEndDate = dtEnd.ToString("MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
        //        vacation.DateVacationTime = null;
        //    }
        //    else
        //    {
        //        DateTime dtTime = dtDateTime.AddSeconds(double.Parse(timeDate)).ToLocalTime();
        //        vacation.DayVecationStartDate = null;
        //        vacation.DayVecationEndDate = null;
        //        vacation.DateVacationTime = dtTime.ToString("MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
        //    }
        //    try
        //    {
        //        //vacation.ApplicantSignature = vacation.PersonalCode_3;
        //        _vacationRepository.Update(vacation);
        //        await _vacationRepository.SaveChangesAsync();
        //        return Json(new { status = "ok" });
        //    }
        //    catch (Exception err)
        //    {

        //        throw;
        //    }
        //}

    }
}