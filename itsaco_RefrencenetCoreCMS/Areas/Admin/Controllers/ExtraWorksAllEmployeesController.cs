﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Employees;
using DomainClasses.Role;
using DomainClasses.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Services;
using Services.Repositories;
using Utilities;
using ViewModels;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("لیست اضافه کاری های همه کارمندان")]
    public class ExtraWorksAllEmployeesController : Controller
    {
        public UserManager<ApplicationUser> _userManager;
        public RoleManager<ApplicationRole> _roleManager;
        public IGenericRepository<Employee> _empgenericRepository;
        public IGenericRepository<ApplicationRole> _ApplicationRoleRepository;
        public IGenericRepository<ApplicationUser> _userRepository;
        public IGenericRepository<ExtraWork> _ExtraWorkRepository;
        public IGenericRepository<ExtraworkSignature> _ExtraWorkSignatureRepository;
        public static byte SupervionLevel;
        public ISMSSender _smsSender;

        public ExtraWorksAllEmployeesController(UserManager<ApplicationUser> userManager, IGenericRepository<Employee> empgenericRepository, IGenericRepository<ApplicationRole> ApplicationRoleRepository, RoleManager<ApplicationRole> roleManager, IGenericRepository<ApplicationUser> userRepository, IGenericRepository<ExtraWork> ExtraWorkRepository, IGenericRepository<ExtraworkSignature> ExtraWorkSignatureRepository, ISMSSender smsSender)
        {
            _userManager = userManager;
            _empgenericRepository = empgenericRepository;
            _ApplicationRoleRepository = ApplicationRoleRepository;
            _roleManager = roleManager;
            _userRepository = userRepository;
            _ExtraWorkRepository = ExtraWorkRepository;
            _ExtraWorkSignatureRepository = ExtraWorkSignatureRepository;
            _smsSender = smsSender;
        }

        [DisplayName("صفحه نخست")]
        public async Task<IActionResult> Index()
        {
            var user = await _userRepository.GetAsync(x => x.UserName == User.Identity.Name);
            ViewBag.UserIdentity_Id = user.Id;
            ViewBag.Fullname = user.FullName;
            return View();
        }


        [DisplayName("لیست درخواست های اضافه کاری")]
        public async Task<IActionResult> ViewAll(string id, string ExtraWorkReason_txt, string ApplicationUserFullName_txt, string StartCreateDate, string EndCreateDate)
        {
            try
            {
                ////بدست آورن رکورد یوزر من
                //var user = await _userManager.GetUserAsync(User);
                ////بدست آوردن لیست  نام نقش های من
                //var myRole = await _userManager.GetRolesAsync(user);
                ////حذف نقش کارمند از لیست نقش های من
                //myRole.Remove("کارمند");
                ////بدست آوردن نقش من
                //var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());
                ////بدست آوردن لیست تمام نقش های زیرمجموعه نقش من
                //List<ApplicationRole> allMyChildrenRoles = new List<ApplicationRole>();
                //allMyChildrenRoles = await getChildrenRoles(mr, allMyChildrenRoles);
                ////بدست آوردن لیست تمام کارمندان نقش های زیرمجموعه نقش من
                //List<ApplicationUser> AllMyEmployees = new List<ApplicationUser>();
                //foreach (var item in allMyChildrenRoles)
                //{
                //    var TempEmployeeList = await _userManager.GetUsersInRoleAsync(item.Name);
                //    AllMyEmployees.AddRange(TempEmployeeList);
                //}
                ////بدست آوردن لیست نقش هایی که مدیر پشتیبانی می تواند درخواست های آنها را ببیند
                //var TempRoles = await _ApplicationRoleRepository.GetAllAsync(x => x.IsAgentOfManager);
                //foreach (var item in TempRoles)
                //{
                //    var TempEmployeeList = await _userManager.GetUsersInRoleAsync(item.Name);
                //    AllMyEmployees.AddRange(TempEmployeeList);
                //}
                ////بدست آوردن تمام درخواست های مرخصی کارمندان زیرمجموعه نقش من
                //IEnumerable<ExtraworkSuperiorViewAllViewModel> AllMyEmployeeExtraworks = new List<ExtraworkSuperiorViewAllViewModel>();
                //foreach (var item in AllMyEmployees)
                //{
                var AllMyEmployeeExtraworks = await _ExtraWorkRepository.GetWithIncludeAsync(
                    selector: x => new ExtraworkSuperiorViewAllViewModel
                    {
                        CreateDate = (x.CreateDate != null ? x.CreateDate.ToShamsiWithTime() : ""),
                        EndDate = x.EndDate,
                        FinalStatus = x.FinalStatus,
                        FullName = x.Employee_Ext_1.ApplicationUser.FullName,
                        StartDate = x.StartDate,
                        ExtraWork_ID = x.ExtraWorkID
                    },
                    include: x => x.Include(s => s.Employee_Ext_1)
                    //,where: x => x.Employee_Ext_1.ApplicationUser.Id == item.Id
                    , orderBy: x => x.OrderByDescending(s => s.ExtraWorkID)
                    );
                //    AllMyEmployeeExtraworks = (AllMyEmployeeExtraworks ?? Enumerable.Empty<ExtraworkSuperiorViewAllViewModel>()).Concat(tempExtraworkList ?? Enumerable.Empty<ExtraworkSuperiorViewAllViewModel>()); //amending `<string>` to the appropriate type

                //}
                //AllMyEmployeeExtraworks = AllMyEmployeeExtraworks.OrderByDescending(x => x.ExtraWork_ID).ToList();

                #region jQuery_DataTable_Ajax_Code
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    AllMyEmployeeExtraworks = AllMyEmployeeExtraworks.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection).ToList();
                }
               
                if (!string.IsNullOrEmpty(searchValue))
                {
                    AllMyEmployeeExtraworks = AllMyEmployeeExtraworks.Where(m => m.FullName.Contains(searchValue) || m.CreateDate.Contains(searchValue)
                    || m.DateVacationTime.Contains(searchValue)
                    || m.StartDate.Contains(searchValue)
                    || m.EndDate.Contains(searchValue)
                    ).ToList();

                }
                recordsTotal = AllMyEmployeeExtraworks.Count();
                var data = AllMyEmployeeExtraworks.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                #endregion
                return new JsonResult(jsonData);


            }
            catch (Exception err)
            {

                throw;
            }
        }

        [NonAction]
        public async Task<List<ApplicationRole>> getChildrenRoles(ApplicationRole role, List<ApplicationRole> childrenRoleList)
        {
            var tempList = await _ApplicationRoleRepository.GetAllAsync(x => x.ParentRoleId == role.Id || x.SendFinalSmsToUser == role.Id);

            //foreach (var item in tempList)
            //{
            //    var result = await getChildrenRoles(item, childrenRoleList);

            //}
            childrenRoleList.AddRange(tempList);
            return childrenRoleList;
        }


        [DisplayName("ویرایش اضافه کاری")]
        public async Task<IActionResult> Edit(int id)
        {
            var user = await _userManager.GetUserAsync(User);
            var employee = await _empgenericRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.OfficeUnit),
                where: x => x.UserIdentity_Id == user.Id
                );
            if (user != null && employee.FirstOrDefault() != null)
            {
                ViewBag.FullName = user.FullName;
                ViewBag.PersonalCode = employee.FirstOrDefault().PersonalCode;
            }

            var ExtraWork = await _ExtraWorkRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.Employee_Ext_1).ThenInclude(s => s.ApplicationUser),
                where: x => x.ExtraWorkID == id);

            ExtraworkSuperiorViewModel esvm = new ExtraworkSuperiorViewModel();

            esvm.extraWork = ExtraWork.FirstOrDefault();
            var extraworkSignatures = await _ExtraWorkSignatureRepository.GetWithIncludeAsync(
                selector: x => x,
                where: x => x.ExtraWork_ID == id,
                include: x => x.Include(s => s.applicationUser),
                orderBy: x => x.OrderBy(s => s.ExtraworkSignature_ID)
                );
            esvm.extraworkSignatures = extraworkSignatures.ToList();
            esvm.CurrentSuperiorExtraWorkSignatures = extraworkSignatures.FirstOrDefault(x => x.PersonalCodeSignaturer == user.Id);
            List<ExtraworkSignature> ESList = extraworkSignatures.OrderByDescending(x => x.ExtraworkSignature_ID).ToList();

            bool flag = false;
            esvm.ThisUserCanSignature = true;
            foreach (var item in ESList)
            {
                if (flag)
                {
                    esvm.ThisUserCanSignature = item.SignatureStatus;
                    break;
                }
                if (item.applicationUser.Id == user.Id)
                {
                    flag = true;
                }

            }

            ViewBag.firstVSStatus = extraworkSignatures.FirstOrDefault().SignatureStatus;
            #region برای مدیر پشتیبانی
            //بدست آوردن لیست  نام نقش های من
            var myRole = await _userManager.GetRolesAsync(user);
            //حذف نقش کارمند از لیست نقش های من
            myRole.Remove("Employee");
            //بدست آوردن نقش من
            var ThisUserRole = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());


            var RequesterUser = await _userRepository.GetAsync(x => x.Id == ExtraWork.FirstOrDefault().Employee_Ext_1.ApplicationUser.Id);
            //بدست آوردن لیست  نام نقش های من
            var RequesterRole = await _userManager.GetRolesAsync(RequesterUser);
            //حذف نقش کارمند از لیست نقش های من
            RequesterRole.Remove("Employee");
            //بدست آوردن نقش من
            var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == RequesterRole.FirstOrDefault());
            if (mr.IsAgentOfManager && mr.SendFinalSmsToUser == "116ce37b-efd3-4bb8-a42b-9027e85c7dd6" && ExtraWork.FirstOrDefault().FinalStatus != true && ThisUserRole.Id == "a2c5ff4a-64b0-4381-9cc2-e65596ebb271")//اگر نقش کاربر درخواست دهنده امکان مدیر پشتیبانی را فعال کرده باشد
            {
                ViewBag.IsAgentOfManager = true;
            }
            #endregion

            return PartialView(esvm);
        }


        [HttpPost]
        public async Task<IActionResult> Edit(ExtraworkSignature extraworkSignature)
        {
            //ذخیره سازی امضای مافوق کنونی
            var user = await _userManager.GetUserAsync(User);
            extraworkSignature.SignatureDateTime = DateTime.Now;
            extraworkSignature.PersonalCodeSignaturer = user.Id;
            _ExtraWorkSignatureRepository.Update(extraworkSignature);
            await _ExtraWorkSignatureRepository.SaveChangesAsync();
            var extraWorks = await _ExtraWorkRepository.GetWithIncludeAsync(
                   selector: x => x,
                   where: x => x.ExtraWorkID == extraworkSignature.ExtraWork_ID,
           include: x => x.Include(s => s.Employee_Ext_1).ThenInclude(s => s.ApplicationUser));

            if (extraworkSignature.SignatureStatus == false)
            {

                extraWorks.FirstOrDefault().FinalStatus = extraworkSignature.SignatureStatus.Value;
                _ExtraWorkRepository.Update(extraWorks.FirstOrDefault());
                await _ExtraWorkRepository.SaveChangesAsync();

                string extraworkStatusString = "";
                if (extraworkSignature.SignatureStatus.Value)
                {
                    extraworkStatusString = "تایید";
                }
                else
                {
                    extraworkStatusString = "رد";
                }
                //ارسال پیامک نتیجه درخواست مرخصی به کارمند درخواست کننده
                await _smsSender.SendfinalVacationAcceptSMSAsync(extraWorks.FirstOrDefault().Employee_Ext_1.ApplicationUser.Mobile_1, extraWorks.FirstOrDefault().Employee_Ext_1.ApplicationUser.FullName, extraworkSignature.SignatureDateTime.ToShamsiWithTime(), extraworkStatusString);
                //ارسال پیامک نتیجه درخواست مرخصی به مدیر منابع انسانی
                // await _smsSender.finalVacationAcceptHRManagerSMSAsync(parentUsers.FirstOrDefault().Mobile_1, vacation2.Employee_V_1.FullName, vacationSignature.SignatureDateTime.ToShamsiWithTime(), vacationStatusString);
            }

            var myRole = await _userManager.GetRolesAsync(user);
            myRole.Remove("Employee");
            var mr = await _ApplicationRoleRepository.GetAsync(x => x.Name == myRole.FirstOrDefault());
            var parentRole = await _ApplicationRoleRepository.GetAsync(x => x.Id == mr.ParentRoleId);
            if (parentRole != null)
            {
                var parentUsers = await _userManager.GetUsersInRoleAsync(parentRole.Name);
                //تا اینجا
                // بررسی شود که آیا باید به مافوق بعدی پیامک بره یا اینکه به سطح مدیر پروژه رسیدیم و باید وضعیت خود مرخصی مشخص بشه و پیامک نتیجه نهایی به کارمند ارسال بشه

                //بررسی شود که آیا باید به مافوق بعدی پیامک برود
                if (mr.SendSmsToParent)
                {
                    string userMobile = parentUsers.FirstOrDefault().Mobile_1;
                    string userName = extraWorks.FirstOrDefault().Employee_Ext_1.ApplicationUser.FullName;

                    await _smsSender.SendSubmitExtraWorkMessageSMSAsync(userMobile, userName, extraWorks.FirstOrDefault().StartDate.ToShortPersian());
                }
            }
            //بدست آوردن نقش کارمند درخواست کننده مرخصی
            var extraworkRequesterRole = await _userManager.GetRolesAsync(extraWorks.FirstOrDefault().Employee_Ext_1.ApplicationUser);
            extraworkRequesterRole.Remove("Employee");
            var vrr = await _ApplicationRoleRepository.GetAsync(x => x.Name == extraworkRequesterRole.FirstOrDefault());
            //اگر شناسه نقشی که وضعیت نهایی مرخصی را مشخص می کند با شناسه مافوق جاری یکسان باشد
            if (vrr.SendFinalSmsToUser == mr.Id)
            {
                extraWorks.FirstOrDefault().FinalStatus = extraworkSignature.SignatureStatus.Value;
                _ExtraWorkRepository.Update(extraWorks.FirstOrDefault());
                await _ExtraWorkRepository.SaveChangesAsync();

                string extraworkStatusString = "";
                if (extraworkSignature.SignatureStatus.Value)
                {
                    extraworkStatusString = "تایید";
                }
                else
                {
                    extraworkStatusString = "رد";
                }
                //ارسال پیامک نتیجه درخواست اضافه کاری به کارمند درخواست کننده
                await _smsSender.SendfinalExtraworkAcceptSMSAsync(extraWorks.FirstOrDefault().Employee_Ext_1.ApplicationUser.Mobile_1, extraWorks.FirstOrDefault().Employee_Ext_1.ApplicationUser.FullName, extraworkSignature.SignatureDateTime.ToShamsiWithTime(), extraworkStatusString);
                //ارسال پیامک نتیجه درخواست به مدیر تایید کننده نهایی که فعلا نیازی نیست چون خودش میدونه چه کرده
                //await _smsSender.finalExtraworkAcceptHRManagerSMSAsync(parentUsers.FirstOrDefault().Mobile_1, extraWorks.FirstOrDefault().Employee_Ext_1.ApplicationUser.FullName, extraworkSignature.SignatureDateTime.ToShamsiWithTime(), extraworkSignature.SignatureStatus.Value);
            }
            return Json(new { status = "ok" });
        }


    }
}