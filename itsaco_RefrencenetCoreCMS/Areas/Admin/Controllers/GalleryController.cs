﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Gallery;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;
using Utilities;

namespace AspCorePanelProject.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]
    [DisplayName("گالری")]

    public class GalleryController : Controller
    {
        public List<SelectListItem> ggList { get; set; }

        public IGenericRepository<GalleryGroup> _galleryGroupRepository;
        public IGenericRepository<Gallery> _galleryRepository;
        public IGenericRepository<GalleryPicture> _galleryPicturesRepository;

        public GalleryController(IGenericRepository<GalleryGroup> galleryGroupRepository
            , IGenericRepository<Gallery> galleryRepository,
            IGenericRepository<GalleryPicture> galleryPicturesRepository)
        {
            _galleryGroupRepository = galleryGroupRepository;
            _galleryRepository = galleryRepository;
            _galleryPicturesRepository = galleryPicturesRepository;
        }
        [DisplayName("صفحه نخست")]
        public IActionResult Index(string id)
        {


            return View();
        }
        [DisplayName("نمایش گروه گالری")]

        public async Task<IActionResult> ViewAll(string id)
        {
            return PartialView(await _galleryGroupRepository.GetAllAsync(x => x.Lang == CultureInfo.CurrentCulture.Name));
        }
        [DisplayName("افزودن گروه گالری")]

        public async Task<IActionResult> CreateGalleryGroup()
        {
            var max = await _galleryGroupRepository.GetAllAsync();
            if (max.Count() > 0)
            {
                ViewBag.maxId = max.LastOrDefault().GalleryGroup_ID + 1;
            }
            else
            {
                ViewBag.maxId = 1;
            }
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> CreateGalleryGroup(GalleryGroup galleryGroup)
        {
            if (ModelState.IsValid)
            {
                if (!await _galleryGroupRepository.Exists(x => x.GalleryGroup_ID == galleryGroup.GalleryGroup_ID))
                {
                    await _galleryGroupRepository.AddAsync(galleryGroup);
                    await _galleryGroupRepository.SaveChangesAsync();
                    return Json(galleryGroup);
                }
                var max = await _galleryGroupRepository.GetAllAsync();
                object[] obj = new object[2];
                obj[0] = "repeate";
                obj[1] = max.LastOrDefault().GalleryGroup_ID;
                return Json(obj);
            }
            return Json(ModelState.Values);
        }
        public async Task<IActionResult> IsExist(int id)
        {
            if (await _galleryGroupRepository.Exists(x => x.GalleryGroup_ID == id))
            {
                var max = await _galleryGroupRepository.GetAllAsync();
                object[] obj = new object[2];
                obj[0] = "repeate";
                obj[1] = max.LastOrDefault().GalleryGroup_ID;
                return Json(obj);
            }
            return Json("ok");
        }
        [DisplayName("ویرایش گروه گالری")]

        public async Task<IActionResult> EditGalleryGroup(byte id)
        {
            return View(await _galleryGroupRepository.GetByIdAsync(id));
        }
        [HttpPost]
        public async Task<IActionResult> EditGalleryGroup(GalleryGroup galleryGroup)
        {
            if (galleryGroup != null)
            {

                _galleryGroupRepository.Update(galleryGroup);
                await _galleryGroupRepository.SaveChangesAsync();
                return Json(galleryGroup);
            }
            return View();
        }
        [DisplayName("حذف گروه گالری")]

        public async Task<IActionResult> DeleteGalleryGroup(byte id)
        {
            var gGroup = await _galleryGroupRepository.GetByIdAsync(id);
            if (gGroup != null)
            {
                var list = await _galleryRepository.GetAllAsync(x => x.GalleryGroupId == gGroup.GalleryGroup_ID);
                if (list.Count() == 0)
                {
                    _galleryGroupRepository.Delete(gGroup);
                    await _galleryGroupRepository.SaveChangesAsync();
                    return Json(gGroup);
                }
                return Json("NOK");
            }
            return View();
        }
        /////////////// بخش گالری
        [DisplayName("لیست گالری ها")]

        public async Task<IActionResult> ViewAllGallery(int id)
        {
            return PartialView(await _galleryRepository.GetWithIncludeAsync<Gallery>(
                selector: x => x,
                where: x => x.GalleryGroupId == id,
                include: x => x.Include(s => s.GalleryGroup)));
        }
        public async Task<IActionResult> ViewAllGroupGallery(string id)
        {
            ggList = new List<SelectListItem>();
            ggList.Add(new SelectListItem
            {
                Text = "انتخاب کنید ...",
                Value = "0"
            });
            foreach (var item in await _galleryGroupRepository.GetAllAsync(x => x.Lang == id))
            {
                ggList.Add(new SelectListItem
                {
                    Text = item.GalleryGroupTitle,
                    Value = item.GalleryGroup_ID.ToString()
                });
            }

            return Json(ggList);
        }
        [DisplayName("افزودن گالری ")]

        public IActionResult CreateGallery()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateGallery(Gallery gallery, IFormFile galleryPic)
        {
            if (!Directory.Exists("wwwroot/Files/Gallery"))
            { Directory.CreateDirectory("wwwroot/Files/Gallery"); }
            if (!Directory.Exists("wwwroot/Files/Gallery/Thumb"))
            { Directory.CreateDirectory("wwwroot/Files/Gallery/Thumb"); }
            if (!Directory.Exists("wwwroot/Files/Gallery/Temp"))
            { Directory.CreateDirectory("wwwroot/Files/Gallery/Temp"); }
            if (ModelState.IsValid)
            {
                if (galleryPic != null)
                {
                    gallery.GalleryImage = galleryPic.FileName.Substring(0, galleryPic.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(galleryPic.FileName);
                    string TempPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Gallery/Temp", gallery.GalleryImage);
                    string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Gallery", gallery.GalleryImage);
                    string pathThumb = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Gallery/Thumb", gallery.GalleryImage);

                    using (var stream = new FileStream(TempPath, FileMode.Create))
                    {
                        galleryPic.CopyTo(stream);
                    }
                    ImageManipulate imageManipulate = new ImageManipulate();
                    imageManipulate.resizeImage2(TempPath, pathThumb, 260, 140);
                    System.IO.File.Delete(TempPath);
                }
                await _galleryRepository.AddAsync(gallery);
                await _galleryRepository.SaveChangesAsync();
                return Json(gallery);

            }
            return Json(ModelState.Values);
        }

        [DisplayName("ویرایش گالری ")]
        public async Task<IActionResult> EditGallery(short id)
        {
            var groupSelected = await _galleryRepository.GetByIdAsync(id);
            ViewBag.GalleryGroup = new SelectList(await _galleryGroupRepository.GetAllAsync(), "GalleryGroup_ID", "GalleryGroupTitle", groupSelected.GalleryGroupId);
            return View(await _galleryRepository.GetByIdAsync(id));
        }

        [HttpPost]
        public async Task<IActionResult> EditGallery(Gallery gallery, IFormFile ImageNameGallery)
        {
            if (ImageNameGallery != null)
            {
                string oldPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Gallery", gallery.GalleryImage);
                string oldPathThumb = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Gallery/Thumb", gallery.GalleryImage);
                DeleteFilesInProj(oldPath, oldPathThumb);

                gallery.GalleryImage = ImageNameGallery.FileName.Substring(0, ImageNameGallery.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(ImageNameGallery.FileName);
                string TempPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Gallery/Temp", gallery.GalleryImage);
                string savepath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Gallery", gallery.GalleryImage);
                string savepathThumb = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Gallery/Thumb", gallery.GalleryImage);

                using (var stream = new FileStream(TempPath, FileMode.Create))
                {
                    ImageNameGallery.CopyTo(stream);
                }
                //
                ImageManipulate imageManipulate = new ImageManipulate();
                imageManipulate.resizeImage2(TempPath, savepathThumb, 260, 140);
                System.IO.File.Delete(TempPath);
            }
            _galleryRepository.Update(gallery);
            await _galleryRepository.SaveChangesAsync();
            return Json(gallery);
        }

        [NonAction]
        public IActionResult DeleteFilesInProj(string path, string pathThumb)
        { ///برای حذف بصورت آسینک
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            if (System.IO.File.Exists(pathThumb))
            {
                System.IO.File.Delete(pathThumb);
            }
            return Json(Ok());
        }
        [DisplayName("حذف گالری ")]

        public async Task<IActionResult> DeleteGallery(short id)
        {
            var gallery = await _galleryRepository.GetByIdAsync(id);
            string pathThumb = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Gallery/Thumb", gallery.GalleryImage);
            System.IO.File.Delete(pathThumb);
            if (gallery != null)
            {
                var list = await _galleryPicturesRepository.GetAllAsync(x => x.GalleryId == id);
                if (list.Count() == 0)
                {
                    _galleryRepository.Delete(gallery);
                    await _galleryRepository.SaveChangesAsync();
                    return Json(gallery.GalleryTitle);
                }
                return Json("Invalid");
            }
            return Json("NOK");
        }

        /////////////// بخش تصاویر گالری
        [DisplayName("لیست تصاویر گالری ")]

        public async Task<IActionResult> ViewAllGalleryPicture(int? id)
        {
            if (id != 0)
            {
                ViewBag.gallery = new SelectList(await _galleryRepository.GetAllAsync(), "Gallery_ID", "GalleryTitle", id);

                return View(await _galleryPicturesRepository.GetAllAsync(x => x.GalleryId == id.Value));
            }
            return View();
        }
        [DisplayName("افزودن تصاویر گالری ")]

        [ActionName("Upload")]
        public async Task<IActionResult> AddGalleryPictures(int id)
        {

            ViewBag.gallery = new SelectList(await _galleryRepository.GetAllAsync(), "GalleryID", "GalleryTitle");
            return View("AddGalleryPictures");
        }

        [HttpPost]
        public async Task<IActionResult> Upload(List<IFormFile> ImageUpload)
        {//این تابع جهت ذخیره عکس ها در فولدر تمپ
            if (!Directory.Exists("wwwroot/Files/GalleryPicture/temp"))
            {
                Directory.CreateDirectory("wwwroot/Files/GalleryPicture/temp");
            }
            if (!Directory.Exists("wwwroot/Files/GalleryPicture/Thumb"))
            {
                Directory.CreateDirectory("wwwroot/Files/GalleryPicture/Thumb");
            }


            await RemoveTempFiles();
            string fileName = "";
            string fn = "";
            Random rnd = new Random();
            long size = ImageUpload.Sum(f => f.Length);

            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/GalleryPicture/temp");

            foreach (var formFile in ImageUpload)
            {
                if (formFile.Length > 0)
                {
                    fileName = rnd.Next(1, 99999).ToString() + formFile.FileName.Replace(" ", "_").Replace("(", "").Replace(")", "");
                    using (var stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                    {
                        formFile.CopyTo(stream);
                    }
                    //ImageManipulate imageManipulate = new ImageManipulate();
                    //imageManipulate.resizeImage2(path, savepathThumb, 260, 140);
                }
                fn += fileName + "&";
            }
            return Ok(new { count = ImageUpload.Count, size, fn });
        }
        [NonAction]

        public async Task<JsonResult> RemoveFile(string filename)
        {
            string path = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/GalleryPicture", filename);
            string pathThumb = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/GalleryPicture/Thumb", filename);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
                System.IO.File.Delete(pathThumb);
            }
            return Json(Ok());
        }

        [NonAction]
        public async Task<JsonResult> RemoveFileFromTemp(string filename)//تابعی برای حذف عکس ها در پوشه تمپ با زدن دکمه ضرب در در عکس های آپلودی
        {
            string path = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/GalleryPicture/temp", filename);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            return Json(Ok());
        }

        public async Task<IActionResult> SaveToMainFolder(GalleryPicture gallery)
        {
            ImageManipulate imageManipulate = new ImageManipulate();
            string fileName = "";
            string destFile = "";
            string thumbFile = "";
            string sourcePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/GalleryPicture/temp");
            string targetPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/GalleryPicture");
            string pathThumb = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/GalleryPicture/Thumb");
            if (System.IO.Directory.Exists(sourcePath))
            {
                string[] files = System.IO.Directory.GetFiles(sourcePath);

                // Copy the files. 

                foreach (string file in files)
                {
                    fileName = System.IO.Path.GetFileName(file);
                    destFile = System.IO.Path.Combine(targetPath, fileName);     //ایجاد مسیر و نام فایل برای ذخیره عکس اصلی
                    thumbFile = System.IO.Path.Combine(pathThumb, fileName);     //ایجاد مسیر و نام فایل برای ذخیره عکس بند انگشتی
                    System.IO.File.Copy(file, destFile, true);                   //ذخیره بدون تغییر سایز عکس در مسیر اصلی

                    imageManipulate.resizeImage2(destFile, thumbFile, 260, 140);   //تغییر اندازه و ذخیره عکس در مسیر بند انگشتی

                    var galleryPictures = new GalleryPicture();
                    galleryPictures.GalleryId = gallery.GalleryId;
                    galleryPictures.ImageNames = fileName;
                    galleryPictures.GalleryPicTitle = gallery.GalleryPicTitle;
                    await _galleryPicturesRepository.AddAsync(galleryPictures);
                }

                await _galleryPicturesRepository.SaveChangesAsync();
                await RemoveTempFiles();
            }
            return Ok(gallery.GalleryId);
        }
        [NonAction]
        public async Task<JsonResult> RemoveTempFiles()
        {
            string sourcePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/GalleryPicture/temp");
            string[] files = System.IO.Directory.GetFiles(sourcePath);
            foreach (string file in files)
            {
                if (System.IO.File.Exists(System.IO.Path.Combine(sourcePath, file)))
                {
                    try
                    {
                        System.IO.File.Delete(file);
                    }
                    catch (System.IO.IOException e)
                    {
                        return Json("NOK");
                    }
                }
            }
            return Json(Ok());
        }
        [DisplayName(" حذف تصاویر گالری")]

        public async Task<JsonResult> checkboxSelected(string values)
        {
            if (values != "")
            {
                string[] splitedValues = values.Split('&');
                for (int i = 0; i < splitedValues.Length - 1; i++)
                {

                    var galleryPic = await _galleryPicturesRepository.GetAsync(x => x.ImageNames == splitedValues[i]);
                    if (galleryPic != null)
                    {

                        await RemoveFile(galleryPic.ImageNames);
                        _galleryPicturesRepository.Delete(galleryPic);
                        await _galleryPicturesRepository.SaveChangesAsync();
                    }
                }
            }
            return Json(Ok());
        }
    }
}