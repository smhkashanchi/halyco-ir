﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Product;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Services.Repositories;

namespace AspCorePanelProject.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]
    [DisplayName("واحد")]
    public class UnitController : Controller
    {
        public List<SelectListItem> unitList;
        public IGenericRepository<Unit> _unitRepository;
        public UnitController(IGenericRepository<Unit> unitRepository)
        {
            _unitRepository = unitRepository;
        }
        [DisplayName("صفحه نخست")]

        public IActionResult Index()
        {
            return View();
        }
        [DisplayName("لیست واحدها")]

        public async Task<ActionResult> ViewAll(string id)
        {
            var unit = await _unitRepository.GetAllAsync(x => x.Lang == id);
            return PartialView(unit);
        }
        [DisplayName("افزودن واحد")]

        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Unit unit)
        {
            if (unit != null)
            {
                await _unitRepository.AddAsync(unit);
                await _unitRepository.SaveChangesAsync();
                return Json(unit);
            }
            return View(unit);
        }
        [DisplayName("ویرایش واحد")]

        public async Task<IActionResult> Edit(Int16 id)
        {
            var unit = await _unitRepository.GetByIdAsync(id);
            return View(unit);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(Unit unit)
        {
            if (unit != null)
            {
                _unitRepository.Update(unit);
                await _unitRepository.SaveChangesAsync();
                return Json(unit);
            }
            return View(unit);
        }
        [DisplayName("حذف واحد")]

        public async Task<IActionResult> Delete(Int16 id)
        {
            var unit = await _unitRepository.GetByIdAsync(id);
            if (unit != null)
            {
                //if (_unitRepository.)
                //{
                _unitRepository.Delete(unit);
                await _unitRepository.SaveChangesAsync();
                return Json(unit);
                //}
                //return Json("NOK");
            }
            return View(unit);
        }

        public async Task<JsonResult> ViewAllUnitList(string id)
        {
            unitList = new List<SelectListItem>();
            unitList.Add(new SelectListItem
            {
                Text = "انتخاب کنید ...",
                Value = "0"
            });
            foreach (var item in await _unitRepository.GetAllAsync(x => x.Lang == id))
            {
                unitList.Add(new SelectListItem
                {
                    Text = item.UnitTitle,
                    Value = item.Unit_ID.ToString()
                });
            }
            return Json(unitList);
        }
    }
}