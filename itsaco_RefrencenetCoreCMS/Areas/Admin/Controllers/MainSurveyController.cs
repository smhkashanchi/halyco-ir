﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Product;
using DomainClasses.Survey;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;
using ViewModels;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Authorize]
    [Area("admin")]
    [DisplayName("نظرسنجی اصلی")]
    public class MainSurveyController : Controller
    {
        public IGenericRepository<MainSurvey> _mainSurveyRepository;
        public IGenericRepository<SurvayQuestionGroup> _survayQuestionGroupRepository;
        public IGenericRepository<SurveyQuestion> _surveyQuestionRepository;
        public IGenericRepository<ProductInfo> _productInfoRepository;
        public IGenericRepository<SurveyCustomerOtherData> _surveyCustomerOtherDataRepository;
        public IGenericRepository<CustomerSurveyProducts> _customerSurveyProductsRepository;
        public IGenericRepository<SurveyQuestionReply> _surveyQuestionReplyRepository;
        public IGenericRepository<SurveyEmployeesOtherData> _SurveyEmployeesOtherDataRepository;
        public IGenericRepository<SurveySuppliersOtherData> _SurveySuppliersOtherDataRepository;
        public MainSurveyController(IGenericRepository<SurveyQuestion> surveyQuestionRepository,IGenericRepository<MainSurvey> mainSurveyRepository, IGenericRepository<SurvayQuestionGroup> survayQuestionGroupRepository, IGenericRepository<ProductInfo> productInfoRepository
            , IGenericRepository<SurveyCustomerOtherData> surveyCustomerOtherDataRepository, IGenericRepository<CustomerSurveyProducts> customerSurveyProductsRepository, IGenericRepository<SurveyQuestionReply> surveyQuestionReplyRepository,
            IGenericRepository<SurveyEmployeesOtherData> SurveyEmployeesOtherDataRepository, IGenericRepository<SurveySuppliersOtherData> SurveySuppliersOtherDataRepository)
        {
            _mainSurveyRepository = mainSurveyRepository;
            _survayQuestionGroupRepository = survayQuestionGroupRepository;
            _surveyQuestionRepository = surveyQuestionRepository;
            _productInfoRepository = productInfoRepository;
            _surveyCustomerOtherDataRepository = surveyCustomerOtherDataRepository;
            _customerSurveyProductsRepository = customerSurveyProductsRepository;
            _surveyQuestionReplyRepository = surveyQuestionReplyRepository;
            _SurveyEmployeesOtherDataRepository = SurveyEmployeesOtherDataRepository;
            _SurveySuppliersOtherDataRepository = SurveySuppliersOtherDataRepository;
        }
        public IActionResult Index()
        {
            return View();
        }
        [DisplayName("لیست نظرسنجی های اصلی")]
        public async Task<IActionResult> ViewAll(byte? id)
        {
            if (id == null)
            {
                return PartialView(await _mainSurveyRepository.GetAllAsync());
            }
            else
            {
                return PartialView(await _mainSurveyRepository.GetAllAsync(x => x.SurveyType == id));
            }
        }
        [DisplayName("افزودن نظرسنجی اصلی")]
        public IActionResult Create()
        {
            return PartialView();
        }
        [HttpPost]
        public async Task<IActionResult> Create(MainSurvey mainSurvey)
        {
            mainSurvey.CreateDate = DateTime.Now;
            await _mainSurveyRepository.AddAsync(mainSurvey);
            await _mainSurveyRepository.SaveChangesAsync();
            return Json("ok");
        }
        [DisplayName("ویرایش نظرسنجی اصلی")]
        public async Task<IActionResult> Edit(int id)
        {
            return PartialView(await _mainSurveyRepository.GetAsync(x=>x.MainSurvey_ID==id));
        }
        [HttpPost]
        public async Task<IActionResult> Edit(MainSurvey mainSurvey)
        {
            _mainSurveyRepository.Update(mainSurvey);
            await _mainSurveyRepository.SaveChangesAsync();
            return Json("ok");
        }
        [DisplayName("حذف نظرسنجی اصلی")]
        public async Task<IActionResult> Delete(int id)
        {
            if(!await _survayQuestionGroupRepository.Exists(x => x.MainSurveyId == id))
            {
                var mainSurvey = await _mainSurveyRepository.GetAsync(x => x.MainSurvey_ID == id);
                if (mainSurvey != null)
                {
                    _mainSurveyRepository.Delete(mainSurvey);
                    await _mainSurveyRepository.SaveChangesAsync();
                    return Json("ok");
                }
                return Json("nall");
            }
            return Json("ex_group");
        }

        [DisplayName("آمار نظرسنجی")]
        public async Task<IActionResult> ViewDatailsSurvey(int id)
        {
            //var thisMainSurvey = await _mainSurveyRepository.GetAsync(x => x.MainSurvey_ID == id);

            SurveyVM survey = new SurveyVM();
            ViewBag.Products = await _productInfoRepository.GetWithIncludeAsync
               (
               selector: x => x,
               include: x => x.Include(s => s.MainProduct),
               where: x => x.MainProduct.IsActive
               );

            var products = await _productInfoRepository.GetWithIncludeAsync
               (
               selector: x => x,
               include: x => x.Include(s => s.MainProduct),
               where: x => x.MainProduct.IsActive
               );


            IEnumerable<SurvayQuestionGroup> quesGroup = await _survayQuestionGroupRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.MainSurvey),
                where: x => x.MainSurveyId==id
                );


            survey.SurvayQuestionGroup = quesGroup;

            //var customerSurvey = await _surveyCustomerOtherDataRepository.GetAllAsync(x => x.MainSurveyId == quesGroup.FirstOrDefault().MainSurvey.MainSurvey_ID);
          

            //var cnt = customerSurvey.Count(x => x.ProductId == 9);
            
            if (quesGroup.Count() > 0)
            {
                ViewBag.CustomerSurvey = await _customerSurveyProductsRepository.GetWithIncludeAsync
              (
              selector: x => x,
              include: x => x.Include(s => s.SurveyCustomerOtherData),
              where: x => x.SurveyCustomerOtherData.MainSurveyId == quesGroup.FirstOrDefault().MainSurvey.MainSurvey_ID
              );

                IEnumerable<SurveyQuestion> questions = await _surveyQuestionRepository.GetWithIncludeAsync(
                    selector: x => x,
                    include: x => x.Include(s => s.SurveyAnswerType).Include(s => s.SurvayQuestionGroup),
                    where: x => x.SurvayQuestionGroup.MainSurveyId == quesGroup.FirstOrDefault().MainSurveyId
                    );

                survey.SurveyQuestions = questions;


                
            }
            //questions.

            return PartialView(survey);
        }

        //دیگه استفاده نمی شود
        [DisplayName("مشاهده لیست همه شرکت کنندگان نظرسنجی ها")]
        public async Task<IActionResult> ViewAllSurveyPeople()
        {
            var AllPersonSurvey = await _surveyCustomerOtherDataRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.MainSurvey),
                orderBy: x => x.OrderByDescending(s => s.C_OtherData_ID));
            return PartialView(AllPersonSurvey);
        }

        [DisplayName("مشاهده لیست همه مشتریان شرکت کننده در یک نظرنسجی")]
        public async Task<IActionResult> ViewOneSurveyPeople(int id)
        {
            var AllPersonSurvey = await _surveyCustomerOtherDataRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.MainSurvey),
                where: x => x.MainSurveyId == id,
                orderBy: x => x.OrderByDescending(s => s.C_OtherData_ID));
            return PartialView(AllPersonSurvey);
        }

        [DisplayName("مشاهده لیست همه کارمندان شرکت کننده در یک نظرنسجی")]
        public async Task<IActionResult> ViewOneEmployeeSurveyPeople(int id)
        {
            var AllPersonSurvey = await _SurveyEmployeesOtherDataRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.MainSurvey).Include(s=>s.Employee).ThenInclude(s=>s.ApplicationUser),
                where: x => x.MainSurveyId == id,
                orderBy: x => x.OrderByDescending(s => s.SE_ID));
            return PartialView(AllPersonSurvey);
        }

        [DisplayName("مشاهده لیست همه تامین کنندگان شرکت کننده در یک نظرنسجی")]
        public async Task<IActionResult> ViewOneSupplierSurveyPeople(int id)
        {
            var AllPersonSurvey = await _SurveySuppliersOtherDataRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.MainSurvey),
                where: x => x.MainSurveyId == id,
                orderBy: x => x.OrderByDescending(s => s.S_OtherData_ID));
            return PartialView(AllPersonSurvey);
        }


        [DisplayName("مشاهده جزییات نظرسنجی مشتری")]
        public async Task<IActionResult> showThisPersonSurvey(int id)
        {
            SurveyVM survey = new SurveyVM();
            var ThisSurveyCustomerOtherData = await _surveyCustomerOtherDataRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.MainSurvey),
                where: x => x.C_OtherData_ID == id
              );

            survey.SurveyCustomerOtherData = ThisSurveyCustomerOtherData.FirstOrDefault();

            var ThisSurveyQuestionReply = await _surveyQuestionReplyRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.SurveyQuestion).ThenInclude(s => s.SurvayQuestionGroup).Include(s => s.SurveyQuestion).ThenInclude(s => s.SurveyAnswerType),
                where: x => x.ExtraInformationId == id
                );
            //ViewBag.ThisSurveyQuestionReply = ThisSurveyQuestionReply;
            survey.surveyQuestionReplies = ThisSurveyQuestionReply;
            //=================================================================================
           
            //var mainSurvey = await _mainSurveyRepository.GetAllAsync(x => x.MainSurvey_ID==ThisSurvey.FirstOrDefault().MainSurveyId, x => x.OrderByDescending(s => s.MainSurvey_ID));

            IEnumerable<SurvayQuestionGroup> quesGroup = await _survayQuestionGroupRepository.GetAllAsync(x => x.MainSurveyId == ThisSurveyCustomerOtherData.FirstOrDefault().MainSurveyId);
            survey.SurvayQuestionGroup = quesGroup;

            IEnumerable<SurveyQuestion> questions = await _surveyQuestionRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.SurveyAnswerType).Include(s => s.SurvayQuestionGroup),
                where: x => x.SurvayQuestionGroup.MainSurveyId == ThisSurveyCustomerOtherData.FirstOrDefault().MainSurveyId
                );

            survey.SurveyQuestions = questions;

            survey.customerSurveyProducts = await _customerSurveyProductsRepository.GetWithIncludeAsync(
                selector:x=>x,
                include:x=>x.Include(s=>s.ProductInfo),
                where:x => x.CustomerSurveyId == id);
             //=========================================================
            if (CultureInfo.CurrentCulture.Name == "fa-IR")
            {
                ViewBag.Products = await _productInfoRepository.GetWithIncludeAsync
               (
               selector: x => x,
               include: x => x.Include(s => s.MainProduct).Include(s => s.ProductsGroup),
               where: x => x.MainProduct.IsActive && x.ProductsGroup.Lang == "fa-IR"
               );
            }
            else
            {
                ViewBag.Products = await _productInfoRepository.GetWithIncludeAsync
               (
               selector: x => x,
               include: x => x.Include(s => s.MainProduct).Include(s => s.ProductsGroup),
               where: x => x.MainProduct.IsActive && x.ProductsGroup.Lang == "en-US"
               );
            }
            return PartialView(survey);
        }

        [DisplayName("مشاهده جزییات نظرسنجی کارمند")]
        public async Task<IActionResult> showThisEmployeeSurvey(Int16 id)
        {
            SurveyVM survey = new SurveyVM();
            var ThissurveyEmployeesOtherData = await _SurveyEmployeesOtherDataRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.MainSurvey).Include(s => s.Employee).ThenInclude(s => s.ApplicationUser),
                where: x => x.SE_ID == id
              );

            survey.surveyEmployeesOtherData = ThissurveyEmployeesOtherData.FirstOrDefault();

            var ThisSurveyQuestionReply = await _surveyQuestionReplyRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.SurveyQuestion).ThenInclude(s => s.SurvayQuestionGroup).Include(s => s.SurveyQuestion).ThenInclude(s => s.SurveyAnswerType),
                where: x => x.ExtraInformationId == id
                );
            //ViewBag.ThisSurveyQuestionReply = ThisSurveyQuestionReply;
            survey.surveyQuestionReplies = ThisSurveyQuestionReply;
            //=================================================================================

            //var mainSurvey = await _mainSurveyRepository.GetAllAsync(x => x.MainSurvey_ID==ThisSurvey.FirstOrDefault().MainSurveyId, x => x.OrderByDescending(s => s.MainSurvey_ID));

            IEnumerable<SurvayQuestionGroup> quesGroup = await _survayQuestionGroupRepository.GetAllAsync(x => x.MainSurveyId == ThissurveyEmployeesOtherData.FirstOrDefault().MainSurveyId);
            survey.SurvayQuestionGroup = quesGroup;

            IEnumerable<SurveyQuestion> questions = await _surveyQuestionRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.SurveyAnswerType).Include(s => s.SurvayQuestionGroup),
                where: x => x.SurvayQuestionGroup.MainSurveyId == ThissurveyEmployeesOtherData.FirstOrDefault().MainSurveyId
                );

            survey.SurveyQuestions = questions;

           
           
            return PartialView(survey);
        }

        [DisplayName("مشاهده جزییات نظرسنجی تامین کننده")]
        public async Task<IActionResult> showThisSupplierSurvey(Int16 id)
        {
            SurveyVM survey = new SurveyVM();
            var ThisSurveySuppliersOtherData = await _SurveySuppliersOtherDataRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.MainSurvey),
                where: x => x.S_OtherData_ID == id
              );

            survey.SurveySuppliersOtherData = ThisSurveySuppliersOtherData.FirstOrDefault();

            var ThisSurveyQuestionReply = await _surveyQuestionReplyRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.SurveyQuestion).ThenInclude(s => s.SurvayQuestionGroup).Include(s => s.SurveyQuestion).ThenInclude(s => s.SurveyAnswerType),
                where: x => x.ExtraInformationId == id
                );
            //ViewBag.ThisSurveyQuestionReply = ThisSurveyQuestionReply;
            survey.surveyQuestionReplies = ThisSurveyQuestionReply;
            //=================================================================================

            //var mainSurvey = await _mainSurveyRepository.GetAllAsync(x => x.MainSurvey_ID==ThisSurvey.FirstOrDefault().MainSurveyId, x => x.OrderByDescending(s => s.MainSurvey_ID));

            IEnumerable<SurvayQuestionGroup> quesGroup = await _survayQuestionGroupRepository.GetAllAsync(x => x.MainSurveyId == ThisSurveySuppliersOtherData.FirstOrDefault().MainSurveyId);
            survey.SurvayQuestionGroup = quesGroup;

            IEnumerable<SurveyQuestion> questions = await _surveyQuestionRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.SurveyAnswerType).Include(s => s.SurvayQuestionGroup),
                where: x => x.SurvayQuestionGroup.MainSurveyId == ThisSurveySuppliersOtherData.FirstOrDefault().MainSurveyId
                );

            survey.SurveyQuestions = questions;



            return PartialView(survey);
        }


    }
}