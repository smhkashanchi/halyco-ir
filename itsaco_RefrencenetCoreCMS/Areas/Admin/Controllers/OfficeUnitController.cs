﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.BasicInformation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize]
    [DisplayName("واحد اداری")]
    public class OfficeUnitController : Controller
    {
        public IGenericRepository<OfficeUnit> _officeUnit;
        public OfficeUnitController(IGenericRepository<OfficeUnit> officeUnit)
        {
           _officeUnit =officeUnit;
        }
        [DisplayName("صفحه نخست ")]
        public IActionResult Index()
        {
            return View();
        }
        [DisplayName("لیست واحدهای اداری ")]
        public async Task<IActionResult> ViewAll(string id)
        {
            return PartialView(await _officeUnit.GetAllAsync(null, x => x.OrderByDescending(s => s.OfficeUnitID)));
        }
        [DisplayName("افزودن  ")]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(OfficeUnit officeUnit)
        {
            if (officeUnit != null)
            {
                await _officeUnit.AddAsync(officeUnit);
                await _officeUnit.SaveChangesAsync();
                return Json(officeUnit);
            }
            return View(officeUnit);
        }
        [DisplayName("ویرایش  ")]
        public async Task<IActionResult> Edit(short id)
        {
            return View(await _officeUnit.GetByIdAsync(id));
        }
        [HttpPost]
        public async Task<IActionResult> Edit(OfficeUnit officeUnit)
        {
            if (officeUnit != null)
            {
               _officeUnit.Update(officeUnit);
                await _officeUnit.SaveChangesAsync();
                return Json(officeUnit);
            }
            return View(officeUnit);
        }
        public async Task<IActionResult> Delete(short id)
        {
            var officeUnit = await _officeUnit.GetByIdAsync(id);
            if (officeUnit != null)
            {
               _officeUnit.Delete(officeUnit);
                await _officeUnit.SaveChangesAsync();
                return Json(new { status = "ok", title =officeUnit.OfficeUnitTitle });
            }
            return Json("null");
        }
    }
}