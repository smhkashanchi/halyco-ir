﻿using DomainClasses.Product;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ModernProject.ViewComponents
{
    public class ShowProductMaxSale:ViewComponent
    {
        public IGenericRepository<ProductInfo> _mainProductsRepository;
        public ShowProductMaxSale(IGenericRepository<ProductInfo>  mainProductsRepository)
        {
            _mainProductsRepository = mainProductsRepository;
        }
        public async Task<IViewComponentResult> InvokeAsync(string id)
        {
            var products = await _mainProductsRepository.GetWithIncludeAsync<ProductInfo>
                (selector: x=>x,
                 where:x=>x.MainProduct.IsActive,
                 include:x=>x.Include(s=>s.ProductColor));
            return await Task.FromResult((IViewComponentResult)View("ShowProductMaxSale", products.Take(10)));
        }
    }
}
