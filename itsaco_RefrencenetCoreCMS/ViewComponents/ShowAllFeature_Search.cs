﻿using DomainClasses.Product;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ModernProject.ViewComponents
{
    public class ShowAllFeature_Search:ViewComponent
    {
        public IGenericRepository<Feature> _featureRepository;
        public ShowAllFeature_Search(IGenericRepository<Feature> featureRepository)
        {
            _featureRepository = featureRepository;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return await Task.FromResult((IViewComponentResult)View("ShowAllFeature_Search", await _featureRepository.GetAllAsync(x=>x.ProductsGroup.Lang== "fa")));
        }
    }
}
