﻿using DataLayer;
using DomainClasses.Connection;
using DomainClasses.Customer;
using DomainClasses.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ModernProject.ViewComponents
{
    public class ConnectionViewComponent:ViewComponent
    {
        private ApplicationDbContext _context;
        private UserManager<ApplicationUser> _userManager;
        private IGenericRepository<Connection> _connectionRepository;
        public ConnectionViewComponent(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IGenericRepository<Connection> connectionRepository)
        {
            _context = context;
            _connectionRepository = connectionRepository;
        }

        public async Task<IViewComponentResult> InvokeAsync(string id)
        {
            var list=await _connectionRepository.GetAllAsync(x => x.ConnectionGroupId == 1, orderBy: x => x.OrderByDescending(s => s.Connection_ID));
            return await Task.FromResult((IViewComponentResult)View("Default", list));
        }
    }
}
