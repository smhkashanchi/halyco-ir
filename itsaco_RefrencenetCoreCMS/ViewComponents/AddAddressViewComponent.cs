﻿using DataLayer;
using DomainClasses.Customer;
using DomainClasses.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ModernProject.ViewComponents
{
    public class AddAddressViewComponent:ViewComponent
    {
        private ApplicationDbContext _context;
        private UserManager<ApplicationUser> _userManager;
        public AddAddressViewComponent(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(string id)
        {
            ViewData["Province"] = new SelectList(_context.State_tb, "State_ID", "StateName");
            CustomerAddress customerAddress = new CustomerAddress();
            return await Task.FromResult((IViewComponentResult)View("AddAddress",customerAddress));
        }
    }
}
