﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Connection;
using DomainClasses.Contents;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories;
using ViewModels;

namespace itsaco_RefrencenetCoreCMS.ViewComponents
{
    public class FooterViewComponent : ViewComponent
    {
        public IGenericRepository<Contents> _contentsRepository;
        public IGenericRepository<Connection> _connectionRepository;

        public FooterViewComponent(IGenericRepository<Contents> contentsRepository, IGenericRepository<Connection> connectionRepository)
        {
            _connectionRepository = connectionRepository;
            _contentsRepository = contentsRepository;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            HalycoViewModel HVM = new HalycoViewModel();
            if (CultureInfo.CurrentCulture.Name == "fa-IR")
            {
                Contents aboutusSummary = await _contentsRepository.GetByIdAsync(-1);
                HVM.aboutUsSummmary = aboutusSummary.ContentSummary;
            }
            else if (CultureInfo.CurrentCulture.Name == "en-US")
            {
                Contents aboutusSummary = await _contentsRepository.GetByIdAsync(-14);
                HVM.aboutUsSummmary = aboutusSummary.ContentSummary;
            }
            if (CultureInfo.CurrentCulture.Name == "fa-IR")
            {
                var Connections = await _connectionRepository.GetAllAsync(x => x.ConnectionGroupId == 1);
                HVM.connections = Connections;
            }
            else if (CultureInfo.CurrentCulture.Name == "en-US")
            {
                var Connections = await _connectionRepository.GetAllAsync(x => x.ConnectionGroupId == 4);
                HVM.connections = Connections;
            }

            return await Task.FromResult((IViewComponentResult)View("Footer", HVM));

        }
    }
}
