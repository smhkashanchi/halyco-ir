﻿using DomainClasses;
using DomainClasses.User;
using itsaco_RefrencenetCoreCMS.Models.ManageViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModels.Manage;

namespace Project_Management.ViewComponents
{

    public class ChangeUsernameViewComponent : ViewComponent
    {
        private readonly UserManager<ApplicationUser> _userManager;
        public ChangeUsernameViewComponent(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }
        [TempData]
        public string StatusMessage { get; set; }
        public async Task<IViewComponentResult> InvokeAsync()
        {


             var user = await _userManager.GetUserAsync(UserClaimsPrincipal);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(UserClaimsPrincipal)}'.");
            }

            var model = new ChangeUserNameViewModel();
            return await Task.FromResult((IViewComponentResult)View("Default",model));

        }
    }
}
