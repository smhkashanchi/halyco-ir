﻿using AspCorePanelProject.Areas.Admin.Controllers;
using DomainClasses.Product;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace ModernProject.ViewComponents
{
    public class MenuProductGroup : ViewComponent
    {
        public IGenericRepository<ProductsGroup> _productsGroupRepository;
        public MenuProductGroup(IGenericRepository<ProductsGroup> productsGroupRepository)
        {
            _productsGroupRepository = productsGroupRepository;
        }
        public async Task<IViewComponentResult> InvokeAsync(string id)
        {
            var menu = GenerateMenu(await _productsGroupRepository.GetAllAsync(x => x.Lang == "fa"), new StringBuilder());
            ViewBag.Menu = menu;
            return await Task.FromResult((IViewComponentResult)View("MenuProductGroup", menu));
        }
        public  string GenerateMenu(IEnumerable<ProductsGroup> parrents, StringBuilder oStringBuilder)
        {
            var root_Parrent = parrents.FirstOrDefault(x => x.ParentId == null);

            var main_menu = $"<a href='product.html' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'>{root_Parrent.ProductGroupTitle}<span class='caret'></span></a>";
            main_menu+= "<ul class='dropdown-menu' role='menu' id='update_menu2'>";
            var main_parent = parrents.Where(x => x.ParentId == root_Parrent.ProductGroup_ID).ToList();
            if (parrents.Count() > 0)
            {
                foreach (var parrent in main_parent)
                {
                    string l = "";
                    var submenu = parrents.Where(x => x.ParentId == parrent.ProductGroup_ID).ToList();
                    string line = "";
                    if (submenu.Count() > 0)
                    {
                        line = String.Format(@"<li class='dropdown'> <a href='#' >{0} <span class='caret'></span> </a>", parrent.ProductGroupTitle, parrent.ProductGroupTitle.ToSlug());
                        // db.Groups.Where(x => x.ParentID == parrent.GroupID).ToList();

                        l = "<ul class='dropdown-menu dropdownhover-left'>";
                        foreach (var sub in submenu)
                        {
                            l += String.Format(@"<li><a href='/Product/Index/{1}'>{0} </a></li>", sub.ProductGroupTitle, sub.ProductGroupTitle.ToSlug());

                        }
                        l += "</ul>";
                  

                    }
                    else
                    {
                        line = String.Format(@"<li class='dropdown'> <a href='/Product/Index/{1}' >{0}  </a>", parrent.ProductGroupTitle, parrent.ProductGroupTitle.ToSlug());

                    }

                    line = line + l + "</li>";
                    //}
                    main_menu += line;
                }
            }
            //oStringBuilder.Append("</li>");
            main_menu += "</ul>";
            return main_menu.ToString();
        }
    }
}
