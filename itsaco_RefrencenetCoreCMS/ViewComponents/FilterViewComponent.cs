﻿using DomainClasses.Product;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModels;

namespace ModernProject.ViewComponents
{
    public class FilterViewComponent:ViewComponent
    {
        private readonly  IGenericRepository<Color> _colorRepository;
        private readonly IGenericRepository<Feature> _featureRepository;
        private readonly IGenericRepository<ProductsGroup> _productsGroupRepository;

        public FilterViewComponent(IGenericRepository<Color> colorRepository, IGenericRepository<Feature> featureRepository
            , IGenericRepository<ProductsGroup> productsGroupRepository)
        {
            _colorRepository = colorRepository;
            _productsGroupRepository = productsGroupRepository;
            _featureRepository = featureRepository;

        }
        public async Task<IViewComponentResult> InvokeAsync(string groupId)
        {
            var list = new FilterMenuViewModel();
            list.Colors = await _colorRepository.GetAllAsync(x=>x.Lang == "fa",orderBy:x=>x.OrderByDescending(s=>s.ColorTitle)) ;
            list.Features =await _featureRepository.GetAllAsync(x=>x.ProductsGroup.Lang=="fa" && x.IsActive) ;
            list.Groups =await _productsGroupRepository.GetAllAsync(x=>x.Lang=="fa",orderBy:s=>s.OrderByDescending(x=>x.ProductGroup_ID)) ;
            //var groups=_productsGroupRepository.GetAllProductsGroupByLang("fa");
            //var group= groups.Where(x => x.ProductGroupTitle == groupId.ToTitle()).FirstOrDefault();
            //list.Selected_GroupId = Convert.ToInt32(group.ProductGroup_ID);
            return await Task.FromResult((IViewComponentResult)View("Filter",list));
        }
    }
}
