﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DomainClasses.Contents;
using Services.Repositories;
using ViewModels;
using Utilities;
using System.Globalization;
using Microsoft.EntityFrameworkCore;

namespace itsaco_RefrencenetCoreCMS.ViewComponents
{

    public class MenuViewComponent:ViewComponent
    {
        public IGenericRepository<Contents> _ContentsRepository;
        public MenuViewComponent(IGenericRepository<Contents> ContentsRepository)
        {
            _ContentsRepository = ContentsRepository;
        }

        public async Task<IViewComponentResult> InvokeAsync(string groupId)
        {
            var staticPageList = await _ContentsRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.contentsGroup),
                where: x => x.contentsGroup.Lang == CultureInfo.CurrentCulture.Name && x.IsActive == true
                );
                //GetAllAsync(x => x.contentsGroup.Lang==CultureInfo.CurrentCulture.Name && x.IsActive == true);
            HalycoViewModel HVM = new HalycoViewModel();
            Dictionary<int, string> staticPage_dic = new Dictionary<int, string>();
            foreach (var item in staticPageList)
            {
                staticPage_dic.Add(item.Content_ID, item.ContentTitle.ToSlug());
            }

            HVM.staticPage_dic = staticPage_dic;
            return await Task.FromResult((IViewComponentResult)View("Menu", HVM));
        }
    }
}
