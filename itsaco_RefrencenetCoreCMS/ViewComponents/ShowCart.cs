﻿using DomainClasses.Product;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModels;

namespace ModernProject.ViewComponents
{
    public class ShowCart : ViewComponent
    {
        public IGenericRepository<ProductInfo> _mainProductsRepository;
        public ShowCart(IGenericRepository<ProductInfo> mainProductsRepository)
        {
            _mainProductsRepository = mainProductsRepository;
        }
        public async Task<IViewComponentResult> InvokeAsync(string id)
        {
            List<ShopCartItemViewModel> list = new List<ShopCartItemViewModel>();

            if (HttpContext.Session.GetString("ShopCart") != null)
            {
                var listStr = HttpContext.Session.GetString("ShopCart");
                var listObj = JsonConvert.DeserializeObject<List<ShoppingCartItems>>(listStr);
                List<ShoppingCartItems> listShop = (List<ShoppingCartItems>)listObj;

                foreach (var item in listShop)
                {
                    var product = await _mainProductsRepository.GetByIdAsync(item.ProductID);
                    var p = product.ProductColor.OrderBy(x => x.ProductPrice).FirstOrDefault();
                    list.Add(new ShopCartItemViewModel()
                    {
                        Count = item.Count,
                        ProductID = item.ProductID,
                        Title = product.ProductTitle,
                        ImageName = product.MainProduct.ProductImage,
                        ProductPrice = p.ProductPrice
                        ,
                        EndPrice = item.Price
                    });
                }
            }
            return await Task.FromResult((IViewComponentResult)View("ShowCart", list));
        }
    }
}
