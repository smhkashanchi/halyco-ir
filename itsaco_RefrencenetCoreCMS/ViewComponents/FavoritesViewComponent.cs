﻿using DomainClasses;
using DomainClasses.User;
using itsaco_RefrencenetCoreCMS.Models.ManageViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Management.ViewComponents
{

    public class FavoritesViewComponent : ViewComponent
    {
        private readonly UserManager<ApplicationUser> _userManager;
        public FavoritesViewComponent(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }
        [TempData]
        public string StatusMessage { get; set; }
        public async Task<IViewComponentResult> InvokeAsync()
        {

            return await Task.FromResult((IViewComponentResult)View("Default"));

        }
    }
}
