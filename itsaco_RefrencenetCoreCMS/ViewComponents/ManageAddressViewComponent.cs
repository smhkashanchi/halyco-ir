﻿using DataLayer;
using DomainClasses.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ModernProject.ViewComponents
{
    public class ManageAddressViewComponent:ViewComponent
    {
        private ApplicationDbContext _db;
        private UserManager<ApplicationUser> _userManager;
        public ManageAddressViewComponent(ApplicationDbContext db, UserManager<ApplicationUser> userManager)
        {
            _db = db;
            _userManager = userManager;
        }

        public async Task<IViewComponentResult> InvokeAsync(string id)
        {
            var user = await _userManager.GetUserAsync(UserClaimsPrincipal);
            var customer = _db.Customer_tb.Where(x => x.ApplicationUser.Email == user.Email).FirstOrDefault();
            var applicationDbContext = _db.CustomerAddress_tb.Include(c => c.Country).Include(x => x.City).Include(x => x.State).Include(c => c.Customer);
            var list = applicationDbContext.Where(x => x.Customer_Id == customer.Customer_ID);
            return await Task.FromResult((IViewComponentResult)View("ManageAddress",list));
        }
    }
}
