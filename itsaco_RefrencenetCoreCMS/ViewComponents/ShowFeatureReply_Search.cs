﻿using DomainClasses.Product;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ModernProject.ViewComponents
{
    public class ShowFeatureReply_Search:ViewComponent
    {
        private readonly IGenericRepository<FeatureReply> _featureReplyRepository;
        public ShowFeatureReply_Search(IGenericRepository<FeatureReply> featureReplyRepository)
        {
            _featureReplyRepository = featureReplyRepository;
        }
        public async Task<IViewComponentResult> InvokeAsync(int id)
        {
            var featuresReply = await _featureReplyRepository.GetWithIncludeAsync<FeatureReply>
                (selector: x => x,
                where: x => x.FeatureId == id,
                include: x => x.Include(s => s.Feature)
                );
            return await Task.FromResult((IViewComponentResult)View("ShowFeatureReply_Search", featuresReply));
        }
    }
}
