﻿using DomainClasses.Customer;
using DomainClasses.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ModernProject.ViewComponents
{
    public class ShowProfile : ViewComponent
    {
        private readonly UserManager<ApplicationUser> _userManager;
        public IGenericRepository<Customer> _customerRepository;
        public ShowProfile( IGenericRepository<Customer> customerRepository, UserManager<ApplicationUser> userManager)
        {
            _customerRepository = customerRepository;
            _userManager = userManager;
        }
        public async Task<IViewComponentResult> InvokeAsync(string id)
        {
            var user = await _userManager.GetUserAsync(UserClaimsPrincipal);
            var customer =await _customerRepository.GetAsync(x=>x.ApplicationUser.Email== user.Email);

            return await Task.FromResult((IViewComponentResult)View("ShowProfile", customer));
        }
    }
}
