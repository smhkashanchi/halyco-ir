﻿using DataLayer;
using DomainClasses.Cart;
using DomainClasses.Customer;
using DomainClasses.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ModernProject.ViewComponents
{
    public class ShowFactorsViewComponent : ViewComponent
    {
        private readonly UserManager<ApplicationUser> _userManager;

        private readonly ApplicationDbContext _db;
        private readonly IGenericRepository<Customer> _customerRepository;
        private readonly IGenericRepository<Factor> _factorRepository;

        public ShowFactorsViewComponent(ApplicationDbContext db, UserManager<ApplicationUser> userManager
            ,IGenericRepository<Customer> customerRepository, IGenericRepository<Factor> factorRepository)
        {
            _userManager = userManager;
            _customerRepository = customerRepository;
            _db = db;
            _factorRepository = factorRepository;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var user = await _userManager.GetUserAsync(UserClaimsPrincipal);
            var customer = await _customerRepository.GetAsync(s => s.ApplicationUser.Email == user.Email);
           
            var factors =await _factorRepository.GetWithIncludeAsync<Factor>(
                selector:x=>x,
                include : x=>x.Include(s=>s.CustomerAddress).Include(s=>s.Customer).Include(s=>s.FactorDetails).Include(s=>s.SendType),
                where:x=>x.Customer_Id==customer.Customer_ID
                );

            return await Task.FromResult((IViewComponentResult)View("ShowFactors",factors));
        }
    }
}
