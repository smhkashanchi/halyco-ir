﻿using DomainClasses.Product;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ModernProject.ViewComponents
{
    public class ShowAllProductsNew:ViewComponent  

    {
        public IGenericRepository<ProductInfo> _mainProductsRepository;
        public ShowAllProductsNew(IGenericRepository<ProductInfo>  mainProductsRepository)
        {
            _mainProductsRepository = mainProductsRepository;
        }
        public async Task<IViewComponentResult> InvokeAsync(string id)
        {
            var products = await _mainProductsRepository.GetWithIncludeAsync<ProductInfo>
                (
                selector: x => x,
                where: x => x.MainProduct.IsActive == true
                , include:x=>x.Include(s=>s.ProductColor),
                orderBy:x=>x.OrderByDescending(s=>s.ProductInfo_ID)
                );
            return await Task.FromResult((IViewComponentResult)View("ShowAllProductsNew", products.Take(10)));
        }
    }
}
