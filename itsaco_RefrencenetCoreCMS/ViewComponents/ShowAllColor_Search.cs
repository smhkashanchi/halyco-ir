﻿using DomainClasses.Product;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ModernProject.ViewComponents
{
    public class ShowAllColor_Search:ViewComponent
    {
        public IGenericRepository<Color> _colorRepository;
        public ShowAllColor_Search(IGenericRepository<Color> colorRepository)
        {
            _colorRepository = colorRepository;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return await Task.FromResult((IViewComponentResult)View("ShowAllColor_Search", _colorRepository.GetAllAsync(x=>x.Lang== "fa")));
        }
    }
}
