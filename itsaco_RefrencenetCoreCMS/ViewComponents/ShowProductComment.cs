﻿using DomainClasses.Product;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ModernProject.ViewComponents
{
    public class ShowProductComment:ViewComponent
    {
        
        public IGenericRepository<MainProduct> _mainProductsRepository;
        public IGenericRepository<ProductComment> _productCommentRepository;
        public ShowProductComment(IGenericRepository<MainProduct> mainProductsRepository, IGenericRepository<ProductComment> productCommentRepository)
        {
            _mainProductsRepository = mainProductsRepository;
            _productCommentRepository = productCommentRepository;
        }
        public async Task<IViewComponentResult> InvokeAsync(int id)
        {
            var comments =await _productCommentRepository.GetAllAsync(x=>x.ProductInforId ==  id && x.IsConfirmed);
            return await Task.FromResult((IViewComponentResult)View("ShowProductComment",comments ));
        }
    }
}
