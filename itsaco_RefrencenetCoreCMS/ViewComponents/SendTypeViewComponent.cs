﻿using DataLayer;
using DomainClasses.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ModernProject.ViewComponents
{
    public class SendTypeViewComponent:ViewComponent
    {
        private ApplicationDbContext _context;
        private UserManager<ApplicationUser> _userManager;
        public SendTypeViewComponent(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(string id)
        {
            var list = _context.SendType_tb.ToList();
            return await Task.FromResult((IViewComponentResult)View("SendType",list));
        }
    }
}
