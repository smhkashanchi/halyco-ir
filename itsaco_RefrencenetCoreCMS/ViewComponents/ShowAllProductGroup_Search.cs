﻿using DomainClasses.Product;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ModernProject.ViewComponents
{
    public class ShowAllProductGroup_Search:ViewComponent
    {
        public IGenericRepository<ProductsGroup> _productsGroupRepository;
        public ShowAllProductGroup_Search(IGenericRepository<ProductsGroup> productsGroupRepository)
        {
            _productsGroupRepository = productsGroupRepository;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return await Task.FromResult((IViewComponentResult)View("ShowAllProductGroup_Search", _productsGroupRepository.GetAllAsync(x=>x.ParentId== 1)));
        }
    }
}
