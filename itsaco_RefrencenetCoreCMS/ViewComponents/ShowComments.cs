﻿using DomainClasses.Contents;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities;

namespace ViewComponents
{
    public class ShowComments:ViewComponent
    {
        public IGenericRepository<ContentsComments> _contentsCommentRepository;
        public IGenericRepository<Contents> _contentsRepository;
        public ShowComments(IGenericRepository<ContentsComments> contentsCommentRepository, IGenericRepository<Contents> contentsRepository)
        {
            _contentsCommentRepository = contentsCommentRepository;
            _contentsRepository = contentsRepository;
        }
        public async Task<IViewComponentResult> InvokeAsync(string id)
        {
            var content =await _contentsRepository.GetAsync(x=>x.ContentTitle== id.ToTitle());
            var contentComment =await _contentsCommentRepository.GetWithIncludeAsync<ContentsComments>
                (selector: x => x,
                where: x => x.ContentId == content.Content_ID && x.IsConfirmed
                , include: x => x.Include(s => s.Contents));
                
            return await Task.FromResult((IViewComponentResult)View("ShowComments", contentComment));
        }
    }
}
