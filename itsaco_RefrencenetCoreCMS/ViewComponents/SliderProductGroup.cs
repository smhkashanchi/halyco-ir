﻿using DomainClasses.Product;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ModernProject.ViewComponents
{
    public class SliderProductGroup:ViewComponent
    {
        public IGenericRepository<ProductsGroup> _productsGroupRepository;
        public SliderProductGroup(IGenericRepository<ProductsGroup> productsGroupRepository)
        {
            _productsGroupRepository = productsGroupRepository;
        }
        public async Task<IViewComponentResult> InvokeAsync(string id)
        {
            return await Task.FromResult((IViewComponentResult)View("SliderProductGroup",await _productsGroupRepository.GetAllAsync(x=>x.Lang==id,
                orderBy:s=>s.OrderByDescending(x=>x.ParentId))));
        }
    }
}
