﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.AVF;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Controllers
{
    public class CatalogController : Controller
    {
        public IGenericRepository<AVF> _aVFRepository;

        public CatalogController(IGenericRepository<AVF> aVFRepository)
        {
            _aVFRepository = aVFRepository;
        }
        public async Task<IActionResult> Index()
        {
            if (CultureInfo.CurrentCulture.Name == "fa-IR")
            {
                var CatalogList = await _aVFRepository.GetWithIncludeAsync(
                    selector: x => x,
                    include: s => s.Include(x => x.AVFGroup),
                    where: x => x.AVFGroupId == 15 && x.IsActive == true,
                    orderBy: x => x.OrderByDescending(s => s.AVF_ID)
                     );

                return View(CatalogList);
            }
            else
            {
                var CatalogList = await _aVFRepository.GetWithIncludeAsync(
                  selector: x => x,
                  include: s => s.Include(x => x.AVFGroup),
                  where: x => x.AVFGroupId == 23 && x.IsActive == true,
                  orderBy: x => x.OrderByDescending(s => s.AVF_ID)
                   );

                return View(CatalogList);
            }
        }
    }
}