﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Contents;
using DomainClasses.Product;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;
using ViewModels;

namespace ModernProject.Controllers
{
    public class SearchController : Controller
    {
        public IGenericRepository<ProductInfo> _mainProductsRepository;
        public IGenericRepository<Contents> _contentsRepository;
        public SearchController(IGenericRepository<ProductInfo> mainProductsRepository, IGenericRepository<Contents> contentsRepository)
        {
            _mainProductsRepository = mainProductsRepository;
            _contentsRepository = contentsRepository;
        }
        public async Task<IActionResult> Index(string id)
        {
            List<Contents> contentsList = new List<Contents>();
            List<ProductInfo> productsList = new List<ProductInfo>();
            ViewBag.Title = id;
            var products = await _mainProductsRepository.GetAllAsync(x => x.ProductTitle.Contains(id) || x.Description.Contains(id));
            productsList.AddRange(products.ToList());
            ViewBag.ProductList = productsList;

             var contents = await _contentsRepository.GetAllAsync(x => x.ContentTitle.Contains(id) || x.ContentSummary.Contains(id) || x.ContentText.Contains(id));
            contentsList.AddRange(contents.ToList());
            ViewBag.ContentList = contentsList;

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> ArchiveSearch(string pnameCode, string pColor, string pgProduct, string pf)
        {
            //var productByPgProduct = "";
            string[] pgId = pgProduct.Split("#");
            for (int i = 0; i < pgId.Length - 1; i++)
            {
                var product = await _mainProductsRepository.GetWithIncludeAsync<ProductInfo>(selector: x => x,
                    where: x => x.ProductGroupId == Int32.Parse(pgId[i]) && x.MainProduct.IsActive,
                    include: s => s.Include(x => x.MainProduct));
            }



            return View();
        }
    }
}