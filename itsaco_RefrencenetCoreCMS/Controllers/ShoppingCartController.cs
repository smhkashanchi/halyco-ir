﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Product;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Services.Repositories;
using ViewModels;

namespace ModernProject.Controllers
{
    public class ShoppingCartController : Controller
    {
        public IGenericRepository<ProductInfo> _mainProductsRepository;
        public ShoppingCartController(IGenericRepository<ProductInfo>  mainProductsRepository)
        {
            _mainProductsRepository = mainProductsRepository;
        }
        public async Task<IActionResult> ShowCart()
        {
            List<ShopCartItemViewModel> list = new List<ShopCartItemViewModel>();
          
            if (HttpContext.Session.GetString("ShopCart") != null)
            {
                var listStr = HttpContext.Session.GetString("ShopCart");
                var listObj = JsonConvert.DeserializeObject<List<ShoppingCartItems>>(listStr);
                List<ShoppingCartItems> listShop = (List<ShoppingCartItems>)listObj;

                foreach (var item in listShop)
                {
                    var products =await _mainProductsRepository.GetWithIncludeAsync<ProductInfo>(where:x=>x.ProductInfo_ID == item.ProductID,
                        include:x=>x.Include(s=>s.MainProduct),selector:x=>x);
                    var product = products.FirstOrDefault();
                    var p = product.ProductColor.OrderBy(x=>x.ProductPrice).FirstOrDefault();
                    list.Add(new ShopCartItemViewModel()
                    {
                        Count = item.Count,
                        ProductID = item.ProductID,
                        Title = product.ProductTitle,
                        ImageName = product.MainProduct.ProductImage,
                        ProductPrice = p.ProductPrice
                        ,EndPrice=item.Price
                    });
                }
            }
            
            return PartialView(list);
        }
        public ActionResult EmptyCart()
        {
            List<ShoppingCartItems> list = new List<ShoppingCartItems>();
            var listStr2 = JsonConvert.SerializeObject(list);
            HttpContext.Session.SetString("ShopCart", listStr2);


            return Json(new { res = true });
        }

        public async Task<IActionResult> DeleteItem(int id)
        {
          

            List<ShoppingCartItems> list = new List<ShoppingCartItems>();

            var listStr = HttpContext.Session.GetString("ShopCart");
            var listObj = JsonConvert.DeserializeObject<List<ShoppingCartItems>>(listStr);
            List<ShoppingCartItems> listShop = (List<ShoppingCartItems>)listObj;
      

            listShop.Remove(listShop.Find(x=>x.ProductID== id));

            var listStr2 = JsonConvert.SerializeObject(listShop);
            HttpContext.Session.SetString("ShopCart", listStr2);


            return Json(new { res = true,id=id });
        }
        public ActionResult PaymentConfirm()
        {
            return View();
        }
    }
}