﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories;
using DomainClasses.Supporter;

namespace itsaco_RefrencenetCoreCMS.Controllers
{
    public class CertificateController : Controller
    {
        public IGenericRepository<CoWorker> _CoWorkerRepository;

        public CertificateController(IGenericRepository<CoWorker> CoWorkerRepository)
        {
            _CoWorkerRepository = CoWorkerRepository;
        }

        public async Task<IActionResult> Index()
        {
            var certificateList = await _CoWorkerRepository.GetAllAsync(x => x.IsActive == true, orderBy: x => x.OrderByDescending(s => s.CoWorker_ID));

            return View(certificateList);
        }
    }
}