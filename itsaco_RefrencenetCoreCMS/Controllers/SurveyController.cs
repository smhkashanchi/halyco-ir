﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

using DomainClasses.Product;
using DomainClasses.Survey;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using Services.Repositories;

using ViewModels;

namespace itsaco_RefrencenetCoreCMS.Controllers
{
    public class SurveyController : Controller
    {
        public static Int16 OtherData_ID;
        public IGenericRepository<MainSurvey> _mainSurveyRepository;
        public IGenericRepository<SurvayQuestionGroup> _survayQuestionGroupRepository;
        public IGenericRepository<SurveyQuestion> _survayQuestionRepository;
        public IGenericRepository<SurveySuppliersOtherData> _surveySuppliersOtherDataRepository;
        public IGenericRepository<SurveyCustomerOtherData> _surveyCustomerOtherDataRepository;
        public IGenericRepository<SurveyQuestionReply> _surveyQuestionReplyRepository;
        public IGenericRepository<ProductInfo> _productInfoRepository;
        public IGenericRepository<CustomerSurveyProducts> _customerSurveyProductsRepository;
        public SurveyController(IGenericRepository<CustomerSurveyProducts> customerSurveyProductsRepository, IGenericRepository<SurveyCustomerOtherData> surveyCustomerOtherDataRepository, IGenericRepository<SurveyQuestion> survayQuestionRepository, IGenericRepository<MainSurvey> mainSurveyRepository, IGenericRepository<SurvayQuestionGroup> survayQuestionGroupRepository, IGenericRepository<SurveySuppliersOtherData> surveySuppliersOtherDataRepository, IGenericRepository<SurveyQuestionReply> surveyQuestionReplyRepository, IGenericRepository<ProductInfo> productInfoRepository)
        {
            _mainSurveyRepository = mainSurveyRepository;
            _survayQuestionGroupRepository = survayQuestionGroupRepository;
            _survayQuestionRepository = survayQuestionRepository;
            _surveySuppliersOtherDataRepository = surveySuppliersOtherDataRepository;
            _surveyQuestionReplyRepository = surveyQuestionReplyRepository;
            _productInfoRepository = productInfoRepository;
            _surveyCustomerOtherDataRepository = surveyCustomerOtherDataRepository;
            _customerSurveyProductsRepository = customerSurveyProductsRepository;
        }
        public async Task<IActionResult> Index()
        {
            var mainSurvey = await _mainSurveyRepository.GetAllAsync(x => x.SurveyType == 0 && x.IsActive, x => x.OrderByDescending(s => s.MainSurvey_ID));
            if (mainSurvey.Count() > 0)
            {
                ViewData["SupplierSurveyName"] = mainSurvey.FirstOrDefault().Title;
            }
            return View();
        }

        [Route("/CustomerSurvey")]
        public async Task<IActionResult> CustomerSurvey()
        {
            var mainSurvey = await _mainSurveyRepository.GetAllAsync(x => x.SurveyType == 1 && x.IsActive, x => x.OrderByDescending(s => s.MainSurvey_ID));
            if (mainSurvey.ToList().Count() > 0)
            {
                ViewData["CustomerSurveyName"] = mainSurvey.FirstOrDefault().Title;
            }
            if (CultureInfo.CurrentCulture.Name == "fa-IR")
            {
                ViewBag.Products = await _productInfoRepository.GetWithIncludeAsync
               (
               selector: x => x,
               include: x => x.Include(s => s.MainProduct).Include(s => s.ProductsGroup),
               where: x => x.MainProduct.IsActive && x.ProductsGroup.Lang == "fa-IR"
               );
            }
            else
            {
                ViewBag.Products = await _productInfoRepository.GetWithIncludeAsync
               (
               selector: x => x,
               include: x => x.Include(s => s.MainProduct).Include(s => s.ProductsGroup),
               where: x => x.MainProduct.IsActive && x.ProductsGroup.Lang == "en-US"
               );
            }
            return View();
        }

        public async Task<IActionResult> ViewSupplierSurvey()
        {
            SurveyVM survey = new SurveyVM();
            var mainSurvey = await _mainSurveyRepository.GetAllAsync(x => x.SurveyType == 0 && x.IsActive, x => x.OrderByDescending(s => s.MainSurvey_ID));

            IEnumerable<SurvayQuestionGroup> quesGroup = await _survayQuestionGroupRepository.GetAllAsync(x => x.MainSurveyId == mainSurvey.FirstOrDefault().MainSurvey_ID);
            survey.SurvayQuestionGroup = quesGroup;

            IEnumerable<SurveyQuestion> questions = await _survayQuestionRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.SurveyAnswerType).Include(s => s.SurvayQuestionGroup),
                where: x => x.SurvayQuestionGroup.MainSurveyId == quesGroup.FirstOrDefault().MainSurveyId
                );

            survey.SurveyQuestions = questions;

            //questions.

            return PartialView(survey);
            //survey.
        }
        [HttpPost]
        public async Task<IActionResult> SurveySubmit(SurveySuppliersOtherData surveySuppliersOtherData)
        {
            try
            {
                surveySuppliersOtherData.CompleteFormDate = DateTime.Now;
                await _surveySuppliersOtherDataRepository.AddAsync(surveySuppliersOtherData);
                await _surveySuppliersOtherDataRepository.SaveChangesAsync();

                OtherData_ID = surveySuppliersOtherData.S_OtherData_ID;
                return Json("ok");
            }
            catch (Exception err)
            {
                throw err;
            }

        }

        [HttpPost]
        public async Task<IActionResult> GetSurveyData(List<string> questionId, List<string> selectedReply)
        {
            try
            {
                SurveyQuestionReply sqr = new SurveyQuestionReply();
                for (int i = 0; i < questionId.Count; i++)
                {

                    sqr.QuestionId = Convert.ToInt16(questionId[i]);

                    sqr.SelectedReply = Convert.ToByte(selectedReply[i]);
                    sqr.ExtraInformationId = OtherData_ID;

                    await _surveyQuestionReplyRepository.AddAsync(sqr);
                    await _surveyQuestionReplyRepository.SaveChangesAsync();


                    ////
                    ///
                    var thisQuestion = await _survayQuestionRepository.GetAsync(x => x.Question_ID == Convert.ToInt16(questionId[i]));

                    thisQuestion.ReplyCount += 1;
                    switch (selectedReply[i])
                    {
                        case "1":
                            thisQuestion.OptionVoteCount5 += 1;
                            break;
                        case "2":
                            thisQuestion.OptionVoteCount4 += 1;
                            break;
                        case "3":
                            thisQuestion.OptionVoteCount3 += 1;
                            break;
                        case "4":
                            thisQuestion.OptionVoteCount2 += 1;
                            break;
                        case "5":
                            thisQuestion.OptionVoteCount1 += 1;
                            break;
                        default:
                            break;
                    }

                    _survayQuestionRepository.Update(thisQuestion);
                    await _survayQuestionRepository.SaveChangesAsync();
                }
                return Json("ok");
            }
            catch (Exception err)
            {

                throw err;
            }
        }

        //----------------------------نظرسنجی مشتریان
        public async Task<IActionResult> ViewCustomerSurvey()
        {
            SurveyVM survey = new SurveyVM();
            var mainSurvey = await _mainSurveyRepository.GetAllAsync(x => x.SurveyType == 1 && x.IsActive, x => x.OrderByDescending(s => s.MainSurvey_ID));

            IEnumerable<SurvayQuestionGroup> quesGroup = await _survayQuestionGroupRepository.GetAllAsync(x => x.MainSurveyId == mainSurvey.FirstOrDefault().MainSurvey_ID);
            survey.SurvayQuestionGroup = quesGroup;

            IEnumerable<SurveyQuestion> questions = await _survayQuestionRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.SurveyAnswerType).Include(s => s.SurvayQuestionGroup),
                where: x => x.SurvayQuestionGroup.MainSurveyId == quesGroup.FirstOrDefault().MainSurveyId
                );

            survey.SurveyQuestions = questions;

            //questions.

            return PartialView(survey);
            //survey.
        }

        [HttpPost]
        public async Task<IActionResult> CustomerSurveySubmit(SurveyCustomerOtherData surveyCustomerOtherData, List<string> product)
        {
            try
            {
                surveyCustomerOtherData.CompeletFromDate = DateTime.Now;
                await _surveyCustomerOtherDataRepository.AddAsync(surveyCustomerOtherData);
                await _surveyCustomerOtherDataRepository.SaveChangesAsync();

                OtherData_ID = surveyCustomerOtherData.C_OtherData_ID;

                if (product.Count > 0)
                {
                    for (int i = 0; i < product.Count; i++)
                    {
                        CustomerSurveyProducts csp = new CustomerSurveyProducts
                        {
                            CustomerSurveyId = OtherData_ID,
                            ProductId = Convert.ToInt32(product[i])
                        };
                        await _customerSurveyProductsRepository.AddAsync(csp);
                        await _customerSurveyProductsRepository.SaveChangesAsync();
                    }
                }
                return Json("ok");
            }
            catch (Exception err)
            {
                throw err;
            }

        }

        [HttpPost]
        public async Task<IActionResult> GetCustomerSurveyData(List<string> questionId, List<string> selectedReply)
        {
            try
            {
                SurveyQuestionReply sqr = new SurveyQuestionReply();
                for (int i = 0; i < questionId.Count; i++)
                {

                    sqr.QuestionId = Convert.ToInt16(questionId[i]);

                    sqr.SelectedReply = Convert.ToByte(selectedReply[i]);
                    sqr.ExtraInformationId = OtherData_ID;

                    await _surveyQuestionReplyRepository.AddAsync(sqr);
                    await _surveyQuestionReplyRepository.SaveChangesAsync();


                    ////
                    ///
                    var thisQuestion = await _survayQuestionRepository.GetAsync(x => x.Question_ID == Convert.ToInt16(questionId[i]));

                    thisQuestion.ReplyCount += 1;
                    switch (selectedReply[i])
                    {
                        case "1":
                            thisQuestion.OptionVoteCount5 += 1;
                            break;
                        case "2":
                            thisQuestion.OptionVoteCount4 += 1;
                            break;
                        case "3":
                            thisQuestion.OptionVoteCount3 += 1;
                            break;
                        case "4":
                            thisQuestion.OptionVoteCount2 += 1;
                            break;
                        case "5":
                            thisQuestion.OptionVoteCount1 += 1;
                            break;
                        default:
                            break;
                    }

                    _survayQuestionRepository.Update(thisQuestion);
                    await _survayQuestionRepository.SaveChangesAsync();
                }
                return Json("ok");
            }
            catch (Exception err)
            {

                throw err;
            }
        }
    }
}