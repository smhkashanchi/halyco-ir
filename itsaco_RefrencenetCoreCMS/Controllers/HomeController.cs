﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainClasses.Connection;
using DomainClasses.Contents;
using DomainClasses.Customer;
using DomainClasses.FAQ;
using DomainClasses.NewsLetters;
using DomainClasses.Product;
using DomainClasses.SlideShow;
using itsaco_RefrencenetCoreCMS.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;
using Utilities;
using ViewModels;

namespace AspCorePanelProject.Controllers
{
    public class HomeController : Controller
    {
        public IGenericRepository<SlideShow> _slideShowRepository;
        public IGenericRepository<ProductsGroup> _productsGroupRepository;
        public IGenericRepository<Contents> _contentsRepository;
        public IGenericRepository<Customer> _customerRepository;
        public IGenericRepository<Connection> _connectionRepository;
        public IGenericRepository<ProductInfo> _productInfoRepository;
        public IGenericRepository<FAQ> _faqRepository;
        public IGenericRepository<NewsLetter> _newsLetterRepository;
        public IHostingEnvironment _hostingEnvironment;

        public HomeController(IGenericRepository<SlideShow> slideShowRepository,
            IHostingEnvironment hostingEnvironment,
            IGenericRepository<ProductsGroup> productsGroupRepository,
            IGenericRepository<Contents> contentsRepository
            , IGenericRepository<Customer> customerRepository,
            IGenericRepository<Connection> connectionRepository,
            IGenericRepository<ProductInfo> productInfoRepository,
            IGenericRepository<FAQ> faqRepository,
            IGenericRepository<NewsLetter> newsLetterRepository)

        {
            _slideShowRepository = slideShowRepository;
            _productsGroupRepository = productsGroupRepository;
            _contentsRepository = contentsRepository;
            _customerRepository = customerRepository;
            _connectionRepository = connectionRepository;
            _productInfoRepository = productInfoRepository;
            _faqRepository = faqRepository;
            _newsLetterRepository = newsLetterRepository;
            _hostingEnvironment = hostingEnvironment;
        }
        public async Task<IActionResult> Index(int id)
        {
            // ViewBag.ShowLogin = id;
            HalycoViewModel HVM = new HalycoViewModel();
            var slideshowList = await _slideShowRepository.GetWithIncludeAsync
                (
                selector:x=>x,
                include:x=>x.Include(s=>s.slideShowGroup),
                where:x=>x.slideShowGroup.Lnag == CultureInfo.CurrentCulture.Name && x.IsActive
                );//.GetAllAsync(x => x.SSGId == 1 && x.IsActive == true, orderBy: x => x.OrderByDescending(s => s.SlideShowID));
            HVM.slideShows = slideshowList.OrderByDescending(s => s.SlideShowID);
           
            var productList = await _productInfoRepository.GetWithIncludeAsync(
                selector: x => x,
                include: x => x.Include(s => s.MainProduct).Include(s=>s.ProductsGroup),
                where: x => x.MainProduct.IsActive == true && x.ProductsGroup.Lang==CultureInfo.CurrentCulture.Name,
                orderBy: x => x.OrderByDescending(s => s.MainProduct.Product_ID));
            HVM.productInfos = productList;
            return View(HVM);
        }
        public IActionResult Index2()
        {
            return View();
        }
        public async Task<IActionResult> StaticPage(string id) {
            var staticPageContent =await _contentsRepository.GetAsync(x => x.ContentTitle == id.ToTitle());
            return View(staticPageContent);
        }

        [HttpPost]
        [Route("file_uploads")]
        public IActionResult UploadImage(IFormFile upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            if (upload.Length <= 0)
                return null;

            var fileName = Guid.NewGuid() + Path.GetExtension(upload.FileName).ToLower();



            var path = Path.Combine(
                _hostingEnvironment.WebRootPath, "CkImages",
                fileName);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                upload.CopyTo(stream);

            }



            var url = $"{"/CkImages/"}{fileName}";


            return Json(new { uploaded = true, url });
        }
        public async Task<IActionResult> FAQ()
        {
            var list = await _faqRepository.GetAllAsync();
            return View(list);
        }
        public IActionResult UpdateMenu()
        {
            //var menu = GenerateMenu(_productsGroupRepository.GetAllProductsGroupByLang("fa"), new StringBuilder());
            return Json(new { menu = "" });
        }
        public string GenerateMenu(IEnumerable<ProductsGroup> parrents, StringBuilder oStringBuilder)
        {
            // oStringBuilder.AppendLine("<li class='dropdown'>");
            var main_menu = "";
            var main_parent = parrents.Where(x => x.ParentId == null).ToList();
            if (parrents.Count() > 0)
            {
                foreach (var parrent in main_parent)
                {
                    string l = "";
                    string line = String.Format(@"<li class='dropdown'> <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button'>{0} <span class='caret'></span> </a>", parrent.ProductGroupTitle, parrent.ProductGroupTitle.ToSlug());
                    var submenu = parrents.Where(x => x.ParentId == parrent.ProductGroup_ID).ToList();// db.Groups.Where(x => x.ParentID == parrent.GroupID).ToList();
                    if (submenu.Count() > 0)
                    {
                        l = "<ul class='dropdown-menu dropdownhover-left'>";
                        foreach (var sub in submenu)
                        {
                            l += String.Format(@"<li><a href='{1}'>{0} </a></li>", sub.ProductGroupTitle, sub.ProductGroupTitle.ToSlug());

                        }
                        l += "</ul>";

                    }
                    else
                    {
                        oStringBuilder.Append(line);
                    }

                    line = line + l + "</li>";
                    //}
                    main_menu += line;
                }
            }
            //oStringBuilder.Append("</li>");

            return main_menu.ToString();
        }
        public async Task<IActionResult> Slider()
        {
            var sliders = await _slideShowRepository.GetAllAsync(x => x.SSGId == 1, x => x.OrderByDescending(s => s.SlideShowID));
            return PartialView(sliders);
        }

        public async Task<IActionResult> ShowContents()
        {
            return View();
        }

        public async Task<IActionResult> AboutUs()
        {

            return View();
        }

        public async Task<IActionResult> Search(string q)
        {
            //if (q != "")
            //{
            //    var products = await _productInfoRepository.GetWithIncludeAsync<ProductInfo>
            //        (
            //        selector: x => x,
            //        include: x => x.Include(s => s.MainProduct).Include(s => s.ProductColor)
            //        , where: x => x.ProductTitle.Contains(q) || x.Description.Contains(q) || x.Tags.Contains(q)
            //        );

            //    ////List<Contents> list = new List<Contents>();
            //    ////list.AddRange(_db.Contents_tb
            //    ////    .Where(x => x.ContentTitle.Contains(query) || x.ContentText.Contains(query) || x.ContentSummary.Contains(query)));
            //    ////list.AddRange(_db.ContentsTag_tb.Where(x => x.TagText.Contains(query)).Select(x => x.contents).ToList());

            //    //return list;

            //    var contents = _
            //    ViewBag.contents = contents;
            //    return View(products);
            //}
            return Redirect("/");
        }
        /// 
        /// //
        /// 
        /// <summary>
        /// /
        /// </summary>
        /// <returns></returns>
        public IActionResult About()
        {
            ViewBag.s = HttpContext.Session.GetString("Hi");
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<IActionResult> RegisterInNewsLetter(string email)
        {
            if (await _newsLetterRepository.Exists(s => s.Email == email))
            {
                return Json(new { res = false });

            }
            var newsLetter = new NewsLetter { Email = email, GroupId = 1 };
            await _newsLetterRepository.AddAsync(newsLetter);
            await _newsLetterRepository.SaveChangesAsync();
            return Json(new { res = true });
        }

        [Route("get-captcha-image")]
        public IActionResult GetCaptchaImage()
        {
            int width = 100;
            int height = 36;
            var captchaCode = KashanchiCaptcha.GenerateCaptchaCode();
            var result = KashanchiCaptcha.GenerateCaptchaImage(width, height, captchaCode);
            HttpContext.Session.SetString("CaptchaCode", result.CaptchaCode);
            Stream s = new MemoryStream(result.CaptchaByteData);
            var mysession = HttpContext.Session.GetString("CaptchaCode");

            return new FileStreamResult(s, "image/png");
        }

    }
}
