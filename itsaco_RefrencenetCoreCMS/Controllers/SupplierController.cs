﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.BasicInformation;
using DomainClasses.Suppliers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Controllers
{
    public class SupplierController : Controller
    {
        public IGenericRepository<ActivityField> _activityFieldRepository;
        public IGenericRepository<Activity> _activityRepository;
        public IGenericRepository<SupplierShareholders> _supplierShareholdersRepository;
        public IGenericRepository<SuppliersActivityFields> _suppliersActivityFieldsRepository;
        public IGenericRepository<SuppliersResume> _suppliersResumeRepository;
        public IGenericRepository<SuppliersCertificates> _suppliersCertificatesRepository;
        public IGenericRepository<SuppliersFiles> _suppliersFilesRepository;
        public IGenericRepository<MainSuppliers> _mainSuppliersRepository;
        public SupplierController(IGenericRepository<SuppliersFiles> suppliersFilesRepository,IGenericRepository<SuppliersCertificates> suppliersCertificatesRepository,IGenericRepository<SuppliersResume> suppliersResumeRepository,IGenericRepository<SuppliersActivityFields> suppliersActivityFieldsRepository,IGenericRepository<MainSuppliers> mainSuppliersRepository,IGenericRepository<ActivityField> activityFieldRepository, IGenericRepository<Activity> activityRepository,  IGenericRepository<SupplierShareholders> supplierShareholdersRepository)
        {
            _activityFieldRepository = activityFieldRepository;
            _activityRepository = activityRepository;
            _mainSuppliersRepository = mainSuppliersRepository;
            _supplierShareholdersRepository = supplierShareholdersRepository;
            _suppliersActivityFieldsRepository = suppliersActivityFieldsRepository;
            _suppliersResumeRepository = suppliersResumeRepository;
            _suppliersFilesRepository = suppliersFilesRepository;
            _suppliersCertificatesRepository = suppliersCertificatesRepository;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult MainSupplier()
        {
            return PartialView();
        }
        public IActionResult ShareholderSupplier()
        {
            return PartialView();
        }
        public async Task<IActionResult> ActivitySupplier()
        {
            var af = await _activityFieldRepository.GetAllAsync();
            ViewBag.ActivityField =new SelectList(af, "ActivityFieldID", "ActivityFieldTitle");

            return PartialView();
        }
        public async Task<JsonResult> GetAllActivity(int id)
        {
            return Json(await _activityRepository.GetAllAsync(x => x.ActivityFieldID == id));
        }
        public IActionResult ResumeSupplier()
        {
            return PartialView();
        }
        public IActionResult CertificateSupplier()
        {
            return PartialView();
        }
        public async Task<IActionResult> DocumentsSupplier()
        {
            //ViewBag.DocumentType = new SelectList(await _suppliersFileTypeRepository.GetAllAsync(), "FileType_ID", "Title");

            return PartialView();
        }
        [HttpPost]
        public async Task<IActionResult> DocumentUpload(IFormFile docFile)
        {
            if (!Directory.Exists("wwwroot/Files/SupplierDocs/temp"))
            {
                Directory.CreateDirectory("wwwroot/Files/SupplierDocs/temp");
            }

            //await RemoveTempFiles();
            string fileName = "";
            string fn = "";
            Random rnd = new Random();

            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SupplierDocs/temp");

            fileName = rnd.Next(1, 99999).ToString() + docFile.FileName.Replace(" ", "_").Replace("(", "").Replace(")", "");
            using (var stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
            {
                docFile.CopyTo(stream);
            }
            return Json(new { status = "ok", fn = fileName });
        }
        public async Task<JsonResult> RemoveTempFiles()
        {
            string sourcePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SupplierDocs/temp");
            string[] files = System.IO.Directory.GetFiles(sourcePath);
            foreach (string file in files)
            {
                if (System.IO.File.Exists(System.IO.Path.Combine(sourcePath, file)))
                {
                    try
                    {
                        System.IO.File.Delete(file);
                    }
                    catch (System.IO.IOException e)
                    {
                        return Json("NOK");
                    }
                }
            }
            return Json(Ok());
        }
        public IActionResult SupplierRegisterPrint()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> SupplierRegister(MainSuppliers mainSuppliers,List<string> fullName,
            List<string> types, List<string> percents, List<string> activity, List<string> capacity,
            List<string> company, List<string> employer, List<string> periods, List<string> descrip, List<string> certificateDescrip, List<string> certiName, List<string> certiType,
             List<string> docTitle, List<string> docFile)

        {
            SupplierShareholders ssh = new SupplierShareholders();
            SuppliersActivityFields saf = new SuppliersActivityFields();
            SuppliersResume sr = new SuppliersResume();
            SuppliersCertificates sc = new SuppliersCertificates();
            SuppliersFiles sf = new SuppliersFiles();
            try
            {
                await _mainSuppliersRepository.AddAsync(mainSuppliers);
                await _mainSuppliersRepository.SaveChangesAsync();

                Int16 SupplierId = mainSuppliers.Suppliers_ID;
                ///ShareHolder
                for (int i = 0; i < fullName.Count; i++)
                {
                    ssh.FullName = fullName[i];
                    if (types[i].Trim() == "0")
                    {
                        ssh.Type = false;
                    }
                    else
                    {
                        ssh.Type = true;
                    }
                    var shareHolders = await _supplierShareholdersRepository.GetAllAsync();
                    Int16 ShareHolderId = shareHolders.LastOrDefault() != null ? shareHolders.LastOrDefault().ShareHolder_ID : (short)0;

                    ssh.ShareHolder_ID = (short)(ShareHolderId+1);
                    ssh.PercentageShare = Convert.ToByte(percents[i]);
                    ssh.Suppliers_ID = SupplierId;
                    await _supplierShareholdersRepository.AddAsync(ssh);
                    await _supplierShareholdersRepository.SaveChangesAsync();
                }

                ///ActivityField

                for (int i = 0; i < activity.Count; i++)
                {
                    var activityField = await _suppliersActivityFieldsRepository.GetAllAsync();
                    Int16 SupplierActivityId = activityField.LastOrDefault() != null ? activityField.LastOrDefault().S_ActivityFields_ID : (short)0;

                    saf.S_ActivityFields_ID =(short)(SupplierActivityId+1);
                    saf.ActivityId = Convert.ToInt16(activity[i]);
                    saf.SupplierId = SupplierId;
                    saf.SupplyCapacity =Convert.ToInt32(capacity[i]);

                    await _suppliersActivityFieldsRepository.AddAsync(saf);
                    await _suppliersActivityFieldsRepository.SaveChangesAsync();
                }

                ///Resume

                for (int i = 0; i < company.Count; i++)
                {
                    var resume = await _suppliersResumeRepository.GetAllAsync();
                    int ResumeId = resume.LastOrDefault() != null ? resume.LastOrDefault().Resume_ID :0;

                    sr.Resume_ID= ResumeId + 1;
                    sr.Title = company[i];
                    sr.Employer = employer[i];
                    sr.CollaborationPeriod = periods[i];
                    sr.ResumeDescription = descrip[i];
                    sr.SupplierId = SupplierId;

                    await _suppliersResumeRepository.AddAsync(sr);
                    await _suppliersResumeRepository.SaveChangesAsync();

                }

                ///Certificate
                ///
                for (int i = 0; i < certiName.Count; i++)
                {
                    var certificate = await _suppliersCertificatesRepository.GetAllAsync();
                    int CertificateId = certificate.LastOrDefault() != null ? certificate.LastOrDefault().Certificate_ID : 0;

                    sc.Certificate_ID = CertificateId + 1;

                    sc.Description = certificateDescrip[i];
                    sc.Title = certiName[i];
                    sc.Type = Convert.ToBoolean(certiType[i]);
                    sc.SupplierId = SupplierId;
                    await _suppliersCertificatesRepository.AddAsync(sc);
                    await _suppliersCertificatesRepository.SaveChangesAsync();
                }
                ///Document
                ///
                for (int i = 0; i < docTitle.Count; i++)
                {
                    var documents = await _suppliersFilesRepository.GetAllAsync();
                    int DocumentId = documents.LastOrDefault() != null ? documents.LastOrDefault().ID : 0;

                    sf.ID = DocumentId + 1;

                    sf.FileNames = docFile[i];
                   // sf.FileTypeId =Convert.ToByte(docTitle[i]);
                    sf.SupplierId = SupplierId;

                    await _suppliersFilesRepository.AddAsync(sf);
                    await _suppliersFilesRepository.SaveChangesAsync();
                }
                await SaveToMainFolder();
            }
            catch (Exception err)
            {

                throw;
            }
           
            return Json("ok");
        }


        public async Task<IActionResult> SaveToMainFolder()
        {
            string fileName = "";
            string destFile = "";
            string sourcePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SupplierDocs/temp");
            string targetPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SupplierDocs");
            if (System.IO.Directory.Exists(sourcePath))
            {
                string[] files = System.IO.Directory.GetFiles(sourcePath);

                // Copy the files. 

                foreach (string file in files)
                {
                    fileName = System.IO.Path.GetFileName(file);
                    destFile = System.IO.Path.Combine(targetPath, fileName);     //ایجاد مسیر و نام فایل برای ذخیره عکس اصلی
                    System.IO.File.Copy(file, destFile, true);                   //ذخیره بدون تغییر سایز عکس در مسیر اصلی

                    System.IO.File.Delete(file);
                }

                //await RemoveTempFiles();
            }
            return Ok();
        }

        public IActionResult DeleteDocumentUpload(string fn)
        {
            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SupplierDocs/temp");
            string myFile = Path.Combine(path, fn);
            if (System.IO.File.Exists(myFile))
            {
                System.IO.File.Delete(myFile);
            }
            return Json("ok");
        }
    }
}