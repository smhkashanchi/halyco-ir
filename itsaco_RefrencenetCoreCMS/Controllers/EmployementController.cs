﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Employement;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Controllers
{
    public class EmployementController : Controller
    {
        public static int EmployementID = 0;
        public IGenericRepository<Emp_BaseInformation> _baseInformationRepository;
        public IGenericRepository<Emp_ComputerSkillItems> _computerSkillItemsRepository;
        public IGenericRepository<Emp_ActivityItems> _activityItemsRepository;
        public IGenericRepository<Emp_EducationalRecords> _educationalRecordsRepository;
        public IGenericRepository<Emp_WorkExperience> _workExperienceRepository;
        public IGenericRepository<Emp_TraningCourse> _traningCourseRepository;
        public IGenericRepository<Emp_Guarantor> _guarantorRepository;
        public IGenericRepository<Emp_UnderSupervisor> _underSupervisorRepository;
        public IGenericRepository<Emp_ComputerSkills> _computerSkillsRepository;
        public IGenericRepository<Emp_Activity> _activityRepository;
        public IGenericRepository<Emp_Documents> _documentsRepository;
        public EmployementController(IGenericRepository<Emp_Documents> documentsRepository,IGenericRepository<Emp_Activity> activityRepository,IGenericRepository<Emp_ComputerSkills> computerSkillsRepository,IGenericRepository<Emp_UnderSupervisor> underSupervisorRepository,IGenericRepository<Emp_Guarantor> guarantorRepository,IGenericRepository<Emp_TraningCourse> traningCourseRepository,IGenericRepository<Emp_WorkExperience> workExperienceRepository,IGenericRepository<Emp_EducationalRecords> educationalRecordsRepository,IGenericRepository<Emp_BaseInformation> baseInformationRepository,IGenericRepository<Emp_ComputerSkillItems> computerSkillItemsRepository,IGenericRepository<Emp_ActivityItems> activityItemsRepository)
        {
            _baseInformationRepository = baseInformationRepository;
            _computerSkillItemsRepository = computerSkillItemsRepository;
            _activityItemsRepository = activityItemsRepository;
            _educationalRecordsRepository = educationalRecordsRepository;
            _workExperienceRepository = workExperienceRepository;
            _guarantorRepository = guarantorRepository;
            _traningCourseRepository = traningCourseRepository;
            _underSupervisorRepository = underSupervisorRepository;
            _computerSkillsRepository = computerSkillsRepository;
            _activityRepository = activityRepository;
            _documentsRepository = documentsRepository;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult PersonInformation()
        {
            return PartialView();
        }
        public IActionResult EducationalRecords()
        {
            return PartialView();
        }
        public IActionResult WorkExperience()
        {
            return PartialView();
        }
        public async Task<IActionResult> SkillComputer()
        {
            return PartialView(await _computerSkillItemsRepository.GetAllAsync());
        }
        public IActionResult TraningCourses()
        {
            return PartialView();
        }
        public async Task<IActionResult> ActivityField()
        {
            return PartialView(await _activityItemsRepository.GetAllAsync());
        }
        public IActionResult GarantyNames()
        {
            return PartialView();
        }
        public IActionResult Suppervisor()
        {
            return PartialView();
        }
        public IActionResult Documents()
        {
            return PartialView();
        }
        [HttpPost]
        public IActionResult DocumentUpload(IFormFile docFile)
        {
            if (!Directory.Exists("wwwroot/Files/Employement/temp"))
            {
                Directory.CreateDirectory("wwwroot/Files/Employement/temp");
            }

            //await RemoveTempFiles();
            string fileName = "";
            string fn = "";
            Random rnd = new Random();

            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Employement/temp");

            fileName = rnd.Next(1, 99999).ToString() + docFile.FileName.Replace(" ", "_").Replace("(", "").Replace(")", "");
            using (var stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
            {
                docFile.CopyTo(stream);
            }
            return Json(new { status = "ok", fn = fileName });
        }
        public async Task<JsonResult> RemoveTempFiles()
        {
            string sourcePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Employement/temp");
            string[] files = System.IO.Directory.GetFiles(sourcePath);
            foreach (string file in files)
            {
                if (System.IO.File.Exists(System.IO.Path.Combine(sourcePath, file)))
                {
                    try
                    {
                        System.IO.File.Delete(file);
                    }
                    catch (System.IO.IOException e)
                    {
                        return Json("NOK");
                    }
                }
            }
            return Json(Ok());
        }
        public IActionResult DeleteDocumentUpload(string fn)
        {
            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Employement/temp");
            string myFile = Path.Combine(path, fn);
            if (System.IO.File.Exists(myFile))
            {
                System.IO.File.Delete(myFile);
            }
            return Json("ok");
        }
        [HttpPost]
        public async Task<IActionResult> SaveEmployement(Emp_BaseInformation emp_BaseInformation,IFormFile employementImage)
        {
            try
            {
                if (employementImage != null)
                {
                    if (!Directory.Exists("wwwroot/Files/Employement/Image"))
                    {
                        Directory.CreateDirectory("wwwroot/Files/Employement/Image");
                    }

                    emp_BaseInformation.Image = employementImage.FileName.Substring(0, employementImage.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(employementImage.FileName);
                    string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Employement/Image", emp_BaseInformation.Image);

                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        employementImage.CopyTo(stream);
                    }
                }

                await _baseInformationRepository.AddAsync(emp_BaseInformation);
                await _baseInformationRepository.SaveChangesAsync();

                EmployementID = emp_BaseInformation.Employment_ID;

                return Json("ok");
            }
            catch (Exception err)
            {

                throw;
            }
           
        }
        [HttpPost]
        public async Task<IActionResult> SaveEducational(List<string> evidence, List<string> field, List<string> average, List<string> endDate, List<string> universityType, List<string> institutionName, List<string> city_Country)
        {
            try
            {
                Emp_EducationalRecords eer = new Emp_EducationalRecords();
                for (int i = 0; i < evidence.Count; i++)
                {
                    var educationals = await _educationalRecordsRepository.GetAllAsync();
                    int educationalId = educationals.LastOrDefault() != null ? educationals.LastOrDefault().Edu_ID : 0;

                    eer.Edu_ID = educationalId+1;
                    eer.Employment_Id = EmployementID;
                    eer.Average = average[i];
                    eer.City_Country = city_Country[i];
                    eer.EndDate = endDate[i];
                    eer.Evidence = evidence[i];
                    eer.InstitutionName = institutionName[i];
                    eer.Field = field[i];
                    eer.UniversityType = universityType[i];

                    await _educationalRecordsRepository.AddAsync(eer);
                    await _educationalRecordsRepository.SaveChangesAsync();
                }
                return Json("ok");
            }
            catch (Exception err)
            {

                throw;
            }
        }

        [HttpPost]
        public async Task<IActionResult> SaveWorkExperience(List<string> organizationName, List<string> post, List<string> assistPeriod, List<string> assistCutDate, List<string> tell, List<string> averageSalary, List<string> assistCutReason)
        {
            try
            {
                Emp_WorkExperience ew = new Emp_WorkExperience();
                for (int i = 0; i < organizationName.Count; i++)
                {
                    var workExperience = await _workExperienceRepository.GetAllAsync();
                    int workExperienceId = workExperience.LastOrDefault() != null ? workExperience.LastOrDefault().WorkExp_ID : 0;

                    ew.WorkExp_ID = workExperienceId+1;
                    ew.Employment_Id = EmployementID;
                    ew.AssistCutDate =assistCutDate.Count==organizationName.Count? assistCutDate[i] : null;
                    ew.AssistCutReason = assistCutReason.Count == organizationName.Count ? assistCutReason[i] : null;
                    ew.AssistPeriod = assistPeriod[i];
                    ew.AverageSalary = averageSalary.Count == organizationName.Count ? averageSalary[i] : null;
                    ew.OrganizationName = organizationName[i];
                    ew.Post = post[i];
                    ew.Tell = tell.Count == organizationName.Count ? tell[i] : null;

                    await _workExperienceRepository.AddAsync(ew);
                    await _workExperienceRepository.SaveChangesAsync();
                }
                return Json("ok");
            }
            catch (Exception err)
            {

                throw;
            }
        }

        [HttpPost]
        public async Task<IActionResult> SaveTraningCourses(List<string> organizationName, List<string> traningName, List<string> startDate, List<string> traningPeroid, List<string> isEvidence, List<string> description)
        {
            try
            {
                Emp_TraningCourse etc = new Emp_TraningCourse();
                for (int i = 0; i < organizationName.Count; i++)
                {
                    var traningCourse = await _traningCourseRepository.GetAllAsync();
                    int traningCourseId = traningCourse.LastOrDefault() != null ? traningCourse.LastOrDefault().TraningCourse_ID : 0;

                    etc.TraningCourse_ID = traningCourseId + 1;
                    etc.Employement_Id = EmployementID;
                    etc.Description = description.Count == organizationName.Count ? description[i] : null;
                    etc.IsEvidence =Convert.ToBoolean(isEvidence[i]);
                    etc.StartDate = startDate.Count == organizationName.Count ? startDate[i] : null;
                    etc.TraningName = traningName[i];
                    etc.OrganizationName = organizationName[i];
                    etc.TraningPeroid = traningPeroid.Count == organizationName.Count ? traningPeroid[i] : null;

                    await _traningCourseRepository.AddAsync(etc);
                    await _traningCourseRepository.SaveChangesAsync();
                }
                return Json("ok");
            }
            catch (Exception err)
            {

                throw;
            }
        }

        [HttpPost]
        public async Task<IActionResult> SaveGarantyName(List<string> fullName, List<string> relation, List<string> job, List<string> mobile, List<string> workPlace)
        {
            try
            {
                Emp_Guarantor eg = new Emp_Guarantor();
                for (int i = 0; i < fullName.Count; i++)
                {
                    var gurantor = await _guarantorRepository.GetAllAsync();
                    int gurantorId = gurantor.LastOrDefault() != null ? gurantor.LastOrDefault().Guarantor_ID : 0;

                    eg.Guarantor_ID = gurantorId + 1;
                    eg.Employement_Id = EmployementID;
                    eg.FullName =fullName[i];
                    eg.Job = job.Count == fullName.Count ? job[i] : null;
                    eg.Mobile = mobile.Count == fullName.Count ? mobile[i] : null;
                    eg.Relation = relation.Count == fullName.Count ? relation[i] : null;
                    eg.WorkPlace = workPlace.Count == fullName.Count ? workPlace[i] : null;

                    await _guarantorRepository.AddAsync(eg);
                    await _guarantorRepository.SaveChangesAsync();
                }
                return Json("ok");
            }
            catch (Exception err)
            {

                throw;
            }
        }

        [HttpPost]
        public async Task<IActionResult> SaveSuppervisors(List<string> fullName, List<string> relation, List<string> job, List<string> jender, List<string> birthDate, List<string> levelEducation, List<string> skillComputers, List<string> activities, List<string> docTitle, List<string> docFile)
        {
            try
            {
                Emp_UnderSupervisor eus = new Emp_UnderSupervisor();
                Emp_ComputerSkills ecs = new Emp_ComputerSkills();
                Emp_Activity ea = new Emp_Activity();
                Emp_Documents ed = new Emp_Documents();

                for (int i = 0; i < fullName.Count; i++)
                {
                    var suppervisors = await _underSupervisorRepository.GetAllAsync();
                    int suppervisorId = suppervisors.LastOrDefault() != null ? suppervisors.LastOrDefault().ID : 0;

                    eus.ID = suppervisorId + 1;
                    eus.Employement_Id = EmployementID;
                    eus.FullName = fullName[i];
                    eus.Job = job.Count == fullName.Count ? job[i] : null;
                    eus.LevelEducation = levelEducation.Count == fullName.Count ? levelEducation[i] : null;
                    eus.Relation = relation.Count == fullName.Count ? relation[i] : null;
                    eus.Jender = jender.Count == fullName.Count ? jender[i] : null;
                    eus.BirthDate = birthDate.Count == fullName.Count ? birthDate[i] : null;

                    await _underSupervisorRepository.AddAsync(eus);
                    await _underSupervisorRepository.SaveChangesAsync();
                }

                for (int i = 0; i < skillComputers.Count; i++)
                {
                    ecs.Employement_Id = EmployementID;
                    ecs.ItemId = Convert.ToInt16(skillComputers[i]);

                    await _computerSkillsRepository.AddAsync(ecs);
                     await _computerSkillsRepository.SaveChangesAsync();
                }
                for (int i = 0; i < activities.Count; i++)
                {
                    ea.Employement_Id = EmployementID;
                    ea.ItemId = Convert.ToInt16(activities[i]);

                    await _activityRepository.AddAsync(ea);
                    await _activityRepository.SaveChangesAsync();
                }


                for (int i = 0; i < docFile.Count; i++)
                {
                    var documents = await _documentsRepository.GetAllAsync();
                    
                    int DocumentId = documents.LastOrDefault() != null ? documents.LastOrDefault().Doc_ID : 0;

                    ed.Doc_ID = DocumentId + 1;

                    ed.FileName = docFile[i];
                    if (docTitle[i] != null)
                    {
                        ed.Title = docTitle[i];
                    }
                    ed.Employement_Id = EmployementID;

                    await _documentsRepository.AddAsync(ed);
                    await _documentsRepository.SaveChangesAsync();
                }
                await SaveToMainFolder();

                return Json("ok");
            }
            catch (Exception err)
            {

                throw;
            }
        }

        public async Task<IActionResult> SaveToMainFolder()
        {
            string fileName = "";
            string destFile = "";
            string sourcePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Employement/temp");
            string targetPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/Employement");
            if (System.IO.Directory.Exists(sourcePath))
            {
                string[] files = System.IO.Directory.GetFiles(sourcePath);

                // Copy the files. 

                foreach (string file in files)
                {
                    fileName = System.IO.Path.GetFileName(file);
                    destFile = System.IO.Path.Combine(targetPath, fileName);     //ایجاد مسیر و نام فایل برای ذخیره عکس اصلی
                    System.IO.File.Copy(file, destFile, true);                   //ذخیره بدون تغییر سایز عکس در مسیر اصلی
                    if (System.IO.File.Exists(file))
                    {
                        System.IO.File.Delete(file);
                    }
                }
            }
            return Ok();
        }
    }
}