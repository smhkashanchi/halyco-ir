﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Cart;
using DomainClasses.Product;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;

namespace ModernProject.Controllers
{
    public class FactorDetailsController : Controller
    {
        private readonly IGenericRepository<FactorDetails> _factorDetailsRepository;
        private readonly IGenericRepository<Factor> _factorRepository;
        private readonly IGenericRepository<Garanty> _garantyRepository;
        private readonly IGenericRepository<ProductInfo> _productInfoRepository;

        public FactorDetailsController(IGenericRepository<FactorDetails> factorDetailsRepository,
            IGenericRepository<Factor> _factorRepository,
            IGenericRepository<Garanty> _garantyRepository,
            IGenericRepository<ProductInfo> _productInfoRepository)
        {
            _factorDetailsRepository = factorDetailsRepository;
        }

        // GET: FactorDetails
        public async Task<IActionResult> Index()
        {
            var factors = await _factorDetailsRepository.GetWithIncludeAsync<FactorDetails>(selector: x => x,
                include: x => x.Include(s => s.Factor).Include(s => s.Garanty).Include(s => s.ProductInfo));
            return View(factors);
        }

        // GET: FactorDetails/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var factorDetails = await _factorDetailsRepository.GetWithIncludeAsync<FactorDetails>(
                selector: x => x,
                include: x => x.Include(s => s.Factor).Include(s => s.Garanty).Include(s => s.ProductInfo),
                where: x => x.FactorDetails_Id == id);
            if (factorDetails == null)
            {
                return NotFound();
            }

            return View(factorDetails.FirstOrDefault());
        }

        // GET: FactorDetails/Create
        public async Task<IActionResult> Create()
        {
            ViewData["Factor_Id"] = new SelectList(await _factorRepository.GetAllAsync(), "Factor_Id", "Factor_Id");
            ViewData["Garanty_Id"] = new SelectList(await _garantyRepository.GetAllAsync(), "Garanty_ID", "GarantyName");
            ViewData["ProductInfo_Id"] = new SelectList(await _productInfoRepository.GetAllAsync(), "ProductInfo_ID", "Description");
            return View();
        }

        // POST: FactorDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FactorDetails_Id,Count,FinalPrice,Garanty_Id,ProductInfo_Id,Factor_Id")] FactorDetails factorDetails)
        {
            if (ModelState.IsValid)
            {
                await _factorDetailsRepository.AddAsync(factorDetails);
                await _factorDetailsRepository.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Factor_Id"] = new SelectList(await _factorRepository.GetAllAsync(), "Factor_Id", "Factor_Id");
            ViewData["Garanty_Id"] = new SelectList(await _garantyRepository.GetAllAsync(), "Garanty_ID", "GarantyName");
            ViewData["ProductInfo_Id"] = new SelectList(await _productInfoRepository.GetAllAsync(), "ProductInfo_ID", "Description");
            return View(factorDetails);
        }

        // GET: FactorDetails/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var factorDetails = await _factorDetailsRepository.GetByIdAsync(id);
            if (factorDetails == null)
            {
                return NotFound();
            }
            ViewData["Factor_Id"] = new SelectList(await _factorRepository.GetAllAsync(), "Factor_Id", "Factor_Id");
            ViewData["Garanty_Id"] = new SelectList(await _garantyRepository.GetAllAsync(), "Garanty_ID", "GarantyName");
            ViewData["ProductInfo_Id"] = new SelectList(await _productInfoRepository.GetAllAsync(), "ProductInfo_ID", "Description");
            return View(factorDetails);
        }

        // POST: FactorDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("FactorDetails_Id,Count,FinalPrice,Garanty_Id,ProductInfo_Id,Factor_Id")] FactorDetails factorDetails)
        {
            if (id != factorDetails.FactorDetails_Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _factorDetailsRepository.Update(factorDetails);
                    await _factorDetailsRepository.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await FactorDetailsExists(factorDetails.FactorDetails_Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Factor_Id"] = new SelectList(await _factorRepository.GetAllAsync(), "Factor_Id", "Factor_Id");
            ViewData["Garanty_Id"] = new SelectList(await _garantyRepository.GetAllAsync(), "Garanty_ID", "GarantyName");
            ViewData["ProductInfo_Id"] = new SelectList(await _productInfoRepository.GetAllAsync(), "ProductInfo_ID", "Description");
            return View(factorDetails);
        }

        // GET: FactorDetails/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var factorDetails = await _factorDetailsRepository.GetWithIncludeAsync<FactorDetails>(
                  selector: x => x,
                  include: x => x.Include(s => s.Factor).Include(s => s.Garanty).Include(s => s.ProductInfo),
                  where: x => x.FactorDetails_Id == id);
            if (factorDetails == null)
            {
                return NotFound();
            }

            return View(factorDetails);
        }

        // POST: FactorDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            //var factorDetails = await _context.FactorDetails.FindAsync(id);
            //_context.FactorDetails.Remove(factorDetails);
            //await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private async Task<bool> FactorDetailsExists(int id)
        {
            return await _factorDetailsRepository.Exists(x => x.FactorDetails_Id == id);
        }
    }
}
