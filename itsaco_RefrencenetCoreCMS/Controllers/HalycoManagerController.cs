﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Manager;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Controllers
{
    public class HalycoManagerController : Controller
    {
        public IGenericRepository<HalycoManager> _HalycoManagerRepository;
        public HalycoManagerController(IGenericRepository<HalycoManager> HalycoManagerRepository)
        {
            _HalycoManagerRepository = HalycoManagerRepository;
        }
        public IActionResult Index()
        {

            return View();
        }

        public async Task<IActionResult> Manager()
        {
            var managerList =await _HalycoManagerRepository.GetAllAsync(x => x.IsActive == true && x.ManagerType == true);
            return View(managerList);
        }


        public async Task<IActionResult> ExecutiveManager()
        {
            var managerList =await _HalycoManagerRepository.GetAllAsync(x => x.IsActive == true && x.ManagerType == false);
            return View(managerList);
        }
    }
}