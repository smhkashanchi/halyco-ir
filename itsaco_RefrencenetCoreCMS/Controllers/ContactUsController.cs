﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DomainClasses.Connection;
using DomainClasses.ContactUsMessage;
using GoogleRecaptchaInCore.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Services.Repositories;
using Utilities;
using Utilities.DatePersion;

namespace ModernProject.Controllers
{
    public class ContactUsController : Controller
    {
        public IGenericRepository<Connection> _connectionRepository;
        public IGenericRepository<ContactusMessageGroup> _contactusMsgGroupRepository;
        public IGenericRepository<ContactusMessage> _contactusMsgRepository;
        public IGenericRepository<Map> _mapRepository;
        public ContactUsController(IGenericRepository<Connection> connectionRepository,
            IGenericRepository<ContactusMessageGroup> contactusMsgGroupRepository,
            IGenericRepository<ContactusMessage> contactusMsgRepository, IGenericRepository<Map> mapRepository)
        {
            _connectionRepository = connectionRepository;
            _contactusMsgGroupRepository = contactusMsgGroupRepository;
            _contactusMsgRepository = contactusMsgRepository;
            _mapRepository = mapRepository;
        }
        public async Task<IActionResult> Index()
        {
            ViewBag.allCMG = new SelectList(await _contactusMsgGroupRepository.GetAllAsync(x => x.Lang == CultureInfo.CurrentCulture.Name, orderBy: x => x.OrderByDescending(s => s.CMG_ID)), "CMG_ID", "CMGName");
            var map = await _mapRepository.GetAsync(x=>x.Type==1);
            ViewBag.Lat = map.Latitude;
            ViewBag.Long = map.longitude;

            if (CultureInfo.CurrentCulture.Name == "fa-IR")
            {
                var ConnectionList = await _connectionRepository.GetAllAsync(x => x.ConnectionGroupId == 1);
                return View(ConnectionList);
            }
            else
            {
                var ConnectionList = await _connectionRepository.GetAllAsync(x => x.ConnectionGroupId == 4);
                return View(ConnectionList);
            }
        }

        public async Task<IActionResult> contactSaleUnit()
        {
            var map = await _mapRepository.GetAsync(x => x.Type == 2);
            ViewBag.Lat = map.Latitude;
            ViewBag.Long = map.longitude;
            if (CultureInfo.CurrentCulture.Name == "fa-IR")
            {
                var ConnectionList = await _connectionRepository.GetAllAsync(x => x.ConnectionGroupId == 3);
                return View(ConnectionList);
            }
            else
            {
                var ConnectionList = await _connectionRepository.GetAllAsync(x => x.ConnectionGroupId == 6);
                return View(ConnectionList);
            }
        }

        public async Task<IActionResult> contactBuyUnit()
        {
            var map = await _mapRepository.GetAsync(x => x.Type == 3);
            ViewBag.Lat = map.Latitude;
            ViewBag.Long = map.longitude;
            if (CultureInfo.CurrentCulture.Name == "fa-IR")
            {
                var ConnectionList = await _connectionRepository.GetAllAsync(x => x.ConnectionGroupId == 2);
                return View(ConnectionList);
            }
            else
            {
                var ConnectionList = await _connectionRepository.GetAllAsync(x => x.ConnectionGroupId == 5);
                return View(ConnectionList);
            }
        }



        [HttpPost]
        public async Task<IActionResult> InsertContactMessage(ContactusMessage contactusMessage)
        {
            string urlToPost = "https://www.google.com/recaptcha/api/siteverify";

            //Secret key خود را در این قسمت وارد کنید

            string secretKey = "6LcOxn0UAAAAAN_ARgUq_UzkJtlFv-CxjuDN-cPi"; // change this
            string gRecaptchaResponse = contactusMessage.Captcha;//form["g-recaptcha-response"]; //

            var postData = "secret=" + secretKey + "&response=" + gRecaptchaResponse;

            // send post data
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlToPost);
            request.Method = "POST";
            request.ContentLength = postData.Length;
            request.ContentType = "application/x-www-form-urlencoded";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(postData);
            }

            // receive the response now
            string result = string.Empty;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    result = reader.ReadToEnd();
                }
            }

            // validate the response from Google reCaptcha
            var captChaesponse = JsonConvert.DeserializeObject<reCaptchaResponse>(result);
            if (!captChaesponse.Success)
            {
                ViewBag.Message = "لطفا تصویر امنیتی را وارد کنید";
                return Json("incorrect");
            }



            if (contactusMessage != null)
            {
                contactusMessage.SendDate = System.DateTime.Now.Date;
                await _contactusMsgRepository.AddAsync(contactusMessage);
                await _contactusMsgRepository.SaveChangesAsync();
                return Json("ok");
            }
            return Json("nall");
        }

        public IActionResult PortalLink()
        {
            return View();
        }
        public IActionResult PortalLink2()
        {
            return View();
        }
    }
}