﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Services.Repositories;
using DomainClasses.Gallery;
using Microsoft.EntityFrameworkCore;
using System.Globalization;

namespace itsaco_RefrencenetCoreCMS.Controllers
{
    public class GalleryController : Controller
    {
        public IGenericRepository<Gallery> _GalleryRepository;
        public IGenericRepository<GalleryPicture> _GalleryPictureRepository;
        public GalleryController(IGenericRepository<Gallery> GalleryRepository, IGenericRepository<GalleryPicture> GalleryPictureRepository)
        {
            _GalleryPictureRepository = GalleryPictureRepository;
            _GalleryRepository = GalleryRepository;
        }
        public async Task<IActionResult> Index()
        {
            var GalleryList = await _GalleryRepository.GetWithIncludeAsync(
                selector:x=>x,
                include:x=>x.Include(s=>s.GalleryGroup),
                where:x=>x.GalleryGroup.Lang==CultureInfo.CurrentCulture.Name
                );
                //.GetAllAsync(x => x.GalleryGroupId == 1 && x.IsActive == true, orderBy: x => x.OrderByDescending(s => s.Gallery_ID));
            return View(GalleryList.OrderByDescending(x=>x.Gallery_ID));
        }

        public async Task<IActionResult> GalleryDetail(int id)
        {
            var galleryPicturesList = await _GalleryPictureRepository.GetWithIncludeAsync(
                selector: x => x,
                include:s=>s.Include(x=>x.Gallery),
                where:x=>x.GalleryId==id
                 );
            return View(galleryPicturesList);
        }
    }
}