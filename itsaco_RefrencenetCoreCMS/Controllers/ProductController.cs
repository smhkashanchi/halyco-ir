﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Gallery;
using DomainClasses.Product;
using DomainClasses.Seo;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Services.Repositories;
using Utilities;
using ViewModels;

namespace ModernProject.Controllers
{
    public class ProductController : Controller
    {
        public IGenericRepository<ProductInfo> _mainProductsRepository;
        public IGenericRepository<ProductsGroup> _productsGroupRepository;
        public IGenericRepository<GalleryPicture> _galleryPicturesRepository;
        public IGenericRepository<ProductColor> _productColorReporsitory;
        public IGenericRepository<ProductComment> _productCommentRepository;
        public IGenericRepository<Off> _offRepository;
        public IGenericRepository<MainPageSeo> _seoPageProductRepository;
        public ProductController(IGenericRepository<ProductInfo> mainProductsRepository,
            IGenericRepository<ProductsGroup> productsGroupRepository,
            IGenericRepository<GalleryPicture> galleryPicturesRepository,
            IGenericRepository<ProductColor> productColorReporsitory
          , IGenericRepository<ProductComment> productCommentRepository,
           IGenericRepository<Off> offRepository, IGenericRepository<MainPageSeo> seoPageProductRepository)
        {
            _mainProductsRepository = mainProductsRepository;
            _productsGroupRepository = productsGroupRepository;
            _galleryPicturesRepository = galleryPicturesRepository;
            _productColorReporsitory = productColorReporsitory;
            _productCommentRepository = productCommentRepository;
            _offRepository = offRepository;
            _seoPageProductRepository = seoPageProductRepository;
        }
        public async Task<IActionResult> Index()
        {

           // ViewBag.GroupId = id.ToTitle();
            List<ShowProductViewModel> list = new List<ShowProductViewModel>();

            var main =await _mainProductsRepository.GetWithIncludeAsync<ProductInfo>(
               selector:x=>x,
                 include: x => x.Include(s => s.MainProduct).Include(s=>s.ProductsGroup),
               where:x=>x.MainProduct.IsActive && x.ProductsGroup.Lang==CultureInfo.CurrentCulture.Name
             
                );
            //main = main.Take(10);
            foreach (var item in main)
            {
                var pi = new ShowProductViewModel();
                pi.Product_Id = item.ProductId;
                pi.Product_Title = item.ProductTitle;
                //pi.Product_Price = (int)item.ProductColor.Min(x => x.ProductPrice);
                pi.Product_Image = item.MainProduct.ProductImage;
                list.Add(pi);

            }

            //Seo ...
            var seoPage = await _seoPageProductRepository.GetAsync(x => x.PageId == main.FirstOrDefault().ProductGroupId && x.PageType == 2);
            if (seoPage != null)
            {
                ViewBag.SeoPage = seoPage;
            }
          
            // ViewBag.groupId = id;
            return View(list);
        }
        public async Task<IActionResult> Archive(string id)
        {
            var products = await _mainProductsRepository.GetWithIncludeAsync<ProductInfo>(
               selector: x => x,
               where: x => x.ProductsGroup.ProductGroupTitle == id.ToTitle() && x.MainProduct.IsActive,
               include: x => x.Include(s => s.ProductColor)
                );
            return View(products.Take(10));
        }
        [Route("[action]/{Id}")]
        public async Task<IActionResult> ProductDetails(string id)
        {

            var products = await _mainProductsRepository.GetWithIncludeAsync<ProductInfo>
                (
                selector:x=>x,
                where:x=>x.ProductTitle==id.ToTitle(),
                include:x=>x.Include(s=>s.MainProduct)
                );
            var product = products.FirstOrDefault();

            //Seo ...
            int productId = product.ProductInfo_ID;
            var seoPage = await _seoPageProductRepository.GetAsync(x => x.PageId ==productId  && x.PageType == 1);
            if (seoPage != null)
            {
                ViewBag.SeoPage = seoPage;
            }


            return View(product);
         
        }
        [HttpPost]
        public async Task<IActionResult> InsertComment(ProductComment contentsComments)
        {
            if (contentsComments != null)
            {
                contentsComments.CommentDate = DateTime.Now;
               await _productCommentRepository.AddAsync(contentsComments);
              await  _productCommentRepository.SaveChangesAsync();
                return Json("ok");
            }
            return Json("nall");
        }
        [HttpGet]
        public async Task<IActionResult> IndexByGroup(int id)
        {
            List<ShowProductViewModel> list = new List<ShowProductViewModel>();


            var main = await _mainProductsRepository.GetWithIncludeAsync<ProductInfo>(
                selector:x=>x,
                where: c => c.ProductGroupId == id && c.MainProduct.IsActive ,
                include:x=>x.Include(s=>s.MainProduct)
                );
            foreach (var item in main)
            {
                var pi = new ShowProductViewModel();
                pi.Product_Title = item.ProductTitle;
            //    pi.Product_Price = (int)item.ProductColor.Min(x => x.ProductPrice);
                pi.Product_Id = item.ProductId;
                pi.Product_Image = pi.Product_Image;
                list.Add(pi);
            }


            return View(list);
        }
        public IActionResult ShowProduct(ShowProductViewModel[] list)
        {
            return PartialView("_showProduct", list.ToList());
        }
        public async Task<IActionResult> FilterByCode(List<int> feature_list, List<int> group_list, List<int> color_list, string groupId, string code)
        {
            IEnumerable<ProductInfo> list_group = new List<ProductInfo>();
            IEnumerable<ProductInfo> list_color = new List<ProductInfo>();
            IEnumerable<ProductInfo>  list_feature = new List<ProductInfo>();
            if (group_list.Count == 0)
            {
                list_group = await _mainProductsRepository.GetWithIncludeAsync<ProductInfo>(
               selector: x => x,
               where: x => x.ProductsGroup.ProductGroupTitle == groupId.ToTitle() && x.MainProduct.IsActive,
               include: x => x.Include(s => s.ProductColor)
                ) ; 
            }
            else
            {
              
                foreach (var item in group_list)
                {

                    IEnumerable<string> a = new List<string>();
                    var gruops = await _mainProductsRepository.GetWithIncludeAsync<ProductInfo>(selector: x => x,
                        where: x => x.MainProduct.IsActive,
                        include: x => x.Include(s => s.ProductColor).Include(s => s.ProductsGroup).Include(s => s.ProductFeatures).Include(s => s.Producer));
                    list_group = list_group.Concat(gruops);

                }
            }
            foreach (var item in color_list)
            {
                var c = list_group.Where(x => x.ProductColor.Any(s => s.ColorId == item)).ToList();
                list_color = list_color.Concat(c).ToList();


            }
            foreach (var item in feature_list)
            {
                if (list_color.Count() != 0)
                {
                    var f = list_color.Where(x => x.ProductFeatures.Any(s => s.FeatureReplyId == item)).ToList();
                    list_feature = list_feature.Concat(f).ToList();
                }
                else
                {
                    var f = list_group.Where(x => x.ProductFeatures.Any(s => s.FeatureReplyId == item)).ToList();
                    list_feature = list_feature.Concat(f).ToList();
                }
            }
            var list_main = new List<ProductInfo>();

            if (feature_list.Count != 0)
            {
                list_main = list_feature.Distinct().ToList();

            }
            else if (color_list.Count != 0)
                list_main = list_color.Distinct().ToList();



            else
                list_main = list_group.Distinct().ToList();

            list_main = list_main.Where(x => x.MainProduct.ProductCode == code).ToList();
            var listP = new List<ShowProductViewModel>();
            foreach (var item in list_main)
            {
                ShowProductViewModel vm = new ShowProductViewModel();
                vm.Product_Id = item.ProductId;
                vm.Product_Image = item.MainProduct.ProductImage;
             //   vm.Product_Price = (int)item.ProductColor.Min(x => x.ProductPrice);
                vm.Product_Title = item.ProductTitle;
                listP.Add(vm);
            }

            return Json(new { res = true, list = listP });
        }

        public async Task<IActionResult> Filter(List<int> feature_list, List<int> group_list, List<int> color_list, string groupId)
        {
            IEnumerable<ProductInfo> list_group = new List<ProductInfo>();
            IEnumerable<ProductInfo> list_color = new List<ProductInfo>();
            IEnumerable<ProductInfo> list_feature = new List<ProductInfo>();
            if (group_list.Count == 0)
            {
                list_group = await _mainProductsRepository.GetWithIncludeAsync<ProductInfo>(
               selector: x => x,
               where: x => x.ProductsGroup.ProductGroupTitle == groupId.ToTitle() && x.MainProduct.IsActive,
               include: x => x.Include(s => s.ProductColor)
                );
                list_group = list_group.Take(10);
            }
            else
            {
                foreach (var item in group_list)
                {
                    IEnumerable<string> a = new List<string>();
                    var gruops = await _mainProductsRepository.GetWithIncludeAsync<ProductInfo>(selector: x => x,
                        where: x => x.ProductsGroup.ParentId==item,
                        include: x => x.Include(s => s.ProductColor).Include(s => s.ProductsGroup).Include(s => s.ProductFeatures).Include(s => s.Producer));


                    if (gruops.Count() == 0)
                    {
                     
                         gruops = await _mainProductsRepository.GetWithIncludeAsync<ProductInfo>(selector: x => x,
                       where: x => x.ProductsGroup.ProductGroup_ID == item && x.ProductsGroup.IsParent ,
                       include: x => x.Include(s => s.ProductColor).Include(s => s.ProductsGroup).Include(s => s.ProductFeatures).Include(s => s.Producer));

                    }
                    list_group = list_group.Concat(gruops.ToList()).ToList();

                }
            }
            foreach (var item in color_list)
            {
                var c = list_group.Where(x => x.ProductColor.Any(s => s.ColorId == item)).ToList();
                list_color = list_color.Concat(c).ToList();


            }
            foreach (var item in feature_list)
            {
                if (list_color.Count() != 0)
                {
                    var f = list_color.Where(x => x.ProductFeatures.Any(s => s.FeatureReplyId == item)).ToList();
                    list_feature = list_feature.Concat(f).ToList();
                }
                else
                {
                    var f = list_group.Where(x => x.ProductFeatures.Any(s => s.FeatureReplyId == item)).ToList();
                    list_feature = list_feature.Concat(f).ToList();
                }
            }
            var list_main = new List<ProductInfo>();

            if (feature_list.Count != 0)
            {
                list_main = list_feature.Distinct().ToList();

            }
            else if (color_list.Count != 0)
                list_main = list_color.Distinct().ToList();



            else
                list_main = list_group.Distinct().ToList();

            var listP = new List<ShowProductViewModel>();
            foreach (var item in list_main)
            {
                ShowProductViewModel vm = new ShowProductViewModel();
                vm.Product_Id = item.ProductId;
                vm.Product_Image = item.MainProduct.ProductImage;
               // vm.Product_Price = (int)item.ProductColor.Min(x => x.ProductPrice);
                vm.Product_Title = item.ProductTitle;
                listP.Add(vm);
            }

            return Json(new { res = true, list = listP });
        }
        public async Task<IActionResult> Off(string text)
        {
            var str = HttpContext.Session.GetString("ShopCart");
            var obj = JsonConvert.DeserializeObject<List<ShoppingCartItems>>(str);

            var Alloff = await _offRepository.GetAllAsync(x=>x.Lang== "fa" , x=>x.OrderByDescending(s=>s.Off_ID));
            var off = Alloff.Where(x => x.OffTitle == text).FirstOrDefault();
            if (off == null)
            {
                return Json(new { res = false });
            }
            var AllPrice = 0;
            foreach (var item in obj)
            {
                var products =await _mainProductsRepository.GetWithIncludeAsync<ProductInfo>(
                    selector:x=>x,
                    where:x=>x.ProductInfo_ID==item.ProductID,
                    include:x=>x.Include(s=>s.MainProduct)
                    );
                var product = products.FirstOrDefault();

                if (product.Off != null)
                {
                    if (product.Off.Off_ID == off.Off_ID)
                    {
                        var productColor = product.ProductColor.Where(x => x.ProductInfo.ProductId == product.ProductId);
                        var sum = item.Count * productColor.Min(x => x.ProductPrice);
                        float f = ((float)off.Price / 100);
                        var endOff = (sum - ((f) * sum));
                        item.Price = (int)endOff;
                    }
                }
                AllPrice += item.Price;

            }
            var listStr2 = JsonConvert.SerializeObject(obj);
            HttpContext.Session.SetString("ShopCart", listStr2);

            return Json(new { res = true, price = AllPrice });
        }
    }
}