﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Product;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Services.Repositories;
using ViewModels;

namespace ModernProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShopController : ControllerBase
    {
        public IGenericRepository<ProductInfo> _mainProductsRepository;
        public ShopController(IGenericRepository<ProductInfo> mainProductsRepository)
        {
            _mainProductsRepository = mainProductsRepository;
        }
        // GET: api/Shop
        [HttpGet]
        public int Get()
        {
            List<ShoppingCartItems> list = new List<ShoppingCartItems>();

            var listStr = JsonConvert.SerializeObject(list);
            //HttpContext.Session.SetString("ShopCart", listStr);
            if (HttpContext.Session.GetString("ShopCart") != null)
            {
                var str = HttpContext.Session.GetString("ShopCart");
                var obj = JsonConvert.DeserializeObject<List<ShoppingCartItems>>(str);
                list = obj;
            }
            return list.Sum(x=>x.Count);
        }

        // GET: api/Shop/5
        [HttpGet("{id}", Name = "Get")]
        public async Task <int> Get(int id,int colorId)
        {
            List<ShoppingCartItems> list = new List<ShoppingCartItems>();

            var listStr = JsonConvert.SerializeObject(list);
            //HttpContext.Session.SetString("ShopCart", listStr);
            if (HttpContext.Session.GetString("ShopCart") != null)
            {
                var str = HttpContext.Session.GetString("ShopCart");
                var obj = JsonConvert.DeserializeObject<List<ShoppingCartItems>>(str);
                list = obj;
            }
            if (list.Any(x => x.ProductID == id))
            {
                var product = await _mainProductsRepository.GetWithIncludeAsync<ProductInfo>(selector:x=>x,
                    where:x=>x.ProductInfo_ID==id,
                    include:x=>x.Include(s=>s.MainProduct));
                var color = product.FirstOrDefault().ProductColor.Where(x => x.ProductId == id).FirstOrDefault();

                int index = list.FindIndex(x => x.ProductID == id);
                list[index].Count += 1;
                list[index].Price +=  (int)color.ProductPrice;
            }
            else
            {
                var product =await _mainProductsRepository.GetWithIncludeAsync<ProductInfo>(selector: x => x,
                    where: x => x.ProductInfo_ID == id,
                    include: x => x.Include(s => s.MainProduct));
                var color = product.FirstOrDefault().ProductColor.Where(x => x.ProductId == id);
                list.Add(new ShoppingCartItems()
                {
                    ProductID = id,
                    Count = 1,
                    Price = (int)color.Min(x => x.ProductPrice)
                });
            }
            var listStr2 = JsonConvert.SerializeObject(list);
            HttpContext.Session.SetString("ShopCart", listStr2);

            return Get();
        }


    }
}
