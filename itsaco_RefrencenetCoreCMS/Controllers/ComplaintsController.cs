﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.CustomerComplaint;
using DomainClasses.Product;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;

namespace itsaco_RefrencenetCoreCMS.Areas.Admin.Controllers
{
    public class ComplaintsController : Controller
    {
        public IGenericRepository<ProductInfo> _productInfoRepository;
        public IGenericRepository<CustomerComplaintFiles> _customerComplaintFilesRepository;
        public IGenericRepository<CustomerComplaintMaster> _customerComplaintMasterRepository;
        public ComplaintsController(IGenericRepository<CustomerComplaintMaster> customerComplaintMasterRepository,IGenericRepository<ProductInfo> productInfoRepository, IGenericRepository<CustomerComplaintFiles> customerComplaintFilesRepository)
        {
            _productInfoRepository = productInfoRepository;
            _customerComplaintMasterRepository = customerComplaintMasterRepository;
            _customerComplaintFilesRepository = customerComplaintFilesRepository;
        }
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> Complaint()
        {
            ViewBag.Products = new SelectList(await _productInfoRepository.GetWithIncludeAsync
                (
                selector: x => x,
                include: x => x.Include(s => s.MainProduct).Include(s => s.ProductsGroup),
                where: x => x.MainProduct.IsActive && x.ProductsGroup.Lang == CultureInfo.CurrentCulture.Name
                ), "ProductInfo_ID", "ProductTitle");

            return PartialView();
        }
        public IActionResult ComplaintDocs()
        {
            return PartialView();
        }

        [HttpPost]
        //[RequestFormLimits(MultipartBodyLengthLimit = 209715200)]
        //[RequestSizeLimit(209715200)]
        public async Task<IActionResult> DocumentUpload(IFormFile docFile)
        {
            if (!Directory.Exists("wwwroot/Files/ComplaintsDocs/temp"))
            {
                Directory.CreateDirectory("wwwroot/Files/ComplaintsDocs/temp");
            }

            string fileName = "";
            Random rnd = new Random();

            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/ComplaintsDocs/temp");

            fileName = rnd.Next(1, 99999).ToString() + docFile.FileName.Replace(" ", "_").Replace("(", "").Replace(")", "");
            using (var stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
            {
               await docFile.CopyToAsync(stream);
            }
            return Json(new { status = "ok", fn = fileName });
        }
        public IActionResult DeleteDocumentUpload(string fn)
        {
            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/ComplaintsDocs/temp");
            string myFile = Path.Combine(path, fn);
            if (System.IO.File.Exists(myFile))
            {
                System.IO.File.Delete(myFile);
            }
            return Json("ok");
        }

        [HttpPost]
        public async Task<IActionResult> ComplaintsRegister(CustomerComplaintMaster customerComplaintMaster,List<string> docTitle, List<string> docFile)
        {
            CustomerComplaintFiles ccf = new CustomerComplaintFiles();
            try
            {
                await _customerComplaintMasterRepository.AddAsync(customerComplaintMaster);
                await _customerComplaintMasterRepository.SaveChangesAsync();

                Int16 ComplaintsId = customerComplaintMaster.ComplaintID;
               
                ///Document
                ///
                for (int i = 0; i < docFile.Count; i++)
                {
                    var documents = await _customerComplaintFilesRepository.GetAllAsync();
                    Int16 zero = 0;
                    Int16 DocumentId = documents.LastOrDefault() != null ? documents.LastOrDefault().CC_File_ID : zero;

                    ccf.CC_File_ID =(short)(DocumentId + 1);

                    ccf.FileNames = docFile[i];
                    if (docTitle[i] != "")
                    {
                        ccf.Title = docTitle[i];
                    }
                    ccf.ComplaintId = ComplaintsId;

                    await _customerComplaintFilesRepository.AddAsync(ccf);
                    await _customerComplaintFilesRepository.SaveChangesAsync();
                }
                await SaveToMainFolder();
            }
            catch (Exception err)
            {

                throw;
            }

            return Json("ok");
        }
        public async Task<JsonResult> RemoveTempFiles()
        {
            string sourcePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/ComplaintsDocs/temp");
            string[] files = System.IO.Directory.GetFiles(sourcePath);
            foreach (string file in files)
            {
                if (System.IO.File.Exists(System.IO.Path.Combine(sourcePath, file)))
                {
                    try
                    {
                        System.IO.File.Delete(file);
                    }
                    catch (System.IO.IOException e)
                    {
                        return Json("NOK");
                    }
                }
            }
            return Json(Ok());
        }
        public async Task<IActionResult> SaveToMainFolder()
        {
            string fileName = "";
            string destFile = "";
            string sourcePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/ComplaintsDocs/temp");
            string targetPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/ComplaintsDocs");
            if (System.IO.Directory.Exists(sourcePath))
            {
                string[] files = System.IO.Directory.GetFiles(sourcePath);

                // Copy the files. 

                foreach (string file in files)
                {
                    fileName = System.IO.Path.GetFileName(file);
                    destFile = System.IO.Path.Combine(targetPath, fileName);     //ایجاد مسیر و نام فایل برای ذخیره عکس اصلی
                    System.IO.File.Copy(file, destFile, true);                   //ذخیره بدون تغییر سایز عکس در مسیر اصلی

                    System.IO.File.Delete(file);
                }
            }
            return Ok();
        }

    }
}