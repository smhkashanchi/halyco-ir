﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Utilities;
using DomainClasses.Cart;
using ViewModels;
using Utilities.DatePersion;
using DataLayer;
using DomainClasses.User;
using Services.Repositories;
using Microsoft.AspNetCore.Authorization;

namespace ModernProject.Controllers
{
    [Authorize(Roles = "Customer")]
    public class FactorsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IGenericRepository<Factor> _factorRepository;
        private readonly IGenericRepository<FactorDetails> _factorDetailsRepository;
        public FactorsController(ApplicationDbContext context, UserManager<ApplicationUser> userManager,
            IGenericRepository<Factor> factorRepository
            , IGenericRepository<FactorDetails> factorDetailsRepository)
        {
            _factorRepository = factorRepository;
            _context = context;
            _userManager = userManager;
            _factorDetailsRepository = factorDetailsRepository;
        }

        // GET: Factors
        public async Task<IActionResult> Index()
        {


            var factors = await _factorRepository.GetWithIncludeAsync<Factor>(selector: x => x, include:
                x => x.Include(f => f.Customer).Include(f => f.CustomerAddress)
                .Include(f => f.Language).Include(f => f.SendType));
            return View(factors);

        }

        // GET: Factors/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            var factor = await _factorRepository.GetWithIncludeAsync<Factor>(selector: x => x,
              include: s => s.Include(x => x.FactorDetails).ThenInclude(x => x.ProductInfo).ThenInclude(x => x.ProductColor).Include(x => x.FactorDetails).ThenInclude(x => x.ProductInfo).ThenInclude(x => x.MainProduct)
                 .Include(x => x.CustomerAddress).ThenInclude(x => x.City).ThenInclude(x => x.State).Include(x => x.Customer).Include(x => x.SendType),
              where: x => x.Factor_Id == id);
            return PartialView(factor);
        }

        // GET: Factors/Create
        public IActionResult Create()
        {
            ViewData["Customer_Id"] = new SelectList(_context.Customer_tb, "Customer_ID", "Email");
            ViewData["CustomerAddress_Id"] = new SelectList(_context.CustomerAddress_tb, "CustomerAddress_Id", "Address");
            ViewData["Lang_ID"] = new SelectList(_context.Language_tb, "Lang_ID", "Lang_ID");
            ViewData["SendType_ID"] = new SelectList(_context.SendType_tb, "SendType_Id", "SendType_Id");
            return View();
        }

        // POST: Factors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<IActionResult> Create(int sendType, int address)
        {

            var str = HttpContext.Session.GetString("ShopCart");
            var obj = JsonConvert.DeserializeObject<List<ShoppingCartItems>>(str);

            var EndPrice = 0;
            foreach (var item in obj)
            {
                EndPrice += item.Count * item.Price;
            }

            var user = await _userManager.GetUserAsync(User);
            var customer = _context.Customer_tb.Where(x => x.ApplicationUser.Email == user.Email).Include(x => x.CustomerAddresses).FirstOrDefault();
            Random r = new Random();
            var factor = new Factor()
            {
                Factor_Date = ConvertDate.GetDatePersion(),
                Bank_Code = r.Next(1, 10000),
                Customer = customer,
                Customer_Id = customer.Customer_ID,
                CustomerAddress_Id = address,
                Factor_Lang = "fa",
                Order_Status = 1,
                PayMent_Status = 1,
                Payment_Type = 1,
                SendType_ID = Convert.ToByte(sendType),
                Lang_ID = "fa",
                TotalPrice = EndPrice,


            };
            await _factorRepository.AddAsync(factor);
            await _factorRepository.SaveChangesAsync();

            foreach (var item in obj)
            {
                var factorDetail = new FactorDetails()
                {
                    Name = "فاکتور",
                    Count = (short)item.Count,
                    Factor_Id = factor.Factor_Id,
                    FinalPrice = item.Count * item.Price,
                    ProductInfo_Id = item.ProductID
                };
                await _factorDetailsRepository.AddAsync(factorDetail);
                await _factorDetailsRepository.SaveChangesAsync();
            }
            List<ShoppingCartItems> list = new List<ShoppingCartItems>();
            var listStr2 = JsonConvert.SerializeObject(list);
            HttpContext.Session.SetString("ShopCart", listStr2);

            return Json(new { bankCode = factor.Bank_Code });
        }

        // GET: Factors/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var factor = await _factorRepository.GetByIdAsync(id);
            if (factor == null)
            {
                return NotFound();
            }
            ViewData["Customer_Id"] = new SelectList(_context.Customer_tb, "Customer_ID", "Email", factor.Customer_Id);
            ViewData["CustomerAddress_Id"] = new SelectList(_context.CustomerAddress_tb, "CustomerAddress_Id", "Address", factor.CustomerAddress_Id);
            ViewData["Lang_ID"] = new SelectList(_context.Language_tb, "Lang_ID", "Lang_ID", factor.Lang_ID);
            ViewData["SendType_ID"] = new SelectList(_context.SendType_tb, "SendType_Id", "SendType_Id", factor.SendType_ID);
            return View(factor);
        }

        // POST: Factors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Factor_Id,Factor_Date,TotalPrice,Order_Status,PayMent_Status,Factor_Lang,Bank_Code,Payment_Type,Send_Type,SendPrice,Customer_Id,CustomerAddress_Id,Lang_ID,SendType_ID")] Factor factor)
        {
            if (id != factor.Factor_Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(factor);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await FactorExists(factor.Factor_Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Customer_Id"] = new SelectList(_context.Customer_tb, "Customer_ID", "Email", factor.Customer_Id);
            ViewData["CustomerAddress_Id"] = new SelectList(_context.CustomerAddress_tb, "CustomerAddress_Id", "Address", factor.CustomerAddress_Id);
            ViewData["Lang_ID"] = new SelectList(_context.Language_tb, "Lang_ID", "Lang_ID", factor.Lang_ID);
            ViewData["SendType_ID"] = new SelectList(_context.SendType_tb, "SendType_Id", "SendType_Id", factor.SendType_ID);
            return View(factor);
        }

        // GET: Factors/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var factor = await _factorRepository.GetWithIncludeAsync<Factor>(selector: x => x,
                include: x => x.Include(s => s.Customer).Include(s => s.CustomerAddress).Include(s => s.Language).Include(s => s.SendType),
                where: x => x.Factor_Id == id);


            if (factor == null)
            {
                return NotFound();
            }

            return View(factor);
        }

        // POST: Factors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var factor = await _factorRepository.GetByIdAsync(id);
            _factorRepository.Delete(factor);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private async Task<bool> FactorExists(int id)
        {
            return await _factorRepository.Exists(x => x.Factor_Id == id);
        }
    }
}
