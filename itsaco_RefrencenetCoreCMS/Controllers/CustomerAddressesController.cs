﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Identity;
using DataLayer;
using DomainClasses.User;
using DomainClasses.Customer;
using ViewModels;

namespace ModernProject.Controllers
{
    public class CustomerAddressesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public CustomerAddressesController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: CustomerAddresses
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.CustomerAddress_tb.Include(c => c.Country).Include(x=>x.City).Include(x=>x.State).Include(c => c.Customer);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: CustomerAddresses/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var customerAddress = await _context.CustomerAddress_tb
                .Include(c => c.Country)
                .Include(c => c.Customer)
                .FirstOrDefaultAsync(m => m.CustomerAddress_Id == id);
            if (customerAddress == null)
            {
                return NotFound();
            }

            return View(customerAddress);
        }

        // GET: CustomerAddresses/Create
        public IActionResult Create()
        {
            ViewData["Country_Id"] = new SelectList(_context.Country_tb, "Country_ID", "CountryName");
            ViewData["Customer_Id"] = new SelectList(_context.Customer_tb, "Customer_ID", "Email");
            return View();
        }

        // POST: CustomerAddresses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<IActionResult> Create(CustomerAddress customerAddress)
        {
            if (ModelState.IsValid)
            {
                //Defualt
                customerAddress.Country_Id = 1;
                var user = await _userManager.GetUserAsync(User);
                var customer = _context.Customer_tb.Where(x => x.ApplicationUser.Email == user.Email).FirstOrDefault();
                customerAddress.Customer_Id = customer.Customer_ID;
                customerAddress.Customer = customer;

                _context.Add(customerAddress);
                await _context.SaveChangesAsync();
                return Json(new { res = true });
            }
            ViewData["Country_Id"] = new SelectList(_context.Country_tb, "Country_ID", "CountryName", customerAddress.Country_Id);
            ViewData["Customer_Id"] = new SelectList(_context.Customer_tb, "Customer_ID", "Email", customerAddress.Customer_Id);
            return Json(new { res=false});
        }

        // GET: CustomerAddresses/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var customerAddress = await _context.CustomerAddress_tb.FindAsync(id);
            if (customerAddress == null)
            {
                return NotFound();
            }
            ViewData["Country_Id"] = new SelectList(_context.Country_tb, "Country_ID", "CountryName", customerAddress.Country_Id);
            ViewData["Customer_Id"] = new SelectList(_context.Customer_tb, "Customer_ID", "Email", customerAddress.Customer_Id);
            return View(customerAddress);
        }

        // POST: CustomerAddresses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CustomerAddess_Id,Address,PostalCode,Phone,Customer_Id,Country_Id")] CustomerAddress customerAddress)
        {
            if (id != customerAddress.CustomerAddress_Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(customerAddress);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CustomerAddressExists(customerAddress.CustomerAddress_Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Country_Id"] = new SelectList(_context.Country_tb, "Country_ID", "CountryName", customerAddress.Country_Id);
            ViewData["Customer_Id"] = new SelectList(_context.Customer_tb, "Customer_ID", "Email", customerAddress.Customer_Id);
            return View(customerAddress);
        }

        // GET: CustomerAddresses/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var customerAddress = await _context.CustomerAddress_tb
                .Include(c => c.Country)
                .Include(c => c.Customer)
                .FirstOrDefaultAsync(m => m.CustomerAddress_Id == id);
            if (customerAddress == null)
            {
                return NotFound();
            }

            return View(customerAddress);
        }

        // POST: CustomerAddresses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var customerAddress = await _context.CustomerAddress_tb.FindAsync(id);
            _context.CustomerAddress_tb.Remove(customerAddress);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        public IActionResult ListCity(int stateid)
        {
            var list = _context.City_tb.Include(x => x.State).Where(x => x.State_ID == stateid).ToList();
            var cities = new List<CityViewModel>();
            foreach (var item in list)
            {
                var city = new CityViewModel()
                {
                    City_Id = item.City_ID,
                    City_Name=item.CityName
                };
                cities.Add(city);
            }
            if (list.Count > 0)
                return Json(new
                {
                    res = JsonConvert.SerializeObject(cities)
                });
            else
                return Json(new { res = false });

        }
        private bool CustomerAddressExists(int id)
        {
            return _context.CustomerAddress_tb.Any(e => e.CustomerAddress_Id == id);
        }
    }
}
