﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.AVF;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;

namespace ModernProject.Controllers
{
    public class VideoController : Controller
    {
        public IGenericRepository<AVF> _aVFRepository;
        public IGenericRepository<AVFGroup> _AVFGroupRepository;
        public VideoController(IGenericRepository<AVF> aVFRepository, IGenericRepository<AVFGroup> AVFGroupRepository)
        {
            _aVFRepository = aVFRepository;
            _AVFGroupRepository = AVFGroupRepository;
        }
        public async Task<IActionResult> Index()
        {
            if (CultureInfo.CurrentCulture.Name == "fa-IR")
            {
                // var videos = await _aVFRepository.GetAllAsync(x=>x.AVFGroupId== 1,x=>x.OrderByDescending(s=>s.AVF_ID));
                var videoGroupList = await _AVFGroupRepository.GetAllAsync(x => x.IsActive == true && x.ParentId == 20, orderBy: x => x.OrderByDescending(s => s.AVFGroup_ID));
                return View(videoGroupList);
            }
            else if(CultureInfo.CurrentCulture.Name == "en-US")
            {
                var videoGroupList = await _AVFGroupRepository.GetAllAsync(x => x.IsActive == true && x.ParentId == 24, orderBy: x => x.OrderByDescending(s => s.AVFGroup_ID));
                return View(videoGroupList);
            }
            return View();
        }
        public async Task<IActionResult> ShowVideos(int id)
        {
            var VideoList = await _aVFRepository.GetWithIncludeAsync(
                 selector: x => x,
                 include: s => s.Include(x => x.AVFGroup),
                 where: x => x.AVFGroupId == id && x.IsActive==true  ,
                 orderBy:x=>x.OrderByDescending(s=>s.AVF_ID)
                  );

            return View(VideoList);
        }
        public async Task<IActionResult> ShowVideoDefault()
        {
            var video =await _aVFRepository.GetWithIncludeAsync<AVF>(selector: x=>x,
               where: x=>x.AVFGroup.AVFGroupTitle == "ویدیو" && x.IsActive, 
               include:x=>x.Include(s=>s.AVFGroup));
            return Json(video.FirstOrDefault());
        }
    }
}