﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ViewModels;

namespace ModernProject.Controllers
{
    public class CartController : Controller
    {
        public IActionResult Index()
        {
            var IsEmpty = false;

            if (HttpContext.Session.GetString("ShopCart") == null)
            {
                IsEmpty = true;
            }
            else
            {
                var str = HttpContext.Session.GetString("ShopCart");
                var obj = JsonConvert.DeserializeObject<List<ShoppingCartItems>>(str);
                if (obj.Count == 0 || obj == null)
                    IsEmpty = true;
            }
            ViewBag.Cart_Empty = IsEmpty;
            return View();
        }
    }
}