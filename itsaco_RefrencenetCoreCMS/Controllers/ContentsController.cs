﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Contents;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Services.Repositories;
using Utilities;

namespace ModernProject.Controllers
{
    public class ContentsController : Controller
    {
        public IGenericRepository<ContentsGroup> _contentsGroupRepository;
        public IGenericRepository<Contents> _contentsRepository;
        public IGenericRepository<ContentsComments> _contentsCommentRepository;
        public IGenericRepository<ContentFiles> _contentFilesRepository;
        public ContentsController(IGenericRepository<ContentsGroup> contentsGroupRepository, IGenericRepository<Contents> contentsRepository,
             IGenericRepository<ContentsComments> contentsCommentRepository, IGenericRepository<ContentFiles> contentFilesRepository)
        {
            _contentsGroupRepository = contentsGroupRepository;
            _contentsRepository = contentsRepository;
            _contentsCommentRepository = contentsCommentRepository;
            _contentFilesRepository = contentFilesRepository;
        }
        public async Task<IActionResult> Index()
        {
            if (CultureInfo.CurrentCulture.Name == "fa-IR")
            {
                var ContentGroupList = await _contentsGroupRepository.GetAllAsync(x => x.IsActive == true && x.ParentID == 1);
                var ContentGroupSelect = new SelectList(ContentGroupList, "ContentGroup_ID", "ContentGroupName");
                ViewData["ContentGroupList"] = ContentGroupSelect;
            }
            else if (CultureInfo.CurrentCulture.Name == "en-US")
            {
                var ContentGroupList = await _contentsGroupRepository.GetAllAsync(x => x.IsActive == true && x.ParentID == 2);
                var ContentGroupSelect = new SelectList(ContentGroupList, "ContentGroup_ID", "ContentGroupName");
                ViewData["ContentGroupList"] = ContentGroupSelect;
            }


            await showArticles();
            return View();
        }

        public async Task<IActionResult> showArticles(int id = 0)
        {
            IEnumerable<Contents> articles = null;
            if (id == 0)
            {
                articles = await _contentsRepository.GetWithIncludeAsync(
                    selector: x => x,
                    include: x => x.Include(s => s.contentsGroup),
                    where: x => x.IsActive == true && x.ContentGroupId != -1 && x.ContentGroupId != -2 && x.contentsGroup.Lang == CultureInfo.CurrentCulture.Name
                    );
                //..GetAllAsync(x => x.IsActive == true && x.ContentGroupId!=-1, orderBy: x => x.OrderByDescending(s => s.Content_ID));
            }
            else
            {
                articles = await _contentsRepository.GetAllAsync(x => x.IsActive == true && x.ContentGroupId == id, orderBy: x => x.OrderByDescending(s => s.Content_ID));
            }
            return PartialView(articles);
        }

        public async Task<IActionResult> ShowAllContentsGroup() //tab
        {
            var groups = await _contentsGroupRepository.GetAllAsync(x => x.Lang == "fa" && x.ParentID == 1 && x.IsActive, s => s.OrderBy(x => x.ContentGroup_ID));
            return PartialView(groups);
        }
        public async Task<IActionResult> ShowAllContents(int id)
        {
            ViewBag.groups = await _contentsGroupRepository.GetAllAsync(x => x.Lang == "fa" && x.ParentID == 1 && x.IsActive, s => s.OrderBy(x => x.ContentGroup_ID));
            ViewBag.allContents = await _contentsRepository.GetWithIncludeAsync<Contents>(selector: x => x,
                where: x => x.contentsGroup.ParentID == id,
                include: s => s.Include(x => x.contentsGroup));
            //var contents = _contentsRepository.GetAllContentsByGroupIdIsActive(id);
            return PartialView();
        }
        public async Task<IActionResult> ShowAllContentsByGroup(int id)//نمایش کانتنت ها در دیو کانتنت تب ها
        {
            ViewBag.groupId = "ok";
            var contents = await _contentsRepository.GetWithIncludeAsync<Contents>(
                selector: x => x,
                where: x => x.ContentGroupId == id && x.IsActive && x.contentsGroup.IsActive,
                include: x => x.Include(s => s.contentsGroup));
            return PartialView(contents);
        }
        public async Task<IActionResult> ShowContentDetails(string id)
        {
            if (id != "")
            {
                var content = await _contentsRepository.GetWithIncludeAsync<Contents>(selector: x => x,
                    where: x => x.ContentTitle == id.ToTitle(),
                    include: x => x.Include(s => s.contentsGroup));

                var contentFiles = await _contentFilesRepository.GetWithIncludeAsync
                    (
                    selector: x => x,
                    include: x => x.Include(s => s.AVF),
                    where: x => x.ContentId == content.FirstOrDefault().Content_ID
                    );
                ViewBag.ContentFiles = contentFiles;

                return View(content.FirstOrDefault());
            }
            return NotFound();
        }
        public async Task<IActionResult> ShowComments(string id)
        {
            var content = await _contentsRepository.GetWithIncludeAsync<Contents>(selector: x => x,
                    where: x => x.ContentTitle == id.ToTitle(),
                    include: x => x.Include(s => s.contentsGroup));
            var comments = await _contentsCommentRepository.GetWithIncludeAsync<ContentsComments>(selector: x => x,
                where: x => x.ContentId == content.FirstOrDefault().Content_ID,
                include: x => x.Include(s => s.Contents));
            return PartialView(comments);
        }
        [HttpPost]
        public async Task<IActionResult> InsertComment(ContentsComments contentsComments)
        {
            if (contentsComments != null)
            {
                contentsComments.CommentDate = DateTime.Now;
                await _contentsCommentRepository.AddAsync(contentsComments);
                await _contentsCommentRepository.SaveChangesAsync();
                return Json("ok");
            }
            return Json("nall");
        }
    }
}