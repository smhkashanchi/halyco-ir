// ...........show and hide nav where window resized for profile......
function hideNav () {
    if ($(window).width() < 768) {
        $('#tab-container').attr("class","col-xs-12 collapse");
        $('#tab-container').attr("aria-expanded",false);
    }
}
$(window).resize(function() {
    if ($(window).width() < 768) {
        $('#tab-container').attr("class","collapse");
        $('#tab-container').attr("aria-expanded",false);
    }else {
        $('#tab-container').attr("class","collapse in");
        $('#tab-container').attr("aria-expanded",true);
    }
});
// ........for menu3 at cart......
var acc = document.getElementsByClassName("accordion");
var i;
for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        this.nextElementSibling.classList.toggle("show");

    }
}
// ..............tooltip................
$(function () {
    $('[data-toggle="tooltip"]').tooltip();
});
$(document).ready(function(e) {
    $(window).resize(function () {
        $(".navbar-collapse").css({ maxHeight: $(window).height() - $(".navbar-header").height() + "px" });
    });
    // ...........show and hide nav where window resized for profile......
    if ($(window).width() < 768) {
        $('#tab-container').attr("class","collapse");
        $('#tab-container').attr("aria-expanded",false);
    }else {
        $('#tab-container').attr("class","collapse in");
        $('#tab-container').attr("aria-expanded",true);
    }
    // ...........hide ENTRY.............
    $('.linkMembership').click(function() {
        $('#myModal').modal('hide');
    });
// ..........enseraf as kharid.......
    $('.btnclose').click(function() {
        $('.box-cart').collapse('hide');
    });
// ....................BOX CART..................
    $('.delete').click(function() {
        $('.btn-toggle').attr('data-toggle','collapse');
        if( confirm("آیا از حذف این محصول اطمینان دارید?")) {
            $(this).closest('tr').css('background', 'red').fadeOut('slow' , function(){
                $(this).remove();
            });
        }
        setTimeout(function(){ $('.btn-toggle').attr('data-toggle','dropdown'); },1000);
    });
    $('.box-cart li table input').click(function() {
        $('.btn-toggle').attr('data-toggle','collapse');
        setTimeout(function(){ $('.btn-toggle').attr('data-toggle','dropdown'); },500);
    });
//................box cart arabic..............
    $('.deleteArabic').click(function() {
        $('.btn-toggle').attr('data-toggle','collapse');
        if( confirm("هل أنت متأكد من أنك تريد حذف هذا المنتج؟")) {
            $(this).closest('tr').css('background', 'red').fadeOut('slow' , function(){
                $(this).remove();
            });
        }
        setTimeout(function(){ $('.btn-toggle').attr('data-toggle','dropdown'); },1000);
    });
//................box cart english..............
    $('.deleteEnglish').click(function() {
        $('.btn-toggle').attr('data-toggle','collapse');
        if( confirm("Are you sure you want to delete this product?")) {
            $(this).closest('tr').css('background', 'red').fadeOut('slow' , function(){
                $(this).remove();
            });
        }
        setTimeout(function(){ $('.btn-toggle').attr('data-toggle','dropdown'); },1000);
    });
// ............select color at detail-product..........
    $('.ProductColor .color , .bank .color , .collapseFrame .color').click(function(){
        $('.selectColor').removeClass('selectColor');
        $(this).addClass('selectColor').find(':radio').prop('checked',true);
    });
// ............select color at cart..........
    $('#menu4 .table').click(function(){
        $('.backColor').removeClass('backColor');
        $(this).addClass('backColor').find(':radio').prop('checked',true);
    });
    $('#menu5 .table').click(function(){
        $('.backColor1').removeClass('backColor1');
        $(this).addClass('backColor1').find(':radio').prop('checked',true);
    });
    /*......................ORDER LIST.......................*/
    $('.listMain').hide();
    $('#menu4 table:first-child a').click(function() {
        var hreflist    =   $(this).attr('href');
        $('.listMain:visible').hide();
        $(hreflist).fadeIn();
        var topElement  =    $(hreflist).offset().top;
        $('html , body').animate({scrollTop: topElement}, 3000 , 'easeOutBack');

        return false;
    });
    /*......................ADDRESS EDITE.......................*/
    $('.btn-edit').click(function(){
        $('span.edit').each(function(key){
            var name  = $(this).data('input');
            var value = $(this).text();
            $('#myModal3 input[name="'+name+'"], #myModal3 select[name="'+name+'"], #myModal3 textarea[name="'+name+'"]').val(value);
        });
    });
    $('.delete1').click(function(e) {
        e.preventDefault();
        if( confirm("آیا از حذف این آدرس اطمینان دارید?")) {
            $(this).parents('table').parents('table').css('background', 'red').fadeOut('slow' , function(){
                $(this).remove();
            });
        }
    });
    $('.deleteArabic1').click(function(e) {
        e.preventDefault();
        if( confirm("هل أنت متأكد من أنك تريد حذف هذا العنوان؟")) {
            $(this).parents('table').parents('table').css('background', 'red').fadeOut('slow' , function(){
                $(this).remove();
            });
        }
    });
    $('.deleteEnglish1').click(function(e) {
        e.preventDefault();
        if( confirm("Are you sure you want to delete this Address?")) {
            $(this).parents('table').parents('table').css('background', 'red').fadeOut('slow' , function(){
                $(this).remove();
            });
        }
    });
    /*.................move To top..............*/
    jQuery(window).scroll(function(){
        if (jQuery(this).scrollTop() > 350) {
            jQuery('#topcontrol').css({bottom:"20px"});
        } else {
            jQuery('#topcontrol').css({bottom:"-100px"});
        }
    });
    jQuery('#topcontrol').click(function(){
        jQuery('html, body').animate({scrollTop: '0px'}, 800);
        return false;
    });

    /*.............................*/
    $('.btnPoll').click(function() {
        $('.testBar .progress').slideToggle();
    });

    /*...............responsive header hexa..............*/
    $('.dokme-carpet').click(function () {
        $('.header-main-Hexagonal').animate({'right':'0px','display':'block'},500);
    });

    $('.header-main-Hexagonal .close').click(function () {
        $('.header-main-Hexagonal').animate({'right':'-420px','display':'none'},500);
    });

    /*............... product more Item ..............*/
    $('.up-down').click(function(event) {

        if ( $(this).prev().is(':visible') ) {
            $(this).text('نمایش بیشتر');
        }else {
            $(this).text('نمایش کمتر');
        }

    });

    $('.wab-up-down').click(function(event) {

        if ( $(this).prev().is(':visible') ) {
            $(this).text('عرض المزيد');
        }else {
            $(this).text('عرض أقل');
        }

    });

    $('.zen-up-down').click(function(event) {

        if ( $(this).prev().is(':visible') ) {
            $(this).text('View more');
        }else {
            $(this).text('View less');
        }

    });

    /*............... colaps for order carpet ..............*/
    $(document).bind('click', function(e) {
        var $clicked = $(e.target);
        // if (!$clicked.parents().parent().hasClass(".colaps"))
        $(".colaps").collapse('hide');
    });
    $('.collapseFrame').click(function(event) {
        event.stopPropagation();
    });

    /*............... zooming ..............*/
    // $("#zoom_03").elevateZoom({constrainType:"height", constrainSize:274, zoomType: "lens", containLensZoom: true, gallery:'gallery_01', cursor: 'pointer', galleryActiveClass: "active"});

//pass the images to Fancybox
//     $("#zoom_03").bind("click", function(e) {
//         var ez =   $('#zoom_03').data('elevateZoom');
//         $.fancybox(ez.getGalleryList());
//         return false;
//     });
});