﻿$(document).ready(function () {
    $("#language_ddl").val("fa");

});

$(document).ajaxStart(function () {
    $('#ajaxLoading').removeClass('hidden');
    console.log('start');
});

$(document).ajaxStop(function () {
    $('#ajaxLoading').addClass('hidden');
    console.log('stop');
});
//--------------------------------------------------- کد مربوط به فعال بودن یک منو و باز ماندن منوی حاوی زیرمنو --------------------------
$('ul.sidebar-menu li:first').addClass('active');

$('ul.sidebar-menu>li a').click(function () {

    $('ul.sidebar-menu>li').removeClass('active');
    $(this).parent('li').addClass('active');
    var oldLink = $(this).attr('href');
    $.cookie('zaroldLink', oldLink);
});
var lastLink = $.cookie('zaroldLink');
$('ul.sidebar-menu>li a[href="' + lastLink + '"]').click();


$('ul.sidebar-menu>li.treeview').click(function () {
    if ($(this).hasClass('active')) {
        $('ul.sidebar-menu>li.treeview').removeClass('menu-open');
        $('ul.sidebar-menu>li.treeview ul.treeview-menu').slideUp(500);
        $('.arrow', $(this)).removeClass("open");
    }
    else {
        $('ul.sidebar-menu>li.treeview').removeClass('menu-open').removeClass('active');
        $(this).addClass('menu-open').addClass('active');
        $('ul.sidebar-menu>li.treeview ul.treeview-menu').slideUp(500);
        $(this).children('ul.treeview-menu').slideDown(500);
        $('.arrow').removeClass("open");
        $('.arrow', $(this)).addClass("open");
        var lastSubmenuId = $(this).attr('id');
        $.cookie('lastSubmenuId', lastSubmenuId);
    }

});
//alert(lastLink);
$('a[href="' + lastLink + '"]').parent().parent().parent('li.treeview').click();//css({ 'display': 'block' });
//--------------------------------------------------- کد مربوط به فعال بودن یک منو و باز ماندن منوی حاوی زیرمنو --------------------------


//-----------------------------------------------------------------
function openModalForContactWithDeveloper() {
    $.get("/Admin/ContactDev/SendMessage", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ارسال پیام");
        $("#myModalBody").html(result);
    });

}
function ContactWithDeveloper() {
    var frm = $("#ContactWithDeveloper_frm").valid();
    if (frm === false) {
        return false;
    }

    $("#div-loading").removeClass('hidden');
    var person = new FormData();
    var files = $("#attachFile_fu").get(0).files;

    person.append("Name", $("#Name").val()),
        person.append("Mobile", $("#Mobile").val()),
        person.append("Email", $("#Email").val()),
        person.append("MessageText", $("#MessageText").val()),
        person.append("atFile", files[0])


    $.ajax({
        method: 'POST',
        url: '/Admin/ContactDev/SendMessage',
        data: person,
        contentType: false,
        processData: false,
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.statusCode === 200) {
                swal({
                    title: "ارتباط با توسعه دهنده",
                    text: "پیام شما به ایمیل برنامه نویس ارسال شد",
                    type: "success"
                    , confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                })
            }

        },
        error: function (result) {
            alert(result.error);
        }
    });
}
//عدم دسترسی
function ShowAccessDenied() {
    //window.location.href = "/Admin/Account/AccessDenied"; return false; 
    swal({

        type: 'warning',
        title: "مجوز دسترسی",
        text: "متاسفانه شما به این بخش دسترسی ندارید",
        showConfirmButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: "تائید",
    });
}
//Get lang
function GetAllLanguageForMasterPage() {

    $.ajax({
        method: 'POST',
        url: '/Admin/Language/GetAllLangForLayoutPage',
        datatype: "json",
        contentType: "application/json;charset=utf-8",
        data: '',
        success: function (data) {

            var options = "";
            //options += "<option value='0'>زبان ...</option>";
            data.forEach(function (obj) {
                options += "<option value=" + obj.lang_ID + ">" + obj.lang_Name + "</option>";
            });
            $("#language_ddl").html(options);
        },
        error: function (result) {
        }
    });
}

function SetTabsId(id) {
    $('#tab_color').attr('onclick', 'GetAllProductColorByProductId(' + id + ')');
    $('#tab_feature').attr('onclick', 'GetAllProductFeatureByProductId(' + id + ')');
    $('#tab_garanty').attr('onclick', 'GetAllProductGarantyByProductId(' + id + ')');
    $('#tab_advantage').attr('onclick', 'GetAllProductAdvantageByProductId(' + id + ')');
    $('#tab_comment').attr('onclick', 'GetAllCommentsByProductId(' + id + ')');
    $('#tab_similar').attr('onclick', 'GetAllSimilarProductsByProductId(' + id + ')');


}
//دسترسی ها //
function Checkbox() {
}

//
//قسمت نقش

//

function GetAllRoles() {

    $.ajax({
        method: 'POST',
        url: '/Admin/Roles/ViewAll',
        data: '',
        success: function (data) {
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}

function openModalForAddRole() {
    $.get("/Admin/Roles/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن نقش ");
        $("#myModalBody").html(result);
    });
}
function BeginAddRole() {

    $("#div-loading").removeClass('hidden');
}
function CreateRole_Success(data) {
    $("#div-loading").addClass('hidden');
    $("#viewAll").load("/Admin/Roles/ViewAll");
    $("#myModal").modal('hide');
    swal({
        title: "نقش",
        text: "نقش با موفقیت ثبت شد",
        type: "success"
        , confirmButtonColor: '#3085d6',
        confirmButtonText: "تائید"
    })
    GetAllRoles(data);
}
function CreateUserRole_Failure() {


    swal("خطا", "خطا در ارسال اطلاعات دوباره سعی کنید", "error");

}

function openModalForEditRole(id) {
    $.get("/Admin/Roles/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش نقش ");
        $("#myModalBody").html(result);
    });
}
function EditRole_Success(data) {
    if (!data.res) {
        swal({

            type: 'warning',
            title: " نقش",
            text: data.message,
            showConfirmButton: true,
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید",
        });
        return false;

    }
    $("#div-loading").addClass('hidden');
    $("#viewAll").load("/Admin/Roles/ViewAll");
    $("#myModal").modal('hide');

    swal({
        title: "نقش",
        text: "نقش با موفقیت ویرایش شد",
        type: "success",
        confirmButtonColor: '#3085d6',
        confirmButtonText: "تائید"
    });
    // GetAllRoles();
}
function EditRole_Failure() {


    swal("خطا", "خطا در ارسال اطلاعات دوباره سعی کنید", "error");

}

function DeleteRole(id) {
    swal({
        title: "هشدار !!!",
        text: "آیا برای حذف این نقش مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"

    })
        .then((willDelete) => {
            if (willDelete.value) {

                $.ajax({
                    url: "/Admin/Roles/Delete",
                    type: "Post",
                    data: { id: id }
                }).done(function (data) {
                    if (data.redirect) { ShowAccessDenied(); return false; }
                    if (!data.res) {
                        swal({

                            type: 'warning',
                            title: " نقش",
                            text: data.message,
                            showConfirmButton: true,
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید",
                        });
                        return false;
                    }
                    else {
                        swal({

                            type: 'success',
                            title: " نقش",
                            text: "نقش با موفیقیت حذف شد",
                            showConfirmButton: true,
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید",
                        });

                        $('#role_tb').find('tr[id=role_' + data.message + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#role_tb').DataTable();
                            table.row("#role_" + data.message).remove().draw(false);
                        });

                        GetAllRoles();

                    }
                });
            }
        });
}


//
//قسمت کاربران

//
function SetUserName() {

    $("#UserName").val($("#Email").val());
}

function GetAllUsers() {

    $.ajax({
        method: 'POST',
        url: '/Admin/Users/ViewAll',
        data: '',
        success: function (data) {
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}
function openModalForAddUser(id) {
    $.get("/Admin/Users/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن مدیر ");
        $("#myModalBody").html(result);
    });
}
function CreateUser_Success(data) {
    if (!data.res) {
        swal({

            type: 'warning',
            title: " کاربران",
            text: data.message,
            showConfirmButton: true,
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید",
        });
        return false;

    }
    $("#div-loading").addClass('hidden');
    $("#viewAll").load("/Admin/Users/ViewAll");
    $("#myModal").modal('hide');
    swal({
        title: "کاربران", text: "کاربر  با موفقیت ثبت شد", type: "success", showConfirmButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: "تائید",
    });
    GetAllUsers(data);
}
function CreateUser_Failure() {


    swal("خطا", "خطا در ارسال اطلاعات دوباره سعی کنید", "error");

}

function openModalForEditUser(id) {

    $.get("/Admin/Users/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش مدیر ");
        $("#myModalBody").html(result);
    });

}
function EditUser_Success(data) {

    if (!data.res) {
        swal({

            type: 'warning',
            title: " کاربران",
            text: data.message,
            showConfirmButton: true,
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید",
        })
        return false;
    }
    $("#div-loading").addClass('hidden');
    $("#viewAll").load("/Admin/Users/ViewAll");
    $("#myModal").modal('hide');
    swal({
        title: "کاربران", text: "اطلاعات کاربر با موفقیت ویرایش شد", type: "success", showConfirmButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: "تائید",
    });
    GetAllUsers();

}
function EditUser_Failure() {
    swal("خطا", "خطا در ارسال اطلاعات دوباره سعی کنید", "error");
}

function openModalForPermission(id) {
    $.get("/Admin/Roles/Permission/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("تعیین سطح دسترسی");
        $("#myModalBody").html(result);
    });
}
function openModalForAddRoleToUser(id) {
    $.get("/Admin/Roles/AddRole/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن نقش به کاربر");
        $("#myModalBody").html(result);
    });
}
function AddRoleToUser_Success(data) {

    $("#div-loading").addClass('hidden');
    //$("#viewAll").load("/Admin/Users/ViewAll");
    $("#myModal").modal('hide');
    swal({
        title: "کاربران", text: "نقش های کاربر با موفقیت ویرایش شد", type: "success", showConfirmButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: "تائید",
    });
    //GetAllUsers();

}
function AddRoleToUser_Failure() {
    swal("خطا", "خطا در ارسال اطلاعات دوباره سعی کنید", "error");
}

var EmployeeId = '';
function viewchangeUserPassword(id) {
    EmployeeId = id;
    console.log(id);
    $.get("/Admin/Users/changeEmployeePassword/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("تغییر رمز کاربر");
        $("#myModalBody").html(result);
    });

}

function changeUserPassword() {
    if ($('#newEmployeePassword').val() == $('#newEmployeePassword2').val()) {
        $.ajax({
            method: 'Post',
            url: '/Admin/Users/changeEmployeePassword',
            data: ({ id: EmployeeId, currentAdminPassword: $('#currentAdminPassword').val(), newEmployeePassword: $('#newEmployeePassword').val() }),
            success: function (data) {
                debugger;
                if (data == 'adminPasswordIncorrect') {
                    swal({
                        title: "کاربران",
                        text: "رمز عبور مدیر صحیح نمی باشد",
                        type: "warning",
                        showConfirmButton: true,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                } else {
                    swal({
                        title: "کاربران",
                        text: "رمز عبور کاربر  با نام " + data.fullName + " با موفقیت تغییر یافت",
                        type: "success",
                        showConfirmButton: true,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
            },
            error: function (result) {
            }
        });
    } else {
        swal({
            title: "کاربران",
            text: "رمز عبور و تکرار آن یکسان نمی باشد",
            type: "warning",
            showConfirmButton: true,
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید",
        });
    }
}
//

//
//قسمت زبان

//
function GetAllDataInDropDownList(lang, controller, action, selector) {
    $.ajax({
        method: "GET",
        url: "/Admin/" + controller + "/" + action + "",
        data: { id: lang },
        success: function (result) {
            var option = "";
            result.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $(selector).html(option);
            //
        },
        error: function (res) {

        }
    });
}
//
function GetAllLanguage() {

    $.ajax({
        method: 'POST',
        url: '/Admin/Language/ViewAll',
        data: '',
        success: function (data) {
            //$("#viewAll").load('@(Url.Action("ViewAll", "Language", null))');
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}
//
function openModalForAddLang() {
    $.get("/Admin/Language/Create/", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن زبان جدید");
        $("#myModalBody").html(result);
    });

}
//زمانی که شروع به پست کردن اطلاعات میکنیم
var onBegin = function () {

    $("#div-loading").removeClass('hidden');
};

//زمانی که اطلاعات به درستی ثبت شد این تابع فراخوانی می شود
var onSuccessLang = function (res) {
    $("#div-loading").addClass('hidden');

    if (res === "repeate") {
        //swal("هشدار", "شناسه وارد شده تکراری است", "warning");
        swal({

            type: 'warning',
            title: "افزودن زبان",
            text: "شناسه وارد شده قبلا ثبت شده",
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });

    }
    else {

        $("#viewAll").load("/Admin/Language/ViewAll");
        $("#myModal").modal('hide');
        swal({

            type: 'success',
            title: "افزودن زبان",
            text: "اطلاعات زبان با موفقیت ثبت شد",
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        $(".swal2-container").css('z-index:100000');

        //swal("تبریک", "زبان " + res.lang_Name + " با موفقیت ثبت شد", "success");
    }
};
//

function openModalForEditLang(id) {
    $.get("/Admin/Language/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش زبان ");
        $("#myModalBody").html(result);
    });

}

var onSuccessLangEdit = function (res) {
    $("#div-loading").addClass('hidden');


    $("#viewAll").load("/Admin/Language/ViewAll");
    $("#myModal").modal('hide');

    swal({

        type: 'success',
        title: "ویرایش زبان",
        text: "زبان " + res.lang_Name + " با موفقیت ویرایش شد",
        showConfirmButton: true,
        confirmButtonText: "تائید"
    });
};

//////////////////////////قسمت گروه اسلایدشو
//
function ViewAll(lang) {
    var controller = window.location.href;
    controller = controller.substring(controller.indexOf("/admin/"), controller.length);
    $.ajax({
        method: 'POST',
        url: controller + '/ViewAll',
        data: { id: lang },
        success: function (data) {

            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}

function changeLanguage(lang) {


    var controller = window.location.href;
    controller = controller.toLowerCase();
    controller = controller.substring(controller.indexOf("/admin/"), controller.length);
    if (controller.includes("contents")) { //اگر در صفحه مطالب باشیم
        getAllContentsGroupForTreeView();
        GetAllGalleryGroupByLangForContents(lang);
    }
    else if (controller.includes("gallery")) {
        ViewAll(lang);
        GetAllGalleryGroup(lang);
    }
    else if (controller.includes("contactusmessage")) {
        ViewAll(lang);
        GetAllContactusMessageGroupByLang(lang); ///تابعی برای نمایش گروه ها در دراپ داون
    }
    else if (controller.includes("slideshow")) {
        ViewAll(lang);

        GetAllSlideShowGroupByLang(lang); ///تابعی برای نمایش گروه ها در دراپ داون

        GetAllSlideShowByGroupId($('#slideShowGroup_ddl').val());
    }
    else if (controller.includes("products")) {
        getAllProductsGroupForTreeView();
    }
    else if (controller.includes("officepost")) {
        getAllOfficePostForTreeView();
    }
    else if (controller.includes("features")) {
        getAllProductsGroupForFeatureTreeView();
    }
    else if (controller.includes("connection")) {
        ViewAll(lang);
        GetAllConnectionGroups(lang, "#connectionsGroup_ddl");
    }
    else if (controller.includes("location")) {
        ViewAll(lang);
        GetAllDataInDropDownList(lang, "Location", "GetAllCountry", "#country_ddl");
        //GetAllDataInDropDownList($("#state_ddl").val(), "Location", "GetAllState", "#state_ddl");
    }
    else if (controller.includes("filesmanage")) {
        //ViewAll(lang);
        //GetAllDownloadGroupByLang(lang); ///تابعی برای نمایش گروه ها دانلود در دراپ داون
        getAllAVFGroupForTreeView();

    }
    else if (controller.includes("projects")) {
        getAllProjectsGroupForTreeView();
    }
    else {
        ViewAll(lang);
    }
}
//
function GetAllSlideShowGroup() {
    $("#div-loading").removeClass('hidden');
    $.ajax({
        method: 'POST',
        url: '/Admin/SlideShow/ViewAll',
        data: '',
        success: function (data) {
            $("#div-loading").addClass('hidden');
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}
//
function openModalForAddSlideShowGroup() {
    $.get("/Admin/SlideShow/Create/", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گروه جدید");
        $("#myModalBody").html(result);
    });

}
//
function InsertSlideShowGroup() {
    //var frm = $("#sgFrm").valid();
    //if (frm === true) {

    $("#div-loading").removeClass('hidden');

    var isactive = $('#IsActiveSsg:checked').length;
    var status = "";
    if (isactive === 1) {
        status = "true";
    }
    else {
        status = "false";
    }

    var slideShowGroup = {
        SSGName: $("#SSGName").val(),
        IsActive: status,
        Lnag: $("#language_ddl").val()
    }



    //    formData.append("IsActive", "true")
    //}
    //else {
    //    formData.append("IsActive", "false")
    //}
    // formData.append("SSGName", $("#SSGName").val()),
    //    formData.append("Language", $("#language_ddl").val())


    $.ajax({
        method: 'Get',
        url: '/Admin/SlideShow/Create',
        data: slideShowGroup,
        success: function (data) {
            $("#div-loading").addClass('hidden');
            $("#viewAll").load("/Admin/SlideShow/ViewAll/" + data + "");
            $("#myModal").modal('hide');
            swal({
                title: "اسلایدشو",
                text: "گروه اسلایدشو با موفقیت ثبت شد",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            })
            GetAllSlideShowGroupByLang(data);
        },
        error: function (result) {
        }
    });
    //}
    //else {
    //    return false;
    //}
}
//
function openModalForEditSlideShowGroup(id) {
    $.get("/Admin/SlideShow/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گروه ");
        $("#myModalBody").html(result);
    });

}
//
function UpdateSlideShowGroup() {
    //var frm = $("#sgFrmEdit").valid();
    //if (frm === true) {

    $("#div-loading").removeClass('hidden');

    var isactive = $('#IsActiveSsgEdit:checked').length;
    var status = "";
    if (isactive === 1) {
        status = "true";
    }
    else {
        status = "false";
    }

    var slideShowGroup = {
        SlideShowGroupID: $("#SlideShowGroupID").val(),
        SSGName: $("#SSGName").val(),
        IsActive: status,
        Lnag: $("#language_ddl").val()
    }
    $.ajax({
        method: 'POST',
        url: '/Admin/SlideShow/Edit',
        data: slideShowGroup,
        success: function (data) {
            $("#div-loading").addClass('hidden');
            $("#viewAll").load("/Admin/SlideShow/ViewAll/" + data + "");
            $("#myModal").modal('hide');
            swal({
                title: "اسلایدشو",
                text: "گروه اسلایدشو با موفقیت ویرایش شد",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            })
            GetAllSlideShowGroupByLang(data);
        },
        error: function (result) {
        }
    });
    //}
    //else {
    //    return false;
    //}
}
//
function DeleteSlideShowGroup(id) {
    swal({
        title: "هشدار !!!",
        text: "آیا برای حذف این گروه مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"

    })
        .then((willDelete) => {
            if (willDelete.value) {

                $.ajax({
                    url: "/Admin/SlideShow/Delete",
                    type: "Get",
                    data: { id: id }
                }).done(function (result) {
                    if (result.redirect) { ShowAccessDenied(); return false; }

                    if (result === "NOK") {
                        swal({
                            title: "اسلایدشو",
                            text: "برای این گروه اسلایدشو تعریف شده , لطفا ابتدا آنها را حذف کنید تا قادر به حذف گروه باشید",
                            type: "warning",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        })
                    }
                    else {
                        $('#slideShow_tb').find('tr[id=ssg_' + id + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#slideShow_tb').DataTable();
                            table.row("#ssg_" + id).remove().draw(false);
                        });
                        GetAllSlideShowGroupByLang(result.lnag);
                        swal({ title: "اسلایدشو", text: "گروه " + result.ssgName + " با موفقیت حذف شد", icon: "success", buttons: [false, "تایید"], });
                    }
                });
            }
        });
}

//////////////////////////قسمت اسلایدشو
//
function GetAllSlideShowGroupByLang(lang) {
    $.ajax({ //گرفتن اطلاعات جیسون از سمت سرور و دی سریالایز کردن آن
        method: "GET",
        async: false,
        url: "/Admin/SlideShow/ViewAllSSG",
        data: { id: lang },
        success: function (result) {
            var option = "";
            result.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $("#slideShowGroup_ddl").html(option);
            $("#SSGId").html(option);
            //
        },
        error: function (res) {

        }
    });
}
//

function GetAllSlideShowByGroupId(id) {
    console.log(id);
    $.ajax({
        method: 'POST',
        async: false,
        url: '/Admin/SlideShow/SlideShowViewAll',
        data: { id: id },
        success: function (data) {

            $("#viewAllSlideShow").html(data);
        },
        error: function (result) {
        }
    });
}


//
function openModalForAddSlideShow() {
    var groupId = $("#slideShowGroup_ddl").val();
    if (groupId == 0) {
        swal({
            title: "اسلایدشو",
            text: "لطفا گروهی برای اسلایدشو انتخاب نمایید",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید"
        })
    }
    else {
        $.get("/Admin/SlideShow/CreateSlideShow/", function (result) {
            $("#myModal").modal();
            $("#myModalLabel").html("افزودن اسلایدشو جدید");
            $("#myModalBody").html(result);
        });
    }
}
//

function AddSlideShow() {
    var frm = $("#sliderFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');

        var slideShow = new FormData();
        var files = $("#imgSlideShowUploader").get(0).files;
        var isactive = $('#isActiveSlideShow:checked').length;
        var groupId = $("#slideShowGroup_ddl").val();

        if (files.length > 0) {


            if (isactive === 1) {
                slideShow.append("IsActive", "true")
            }
            else {
                slideShow.append("IsActive", "false")
            }
            slideShow.append("SSGId", groupId),
                slideShow.append("SSTitle", $("#SSTitle").val()),
                slideShow.append("SSText", $("#SSText").val()),
                slideShow.append("Link", $("#Link").val()),
                slideShow.append("imgSlideShow", files[0])
        }


        $.ajax({
            method: 'POST',
            url: '/Admin/SlideShow/CreateSlideShow',
            data: slideShow,
            contentType: false,
            processData: false,
            success: function (data) {
                $("#div-loading").addClass('hidden');

                if (data === "NoImage") {
                    swal({
                        title: "اسلایدشو",
                        text: "لطفا عکسی را برای اسلایدشو انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    })
                }
                else {
                    $("#viewAllSlideShow").load("/Admin/SlideShow/SlideShowViewAll/" + data);
                    $("#myModal").modal("hide");
                    swal({
                        title: "اسلایدشو",
                        text: "اسلایدشو شما با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            },
            error: function (result) {
                alert(result.error);
            }
        });
    }
    else {
        return false;
    }
}
//
function openModalForEditSlideShow(id) {
    $.get("/Admin/SlideShow/EditSlideShow/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش اسلایدشو");
        $("#myModalBody").html(result);
    });

}
//
function EditSlideShow() {
    $("#div-loading").removeClass('hidden');

    var slideShow = new FormData();
    var files = $("#imgSlideShowUploaderEdit").get(0).files;
    var isactive = $('#isActiveSlideShowEdit:checked').length;



    if (isactive === 1) {
        slideShow.append("IsActive", "true")
    }
    else {
        slideShow.append("IsActive", "false")
    }
    slideShow.append("SlideShowID", $("#SlideShowID").val()),
        slideShow.append("SSGId", $("#SSGId").val()),
        slideShow.append("SSTitle", $("#SSTitle").val()),
        slideShow.append("SSText", $("#SSText").val()),
        slideShow.append("Link", $("#Link").val()),
        slideShow.append("SSPic", $("#SSPic").val())


    if (files.length > 0) {
        slideShow.append("imgSliderUpload", files[0])
    }


    $.ajax({
        method: 'POST',
        url: '/Admin/SlideShow/EditSlideShow',
        data: slideShow,
        contentType: false,
        processData: false,
        success: function (data) {
            $("#div-loading").addClass('hidden');

            $("#viewAllSlideShow").load("/Admin/SlideShow/SlideShowViewAll/" + data);
            $("#myModal").modal("hide");

            swal({
                title: "اسلایدشو",
                text: "اسلایدشو با موفقیت ویرایش شد",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
        },
        error: function (result) {
            alert(result.error);
        }
    });
}

//
function checkboxSelectedForSlider() {
    swal({
        type: "warning",
        title: "هشدار",
        text: "آیا برای حذف اسلایدشوهای انتخاب شده مطمئن هستید؟",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {
                var checkboxValues = "";
                $("div.checked").children('.forRemove').each(function () { // گرفتن ولیو های چکس باکس هایی که تیک خورده باشند
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");
                if (checkboxValues != "") {

                    $.ajax({
                        url: "/Admin/SlideShow/checkboxSelected",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        for (var i = 0; i < cv.length - 1; i++) {

                            $('#slideShow_tb').find('tr[id=ss_' + cv[i] + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#slideShow_tb').DataTable();
                                table.row("#ss_" + cv[i]).remove().draw(false);
                            });
                        }
                        swal({
                            title: "اسلایدشو",
                            text: "اسلایدشو با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                    });
                }
                else {
                    swal({
                        title: "اسلایدشو",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            }
        });

}
//////////////////////////قسمت سوالات متداول

//
//function changeLanguage(lang) {
//    $.ajax({
//        method: 'POST',
//        url: '/Admin/FAQ/ViewAll',
//        data: { lang: lang },
//        success: function (data) {
//            $("#viewAll").html(data);
//        },
//        error: function (result) {
//        }
//    });
//}

//
function GetAllFAQ() {

    $.ajax({
        method: 'POST',
        url: '/Admin/FAQ/ViewAll',
        data: '',
        success: function (data) {
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}
//
//function GetAllFAQByLang(id) {

//    $.ajax({
//        method: 'POST',
//        url: '/Admin/FAQ/ViewAll',
//        data: '',
//        success: function (data) {
//            $("#viewAll").html(data);
//        },
//        error: function (result) {
//        }
//    });
//}
//
//

function openModalForAddFAQ() {
    $.get("/Admin/FAQ/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن سوال جدید");
        $("#myModalBody").html(result);
    });

}
//

function InsertFAQ() {
    var frm = $("#faqFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActiveFaq:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var fAQ = {
            FAQuestion: $("#FAQuestion").val(),
            FAQAnswer: $("#FAQAnswer").val(),
            IsActive: status,
            Lnag: $("#language_ddl").val()
        }



        $.ajax({
            method: 'POST',
            url: '/Admin/FAQ/Create',
            data: fAQ,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                swal({
                    title: "ثبت سوال",
                    text: "سوال شما با موفقیت ثبت شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });

                $("#viewAll").load("/Admin/FAQ/ViewAll/" + data);
                $("#myModal").modal('hide');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//

//
function openModalForEditFAQ(id) {
    $.get("/Admin/FAQ/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش جدید");
        $("#myModalBody").html(result);
    });

}
//

function EditFAQ() {
    var frm = $("#faqFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActiveFaqEdit:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var fAQ = {
            FAQ_ID: $("#FAQ_ID").val(),
            FAQuestion: $("#FAQuestion").val(),
            FAQAnswer: $("#FAQAnswer").val(),
            IsActive: status,
            Lnag: $("#language_ddl").val()
        }



        $.ajax({
            method: 'POST',
            url: '/Admin/FAQ/Edit',
            data: fAQ,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                swal({
                    title: "ویرایش سوال",
                    text: "سوال شما با موفقیت ویرایش شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#viewAll").load("/Admin/FAQ/ViewAll/" + data);
                $("#myModal").modal('hide');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function checkboxSelectedForFAQ() {
    swal({
        title: "هشدار",
        text: "آیا برای حذف سوال مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {
                var cv = [];
                var checkboxValues = "";
                $("div.checked").children('.forRemove').each(function () {
                    checkboxValues += $(this).val() + "&";
                    cv.push($(this).val());
                });


                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/FAQ/checkboxSelected",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {

                        if (data.redirect) { ShowAccessDenied(); return false; }

                        for (var i = 0; i < cv.length; i++) {

                            $('#faq_tb').find('tr[id=faq_' + cv[i] + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                table.row("#faq_" + cv[i]).remove().draw(false);
                            });
                        }
                        swal({
                            title: "حذف",
                            text: "سوالات انتخاب شده با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                    });
                }
                else {
                    swal({
                        title: "حذف",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            }
        });
}

//////////////////////قسمت گروه پیام های ارتباطی
//
function GetAllContactusMessageGroup() {

    $.ajax({
        method: 'POST',
        url: '/Admin/ContactUsMessage/ViewAll',
        data: '',
        success: function (data) {

            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}
//
function GetAllContactusMessageGroupByLang(lang) {
    $.ajax({
        method: "GET",
        url: "/Admin/ContactUsMessage/ViewAllCMG",
        data: { id: lang },
        success: function (result) {
            var option = "";
            result.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $("#contactusMsgGroup_ddl").html(option);
        },
        error: function (res) {

        }
    });
}
//
function openModalForAddContactusMessageGroup() {
    $.get("/Admin/ContactUsMessage/CreateCMG/", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گروه جدید");
        $("#myModalBody").html(result);
    });

}
//
function InsertContactusMessageGroup() {
    var frm = $("#cmgFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActiveCmg:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var contactusMessageGroup = {
            CMGName: $("#CMGName").val(),
            IsActive: status,
            Lang: $("#language_ddl").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/ContactUsMessage/CreateCMG',
            data: contactusMessageGroup,
            success: function (data) {

                $("#div-loading").addClass('hidden');
                GetAllContactusMessageGroupByLang(data.lang);
                swal({
                    title: "گروه پیام های ارتباطی",
                    text: "گروه " + data.cmgName + " با موفقیت ثبت شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#viewAll").load("/Admin/ContactUsMessage/ViewAll/" + data.lang);
                $("#myModal").modal('hide');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function openModalForEditContactusMessageGroup(id) {
    $.get("/Admin/ContactUsMessage/EditCMG/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گروه");
        $("#myModalBody").html(result);
    });

}
//
function UpdateContactusMessageGroup() {
    var frm = $("#cmgFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActiveCmgEdit:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var contactusMessageGroup = {
            CMG_ID: $("#CMG_ID").val(),
            CMGName: $("#CMGName").val(),
            IsActive: status,
            Lang: $("#language_ddl").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/ContactUsMessage/EditCMG',
            data: contactusMessageGroup,
            success: function (data) {

                $("#div-loading").addClass('hidden');
                GetAllContactusMessageGroupByLang(data.lang);

                swal({
                    title: "گروه پیام های ارتباطی",
                    text: "گروه " + data.cmgName + " با موفقیت ویرایش شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#viewAll").load("/Admin/ContactUsMessage/ViewAll/" + data.lang);
                $("#myModal").modal('hide');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//

function DeleteContactusMessageGroup(id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این گروه مطمئن هستید؟",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $.ajax({
                    url: "/Admin/ContactUsMessage/DeleteCMG",
                    type: "Get",
                    data: { id: id }
                }).done(function (data) {
                    if (data.redirect) { ShowAccessDenied(); return false; }
                    if (data === "NOK") {
                        swal({
                            title: "گروه پیام های ارتباطی",
                            text: "این گروه شامل پیام ارتباطی می باشد بعد از حذف آنها می توانید گروه را حذف نمایید",
                            type: "warning",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                    }

                    else {

                        $('#contactUsGroup_tb').find('tr[id=cmg_' + id + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#contactUsGroup_tb').DataTable();
                            table.row("#cmg_" + id).remove().draw(false);
                        });
                        GetAllContactusMessageGroupByLang(data.lang);
                        swal({
                            title: "گروه پیام های ارتباطی",
                            text: "گروه " + data.cmgName + " با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                    }
                });
            }
        });
}
////////// بخش پیام های ارتباطی
function GetAllContactusMessageByGroupId(id) {

    $.ajax({
        method: 'POST',
        url: '/Admin/ContactUsMessage/ViewAllCM',
        data: { id: id },
        success: function (data) {

            $("#viewAllContactusMessage").html(data);
        },
        error: function (result) {
        }
    });
}
//
function openModalForReplyToMessage(id) {
    $.get("/Admin/ContactUsMessage/EditCM/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("شرح پیام");
        $("#myModalBody").html(result);
    });

}
//
function CheckIsEmptyCMForm() {
    if ($("#MessageAnswer").val() == "") {
        $("#btnSend").prop("disabled", true);
    }
    else {
        $("#btnSend").prop("disabled", false);
    }
}
///
function UpdateContactusMessage() {
    // if ($("#MessageAnswer").val() != "") {
    $("#div-loading").removeClass('hidden');

    var contactusMessage = {
        ContactusMessage_ID: $("#ContactusMessage_ID").val(),
        SenderName: $("#SenderName").text(),
        SenderEmail: $("#SenderEmail").text(),
        SenderMobile: $("#SenderMobile").text(),
        MessageText: $("#MessageText").text(),
        MessageAnswer: $("#MessageAnswer").val(),
        CMGId: $("#CMGId").val(),
        SendDate: $("#SendDate").val(),
        IsNew: false
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/ContactUsMessage/EditCM',
        data: contactusMessage,
        success: function (data) {
            $("#div-loading").addClass('hidden');

            $("#viewAllContactusMessage").load("/Admin/ContactUsMessage/ViewAllCM/" + data.cmgId);
            $("#myModal").modal('hide');
            swal({
                title: "پیام های ارتباطی",
                text: "پاسخ پیام شما برای " + data.senderName + " ارسال گردید",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });

        },
        error: function (result) {
        }
    });
    //}
    //else {
    //    swal("هشدار ", "", "warning");
    //}
}
//
function checkboxSelectedForCM() {

    swal({
        title: "هشدار",
        text: "آیا برای حذف این موارد مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                var checkboxValues = "";
                var Ids = [];
                $("div.checked").children('.forRemove').each(function () {
                    checkboxValues += $(this).val() + "&";
                    Ids.push($(this).val());

                });

                var cv = [];
                cv = checkboxValues.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/ContactUsMessage/checkboxSelected",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }
                        for (var i = 0; i < Ids.length; i++) {

                            $('#contactUs_tb').find('tr[id=cm_' + Ids[i] + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#contactUs_tb').DataTable();
                                table.row("#cm_" + cv[i]).remove().draw(false);
                            });
                        }
                        swal({
                            title: "حذف",
                            text: "موارد انتخاب شده با موفیقت حذف گردید",
                            type: "warning",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید",
                        })

                    });
                }
                else {

                    swal({
                        title: "هشدار",
                        text: "لطفا یک مورد را انتخاب نمایید !",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    })
                }
            }
        });
}

////////////////////////قسمت گروه مطالب

function GetContentsGroupById(parentId) {

    $.ajax({
        method: 'POST',
        url: '/Admin/Contents/ViewCnGroupData',
        data: { id: parentId },
        success: function (data) {

            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}
//
function openModalForAddContentsGroup() {
    if (ParentId2 != undefined) {
        $.get("/Admin/Contents/CreateContentsGroup", function (result) {

            $("#myModal").modal();
            $("#myModalLabel").html("افزودن گروه جدید");
            $("#myModalBody").html(result);
            //getAllContentsGroupForTreeViewWithoutOpt();
        });
    } else {
        swal({
            title: "گروه مطالب",
            text: "ابتدا یک گروه را به عنوان گروه پدر انتخاب کنید",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید"
        });
    }
}
//
var ParentId, ParentId2, GroupId, GroupIdEdit = "";
function getAllContentsGroupForTreeView() { //خواندن گروه ها برای تری ویو ها
    var lang = $("#language_ddl").val();
    var pgID = 0;
    if ($("#language_ddl").val() == "fa-IR") {
        pgID = 1;
    }
    else if ($("#language_ddl").val() == "en-US") { pgID = 2; }
    if (pgID == 1 || pgID == 2) {
        $.ajax({
            method: 'POST',
            url: '/Admin/Contents/createTreeView',
            data: { pgId: pgID, lang: lang },
            //data: "{'parentId':'0','lang':'" + lang + "','trviewElement':''}",

            success: function (response) {

                var $checkableTree = $('#contentsGroup1_tw').treeview({
                    data: response,
                    showBorder: true,
                    showIcon: false,
                    showCheckbox: false,

                    onNodeSelected: function (event, node) {
                        GetContentsGroupById(node.tags);//نمایش جزئیات گروه ها در لیست جزئیات
                        ParentId2 = node.tags;
                    }
                });
                var $checkableTree2 = $('#contentsGroup2_tw').treeview({
                    data: response,
                    showBorder: false,
                    showIcon: false,
                    showCheckbox: false,

                    onNodeSelected: function (event, node) {
                        ParentId = node.tags;//گرفتن آی دی گروه کلیک شده برای ثبت بعنوان فیلد پدر
                    }
                });

                var $checkableTree3 = $('#contentsGroup3_tw').treeview({
                    data: response,
                    showBorder: false,
                    showIcon: false,
                    showCheckbox: false,

                    onNodeSelected: function (event, node) {
                        GroupId = node.tags;
                        ParentId = node.tags;
                        GetAllContentsByGroupId(node.tags);//گرفتن تمامی مطالب براساس گروه 
                    }
                });

                //var $checkableTree4 = $('#contentsGroup4_tw').treeview({
                //    data: response,
                //    showBorder: false,
                //    showIcon: false,
                //    showCheckbox: false,

                //    onNodeSelected: function (event, node) {
                //        GroupId = node.tags;//گرفتن آی دی گروه کلیک شده برای ثبت مطلب
                //    }
                //});

                var $checkableTree5 = $('#contentsGroup5_tw').treeview({
                    data: response,
                    showBorder: false,
                    showIcon: false,
                    showCheckbox: false,

                    onNodeSelected: function (event, node) {
                        GroupIdEdit = node.tags;//گرفتن آی دی گروه کلیک شده برای ویرایش مطلب
                    }
                });

            },
            error: function (response) {

            }
        });
    }
}

//این تابع برای تری ویو هایی است که عملیات چندگانه حذف و ... را ندارند
function getAllContentsGroupForTreeViewWithoutOpt() { //خواندن گروه ها برای تری ویو ها
    var lang = $("#language_ddl").val();

    $.ajax({
        method: 'POST',
        url: '/Admin/Contents/ViewInTreeView',
        data: { parentId: "null", lang: lang, treeview: "", flag: false },
        //data: "{'parentId':'0','lang':'" + lang + "','trviewElement':''}",
        success: function (response) {
            $("#cn2g").html(response);
            $("#cn3g").html(response);
            $("#cn4g").html(response);
        },
        error: function (response) {

        }
    });
}
//
//
function InsertContentsGroup() {
    var frm = $("#contentGroupFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActiveCnGroup:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        if (ParentId2 == undefined) {
            ParentId = 0;
        }

        var contentsGroup = {
            ContentGroup_ID: $("#ContentGroup_ID").val(),
            ContentGroupName: $("#ContentGroupName").val(),
            ParentID: ParentId2,
            IsActive: status,
            Lang: $("#language_ddl").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Contents/CreateContentsGroup',
            data: contentsGroup,
            success: function (data) {

                if (data[0] == "repeate") {
                    swal({
                        title: "گروه مطالب",
                        text: "شناسه گروه وارد شده تکراری است. میتوانید شناسه را از " + data[1] + " به بعد وارد نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });

                    $("#div-loading").addClass('hidden');
                }
                else {
                    $("#div-loading").addClass('hidden');

                    //$("#viewAll").load("/Admin/Contents/ViewAllContentsGroup/" + data.parentID);
                    $("#myModal").modal('hide');
                    $("#cnGroupModal").modal('hide');
                    //اضافه کردن سطر به تری ویو
                    getAllContentsGroupForTreeView();
                    //
                    swal({
                        title: "گروه مطالب",
                        text: "گروه " + data.contentGroupName + " با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function openModalForEditContentsGroup(id) {
    $.get("/Admin/Contents/EditContentsGroup/" + id, function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گروه");
        $("#myModalBody").html(result);

        $("li[data-title=" + id + "]").css({ 'color': 'white' });
    });

}

//

//
function UpdateContentsGroup() {
    var frm = $("#contentGroupFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActiveCnGroupEdit:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }
        var contentsGroup = {
            ContentGroup_ID: $("#ContentGroup_ID").val(),
            ContentGroupName: $("#ContentGroupName").val(),
            ParentID: $("#ParentID").val(),
            IsActive: status,
            Lang: $("#language_ddl").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Contents/EditContentsGroup',
            data: contentsGroup,
            success: function (data) {

                $("#div-loading").addClass('hidden');

                $("#viewAll").load("/Admin/Contents/ViewCnGroupData/" + data.ContentGroup_ID);
                $("#myModal").modal('hide');
                getAllContentsGroupForTreeView();
                swal({
                    title: "گروه مطالب",
                    text: "گروه " + data.contentGroupName + " با موفقیت ویرایش شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//

function DeleteContentsGroup(id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این گروه مطمئن هستید؟",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $.ajax({
                    url: "/Admin/Contents/DeleteContentsGroup",
                    type: "Get",
                    data: { id: id }
                }).done(function (result) {
                    if (data.redirect) { ShowAccessDenied(); return false; }

                    if (result === "NOK") {
                        swal("هشدار", "این گروه شامل مطالبی می باشد بعد از حذف آنها می توانید گروه را حذف نمایید", "warning")
                    }
                    else {
                        $("li[data-title" + id + "]").remove();

                        $('#cnGroup_tb').find('tr[id=cng_' + result.contentGroup_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#cnGroup_tb').DataTable();
                            table.row("#cng_" + result.contentGroup_ID).remove().draw(false);
                        });

                        swal({
                            title: "گروه مطالب",
                            text: "گروه " + result.contentGroupName + " با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                        getAllContentsGroupForTreeView();
                    }
                });
            }
        });
}

///////////////////// بخش مطالب

function GetAllContentsByGroupId(Id) {
    $.ajax({
        method: "GET",
        url: "/Admin/Contents/ViewAllContents",
        data: { id: Id },
        success: function (result) {
            $("#viewAllContents").html(result);
        },
        error: function (res) {

        }
    });
}
//
function openModalForAddContents() {
    if (GroupId != undefined) {
        $.get("/Admin/Contents/CreateContents", function (result) {

            $("#myModal").modal();
            $("#myModalLabel").html("افزودن مطلب جدید");
            $("#myModalBody").html(result);
            $(".modal-dialog").css({ 'width': '75%' });
            //$("li[data-title=" + id + "]").css({ 'color': 'white' });
        });
    }
    else {
        swal("هشدار", "لطفا گروهی را برای مطلب خود انتخاب کنید", "warning");
    }
}
//
function InsertContents() {
    var frm = $("#contentFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');


        var contents = new FormData();
        var isactive = $('#isActiveContents:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var files = $("#ContentUploader").get(0).files;
        if (files.length == 0) {
            $("#validation_file").text("عکس مورد نظر خود را انتخاب نمائید");
            $("#div-loading").addClass('hidden');
            return false;
        }
        if (files.length > 0) {
            contents.append("Content_ID", $("#Content_ID").val()),
                contents.append("ContentTitle", $("#ContentTitle").val()),
                contents.append("ContentImage", files[0]),
                contents.append("ContentText", CKEDITOR.instances['ContentText'].getData()),
                contents.append("ContentSummary", $("#ContentSummary").val()),
                contents.append("IsActive", status),
                contents.append("ContentGroupId", GroupId)
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Contents/CreateContents',
            data: contents,
            contentType: false,//برای ارسال عکس باید این مولفه با فالس مقداردهی شود
            processData: false, //برای استفاده از فرم دیتا باید این مولفه اضافه شود
            success: function (data) {

                if (data[0] == "repeate") {
                    swal({
                        title: "مطلب",
                        text: "شناسه وارد شده تکراری است. میتوانید شناسه را از " + data[1] + " به بعد وارد نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                }
                else {
                    $("#div-loading").addClass('hidden');
                    $("#viewAllContents").load("/Admin/Contents/ViewAllContents/" + data.contentGroupId);

                    $("#myModal").modal('hide');
                    swal({
                        title: "مطلب",
                        text: "مطلب " + data.contentTitle + " با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            },
            error: function (result) {
            }
        });

    }
    else {
        return false;
    }
}
//
function openModalForEditContents(id) {
    $.get("/Admin/Contents/EditContents/" + id, function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش مطلب");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
        $("li[data-title=" + id + "]").css({ 'color': 'white' });

    });

}
//
function UpdateContents() {
    var frm = $("#contentFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');


        var contents = new FormData();
        var isactive = $('#isActiveContentsEdit:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var files = $("#ContentUploader").get(0).files;

        contents.append("Content_ID", $("#Content_ID").val()),
            contents.append("ContentTitle", $("#ContentTitle").val()),
            contents.append("ContentImage", $("#OldContentImage").val()),
            contents.append("ContentText", CKEDITOR.instances['ContentTextEdit'].getData()),
            //contents.append("ContentText", $("#ContentTextEdit").val()),
            contents.append("ContentSummary", $("#ContentSummary").val()),
            contents.append("CreateDate", $("#CreateDate").val()),
            contents.append("IsActive", status)

        if (GroupIdEdit != "") {

            contents.append("ContentGroupId", GroupIdEdit)
        }
        else {
            contents.append("ContentGroupId", $("#GroupIdEdit").val())
        }
        if (files.length > 0) {
            contents.append("NewContentImage", files[0])
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Contents/EditContents',
            data: contents,
            contentType: false,//برای ارسال عکس باید این مولفه با فالس مقداردهی شود
            processData: false, //برای استفاده از فرم دیتا باید این مولفه اضافه شود
            success: function (data) {

                if (data[0] == "repeate") {
                    swal({
                        title: "مطلب",
                        text: "شناسه وارد شده تکراری است.میتوانید شناسه را از " + data[1] + " به بعد وارد نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                }
                else {
                    $("#div-loading").addClass('hidden');
                    $("#viewAllContents").load("/Admin/Contents/ViewAllContents/" + data.contentGroupId);

                    $("#myModal").modal('hide');

                    swal({
                        title: "مطلب",
                        text: "مطلب " + data.contentTitle + " با موفقیت ویرایش شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            },
            error: function (result) {
                //$("#div-loading").addClass('hidden');
            }
        });
    }
    else {
        return false;
    }
}
//
function DeleteContents(id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این مطلب مطمئن هستید؟",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $.ajax({
                    url: "/Admin/Contents/DeleteContents",
                    type: "Get",
                    data: { id: id }
                }).done(function (result) {
                    if (data.redirect) { ShowAccessDenied(); return false; }

                    $('#content_tb').find('tr[id=cn_' + id + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                        var table = $('#content_tb').DataTable();
                        table.row("#cn_" + id).remove().draw(false);
                    });

                    swal({
                        title: "مطلب",
                        text: "مطلب " + result + " با موفقیت حذف شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                });
            }
        });
}
//
function GetAllContentsGalleryByContentId(contentId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Contents/ViewAllContentsGallery",
        data: { id: contentId },
        success: function (result) {

            $("#contents").removeClass("active");
            $("#contentsGallery").addClass("active");

            $("#contentTab").removeClass("active");
            $("#contentGalleryTab").addClass("active");
            $("#viewAllContentsGallery").html(result);
        },
        error: function (res) {

        }
    });
}
//
function openModalForAddContentsGallery() {
    var lang = $("#language_ddl").val();
    $.get("/Admin/Contents/CreateContentsGallery/" + lang, function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گالری مطلب");
        $("#myModalBody").html(result);
        //$("li[data-title=" + id + "]").css({ 'color': 'white' });
    });

}
//
function GetAllGalleryGroupByLangForContents(lang) {//

    $.ajax({ //
        method: "GET",
        url: "/Admin/Gallery/ViewAllGroupGallery",
        data: { id: lang },
        success: function (result) {

            var option = "";//<option value='0'>انتخاب کنید ...</option>";
            result.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $("#galleryGroup_ddl").html(option);
            //
        },
        error: function (res) {

        }
    });
}
//
function GetAllGalleryByGroupIdForContents(GroupId) {//نمایش گالری براساس گروه گالری در دراپ داون ثبت گالری مطلب 

    $.ajax({ //گرفتن اطلاعات جیسون از سمت سرور و دی سریالایز کردن آن
        method: "GET",
        url: "/Admin/Contents/GetAllGalleryByGroupId",
        data: { id: GroupId },
        success: function (result) {

            var option = "";
            if (result.length > 0) {
                result.forEach(function (obj) {
                    option += "<option value=" + obj.value + ">" + obj.text + "</option>";
                });
            }
            else {
                option = "<option value='0'>گالری ندارد</option>";
            }
            $("#GalleryId").html(option);

        },
        error: function (res) {

        }
    });

}
//
function InsertContentsGallery() {
    $("#div-loading").removeClass('hidden');

    var ContentID = $("#contentId").text();

    if (ContentID != "") {
        var contentsGallery = {
            ContentId: ContentID,
            GalleryId: $("#GalleryId").val()
        }


        $.ajax({
            method: 'POST',
            url: '/Admin/Contents/CreateContentsGallery',
            data: contentsGallery,
            success: function (data) {
                if (data == "repeate") {
                    $("#div-loading").addClass('hidden');
                    swal("هشدار", "گالری انتخاب شده در لیست این مطلب ثبت شده است!", "warning");
                }
                else {
                    $("#div-loading").addClass('hidden');
                    $("#viewAllContentsGallery").load("/Admin/Contents/ViewAllContentsGallery/" + data + "");
                    $("#myModal").modal('hide');
                    swal({
                        title: "گالری مطلب",
                        text: "گالری مطلب با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        swal({
            title: "گالری مطلب",
            text: "مطلبی برای گالری انتخاب نشده است",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید"
        });
        $("#div-loading").addClass('hidden');
    }
}
//
function checkboxSelectedForContentsGallery() {

    swal({
        type: 'warning',
        title: "هشدار",
        text: "آیا برای حذف این موارد مطمئن هستید؟",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                var checkboxValues = "";
                $("div.checked").children('.forRemove').each(function () {
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Contents/checkboxSelected",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        for (var i = 0; i < cv.length - 1; i++) {
                            $('#ContentGallery_tb').find('tr[id=cngall_' + cv[i] + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#ContentGallery_tb').DataTable();
                                table.row("#cngall_" + cv[i]).remove().draw(false);
                            });

                        }
                        swal({
                            title: "گالری مطلب",
                            text: "گالری این مطلب با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                    });
                }
                else {
                    swal({
                        title: "گالری مطلب",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            }
        });
}
////////////////// بخش فایل مطالب
function GetAllContentsFilesByContentId(contentId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Contents/ViewAllContentsFiles",
        data: { id: contentId },
        success: function (result) {

            $("#contents").removeClass("active");
            $("#contentsFile").addClass("active");

            $("#contentTab").removeClass("active");
            $("#contentFileTab").addClass("active");
            $("#viewAllContentsFiles").html(result);
        },
        error: function (res) {

        }
    });
}
//
function openModalForAddContentsFile() {
    $.get("/Admin/Contents/CreateContentFiles", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن فایل مطلب");
        $("#myModalBody").html(result);
        //$("li[data-title=" + id + "]").css({ 'color': 'white' });
    });

}
//
function GetAllAvfGroupByLangForContents(lang) {//

    $.ajax({ //
        method: "GET",
        url: "/Admin/FilesManage/GetAllGroupsByLang",
        data: { id: lang },
        success: function (result) {

            var option = "";//"<option value='0'>انتخاب کنید ...</option>";
            result.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $("#avfGroup_ddl").html(option);
            //
        },
        error: function (res) {

        }
    });
}
//
function GetAllAvfByGroupIdForContents(GroupId) {

    $.ajax({ //گرفتن اطلاعات جیسون از سمت سرور و دی سریالایز کردن آن
        method: "GET",
        url: "/Admin/Contents/GetAllFilesByGroupId",
        data: { id: GroupId },
        success: function (result) {
            debugger;
            var option = "";
            if (result.length > 0) {
                result.forEach(function (obj) {
                    option += "<option value=" + obj.value + ">" + obj.text + "</option>";
                });
            }
            else {
                option = "<option value='0'>فایل ندارد</option>";
            }
            $("#AvfId").html(option);

        },
        error: function (res) {

        }
    });

}
//
function InsertContentFiles() {
    $("#div-loading").removeClass('hidden');

    var ContentID = $("#contentId").text();

    if (ContentID != "") {
        var contentFiles = {
            ContentId: ContentID,
            AvfId: $("#AvfId").val()
        }


        $.ajax({
            method: 'POST',
            url: '/Admin/Contents/CreateContentFiles',
            data: contentFiles,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                if (data == "repeate") {

                    swal("هشدار", "فایل انتخاب شده در لیست فایل های این مطلب ثبت شده است!", "warning");
                }
                else {
                    $("#viewAllContentsFiles").load("/Admin/Contents/ViewAllContentsFiles/" + data + "");
                    $("#myModal").modal('hide');
                    swal({
                        title: "فایل مطلب",
                        text: "فایل مطلب با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        swal({
            title: "فایل مطلب",
            text: "مطلبی برای ثبت فایل انتخاب نشده است",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید"
        });

    }
}
//
function checkboxSelectedForContentsFiles(ContentId) {

    swal({
        type: 'warning',
        title: "هشدار",
        text: "آیا برای حذف این موارد مطمئن هستید؟",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                var checkboxValues = "";
                $("div.checked").children('.forRemove').each(function () {
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Contents/checkboxSelectedContentFile",
                        type: "Get",
                        data: { values: checkboxValues, id: ContentId }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        for (var i = 0; i < cv.length - 1; i++) {
                            $('#ContentFile_tb').find('tr[id=cnFile_' + cv[i] + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#ContentFile_tb').DataTable();
                                table.row("#cnFile_" + cv[i]).remove().draw(false);
                            });

                        }
                        swal({
                            title: "فایل مطلب",
                            text: "فایل این مطلب با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                    });
                }
                else {
                    swal({
                        title: "فایل مطلب",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            }
        });
}
////////////////// بخش کلمات کلیدی مطالب
function InsertOrUpdateContentsTag(contentId) {
    $("#div-loading").removeClass('hidden');


    $.ajax({
        method: 'POST',
        url: '/Admin/Contents/CreateOrEditContentsTag',
        data: { id: contentId },
        success: function (data) {

            if (data == "add") {
                openModalForAddContentsTags(contentId);
            }
            else {
                openModalForEditContentsTags(contentId);
            }
        },
        error: function (result) {
        }
    });
}
function openModalForAddContentsTags(ContentId) { //برای دادن آی دی مطلب به ویو تگ ها آی دی رو بصورت پارامتر میگیریم و به کنترلر پاس میدهیم و از آنجا به ویو
    // ایجاد تگ توسط ویو بگ ارسال میکنیم
    $.get("/Admin/Contents/CreateContentsTag/" + ContentId, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن کلمات کلیدی");
        $("#myModalBody").html(result);
    });

}
//
function InsertContentsTags() {
    $("#div-loading").removeClass('hidden');
    var contentsTags = {
        ContentId: $("#ContentId").val(),
        TagText: $("#TagText").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/Contents/CreateContentsTag',
        data: contentsTags,
        success: function (data) {
            $("#div-loading").addClass('hidden');
            $("#myModal").modal('hide');
            $("#viewAllContents").load("/Admin/Contents/ViewAllContents/" + GroupId + "");
            swal({
                title: "کلمات کلیدی",
                text: "کلمات کلیدی مطلب ثبت شد",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
        },
        error: function (result) {
        }
    });
}
//
function openModalForEditContentsTags(id) {
    $.get("/Admin/Contents/EditContentsTag/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش کلمات کلیدی");
        $("#myModalBody").html(result);
    });

}
//
function UpdateContentsTags() {
    var contentID = $("#ContentId").val();

    if ($("#TagText").val() == "") { //اگر کاربر تصمیم به پاک کردن کلمات کلیدی کرد اون رکورد در دیتابیس هم حذف شود
        DeleteContentsTag(contentID);
    }
    else {

        $("#div-loading").removeClass('hidden');
        var contentsTags = {
            Tag_ID: $("#Tag_ID").val(),
            ContentId: contentID,
            TagText: $("#TagText").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Contents/EditContentsTag',
            data: contentsTags,
            success: function (data) {

                $("#div-loading").addClass('hidden');
                $("#myModal").modal('hide');
                $("#viewAllContents").load("/Admin/Contents/ViewAllContents/" + GroupId + "");
                swal({
                    title: "کلمات کلیدی",
                    text: "کلمات کلیدی مطلب با موفقیت ویرایش شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
            },
            error: function (result) {
            }
        });
    }
}
//
function DeleteContentsTag(contentId) {

    $("#div-loading").removeClass('hidden');

    $.ajax({
        method: 'POST',
        url: '/Admin/Contents/DeleteContentsTag',
        data: { id: contentId },
        success: function (data) {
            if (data.redirect) { ShowAccessDenied(); return false; }

            $("#myModal").modal('hide');
            $("#div-loading").addClass('hidden');

            swal({
                title: "کلمات کلیدی",
                text: "کلمات کلیدی مطلب با موفقیت حذف شد",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
            //swal("تبریک", "کلمات ", "success");
            //$("#galleryGroup_" + Id).hide();
        },
        error: function (result) {

        }
    });
}
//
////////////////////// بخش نظرات مطالب
function GetAllCommentsByContentId(contentId) {//

    $.ajax({ //
        method: "GET",
        url: "/Admin/Contents/ViewAllComments",
        data: { id: contentId },
        success: function (result) {

            $("#contents").removeClass("active");
            $("#contentsComment").addClass("active");

            $("#contentTab").removeClass("active");
            $("#contentCommentTab").addClass("active");

            $("#viewAllContentComments").html(result);
            //
        },
        error: function (res) {

        }
    });
}
//
function checkboxSelectedForContentsComments() {

    swal({
        title: "هشدار",
        text: "آیا برای حذف این موارد مطمئن هستید؟",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete.value) {

                var checkboxValues = "";
                $("div.checked").children('.forRemove').each(function () {
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Contents/checkboxSelectedComments",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        for (var i = 0; i < cv.length - 1; i++) {
                            $("#comment_" + cv[i]).hide("slow");
                        }
                        swal({
                            title: "نظرات مطالب",
                            text: "نظر این مطلب با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });

                    });
                }
                else {
                    swal({
                        title: "نظرات مطالب",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            }
        });
}
//
function openModalForEditComment(id) {
    $.get("/Admin/Contents/EditComment/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش وضعیت نظرات ");
        $("#myModalBody").html(result);
    });

}
//زمانی که اطلاعات به درستی ثبت شد این تابع فراخوانی می شود
var onSuccessEditComment = function (res) {
    $("#div-loading").addClass('hidden');

    $("#viewAllContentComments").load("/Admin/Contents/ViewAllComments");
    $("#myModal").modal('hide');
    //swal("تبریک", "زبان " + res.lang_Name + " با موفقیت ثبت شد", "success");
};


//
function UpdateContentsComment() {
    $("#div-loading").removeClass('hidden');
    var status = $("#IsConfirmedEdit:checked").length;

    if (status == 1) {
        status = "true";
    }
    else {
        status = "false";
    }
    var contentsComments = {
        Comment_ID: $("#Comment_ID").val(),
        Comment: $("#Comment").val(),
        UserName: $("#UserName").val(),
        Email: $("#Email").val(),
        CommentDate: $("#dateComment").val(),
        PositiveScore: $("#PositiveScore").val(),
        NegativeScore: $("#NegativeScore").val(),
        IsConfirmed: status,
        IsNew: $("#IsNew").val(),
        ContentId: $("#ContentId").val(),
        ContentGroupId: $("#ContentGroupId").val(),

    }


    $.ajax({
        method: 'POST',
        url: '/Admin/Contents/EditComment',
        data: contentsComments,
        success: function (data) {


            $("#div-loading").addClass('hidden');

            $("#viewAllContentComments").load("/Admin/Contents/ViewAllComments/" + data.contentId);
            $("#myModal").modal('hide');
            swal({
                title: "نظرات مطالب",
                text: "وضعیت این نظر با موفقیت ویرایش شد",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });

        },
        error: function (result) {
        }
    });
}
/////////////////////////بخش گروه گالری
function openModalForAddGalleryGroup() {
    $.get("/Admin/Gallery/CreateGalleryGroup", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گروه جدید");
        $("#myModalBody").html(result);
    });

}


//
function IsExist(Id, Selector, ElementMsg) {
    $.ajax({
        method: 'POST',
        url: '/Admin/Gallery/IsExist',
        data: { id: Id },
        success: function (data) {

            if (data[0] === "repeate") {
                $(Selector).focus();
                $(ElementMsg).text("شناسه وارد شده تکرار است شما می توانید شناسه را از " + data[1] + " به بعد وارد نمایید");
            }
        },
        error: function (result) {

        }
    });
}
///
function InsertGalleryGroup() {
    var frm = $("#galleryGroupFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');

        var galleryGroup = {
            GalleryGroup_ID: $("#GalleryGroup_ID").val(),
            GalleryGroupTitle: $("#GalleryGroupTitle").val(),
            Lang: $("#language_ddl").val()
        }


        $.ajax({
            method: 'POST',
            url: '/Admin/Gallery/CreateGalleryGroup',
            data: galleryGroup,
            success: function (data) {
                //if (data[0] === "repeate") {
                //    swal("هشدار", "شناسه وارد شده تکراری است. میتوانید شناسه را از " + data[1] + " به بعد وارد نمایید", "warning");
                //    $("#div-loading").addClass('hidden');
                //}
                //else {
                $("#div-loading").addClass('hidden');
                $("#viewAll").load("/Admin/Gallery/ViewAll/" + data.lang + "");
                $("#myModal").modal('hide');
                GetAllGalleryGroup(data.lang);
                swal({
                    title: "گروه گالری",
                    text: "گروه گالری " + data.galleryGroupTitle + " با موفقیت ثبت گردید",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                //}
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function openModalForEditGalleryGroup(id) {
    $.get("/Admin/Gallery/EditGalleryGroup/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گروه گالری");
        $("#myModalBody").html(result);
    });

}
//
function UpdateGalleryGroup() {
    var frm = $("#galleryGroupFrmEdit").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');

        var galleryGroup = {
            GalleryGroup_ID: $("#GalleryGroup_ID").val(),
            GalleryGroupTitle: $("#GalleryGroupTitle").val(),
            Lang: $("#language_ddl").val()
        }


        $.ajax({
            method: 'POST',
            url: '/Admin/Gallery/EditGalleryGroup',
            data: galleryGroup,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                $("#viewAll").load("/Admin/Gallery/ViewAll/" + data.lang + "");
                $("#myModal").modal('hide');
                GetAllGalleryGroup(data.lang);
                swal({
                    title: "گروه گالری",
                    text: "گروه گالری " + data.galleryGroupTitle + " با موفقیت ویرایش گردید",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });

            },
            error: function (result) {
            }
        });
    } else {
        return false;
    }
}
//
function DeleteGalleryGroup(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این گروه مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Gallery/DeleteGalleryGroup',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }
                        if (data === "NOK") {
                            swal({
                                title: "حذف",
                                text: "این گروه شامل گالری می باشد بعد از حذف آنها می توانید گروه را حذف نمایید",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            })
                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            GetAllGalleryGroup(data.lang);
                            swal({
                                title: "حذف",
                                text: "گروه گالری " + data.galleryGroupTitle + " با موفقیت حذف شد.",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                            $('#galleryg_tb').find('tr[id=galleryGroup_' + Id + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#galleryg_tb').DataTable();
                                table.row("#galleryGroup_" + Id).remove().draw(false);
                            });

                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
//////////////////////////بخش گالری

function GetAllGalleryGroup(lang) { //برای نمایش گروه ها در دراپ داون صفحه گالری
    $.ajax({
        method: "GET",
        url: "/Admin/Gallery/ViewAllGroupGallery",
        data: { id: lang },
        success: function (result) {

            var option = "";
            result.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $("#galleryGroup_ddl").html(option);
            $("#GalleryGroupId").html(option);
        },
        error: function (res) {

        }
    });
}
//
function GetAllGalleryByGroupId(GroupId) {//نمایش گالری براساس گروه گالری
    $.ajax({
        method: "GET",
        url: "/Admin/Gallery/ViewAllGallery",
        data: { id: GroupId },
        success: function (result) {

            $("#viewAllGallery").html(result);
        },
        error: function (res) {

        }
    });
}
//
function openModalForAddGallery() {
    var groupId = $("#galleryGroup_ddl").val();
    if (groupId == 0) {
        swal({
            title: " گالری",
            text: "لطفا گروهی برای گالری انتخاب نمایید",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید"
        });
    }
    else {
        $.get("/Admin/Gallery/CreateGallery", function (result) {
            $("#myModal").modal();
            $("#myModalLabel").html("افزودن گالری جدید");
            $("#myModalBody").html(result);
        });
    }
}
//
function InsertGallery() {
    var frm = $("#galleryFrm").valid();
    if (frm === true) {


        $("#div-loading").removeClass('hidden');
        var gallery = new FormData();
        var files = $("#ImageUpload").get(0).files;
        var isactive = $('#IsActiveGallery:checked').length;
        var groupId = $("#galleryGroup_ddl").val();



        //if (files.length > 0) {
        if (isactive == 1) {
            gallery.append("IsActive", "true")
        }
        else {
            gallery.append("IsActive", "false")
        }
        gallery.append("GalleryGroupId", groupId),
            gallery.append("GalleryTitle", $("#GalleryTitle").val()),
            gallery.append("galleryPic", files[0])
        //}


        $.ajax({
            method: 'POST',
            url: '/Admin/Gallery/CreateGallery',
            data: gallery,
            contentType: false,
            processData: false,
            success: function (data) {
                swal({
                    title: " گالری",
                    text: "گالری " + data.galleryTitle + " با موفقیت ثبت شد  ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#viewAllGallery").load("/Admin/Gallery/ViewAllGallery/" + data.galleryGroupId);
                $("#div-loading").addClass('hidden');
                $("#myModal").modal('hide');

            },
            error: function (result) {
                alert(result.error);
            }
        });
    }
    else {
        return false;
    }
}

//
function DeleteGallery(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این گالری مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Gallery/DeleteGallery',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }
                        if (data === "Invalid") {
                            swal({
                                title: "هشدار",
                                text: "این گالری شامل تصاویری می باشد بعد از حذف آنها می توانید گالری را حذف نمایید",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            })
                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "حذف",
                                text: "گالری با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            })

                            $('#gallery_tb').find('tr[id=gallery_' + Id + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#gallery_tb').DataTable();
                                table.row("#gallery_" + Id).remove().draw(false);
                            });
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}

//
function openModalForEditGallery(id) {
    $.get("/Admin/Gallery/EditGallery/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گالری");
        $("#myModalBody").html(result);
    });
}
//
function UpdateGallery() {
    var frm = $("#galleryFrmEdit").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');
        var gallery = new FormData();
        var files = $("#ImageUpload").get(0).files;
        var isactive = $('#IsActiveEdit:checked').length;



        if (isactive == 1) {
            gallery.append("IsActive", "true")
        }
        else {
            gallery.append("IsActive", "false")
        }
        gallery.append("Gallery_ID", $("#Gallery_ID").val()),
            gallery.append("GalleryGroupId", $("#GalleryGroupId").val()),
            gallery.append("GalleryTitle", $("#GalleryTitle").val()),
            gallery.append("ImageNameGallery", files[0]),
            gallery.append("GalleryImage", $("#oldImage").val())

        $.ajax({
            method: 'POST',
            url: '/Admin/Gallery/EditGallery',
            data: gallery,
            contentType: false,
            processData: false,
            success: function (data) {
                swal({
                    title: " گالری",
                    text: "گالری " + data.galleryTitle + " با موفقیت ویرایش شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#viewAllGallery").load("/Admin/Gallery/ViewAllGallery/" + data.galleryGroupId);
                $("#div-loading").addClass('hidden');
                $("#myModal").modal('hide');
            },
            error: function (result) {
                alert(result.error);
            }
        });
    }
    else {
        return false;
    }
}
/////////////////// بخش تصاویر گالری


function ShowAllViewPictures(id) {

    $.get("/Admin/Gallery/ViewAllGalleryPicture/" + id, function (result) {
        $("#viewAllGalleryPictures").html(result);
        $("#gallery").removeClass("active");
        $("#galleryPic").addClass("active");

        $("#galleryTab").removeClass("active");
        $("#galleryPicTab").addClass("active");
    });
}
//
function openModalForAddGalleryPicture() {
    $.get("/Admin/Gallery/Upload", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن تصاویر گالری جدید");
        $("#myModalBody").html(result);
    });

}
//
function checkboxSelectedGalleryPictures() {

    swal({
        type: "warning",
        title: "هشدار",
        text: "آیا برای حذف این راه ارتباطی مطمئن هستید؟",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                var checkboxValues = "";
                $("div.checked").children('.forRemove').each(function () { // گرفتن ولیو های چکس باکس هایی که تیک خورده باشند
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");

                if (cv == "") {
                    swal({
                        title: "حذف",
                        text: "عکس های مورد نظر را انتخاب کنید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    })
                    return false;
                }
                $.ajax({
                    url: "/Admin/Gallery/checkboxSelected",
                    type: "Get",
                    data: { values: checkboxValues }
                }).done(function (data) {
                    if (data.redirect) { ShowAccessDenied(); return false; }

                    for (var i = 0; i < cv.length - 1; i++) {
                        var id = "gp_" + cv[i].replace(".", "_");
                        $('#galleryPic_tb').find('tr[id=gp_' + cv[i].replace(".", "_") + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#galleryPic_tb').DataTable();
                            table.row("#gp_" + cv[i].replace(".", "_")).remove().draw(false);
                        });
                    }
                    swal({
                        title: "حذف",
                        text: "تصاویر گالری با موفقیت حذف گردید",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                });
            }
        });
}
//
function removePictureFile2(pictureFileName) {
    //تابعی که با زدن ضرب در روی عکس فراخوانی میشود

    $.ajax({
        url: "/Admin/Gallery/RemoveFileFromTemp",
        type: "Get",
        data: { filename: pictureFileName }
    }).done(function (result) {
        var responsePart = eval(result.d);

        removedPicName = pictureFileName;
        for (var i = 0; i < newImageArray.length; i++) {
            if (newImageArray[i] == pictureFileName) {
                newImageArray.splice(i, 1);
                break;
            }
        }
        var arr = '';
        for (var i = 0; i < newImageArray.length; i++) {
            arr += newImageArray[i] + ' * ';
        }
    });

}
//
function Upload() { //ذخیره اصلی اطلاعات

    var frm = $("#galleryPicFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var galleryId = $("#GalleryId").val();
        if (galleryId == undefined) {
            swal({
                type: "warning",
                title: "هشدار",
                text: "لطفا ابتدا گالری تصاویر را انتخاب کنید",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
            })
            $("#div-loading").addClass('hidden');

        }
        else {
            var formData = {
                GalleryId: galleryId,
                GalleryPicTitle: $("#GalleryPicTitle").val(),
            }

            $.ajax({
                data: formData,
                type: 'get',
                url: '/Admin/Gallery/SaveToMainFolder',
                success: function (response) {
                    if (response != null) {
                        //$("#myModal").modal('hide');
                        $("#viewAllGalleryPictures").load("/Admin/Gallery/ViewAllGalleryPicture/" + response);
                        $("#div-loading").addClass('hidden');
                        swal("ثبت", "تصاویر گالری با موفقیت ثبت گردید", "success");
                        newImageArray = [];
                        $("#imgPreviews").html('');
                        $("#GalleryPicTitle").val('');
                    }
                },

            });
        }
    }
    else {
        return false;
    }
}

/////////////////////////Validate Length Of TextBox

function ValueTextBoxCounter(textValue, element, label, maxLength) {

    var len = textValue.value.length;
    len = maxLength - len;
    if (len < 0) {
        element.val(textValue.value.slice(0, maxLength));
        return;
    }
    label.text(len);
}
//////////////////// بخش گروه پروژه ها         
var pcgParentId, pcGroupId, projectGroupId, projectGroupIdEdit = "";
function getAllProjectsGroupForTreeView() { //خواندن گروه ها برای تری ویو ها
    var lang = $("#language_ddl").val();

    $.ajax({
        method: 'POST',
        url: '/Admin/Projects/createTreeView',
        data: { pgId: 1, lang: lang },
        success: function (response) {

            var $checkableTree = $('#projectGroup1_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    pcgParentId = node.tags;
                    GetProjectGroupById(node.tags);
                },
                onNodeUnselected: function (event, node) {
                    pcgParentId = undefined;
                    //GetProductGroupById(node.tags);
                }
            });

            var $checkableTree2 = $('#projectsGroup2_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    pcGroupId = node.tags;
                    GetAllProjectsByGroupById(node.tags, "#viewAllProjects");
                }
            });

            //// جهت نمایش در ویو ثبت جزئیات محصول
            //var $checkableTree3 = $('#productsGroup3_tw').treeview({
            //    data: response,
            //    showBorder: false,
            //    showIcon: false,
            //    showCheckbox: false,

            //    onNodeSelected: function (event, node) {
            //        productGroupId = node.tags;
            //    }
            //});

            //var $checkableTree4 = $('#productsGroup4_tw').treeview({
            //    data: response,
            //    showBorder: false,
            //    showIcon: false,
            //    showCheckbox: false,

            //    onNodeSelected: function (event, node) {
            //        GetAllProductsForSimilarByGroupById(node.tags);
            //    }
            //});
        },
        error: function (response) {

        }
    });
}
/////
function GetProjectGroupById(Id) {
    $.ajax({
        method: "GET",
        url: "/Admin/Projects/ViewProjectGroup",
        data: { id: Id },
        success: function (result) {

            $("#viewAll").html(result);
        },
        error: function (res) {

        }
    });
}
/////
function openModalForAddProjectsGroup() {
    id = $("#language_ddl").val();
    $.get("/Admin/Projects/CreateProjectsGroup", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گروه پروژه جدید");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '60%' });
    });

}
/////
function InsertProjectGroup() {
    var frm = $("#ProjectGroup_Form").valid();
    if (!frm) return false;

    $("#div-loading").removeClass('hidden');
    var projectGroup = new FormData();
    var files = $("#imgUpload").get(0).files;


    var isParent = false;
    if (pcgParentId == undefined) {
        pcgParentId = null;
        isParent = true;
    }


    if (files.length > 0) {

        projectGroup.append("Title", $("#Title").val()),
            projectGroup.append("Description", $("#Description").val()),
            projectGroup.append("ParentId", pcgParentId),
            projectGroup.append("IsParent", isParent),
            projectGroup.append("pgImage", files[0])
        projectGroup.append("Lang", $("#language_ddl").val())
    }


    $.ajax({
        method: 'POST',
        url: '/Admin/Projects/CreateProjectsGroup',
        data: projectGroup,
        contentType: false,
        processData: false,
        success: function (data) {
            getAllProjectsGroupForTreeView();
            swal({
                title: " گروه پروژه",
                text: "گروه پروژه " + data.title + " با موفقیت ثبت شد  ",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
            $("#viewAll").load("/Admin/Projects/ViewProjectGroup/" + data.parentId);
            $("#div-loading").addClass('hidden');
            $("#myModal").modal('hide');

        },
        error: function (result) {
            alert(result.error);
        }
    });

}
/////
function openModalForEditProjectsGroup(id) {
    $.get("/Admin/Projects/EditProjectsGroup/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گروه پروژه ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '60%' });
    });

}
/////
function UpdateProjectGroup() {
    $("#div-loading").removeClass('hidden');
    var projectGroup = new FormData();
    var files = $("#imgUpload").get(0).files;

    var isParent = false;
    if (pcgParentId == undefined) {
        pcgParentId = null;
        isParent = true;
    }



    projectGroup.append("ProjectGroup_ID", $("#ProjectGroup_ID").val()),
        projectGroup.append("Title", $("#Title").val()),
        projectGroup.append("Description", $("#Description").val()),
        projectGroup.append("ParentId", $("#ParentId").val()),
        projectGroup.append("IsParent", $("#IsParent").val()),
        projectGroup.append("GroupImage", $("#oldGroupImage").val()),
        projectGroup.append("pgImage", files[0]),

        projectGroup.append("Lang", $("#language_ddl").val())


    $.ajax({
        method: 'POST',
        url: '/Admin/Projects/EditProjectsGroup',
        data: projectGroup,
        contentType: false,
        processData: false,
        success: function (data) {
            getAllProjectsGroupForTreeView();
            swal({
                title: " گروه پروژه",
                text: "گروه پروژه " + data.title + " با موفقیت ویرایش شد  ",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
            $("#viewAll").load("/Admin/Projects/ViewProjectGroup/" + data.parentId);
            $("#div-loading").addClass('hidden');
            $("#myModal").modal('hide');

        },
        error: function (result) {
            alert(result.error);
        }
    });

}
//////
function DeleteProjectGroup(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این گروه پروژه مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Projects/DeleteProjectGroup',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data === "NOK") {
                            swal({
                                title: " گروه پروژه",
                                text: "گروه پروژهاین گروه شامل گروه های فرزند می باشد ابتدا باید فرزندان حذف شوند",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                            $("#div-loading").addClass('hidden');
                        }
                        else if (data === "isProjects") {
                            swal({
                                title: " گروه پروژه",
                                text: "این گروه شامل پروژه هایی می باشد ابتدا باید محصولات آن حذف شوند",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                            $("#div-loading").addClass('hidden');
                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: " گروه پروژه",
                                text: "گروه " + data.title + "  با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });

                            $("#pg_" + data.projectGroup_ID).hide("slow");
                            getAllProjectsGroupForTreeView();
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
///////

///////////////////// بخش پروژه ها
function SetSlugProjects() {

    var pTitle = $("#Title").val();
    pTitle = pTitle.replace(/\s+/g, "-");
    //pTitle = pTitle + "/";
    $("#Slug").val(pTitle);
}
function GetAllProjectsByGroupById(groupId, elementView) {
    $.ajax({
        method: "GET",
        url: "/Admin/Projects/ViewAllProjects",
        data: { id: groupId },
        success: function (result) {

            $(elementView).html(result);
        },
        error: function (res) {

        }
    });
}
/////
function openModalForAddProjects() {
    //id = $("#language_ddl").val();
    if (pcGroupId != undefined) {
        $.get("/Admin/Projects/CreateProjects", function (result) {
            $("#myModal").modal();
            $("#myModalLabel").html("افزودن پروژه جدید");
            $("#myModalBody").html(result);
            $(".modal-dialog").css({ 'width': '75%' });
        });
    }
    else {
        swal({
            title: "پروژه",
            text: "لطفا برای ثبت پروژه گروهی را انتخاب نمایید",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید"
        });
    }
}
function InsertProjects() {


    var frm = $("#projectsFrm").valid();
    if ($("#Description").val() == "") {
        $("#Desc_val").text("توضیحات را وارد کنید");
        $("#imgUploader_val").text("لطفا تصویر را انتخاب نمائید");
        return false;
    }
    var files = $("#imgUploader").get(0).files;
    if (files.length <= 0) {
        $("#imgUploader_val").text("لطفا تصویر را انتخاب نمائید"); return false;
    }
    if (frm === true) {


        $("#div-loading").removeClass('hidden');
        var projects = new FormData();
        var files = $("#imgUploader").get(0).files;
        //var video = $("#videoUploader").get(0).files;

        var isactive = $('#IsActive:checked').length;
        var isPopular = $('#IsPopular:checked').length;



        // if (files.length > 0) {
        if (isactive == 1) {
            projects.append("IsActive", "true")
        }
        else {
            projects.append("IsActive", "false")
        }

        if (isPopular == 1) {
            projects.append("IsPopular", "true")
        }
        else {
            projects.append("IsPopular", "false")
        }

        projects.append("Title", $("#Title").val()),
            projects.append("Description", CKEDITOR.instances['Description'].getData()),
            projects.append("GroupId", pcGroupId),
            //projects.append("video", video[0]),
            projects.append("imgProject", files[0]),
            projects.append("GalleryId", $("#GalleryId").val()),
            projects.append("Tags", $("#Tags").val()),
            projects.append("Slug", $("#Slug").val()),
            projects.append("MetaTitle", $("#MetaTitle").val()),
            projects.append("MetaKeyword", $("#MetaKeyword").val()),
            projects.append("MetaDescription", $("#MetaDescription").val()),
            projects.append("MetaOther", $("#MetaOther").val())
        // }


        $.ajax({
            method: 'POST',
            url: '/Admin/Projects/CreateProjects',
            data: projects,
            contentType: false,
            processData: false,
            success: function (data) {
                swal({
                    title: "پروژه",
                    text: " محصول " + data.title + " با موفقیت ثبت شد  ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#viewAllProjects").load("/Admin/Projects/ViewAllProjects/" + data.groupId);
                $("#div-loading").addClass('hidden');
                $("#myModal").modal('hide');

            },
            error: function (result) {
                alert(result.error);
            }
        });

    }
    else {
        return false;
    }
}

/////
function openModalForEditProjects(id) {
    //id = $("#language_ddl").val();
    $.get("/Admin/Projects/EditProjects/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش محصول ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
    });

}
//////
function UpdateProjects() {

    var frm = $("#projectsFrmEdit").valid();
    if (frm === true) {

        //if (pcGroupId != undefined) {
        $("#div-loading").removeClass('hidden');
        var projects = new FormData();
        var files = $("#imgUploader").get(0).files;
        //var video = $("#videoUploader").get(0).files;
        var isactive = $('#IsActive:checked').length;
        var isPopular = $('#IsPopular:checked').length;



        if (isactive == 1) {
            projects.append("IsActive", "true")
        }
        else {
            projects.append("IsActive", "false")
        }
        if (isPopular == 1) {
            projects.append("IsPopular", "true")
        }
        else {
            projects.append("IsPopular", "false")
        }

        projects.append("Project_ID", $("#Project_ID").val()),
            projects.append("Title", $("#Title").val()),
            projects.append("Description", CKEDITOR.instances['DescriptionEdit'].getData()),
            projects.append("GroupId", pcGroupId),
            //projects.append("video", video[0]),
            projects.append("imgProject", files[0]),
            projects.append("GalleryId", $("#GalleryId").val()),
            projects.append("Tags", $("#Tags").val()),
            projects.append("Slug", $("#Slug").val()),
            projects.append("Catalog", $("#oldCatalog").val()),
            projects.append("CreateDate", $("#CreateDate").val()),
            projects.append("Image", $("#oldImage").val()),
            projects.append("MetaTitle", $("#MetaTitle").val()),
            projects.append("MetaKeyword", $("#MetaKeyword").val()),
            projects.append("MetaDescription", $("#MetaDescription").val()),
            projects.append("MetaOther", $("#MetaOther").val())


        $.ajax({
            method: 'POST',
            url: '/Admin/Projects/EditProjects',
            data: projects,
            contentType: false,
            processData: false,
            success: function (data) {
                swal({
                    title: "پروژه",
                    text: " پروژه " + data.title + " با موفقیت ویرایش شد  ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#viewAllProjects").load("/Admin/Projects/ViewAllProjects/" + data.groupId);
                $("#div-loading").addClass('hidden');
                $("#myModal").modal('hide');

            },
            error: function (result) {
                alert(result.error);
            }
        });
        //}
        //else {
        //    swal("هشدار ", "لطفا برای ثبت پروژه گروهی را انتخاب نمایید", "warning");
        //}
    }
    else {
        return false;
    }
}
//////
function checkboxSelectedForProjects() {
    swal({
        title: "هشدار",
        text: "آیا برای حذف پروژه ها مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {
                var checkboxValues = "";
                $("div.checked").children('.forRemove').each(function () {
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Projects/checkboxSelectedProjects",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "ok") {
                            for (var i = 0; i < cv.length - 1; i++) {
                                $('#projects_tb').find('tr[id=project_' + cv[i] + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                    var table = $('#projects_tb').DataTable();
                                    table.row("#project_" + cv[i]).remove().draw(false);
                                });
                            }
                            swal({
                                title: "پروژه",
                                text: "پروژه با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                        }
                    });
                }
                else {
                    swal({
                        title: "پروژه",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            }
        });
}

//////
function openModalForShowRateOption(id) {
    $.get("/Admin/Projects/ViewRateOptionDetails/" + id, function (result) {
        $("#rateModal").modal();
        $("#rateModalLabel").html("امتیازات پروژه ");
        $("#rateModalBody").html(result);
    });

}

/////////////////// بخش گروه محصولات

var pgParentId, pGroupId, productGroupId, productGroupIdEdit = "";
function getAllProductsGroupForTreeView() { //خواندن گروه ها برای تری ویو ها
    var lang = $("#language_ddl").val();
    var pgID = 0;
    if ($("#language_ddl").val() == "fa-IR") {
        pgID = 1;
    }
    else if ($("#language_ddl").val() == "en-US") {
        pgID = 2;
    }
    if (pgID != 0) {
        $.ajax({
            method: 'POST',
            url: '/Admin/Products/createTreeView',
            data: { pgId: pgID, lang: lang },
            success: function (response) {

                var $checkableTree = $('#productsGroup1_tw').treeview({
                    data: response,
                    showBorder: false,
                    showIcon: false,
                    showCheckbox: false,

                    onNodeSelected: function (event, node) {

                        //GetContentsGroupById(node.tags);//نمایش جزئیات گروه ها در لیست جزئیات
                        pgParentId = node.tags;
                        GetProductGroupById(node.tags);
                    },
                    onNodeUnselected: function (event, node) {

                        pgParentId = undefined;
                        //GetProductGroupById(node.tags);
                    }
                });

                var $checkableTree2 = $('#productsGroup2_tw').treeview({
                    data: response,
                    showBorder: false,
                    showIcon: false,
                    showCheckbox: false,

                    onNodeSelected: function (event, node) {
                        pGroupId = node.tags;/// برای دادن آی دی به مین پروداکت جهت نمایش اندازه های عکس
                        GetAllProductsByGroupById(node.tags);
                    }
                });

                // جهت نمایش در ویو ثبت جزئیات محصول
                var $checkableTree3 = $('#productsGroup3_tw').treeview({
                    data: response,
                    showBorder: false,
                    showIcon: false,
                    showCheckbox: false,

                    onNodeSelected: function (event, node) {
                        productGroupId = node.tags;
                    }
                });

                var $checkableTree4 = $('#productsGroup4_tw').treeview({
                    data: response,
                    showBorder: false,
                    showIcon: false,
                    showCheckbox: false,

                    onNodeSelected: function (event, node) {
                        GetAllProductsForSimilarByGroupById(node.tags);

                    }
                });
            },
            error: function (response) {

            }
        });
    }
}
//
//
function getAllProductsGroupForTreeView2(lang) { //برای نمایش گروه در ثبت جزئیات محصول
    debugger;
    var pgID = 0;
    if (lang == "fa-IR")
        pgID = 1;
    else if (lang == "en-US")
        pgID = 2

    $.ajax({
        method: 'POST',
        url: '/Admin/Products/createTreeView',
        data: { pgId: pgID, lang: lang },
        success: function (response) {
            debugger;
            // جهت نمایش در ویو ثبت جزئیات محصول
            var $checkableTree3 = $('#productsGroup3_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    productGroupId = node.tags;
                }
            });
            var $checkableTree4 = $('#productsGroup4_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    productGroupIdEdit = node.tags;
                }
            });
            var $checkableTree4 = $('#productsGroup5_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    productGroupIdEdit = node.tags;
                }
            });
        },
        error: function (response) {

        }
    });
}
//
function openModalForAddProductsGroup(id) {
    id = $("#language_ddl").val();
    $.get("/Admin/Products/CreateProductsGroup/" + id, function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گروه محصول جدید");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '60%' });
    });

}
//

function InsertProductGroup() {
    var frm = $("#Form_ProductGroup").valid();
    if (!frm) return false;

    $("#div-loading").removeClass('hidden');
    var productsGroup = new FormData();
    var files = $("#imgUpload").get(0).files;

    var isParent = false;

    if (pgParentId == undefined) {
        pgParentId = null;
        isParent = true;
    }


    if (files.length > 0) {

        productsGroup.append("ProductGroupTitle", $("#ProductGroupTitle").val()),
            productsGroup.append("OffId", $("#OffId").val()),
            productsGroup.append("Description", $("#Description").val()),
            productsGroup.append("PicLargeWidth", $("#PicLargeWidth").val()),
            productsGroup.append("PicSmallWidth", $("#PicSmallWidth").val()),
            productsGroup.append("PicLargeHeight", $("#PicLargeHeight").val()),
            productsGroup.append("PicSmallHeight", $("#PicSmallHeight").val()),
            productsGroup.append("ParentId", pgParentId),
            productsGroup.append("IsParent", isParent),
            productsGroup.append("pgImage", files[0])
        productsGroup.append("Lang", $("#language_ddl").val())
    }


    $.ajax({
        method: 'POST',
        url: '/Admin/Products/CreateProductsGroup',
        data: productsGroup,
        contentType: false,
        processData: false,
        success: function (data) {
            var dataP = data.productsGroup;
            if (!data.res) {
                swal({
                    title: "گروه محصول",
                    text: "ابتدا گروه مورد نظر را انتخاب کنید",
                    type: "warning",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                })
                $("#div-loading").addClass('hidden');

                return false;
            }
            getAllProductsGroupForTreeView();
            swal({
                title: "گروه محصول",
                text: "گروه محصول " + dataP.productGroupTitle + " با موفقیت ثبت شد",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید",
            });
            $("#viewAll").load("/Admin/Products/ViewProductGroup/" + dataP.productGroup_ID);
            $("#div-loading").addClass('hidden');
            $("#myModal").modal('hide');
        },
        error: function (result) {
            alert(result.error);
        }
    });

}
//
function GetProductGroupById(Id) {
    $.ajax({
        method: "GET",
        url: "/Admin/Products/ViewProductGroup",
        data: { id: Id },
        success: function (result) {

            $("#viewAll").html(result);
        },
        error: function (res) {

        }
    });
}
//
function openModalForEditProductsGroup(id) {
    $.get("/Admin/Products/EditProductsGroup/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گروه محصولات ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '60%' });
    });

}
//
//

function UpdateProductGroup() {
    $("#div-loading").removeClass('hidden');
    var productsGroup = new FormData();
    var files = $("#imgUpload").get(0).files;
    //
    //var isParent = false;
    //if (pgParentId == undefined) {
    //    pgParentId = null;
    //    isParent = true;
    //}




    productsGroup.append("ProductGroup_ID", $("#ProductGroup_ID").val()),
        productsGroup.append("ProductGroupTitle", $("#ProductGroupTitle").val()),
        productsGroup.append("OffId", $("#OffId").val()),
        productsGroup.append("Description", $("#Description").val()),
        productsGroup.append("PicLargeWidth", $("#PicLargeWidth").val()),
        productsGroup.append("PicSmallWidth", $("#PicSmallWidth").val()),
        productsGroup.append("PicLargeHeight", $("#PicLargeHeight").val()),
        productsGroup.append("PicSmallHeight", $("#PicSmallHeight").val()),
        productsGroup.append("ParentId", $("#ParentId").val()),
        productsGroup.append("IsParent", $("#IsParent").val()),
        productsGroup.append("ProductGroupImage", $("#ProductGroupImage").val()),

        productsGroup.append("Lang", $("#language_ddl").val())
    if (files.length > 0) {
        productsGroup.append("pgImage", files[0])
    }


    $.ajax({
        method: 'POST',
        url: '/Admin/Products/EditProductsGroup',
        data: productsGroup,
        contentType: false,
        processData: false,
        success: function (data) {
            getAllProductsGroupForTreeView();
            swal({
                title: "گروه محصول",
                text: "گروه محصول " + data.productGroupTitle + " با موفقیت ویرایش شد  ",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید",
            })
            $("#viewAll").load("/Admin/Products/ViewProductGroup/" + data.productGroup_I);
            $("#div-loading").addClass('hidden');
            $("#myModal").modal('hide');

        },
        error: function (result) {
            alert(result.error);
        }
    });

}
//
function DeleteProductGroup(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این گروه محصول مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Products/DeleteProductsGroup',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data === "NOK") {
                            swal({
                                title: "گروه محصول",
                                text: "این گروه شامل گروه های فرزند می باشد ابتدا باید فرزندان حذف شوند",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                            $("#div-loading").addClass('hidden');
                        }
                        else if (data == "isProduct") {
                            swal({
                                title: "گروه محصول",
                                text: "این گروه شامل محصول می باشد ابتدا باید محصولات آن حذف شود",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                            $("#div-loading").addClass('hidden');
                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "گروه محصول",
                                text: "گروه " + data.productGroupTitle + "  با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                            $('#pGroup_tb').find('tr[id=pg_' + data.productGroup_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#pGroup_tb').DataTable();
                                table.row("#pg_" + data.productGroup_ID).remove().draw(false);
                            });

                            getAllProductsGroupForTreeView();
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
//


///////////////////////////// بخش تخفیفات
function openModalForAddOff() {
    $.get("/Admin/Off/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن تخفیف جدید");
        $("#myModalBody").html(result);
    });

}
//
function InsertOff() {
    var frm = $("#offFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');
        var isactive = $('#isActiveOff:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var off = {
            OffTitle: $("#OffTitle").val(),
            StartDate: $("#StartDate").val(),
            EndDate: $("#EndDate").val(),
            Price: $("#Price").val(),
            IsActive: status,
            Lang: $("#language_ddl").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Off/Create',
            data: { off, sdate: $("#tarikhAlt1").val(), eDate: $("#tarikhAlt2").val() },
            success: function (data) {
                if (data.res) {
                    swal({
                        title: "تخفیف",
                        text: "تاریخ پایان باید بعد از تاریخ شروع انتخاب شود",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                    return false;

                }
                $("#viewAll").load("/Admin/Off/ViewAll/" + data.lang);
                $("#myModal").modal("hide");
                swal("تبریک", "تخفیف با عنوان " + data.offTitle + " با موفقیت ثبت شد ", "success");
                swal({
                    title: "تخفیف",
                    text: "تخفیف با عنوان " + data.offTitle + " با موفقیت ثبت شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function openModalForEditOff(id) {
    $.get("/Admin/Off/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش تخفیف ");
        $("#myModalBody").html(result);
    });

}
//
function UpdateOff() {

    var frm = $("#offFrmEdit").valid();
    if (frm === true) {


        $("#div-loading").removeClass('hidden');
        var isactive = $('#isActiveOff:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var off = {
            OffTitle: $("#OffTitle").val(),
            Off_ID: $("#Off_ID").val(),
            StartDate: $("#StartDate").val(),
            EndDate: $("#EndDate").val(),
            Price: $("#Price").val(),
            IsActive: status,
            Lang: $("#language_ddl").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Off/Edit',
            data: { off, sdate: $("#tarikhAlt1").val(), eDate: $("#tarikhAlt2").val() },
            success: function (data) {
                if (data.res) {
                    swal({
                        title: "تخفیف",
                        text: "تاریخ پایان باید بعد از تاریخ شروع انتخاب شود",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                    return false;

                }
                $("#viewAll").load("/Admin/Off/ViewAll/" + data.lang);
                $("#myModal").modal("hide");
                swal({
                    title: "تخفیف",
                    text: "تخفیف با عنوان " + data.offTitle + " با موفقیت ویرایش شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}


//
function DeleteOff(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف تخفیف مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Off/DeleteOff',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        $("#div-loading").addClass('hidden');
                        swal({
                            title: "تخفیف",
                            text: "تخفیف " + data.offTitle + "  با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });

                        $('#off_tb').find('tr[id=off_' + data.off_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#off_tb').DataTable();
                            table.row("#off_" + data.off_ID).remove().draw(false);
                        });

                    },
                    error: function (result) {
                    }
                });
            }
        });
}
//
//////////////////////////// بخش محصولات

function GetAllProductsByGroupById(groupId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Products/ViewAllProducts",
        data: { id: groupId },
        success: function (result) {
            $("#viewAllProducts").html(result);
        },
        error: function (res) {

        }
    });
}
//
function GetAllProductsForSimilarByGroupById(groupId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Products/ViewAllProductsForSimilar",
        data: { id: groupId },
        success: function (result) {

            $("#viewAllProductsForSimilar").html(result);
        },
        error: function (res) {

        }
    });
}
///
function openModalForAddMainProduct() {
    if (pGroupId == undefined) {
        swal({
            title: "محصول",
            text: "برای نمایش اندازه های تصویر محصول ابتدا باید گروه محصول را انتخاب کنید",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید"
        });
    }
    var id = $("#language_ddl").val();
    $.get("/Admin/Products/CreateMainProduct/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن اطلاعات مشترک محصولات ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '85%' });
    });

}

//
function MainProductProductImageSize(id) {
    /// گرفتن ویو کامپوننت بصورت آیجکس

    $.get("/Admin/Products/GetProductImageSizesViewComponent/" + id, function (result) {
        $("#imageSize").html(result);
    });

}
//
function GetAllLanguageForCreateProductView(result) {
    //این تابع جهت سااخت بدنه هر تب در زبان های محصول می باشد که توسط تابع زیرین صدا زده می شود

    $.ajax({
        method: 'POST',
        url: '/Admin/Language/GetAllLangForLayoutPage',
        datatype: "json",
        contentType: "application/json;charset=utf-8",
        data: '',
        success: function (data) {
            var res = "";

            data.forEach(function (obj) {
                res += "<div id='productInfo_" + obj.lang_ID + "' class='tab-pane'>" + result + "</div>"
            });

            $("#contentDiv").append(res);

            res = "<input type='button' value='ثبت' class='btn btn-default' onclick='test()' />";
        },
        error: function (result) {
        }
    });
}


function openModalForAddProductInfo() {

    $.get("/Admin/Products/CreateProduct", function (result) {
        GetAllLanguageForCreateProductView(result);

        //$("#Description").ckeditor();
    });
}

/////
var ProductId = "";
function InsertMainProduct() {
    var frm = $("#mainProductFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');


        var mainProduct = new FormData();
        var isactive = $('#isActiveProduct:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }
        //
        var isExist = $('#isExistProduct:checked').length;
        var exist = "";
        if (isExist === 1) {
            exist = "true";
        }
        else {
            exist = "false";
        }
        //
        //
        var isPopular = $('#IsPopularProduct:checked').length;
        var popular = "";
        if (isPopular === 1) {
            popular = "true";
        }
        else {
            popular = "false";
        }
        //
        var files = $("#ProductImageUploader").get(0).files;
        var catalog = $("#catalog").get(0).files;
        if (files.length > 0) {
            mainProduct.append("ProductCode", $("#ProductCode").val()),
                mainProduct.append("SaledCount", $("#SaledCount").val()),
                mainProduct.append("GalleryId", $("#GalleryId").val()),
                mainProduct.append("imgProduct", files[0]),
                mainProduct.append("pCatalog", catalog[0]),
                mainProduct.append("ExistCount", $("#ExistCount").val()),
                mainProduct.append("IsExsit", exist),
                mainProduct.append("IsActive", status),
                mainProduct.append("ContentGroupId", GroupId),
                mainProduct.append("IsPopular", popular),

                mainProduct.append("smallPicWidth", $("#smallPicWidth").val()),
                mainProduct.append("smallPicHeight", $("#smallPicHeight").val()),
                mainProduct.append("IsDecore", "0")
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Products/CreateMainProduct',
            data: mainProduct,//mainProduct,
            contentType: false,//برای ارسال عکس باید این مولفه با فالس مقداردهی شود
            processData: false, //برای استفاده از فرم دیتا باید این مولفه اضافه شود
            async: false,
            success: function (data) {
                if (data == "noImage") {
                    //swal("هشدار", "اطلاعات وارد شده ناقص می باشد", "warning");
                    $("#ProductImage").addClass("required").text("لطفا برای محصول عکسی انتخاب شود !");
                }
                else if (data == "ok") {
                    $("#div-loading").addClass('hidden');

                    $("#mainProduct").removeClass("active");
                    $("#productInfo").addClass("active");

                    $("#mainProduct_tab").removeClass("active");
                    $("#productInfo_tab").addClass("active");


                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
////
function SendSizeImageForProduct() {
    $.ajax({
        method: "GET",
        url: "/Admin/Products/resizeImage",
        async: false,
        data: { smallPicWidth: $("#smallPicWidth").val(), smallPicHeight: $("#smallPicHeight").val() },
        success: function (result) {

        },
        error: function (res) {

        }
    });
}
///
function SetSlug() {
    //مقدار دادن به اینپوت اسلاگ با گرفتن مقدار نام محصول و جایگزینی فاصله هاش با دَش
    var pTitle = $("#ProductTitle").val();
    pTitle = pTitle.trim().replace(/\s+/g, "-");
    $("#Slug").val(pTitle);
}
///
function GetDropDownDataInProductInfo(lang) {
    /// این تابع برای فراخوانی موارد ذکر شده درصورت تغییر زبان در ویو ثبت محصول
    getAllProductsGroupForTreeView2(lang);
    GetAllDataInDropDownList(lang, "Off", "ViewAllOffList", "#OffId");
    GetAllDataInDropDownList(lang, "Unit", "ViewAllUnitList", "#UnitId");
    GetAllDataInDropDownList(lang, "Producer", "ViewAllProducerList", "#ProducerId");
}
///
function ClearProductForm() {
    $("#ProductTitle").val('');
    $("#Slug").val('');
    $("#Description").val('');
    $("#UnitId").val('0');
    $("#OffId").val('0');
    $("#ProducerId").val('0');
    $("#MetaTitle").val('');
    $("#MetaKeyword").val('');
    $("#MetaDescription").val('');
    $("#MetaOther").val('');
    $("#Tags").val('');
}
///
function InsertProductInfo() {
    var frm = $("#productFrm").valid();
    if (frm === true) {

        if (productGroupId == undefined) {
            swal({
                title: " محصول",
                text: "لطفا یک گروه را انتخاب نمایید",
                type: "warning",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید",
            });
        }
        else {
            $("#div-loading").removeClass('hidden');


            var productInfo = new FormData();

            productInfo.append("ProductGroupId", productGroupId),
                productInfo.append("ProductTitle", $("#ProductTitle").val()),
                productInfo.append("Slug", $("#Slug").val()),
                productInfo.append("ProductSummary", $("#ProductSummary").val()),
                productInfo.append("Description", CKEDITOR.instances['Description'].getData()),
                productInfo.append("UnitId", $("#UnitId").val()),
                productInfo.append("OffId", $("#OffId").val()),
                productInfo.append("ProducerId", $("#ProducerId").val()),
                productInfo.append("MetaTitle", $("#MetaTitle").val()),
                productInfo.append("MetaKeyword", $("#MetaKeyword").val()),
                productInfo.append("MetaDescription", $("#MetaDescription").val()),
                productInfo.append("MetaOther", $("#MetaOther").val()),
                productInfo.append("Tags", $("#Tags").val())

            $.ajax({
                method: 'POST',
                url: '/Admin/Products/CreateProduct',
                data: productInfo,
                processData: false,
                contentType: false,
                success: function (data) {
                    if (data == "NoId") {
                        swal({
                            title: " محصول",
                            text: "برای ثبت محصول ابتدا باید اطلاعات مشترک محصول ثبت گردد!",
                            type: "warning",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید",
                        });
                    }
                    else {
                        $("#myModal").modal("hide");

                        $("#div-loading").addClass('hidden');
                        $("#viewAllProducts").load("/Admin/Products/ViewAllProducts/" + data.productGroupId);
                        swal("ثبت", "محصول " + data.productTitle + " با موفقیت ثبت شد", "success");
                        swal({
                            title: " ثبت محصول",
                            text: "محصول " + data.productTitle + " با موفقیت ثبت شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید",
                        })

                        ClearProductForm();
                    }

                },
                error: function (result) {
                }
            });
        }
    }
    else {
        return false;
    }
}

///
function openModalForEditMainProduct(infoId) {
    var lang = $("#language_ddl").val();
    $.ajax({
        method: "GET",
        url: "/Admin/Products/EditMainProduct",
        data: { id: infoId, lang: lang },
        success: function (result) {

            $("#myModal").modal();
            $("#myModalLabel").html("ویرایش اطلاعات مشترک محصول ");
            $("#myModalBody").html(result);
            $(".modal-dialog").css({ 'width': '85%' });
        },
        error: function (res) {

        }
    });

    //$.get("/Admin/Products/EditMainProduct/" + id + "/" + lang, function (result) {
    //    $("#myModal").modal();
    //    $("#myModalLabel").html("ویرایش اطلاعات مشترک محصول ");
    //    $("#myModalBody").html(result);
    //    $(".modal-dialog").css({ 'width': '85%' });
    //});

}
///
function UpdateMainProduct() {


    $("#div-loading").removeClass('hidden');


    var mainProduct = new FormData();
    var isactive = $('#isActiveProduct:checked').length;
    var status = "";
    if (isactive === 1) {
        status = "true";
    }
    else {
        status = "false";
    }
    //
    var isExist = $('#isExistProduct:checked').length;
    var exist = "";
    if (isExist === 1) {
        exist = "true";
    }
    else {
        exist = "false";
    }
    //
    //
    var isPopular = $('#IsPopularProductEdit:checked').length;
    var popular = "";
    if (isPopular === 1) {
        popular = "true";
    }
    else {
        popular = "false";
    }
    //
    var files = $("#ProductImageUploader").get(0).files;
    var catalog = $("#catalog").get(0).files;

    mainProduct.append("Product_ID", $("#Product_ID").val()),
        mainProduct.append("ProductCode", $("#ProductCode").val()),
        mainProduct.append("SaledCount", $("#SaledCount").val()),
        mainProduct.append("GalleryId", $("#GalleryId").val()),

        mainProduct.append("ExistCount", $("#ExistCount").val()),
        mainProduct.append("IsExsit", exist),
        mainProduct.append("ProductImage", $("#oldImage").val()),
        mainProduct.append("ProductCatalog", $("#oldCatalog").val()),
        mainProduct.append("CreateDate", $("#CreateDate").val()),
        mainProduct.append("IsActive", status),
        mainProduct.append("ContentGroupId", GroupId),
        mainProduct.append("IsPopular", popular),
        mainProduct.append("pCatalog", catalog[0])

    //mainProduct.append("smallPicWidth", $("#smallPicWidth").val()),
    //mainProduct.append("smallPicHeight", $("#smallPicHeight").val()),
    mainProduct.append("IsDecore", "0")
    if (files.length > 0) {
        mainProduct.append("imgProduct", files[0])
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/Products/EditMainProduct',
        data: mainProduct,
        contentType: false,//برای ارسال عکس باید این مولفه با فالس مقداردهی شود
        processData: false, //برای استفاده از فرم دیتا باید این مولفه اضافه شود
        success: function (data) {
            if (data == "no") {
                swal({
                    title: " هشدار",
                    text: "اطلاعات وارد شده ناقص می باشد",
                    type: "warning",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                });
            }
            else {
                $("#div-loading").addClass('hidden');
                //$("#viewAllContents").load("/Admin/Products/ViewAllProducts/" + data.productGroupId);

                $("#mainProduct").removeClass("active");
                $("#productInfo").addClass("active");

                $("#mainProduct_tab").removeClass("active");
                $("#productInfo_tab").addClass("active");
            }
        },
        error: function (result) {
        }
    });

}
///
function changeHiddenInputValue() {
    $.ajax({
        method: "GET",
        url: "/Admin/Products/EditProduct",
        data: { id: productId, lang: lang },
        success: function (result) {
            if (result == "nall") {
                swal({
                    title: " هشدار",
                    text: "برای ویرایش جزئیات محصول دکمه ویرایش محصول را کلیک کنید",
                    type: "warning",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                });
            }
            else {
                getAllProductsGroupForTreeView2(lang);
                $("#productInfoBody").html(result);
            }
        },
        error: function (res) {

        }
    });
}
///
//function openModalForEditProductInfo(productId, lang) {
//    //این تابع آی دی مشترک و زبان رو میگیره و دیکشنری محصولاتی که با این آیدی مشترک هستند رو برمیگرددونه
//    $.ajax({
//        method: "GET",
//        url: "/Admin/Products/EditProduct",
//        data: { id: productId, lang: lang },
//        success: function (result) {

//            if (result == "nall") {
//                swal({
//                    title: " محصول",
//                    text: "برای ویرایش جزئیات محصول دکمه ویرایش محصول را کلیک کنید",
//                    type: "warning",
//                    confirmButtonColor: '#3085d6',
//                    confirmButtonText: "تائید",
//                });
//            }
//            else if (result == "noSet") {
//                var htm = "<br/><br/><br/><br/><div class='alert alert-warning'><p>این محصول برای این زبان تعریف نشده است !</p></div>"
//                $("#productInfoBody").html(htm);
//            }
//            else {
//                getAllProductsGroupForTreeView2(lang);
//                $("#productInfoBody").html(result);
//            }
//        },
//        error: function (res) {

//        }
//    });


//}

function openModalForEditProductInfo(productId, lang) {
    //این تابع آی دی مشترک و زبان رو میگیره و دیکشنری محصولاتی که با این آیدی مشترک هستند رو برمیگرددونه
    $.ajax({
        method: "GET",
        url: "/Admin/Products/EditProduct",
        data: { id: productId, lang: lang },
        success: function (result) {
            debugger;
            if (result == "nall") {
                swal("هشدار", "برای ویرایش جزئیات محصول دکمه ویرایش محصول را کلیک کنید!", "warning");
            }
            else if (result == "noSet") {
                //var htm = "<br/><br/><br/><br/><div class='alert alert-warning'><p>این محصول برای این زبان تعریف نشده است !</p></div>"
                //$("#productInfoBody").html(htm);
                $.get("/Admin/Products/CreateProduct/" + lang, function (result) {
                    $("#productInfoBody").html(result);
                });
                getAllProductsGroupForTreeView2(lang);
            }
            else {
                getAllProductsGroupForTreeView2(lang);
                $("#productInfoBody").html(result);
            }
        },
        error: function (res) {

        }
    });


}
///
function UpdateProductInfo() {
    var frm = $("#editProductForm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');


        var productInfo = new FormData();

        if (productGroupIdEdit != "") {
            productInfo.append("ProductGroupId", productGroupIdEdit)
        }
        else {
            productInfo.append("ProductGroupId", $("#ProductGroupId").val())
        }
        productInfo.append("ProductInfo_ID", $("#ProductInfo_IDEdit").val()),
            productInfo.append("ProductTitle", $("#ProductTitle").val()),
            productInfo.append("ProductId", $("#ProductId").val()),
            productInfo.append("Slug", $("#Slug").val()),
            productInfo.append("ProductSummary", $("#ProductSummary").val()),
            productInfo.append("Description", CKEDITOR.instances['DescriptionEdit'].getData()),
            productInfo.append("UnitId", $("#UnitId").val()),
            productInfo.append("OffId", $("#OffId").val()),
            productInfo.append("ProducerId", $("#ProducerId").val()),
            productInfo.append("MetaTitle", $("#MetaTitle").val()),
            productInfo.append("MetaKeyword", $("#MetaKeyword").val()),
            productInfo.append("MetaDescription", $("#MetaDescription").val()),
            productInfo.append("MetaOther", $("#MetaOther").val()),
            productInfo.append("Tags", $("#Tags").val())

        $.ajax({
            method: 'POST',
            url: '/Admin/Products/EditProduct',
            data: productInfo,
            processData: false,
            contentType: false,
            success: function (data) {

                //else {

                $("#div-loading").addClass('hidden');
                $("#viewAllProducts").load("/Admin/Products/ViewAllProducts/" + data.productGroupId);
                swal({
                    title: " محصول",
                    text: "محصول " + data.productTitle + " با موفقیت ویرایش شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                });

                $("#DescriptionEdit").val('');
                $("#editProductForm")[0].reset();
                //}
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
///

function checkboxSelectedForProduct() {
    swal({
        type: "warning",
        title: "هشدار",
        text: "آیا برای حذف این محصولات انتخاب شده مطمئن هستید؟",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {
                var checkboxValues = "";
                $("div.checked").children('.forRemoveProduct').each(function () {
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Products/checkboxSelected",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "ok") {
                            for (var i = 0; i < cv.length - 1; i++) {
                                $('#productsTb').find('tr[id=product_' + cv[i] + ']').css('background-color', '#B26666').fadeOut(1000, function () {

                                    var table = $('#productsTb').DataTable();
                                    table.row("#product_" + cv[i]).remove().draw(false);
                                });
                            }
                            swal({
                                title: " محصول",
                                text: "محصول با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                    });
                }
                else {
                    swal({
                        title: " محصول",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
            }
        });
}
/////////////////////////////// بخش رنگ محصولات

function GetAllProductColorByProductId(productId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Products/ViewAllProductColor",
        data: { id: productId },
        success: function (result) {
            $(".products_tabs").removeClass("active");
            $("#products").removeClass("active");
            $("#productColor").addClass("active");

            $("#productsTab").removeClass("active");
            $("#productColorTab").addClass("active");


            $("#viewAllProductColor").html(result);
        },
        error: function (res) {

        }
    });
}
///
function GetAllColorByLang() {
    $.ajax({
        method: "GET",
        url: "/Admin/Products/GetAllColorForAdd",
        data: { id: $("#language_ddl").val() },
        success: function (result) {

            var option = "";
            result.forEach(function (obj) {
                option += "<option value=" + obj.value + " style='background-color:" + obj.value + ";color:'white''>" + obj.text + "</option>";
                //$("#ColorId").prop("disabled", false).css('background-color', obj.value);
            });

            $("#ColorId").html(option);

        },
        error: function (res) {

        }
    });
}
//
function openModalForAddProductColor() {
    var id = $("#language_ddl").val();
    $.get("/Admin/Products/CreatePoductColor/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن رنگ محصول ");
        $("#myModalBody").html(result);
    });
    GetAllColorByLang();
}
//
function InsertProductColor() {
    $("#div-loading").removeClass('hidden');
    var producColor = {
        ProductId: $("#pInfoId").text(),
        ProductPrice: $("#ProductPrice").val(),
        ColorId: $("#ColorId").val(),
        ProductCount: $("#ProductCount").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/Products/CreatePoductColor',
        data: producColor,
        success: function (data) {
            if (data == "nall") {
                swal({
                    title: " رنگ محصول",
                    text: "برای ثبت رنگ محصول  ابتدا از بخش محصولات , محصولی را انتخاب نمائید",
                    type: "warning",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                });
                return false;
            }
            $("#viewAllProductColor").load("/Admin/Products/ViewAllProductColor/" + data.productId);
            $("#myModal").modal("hide");
            swal({
                title: " رنگ محصول",
                text: "رنگ محصول با موفقیت ثبت شد",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید",
            });
        },
        error: function (result) {
        }
    });
}

///
function checkboxSelectedForProductColor() {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این رنگ محصول  مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {
                var checkboxValues = "";
                $("div.checked").children('.forRemoveProductColor').each(function () {
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Products/checkboxSelectedProductColor",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "ok") {
                            for (var i = 0; i < cv.length - 1; i++) {
                                $('#product_color').find('tr[id=pcolor_' + cv[i] + ']').css('background-color', '#B26666').fadeOut(1000, function () {

                                    var table = $('#product_color').DataTable();
                                    table.row("#pcolor_" + cv[i]).remove().draw(false);
                                });
                            }
                            swal({
                                title: " رنگ محصول",
                                text: "رنگ محصول با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                    });
                }
                else {
                    swal({
                        title: " رنگ محصول",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
            }
        });
}


/////////////////////////////// بخش ویژگی محصولات
function GetAllProductFeatureByProductId(productId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Products/ViewAllProductFeaturesByProductId",
        data: { id: productId },
        success: function (result) {

            $("#products").removeClass("active");
            $("#productFeature").addClass("active");

            $("#productsTab").removeClass("active");
            $("#productFeatureTab").addClass("active");

            $("#addProductFeature").load("/Admin/Products/CreateProductFeature/" + productId);
            $("#viewAllProductFeatures").html(result);
        },
        error: function (res) {

        }
    });
}
/////
function openModalForAddProductFeature() {
    var id = $("#pInfoId").text();

    $.get("/Admin/Products/CreateProductFeature/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن ویژگی محصول ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '600px' });
    });
}
////
function GetAllProductFeatureReplyByFeatureId(FeatureId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Products/GetAllFeatureReplyByFeatureId",
        data: { id: FeatureId },
        success: function (result) {
            var option = "";
            result.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $("#featureReply_ddl").html(option);
        },
        error: function (res) {

        }
    });
}
////////
function InsertProductFeature(featureId) {
    if ($("#pInfoId").text() != "") {
        $("#loading").removeClass('hidden');
        var productFeature = {
            ProductInfoId: $("#pInfoId").text(),
            FeatureReplyId: $("#fr_" + featureId).val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Products/CreateProductFeature',
            data: productFeature,
            success: function (data) {
                if (data != "repeate") {

                    $("#viewAllProductFeatures").load("/Admin/Products/ViewAllProductFeaturesByProductId/" + data.productInfoId);
                    $("#myModal").modal("hide");
                    swal({
                        title: " ویژگی محصول",
                        text: "ویژگی محصول با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
                else {
                    swal({
                        title: " ویژگی محصول",
                        text: "این ویژگی در لیست ویژگی های محصول وجود دارد",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        swal({
            title: " ویژگی محصول",
            text: "برای ثبت ویژگی محصول ابتدا از بخش محصولات , محصولی را انتخاب نمایید",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید",
        });
    }
}
///////
function checkboxSelectedForProductFeatures() {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این ویژگی محصول  مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {
                var checkboxValues = "";
                var featureReplyId = "";
                $("div.checked").children('.forRemoveFeature').each(function () {
                    checkboxValues += $(this).val() + "&";
                    featureReplyId += $(this).attr('id') + "&";
                });

                var cv = [];
                var fr = [];
                cv = checkboxValues.split("&");
                fr = featureReplyId.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Products/checkboxSelectedProductFeature",
                        type: "Get",
                        data: { values: checkboxValues, featureReply: featureReplyId }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        var el;
                        if (data == "ok") {
                            for (var i = 0; i < cv.length - 1; i++) {

                                el = "pf_" + cv[i] + "_" + fr[i];

                                $('#productFeature_tb').find('tr[id=' + el + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                    var table = $('#productFeature_tb').DataTable();
                                    table.row("#" + el).remove().draw(false);
                                });

                            }
                            swal({
                                title: " ویژگی محصول",
                                text: "ویژگی محصول با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                    });
                }
                else {
                    swal({
                        title: " ویژگی محصول",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
            }
        });
}
/////////////////////////////// بخش محصولات مشابه
function GetAllSimilarProductsByProductId(productId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Products/ViewAllSimilarProducts",
        data: { id: productId },
        success: function (result) {

            $("#products").removeClass("active");
            $("#similarProduct").addClass("active");

            $("#productsTab").removeClass("active");
            $("#similarProductTab").addClass("active");

            $("#viewAllSimilarProduct").load("/Admin/Products/ViewAllSimilarProducts/" + productId);
            //$("#viewAllSimilarProduct").html(result);
        },
        error: function (res) {

        }
    });
}
////
function InsertSimilarProduct(similarId) {
    if ($("#pInfoId").text() != "") {
        $("#div-loading").removeClass('hidden');
        var similarProduct = {
            ProductID: $("#pInfoId").text(),
            SimilarProduct_ID: similarId
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Products/CreateSimilarProduct',
            data: similarProduct,
            success: function (data) {
                if (data == "repeate") {
                    swal({
                        title: " محصول مشابه ",
                        text: "این محصول به لیست محصولات مشابه اضافه شده است",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
                else if (data == "error") {
                    swal({
                        title: " محصول مشابه ",
                        text: "مشابه محصول با خود محصول یکسان است",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
                else {//if (data == "ok")

                    $("#viewAllSimilarProduct").load("/Admin/Products/ViewAllSimilarProducts/" + data.productID);
                    $("#myModal").modal("hide");
                    swal({
                        title: " محصول مشابه ",
                        text: "محصول مشابه با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        swal({
            title: " محصول مشابه ",
            text: "برای ثبت محصول مشابه ابتدا از بخش محصولات , محصولی را انتخاب نمایید",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید",
        });
    }
}
////
function DeleteSimilarProduct(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این محصول مشابه مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Products/DeleteSimilarProduct',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        $("#div-loading").addClass('hidden');
                        swal({
                            title: " محصول مشابه ",
                            text: "محصول مشابه مورد نظر با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید",
                        });

                        $('#similar_tb').find('tr[id=sim_' + Id + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#similar_tb').DataTable();
                            table.row("#sim_" + Id).remove().draw(false);
                        });

                    },
                    error: function (result) {
                    }
                });
            }
        });
}
/////////////////////////////// بخش گارانتی محصولات

function GetAllProductGarantyByProductId(productId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Products/ViewAllProductGaranty",
        data: { id: productId },
        success: function (result) {

            $("#products").removeClass("active");
            $("#productGaranty").addClass("active");

            $("#productsTab").removeClass("active");
            $("#productGarantyTab").addClass("active");

            $("#viewAllProductGaranty").html(result);
        },
        error: function (res) {

        }
    });
}

///
function openModalForAddProductGaranty() {
    var id = $("#language_ddl").val();
    $.get("/Admin/Products/CreateProductGaranty/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گارانتی محصول ");
        $("#myModalBody").html(result);
    });
}

///
function InsertProductGaranty() {
    if ($("#pInfoId").text() != "") {
        $("#div-loading").removeClass('hidden');
        var productGaranty = {
            ProductInfoId: $("#pInfoId").text(),
            GarantyId: $("#GarantyId").val(),
            GarantyPrice: $("#GarantyPrice").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Products/CreateProductGaranty',
            data: productGaranty,
            success: function (data) {

                $("#viewAllProductGaranty").load("/Admin/Products/ViewAllProductGaranty/" + data.productInfoId);
                $("#myModal").modal("hide");
                swal({
                    title: " گارانتی محصول ",
                    text: "گارانتی محصول با موفقیت ثبت شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                });
            },
            error: function (result) {
            }
        });
    }
    else {
        swal({
            title: " گارانتی محصول ",
            text: "برای ثبت گارانتی محصول ابتدا از بخش محصولات , محصولی را انتخاب نمایید",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید",
        });
    }
}
///
function checkboxSelectedForProductGaranty() {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این گارانتی محصول  مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"

    })
        .then((willDelete) => {
            if (willDelete.value) {
                var checkboxValues = "";
                var garantyId = "";
                $("div.checked").children('.forRemoveProductGaranty').each(function () {
                    checkboxValues += $(this).val() + "&";
                    garantyId += $(this).attr('id') + "&";
                });

                var cv = [];
                var fr = [];
                cv = checkboxValues.split("&");
                fr = garantyId.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Products/checkboxSelectedProductGaranty",
                        type: "Get",
                        data: { values: checkboxValues, garantyID: garantyId }
                    }).done(function (data) {

                        if (data.redirect) { ShowAccessDenied(); return false; }

                        var el;
                        if (data == "ok") {
                            for (var i = 0; i < cv.length - 1; i++) {
                                el = "pg_" + cv[i] + "_" + fr[i];
                                $('#productGaranty_tb').find('tr[id=' + el + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                    var table = $('#productGaranty_tb').DataTable();
                                    table.row("#" + el).remove().draw(false);
                                });

                            }
                            swal({
                                title: " گارانتی محصول ",
                                text: "گارانتی محصول با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                    });
                }
                else {
                    swal({
                        title: " گارانتی محصول ",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
            }
        });
}
/////////////////////////////// بخش مزایا  / معایب محصولات
function GetAllProductAdvantageByProductId(productId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Products/ViewAllAdvantage",
        data: { id: productId },
        success: function (result) {

            $("#products").removeClass("active");
            $("#productAdvantage").addClass("active");

            $("#productsTab").removeClass("active");
            $("#productAdvantageTab").addClass("active");

            $("#viewAllProductAdvantage").html(result);
        },
        error: function (res) {

        }
    });
}
////
function openModalForAddProductAdvantage() {
    $.get("/Admin/Products/CreateAdvantage", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن قابلیت برای محصول ");
        $("#myModalBody").html(result);
    });
}
////
function InsertProductAdvantage() {
    var frm = $("#advantageFrm").valid();
    if (frm === true) {

        if ($("#pInfoId").text() != "") {
            $("#div-loading").removeClass('hidden');

            var type = $("#adType_ddl").val();
            if (type == 1) {
                type = true;
            }
            else {
                type = false;
            }
            var productAdvantage = {
                ProductInfoId: $("#pInfoId").text(),
                AdvantageTitle: $("#AdvantageTitle").val(),
                AdvantageType: type
            }

            $.ajax({
                method: 'POST',
                url: '/Admin/Products/CreateAdvantage',
                data: productAdvantage,
                success: function (data) {

                    $("#viewAllProductAdvantage").load("/Admin/Products/ViewAllAdvantage/" + data.productInfoId);
                    $("#myModal").modal("hide");
                    if ($("#adType_ddl").val() == "0") {
                        swal({
                            title: " قابلیت محصول ",
                            text: "قابلیت محصول با موفقیت ثبت شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید",
                        });
                    }
                    else {
                        swal({
                            title: " قابلیت محصول ",
                            text: "قابلیت محصول با موفقیت ثبت شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید",
                        });
                        $("#AdvantageTitle").val('');
                    }
                    $("#div-loading").addClass('hidden');
                },
                error: function (result) {
                }
            });
        }
        else {
            swal({
                title: " قابلیت محصول ",
                text: "برای ثبت قابلیت محصول ابتدا از بخش محصولات , محصولی را انتخاب نمایید",
                type: "warning",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید",
            });
        }
    }
    else {
        return false;
    }
}

///
function openModalForEditProductAdvantage(id) {
    $.get("/Admin/Products/EditAdvantage/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش قابلیت محصول ");
        $("#myModalBody").html(result);
    });
}
///
function UpdateProductAdvantage() {
    var frm = $("#advantageFrmEdit").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');

        var type = $("#adType_ddl").val();
        if (type == 1) {
            type = true;
        }
        else {
            type = false;
        }
        var productAdvantage = {
            Advantage_ID: $("#Advantage_ID").val(),
            ProductInfoId: $("#pInfoId").text(),
            AdvantageTitle: $("#AdvantageTitle").val(),
            AdvantageType: type
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Products/EditAdvantage',
            data: productAdvantage,
            success: function (data) {

                $("#viewAllProductAdvantage").load("/Admin/Products/ViewAllAdvantage/" + data.productInfoId);
                $("#myModal").modal("hide");
                if ($("#adType_ddl").val() == "0") {
                    swal({
                        title: " قابلیت محصول ",
                        text: "قابلیت  محصول با موفقیت ویرایش شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
                else {
                    swal({
                        title: " قابلیت محصول ",
                        text: "قابلیت  محصول با موفقیت ویرایش شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                    $("#AdvantageTitle").val('');
                }
                $("#div-loading").addClass('hidden');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

///
///
function checkboxSelectedForProductAdvantage() {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این قابلیت  مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"

    })
        .then((willDelete) => {
            if (willDelete.value) {
                var checkboxValues = "";
                $("div.checked").children('.forRemove').each(function () {
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Products/checkboxSelectedProductAdvantage",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "ok") {
                            for (var i = 0; i < cv.length - 1; i++) {


                                $('#pAdvantage').find('tr[id=advan_' + cv[i] + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                    var table = $('#pAdvantage').DataTable();
                                    table.row("#advan_" + cv[i]).remove().draw(false);
                                });


                            }
                            swal({
                                title: " قابلیت محصول ",
                                text: "قابلیت محصول با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                    });
                }
                else {
                    swal({
                        title: " قابلیت محصول ",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
            }
        });
}
/////////////////////////////// بخش مزایا  / معایب پروژه ها
function GetAllProjectsAdvantageByProjectsId(projectId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Projects/ViewAllAdvantage",
        data: { id: projectId },
        success: function (result) {

            $("#projects").removeClass("active");
            $("#advantage").addClass("active");

            $("#projectTab").removeClass("active");
            $("#advantageTab").addClass("active");

            $("#viewAllProjectsAdvantage").html(result);
        },
        error: function (res) {

        }
    });
}
////
function openModalForAddProjectsAdvantage() {
    $.get("/Admin/Projects/CreateAdvantage", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن قابلیت برای پروژه ");
        $("#myModalBody").html(result);
    });
}
////
function InsertProjectsAdvantage() {
    var frm = $("#advantageFrm").valid();
    if (frm === true) {

        if ($("#pInfoId").text() != "") {
            $("#div-loading").removeClass('hidden');

            var type = $("#adType_ddl").val();
            if (type == 1) {
                type = true;
            }
            else {
                type = false;
            }
            var projectsAdvantage = {
                ProductInfoId: $("#pInfoId").text(),
                AdvantageTitle: $("#AdvantageTitle").val(),
                AdvantageType: type
            }

            $.ajax({
                method: 'POST',
                url: '/Admin/Projects/CreateAdvantage',
                data: projectsAdvantage,
                success: function (data) {

                    $("#viewAllProjectsAdvantage").load("/Admin/Projects/ViewAllAdvantage/" + data.productInfoId);
                    $("#myModal").modal("hide");
                    if ($("#adType_ddl").val() == "0") {
                        swal({
                            title: " قابلیت پروژه ",
                            text: "قابلیت پروژه با موفقیت ثبت شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید",
                        });
                    }
                    else {
                        swal({
                            title: " قابلیت پروژه ",
                            text: "قابلیت پروژه با موفقیت ثبت شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید",
                        });
                    }
                    $("#div-loading").addClass('hidden');
                },
                error: function (result) {
                }
            });
        }
        else {
            swal({
                title: " قابلیت پروژه ",
                text: "برای ثبت قابلیت پروژه ابتدا از بخش پروژه ها , پروژه ای را انتخاب نمایید",
                type: "warning",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید",
            });
        }
    }
    else {
        return false;
    }
}

///
function openModalForEditProjectsAdvantage(id) {
    $.get("/Admin/Projects/EditAdvantage/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش قابلیت پروژه ");
        $("#myModalBody").html(result);
    });
}
///
function UpdateProjectsAdvantage() {
    var frm = $("#advantageFrmEdit").valid();
    if (frm === true) {


        $("#div-loading").removeClass('hidden');

        var type = $("#adType_ddl").val();
        if (type == 1) {
            type = true;
        }
        else {
            type = false;
        }
        var projectsAdvantage = {
            Advantage_ID: $("#Advantage_ID").val(),
            ProductInfoId: $("#pInfoId").text(),
            AdvantageTitle: $("#AdvantageTitle").val(),
            AdvantageType: type
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Projects/EditAdvantage',
            data: projectsAdvantage,
            success: function (data) {

                $("#viewAllProjectsAdvantage").load("/Admin/Projects/ViewAllAdvantage/" + data.productInfoId);
                $("#myModal").modal("hide");
                if ($("#adType_ddl").val() == "0") {
                    swal({
                        title: " قابلیت پروژه ",
                        text: "قابلیت پروژه با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
                else {
                    swal({
                        title: " قابلیت پروژه ",
                        text: "قابلیت پروژه با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
                $("#div-loading").addClass('hidden');
            },
            error: function (result) {
            }
        });

    }
    else {
        return false;
    }
}

///
///
function checkboxSelectedForProjectAdvantage() {
    swal({
        title: "هشدار",
        text: "آیا برای حذف قابلیت های این محصول مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {
                var checkboxValues = "";
                $("div.checked").children('.forRemoveAdvantage').each(function () {
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Projects/checkboxSelectedProjectsAdvantage",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {

                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "ok") {
                            for (var i = 0; i < cv.length - 1; i++) {
                                $('#pjAdvantage').find('tr[id=advan_' + cv[i] + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                    var table = $('#pjAdvantage').DataTable();
                                    table.row("#advan_" + cv[i]).remove().draw(false);
                                });


                            }
                            swal({
                                title: " قابلیت پروژه ",
                                text: "قابلیت پروژه با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                    });
                }
                else {
                    swal({
                        title: " قابلیت پروژه ",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
            }
        });
}
//////////////////////////////////// مربوط به بخش ویدئو های محصول
function getAllAVFGroupForTreeViewInProject() { //خواندن گروه ها برای تری ویو ها
    var lang = $("#language_ddl").val();

    $.ajax({
        method: 'POST',
        url: '/Admin/Projects/createTreeViewVideo',
        data: { pgId: 1, lang: lang },
        //data: "{'parentId':'0','lang':'" + lang + "','trviewElement':''}",
        success: function (response) {
            var $checkableTree = $('#video_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    GetAllVideoByIdInProject(node.tags);
                }
            });

        },
        error: function (response) {

        }
    });
}

function GetAllVideoByIdInProject(groupId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Projects/GetAllVideoByIdInProject",
        data: { id: groupId },
        success: function (result) {

            var option = "";
            result.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $("#videos_ddl").html(option);
            //$("#SSGId").html(option);
            //
        },
        error: function (res) {

        }
    });
}

function getAllProjectVideoById(projectId) {
    $.ajax({ //
        method: "GET",
        url: "/Admin/Projects/ViewAllVideo",
        data: { id: projectId },
        success: function (result) {
            $("#productId_lbl").text(projectId);
            $("#projects").removeClass("active");
            $("#videos").addClass("active");

            $("#projectTab").removeClass("active");
            $("#videoTab").addClass("active");
            $("#viewAllProjectsVideo").html(result);
            //
        },
        error: function (res) {

        }
    });
}

function InsertProjectVideo() {

    if ($("#productId_lbl").text() != "") {
        $("#div-loading").removeClass('hidden');

        var values = [];
        $('#videos_lst option').each(function () {
            values.push($(this).attr('value'));
        });

        var projectVideo = {
            ProjectId: $("#productId_lbl").text(),
            VideoIds: values
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Projects/CreateProjectVideo',
            data: projectVideo,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                if (data.status == true) {
                    $("#viewAllProjectsVideo").load("/Admin/Projects/ViewAllVideo/" + data.projectId);
                    swal({
                        title: " ویدئو پروژه ",
                        text: "ویدئوهای پروژه با موفقیت ثبت گردید",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                    $("#videos_lst").html('');
                }
                else if (data.status == "repeate") {
                    swal({
                        title: " ویدئو پروژه ",
                        text: " ویدئو قبلا برای این محصول ثبت شده است",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
                else {
                    swal({
                        title: " ویدئو پروژه ",
                        text: "خطایی رخ داده است ",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        swal({
            title: " ویدئو پروژه ",
            text: "برای ثبت ویدئو محصولی انتخاب نشده ",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید",
        });
    }
}
///////
function checkboxSelectedForProjectsVideo() {

    swal({
        title: "هشدار",
        text: "آیا برای حذف ویدئو محصول مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                var checkboxValues = "";
                $("div.checked").children('.forRemove').each(function () {
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Projects/checkboxSelectedProjectsVideo",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        for (var i = 0; i < cv.length - 1; i++) {
                            $("#pjv_" + cv[i]).hide("slow");
                        }
                        swal({
                            title: " ویدئو پروژه ",
                            text: "ویدئوهای این محصول با موفقیت حذف شد ",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید",
                        });
                    });
                }
                else {
                    swal({
                        title: " ویدئو پروژه ",
                        text: "لطفا یک مورد را انتخاب نمایید ",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
            }
        });
}

///////
/////////////////////////////// بخش نظرات محصول
function GetAllCommentsByProductId(productId) {//

    $.ajax({ //
        method: "GET",
        url: "/Admin/Products/ViewAllComments",
        data: { id: productId },
        success: function (result) {

            $("#products").removeClass("active");
            $("#productComment").addClass("active");

            $("#productsTab").removeClass("active");
            $("#productCommentTab").addClass("active");

            $("#viewAllProductComments").html(result);
            //
        },
        error: function (res) {

        }
    });
}
////
function openModalForEditProductComment(id) {
    $.get("/Admin/Products/EditComment/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش وضعیت نظرات ");
        $("#myModalBody").html(result);
    });

}
//
function UpdateProductComment() {
    $("#div-loading").removeClass('hidden');
    var status = $("#IsConfirmedEdit:checked").length;

    if (status == 1) {
        status = "true";
    }
    else {
        status = "false";
    }
    var productComment = {
        Comment_ID: $("#Comment_ID").val(),
        Comment: $("#Comment").val(),
        CommentReply: $("#CommentReply").val(),
        UserName: $("#UserName").val(),
        Email: $("#Email").val(),
        CommentDate: $("#dateComment").val(),
        PositiveScore: $("#PositiveScore").val(),
        NegativeScore: $("#NegativeScore").val(),
        IsConfirmed: status,
        IsNew: false,
        ProductInforId: $("#ProductInforId").val(),
        ProductGroupId: $("#ProductGroupId").val(),
        CommentDate: $("#CommentDate").val(),

    }


    $.ajax({
        method: 'POST',
        url: '/Admin/Products/EditComment',
        data: productComment,
        success: function (data) {


            $("#div-loading").addClass('hidden');

            $("#viewAllProductComments").load("/Admin/Products/ViewAllComments/" + data.productInforId);
            $("#myModal").modal('hide');
            swal({
                title: "نظرات محصول ",
                text: "وضعیت این نظر با موفقیت ویرایش شد ",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید",
            });

        },
        error: function (result) {
        }
    });
}

////
function checkboxSelectedForProductComments() {

    swal({
        title: "هشدار",
        text: "آیا برای حذف نظرات محصولات مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                var checkboxValues = "";
                $("div.checked").children('.forRemove').each(function () {
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Products/checkboxSelectedComments",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        for (var i = 0; i < cv.length - 1; i++) {
                            $("#comment_" + cv[i]).hide("slow");
                        }
                        swal({
                            title: "نظرات محصول ",
                            text: "نظر این محصول با موفقیت حذف شد ",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید",
                        });
                    });
                }
                else {
                    swal({
                        title: "نظرات محصول ",
                        text: "لطفا یک مورد را انتخاب نمایید ",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
            }
        });
}
/////////////////////////////// بخش تولیدکننده
function openModalForAddProducer() {
    $.get("/Admin/Producer/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن تولید کننده محصول ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}
////
function InsertProducer() {
    var frm = $("#producerFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');
        var producer = new FormData();
        var files = $("#producerUploader").get(0).files;


        producer.append("ProducerTitle", $("#ProducerTitle").val()),
            producer.append("Description", $("#Description").val()),
            producer.append("producerImage", files[0])
        producer.append("Lang", $("#language_ddl").val())

        $.ajax({
            method: 'POST',
            url: '/Admin/Producer/Create',
            data: producer,
            contentType: false,
            processData: false,
            success: function (data) {

                $("#viewAll").load("/Admin/Producer/ViewAll/" + data.lang);
                $("#myModal").modal("hide");
                swal({
                    title: "تولید کننده ",
                    text: "تولید کننده " + data.producerTitle + " با موفقیت ثبت شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                });
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
////
function openModalForEditProducer(id) {
    $.get("/Admin/Producer/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش تولید کننده محصول ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}
////
function UpdateProducer() {
    var frm = $("#producerFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');
        var producer = new FormData();
        var files = $("#producerUploader").get(0).files;


        producer.append("Producer_ID", $("#Producer_ID").val()),
            producer.append("ProducerTitle", $("#ProducerTitle").val()),
            producer.append("Description", $("#Description").val()),
            producer.append("Icon", $("#oldImage").val()),
            producer.append("producerImage", files[0])
        producer.append("Lang", $("#language_ddl").val())

        $.ajax({
            method: 'POST',
            url: '/Admin/Producer/Edit',
            data: producer,
            contentType: false,
            processData: false,
            success: function (data) {

                $("#viewAll").load("/Admin/Producer/ViewAll/" + data.lang);
                $("#myModal").modal("hide");
                swal({
                    title: "تولید کننده ",
                    text: "تولید کننده " + data.producerTitle + " با موفقیت ویرایش شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                });
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
///
function DeleteProducer(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف تولیدکننده مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Producer/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "NOK") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "تولید کننده ",
                                text: "این تولیدکننده برای محصولی تعریف شده است ابتدا محصول باید حذف شود",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                        else {

                            console.log(data.producer_ID);
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "تولید کننده ",
                                text: "تولیدکننده " + data.producerTitle + "  با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });

                            $('#producer_tb').find('tr[id=producer_' + data.producer_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#producer_tb').DataTable();
                                table.row("#producer_" + data.producer_ID).remove().draw(false);
                            });

                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
////////////////////////////// بخش گارانتی
function openModalForAddGaranty() {
    $.get("/Admin/Garanty/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گارانتی جدید ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}
/////
function InsertGaranty() {
    var frm = $("#garantyFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');
        var garanty = new FormData();
        var files = $("#garantyUploader").get(0).files;

        var isactive = $('#isActiveGaranty:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        garanty.append("GarantyName", $("#GarantyName").val()),
            garanty.append("Description", $("#Description").val()),
            garanty.append("imgGaranty", files[0]),
            garanty.append("IsActive", status),
            garanty.append("Lang", $("#language_ddl").val())

        $.ajax({
            method: 'POST',
            url: '/Admin/Garanty/Create',
            data: garanty,
            contentType: false,
            processData: false,
            success: function (data) {

                $("#viewAll").load("/Admin/Garanty/ViewAll/" + data.lang);
                $("#myModal").modal("hide");
                swal({
                    title: "گارانتی ",
                    text: "گارانتی " + data.garantyName + " با موفقیت ثبت شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                });
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
///
function openModalForEditGaranty(id) {
    $.get("/Admin/Garanty/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گارانتی  ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}
///
function UpdateGaranty() {
    var frm = $("#garantyFrmEdit").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');
        var garanty = new FormData();
        var files = $("#garantyUploader").get(0).files;

        var isactive = $('#isActiveGaranty:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        garanty.append("Garanty_ID", $("#Garanty_ID").val()),
            garanty.append("GarantyName", $("#GarantyName").val()),
            garanty.append("Description", $("#Description").val()),
            garanty.append("Icon", $("#oldImage").val()),
            garanty.append("imgGaranty", files[0]),
            garanty.append("IsActive", status),
            garanty.append("Lang", $("#language_ddl").val())

        $.ajax({
            method: 'POST',
            url: '/Admin/Garanty/Edit',
            data: garanty,
            contentType: false,
            processData: false,
            success: function (data) {

                $("#viewAll").load("/Admin/Garanty/ViewAll/" + data.lang);
                $("#myModal").modal("hide");
                swal({
                    title: "گارانتی ",
                    text: "گارانتی " + data.garantyName + " با موفقیت ویرایش شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                });
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
///
function DeleteGaranty(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف گارانتی مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Garanty/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "NOK") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "گارانتی ",
                                text: "این گارانتی برای محصولی تعریف شده است ابتدا محصول باید حذف شود ",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "گارانتی ",
                                text: "گارانتی " + data.garantyName + "  با موفقیت حذف شد ",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });

                            $('#garanty_tb').find('tr[id=garanty_' + data.garanty_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#garanty_tb').DataTable();
                                table.row("#garanty_" + data.garanty_ID).remove().draw(false);
                            });

                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
////////////////////////////// بخش واحد
function openModalForAddUnit() {
    $.get("/Admin/Unit/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن واحد جدید ");
        $("#myModalBody").html(result);
    });

}
//
function InsertUnit() {
    var frm = $("#unitFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');

        var unit = {
            UnitTitle: $("#UnitTitle").val(),
            Lang: $("#language_ddl").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Unit/Create',
            data: unit,
            success: function (data) {

                $("#viewAll").load("/Admin/Unit/ViewAll/" + data.lang);
                $("#myModal").modal("hide");
                swal({
                    title: "واحد محصول ",
                    text: "واحد با عنوان " + data.unitTitle + " با موفقیت ثبت شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                });
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function openModalForEditUnit(id) {
    $.get("/Admin/Unit/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش واحد  ");
        $("#myModalBody").html(result);
    });

}
//
function UpdateUnit() {
    var frm = $("#unitFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var unit = {
            Unit_ID: $("#Unit_ID").val(),
            UnitTitle: $("#UnitTitle").val(),
            Lang: $("#language_ddl").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Unit/Edit',
            data: unit,
            success: function (data) {

                $("#viewAll").load("/Admin/Unit/ViewAll/" + data.lang);
                $("#myModal").modal("hide");
                swal({
                    title: "واحد محصول ",
                    text: "واحد با عنوان " + data.unitTitle + " با موفقیت ویرایش شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                });
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function DeleteUnit(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف واحد مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Unit/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "NOK") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "واحد محصول ",
                                text: "این واحد برای محصولی تعریف شده است ابتدا محصول باید حذف شود",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "واحد محصول ",
                                text: "واحد " + data.unitTitle + "  با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });

                            $('#unit_tb').find('tr[id=unit_' + data.unit_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#unit_tb').DataTable();
                                table.row("#unit_" + data.unit_ID).remove().draw(false);
                            });

                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
//////////////////////////// بخش رنگ ها
function openModalForAddColor() {
    $.get("/Admin/Color/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن رنگ جدید ");
        $("#myModalBody").html(result);
    });

}
//
function InsertColor() {
    var frm = $("#colorFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var color = {
            ColorTitle: $("#ColorTitle").val(),
            ColorCode: $("#ColorCode").val(),
            Lang: $("#language_ddl").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Color/Create',
            data: color,
            success: function (data) {
                if (data == "repeate") {
                    $("#div-loading").addClass('hidden');
                    swal({

                        type: 'warning',
                        title: "افزودن رنگ",
                        text: "این کد رنگ قبلا ثبت شده",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });


                }
                else if (data != "nall") {

                    $("#viewAll").load("/Admin/Color/ViewAll/" + data.lang);
                    $("#myModal").modal("hide");

                    swal({

                        type: 'success',
                        title: "افزودن رنگ",
                        text: "رنگ  " + data.colorTitle + " با موفقیت ثبت شد ",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

//
function openModalForEditColor(id) {
    $.get("/Admin/Color/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش رنگ");
        $("#myModalBody").html(result);
    });

}
//
function UpdateColor() {

    var frm = $("#colorFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var color = {
            Color_ID: $("#Color_ID").val(),
            ColorTitle: $("#ColorTitle").val(),
            ColorCode: $("#ColorCode").val(),
            Lang: $("#language_ddl").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Color/Edit',
            data: color,
            success: function (data) {

                $("#viewAll").load("/Admin/Color/ViewAll/" + data.lang);
                $("#myModal").modal("hide");
                swal({
                    type: 'success',
                    title: "افزودن رنگ",
                    text: "رنگ  " + data.colorTitle + " با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                $("#div-loading").addClass('hidden');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function DeleteColor(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این رنگ مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Color/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "NOK") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                type: 'warning',
                                title: "افزودن رنگ",
                                text: "این واحد برای محصولی تعریف شده است ابتدا محصول باید حذف شود",
                                showConfirmButton: true,
                                confirmButtonText: "تائید"
                            });

                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                type: 'success',
                                title: "افزودن رنگ",
                                text: "رنگ " + data.colorTitle + "  با موفقیت حذف شد",
                                showConfirmButton: true,
                                confirmButtonText: "تائید"
                            });
                            $('#color_tb').find('tr[id=color_' + data.color_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#color_tb').DataTable();
                                table.row("#color_" + data.color_ID).remove().draw(false);
                            });
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}

/////////////////////////////////////////// زبان
function InsertLang() {
    var frm = $("#lang_form").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActiveLang:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var lang = {
            Lang_ID: $("#Lang_ID").val(),
            Lang_Name: $("#Lang_Name").val(),
            IsActive: status
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Language/Create',
            data: lang,
            success: function (data) {
                if (data == "repeate") {
                    $("#div-loading").addClass('hidden');
                    swal({

                        type: 'warning',
                        title: "افزودن زبان",
                        text: "این شناسه زبان قبلا ثبت شده",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });


                }
                else if (data != "nall") {

                    $("#viewAll").load("/Admin/Language/ViewAll/" + data.lang);
                    $("#myModal").modal("hide");

                    swal({

                        type: 'success',
                        title: "افزودن زبان",
                        text: "زبان  " + lang.Lang_Name + " با موفقیت ثبت شد ",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
function UpdateLang() {

    var frm = $("#lang_form").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActiveLang:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var lang = {
            Lang_ID: $("#Lang_ID").val(),
            Lang_Name: $("#Lang_Name").val(),
            IsActive: status
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Language/Edit',
            data: lang,
            success: function (data) {
                if (data == "repeate") {
                    swal({

                        type: 'warning',
                        title: "ویرایش زبان",
                        text: "این شناسه زبان قبلا ثبت شده",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                    return false;

                }

                swal({
                    type: 'success',
                    title: "ویرایش زبان",
                    text: "زبان  " + data.lang_ID + " با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });
                $("#viewAll").load("/Admin/Language/ViewAll/" + data.lang_ID);
                $("#myModal").modal("hide");


                $("#div-loading").addClass('hidden');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

/////////////////////////////////////// بخش فایل های دانلودی


var avfParentId, groupIdForAddFile = "", avfGroupId, avfGroupIdEdit = "";
function getAllAVFGroupForTreeView() { //خواندن گروه ها برای تری ویو ها
    var lang = $("#language_ddl").val();
    var pgID = 0;
    if ($("#language_ddl").val() == "fa-IR") {
        pgID = 1;
    }
    else if ($("#language_ddl").val() == "en-US") {
        pgID = 22;
    }
    $.ajax({
        method: 'POST',
        url: '/Admin/FilesManage/createTreeView',
        data: { pgId: pgID, lang: lang },
        //data: "{'parentId':'0','lang':'" + lang + "','trviewElement':''}",
        success: function (response) {
            var $checkableTree = $('#fileManageGroup1_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    GetAVFGroupById(node.tags);//نمایش جزئیات گروه ها در لیست جزئیات
                    avfGroupId = node.tags;
                }
            });

            var $checkableTree2 = $('#fileManageGroup2_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    avfGroupIdEdit = node.tags;
                    groupIdForAddFile = node.tags;
                    GetAllFilesByGroupId(node.tags);
                }
            });

        },
        error: function (response) {

        }
    });
}
function GetAVFGroupById(parentId) {

    $.ajax({
        method: 'POST',
        url: '/Admin/FilesManage/ViewGroupData',
        data: { id: parentId },
        success: function (data) {

            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}

function openModalForAddAVFGroup() {
    $.get("/Admin/FilesManage/CreateGroup", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گروه دانلود جدید ");
        $("#myModalBody").html(result);
    });

}
//
function InsertAVFGroup() {
    var frm = $("#fileGroupFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActiveAVFG:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }


        if (avfGroupId == undefined) {
            avfGroupId = null;
        }

        var file = $('#ImageUpload').get(0).files;
        var aVFGroup = new FormData();

        aVFGroup.append("AVFGroupTitle", $("#AVFGroupTitle").val()),
            aVFGroup.append("IsActive", status),
            aVFGroup.append("ParentId", avfGroupId),
            aVFGroup.append("Lang", $("#language_ddl").val()),
            aVFGroup.append("AVFGroupImage", file[0])

        //aVFGroup.append("AVFGroupImage", file);

        $.ajax({
            method: 'POST',
            url: '/Admin/FilesManage/CreateGroup',
            data: aVFGroup,
            contentType: false,
            processData: false,
            success: function (data) {
                var model = data.model;
                if (!data.res) {
                    $("#div-loading").addClass('hidden');

                    swal({
                        title: "گروه دانلود",
                        text: "ابتدا یک گروه انتخاب نمائید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
                $("#viewAll").load("/Admin/FilesManage/ViewAll/" + model.lang);
                $("#myModal").modal("hide");
                swal({
                    title: "گروه دانلود",
                    text: "گروه دانلود  " + model.avfGroupTitle + " با موفقیت ثبت شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#div-loading").addClass('hidden');
                getAllAVFGroupForTreeView();
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//

function openModalForEditAVFGroup(id) {
    $.get("/Admin/FilesManage/EditGroup/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گروه دانلود ");
        $("#myModalBody").html(result);
    });

}
//
function UpdateAVFGroup() {
    var frm = $("#fileGroupFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActiveAVFG:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }
        var file = $("#ImageUpload").get(0).files;
        var aVFGroup = new FormData();
        if (avfGroupId != undefined) {
            avfGroupId = avfGroupId;

        }
        else {
            avfGroupId = $("#ParentId").val();
        }

        aVFGroup.append("AVFGroup_ID", $("#AVFGroup_ID").val()),
            aVFGroup.append("AVFGroupTitle", $("#AVFGroupTitle").val()),
            aVFGroup.append("IsActive", status),
            aVFGroup.append("ParentId", $('#ParentId').val()),
            aVFGroup.append("Lang", $("#language_ddl").val()),
            aVFGroup.append("AVFGroupImage", $('#oldImage').val()),
            aVFGroup.append("AVFGroupPic", file[0])

        //var aVFGroup = {
        //    AVFGroup_ID: $("#AVFGroup_ID").val(),
        //    ParentId: $("#ParentId").val(),
        //    AVFGroupTitle: $("#AVFGroupTitle").val(),
        //    IsActive: status,
        //    Lang: $("#language_ddl").val()
        //}

        $.ajax({
            method: 'POST',
            url: '/Admin/FilesManage/EditGroup',
            data: aVFGroup,
            contentType: false,
            processData: false,
            success: function (data) {

                $("#viewAll").load("/Admin/FilesManage/ViewAll/" + data);
                $("#myModal").modal("hide");

                $("#div-loading").addClass('hidden');
                getAllAVFGroupForTreeView();
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//

function DeleteAVFGroup(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این گروه مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"


    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/FilesManage/DeleteGroup',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "NOK") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "هشدار",
                                text: "این گروه شامل فایل می باشد ابتدا فایل ها باید حذف شوند",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                        }
                        else {
                            $("#div-loading").addClass('hidden');

                            $('#fileGroup_tb').find('tr[id=avfg_' + data.avfGroup_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#fileGroup_tb').DataTable();
                                table.row("#avfg_" + data.avfGroup_ID).remove().draw(false);
                            });

                            getAllAVFGroupForTreeView();
                            swal({
                                title: "تبریک",
                                text: " گروه " + data.avfGroupTitle + " با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
//
/////////////////////////////////// بخش فایل ها
function GetAllDownloadGroupByLang(lang) {
    $.ajax({ //گرفتن اطلاعات جیسون از سمت سرور و دی سریالایز کردن آن
        method: "GET",
        url: "/Admin/FilesManage/GetAllGroupsByLang",
        data: { id: lang },
        success: function (result) {

            var option = "";
            result.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $("#downloadGroup_ddl").html(option);
            //$("#SSGId").html(option);
            //
        },
        error: function (res) {

        }
    });
}
function GetAllFilesByGroupId(groupId) {

    $.ajax({
        method: 'POST',
        url: '/Admin/FilesManage/ViewAllFiles',
        data: { id: groupId },
        success: function (data) {

            $("#viewAllFiles").html(data);
        },
        error: function (result) {
        }
    });
}

function openModalForAddAVF() {
    //alert(avfGroupIdEdit);
    if (groupIdForAddFile != "") {
        $.get("/Admin/FilesManage/Create", function (result) {
            $("#myModal").modal();
            $("#myModalLabel").html("افزودن فایل جدید ");
            $("#myModalBody").html(result);
            $(".modal-dialog").css({ 'width': "60%" })
        });
    }
    else {
        swal({
            title: "هشدار",
            text: "برای ثبت فایل ابتدا گروه را انتخاب نمایید",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید"
        });
    }
}
function changeFormDesign(value) {
    if (value == "0") {
        $("#link").removeClass('hidden');
        $("#file").addClass('hidden');
        $("#Link_lbl").text("لینک");
    }
    else {
        $("#link").addClass('hidden');
        $("#file").removeClass('hidden');
        $("#FileNameOrLink").val('');
        $("#file_lbl").text("فایل");
    }
}
function InsertFile() {
    var frm = $("#fileFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');





        var aVF = new FormData();
        var files = $("#fileUpload").get(0).files;
        var image = $("#fileImageUpload").get(0).files;

        var isactive = $('#isActiveFile:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }
        ///
        var type = $("#FileOrLink").val();
        if (type == 1) {
            type = true;
        }
        else {
            type = false;
        }
        ///
        if ($("#file").hasClass("hidden")) {
            aVF.append("AVFSize", $(".AVFSize").val())
        }
        else {
            aVF.append("AVFSize", $(".AVFSize2").val())
        }
        aVF.append("AVFTitle", $("#AVFTitle").val()),
            aVF.append("AVFGroupId", groupIdForAddFile),
            aVF.append("FileOrLink", type),
            aVF.append("IsActive", status),
            aVF.append("FileNameOrLink", $("#FileNameOrLink").val()),
            aVF.append("file", files[0]),

            aVF.append("AVFDownloadCount", $("#AVFDownloadCount").val()),
            aVF.append("FileType", $("#FileType").val())
        if (image.length > 0) {
            aVF.append("vidoImg", image[0])
        }

        $.ajax({


            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = (evt.loaded / evt.total) * 100;
                        //Do something with upload progress here
                        $("#statusProgress").text(Math.floor(percentComplete) + "%");
                        $("#prg").removeClass('hidden');
                        $("#prg .progress-bar").css("width", percentComplete + "%").addClass("progress-bar-striped active");
                    }
                }, false);
                return xhr;
            },


            method: 'POST',
            url: '/Admin/FilesManage/Create',
            data: aVF,
            contentType: false,
            processData: false,
            success: function (data) {

                $("#viewAllFiles").load("/Admin/FilesManage/ViewAllFiles/" + data.avfGroupId);
                $("#myModal").modal("hide");
                swal({
                    title: "فایل",
                    text: "فایل " + data.avfTitle + " با موفقیت ثبت شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
            },
            error: function (result) {
            }
        });

    }
    else {
        return false;
    }
}

function openModalForEditAVF(id) {
    $.get("/Admin/FilesManage/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش فایل  ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': "60%" })
    });

}

function UpdateFile() {
    //var frm = $("#fileFrmEdit").valid();
    //if (frm === true) {
    $("#div-loading").removeClass('hidden');

    var aVF = new FormData();
    var files = $("#fileUpload").get(0).files;
    var image = $("#fileImageUpload").get(0).files;

    var isactive = $('#isActiveFile:checked').length;
    var status = "";
    if (isactive === 1) {
        status = "true";
    }
    else {
        status = "false";
    }
    ///
    var type = $("#FileOrLink").val();
    if (type == 1) {
        type = true;
    }
    else {
        type = false;
    }
    ///
    if (type == true) {
        aVF.append("FileNameOrLink", $("#OldFileName").val())
    }
    ///
    if ($("#file").hasClass("hidden")) {
        aVF.append("AVFSize", $(".AVFSize").val())
    }
    else {
        aVF.append("AVFSize", $(".AVFSize2").val())
    }
    aVF.append("AVFTitle", $("#AVFTitle").val()),
        aVF.append("AVF_ID", $("#AVF_ID").val()),
        aVF.append("AVFGroupId", $("#AVFGroupId").val()),
        aVF.append("VideoImage", $("#OldVideoImage").val()),
        aVF.append("FileOrLink", type),
        aVF.append("IsActive", status),
        aVF.append("FileNameOrLink", $("#FileNameOrLink").val()),
        aVF.append("file", files[0]),
        aVF.append("videoImg", image[0]),

        aVF.append("AVFDownloadCount", $("#AVFDownloadCount").val()),
        aVF.append("CreateDate", $("#CreateDate").val()),
        aVF.append("FileType", $("#FileType").val())


    $.ajax({

        xhr: function () {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = (evt.loaded / evt.total) * 100;
                    //Do something with upload progress here
                    $("#statusProgress").text(Math.floor(percentComplete) + "%");
                    $("#prg").removeClass('hidden');
                    $("#prg .progress-bar").css("width", percentComplete + "%").addClass("progress-bar-striped active");
                }
            }, false);
            return xhr;
        },
        method: 'POST',
        url: '/Admin/FilesManage/Edit',
        data: aVF,
        contentType: false,
        processData: false,
        success: function (data) {

            $("#viewAllFiles").load("/Admin/FilesManage/ViewAllFiles/" + data.avfGroupId);
            $("#myModal").modal("hide");
            swal({
                title: "فایل",
                text: "فایل " + data.avfTitle + " با موفقیت ویرایش شد ",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
        },
        error: function (result) {
        }
    });
    //}
    //else {
    //    return false;
    //}
}

function DeleteAVF(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این فایل مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"


    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/FilesManage/DeleteAVF',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "nall") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "خطا",
                                text: "خطای سروری رخ داده است ",
                                type: "error",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "فایل",
                                text: "فایل " + data.avfTitle + "  با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });

                            $('#file_tb').find('tr[id=avf_' + data.avF_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#file_tb').DataTable();
                                table.row("#avf_" + data.avF_ID).remove().draw(false);
                            });


                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
/////////////////////////////////// بخش ویژگی ها
var pgId, pgId2 = "";
function getAllProductsGroupForFeatureTreeView() { //خواندن گروه ها برای تری ویو ها
    var lang = $("#language_ddl").val();

    $.ajax({
        method: 'POST',
        url: '/Admin/Features/createTreeView',
        data: { pgId: 1, lang: lang },
        success: function (response) {

            var $checkableTree = $('#feature1_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    //نمایش لیست ویژگی ها بر اساس گروه
                    pgId = node.tags;
                    GetAllFeatureByProductGroupId(node.tags);
                }
            });

            var $checkableTree2 = $('#feature2_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    pgId2 = node.tags;
                    GetAllFeatureBygroupId(node.tags);
                }
            });
        },
        error: function (response) {

        }
    });
}
//

function GetAllFeatureByProductGroupId(groupId) {
    $.ajax({
        method: 'POST',
        url: '/Admin/Features/ViewAllFeatures',
        data: { id: groupId },
        success: function (data) {
            $("#viewAllFeatures").html(data);
        },
        error: function (result) {
        }
    });
}
///
function openModalForAddFeature() {
    $.get("/Admin/Features/CreateFeature", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن ویژگی جدید ");
        $("#myModalBody").html(result);
    });

}
///
function InsertFeature() {
    var frm = $("#featureFrm").valid();
    if (frm === true) {
        if (pgId != undefined) {
            $("#div-loading").removeClass('hidden');

            var isactive = $('#isActive:checked').length;
            var status = "";
            if (isactive === 1) {
                status = "true";
            }
            else {
                status = "false";
            }

            var feature = {
                FeatureTitle: $("#FeatureTitle").val(),
                IsActive: status,
                ProductGroupId: pgId
            }

            $.ajax({
                method: 'POST',
                url: '/Admin/Features/CreateFeature',
                data: feature,
                success: function (data) {

                    $("#viewAllFeatures").load("/Admin/Features/ViewAllFeatures/" + data.productGroupId);
                    $("#myModal").modal("hide");
                    swal({
                        title: "ویژگی",
                        text: "ویژگی با عنوان   " + data.featureTitle + " با موفقیت ثبت شد ",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                },
                error: function (result) {
                }
            });
        }
        else {
            swal({
                title: "ویژگی",
                text: "لطفا برای ثبت ویژگی ابتدا گروه محصول را انتخاب نمایید",
                type: "warning",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
        }
    }
    else {
        return false;
    }
}
///
function openModalForEditFeature(id) {
    $.get("/Admin/Features/EditFeature/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش ویژگی ها ");
        $("#myModalBody").html(result);
    });

}
///
function UpdateFeature() {
    var frm = $("#featureFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActive:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var feature = {
            Feature_ID: $("#Feature_ID").val(),
            FeatureTitle: $("#FeatureTitle").val(),
            IsActive: status,
            ProductGroupId: $("#ProductGroupId").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Features/EditFeature',
            data: feature,
            success: function (data) {

                $("#viewAllFeatures").load("/Admin/Features/ViewAllFeatures/" + data.productGroupId);
                $("#myModal").modal("hide");
                swal({
                    title: "ویژگی",
                    text: "ویژگی با عنوان   " + data.featureTitle + " با موفقیت ویرایش شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#div-loading").addClass('hidden');
            },
            error: function (result) {
            }
        });
    }
    else {
        return true;
    }
}
///
function DeleteFeature(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف واحد مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Features/DeleteFeature',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "NOK") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "ویژگی",
                                text: "این گروه شامل فایل می باشد ابتدا فایل ها باید حذف شوند",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "ویژگی",
                                text: "ویژگی  " + data.featureTitle + "  با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                            $("#div-loading").addClass('hidden');
                            $('#feature_tb').find('tr[id=feature_' + data.feature_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#feature_tb').DataTable();
                                table.row("#feature_" + data.feature_ID).remove().draw(false);
                            });
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}

////////////////////////////// بخش جواب ویژگی ها
function GetAllFeatureBygroupId(groupId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Features/GetAllFeatureByProductGroupId",
        data: { id: groupId },
        success: function (result) {

            var option = "";
            result.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $("#feature_ddl").html(option);
            //$("#SSGId").html(option);
            //
        },
        error: function (res) {

        }
    });
}
///
function GetAllFeatureReplyByFeatureId(featureId) {

    $.ajax({
        method: 'POST',
        url: '/Admin/Features/ViewAllFeatureReply',
        data: { id: featureId },
        success: function (data) {

            $("#viewAllFeatureReply").html(data);
        },
        error: function (result) {
        }
    });
}
///
function openModalForAddFeatureReply(id) {
    var id = pgId2;
    if (pgId2 == "") {
        swal({
            title: "جواب ویژگی",
            text: "ویژگی برای مشاهده لیست ویژگی ها ابتدا گروه محصول را انتخاب نمایید",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید"
        });
    }
    $.get("/Admin/Features/CreateFeatureReply/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن جواب ویژگی جدید ");
        $("#myModalBody").html(result);
    });

}
///
function InsertFeatureReply() {
    var frm = $("#frFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');



        var featureReply = {
            FeatureId: $("#FeatureId").val(),
            FeatureReplyText: $("#FeatureReplyText").val(),
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Features/CreateFeatureReply',
            data: featureReply,
            success: function (data) {

                $("#viewAllFeatureReply").load("/Admin/Features/ViewAllFeatureReply/" + data.featureId);
                $("#myModal").modal("hide");
                swal({
                    title: "جواب ویژگی",
                    text: "جواب ویژگی با عنوان    " + data.featureReplyText + " با موفقیت ثبت شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#div-loading").addClass('hidden');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
///

function openModalForEditFeatureReply(id) {

    $.get("/Admin/Features/EditFeatureReply/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش جواب ویژگی ");
        $("#myModalBody").html(result);
    });

}
///
function UpdateFeatureReply() {
    var frm = $("#frFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');



        var featureReply = {
            FeatureId: $("#Feature_Id").val(),
            FeatureReply_ID: $("#FeatureReply_ID").val(),
            FeatureReplyText: $("#FeatureReplyText").val(),
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Features/EditFeatureReply',
            data: featureReply,
            success: function (data) {

                $("#viewAllFeatureReply").load("/Admin/Features/ViewAllFeatureReply/" + data.featureId);
                $("#myModal").modal("hide");
                swal({
                    title: "جواب ویژگی",
                    text: "جواب ویژگی با عنوان    " + data.featureReplyText + " با موفقیت ویرایش شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#div-loading").addClass('hidden');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
///
function checkboxSelectedForDeleteFeatureReply() {
    swal({
        title: "هشدار",
        text: "آیا برای حذف جواب ویژگی ها مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {
                var checkboxValues = "";
                $("div.checked").children('.forRemove').each(function () {
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Features/checkboxSelected",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        for (var i = 0; i < cv.length - 1; i++) {

                            $('#featureReply_tb').find('tr[id=fReply_' + cv[i] + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#featureReply_tb').DataTable();
                                table.row("#fReply_" + cv[i]).remove().draw(false);
                            });

                        }
                        swal({
                            title: "جواب ویژگی",
                            text: "جواب ویژگی با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                    });
                }
                else {
                    swal({
                        title: "جواب ویژگی",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            }
        });
}
///
function SetIsDefualtReply(featureReplyId, featureId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Features/SetIsDefault",
        data: { id: featureReplyId, featureId: featureId },
        success: function (result) {

        },
        error: function (res) {

        }
    });
}

/////////////////////////////////////////// گروه راههای ارتباطی
function openModalForAddConnectionGroup() {
    $.get("/Admin/Connection/CreateGroup", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گروه راه های ارتباطی جدید ");
        $("#myModalBody").html(result);
    });

}

/////
function InsertConnectionGroup() {
    var frm = $("#cnGroupFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');
        var status = $("#isActive_cg:checked").length;
        if (status == 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var connectionGroup = {
            ConnectionGroupTitle: $("#ConnectionGroupTitle").val(),
            IsActive: status,
            Lang: $("#language_ddl").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Connection/CreateGroup',
            data: connectionGroup,
            success: function (data) {

                $("#viewAll").load("/Admin/Connection/ViewAll/" + data.lang);
                $("#myModal").modal("hide");
                swal({
                    title: "گروه ارتباطی",
                    text: "گروه راه های ارتباطی    " + data.connectionGroupTitle + " با موفقیت ثبت شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#div-loading").addClass('hidden');
                GetAllConnectionGroups(data.lang, "#connectionsGroup_ddl");
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
/////
function openModalForEditConnectionGroup(id) {
    $.get("/Admin/Connection/editGroup/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گروه راه های ارتباطی");
        $("#myModalBody").html(result);
    });

}
/////
function UpdateConnectionGroup() {
    var frm = $("#cnGroupFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');
        var status = $("#isActive_cg:checked").length;
        if (status == 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var connectionGroup = {
            ConnectionGroup_ID: $("#ConnectionGroup_ID").val(),
            ConnectionGroupTitle: $("#ConnectionGroupTitle").val(),
            IsActive: status,
            Lang: $("#language_ddl").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Connection/editGroup',
            data: connectionGroup,
            success: function (data) {

                $("#viewAll").load("/Admin/Connection/ViewAll/" + data.lang);
                $("#myModal").modal("hide");
                swal({
                    title: "گروه ارتباطی",
                    text: "گروه راه های ارتباطی    " + data.connectionGroupTitle + " با موفقیت ویرایش شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#div-loading").addClass('hidden');
                GetAllConnectionGroups(data.lang, "#connectionsGroup_ddl");
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

/////
function DeleteConnectionGroup(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این گروه مطمئن هستید؟",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ["خیر", "بله"],
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Connection/DeleteGroup',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "nall") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "گروه ارتباطی",
                                text: "خطایی سروری رخ داده است!",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                        }
                        else if (data == "repeate") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "گروه ارتباطی",
                                text: "این گروه شامل راههای ارتباطی می باشد ",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "گروه ارتباطی",
                                text: "گروه راههای ارتباطی  " + data.connectionGroupTitle + "  با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                            $("#cg_" + data.connectionGroup_ID).hide("slow");
                            GetAllConnectionGroups(data.lang, "#connectionsGroup_ddl");
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
/////////////////////////////////بخش راههای ارتباطی
function GetAllConnectionGroups(lang, selector) {
    $.ajax({
        method: 'POST',
        url: '/Admin/Connection/ViewAllCnGroupList',
        data: { id: lang },
        success: function (data) {

            var option = "<option>انتخاب کنید ...</option>";
            data.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $(selector).html(option);
        },
        error: function (result) {
        }
    });
}

///
function GetAllConnectionByGroupId(groupId) {
    $.ajax({
        method: 'POST',
        url: '/Admin/Connection/ViewAllConnection',
        data: { id: groupId },
        success: function (data) {
            console.log(groupId);
            $("#viewAllConnection").html(data);
        },
        error: function (result) {
        }
    });
}
///

///
function openModalForAddConnection() {
    $.get("/Admin/Connection/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن راه های ارتباطی جدید ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': "60%" })
    });

}
///
function InsertConnection() {
    var frm = $("#connectionFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');
        var groupId = $("#connectionsGroup_ddl").val();

        if (groupId != null) {

            var type = $("#type_ddl").val();
            if (type == 1) {
                type = true;
            }
            else {
                type = false;
            }
            var connection = {
                ConnectionGroupId: groupId,
                ConnectionName: $("#ConnectionName").val(),
                ConnectionValue: $("#ConnectionValue").val(),
                CImage: $("#image_ddl").val(),
                CType: type
            }

            $.ajax({
                method: 'POST',
                url: '/Admin/Connection/Create',
                data: connection,
                success: function (data) {

                    $("#viewAllConnection").load("/Admin/Connection/ViewAllConnection/" + data.connectionGroupId);
                    $("#myModal").modal("hide");
                    swal({
                        title: "راه ارتباطی",
                        text: "راه ارتباطی    " + data.connectionName + " با موفقیت ثبت شد ",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');

                },
                error: function (result) {
                }
            });
        }
        else {
            swal({
                title: "راه ارتباطی",
                text: "برای ثبت راه ارتباطی یک گروه را انتخاب نمایید !",
                type: "warning",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
            $("#div-loading").addClass('hidden');

        }
    }
    else {
        return false;
    }
}
///
function openModalForEditConnection(id) {
    $.get("/Admin/Connection/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش راه های ارتباطی ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': "60%" })
    });

}
///
function UpdateConnection() {
    var frm = $("#connectionFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var type = $("#type_ddl").val();
        if (type == 1) {
            type = true;
        }
        else {
            type = false;
        }
        var connection = {
            Connection_ID: $("#Connection_ID").val(),
            ConnectionGroupId: $("#ConnectionGroupId").val(),
            ConnectionName: $("#ConnectionName").val(),
            ConnectionValue: $("#ConnectionValue").val(),
            CImage: $("#image_ddl").val(),
            CType: type
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Connection/Edit',
            data: connection,
            success: function (data) {

                $("#viewAllConnection").load("/Admin/Connection/ViewAllConnection/" + data.connectionGroupId);
                $("#myModal").modal("hide");
                swal({
                    title: "راه ارتباطی",
                    text: "راه ارتباطی    " + data.connectionName + " با موفقیت ویرایش شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#div-loading").addClass('hidden');
            },
            error: function (result) {
            }
        });
    }
    else {
        false;
    }
}
////

function DeleteConnection(Id) {

    swal({
        type: "warning",
        title: "هشدار",
        text: "آیا برای حذف این راه ارتباطی مطمئن هستید؟",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Connection/DeleteConnection',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        //if (data == "NOK") {
                        //    $("#div-loading").addClass('hidden');
                        //    swal("هشدار", "برای این کشور استان ثبت شده است ابتدا باید استان ها حذف شوند", "warning");
                        //}
                        //else {
                        $("#div-loading").addClass('hidden');
                        $('#connetion_tb').find('tr[id=connection_' + data.connection_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#connetion_tb').DataTable();
                            table.row("#connection_" + data.connection_ID).remove().draw(false);
                        });
                        //}
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
////////////////////////////// بخش کشور
function openModalForAddCountry() {
    $.get("/Admin/Location/CreateCountry", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن کشور جدید ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': "50%" })
    });

}

function InsertCountry() {
    var frm = $("#countryFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');
        var country = new FormData();
        var status = $("#isActiveCountry:checked").length;
        if (status == 1) {
            status = true;
        }
        else {
            status = false;
        }

        country.append("CountryName", $("#CountryName").val()),
            country.append("CountryCode", $("#CountryCode").val()),
            country.append("IsActive", status),
            country.append("Lang", $("#language_ddl").val())

        $.ajax({
            method: 'POST',
            url: '/Admin/Location/CreateCountry',
            data: country,
            contentType: false,
            processData: false,
            success: function (data) {

                if (data == "repeate") {
                    swal({
                        title: "کشور",
                        text: "این کشور قبلا ثبت شده است ",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                }
                else {
                    $("#viewAll").load("/Admin/Location/ViewAll/" + data.lang);
                    $("#myModal").modal("hide");
                    swal({
                        title: "کشور",
                        text: "کشور    " + data.countryName + " با موفقیت ثبت شد ",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                    GetAllDataInDropDownList(data.lang, "Location", "GetAllCountry", "#country_ddl");
                }
            },
            error: function (result) {
                alert("sd");
            }
        });
    }
    else {
        return false;
    }
}

function openModalForEditCountry(id) {
    $.get("/Admin/Location/EditCountry/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش کشور  ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': "50%" })
    });

}

function UpdateCountry() {
    var frm = $("#countryFrmEdit").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');
        var country = new FormData();
        var status = $("#isActiveCountry:checked").length;
        if (status == 1) {
            status = true;
        }
        else {
            status = false;
        }

        country.append("Country_ID", $("#Country_ID").val()),
            country.append("CountryName", $("#CountryName").val()),
            country.append("CountryCode", $("#CountryCode").val()),
            country.append("IsActive", status),
            country.append("Lang", $("#language_ddl").val())

        $.ajax({
            method: 'POST',
            url: '/Admin/Location/EditCountry',
            data: country,
            contentType: false,
            processData: false,
            success: function (data) {

                $("#viewAll").load("/Admin/Location/ViewAll/" + data.lang);
                $("#myModal").modal("hide");
                swal({
                    title: "کشور",
                    text: "کشور    " + data.countryName + " با موفقیت ویرایش شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#div-loading").addClass('hidden');
                GetAllDataInDropDownList(data.lang, "Location", "GetAllCountry", "#country_ddl");
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function DeleteCountry(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این کشور مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Location/DeleteCountry',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "NOK") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "کشور",
                                text: "برای این کشور استان ثبت شده است ابتدا باید استان ها حذف شوند",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "کشور",
                                text: "کشور  " + data.countryName + "  با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                            $("#country_" + data.country_ID).hide("slow");
                            GetAllDataInDropDownList(data.lang, "Location", "GetAllCountry", "#country_ddl");
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}

////////////////////////////////////بخش استان
function GetAllStateByCountryId(countryId) {
    $.ajax({
        method: 'POST',
        url: '/Admin/Location/ViewAllState',
        data: { id: countryId },
        success: function (data) {
            $("#viewAllState").html(data);
            GetAllDataInDropDownList($("#country_ddl").val(), "Location", "GetAllState", "#state_ddl");
        },
        error: function (result) {
        }
    });
}

function openModalForAddState() {
    $.get("/Admin/Location/CreateState", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن استان جدید ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': "50%" })
    });

}

function InsertState() {
    var frm = $("#stateFrm").valid();
    if (frm === true) {

        if ($("#country_ddl").val() != "0") {

            $("#div-loading").removeClass('hidden');
            var state = new FormData();
            var status = $("#isActiveState:checked").length;
            if (status == 1) {
                status = true;
            }
            else {
                status = false;
            }

            state.append("StateName", $("#StateName").val()),
                state.append("StateCode", $("#StateCode").val()),
                state.append("IsActive", status),
                state.append("CountryId", $("#country_ddl").val())

            $.ajax({
                method: 'POST',
                url: '/Admin/Location/CreateState',
                data: state,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data == "repeate") {
                        swal({
                            title: "استان",
                            text: "این استان قبلا ثبت شده است",
                            type: "warning",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                        $("#div-loading").addClass('hidden');
                    }
                    else {
                        $("#viewAllState").load("/Admin/Location/ViewAllState/" + data.countryId);
                        $("#myModal").modal("hide");
                        swal({
                            title: "استان",
                            text: "استان    " + data.stateName + " با موفقیت ثبت شد ",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                        $("#div-loading").addClass('hidden');
                        GetAllStateByCountryId(data.countryId);
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            swal({
                title: "استان",
                text: "برای ثبت استان ابتدا کشور را انتخاب نمایید ",
                type: "warning",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
        }
    }
    else {
        return false;
    }
}

function openModalForEditState(satetId) {
    $.ajax({
        method: 'GET',
        url: '/Admin/Location/EditState',
        data: { id: satetId, lang: $("#language_ddl").val() },
        success: function (result) {
            $("#myModal").modal();
            $("#myModalLabel").html("ویرایش استان  ");
            $("#myModalBody").html(result);
            $(".modal-dialog").css({ 'width': "50%" });
        },
        error: function (result) {
        }
    });
}

function DeleteState(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این استان مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Location/DeleteState',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "NOK") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "استان",
                                text: "برای این استان شهر ثبت شده است ابتدا باید شهر ها حذف شوند",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "استان",
                                text: "استان  " + data.stateName + "  با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                            $('#state_tb').find('tr[id=state_' + data.state_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#state_tb').DataTable();
                                table.row("#state_" + data.state_ID).remove().draw(false);
                            });


                            GetAllStateByCountryId(data.countryId);
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}

function UpdateState() {
    var frm = $("#stateFrmEdit").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');
        var state = new FormData();
        var status = $("#isActiveState:checked").length;
        if (status == 1) {
            status = true;
        }
        else {
            status = false;
        }

        state.append("State_ID", $("#State_ID").val()),
            state.append("StateName", $("#StateName").val()),
            state.append("StateCode", $("#StateCode").val()),
            state.append("IsActive", status),
            state.append("CountryId", $("#CountryId").val())

        $.ajax({
            method: 'POST',
            url: '/Admin/Location/EditState',
            data: state,
            contentType: false,
            processData: false,
            success: function (data) {

                $("#viewAllState").load("/Admin/Location/ViewAllState/" + data.countryId);
                $("#myModal").modal("hide");
                swal({
                    title: "استان",
                    text: "استان    " + data.stateName + " با موفقیت ویرایش شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#div-loading").addClass('hidden');
                GetAllStateByCountryId(data.countryId);
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

//////////////////////////////////////////////// بخش شهرستان
function GetAllCityByStateId(stateId) {
    $.ajax({
        method: 'POST',
        url: '/Admin/Location/ViewAllCity',
        data: { id: stateId },
        success: function (data) {
            $("#viewAllCity").html(data);
        },
        error: function (result) {
        }
    });
}

function openModalForAddCity() {
    $.get("/Admin/Location/CreateCity", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن شهر جدید ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': "50%" })
    });

}

function InsertCity() {

    var frm = $("#cityFrm").valid();
    if (frm === true) {
        var state = $("#state_ddl").val();

        if ($("#state_ddl").val() != "0") {

            $("#div-loading").removeClass('hidden');
            var city = new FormData();
            var status = $("#isActiveCity:checked").length;
            if (status == 1) {
                status = true;
            }
            else {
                status = false;
            }

            city.append("CityName", $("#CityName").val()),
                city.append("CityCode", $("#CityCode").val()),
                city.append("IsActive", status),
                city.append("State_ID", $("#state_ddl").val())

            $.ajax({
                method: 'POST',
                url: '/Admin/Location/CreateCity',
                data: city,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data == "repeate") {
                        swal({
                            title: "شهر",
                            text: "این شهر قبلا ثبت شده است ",
                            type: "warning",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                        $("#div-loading").addClass('hidden');
                    }
                    else {
                        $("#myModal").modal("hide");
                        swal({
                            title: "شهر",
                            text: "شهر    " + data.cityName + " با موفقیت ثبت شد ",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                        $("#div-loading").addClass('hidden');
                        GetAllCityByStateId(data.state_ID);
                    }
                },
                error: function (result) {
                }
            });

        }
        else {
            swal({
                title: "شهر",
                text: "برای ثبت شهر ابتدا استان را انتخاب نمایید ",
                type: "warning",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
        }
    }
    else {
        return false;
    }
}

function openModalForEditCity(id) {
    $.get("/Admin/Location/EditCity/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش شهر ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': "50%" })
    });

}

function UpdateCity() {
    var frm = $("#cityFrmEdit").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');
        var city = new FormData();
        var status = $("#isActiveCity:checked").length;
        if (status == 1) {
            status = true;
        }
        else {
            status = false;
        }

        city.append("City_ID", $("#City_ID").val()),
            city.append("CityName", $("#CityName").val()),
            city.append("CityCode", $("#CityCode").val()),
            city.append("IsActive", status),
            city.append("State_ID", $("#State_ID").val())

        $.ajax({
            method: 'POST',
            url: '/Admin/Location/EditCity',
            data: city,
            contentType: false,
            processData: false,
            success: function (data) {

                $("#myModal").modal("hide");
                swal({
                    title: "شهر",
                    text: "شهر    " + data.cityName + " با موفقیت ویرایش شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#div-loading").addClass('hidden');
                GetAllCityByStateId(data.state_ID);
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function DeleteCity(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این شهر مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Location/DeleteCity',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }


                        $("#div-loading").addClass('hidden');
                        swal({
                            title: "شهر",
                            text: "شهر  " + data.cityName + "  با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });

                        $('#city_tb').find('tr[id=city_' + data.cityId + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#city_tb').DataTable();
                            table.row("#city_" + data.cityId).remove().draw(false);
                        });

                        GetAllCityByStateId(data.state_ID);
                    },
                    error: function (result) {
                    }
                });
            }
        });
}

function GetAllCustomers() {

    $.ajax({
        method: 'POST',
        url: '/Admin/Customer/ViewAll',
        data: '',
        success: function (data) {
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}
/////////////////////////////////////////////////// درباره ما
function GetAboutUs(groupId) {
    $.ajax({
        method: "GET",
        url: "/Admin/AboutUs/ViewAllAboutUs",
        data: { id: groupId },
        success: function (result) {

            $("#viewAll").html(result);
            //
        },
        error: function (res) {

        }
    });
}
/////
function openModalForAddAboutUs() {
    $.get("/Admin/AboutUs/Create", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن درباره ما");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
        //$("li[data-title=" + id + "]").css({ 'color': 'white' });
    });

}
//
function InsertAboutUs() {

    if (CKEDITOR.instances['ContentText'].getData() == "") {
        $("#contentText_val").text("متن اصلی را وارد کنید")
        return false;
    }
    $("#div-loading").removeClass('hidden');
    var contents = new FormData();
    var status = "true";

    var files = $("#ContentUploader").get(0).files;

    contents.append("Content_ID", $("#Content_ID").val()),
        contents.append("ContentTitle", $("#ContentTitle").val()),
        contents.append("ContentText", CKEDITOR.instances['ContentText'].getData()),
        contents.append("ContentSummary", $("#ContentSummary").val()),
        contents.append("IsActive", status),
        //contents.append("img", files[0]),
        //contents.append("img", files[0]),
        contents.append("ContentGroupId", $("#ContentGroupId").val())

    $.ajax({
        method: 'POST',
        url: '/Admin/AboutUs/Create',
        data: contents,
        processData: false,
        contentType: false,
        success: function (data) {


            $("#div-loading").addClass('hidden');
            $("#viewAll").load("/Admin/AboutUs/ViewAllAboutUs/" + data.contentGroupId);

            $("#myModal").modal('hide');

            swal({
                title: "صفحات ایستا",
                text: $("#ContentTitle").val() + " با موفقیت ثبت شد",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
        },
        error: function (result) {
        }
    });


}
//
function openModalForEditAboutUs(id) {
    $.get("/Admin/AboutUs/Edit/" + id, function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش صفحات ایستا");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
        //$("li[data-title=" + id + "]").css({ 'color': 'white' });
    });

}
/////
function UpdateAboutUs() {
    $("#div-loading").removeClass('hidden');
    var contents = new FormData();
    var status = "true";

    var files = $("#ContentUploader").get(0).files;

    contents.append("Content_ID", $("#Content_ID").val()),
        contents.append("ContentTitle", $("#ContentTitle").val()),
        contents.append("ContentText", CKEDITOR.instances['ContentTextEdit'].getData()),
        contents.append("ContentSummary", $("#ContentSummary").val()),
        contents.append("ContentImage", $("#oldImage").val()),
        contents.append("IsActive", status),
        //contents.append("img", files[0]),
        contents.append("ContentGroupId", $("#ContentGroupId").val())

    $.ajax({
        method: 'POST',
        url: '/Admin/AboutUs/Edit',
        data: contents,
        processData: false,
        contentType: false,
        success: function (data) {


            $("#div-loading").addClass('hidden');
            $("#viewAll").load("/Admin/AboutUs/ViewAllAboutUs/4");

            $("#myModal").modal('hide');

            swal({
                title: "صفحات ایستا",
                text: $('#ContentTitle').val() + " با موفقیت ویرایش شد",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
        },
        error: function (result) {
        }
    });


}
////////////////////////////////////////// بخش حامیان
function openModalForAddSupporter() {
    $.get("/Admin/CoWorker/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گواهینامه یا افتخار");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '55%' });
    });
}
//////
function InsertSupporter() {
    var frm = $("#cwFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var coWorker = new FormData();
        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }
        ///
        var type = "";
        if ($("#Type").val() == "0") {
            type = "false";
        }
        else {
            type = "true";
        }

        var files = $("#photoUploader").get(0).files;
        if (files.length > 0) {
            coWorker.append("Title", $("#Title").val()),
                coWorker.append("Link", $("#Link").val()),
                coWorker.append("photo", files[0]),
                coWorker.append("Type", type),
                coWorker.append("IsActive", status)
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/CoWorker/Create',
            data: coWorker,
            contentType: false,//برای ارسال عکس باید این مولفه با فالس مقداردهی شود
            processData: false, //برای استفاده از فرم دیتا باید این مولفه اضافه شود
            success: function (data) {

                if (data[0] == "nall") {
                    swal({
                        title: "گواهینامه / افتخار ",
                        text: "خطایی رخ داده است",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                }
                else {
                    $("#div-loading").addClass('hidden');
                    $("#viewAll").load("/Admin/CoWorker/ViewAll");

                    $("#myModal").modal('hide');
                    swal({
                        title: "گواهینامه / افتخار ",
                        text: "گواهینامه / افتخار " + data.title + " با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

//////
function openModalForEditSupporter(id) {
    $.get("/Admin/CoWorker/Edit/" + id, function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گواهینامه یا افتخار");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '55%' });
    });

}
////////
function UpdateSupporter() {



    var coWorker = new FormData();
    var isactive = $('#IsActive:checked').length;
    var status = "";
    if (isactive === 1) {
        status = "true";
    }
    else {
        status = "false";
    }
    ///
    var type = "";
    if ($("#Type").val() == "0") {
        type = "false";
    }
    else {
        type = "true";
    }

    var files = $("#photoUploader").get(0).files;
    ///if (files.length > 0) {
    coWorker.append("CoWorker_ID", $("#CoWorker_ID").val()),
        coWorker.append("Title", $("#Title").val()),
        coWorker.append("Link", $("#Link").val()),
        coWorker.append("Photo", $("#oldPhoto").val()),
        coWorker.append("photo", files[0]),
        coWorker.append("Type", type),
        coWorker.append("IsActive", status)

    // }

    $.ajax({
        method: 'POST',
        url: '/Admin/CoWorker/Edit',
        data: coWorker,
        processData: false,
        contentType: false,
        success: function (data) {

            if (data == "nall") {
                swal({
                    title: "گواهینامه / افتخار ",
                    text: "خطایی رخ داده است",
                    type: "warning",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#div-loading").addClass('hidden');
            }
            else {
                $("#div-loading").addClass('hidden');
                $("#viewAll").load("/Admin/CoWorker/ViewAll");

                $("#myModal").modal('hide');
                swal({
                    title: "گواهینامه / افتخار ",
                    text: "گواهینامه / افتخار " + data.title + " با موفقیت ویرایش شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
            }
        },
        error: function (result) {
        }
    });

}


///////
function DeleteSupporter(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این گواهینامه / افتخار مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/CoWorker/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }


                        $("#div-loading").addClass('hidden');
                        swal({
                            title: "گواهینامه / افتخار ",
                            text: "گواهینامه / افتخار  " + data.title + "  با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });

                        $('#coWorker_tb').find('tr[id=cw_' + data.coWorker_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#coWorker_tb').DataTable();
                            table.row("#cw_" + data.coWorker_ID).remove().draw(false);
                        });

                    },
                    error: function (result) {
                    }
                });
            }
        });
}
///////////////////////////////////////////////////بخش ساخت تجهیزات 
function GetMaterial(groupId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Material/ViewAllAboutUs",
        data: { id: groupId },
        success: function (result) {

            $("#viewAll").html(result);
            //
        },
        error: function (res) {

        }
    });
}
///
function openModalForAddMaterial() {
    $.get("/Admin/Material/Create", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن بخش ساخت تجهیزات");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
        //$("li[data-title=" + id + "]").css({ 'color': 'white' });
    });

}
//
function InsertMaterial() {
    $("#div-loading").removeClass('hidden');
    var contents = new FormData();
    var status = "true";

    var files = $("#ContentUploader").get(0).files;

    contents.append("Content_ID", $("#Content_ID").val()),
        contents.append("ContentTitle", "ساخت تجهیزات "),
        contents.append("ContentText", CKEDITOR.instances['ContentText'].getData()),
        contents.append("ContentSummary", $("#ContentSummary").val()),
        contents.append("IsActive", status),
        contents.append("img", files[0]),
        contents.append("ContentGroupId", $("#ContentGroupId").val())

    $.ajax({
        method: 'POST',
        url: '/Admin/Material/Create',
        data: contents,
        processData: false,
        contentType: false,
        success: function (data) {


            $("#div-loading").addClass('hidden');
            $("#viewAll").load("/Admin/AboutUs/ViewAllAboutUs/3");

            $("#myModal").modal('hide');

            swal("تبریک", "بخش « ساخت تجهیزات » با موفقیت ثبت شد", "success");
        },
        error: function (result) {
        }
    });


}
//
function openModalForEditMaterial() {
    $.get("/Admin/Material/Edit/-2", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش بخش ساخت تجهیزات");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
        //$("li[data-title=" + id + "]").css({ 'color': 'white' });
    });

}
/////
function UpdateMaterial() {
    $("#div-loading").removeClass('hidden');
    var contents = new FormData();
    var status = "true";

    var files = $("#ContentUploader").get(0).files;

    contents.append("Content_ID", $("#Content_ID").val()),
        contents.append("ContentTitle", "ساخت تجهیزات  "),
        contents.append("ContentText", CKEDITOR.instances['ContentText'].getData()),
        contents.append("ContentSummary", $("#ContentSummary").val()),
        contents.append("ContentImage", $("#oldImage").val()),
        contents.append("IsActive", status),
        contents.append("img", files[0]),
        contents.append("ContentGroupId", $("#ContentGroupId").val())

    $.ajax({
        method: 'POST',
        url: '/Admin/Material/Edit',
        data: contents,
        processData: false,
        contentType: false,
        success: function (data) {


            $("#div-loading").addClass('hidden');
            $("#viewAll").load("/Admin/Material/ViewAllAboutUs/3");

            $("#myModal").modal('hide');

            swal("تبریک", "بخش « ساخت تجهیزات » با موفقیت ویرایش شد", "success");
        },
        error: function (result) {
        }
    });


}

///////////////////////////////////////////////////بخش واردات تجهیزات 
function GetImportaion(groupId) {
    $.ajax({
        method: "GET",
        url: "/Admin/ImportaionPart/ViewAllAboutUs",
        data: { id: groupId },
        success: function (result) {
            $("#viewAll").html(result);
            //
        },
        error: function (res) {

        }
    });
}
///
function openModalForAddImportaion() {
    $.get("/Admin/ImportaionPart/Create", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن بخش واردات و تامین تجهیزات");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
        //$("li[data-title=" + id + "]").css({ 'color': 'white' });
    });

}
//
function InsertImportaion() {
    $("#div-loading").removeClass('hidden');
    var contents = new FormData();
    var status = "true";

    var files = $("#ContentUploader").get(0).files;

    contents.append("Content_ID", $("#Content_ID").val()),
        contents.append("ContentTitle", "واردات و تامین تجهیزات"),
        contents.append("ContentText", CKEDITOR.instances['ContentText'].getData()),
        contents.append("ContentSummary", $("#ContentSummary").val()),
        contents.append("IsActive", status),
        contents.append("img", files[0]),
        contents.append("ContentGroupId", $("#ContentGroupId").val())

    $.ajax({
        method: 'POST',
        url: '/Admin/ImportaionPart/Create',
        data: contents,
        processData: false,
        contentType: false,
        success: function (data) {


            $("#div-loading").addClass('hidden');
            $("#viewAll").load("/Admin/ImportaionPart/ViewAllAboutUs/4");

            $("#myModal").modal('hide');

            swal("تبریک", "بخش « واردات و تامین تجهیزات » با موفقیت ثبت شد", "success");
        },
        error: function (result) {
        }
    });


}
//
function openModalForEditImportaionPart() {
    $.get("/Admin/ImportaionPart/Edit/-3", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش بخش واردات و تامین تجهیزات");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
        //$("li[data-title=" + id + "]").css({ 'color': 'white' });
    });

}
/////
function UpdateImportaionPart() {
    $("#div-loading").removeClass('hidden');
    var contents = new FormData();
    var status = "true";

    var files = $("#ContentUploader").get(0).files;

    contents.append("Content_ID", $("#Content_ID").val()),
        contents.append("ContentTitle", "واردات و تامین تجهیزات  "),
        contents.append("ContentText", CKEDITOR.instances['ContentText'].getData()),
        contents.append("ContentSummary", $("#ContentSummary").val()),
        contents.append("ContentImage", $("#oldImage").val()),
        contents.append("IsActive", status),
        contents.append("img", files[0]),
        contents.append("ContentGroupId", $("#ContentGroupId").val())

    $.ajax({
        method: 'POST',
        url: '/Admin/ImportaionPart/Edit',
        data: contents,
        processData: false,
        contentType: false,
        success: function (data) {


            $("#div-loading").addClass('hidden');
            $("#viewAll").load("/Admin/ImportaionPart/ViewAllAboutUs/4");

            $("#myModal").modal('hide');

            swal("تبریک", "بخش « واردات و تامین تجهیزات » با موفقیت ویرایش شد", "success");
        },
        error: function (result) {
        }
    });


}
/////////////////////////////////////////////////////// بخش طراحی و مهندسی
function GetEngineering(groupId) {
    $.ajax({
        method: "GET",
        url: "/Admin/EnginerDesign/ViewAllAboutUs",
        data: { id: groupId },
        success: function (result) {
            $("#viewAll").html(result);
            //
        },
        error: function (res) {

        }
    });
}
///
function openModalForAddEnginnering() {
    $.get("/Admin/EnginerDesign/Create", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن بخش طراحی و مهندسی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
        //$("li[data-title=" + id + "]").css({ 'color': 'white' });
    });

}
//
function InsertEnginnering() {
    $("#div-loading").removeClass('hidden');
    var contents = new FormData();
    var status = "true";

    var files = $("#ContentUploader").get(0).files;

    contents.append("Content_ID", $("#Content_ID").val()),
        contents.append("ContentTitle", "طراحی و مهندسی"),
        contents.append("ContentText", CKEDITOR.instances['ContentText'].getData()),
        contents.append("ContentSummary", $("#ContentSummary").val()),
        contents.append("IsActive", status),
        contents.append("img", files[0]),
        contents.append("ContentGroupId", $("#ContentGroupId").val())

    $.ajax({
        method: 'POST',
        url: '/Admin/EnginerDesign/Create',
        data: contents,
        processData: false,
        contentType: false,
        success: function (data) {


            $("#div-loading").addClass('hidden');
            $("#viewAll").load("/Admin/EnginerDesign/ViewAllAboutUs/5");

            $("#myModal").modal('hide');

            swal("تبریک", "بخش « طراحی و مهندسی » با موفقیت ثبت شد", "success");
        },
        error: function (result) {
        }
    });


}
//
function openModalForEditEnginnering() {
    $.get("/Admin/EnginerDesign/Edit/-4", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش بخش طراحی و مهندسی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
        //$("li[data-title=" + id + "]").css({ 'color': 'white' });
    });

}
/////
function UpdateEnginnering() {
    $("#div-loading").removeClass('hidden');
    var contents = new FormData();
    var status = "true";

    var files = $("#ContentUploader").get(0).files;

    contents.append("Content_ID", $("#Content_ID").val()),
        contents.append("ContentTitle", "طراحی و مهندسی  "),
        contents.append("ContentText", CKEDITOR.instances['ContentText'].getData()),
        contents.append("ContentSummary", $("#ContentSummary").val()),
        contents.append("ContentImage", $("#oldImage").val()),
        contents.append("IsActive", status),
        contents.append("img", files[0]),
        contents.append("ContentGroupId", $("#ContentGroupId").val())

    $.ajax({
        method: 'POST',
        url: '/Admin/EnginerDesign/Edit',
        data: contents,
        processData: false,
        contentType: false,
        success: function (data) {


            $("#div-loading").addClass('hidden');
            $("#viewAll").load("/Admin/EnginerDesign/ViewAllAboutUs/5");

            $("#myModal").modal('hide');

            swal("تبریک", "بخش « طراحی و مهندسی » با موفقیت ویرایش شد", "success");
        },
        error: function (result) {
        }
    });


}
//////////////////////////////////////////// بخش گروه خبرنامه
function openModalForAddNewsLetterGroup() {
    $.get("/Admin/NewsLetters/CreateGroup", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گروه خبرنامه");
        $("#myModalBody").html(result);
    });

}
function InsertNewsLetterGroup() {
    var frm = $("#nlGroupFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActive:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var newsLetterGroup = {
            Title: $("#Title").val(),
            IsActive: status,
            Lang: $("#language_ddl").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/NewsLetters/CreateGroup',
            data: newsLetterGroup,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                $("#viewAll").load("/Admin/NewsLetters/ViewAll/" + data.lang + "");
                $("#myModal").modal('hide');
                swal({
                    title: "گروه خبرنامه",
                    text: "گروه خبرنامه " + data.title + " با موفقیت ثبت شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });


                GetAllNewsLetterGroup($("#language_ddl").val(), "#newsLetterGroup_ddl");
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function openModalForEditNewsLetterGroup(id) {
    $.get("/Admin/NewsLetters/EditGroup/" + id, function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گروه خبرنامه");
        $("#myModalBody").html(result);
    });

}
function UpdateNewsLetterGroup() {
    var frm = $("#nlGroupFrmEdit").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActive:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var newsLetterGroup = {
            Group_ID: $("#Group_ID").val(),
            Title: $("#Title").val(),
            IsActive: status,
            Lang: $("#language_ddl").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/NewsLetters/EditGroup',
            data: newsLetterGroup,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                $("#viewAll").load("/Admin/NewsLetters/ViewAll/" + data.lang + "");
                $("#myModal").modal('hide');
                swal({
                    title: "گروه خبرنامه",
                    text: "گروه خبرنامه " + data.title + " با موفقیت ویرایش شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                GetAllNewsLetterGroup($("#language_ddl").val(), "#newsLetterGroup_ddl");
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function DeleteNewsLettersGroup(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف گروه خبرنامه مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/NewsLetters/DeleteGroup',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        $("#div-loading").addClass('hidden');
                        swal({
                            title: "گروه خبرنامه",
                            text: "گروه  " + data.title + "  با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                        $('#newsLetterGroup_tb').find('tr[id=nlg_' + data.group_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#newsLetterGroup_tb').DataTable();
                            table.row("#nlg_" + data.group_ID).remove().draw(false);
                        });




                        GetAllNewsLetterGroup($("#language_ddl").val(), "#newsLetterGroup_ddl");
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
//////////////////////////////////////////// بخش  خبرنامه
function GetAllNewsLetterGroup(lang, selector) {
    $.ajax({
        method: 'POST',
        url: '/Admin/NewsLetters/ViewAllGroupList',
        data: { id: lang },
        success: function (data) {

            var option = "";
            data.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $(selector).html(option);
        },
        error: function (result) {
        }
    });
}

function GetAllNewsLetterByGroupId(groupId) {
    $.ajax({
        method: 'POST',
        url: '/Admin/NewsLetters/ViewAllMemberOfNewsLetterByGroupId',
        data: { id: groupId },
        success: function (data) {
            var option = "";
            data.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $("#NLMember_lb").html(option);
        },
        error: function (result) {
        }
    });
}

function SendEmail() {
    var lenghList = $('.NLMemberSend_lb > option').length;
    var emailListString = '';  //ریختن ایمیل ها در نوعی از رشته 
    for (var i = 1; i <= lenghList; i++) {
        emailListString += $('.NLMemberSend_lb').children('option:nth-child(' + i + ')').text() + ';';
    }

    //var ckValue = CKEDITOR.instances['cke_txtContext'].getData();
    alert(emailListString);
    $.ajax({
        method: 'POST',
        url: '/Admin/NewsLetters/SendMail',
        data: { mailFrom: 'amirhosseink.7795@gmail.com', mailTo: emailListString, titleEmail: $('#txtTitle').val(), ckValue: CKEDITOR.instances['txtContext'].getData() },
        success: function (add) {
            var responsePart = eval(add.d);
            if (responsePart[0] == "ok") {
                swal({
                    title: "اشتراک ایمیل",
                    text: "ایمیل شما ارسال گردید",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#txtTitle").val('');
                CKEDITOR.instances['txtContext'].setData('');
            }
            else {
                swal({
                    title: "اشتراک ایمیل",
                    text: "خطایی در ارسال ایمیل رخ داده است",
                    type: "warning",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
            }
        },
        error: function (response) {
            //showModalMessage('.modalMessage', 'خطا', '<h3>خطایی رخ داده است</h3>', 'danger');
        }
    });
}
///////////////////////////////////////// بخش اشتراک پیامکی
function GetAllSmsSubscribes() {
    $.ajax({
        method: 'POST',
        url: '/Admin/SMS_Subscribe/ViewAllMemberOfSms',
        data: '',
        success: function (data) {
            var option = "";
            data.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $("#SSMember_lb").html(option);
        },
        error: function (result) {
        }
    });
}

function SendSMS() {
    $("#div-loading").removeClass('hidden');

    var lenghList = $('.SSMemberSend_lb > option').length;
    var mobileListString = '';  //ریختن ایمیل ها در نوعی از رشته 
    for (var i = 1; i <= lenghList; i++) {
        mobileListString += $('.SSMemberSend_lb').children('option:nth-child(' + i + ')').text() + ';';
    }
    $.ajax({
        method: 'POST',
        url: '/Admin/SMS_Subscribe/SendSMS',
        data: { mobiles: mobileListString, userName: "itsamojtaba", password: "9370270898", message: $("#txtContext").val(), from: "+985000125475" },
        success: function () {
            if (data == "ok") {
                swal({
                    title: "اشتراک پیامک",
                    text: "پیامک به شماره های موردنظر ارسال گردید",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#txtContext").val('');
                $("#div-loading").addClass('hidden');
            }
            else {
                swal({
                    title: "اشتراک پیامک",
                    text: "خطایی در ارسال پیامک رخ داده است",
                    type: "warning",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
            }
        },
        error: function (response) {
        }
    });
}
///////////////////////////////////////// بخش حامیان
function openModalForAddSocialNetwork() {
    $.get("/Admin/SocialNetwork/Create", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن شبکه مجازی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '55%' });
    });

}
//////
function InsertSocialNetwork() {
    var frm = $("#socialNetFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');


        var socialNetworks = new FormData();
        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }
        ///

        socialNetworks.append("Title", $("#Title").val()),
            socialNetworks.append("Link", $("#Link").val()),
            socialNetworks.append("Type", $("#Type").val()),
            socialNetworks.append("IsActive", status)


        $.ajax({
            method: 'POST',
            url: '/Admin/SocialNetwork/Create',
            data: socialNetworks,
            contentType: false,//برای ارسال عکس باید این مولفه با فالس مقداردهی شود
            processData: false, //برای استفاده از فرم دیتا باید این مولفه اضافه شود
            success: function (data) {

                if (data == "nall") {
                    swal("هشدار", "خطایی رخ داده است", "warning");
                    $("#div-loading").addClass('hidden');
                }
                else {
                    $("#div-loading").addClass('hidden');
                    $("#viewAll").load("/Admin/SocialNetwork/ViewAll");

                    $("#myModal").modal('hide');

                    swal({
                        title: "شبکه مجازی",
                        text: "شبکه مجازی " + data.title + " با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
function openModalForEditSocialNetwork(id) {
    $.get("/Admin/SocialNetwork/Edit/" + id, function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش شبکه مجازی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '55%' });
    });

}
//////
function UpdateSocialNetwork() {
    var frm = $("#socialNetFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');


        var socialNetworks = new FormData();
        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }
        ///

        socialNetworks.append("ID", $("#ID").val()),
            socialNetworks.append("Title", $("#Title").val()),
            socialNetworks.append("Link", $("#Link").val()),
            socialNetworks.append("Type", $("#Type").val()),
            socialNetworks.append("IsActive", status)


        $.ajax({
            method: 'POST',
            url: '/Admin/SocialNetwork/Edit',
            data: socialNetworks,
            contentType: false,//برای ارسال عکس باید این مولفه با فالس مقداردهی شود
            processData: false, //برای استفاده از فرم دیتا باید این مولفه اضافه شود
            success: function (data) {

                if (data == "nall") {
                    swal("هشدار", "خطایی رخ داده است", "warning");
                    $("#div-loading").addClass('hidden');
                }
                else {
                    $("#div-loading").addClass('hidden');
                    $("#viewAll").load("/Admin/SocialNetwork/ViewAll");

                    $("#myModal").modal('hide');
                    swal({
                        title: "شبکه مجازی",
                        text: "شبکه مجازی " + data.title + " با موفقیت ویرایش شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function DeleteSocialNetwork(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این شبکه اجتماعی مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/SocialNetwork/Delete',
                    data: { id: Id },
                    success: function (data) {

                        if (data.redirect) { ShowAccessDenied(); return false; }

                        $("#div-loading").addClass('hidden');
                        swal({
                            title: "شبکه مجازی",
                            text: "شبکه اجتماعی  " + data.title + "  با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });

                        $('#socialNet_tb').find('tr[id=scNet_' + data.id + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#socialNet_tb').DataTable();
                            table.row("#scNet_" + data.id).remove().draw(false);
                        });

                    },
                    error: function (result) {
                    }
                });
            }
        });
}

/////////////////////////////////////////////////// کاتالوگ
function GetCatalog() {
    $.ajax({
        method: "GET",
        url: "/Admin/Catalog/ViewAllCatalog",
        data: '',
        success: function (result) {

            $("#viewAll").html(result);
            //
        },
        error: function (res) {

        }
    });
}
function openModalForAddCatalog() {
    $.get("/Admin/Catalog/Create", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن کاتالوگ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '50%' });
        //$("li[data-title=" + id + "]").css({ 'color': 'white' });
    });

}
//
function InsertCatalog() {
    var frm = $("#catalogFrm").valid();
    if (!frm) return false;

    $("#div-loading").removeClass('hidden');
    var catalog = new FormData();


    var files = $("#catalogUploader").get(0).files;

    catalog.append("thisFile", files[0]),
        catalog.append("Title", $("#Title").val())

    $.ajax({
        method: 'POST',
        url: '/Admin/Catalog/Create',
        data: catalog,
        processData: false,
        contentType: false,
        success: function (data) {


            $("#div-loading").addClass('hidden');
            $("#viewAll").load("/Admin/catalog/ViewAllCatalog");

            $("#myModal").modal('hide');

            swal({
                title: "کاتالوگ",
                text: "کاتالوگ با موفقیت ثبت شد",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
        },
        error: function (result) {
        }
    });


}

function DeleteCatalog(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این کاتالوگ مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Catalog/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        $("#div-loading").addClass('hidden');
                        swal({
                            title: "کاتالوگ",
                            text: "کاتالوگ با موفقیت حذف شد ",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                        $('#catalog_tb').find('tr[id=cat_' + data.id + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#catalog_tb').DataTable();
                            table.row("#cat_" + data.id).remove().draw(false);
                        });

                    },
                    error: function (result) {
                    }
                });
            }
        });
}
/////////////// /////  /////////////////////// Profile
var onFailed = function () {
    $("#div-loading").addClass('hidden');
    swal({
        title: "کاتالوگ",
        text: "خطایی رخ داده است !!",
        type: "warning",
        confirmButtonColor: '#3085d6',
        confirmButtonText: "تائید"
    });
};

var onSuccess_gp = function () {
    $("#div-loading").addClass('hidden');
    swal({
        title: "کاتالوگ",
        text: "اطلاعات شما با موفقیت به روز شد",
        type: "success",
        confirmButtonColor: '#3085d6',
        confirmButtonText: "تائید"
    });
};

var onBegin_gp = function () {
    $("#div-loading").removeClass('hidden');
};
///////////////////////////////////////////// SeoPage

function openModalForAddSeoPage() {
    $.get("/Admin/SeoPage/Create", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن صفحه سئو");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });

}

function InsertSeoPage() {
    var frm = $("#seoFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');
        //var url=
        var seoPage = {
            UrlPage: decodeURIComponent($("#UrlPage").val()),
            MetaTitle: $("#MetaTitle").val(),
            MetaKeyword: $("#MetaKeyword").val(),
            MetaDescription: $("#MetaDescription").val(),
            MetaOther: $("#MetaOther").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/SeoPage/Create',
            data: seoPage,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                $("#viewAll").load("/Admin/SeoPage/ViewAll/");
                //$("#myModal").modal('hide');
                swal({ title: "تبریک", text: "صفحه برای سئو با موفقیت ثبت شد", icon: "success", buttons: [false, "تایید"], });
                $("#seoFrm")[0].reset();
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function openModalForEditSeoPage(Id) {
    $.get("/Admin/SeoPage/Edit/" + Id, function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش صفحه سئو");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });

}
function UpdateSeoPage() {
    var frm = $("#seoFrmEdit").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');
        //var url=
        var seoPage = {
            SeoPage_ID: $("#SeoPage_ID").val(),
            UrlPage: decodeURIComponent($("#UrlPage").val()),
            MetaTitle: $("#MetaTitle").val(),
            MetaKeyword: $("#MetaKeyword").val(),
            MetaDescription: $("#MetaDescription").val(),
            MetaOther: $("#MetaOther").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/SeoPage/Edit',
            data: seoPage,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                $("#viewAll").load("/Admin/SeoPage/ViewAll/");
                //$("#myModal").modal('hide');
                swal({ title: "تبریک", text: "صفحه برای سئو با موفقیت ویرایش شد", icon: "success", buttons: [false, "تایید"], });
                $("#seoFrmEdit")[0].reset();
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function checkboxSelectedForSeoPage() {
    swal({
        title: "هشدار !!!",
        text: "آیا برای حذف موارد انتخاب شده مطمئن هستید؟",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ["خیر", "بله"],
    })
        .then((willDelete) => {
            if (willDelete.value) {
                var checkboxValues = "";
                $("div.checked").children('.forRemove').each(function () {
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");
                if (checkboxValues != "") {

                    $.ajax({
                        url: "/Admin/SeoPage/checkboxSelected",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (result) {
                        for (var i = 0; i < cv.length - 1; i++) {
                            $("#seo_" + cv[i]).hide("slow");
                        }
                        swal({ title: "تبریک", text: "عملیات حذف با موفقیت انجام شد", icon: "success", buttons: [false, "تایید"], });
                    });
                }
                else {
                    swal({ title: "هشدار", text: "لطفا یک مورد را انتخاب نمایید !", icon: "warning", buttons: [false, "تایید"], });
                }
            }
        });

}
function test() {

    var featureReply = "";
    //var checkboxValues = "";
    //$("div[class=icheckbox_square-blue]").each(function () { // گرفتن ولیو های چکس باکس هایی که تیک خورده باشند
    //    checkboxValues += $(this).html();//.children("input").val()+ "&";
    //});
    $("div.checked").children('.forRemove').each(function () {
        // checkboxValues += $(this).val() + "&";
        featureReply += $(this).attr('id') + "&";
    });
    alert(featureReply);
}


////////////////////////////////////////// بخش اعضای هیئت مدیره و مدیران اجرایی
function openModalForAddManager() {
    $.get("/Admin/HalycoManagers/Create", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن عضو هیئت مدیره و مدیر اجرایی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '55%' });
    });

}
//////
function InsertManager() {
    var frm = $("#HMForm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');


        var HManager = new FormData();
        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }
        ///
        var type = "";
        if ($("#Type").val() == "0") {
            type = "false";
        }
        else {
            type = "true";
        }

        var files = $("#photoUploader").get(0).files;
        if (files.length > 0) {
            HManager.append("ManagerName", $("#ManagerName").val()),
                HManager.append("ManagerPost", $("#ManagerPost").val()),
                HManager.append("EnglishName", $("#EnglishName").val()),
                HManager.append("EnglishPost", $("#EnglishPost").val()),
                HManager.append("ManagerEmail", $("#ManagerEmail").val()),
                HManager.append("ManagerPhoto", files[0]),
                HManager.append("ManagerType", type),
                HManager.append("IsActive", status)
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/HalycoManagers/Create',
            data: HManager,
            contentType: false,//برای ارسال عکس باید این مولفه با فالس مقداردهی شود
            processData: false, //برای استفاده از فرم دیتا باید این مولفه اضافه شود
            success: function (data) {

                if (data[0] == "nall") {
                    swal({
                        title: "عضو هیئت مدیره / مدیر اجرایی ",
                        text: "خطایی رخ داده است",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                }
                else {
                    $("#div-loading").addClass('hidden');
                    $("#viewAll").load("/Admin/HalycoManagers/ViewAll/" + $('#managertype_ddl').val());

                    $("#myModal").modal('hide');
                    swal({
                        title: "عضو هیئت مدیره / مدیر اجرایی ",
                        text: "عضو هیئت مدیره / مدیر اجرایی " + data.managerName + " با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

//////
function openModalForEditManager(id) {
    $.get("/Admin/HalycoManagers/Edit/" + id, function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش عضو هیئت مدیره یا مدیر اجرایی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '55%' });
    });

}
////////
function UpdateManager() {



    var HManager = new FormData();
    var isactive = $('#IsActive:checked').length;
    var status = "";
    if (isactive === 1) {
        status = "true";
    }
    else {
        status = "false";
    }
    ///
    var type = "";
    if ($("#Type").val() == "0") {
        type = "false";
    }
    else {
        type = "true";
    }

    var files = $("#photoUploader").get(0).files;
    ///if (files.length > 0) {

    HManager.append("HManager_ID", $("#HManager_ID").val()),
        HManager.append("ManagerName", $("#ManagerName").val()),
        HManager.append("ManagerPost", $("#ManagerPost").val()),
        HManager.append("EnglishName", $("#EnglishName").val()),
        HManager.append("EnglishPost", $("#EnglishPost").val()),
        HManager.append("ManagerEmail", $("#ManagerEmail").val()),
        HManager.append("ManagerPhoto", $("#oldPhoto").val()),
        HManager.append("photo", files[0]),
        HManager.append("ManagerType", type),
        HManager.append("IsActive", status)

    // }

    $.ajax({
        method: 'POST',
        url: '/Admin/HalycoManagers/Edit',
        data: HManager,
        processData: false,
        contentType: false,
        success: function (data) {

            if (data == "nall") {
                swal({
                    title: "عضو هیئت مدیره / مدیر اجرایی ",
                    text: "خطایی رخ داده است",
                    type: "warning",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#div-loading").addClass('hidden');
            }
            else {
                $("#div-loading").addClass('hidden');
                $("#viewAll").load("/Admin/HalycoManagers/ViewAll/" + $('#managertype_ddl').val());

                $("#myModal").modal('hide');
                swal({
                    title: "عضو هیئت مدیره / مدیر اجرایی ",
                    text: "عضو هیئت مدیره / مدیر اجرایی " + data.managerName + " با موفقیت ویرایش شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
            }
        },
        error: function (result) {
        }
    });

}


///////
function DeleteManager(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این عضو هیئت مدیره / مدیر اجرایی مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/HalycoManagers/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }


                        $("#div-loading").addClass('hidden');
                        swal({
                            title: "عضو هیئت مدیره / مدیر اجرایی ",
                            text: "عضو هیئت مدیره / مدیر اجرایی  " + data.managerName + "  با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });

                        $("#viewAll").load("/Admin/HalycoManagers/ViewAll/" + $('#managertype_ddl').val());
                    },
                    error: function (result) {
                    }
                });
            }
        });
}

function showViewAllManager(managerType) {
    //  alert(managerType);
    $("#viewAll").load("/Admin/HalycoManagers/ViewAll/" + managerType);
}

////////////////////////////////////////////////////////////// Employee

function openModalForAddEmployee() {
    $.get("/Admin/Employee/CreateEmployee", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن کارمند جدید");
        $("#myModalBody").html(result);
        //$(".modal-dialog").css({ 'width': '65%' });
    });

}
function InsertEmployee() {

    var frm = $("#employFrm").valid();
    if (frm === false) {
        return false;
    }
    $("#div-loading").removeClass('hidden');
    var employee = new FormData();
    var files = $("#imgEmployUpload").get(0).files;
    var respons = $("#responsilityUpload").get(0).files;

    var status = "";
    if ($('#IsActive:checked').length == 1) {
        status = "true";
    }
    else {
        status = "false";
    }


    employee.append("FullName", $("#FullName").val()),
        employee.append("SupervisionLevel", $("#SupervisionLevel").val()),
        employee.append("PersonalCode", $("#PersonalCode").val()),
        employee.append("NationalCode", $("#NationalCode").val()),
        employee.append("PersonPassword", $("#PersonPassword").val()),
        employee.append("PersonRePassword", $("#PersonRePassword").val()),
        employee.append("OfficePostId", $("#OfficePostId").val()),
        employee.append("OfficeUnitId", $("#OfficeUnitId").val()),
        employee.append("Tel", $("#Tel").val()),
        employee.append("Mobile", $("#Mobile").val()),
        employee.append("IsActive", status),
        employee.append("imgUser", files[0]),
        employee.append("respons", respons[0]),
        employee.append("HasShift", $('#HasShift').val()),
        employee.append("ShiftName", $('#ShiftName').val()),
        employee.append("HasAlternateEmployee", $('#HasAlternateEmployee').val()),
        employee.append("Job", $('#Job').val())


    $.ajax({
        method: 'POST',
        url: '/Admin/Employee/CreateEmployee',
        data: employee,
        contentType: false,
        processData: false,
        success: function (data) {

            $("#div-loading").addClass('hidden');
            if (data == "ok") {
                swal({
                    title: "ثبت کارمند",
                    text: "کارمند جدید با موفقیت ثبت شد",
                    type: "success"
                    , confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#viewAll").load("/Admin/Employee/ViewAll");
            }
            else if (data == "repeate") {
                swal({
                    title: "ثبت کارمند",
                    text: "این کارمند وجود دارد !",
                    type: "warning"
                    , confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
            }
            else if (data == "pass_short") {
                swal({
                    title: "ثبت کارمند",
                    text: "رمز عبور وارد شده کوتاه !",
                    type: "warning"
                    , confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
            }
            else {
                swal({
                    title: "ثبت کارمند",
                    text: data.message,
                    type: "warning"
                    , confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                console.log(data);
            }

        },
        error: function (result) {
            alert(result.error);
        }
    });
}

function openModalForEditEmployee(Id) {
    $.get("/Admin/Employee/EditEmployee/" + Id, function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش کارمند");
        $("#myModalBody").html(result);
        //$(".modal-dialog").css({ 'width': '65%' });
    });

}
function UpdateEmployee() {

    var frm = $("#employFrmEdit").valid();
    if (frm === false) {
        return false;
    }
    $("#div-loading").removeClass('hidden');
    var employee = new FormData();
    var files = $("#imgEmployUpload").get(0).files;
    var respons = $("#responsilityUpload").get(0).files;


    var status = "";
    if ($('#IsActive:checked').length == 1) {
        status = "true";
    }
    else {
        status = "false";
    }

    employee.append("UserIdentity_Id", $("#UserIdentity_Id").val()),
        employee.append("FullName", $("#FullName").val()),
        employee.append("SupervisionLevel", $("#SupervisionLevel").val()),
        employee.append("PersonalCode", $("#PersonalCode").val()),
        employee.append("NationalCode", $("#NationalCode").val()),
        employee.append("PersonPassword", $("#PersonPassword").val()),
        employee.append("PersonRePassword", $("#PersonRePassword").val()),
        employee.append("OfficePostId", $("#OfficePostId").val()),
        employee.append("OfficeUnitId", $("#OfficeUnitId").val()),
        employee.append("PersonPic", $("#oldPersonPic").val()),
        employee.append("Responsiblity", $("#Responsiblity").val()),
        employee.append("Tel", $("#Tel").val()),
        employee.append("Mobile", $("#Mobile").val()),
        employee.append("IsActive", status),
        employee.append("imgUser", files[0]),
        employee.append("respons", respons[0]),
        employee.append("HasShift", $('#HasShift').val()),
        employee.append("ShiftName", $('#ShiftName').val()),
        employee.append("HasAlternateEmployee", $('#HasAlternateEmployee').val()),
        employee.append("Job", $('#Job').val())


    $.ajax({
        method: 'POST',
        url: '/Admin/Employee/EditEmployee',
        data: employee,
        contentType: false,
        processData: false,
        success: function (data) {

            $("#div-loading").addClass('hidden');
            if (data == "ok") {
                swal({
                    title: "ثبت کارمند",
                    text: "اطلاعات کارمند با موفقیت ویرایش شد",
                    type: "success"
                    , confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#viewAll").load("/Admin/Employee/ViewAll");
                $("#myModal").modal('hide');
            }
            else if (data == "repeate") {
                swal({
                    title: "ثبت کارمند",
                    text: "این کارمند وجود دارد !",
                    type: "warning"
                    , confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                })
            } else {
                console.log(data);
            }

        },
        error: function (result) {
            alert(result.error);
        }
    });
}

function DeleteEmployee(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این کارمند مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonActivityField: '#3085d6',
        cancelButtonActivityField: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Employee/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        //if (data == "NOK") {
                        //    $("#div-loading").addClass('hidden');
                        //    swal({
                        //        type: 'warning',
                        //        title: "حذف زمینه فعالیت",
                        //        text: "این واحد برای محصولی تعریف شده است ابتدا محصول باید حذف شود",
                        //        showConfirmButton: true,
                        //        confirmButtonText: "تائید"
                        //    });

                        //}
                        else if (data == "emp_salary") {
                            swal({
                                type: 'warning',
                                title: "حذف کارمند",
                                text: " این کارمند فیش حقوقی دارد و قابل حذف نمی باشد",
                                showConfirmButton: true,
                                confirmButtonText: "تائید"
                            });
                        }
                        else if (data == "ok") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                type: 'success',
                                title: "حذف کارمند",
                                text: " کارمند با موفقیت حذف شد",
                                showConfirmButton: true,
                                confirmButtonText: "تائید"
                            });

                            $("#viewAll").load("/Admin/Employee/ViewAll");
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}

//////////////////////////// بخش زمینه فعالیت ها
function openModalForAddActivityField() {
    $.get("/Admin/ActivityField/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن زمینه فعالیت جدید ");
        $("#myModalBody").html(result);
    });

}
//
function InsertActivityField() {

    var frm = $("#ActivityFieldFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');
        var isactive = $('#isActiveOff:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }
        var ActivityField = {
            ActivityFieldTitle: $("#ActivityFieldTitle").val(),
            ActivityFieldDescription: $("#ActivityFieldDescription").val(),
            IsActive: status
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/ActivityField/Create',
            data: ActivityField,
            success: function (data) {
                if (data == "repeate") {
                    $("#div-loading").addClass('hidden');
                    swal({

                        type: 'warning',
                        title: "افزودن زمینه فعالیت",
                        text: "این زمینه فعالیت قبلا ثبت شده",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });


                }
                else if (data != "nall") {

                    $("#viewAll").load("/Admin/ActivityField/ViewAll/");
                    $("#myModal").modal("hide");

                    swal({

                        type: 'success',
                        title: "افزودن زمینه فعالیت",
                        text: "زمینه فعالیت  " + data.activityFieldTitle + " با موفقیت ثبت شد ",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

//
function openModalForEditActivityField(id) {
    $.get("/Admin/ActivityField/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش زمینه فعالیت");
        $("#myModalBody").html(result);
    });

}
//
function UpdateActivityField() {

    var frm = $("#ActivityFieldFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');
        var isactive = $('#isActiveOff:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }
        var ActivityField = {
            ActivityFieldID: $("#ActivityFieldID").val(),
            ActivityFieldTitle: $("#ActivityFieldTitle").val(),
            ActivityFieldDescription: $("#ActivityFieldDescription").val(),
            IsActive: status
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/ActivityField/Edit',
            data: ActivityField,
            success: function (data) {

                $("#viewAll").load("/Admin/ActivityField/ViewAll/");
                $("#myModal").modal("hide");
                swal({
                    type: 'success',
                    title: "افزودن زمینه فعالیت",
                    text: "زمینه فعالیت  " + data.activityFieldTitle + " با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                $("#div-loading").addClass('hidden');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function DeleteActivityField(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این زمینه فعالیت مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonActivityField: '#3085d6',
        cancelButtonActivityField: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/ActivityField/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        //if (data == "NOK") {
                        //    $("#div-loading").addClass('hidden');
                        //    swal({
                        //        type: 'warning',
                        //        title: "حذف زمینه فعالیت",
                        //        text: "این واحد برای محصولی تعریف شده است ابتدا محصول باید حذف شود",
                        //        showConfirmButton: true,
                        //        confirmButtonText: "تائید"
                        //    });

                        //}
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                type: 'success',
                                title: "حذف زمینه فعالیت",
                                text: "زمینه فعالیت " + data.activityFieldTitle + "  با موفقیت حذف شد",
                                showConfirmButton: true,
                                confirmButtonText: "تائید"
                            });
                            //$('#ActivityField_tb').find('tr[id=ActivityField_' + data.ActivityField_ID + ']').css('background-ActivityField', '#B26666').fadeOut(1000, function () {
                            //    var table = $('#ActivityField_tb').DataTable();
                            //    table.row("#ActivityField_" + data.ActivityField_ID).remove().draw(false);
                            //});
                            $("#viewAll").load("/Admin/ActivityField/ViewAll/");
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}


//////////////////////////// بخش واحد اداری
function openModalForAddOfficeUnit() {
    $.get("/Admin/OfficeUnit/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن واحد اداری جدید ");
        $("#myModalBody").html(result);
    });

}
//
function InsertOfficeUnit() {

    var frm = $("#OfficeUnitFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var OfficeUnit = {
            OfficeUnitTitle: $("#OfficeUnitTitle").val(),
            OfficeUnitTasks: $("#OfficeUnitTasks").val(),
            OfficeUnitResponsibility: $("#OfficeUnitResponsibility").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/OfficeUnit/Create',
            data: OfficeUnit,
            success: function (data) {
                if (data == "repeate") {
                    $("#div-loading").addClass('hidden');
                    swal({

                        type: 'warning',
                        title: "افزودن واحد اداری",
                        text: "این واحد اداری قبلا ثبت شده",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });


                }
                else if (data != "nall") {

                    $("#viewAll").load("/Admin/OfficeUnit/ViewAll/");
                    $("#myModal").modal("hide");

                    swal({

                        type: 'success',
                        title: "افزودن واحد اداری",
                        text: "واحد اداری  " + data.officeUnitTitle + " با موفقیت ثبت شد ",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

//
function openModalForEditOfficeUnit(id) {
    $.get("/Admin/OfficeUnit/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش سمت");
        $("#myModalBody").html(result);
    });

}
//
function UpdateOfficeUnit() {

    var frm = $("#OfficeUnitFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var OfficeUnit = {
            OfficeUnitID: $("#OfficeUnitID").val(),
            OfficeUnitTitle: $("#OfficeUnitTitle").val(),
            OfficeUnitTasks: $("#OfficeUnitTasks").val(),
            OfficeUnitResponsibility: $("#OfficeUnitResponsibility").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/OfficeUnit/Edit',
            data: OfficeUnit,
            success: function (data) {

                $("#viewAll").load("/Admin/OfficeUnit/ViewAll/");
                $("#myModal").modal("hide");
                swal({
                    type: 'success',
                    title: "ویرایش واحد اداری",
                    text: "واحد اداری  " + data.officeUnitTitle + " با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                $("#div-loading").addClass('hidden');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function DeleteOfficeUnit(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این واحد اداری مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonOfficeUnit: '#3085d6',
        cancelButtonOfficeUnit: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/OfficeUnit/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        //if (data == "NOK") {
                        //    $("#div-loading").addClass('hidden');
                        //    swal({
                        //        type: 'warning',
                        //        title: "حذف سمت",
                        //        text: "این واحد برای محصولی تعریف شده است ابتدا محصول باید حذف شود",
                        //        showConfirmButton: true,
                        //        confirmButtonText: "تائید"
                        //    });

                        //}


                        else {
                            $("#div-loading").addClass('hidden');

                            console.log(data);
                            swal({
                                type: 'success',
                                title: "حذف واحد اداری",
                                text: "واحد اداری " + data.title + "  با موفقیت حذف شد",
                                showConfirmButton: true,
                                confirmButtonText: "تائید"
                            });
                            //$('#OfficeUnit_tb').find('tr[id=OfficeUnit_' + data.OfficeUnit_ID + ']').css('background-OfficeUnit', '#B26666').fadeOut(1000, function () {
                            //    var table = $('#OfficeUnit_tb').DataTable();
                            //    table.row("#OfficeUnit_" + data.OfficeUnit_ID).remove().draw(false);
                            //});
                            $("#viewAll").load("/Admin/OfficeUnit/ViewAll/");
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}


//////////////////////////// بخش پست اداری
var officePostParentId = 0;
function getAllOfficePostForTreeView() { //خواندن گروه ها برای تری ویو ها
    var lang = $("#language_ddl").val();

    $.ajax({
        method: 'POST',
        url: '/Admin/OfficePost/createTreeView',
        data: { pgId: 1, lang: lang },
        //data: "{'parentId':'0','lang':'" + lang + "','trviewElement':''}",

        success: function (response) {
            debugger;
            var $checkableTree = $('#officePost1_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    OfficePostViewDetails(node.tags);//نمایش جزئیات گروه ها در لیست جزئیات
                    officePostParentId = node.tags;
                }
            });



        },
        error: function (response) {

        }
    });
}
function openModalForAddOfficePost() {
    $.get("/Admin/OfficePost/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن پست اداری جدید ");
        $("#myModalBody").html(result);
    });

}
//
function InsertOfficePost() {
    var frm = $("#OfficePostFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var OfficePost = {
            OfficePostTitle: $("#OfficePostTitle").val(),
            OfficePostRoles: $("#OfficePostRoles").val(),
            ParentId: officePostParentId,
            OfficePostResponsibility: $("#OfficePostResponsibility").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/OfficePost/Create',
            data: OfficePost,
            success: function (data) {
                if (data == "repeate") {
                    $("#div-loading").addClass('hidden');
                    swal({

                        type: 'warning',
                        title: "افزودن پست اداری",
                        text: "این پست اداری قبلا ثبت شده",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });


                }
                else if (data != "nall") {
                    getAllOfficePostForTreeView();
                    $("#viewAll").load("/Admin/OfficePost/ViewOfficePostData/" + data.parentId);
                    $("#myModal").modal("hide");

                    swal({

                        type: 'success',
                        title: "افزودن پست اداری",
                        text: "پست اداری  " + data.title + " با موفقیت ثبت شد ",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                }
            },
            error: function (result) {
            }
        });

    }
    else {
        return false;
    }
}

//
function openModalForEditOfficePost(id) {
    $.get("/Admin/OfficePost/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش سمت");
        $("#myModalBody").html(result);
    });

}
//
function UpdateOfficePost() {
    var frm = $("#OfficePostFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var OfficePost = {
            OfficePostID: $("#OfficePostID").val(),
            ParentId: $("#ParentId").val(),
            OfficePostTitle: $("#OfficePostTitle").val(),
            OfficePostRoles: $("#OfficePostRoles").val(),
            OfficePostResponsibility: $("#OfficePostResponsibility").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/OfficePost/Edit',
            data: OfficePost,
            success: function (data) {

                getAllOfficePostForTreeView();
                $("#viewAll").load("/Admin/OfficePost/ViewOfficePostData/" + data.parentId);

                $("#myModal").modal("hide");
                swal({
                    type: 'success',
                    title: "ویرایش پست اداری",
                    text: "پست اداری  " + data.title + " با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                $("#div-loading").addClass('hidden');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function DeleteOfficePost(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این پست اداری مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonOfficePost: '#3085d6',
        cancelButtonOfficePost: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/OfficePost/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        //if (data == "NOK") {
                        //    $("#div-loading").addClass('hidden');
                        //    swal({
                        //        type: 'warning',
                        //        title: "حذف سمت",
                        //        text: "این پست برای محصولی تعریف شده است ابتدا محصول باید حذف شود",
                        //        showConfirmButton: true,
                        //        confirmButtonText: "تائید"
                        //    });

                        //}


                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                type: 'success',
                                title: "حذف پست اداری",
                                text: "پست اداری " + data.title + "  با موفقیت حذف شد",
                                showConfirmButton: true,
                                confirmButtonText: "تائید"
                            });
                            //$('#OfficePost_tb').find('tr[id=OfficePost_' + data.OfficePost_ID + ']').css('background-OfficePost', '#B26666').fadeOut(1000, function () {
                            //    var table = $('#OfficePost_tb').DataTable();
                            //    table.row("#OfficePost_" + data.OfficePost_ID).remove().draw(false);
                            //});
                            getAllOfficePostForTreeView();
                            //$("#viewAll").load("/Admin/OfficePost/ViewOfficePostData/" + data.parentId);
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
//
function OfficePostViewDetails(parentId) {
    $.ajax({
        method: 'POST',
        url: '/Admin/OfficePost/ViewOfficePostData',
        data: { id: parentId },
        success: function (data) {

            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}


//////////////////////////// بخش فعالیت ها
function openModalForAddActivity() {
    $.get("/Admin/Activity/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن فعالیت جدید ");
        $("#myModalBody").html(result);
    });

}
//
function InsertActivity() {
    var frm = $("#ActivityFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var Activity = {
            ActivityTitle: $("#ActivityTitle").val(),
            ActivityDescription: $("#ActivityDescription").val(),
            ActivityFieldID: $("#ActivityFieldID").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Activity/Create',
            data: Activity,
            success: function (data) {
                if (data == "repeate") {
                    $("#div-loading").addClass('hidden');
                    swal({

                        type: 'warning',
                        title: "افزودن فعالیت",
                        text: "این فعالیت قبلا ثبت شده",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });


                }
                else if (data != "nall") {

                    $("#viewAll").load("/Admin/Activity/ViewAll/" + $('#ActivityFieldList').val());
                    $("#myModal").modal("hide");

                    swal({

                        type: 'success',
                        title: "افزودن فعالیت",
                        text: "فعالیت  " + data.activityTitle + " با موفقیت ثبت شد ",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

//
function openModalForEditActivity(id) {
    $.get("/Admin/Activity/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش فعالیت");
        $("#myModalBody").html(result);
    });

}
//
function UpdateActivity() {
    var frm = $("#ActivityFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var Activity = {
            Activity_ID: $("#Activity_ID").val(),
            ActivityTitle: $("#ActivityTitle").val(),
            ActivityDescription: $("#ActivityDescription").val(),
            ActivityFieldID: $("#ActivityFieldID").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Activity/Edit',
            data: Activity,
            success: function (data) {

                $("#viewAll").load("/Admin/Activity/ViewAll/" + $('#ActivityFieldList').val());
                $("#myModal").modal("hide");
                swal({
                    type: 'success',
                    title: "ویرایش فعالیت",
                    text: "فعالیت  " + data.activityTitle + " با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                $("#div-loading").addClass('hidden');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function DeleteActivity(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این فعالیت مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonActivity: '#3085d6',
        cancelButtonActivity: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Activity/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                type: 'success',
                                title: "حذف فعالیت",
                                text: "فعالیت " + data.title + "  با موفقیت حذف شد",
                                showConfirmButton: true,
                                confirmButtonText: "تائید"
                            });

                            $("#viewAll").load("/Admin/Activity/ViewAll/" + $('#ActivityFieldList').val());
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}

function GetAllActivityByActivityField() {
    $("#viewAll").load("/Admin/Activity/ViewAll/" + $('#ActivityFieldList').val());
}


////////////////////////////// بخش مرخصی
//function openModalForVacationPreview(StartDate, EndDate, VacDate) {
//    $.get("/Admin/Vacation/VacationPreview", function (result) {
//        $("#myModal").modal();
//        $("#myModalLabel").html("درخواست مرخصی");
//        $("#myModalBody").html(result);
//        $(".modal-dialog").css({ 'width': '75%' });
//        $("#lblPerson").text($("#lblFullName").text());
//        $("#lblPersonalCode_1").text($("#lblPersonalCode").text());
//        $("#lblPersonalCode_2").text($("#lblFullName").text());
//        $("#lblAlternateName").text($("#alternateList_ddl option:selected").text());
//        $("#alternateCode").val($("#alternateList_ddl").val());


//        if (VacDate != "nall") {
//            $("#lblVacDate").text(VacDate);
//            $("#lblFromTime").text($("#StartTime").val());
//            $("#lblToTime").text($("#EndTime").val());
//            $("#lblVacationType").text('ساعتی');
//            $("#lblSection").text($("#Section2").val());
//        }
//        else {
//            $("#lblVacDate").text("---");
//            $("#lblFromTime").text("---");
//            $("#lblToTime").text("---");
//        }
//        if (StartDate != "nall") {
//            $("#lblStartDate").text(StartDate);
//            $("#lblDuration").text($("#Duration").val() + " روز ");
//            $("#lblEndDate").text(EndDate);
//            $("#lblVacationType").text('روزانه');
//            $("#lblSection").text($("#Section1").val());
//        }
//        else {
//            $("#lblStartDate").text("---");
//            $("#lblEndDate").text("---");
//            $("#lblDuration").html("---");
//        }

//    });
//}
//function InsertVacation() {
//    $("#div-loading").removeClass('hidden');
//    $.ajax({
//        method: 'POST',
//        url: '/Admin/Vacation/InsertVacation',
//        data: { sdate: $("#tarikhAlt1").val(), eDate: $("#tarikhAlt2").val(), timeDate: $("#tarikhAlt3").val(), st: status },
//        success: function (data) {
//            $("#div-loading").addClass('hidden');
//            //if (data.status == "ok") {
//            //    swal({
//            //        type: 'success',
//            //        title: "درخواست مرخصی",
//            //        text: "درخواست شما با موفقیت ثبت شد ",
//            //        showConfirmButton: true,
//            //        confirmButtonText: "تائید"
//            //    });
//            //}

//            openModalForVacationPreview(data.startDate, data.endDate, data.vacDate);

//        },
//        error: function (result) {
//        }
//    });
//}

//function openModalForAddVacation() {
//    $.get("/Admin/Vacation/InsertVacation", function (result) {
//        $("#myModal").modal();
//        $("#myModalLabel").html("درخواست مرخصی ");
//        $("#myModalBody").html(result);
//        $(".modal-dialog").css({ 'width': '65%' });
//    });

//}

//function RegisterVacation() {
//    //var frm = $("#vacation_frm").valid();
//    //if (frm === true) {
//        $("#div-loading").removeClass('hidden');

//        var vacation = {
//            VecationType: vacationType,
//            PersonalCode_3: $("#lblPersonalCode").text(),
//            StartTime: $("#StartTime").val(),
//            EndTime: $("#EndTime").val(),
//            Duration: $("#Duration").val(),
//            Section: $("#Section").val(),
//            AlternateSignature: $("#alternateCode").val(),
//            StatusAlternateSignature: 0
//    }
//    console.log('1: ' + $("#tarikhAlt1").val());
//    console.log('2: ' + $("#tarikhAlt2").val());
//    console.log('3: ' + $("#tarikhAlt3").val());

//        $.ajax({
//            method: 'POST',
//            url: '/Admin/Vacation/RegisterVacation',
//            data: { vacation: vacation, sdate: $("#tarikhAlt1").val(), eDate: $("#tarikhAlt2").val(), timeDate: $("#tarikhAlt3").val(), st: status },
//            success: function (data) {
//                $("#div-loading").addClass('hidden');
//                if (data.status == "ok") {
//                    swal({
//                        type: 'success',
//                        title: "درخواست مرخصی",
//                        text: "درخواست شما با موفقیت ثبت شد ",
//                        showConfirmButton: true,
//                        confirmButtonText: "تائید"
//                    });

//                    $("#viewAll").load("/Admin/Vacation/ViewAll/" + $("#lblPersonalCode").text());
//                }

//            },
//            error: function (result) {
//            }
//        });
//    //}
//    //else {
//    //    return false;
//    //}
//}

//function ViewAllVacations(id) {
//    debugger;
//    $.get("/Admin/Vacation/ViewAll/" + id, function (res) {
//        $("#viewAll").html(res);
//    });
//}

//function openModalForEditVacation(id) {
//    $.get("/Admin/Vacation/EditVacation/" + id, function (result) {
//        $("#myModal").modal();
//        $("#myModalLabel").html(" ویرایش درخواست مرخصی ");
//        $("#myModalBody").html(result);
//        $(".modal-dialog").css({ 'width': '65%' });
//    });
//}

//function UpdateVacation() {
//    var frm = $("#updateVacation_frm").valid();
//    if (frm === true) {
//        $("#div-loading").removeClass('hidden');

//        var section = "";
//        if (vacationType1 == "True") {
//            section = $("#Section3").val();
//        }
//        else {
//            section = $("#Section4").val();
//        }
//        var vacation = {
//            VecationType: vacationType1,
//            PersonalCode_3: $("#PersonalCode_3").val(),
//            StartTime: $("#StartTime_1").val(),
//            EndTime: $("#EndTime_1").val(),
//            Duration: $("#Duration_1").val(),
//            Vecation_ID: $("#Vecation_ID").val(),
//            CreateDate: $("#CreateDate").val(),
//            Section: section,
//            AlternateSignature: $("#alternateListEdit_ddl").val(),
//            StatusAlternateSignature: 0
//        }


//        $.ajax({
//            method: 'POST',
//            url: '/Admin/Vacation/EditVacation',
//            data: { vacation: vacation, sdate: $("#tarikhAlt4").val(), eDate: $("#tarikhAlt5").val(), timeDate: $("#tarikhAlt6").val(), st: status1 },
//            success: function (data) {
//                $("#div-loading").addClass('hidden');
//                if (data.status == "ok") {
//                    swal({
//                        type: 'success',
//                        title: "درخواست مرخصی",
//                        text: "درخواست شما با موفقیت ویرایش شد ",
//                        showConfirmButton: true,
//                        confirmButtonText: "تائید"
//                    });

//                    $("#viewAll").load("/Admin/Vacation/ViewAll/" + $("#PersonalCode_3").val());
//                }

//            },
//            error: function (result) {
//            }
//        });
//    }
//    else {
//        return false;
//    }
//}
//function GetAllMissionForMali() {

//    $.ajax({
//        method: 'POST',
//        url: '/Admin/MissionsRequest/ViewAll2',
//        data: '',
//        success: function (data) {
//            $("#viewAll2").html(data);
//        },
//        error: function (result) {
//        }
//    });
//}

//function GetAllVacationForMali() {

//    $.ajax({
//        method: 'POST',
//        url: '/Admin/VacationRequest/ViewAll2',
//        data: '',
//        success: function (data) {
//            $("#viewAll2").html(data);
//        },
//        error: function (result) {
//        }
//    });
//}
//function GetAllVacationForVacationOfFactoryManager() {

//    $.ajax({
//        method: 'POST',
//        url: '/Admin/VacationRequest/ViewAllVacationOfFactoryManager',
//        data: '',
//        success: function (data) {
//            $("#viewAllFactoryManager").html(data);
//        },
//        error: function (result) {
//        }
//    });
//}
//function openModalForConfirm(id) {
//    $.get("/Admin/VacationRequest/ViewDetails/" + id, function (result) {
//        $("#myModal").modal();
//        $("#myModalLabel").html("تاییدیه مرخصی");
//        $("#myModalBody").html(result);
//        $(".modal-dialog").css({ 'width': '65%' });
//    });
//}
//function openModalForConfirm2(id) {
//    $.get("/Admin/VacationRequest/ViewDetails2/" + id, function (result) {
//        $("#myModal").modal();
//        $("#myModalLabel").html("تاییدیه مرخصی");
//        $("#myModalBody").html(result);
//        $(".modal-dialog").css({ 'width': '65%' });
//    });
//}
//function ConfirmUnitSignature(vacationId, status) {
//    var statuss = "";
//    if (status == "confirm") {
//        statuss = "ok";
//    }
//    else {
//        statuss = "nok";
//    }

//    $.ajax({
//        method: 'POST',
//        url: '/Admin/VacationRequest/ConfirmUnitSignature',
//        data: { id: vacationId, status: statuss },
//        success: function (data) {
//            if (data.status == "ok") {
//                swal({
//                    type: 'success',
//                    title: "تاییدیه مرخصی",
//                    text: "عملیات با موفقیت انجام شد ",
//                    showConfirmButton: true,
//                    confirmButtonText: "تائید"
//                });
//            }
//        },
//        error: function (result) {
//        }
//    });
//}

//function ConfirmAdministrativeAffairsSignature(vacationId, status) {
//    var statuss = "";
//    if (status == "confirm") {
//        statuss = "ok";
//    }
//    else {
//        statuss = "nok";
//    }

//    $.ajax({
//        method: 'POST',
//        url: '/Admin/VacationRequest/ConfirmAdministrativeAffairsSignature',
//        data: { id: vacationId, status: statuss },
//        success: function (data) {
//            if (data.status == "ok") {
//                swal({
//                    type: 'success',
//                    title: "تاییدیه مرخصی",
//                    text: "عملیات با موفقیت انجام شد ",
//                    showConfirmButton: true,
//                    confirmButtonText: "تائید"
//                });
//            }
//        },
//        error: function (result) {
//        }
//    });
//}


/// ------------------------------------- تامین کنندگان
function GetSupplierDetails(Id) {
    $.get("/Admin/Suppliers/ViewDetails/" + Id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("جزئیات ارزیابی توان مالی تامین کنندگان");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}
/// ------------------------------------- شکایات مشتریان
function GetComplaintDetails(Id) {
    $.get("/Admin/Complaint/ViewDetails/" + Id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("جزئیات شکایت مشتری");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

/// ------------------------------------- ارائه پیشنهادات
function CheckIsExsistEmployee(PersonalCode) {
    $.ajax({
        method: 'POST',
        url: '/Admin/Proposal/CheckIsExsistEmployee',
        data: { id: PersonalCode },
        success: function (response) {
            if (response == "no_exsist") {
                $("#pCode").text("کارمندی با این کد پرسنلی وجود ندارد");
                $("#empName").addClass('hidden');
                $("#empName").text("");
                $("#ppRegister_btn").addClass("disabled");
            }
            else {
                $("#empName").removeClass('hidden');
                $("#empName").text("  (" + response.name + ")");
                $("#pCode").text("");
                $("#ppRegister_btn").removeClass("disabled");
            }
        },
        error: function (response) {
        }
    });
}
function InsertProposalProvider() {
    var frm = $("#proposalProvider_frm").valid();
    if (frm) {
        var Unit = $("#UnitId option:selected").text();
        var UnitId = $("#UnitId").val();
        var Post = $("#PostId option:selected").text();
        var PostId = $("#PostId").val();
        var PersonalCode = $("#PersonalCode").val();
        var PhoneNumber = $("#PhoneNumber").val();

        var tr = "";
        tr = ' <tr>' +
            //'<th scope ="row" class="num">1</th>' +
            '<td>' + PersonalCode + '</td>' +
            '<td data-id=' + UnitId + '>' + Unit + '</td>' +
            '<td data-id=' + PostId + '>' + Post + '</td>' +
            '<td>' + PhoneNumber + '</td>' +
            '<td><i class="fas fa-trash-alt btnProposalProviderDelete"></i></td>' +
            '</tr >';
        $("#proposalProvider_tr").append(tr);
        $("#proposalProvider_tr2").append(tr);
        $("#proposalProvider_frm")[0].reset();
        $("#empName").text("");
    }
    else {
        return false;
    }
}



function ProposalRegister() {
    var frm = $("#mainProposal_frm").valid();

    if (frm) {
        $("#proposalPrint_div").removeClass('hidden');
        $("#proposal_div").addClass('hidden');
        $('html,body').animate({ 'scrollTop': '0px' }, 1000);

        ProposalFormFill();
    }
    else {
        return false;
    }
}

function ProposalFormFill() {
    $("#lblTitle").text($("#Title").val());
    $("#lblDate").text($("#ProposalDate").val());
    $("#lblDescrip").text($("#ProposalDescription").val());
    $("#lblCurrentStatus").text($("#CurrentStatus").val());
    $("#lblImplementMethod").text($("#ImplementMethod").val());
    $("#lblPossibilities").text($("#Possibilities").val());
    $("#lblFinancialSavings").text($("#FinancialSavings").val());
    $("#lblImplementPrice").text($("#ImplementPrice").val());


}

function ComebackToSupplierForm() {
    $("#proposal_div").removeClass('hidden');
    $("#proposalPrint_div").addClass('hidden');
    $('html,body').animate({ 'scrollTop': '0px' }, 1000);
}

function SaveProposalData() {
    var Unit = [], Post = [], PersonalCode = [], PhonNumber = [];
    var docTitle = [], docFile = [];

    var mainProposal = {
        Title: $("#lblTitle").text(),
        ProposalDate: $("#lblDate").text(),
        ProposalDescription: $("#lblDescrip").text(),
        CurrentStatus: $("#lblCurrentStatus").text(),
        ImplementMethod: $("#lblImplementMethod").text(),
        Possibilities: $("#lblPossibilities").text(),
        FinancialSavings: $("#lblFinancialSavings").text(),
        ImplementPrice: $("#lblImplementPrice").text()
    }

    $("#proposalProvider_tr").children('tr').each(function () {
        Unit.push($(this).children('td:eq(1)').attr('data-id'));
        Post.push($(this).children('td:eq(2)').attr('data-id'));
        PersonalCode.push($(this).children('td:eq(0)').html());
        PhonNumber.push($(this).children('td:eq(3)').html());

    });

    $("#docsPreviews").children('.myDoc').children('.forms-file-record').each(function () {
        docTitle.push($(this).children('p').text());
        docFile.push($(this).children('p').attr('data-file'));
    });
    //alert(certiType);
    //alert(docFile);
    //alert(type);
    //alert(percent);



    $.ajax({
        method: 'POST',
        url: '/Admin/Proposal/ProposalRegister',
        data: { mainProposal: mainProposal, 'docTitle': docTitle, 'docFile': docFile, 'units': Unit, 'posts': Post, 'personalCodes': PersonalCode, 'phones': PhonNumber },
        success: function (response) {
            if (response == "ok") {
                swal({
                    type: 'success',
                    title: "ثبت فرم پیشنهادات مشتریان",
                    text: "اطلاعات شما با موفقیت ثبت گردید ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                $("#btnProposalRegister").addClass("disabled");
            }
        },
        error: function (response) {
        }
    });
}

function GetAllProposals() {

    $.ajax({
        method: 'POST',
        url: '/Admin/adminProposals/ViewAll',
        data: '',
        success: function (data) {
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}

function ViewDetailsProposal(Id) {
    $.get("/Admin/adminProposals/ViewDetailsMainProposal/" + Id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("جزئیات پیشنهاد ارائه شده");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });

}
//--------------------------------------------------------- نظرسنجی اصلی
function openModalForAddMainSurvey() {
    $.get("/Admin/MainSurvey/Create/", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن نظرسنجی جدید");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '55%' });
    });

}

function viewAllSurvayByType(typeId) {
    $("#viewAll").load("/Admin/MainSurvey/ViewAll/" + typeId);
}

//
function InsertMainSurvey() {
    var frm = $("#mainSurveyFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');

        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive == 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var mainSurvey = {
            SurveyType: $("#SurveyType").val(),
            IsActive: status,
            Title: $("#Title").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/MainSurvey/Create',
            data: mainSurvey,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                if (data == "ok") {

                    $("#viewAll").load("/Admin/MainSurvey/ViewAll");
                    $("#myModal").modal('hide');
                    swal({
                        title: "نظرسنجی اصلی",
                        text: "نظرسنجی اصلی با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    })
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
///
function openModalForEditMainSurvey(Id) {
    $.get("/Admin/MainSurvey/Edit/" + Id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش نظرسنجی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '55%' });
    });

}
//
function UpdateMainSurvey() {
    var frm = $("#mainSurveyFrmEdit").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');
        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive == 1) {
            status = "true";
        }
        else {
            status = "false";
        }


        var mainSurvey = {
            MainSurvey_ID: $("#MainSurvey_ID").val(),
            CreateDate: $("#CreateDate").val(),
            SurveyType: $("#SurveyType").val(),
            IsActive: status,
            Title: $("#Title").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/MainSurvey/Edit',
            data: mainSurvey,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                if (data == "ok") {

                    $("#viewAll").load("/Admin/MainSurvey/ViewAll");
                    $("#myModal").modal('hide');
                    swal({
                        title: "نظرسنجی اصلی",
                        text: "نظرسنجی اصلی با موفقیت ویرایش شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    })
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
///
function DeleteMainSurvey(Id) {
    swal({
        title: "هشدار !!!",
        text: "آیا برای حذف این نظرسنجی اصلی مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"

    })
        .then((willDelete) => {
            if (willDelete.value) {

                $.ajax({
                    url: "/Admin/MainSurvey/Delete",
                    type: "Get",
                    data: { id: Id }
                }).done(function (result) {
                    if (result.redirect) { ShowAccessDenied(); return false; }

                    if (result == "ex_group") {
                        swal({
                            title: "نظرسنجی اصلی",
                            text: "برای این نظرسنجی اصلی گروه سوالات تعریف شده است",
                            type: "warning",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        })
                    }
                    else if (result == "ok") {
                        $('#mainSurvey_tb').find('tr[id=mSurvey_' + Id + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#mainSurvey_tb').DataTable();
                            table.row("#mSurvey_" + Id).remove().draw(false);
                        });

                        swal({ title: "نظرسنجی", text: " نظرسنجی اصلی با موفقیت حذف شد", icon: "success", buttons: [false, "تایید"], });
                    }
                });
            }
        });
}
////

function ViewDetailsSurvey(Id) {
    $.get("/Admin/MainSurvey/ViewDatailsSurvey/" + Id, function (result) {
        $("a[href='#amarSurvey']").attr("data-toggle", "tab");

        $("#survey_li").removeClass('active');
        $("#survey").removeClass('active');
        $("#amarSurvey_li").addClass('active');
        $("#amarSurvey").addClass('active');

        $("#viewAllAmar").html(result);

        //$("#myModal").modal();
        //$("#myModalLabel").html("آمار نظرسنجی");
        //$("#myModalBody").html(result);
        //$(".modal-dialog").css({ "width": "90%" });
    });

}

function ViewAllSurveyPeople() {
    $.get("/Admin/MainSurvey/ViewAllSurveyPeople/", function (result) {
        //$("a[href='#SurveyPeople']").attr("data-toggle", "tab");

        //$('.nav-tabs li').removeClass('active');
        //$('.tab-pane').removeClass('active');
        //$("#SurveyPeople_li").addClass('active');
        //$("#SurveyPeople").addClass('active');

        $("#viewAllSurveyPeople").html(result);
    });
}

function ViewOneSurveyPeople(id, surveyType) {
    if (surveyType == 0)//تامین کنندگان
    {
        $.get("/Admin/MainSurvey/ViewOneSupplierSurveyPeople/" + id, function (result) {
            //$("a[href='#SurveyPeople']").attr("data-toggle", "tab");

            $('.nav-tabs li').removeClass('active');
            $('.tab-pane').removeClass('active');
            $("#SurveyPeople_li").addClass('active');
            $("#SurveyPeople").addClass('active');

            $("#viewAllSurveyPeople").html(result);
        });
    }
    if (surveyType == 1)//مشتریان
    {
        $.get("/Admin/MainSurvey/ViewOneSurveyPeople/" + id, function (result) {
            //$("a[href='#SurveyPeople']").attr("data-toggle", "tab");

            $('.nav-tabs li').removeClass('active');
            $('.tab-pane').removeClass('active');
            $("#SurveyPeople_li").addClass('active');
            $("#SurveyPeople").addClass('active');

            $("#viewAllSurveyPeople").html(result);
        });
    }
    if (surveyType == 2)//کارکنان
    {
        $.get("/Admin/MainSurvey/ViewOneEmployeeSurveyPeople/" + id, function (result) {
            //$("a[href='#SurveyPeople']").attr("data-toggle", "tab");

            $('.nav-tabs li').removeClass('active');
            $('.tab-pane').removeClass('active');
            $("#SurveyPeople_li").addClass('active');
            $("#SurveyPeople").addClass('active');

            $("#viewAllSurveyPeople").html(result);
        });
    }

}

function showThisPersonSurvey(id, surveyType) {
    if (surveyType == 0)//تامین کنندگان
    {
        $.get("/Admin/MainSurvey/showThisSupplierSurvey/" + id, function (result) {
            $('.nav-tabs li').removeClass('active');
            $('.tab-pane').removeClass('active');
            $("#SurveyPeopleForm_li").addClass('active');
            $("#SurveyPeopleForm").addClass('active');

            $("#viewSurveyDetails").html(result);
        });
    }
    if (surveyType == 1)//مشتریان
    {
        $.get("/Admin/MainSurvey/showThisPersonSurvey/" + id, function (result) {
            $('.nav-tabs li').removeClass('active');
            $('.tab-pane').removeClass('active');
            $("#SurveyPeopleForm_li").addClass('active');
            $("#SurveyPeopleForm").addClass('active');

            $("#viewSurveyDetails").html(result);
        });
    }
    if (surveyType == 2)//کارکنان
    {
        $.get("/Admin/MainSurvey/showThisEmployeeSurvey/" + id, function (result) {
            $('.nav-tabs li').removeClass('active');
            $('.tab-pane').removeClass('active');
            $("#SurveyPeopleForm_li").addClass('active');
            $("#SurveyPeopleForm").addClass('active');

            $("#viewSurveyDetails").html(result);
        });
    }

}

//------------------------------------ گروه سوالات نظرسنجی
function GetAllMainSurvey(container) {

    $.ajax({
        method: 'POST',
        url: '/Admin/SurveyQuestionGroup/GetAllMainSurvey',
        datatype: "json",
        contentType: "application/json;charset=utf-8",
        data: '',
        success: function (data) {

            var options = "<option value='0'>انتخاب کنید ...</option>";
            data.forEach(function (obj) {

                options += "<option value=" + obj.mainSurvey_ID + ">" + obj.title + "</option>";
            });
            container.html(options);
        },
        error: function (result) {
        }
    });
}
///
function GetAllSurveyQuestionGroup(Id) {

    $.ajax({
        method: 'POST',
        url: '/Admin/SurveyQuestionGroup/ViewAll',
        data: { id: Id },
        success: function (data) {

            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}
///
function openModalForAddSurveyQuestionGroup() {
    $.get("/Admin/SurveyQuestionGroup/Create/", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گروه سوالات نظرسنجی");
        $("#myModalBody").html(result);
    });

}
//
function InsertSurveyQuestionGroup() {
    var frm = $("#surveyQuestionGroupFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');


        var mainSurvey = {
            MainSurveyId: $("#MainSurveyId").val(),
            Title: $("#Title").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/SurveyQuestionGroup/Create',
            data: mainSurvey,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                if (data.status == "ok") {

                    $("#viewAll").load("/Admin/SurveyQuestionGroup/ViewAll/" + data.msurveyId);
                    $("#myModal").modal('hide');
                    swal({
                        title: "گروه سوالات نظرسنجی",
                        text: "گروه سوال نظرسنجی با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    })
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

///
function ChangeSelectedOptionSQG(value) {
    $("#mainSurvey_ddl").val(value);
    GetAllSurveyQuestionGroup(value);
}
///
function openModalForEditSurveyQuestionGroup(Id) {
    $.get("/Admin/SurveyQuestionGroup/Edit/" + Id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گروه سوالات نظرسنجی");
        $("#myModalBody").html(result);
    });

}
//
function UpdateSurveyQuestionGroup() {
    var frm = $("#surveyQuestionGroupFrmEdit").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');


        var mainSurvey = {
            SurveyGroup_ID: $("#SurveyGroup_ID").val(),
            MainSurveyId: $("#MainSurveyId").val(),
            Title: $("#Title").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/SurveyQuestionGroup/Edit',
            data: mainSurvey,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                if (data.status == "ok") {

                    $("#viewAll").load("/Admin/SurveyQuestionGroup/ViewAll/" + data.msurveyId);
                    $("#myModal").modal('hide');
                    swal({
                        title: "گروه سوالات نظرسنجی",
                        text: "گروه سوال نظرسنجی با موفقیت ویرایش شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    })
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

///
function DeleteSurveyQuestionGroup(Id) {
    swal({
        title: "هشدار !!!",
        text: "آیا برای حذف این گروه سوال مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"

    })
        .then((willDelete) => {
            if (willDelete.value) {

                $.ajax({
                    url: "/Admin/SurveyQuestionGroup/Delete",
                    type: "Get",
                    data: { id: Id }
                }).done(function (result) {
                    if (result.redirect) { ShowAccessDenied(); return false; }

                    if (result == "ex_ques") {
                        swal({
                            title: "گروه سوالات",
                            text: "برای این گروه , سوال تعریف شده است",
                            type: "warning",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        })
                    }
                    else if (result == "ok") {
                        $('#quesSurveyGroup_tb').find('tr[id=sqg_' + Id + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#quesSurveyGroup_tb').DataTable();
                            table.row("#sqg_" + Id).remove().draw(false);
                        });

                        swal({ title: "گروه سوالات", text: " گروه سوالات نظرسنجی با موفقیت حذف شد", icon: "success", buttons: [false, "تایید"], });
                    }
                });
            }
        });
}

/////////////////////////////////// نوع جواب نظرسنجی
function openModalForAddSurveyAnswerType() {
    $.get("/Admin/SurveyAnswerType/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن نوع جواب نظرسنجی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '60%' });
    });

}
//
function InsertSurveyAnswerType() {
    var frm = $("#answerTypeFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');


        var surveyAnswerType = {
            Title: $("#Title").val(),
            OptionTitle1: $("#OptionTitle1").val(),
            OptionTitle2: $("#OptionTitle2").val(),
            OptionTitle3: $("#OptionTitle3").val(),
            OptionTitle4: $("#OptionTitle4").val(),
            OptionTitle5: $("#OptionTitle5").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/SurveyAnswerType/Create',
            data: surveyAnswerType,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                if (data == "ok") {

                    $("#viewAll").load("/Admin/SurveyAnswerType/ViewAll");
                    $("#myModal").modal('hide');
                    swal({
                        title: "نوع جواب نظرسنجی",
                        text: "نوع جواب با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    })
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
////
function openModalForEditSurveyAnswerType(Id) {
    $.get("/Admin/SurveyAnswerType/Edit/" + Id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش نوع جواب نظرسنجی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '60%' });
    });

}
//
function UpdateSurveyAnswerType() {
    var frm = $("#answerTypeFrmEdit").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');


        var surveyAnswerType = {
            AnswerType_ID: $("#AnswerType_ID").val(),
            Title: $("#Title").val(),
            OptionTitle1: $("#OptionTitle1").val(),
            OptionTitle2: $("#OptionTitle2").val(),
            OptionTitle3: $("#OptionTitle3").val(),
            OptionTitle4: $("#OptionTitle4").val(),
            OptionTitle5: $("#OptionTitle5").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/SurveyAnswerType/Edit',
            data: surveyAnswerType,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                if (data == "ok") {

                    $("#viewAll").load("/Admin/SurveyAnswerType/ViewAll");
                    $("#myModal").modal('hide');
                    swal({
                        title: "نوع جواب نظرسنجی",
                        text: "نوع جواب با موفقیت ویرایش شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    })
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

////
function DeleteSurveyAnswerType(Id) {
    swal({
        title: "هشدار !!!",
        text: "آیا برای حذف این نوع جواب مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"

    })
        .then((willDelete) => {
            if (willDelete.value) {

                $.ajax({
                    url: "/Admin/SurveyAnswerType/Delete",
                    type: "Get",
                    data: { id: Id }
                }).done(function (result) {
                    if (result.redirect) { ShowAccessDenied(); return false; }

                    if (result == "ex_ques") {
                        swal({
                            title: "نوع جواب نظرسنجی",
                            text: "برای این نوع جواب نظرسنجی , سوال تعریف شده است",
                            type: "warning",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        })
                    }
                    else if (result == "ok") {
                        $('#surveyAnswerType_tb').find('tr[id=ansType_' + Id + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#surveyAnswerType_tb').DataTable();
                            table.row("#ansType_" + Id).remove().draw(false);
                        });

                        swal({
                            title: "نوع جواب ",
                            text: " نوع جواب نظرسنجی با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                    }
                });
            }
        });
}

////---------------------------------------------------- بخش سوالات نظرسنجی
function GetAllQuesionGroup_2(container, Id) {
    $.ajax({
        url: '/Admin/SurveyQuestion/GetAllSurveyQuestionGroup',
        type: "GET",
        data: { id: Id },
        success: function (data) {

            var options = "<option value='0'>انتخاب کنید ...</option>";
            data.forEach(function (obj) {

                options += "<option value=" + obj.surveyGroup_ID + ">" + obj.title + "</option>";
            });
            container.html(options);
        },
        error: function (result) {
        }
    });
}
////
function GetAllSurveyQuestion(Id) {

    $.ajax({
        method: 'POST',
        url: '/Admin/SurveyQuestion/ViewAll',
        data: { id: Id },
        success: function (data) {

            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}
////
function openModalForAddSurveyQuestion(Id) {
    $.get("/Admin/SurveyQuestion/Create/" + Id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن سوال نظرسنجی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '60%' });
    });

}
//
function InsertSurveyQuestion() {
    var frm = $("#surveyQuestionFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');


        var surveyQuestion = {
            Question: $("#Question").val(),
            QuestionGroupId: $("#QuestionGroupId").val(),
            AnswerTypeId: $("#AnswerTypeId").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/SurveyQuestion/Create',
            data: surveyQuestion,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                if (data.status == "ok") {

                    $("#viewAll").load("/Admin/SurveyQuestion/ViewAll/" + data.groupId);
                    //$("#myModal").modal('hide');
                    swal({
                        title: "سوال نظرسنجی",
                        text: "سوال نظرسنجی با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    })
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

////
function openModalForEditSurveyQuestion(Id) {
    $.get("/Admin/SurveyQuestion/Edit/" + Id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش سوال نظرسنجی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '60%' });
    });

}
//
function UpdateSurveyQuestion() {
    var frm = $("#surveyQuestionFrmEdit").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');


        var surveyQuestion = {
            Question_ID: $("#Question_ID").val(),
            Question: $("#Question").val(),
            QuestionGroupId: $("#QuestionGroupId").val(),
            AnswerTypeId: $("#AnswerTypeId").val(),
            ReplyCount: $("#ReplyCount").val(),
            OptionVoteCount1: $("#OptionVoteCount1").val(),
            OptionVoteCount2: $("#OptionVoteCount2").val(),
            OptionVoteCount3: $("#OptionVoteCount3").val(),
            OptionVoteCount4: $("#OptionVoteCount4").val(),
            OptionVoteCount5: $("#OptionVoteCount5").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/SurveyQuestion/Edit',
            data: surveyQuestion,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                if (data.status == "ok") {

                    $("#viewAll").load("/Admin/SurveyQuestion/ViewAll/" + data.groupId);
                    //$("#myModal").modal('hide');
                    swal({
                        title: "سوال نظرسنجی",
                        text: "سوال نظرسنجی با موفقیت ویرایش شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    })
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
////
function DeleteSurveyQuestion(Id) {
    swal({
        title: "هشدار !!!",
        text: "آیا برای حذف این سوال نظرسنجی مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"

    })
        .then((willDelete) => {
            if (willDelete.value) {

                $.ajax({
                    url: "/Admin/SurveyQuestion/Delete",
                    type: "Get",
                    data: { id: Id }
                }).done(function (result) {
                    if (result.redirect) { ShowAccessDenied(); return false; }

                    if (result == "ex_ques_reply") {
                        swal({
                            title: "سوال نظرسنجی",
                            text: "برای این سوال نظرسنجی , سوال تعریف شده است",
                            type: "warning",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        })
                    }
                    else if (result == "ok") {
                        $('#quesSurvey_tb').find('tr[id=ques_' + Id + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#quesSurvey_tb').DataTable();
                            table.row("#ques_" + Id).remove().draw(false);
                        });

                        swal({
                            title: "سوال نظرسنجی ",
                            text: " سوال نظرسنجی با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                    }
                });
            }
        });
}
//////////////////////////////////////// نظرسنجی کارکنان
function EmployeeSurvey() {

    var frm = $("#employeeSurvey_frm").valid();

    if (frm) {
        if (EmployeeSurveyValidate()) {

            var surveyEmployeesOtherData = {
                PersonalCode: $("#lblPersonalCode").text(),
                Description: $("#Description").val(),
                MainSurveyId: $("#lblMainSurveyId").text()
            }

            $.ajax({
                method: 'POST',
                url: '/Admin/EmployeeSurvey/SurveySubmit',
                data: { surveyEmployeesOtherData: surveyEmployeesOtherData },
                success: function (response) {
                    if (response == "ok") {
                        CheckValueOfRadiobutton();
                    }
                    else if (response == "Repeated") {
                        swal({
                            heading: 'خطا',
                            text: 'شما قبلا در این نظرسنجی شرکت کرده اید و نمی توانید مجددا شرکت کنید',
                            type: "warning",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        })
                    }
                },
                error: function (response) {
                    //showModalMessage('.modalMessage', 'خطای کلاینت', response, 'danger');
                }
            });
        }
        else {
            swal({
                heading: 'هشدار',
                text: 'لطفا تمام سوالات را پاسخ دهید',
                type: "warning",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            })

        }
    }
    else {
        return false;
    }
}

function EmployeeSurveyValidate() {
    var status = true;

    var questionIds = [];
    var selectedReplies = [];

    $('.notLabel').each(function () {

        if ($(this).find('input[type="radio"]:checked').length > 0) {
            questionIds.push($(this).find('input[type="radio"]:checked').attr('data-val'));
            selectedReplies.push($(this).find('input[type="radio"]:checked').attr('data-id'));
        }
        else {
            status = false;
        }
    });

    if (status != true) {
        return false;
        //alert("no");
    }
    return true;
}

function CheckValueOfRadiobutton() {
    var status = true;

    var questionIds = [];
    var selectedReplies = [];

    $('.notLabel').each(function () {
        if ($(this).find('input[type="radio"]:checked').length > 0) {
            selectedReplies.push($(this).find('input[type="radio"]:checked').attr('data-val'));
            questionIds.push($(this).find('input[type="radio"]:checked').attr('data-id'));
        }
        else {
            status = false;
        }
    });

    if (status != true) {
        swal({
            heading: 'هشدار',
            text: 'لطفا تمام سوالات را پاسخ دهید',
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید"
        })
    }
    else {

        $.ajax({
            method: 'POST',
            url: '/Admin/EmployeeSurvey/GetSurveyData',
            data: { questionId: questionIds, selectedReply: selectedReplies },
            success: function (response) {
                if (response == "ok") {
                    swal({
                        heading: 'نظرسنجی',
                        text: 'با تشکر از شرکت شما در نظرسنجی سایت',
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    })
                }
            },
            error: function (response) {
            }
        });
    }
}



/////
function TestChangeLang(lang) {
    $.ajax({
        method: 'POST',
        url: '/Admin/Language/ChangeLanguage',
        data: { 'culture': lang },
        success: function (response) {
            changeLanguage(lang);
        },
        error: function (response) {
            //showModalMessage('.modalMessage', 'خطای کلاینت', response, 'danger');
        }
    });
}

//ShowPreviewMission
function ShowMissionForApplicant(id) {
    $.get("/Admin/Missions/ShowMissionForApplicant/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ماموریت ");
        $("#myModalBody").html(result);
    });

}


function InsertMission(confirm) {
    var mission = {
        MissionType: $('input[name=MissionType]:checked').val(),
        MissionStart: $("#MissionStart").val(),
        MissionEnd: $("#MissionEnd").val(),
        Reason: $("#Reason").val(),
        Followers: $("#Followers").val(),
        Destination: $("#Destination").val(),
        Issue: $("#Issue").val(),
        PersonalCode_1: $("#PersonalCode_1").val(),
        FullName: $("#FullName").val()

    }
    if (confirm) {
        $("#myModal").modal('hide');

        $.ajax({
            method: 'POST',
            url: '/Admin/Missions/Create',
            data: {
                mission: mission, sDate: $("#tarikhAlt1").val(), eDate: $("#tarikhAlt2").val()
            },
            success: function (data) {
                swal({

                    type: 'success',
                    title: "ثبت ماموریت",
                    text: "ماموریت با موفقیت ثبت شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });
                window.location.href = '/Admin/Missions';
            },
            error: function (result) {
            }
        });
    }
    else {
        var frm = $("#mission_frm").valid();

        if (frm === true) {

            if (!$("#MissionStart").val().length > 0) {
                $("#MissionStart").blur();
                $("#MissionStart_val").text("تاریخ شروع ماموریت را انتخاب کنید");
                return false;
            }

            if (!$("#MissionEnd").val().length > 0) {
                $("#MissionEnd").blur();
                $("#MissionEnd_val").text("تاریخ پایان ماموریت را انتخاب کنید");
                return false;
            }

            $("#div-loading").removeClass('hidden');

            $.ajax({
                method: 'POST',
                url: '/Admin/Missions/ConfirmCreate',
                data: {
                    mission: mission, sDate: $("#tarikhAlt1").val(), eDate: $("#tarikhAlt2").val()
                },
                success: function (data) {
                    if (data.res == "Failed") {
                        swal({

                            type: 'warning',
                            title: "ثبت ماموریت",
                            text: data.message,
                            showConfirmButton: true,
                            confirmButtonText: "تائید"
                        });
                        $("#div-loading").addClass('hidden');

                    }
                    else {
                        $("#div-loading").addClass('hidden');

                        $("#myModal").modal();
                        $("#myModalLabel").html("تائید درخواست ماموریت");
                        $("#myModalBody").html(data);
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            return false;
        }
    }
}
function UpdateMission(confirm) {
    var mission = {
        MissionType: $('input[name=MissionType]:checked').val(),
        MissionStart: $("#MissionStart").val(),
        MissionEnd: $("#MissionEnd").val(),
        Reason: $("#Reason").val(),
        Followers: $("#Followers").val(),
        Destination: $("#Destination").val(),
        Issue: $("#Issue").val(),
        PersonalCode_1: $("#PersonalCode_1").val(),
        FullName: $("#FullName").val(),
        Mission_ID: $("#Mission_ID").val(),
        Employee_1: $("#Employee_1").val()

    }
    if (confirm) {
        $("#myModal").modal('hide');

        var id = $("#Mission_ID").val();
        $.ajax({
            method: 'POST',
            url: '/Admin/Missions/Edit/' + id,
            data: {
                mission: mission, sDate: $("#tarikhAlt1").val(), eDate: $("#tarikhAlt2").val()
            },
            success: function (data) {
                swal({

                    type: 'success',
                    title: "ویرایش ماموریت",
                    text: "ماموریت با موفقیت ویرایش شد",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });
                window.location.href = '/Admin/Missions';
            },
            error: function (result) {
            }
        });
    }
    else {
        var frm = $("#mission_frm").valid();

        if (frm === true) {

            if (!$("#MissionStart").val().length > 0) {
                $("#MissionStart").blur();
                $("#MissionStart_val").text("تاریخ شروع ماموریت را انتخاب کنید");
                return false;
            }

            if (!$("#MissionEnd").val().length > 0) {
                $("#MissionEnd").blur();
                $("#MissionEnd_val").text("تاریخ پایان ماموریت را انتخاب کنید");
                return false;
            }



            $("#div-loading").removeClass('hidden');



            $.ajax({
                method: 'POST',
                url: '/Admin/Missions/ConfirmEdit',
                data: {
                    mission: mission, sDate: $("#tarikhAlt1").val(), eDate: $("#tarikhAlt2").val()
                },
                success: function (data) {
                    if (data.res == "Failed") {
                        swal({

                            type: 'warning',
                            title: "ثبت ماموریت",
                            text: data.message,
                            showConfirmButton: true,
                            confirmButtonText: "تائید"
                        });
                        $("#div-loading").addClass('hidden');

                    }
                    else {
                        $("#div-loading").addClass('hidden');
                        $("#myModal").modal();
                        $("#myModalLabel").html("تائید درخواست ماموریت");
                        $("#myModalBody").html(data);
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            return false;
        }
    }
}
function DeleteMission(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این ماموریت هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Missions/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (!data.res) {
                            $("#div-loading").addClass('hidden');
                            swal({
                                type: 'warning',
                                title: "حذف ماموریت",
                                text: " امکان حذف وجود ندارد این ماموریت تائید شده است ",
                                showConfirmButton: true,
                                confirmButtonText: "تائید"
                            });

                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                type: 'success',
                                title: "حذف ماموریت",
                                text: "ماموریت با موفقیت ثبت شد",
                                showConfirmButton: true,
                                confirmButtonText: "تائید"
                            });
                            $('#mission_tb').find('tr[id=' + data.mission_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#mission_tb').DataTable();
                                table.row("#" + data.mission_ID).remove().draw(false);
                            });
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
function ShowConfirmAdmin(id) {
    $.get("/Admin/MissionsRequest/ShowConfirmAdmin/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ماموریت ");
        $("#myModalBody").html(result);
    });
}
function ConfirmMission(id) {
    $.ajax({
        method: 'GET',
        url: '/Admin/MissionsRequest/ConfirmMission/' + id,
        success: function (data) {
            swal({

                type: 'success',
                title: " ماموریت",
                text: " ماموریت با موفقیت تائید شد",
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            $("#myModal").modal('hide');
            $("#viewAll").load("/Admin/MissionsRequest/ViewAll");


        },
        error: function (result) {
        }
    });
}
function CancelMission(id) {
    $.ajax({
        method: 'GET',
        url: '/Admin/MissionsRequest/CancelMission/' + id,
        success: function (data) {
            swal({

                type: 'success',
                title: " ماموریت",
                text: " ماموریت با موفقیت رد شد",
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            $("#myModal").modal('hide');
            $("#viewAll").load("/Admin/MissionsRequest/ViewAll");

        },
        error: function (result) {
        }
    });
}
//
function viewAllMission() {
    $("#viewAll").load("/Admin/MissionsRequest/ViewAll");
    alert('ok')
}

function ShowHumanResources(id) {
    $.get("/Admin/MissionsRequest/HumanResources/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ماموریت ");
        $("#myModalBody").html(result);
    });
}
function ConfirmHumanResources(id) {
    $.ajax({
        method: 'GET',
        url: '/Admin/MissionsRequest/ConfirmHumanResources/' + id,
        success: function (data) {
            swal({

                type: 'success',
                title: " ماموریت",
                text: " ماموریت با موفقیت تائید شد",
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            $("#myModal").modal('hide');
            $("#viewAll").load("/Admin/MissionsRequest/ViewAll");


        },
        error: function (result) {
        }
    });
}
function CancelHumanResources(id) {
    $.ajax({
        method: 'GET',
        url: '/Admin/MissionsRequest/CancelHumanResources/' + id,
        success: function (data) {
            swal({

                type: 'success',
                title: " ماموریت",
                text: " ماموریت با موفقیت رد شد",
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            $("#myModal").modal('hide');
            $("#viewAll").load("/Admin/MissionsRequest/ViewAll");



        },
        error: function (result) {
        }
    });
}

function ShowFactoryManager(id) {
    $.get("/Admin/MissionsRequest/ConfirmFactoryManager/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ماموریت ");
        $("#myModalBody").html(result);
    });
}
function GetAllMissionForMissionOfFactoryManager() {

    $.ajax({
        method: 'POST',
        url: '/Admin/MissionsRequest/ViewAllMissionOfFactoryManager',
        data: '',
        success: function (data) {
            $("#viewAllFactoryManager").html(data);
        },
        error: function (result) {
        }
    });
}
//

function SalaryFileUpload() {
    $("#div-loading").removeClass('hidden');
    var files = $("#SalaryUploader").get(0).files;
    var data = new FormData();
    data.append("myFile", files[0]);

    $.ajax({
        //xhr: function () {
        //    var xhr = new window.XMLHttpRequest();
        //    xhr.upload.addEventListener("progress", function (evt) {
        //        if (evt.lengthComputable) {
        //            var percentComplete = (evt.loaded / evt.total) * 100;
        //            //Do something with upload progress here
        //            $("#statusProgress").text(Math.floor(percentComplete) + "%");
        //            $("#prg").removeClass('hidden');
        //            $("#prg .progress-bar").css("width", percentComplete + "%").addClass("progress-bar-striped active");
        //        }
        //    }, false);
        //    return xhr;
        //},

        method: 'POST',
        url: '/Admin/EmployeeSalaryUpload/SalaryUpload',
        data: data,
        contentType: false,
        processData: false,
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data == "ok") {
                swal({

                    type: 'success',
                    title: "  فیش حقوقی",
                    text: " فیش حقوقی با موفقیت آپلود شد",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });
            } else {
                swal({

                    type: 'danger',
                    title: "  فیش حقوقی",
                    text: data,
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });
            }
        },
        error: function (result) {
            alert(result.error);
        }
    });
}

///////
function ShowEmployeeSalaryDetails(id) {
    $.get("/Admin/EmployeeSalary/ViewDetails/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("فیش حقوقی ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '99%' });
        
           
        
    });
}

//////////////////////////// بخش مهارت های کامپیوتری
function openModalForAddSkillComputer() {
    $.get("/Admin/ComputerSkills/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن مهارت جدید ");
        $("#myModalBody").html(result);
    });

}
//
function InsertSkillComputer() {
    var frm = $("#SkillComputerFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var emp_ComputerSkillItems = {
            Title: $("#Title").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/ComputerSkills/Create',
            data: emp_ComputerSkillItems,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                if (data == "ok") {
                    $("#myModal").modal('hide');
                    $("#viewAll").load("/Admin/ComputerSkills/ViewAll");

                    swal({

                        type: 'success',
                        title: "افزودن مهارت کامپیوتری",
                        text: "مهارت کامپیوتری با موفقیت ثبت شد",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });


                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function openModalForEditSkillComputer(Id) {
    $.get("/Admin/ComputerSkills/Edit/" + Id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش مهارت  ");
        $("#myModalBody").html(result);
    });

}
//
function UpdateSkillComputer() {
    var frm = $("#SkillComputerFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var emp_ComputerSkillItems = {
            Item_ID: $("#Item_ID").val(),
            Title: $("#Title").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/ComputerSkills/Edit',
            data: emp_ComputerSkillItems,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                if (data == "ok") {
                    $("#myModal").modal('hide');
                    $("#viewAll").load("/Admin/ComputerSkills/ViewAll");
                    swal({

                        type: 'success',
                        title: "افزودن مهارت کامپیوتری",
                        text: "مهارت کامپیوتری با موفقیت ویرایش شد",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });


                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function DeleteSkillComputer(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این مهارت مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/ComputerSkills/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "invalid") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                type: 'warning',
                                title: "حذف مهارت کامپیوتری",
                                text: "خطایی رخ داده است",
                                showConfirmButton: true,
                                confirmButtonText: "تائید"
                            });

                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                type: 'success',
                                title: "حذف مهارت کامپیوتری",
                                text: "عملیات حذف با موفقیت انجام شد",
                                showConfirmButton: true,
                                confirmButtonText: "تائید"
                            });
                            $('#emp_skillComputer_tb').find('tr[id=sk_' + Id + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#emp_skillComputer_tb').DataTable();
                                table.row("#sk_" + Id).remove().draw(false);
                            });
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
//////////////////////////// بخش فعالیت های سازمان برای فرد استخدامی
function openModalForAddActivityOrgItems() {
    $.get("/Admin/ActivityOrgItems/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن فعالیت جدید ");
        $("#myModalBody").html(result);
    });

}
//
function InsertActivityOrgItems() {
    var frm = $("#ActivityItemsFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var emp_ComputerSkillItems = {
            Title: $("#Title").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/ActivityOrgItems/Create',
            data: emp_ComputerSkillItems,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                if (data == "ok") {
                    $("#myModal").modal('hide');
                    $("#viewAll").load("/Admin/ActivityOrgItems/ViewAll");

                    swal({

                        type: 'success',
                        title: "افزودن فعالیت سازمانی",
                        text: "فعالیت سازمانی با موفقیت ثبت شد",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });


                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function openModalForEditActivityOrgItems(Id) {
    $.get("/Admin/ActivityOrgItems/Edit/" + Id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش فعالیت سازمانی  ");
        $("#myModalBody").html(result);
    });

}
//
function UpdateActivityOrgItems() {
    var frm = $("#ActivityItemsFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var emp_ComputerSkillItems = {
            Item_ID: $("#Item_ID").val(),
            Title: $("#Title").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/ActivityOrgItems/Edit',
            data: emp_ComputerSkillItems,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                if (data == "ok") {
                    $("#myModal").modal('hide');
                    $("#viewAll").load("/Admin/ActivityOrgItems/ViewAll");
                    swal({

                        type: 'success',
                        title: "افزودن فعالیت سازمانی",
                        text: "فعالیت سازمانی با موفقیت ویرایش شد",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });



                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function DeleteActivityOrgItems(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این فعالیت سازمانی مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/ActivityOrgItems/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "invalid") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                type: 'warning',
                                title: "حذف فعالیت سازمانی",
                                text: "خطایی رخ داده است",
                                showConfirmButton: true,
                                confirmButtonText: "تائید"
                            });

                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                type: 'success',
                                title: "حذف فعالیت سازمانی",
                                text: "عملیات حذف با موفقیت انجام شد",
                                showConfirmButton: true,
                                confirmButtonText: "تائید"
                            });
                            $('#emp_activityOrg_tb').find('tr[id=ac_org_' + Id + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#emp_activityOrg_tb').DataTable();
                                table.row("#ac_org_" + Id).remove().draw(false);
                            });
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}

///////////////////////////////////////////////////// استخدام
function GetEmployementDetails(Id) {
    $.get("/Admin/Employements/ViewDetails/" + Id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("جزئیات اطلاعات پیشنهاد دهنده استخدام ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
    });

}

////
function GetEducationalRecordsDetails(Id) {
    $.get("/Admin/Employements/ViewDetailsEducational/" + Id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("سوابق تحصیلی و آموزشی ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
    });

}
////
function GetWorkExperienceDetails(Id) {
    $.get("/Admin/Employements/ViewDetailsWorkExperience/" + Id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("سوابق شغلی ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
    });

}
////
function GetTraningCoursesDetails(Id) {
    $.get("/Admin/Employements/ViewDetailsTraningCourses/" + Id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("دوره های آموزشی ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
    });

}
////
function GetGuarantorDetails(Id) {
    $.get("/Admin/Employements/ViewDetailsGrauntor/" + Id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افراد ضامن ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
    });

}
////
function GetUnderSuppervisorDetails(Id) {
    $.get("/Admin/Employements/ViewDetailsUnderSuppervisor/" + Id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افراد تحت تکفل ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
    });

}
////
function GetDocumentsDetails(Id) {
    $.get("/Admin/Employements/ViewDetailsDocuments/" + Id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("مدارک ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
    });

}
////
function DeleteEmployement(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این پیشنهاد دهنده مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Employements/DeleteEmployement',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "ok") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                type: 'success',
                                title: "حذف فرم استخدام",
                                text: "فرد پیشنهاد دهنده استخدام با موفقیت حذف شد",
                                showConfirmButton: true,
                                confirmButtonText: "تائید"
                            });
                            $('#employement_tb').find('tr[id=emp_' + Id + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#employement_tb').DataTable();
                                table.row("#emp_" + Id).remove().draw(false);
                            });
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
/////////////////////////////// SEO

function InsertOrUpdateSeo(pageId, pageType) {
    $("#loading").removeClass('hidden');


    $.ajax({
        method: 'POST',
        url: '/Admin/SeoPage/CreateOrEditContentsSeo',
        data: { id: pageId, type: pageType },
        success: function (data) {

            if (data == "add") {
                openModalForAddSeo(pageId);
            }
            else {
                openModalForEditSeo(pageId);
            }
        },
        error: function (result) {
        }
    });
}
///
function openModalForAddSeo() {
    $.get("/Admin/SeoPage/Create", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("سئوی سفحه");
        $("#myModalBody").html(result);
        //$("#PageId").val(id);
        $(".modal-dialog").css({ 'width': '70%' });
    });

}
////
function openModalForEditSeo(id, pageType) {
    $.ajax({
        method: 'GET',
        url: '/Admin/SeoPage/Edit',
        data: { id: id, type: pageType },
        success: function (data) {
            $("#myModal").modal();
            $("#myModalLabel").html("ویرایش اطلاعات سئو");
            $("#myModalBody").html(data);
            $("#PageId").val(id);
            $(".modal-dialog").css({ 'width': '70%' });
        },
        error: function (result) {
        }
    });
    //$.get("/Admin/SeoPage/Edit/" + id, function (result) {

    //    $("#myModal").modal();
    //    $("#myModalLabel").html("سئوی سفحه");
    //    $("#myModalBody").html(result);
    //    $("#PageId").val(id);
    //    $(".modal-dialog").css({ 'width': '70%' });
    //});

}
///////////////////////////////////////////// SeoPage

function openModalForAddSeoPage() {
    $.get("/Admin/SeoPage/Create", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن صفحه سئو");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });

}

function InsertSeoPage() {
    var frm = $("#seoFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');
        //var url=
        var seoPage = {
            MetaTitle: $("#MetaTitle").val(),
            MetaKeyword: $("#MetaKeyword").val(),
            MetaDescription: $("#MetaDescription").val(),
            MetaOther: $("#MetaOther").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/SeoPage/Create',
            data: seoPage,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                $("#myModal").modal('hide');
                swal({
                    title: "سئو",
                    text: "عملیات با موفقیت انجام شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });

                $("#seoFrm")[0].reset();
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function openModalForEditSeoPage(Id) {
    $.get("/Admin/SeoPage/Edit/" + Id, function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش صفحه سئو");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });

}
function UpdateSeoPage() {
    var frm = $("#seoFrmEdit").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');
        //var url=
        var seoPage = {
            SeoPage_ID: $("#SeoPage_ID").val(),
            MetaTitle: $("#MetaTitle").val(),
            MetaKeyword: $("#MetaKeyword").val(),
            MetaDescription: $("#MetaDescription").val(),
            MetaOther: $("#MetaOther").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/SeoPage/Edit',
            data: seoPage,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                $("#myModal").modal('hide');
                //$("#myModal").modal('hide');
                swal({
                    title: "سئو",
                    text: "عملیات با موفقیت انجام شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#seoFrmEdit")[0].reset();
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}


//function SearchVacation(st) {
//    var FromDate = $("#tarikhAlt1").val();
//    var ToDate = $("#tarikhAlt2").val();

//    $.ajax({
//        method: 'POST',
//        url: '/Admin/Vacation/Search',
//        data: {fromDate: FromDate, toDate: ToDate,status:st},
//        success: function (data) {
//            if (st == "1") {
//                $("#viewAll").html(data);
//            }
//            else if (st == "2") {
//                $("#viewAllFactoryManager").html(data);
//            }
//            else {
//                $("#viewAll2").html(data);
//            }
//        },
//        error: function (result) {
//        }
//    });
//}

function SearchMission(st) {
    var FromDate = $("#tarikhAlt1").val();
    var ToDate = $("#tarikhAlt2").val();
    console.log(FromDate);
    $.ajax({
        method: 'POST',
        url: '/Admin/MissionsRequest/Search',
        data: { fromDate: FromDate, toDate: ToDate, status: st },
        success: function (data) {
            if (st == "1") {
                $("#viewAll").html(data);
            }
            else if (st == "2") {
                $("#viewAllFactoryManager").html(data);
            }
            else {
                $("#viewAll2").html(data);
            }
        },
        error: function (result) {
        }
    });
}


///////////////////////////////////////////////// Seo Static Page

function GetAllSeoStaticPage() {

    $.ajax({
        method: 'POST',
        url: '/Admin/SeoStaticPages/ViewAll',
        data: '',
        success: function (data) {
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}

function openModalForAddSeoPageStatic() {
    $.get("/Admin/SeoStaticPages/Create", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن صفحه ثابت سئو");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });

}

function InsertSeoPageStatic() {
    var frm = $("#seoStaticPage_frm").valid();
    if (frm === true) {

        $("#loading").removeClass('hidden');
        //var url=
        var seoStaticPage = {
            MetaTitle: $("#MetaTitle").val(),
            MetaKeyword: $("#MetaKeyword").val(),
            MetaDescription: $("#MetaDescription").val(),
            MetaOther: $("#MetaOther").val(),
            PageId: $("#pageType_ddl").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/SeoStaticPages/Create',
            data: seoStaticPage,
            success: function (data) {
                if (data == "ok") {
                    swal({ title: "تبریک", text: "سئو صفحه با موفقیت ثبت شد", icon: "success", buttons: [false, "تایید"], });
                    $("#myModal").modal('hide');
                }
                else {
                    swal({
                        title: "هشدار !!!",
                        text: "این صفحه قبلا سئو شده آیا برای جایگزینی مطمئن هستید؟",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                        buttons: ["خیر", "بله"],
                    })
                        .then((willDelete) => {
                            if (willDelete) {
                                var seoStaticPage2 = {
                                    SeoPage_ID: data,
                                    MetaTitle: $("#MetaTitle").val(),
                                    MetaKeyword: $("#MetaKeyword").val(),
                                    MetaDescription: $("#MetaDescription").val(),
                                    MetaOther: $("#MetaOther").val(),
                                    PageId: $("#pageType_ddl").val()
                                }
                                UpdateAsyncSeoStaticPage(seoStaticPage2);
                            }
                        });
                }
                $("#loading").addClass('hidden');
                $("#viewAll").load("/Admin/SeoStaticPages/ViewAll/");
                //$("#seoFrm")[0].reset();
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function UpdateAsyncSeoStaticPage(seoStaticPage) {

    $.ajax({
        method: 'POST',
        url: '/Admin/SeoStaticPages/EditAsync',
        data: seoStaticPage,
        success: function (data) {
            $("#loading").addClass('hidden');
            $("#viewAll").load("/Admin/SeoStaticPages/ViewAll/");
            $("#myModal").modal('hide');
            swal({ title: "تبریک", text: "سئو صفحه با موفقیت ویرایش شد", icon: "success", buttons: [false, "تایید"], });
            //$("#seoFrm")[0].reset();
        },
        error: function (result) {
        }
    });

}


function openModalForEditSeoPageStatic(Id) {
    $.get("/Admin/SeoStaticPages/Edit/" + Id, function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش صفحه ثابت سئو");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });

}

function UpdateSeoPageStatic() {
    var frm = $("#seoStaticPageEdit_frm").valid();
    if (frm === true) {

        $("#loading").removeClass('hidden');
        //var url=
        var seoStaticPage = {
            SeoPage_ID: $("#SeoPage_ID").val(),
            MetaTitle: $("#MetaTitle").val(),
            MetaKeyword: $("#MetaKeyword").val(),
            MetaDescription: $("#MetaDescription").val(),
            MetaOther: $("#MetaOther").val(),
            PageId: $("#pageType_ddl").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/SeoStaticPages/Edit',
            data: seoStaticPage,
            success: function (data) {
                $("#loading").addClass('hidden');
                $("#viewAll").load("/Admin/SeoStaticPages/ViewAll/");
                $("#myModal").modal('hide');
                swal({ title: "تبریک", text: "سئو صفحه با موفقیت ویرایش شد", icon: "success", buttons: [false, "تایید"], });
                //$("#seoFrm")[0].reset();
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function checkboxSelectedForSeoPageStatic() {
    swal({
        title: "هشدار !!!",
        text: "آیا برای حذف موارد انتخاب شده مطمئن هستید؟",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ["خیر", "بله"],
    })
        .then((willDelete) => {
            if (willDelete) {
                var checkboxValues = "";
                $("input[class=forRemove]:checked").each(function () { // گرفتن ولیو های چکس باکس هایی که تیک خورده باشند
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");
                if (checkboxValues != "") {

                    $.ajax({
                        url: "/Admin/SeoStaticPages/checkboxSelected",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (result) {
                        for (var i = 0; i < cv.length - 1; i++) {
                            $("#sps_" + cv[i]).hide("slow");
                        }
                        swal({ title: "تبریک", text: "صفحات سئو با موفقیت حذف شد", icon: "success", buttons: [false, "تایید"], });
                    });
                }
                else {
                    swal({ title: "هشدار", text: "لطفا یک مورد را انتخاب نمایید !", icon: "warning", buttons: [false, "تایید"], });
                }
            }
        });

}

////============================================== vacation Alternate Reques
//ALtPersonID = '';
//function altVacationRequest(id) {
//    ALtPersonID = id;
//    $.get("/Admin/altVacationRequest/ViewAll/" + id, function (res) {
//        $("#viewAll").html(res);
//    });
//} 

//function openModalForEditaltVacation(id) {
//    $.get("/Admin/altVacationRequest/EditAltVacation/" + id, function (result) {
//        $("#myModal").modal();
//        $("#myModalLabel").html("مشاهده درخواست مرخصی ");
//        $("#myModalBody").html(result);
//        $(".modal-dialog").css({ 'width': '65%' });
//    });
//}

//function UpdateAltVacation() {
//    //var frm = $("#updateAltVacation_frm").valid();
//    //if (frm === true) {
//        $("#div-loading").removeClass('hidden');

//        var section = "";
//        if (vacationType1 == "True") {
//            section = $("#Section3").val();
//        }
//        else {
//            section = $("#Section4").val();
//        }
//        var vacation = {
//            VecationType: vacationType1,
//            PersonalCode_3: $("#PersonalCode_3").val(),
//            CreateDate: $("#CreateDate").val(),
//            StartTime: $("#StartTime_1").val(),
//            EndTime: $("#EndTime_1").val(),
//            Duration: $("#Duration_1").val(),
//            Vecation_ID: $("#Vecation_ID").val(),
//            Section: section,
//            AlternateSignature: $("#alternateListEdit_ddl").val(),
//            StatusAlternateSignature: $("#StatusAlternateSignature").val(),
//            AlternateSignatureReason: $("#AlternateSignatureReason").val()
//        }


//        $.ajax({
//            method: 'POST',
//            url: '/Admin/altVacationRequest/EditAltVacation',
//            data: { vacation: vacation, sdate: $("#tarikhAlt4").val(), eDate: $("#tarikhAlt5").val(), timeDate: $("#tarikhAlt6").val(), st: status1 },//
//            success: function (data) {
//                $("#div-loading").addClass('hidden');
//                if (data.status == "ok") {
//                    swal({
//                        type: 'success',
//                        title: "درخواست مرخصی",
//                        text: "درخواست شما با موفقیت ویرایش شد ",
//                        showConfirmButton: true,
//                        confirmButtonText: "تائید"
//                    });

//                    $("#viewAll").load("/Admin/altVacationRequest/ViewAll/" + ALtPersonID);
//                }
//            },
//            error: function (result) {
//            }
//        });
//    //}
//    //else {
//    //    return false;
//    //}
//}

////======================================================== VRsuperior

//function ViewAllVRSupriorRequest() {
//    debugger;
//    $.get("/Admin/VRsuperior/ViewAll/" , function (res) {
//        $("#viewAll").html(res);
//    });
//}

//function openModalForEditVRSupriorVacation(id) {
//    $.get("/Admin/VRsuperior/EditVRsuperiorVacation/" + id, function (result) {
//        $("#myModal").modal();
//        $("#myModalLabel").html(" ویرایش درخواست مرخصی ");
//        $("#myModalBody").html(result);
//        $(".modal-dialog").css({ 'width': '65%' });
//    });
//}

//function UpdateVRSupriorVacation() {
//    //var frm = $("#updateAltVacation_frm").valid();
//    //if (frm === true) {
//    $("#div-loading").removeClass('hidden');

//    var section = "";
//    if (vacationType1 == "True") {
//        section = $("#Section3").val();
//    }
//    else {
//        section = $("#Section4").val();
//    }
//    var vacation = {
//        VecationType: vacationType1,
//        CreateDate: $('#CreateDate').val(),
//        PersonalCode_3: $("#PersonalCode_3").val(),
//        StartTime: $("#StartTime_1").val(),
//        EndTime: $("#EndTime_1").val(),
//        Duration: $("#Duration_1").val(),
//        Vecation_ID: $("#Vecation_ID").val(),
//        Section: section,
//        AlternateSignature: $("#AlternateSignature").val(),
//        StatusAlternateSignature: $("#StatusAlternateSignature").val(),
//        //AlternateSignatureReason: $("#AlternateSignatureReason").val(),
//        StatusUnitsSupervisorSignature: $("#StatusUnitsSupervisorSignature").val(),
//        UnitsSupervisorReason: $("#UnitsSupervisorReason").val(),

//    }


//    $.ajax({
//        method: 'POST',
//        url: '/Admin/VRsuperior/EditVRsuperiorVacation',
//        data: { vacation: vacation, sdate: $("#tarikhAlt4").val(), eDate: $("#tarikhAlt5").val(), timeDate: $("#tarikhAlt6").val(), st: status1 },//
//        success: function (data) {
//            $("#div-loading").addClass('hidden');
//            if (data.status == "ok") {
//                swal({
//                    type: 'success',
//                    title: "درخواست مرخصی",
//                    text: "درخواست شما با موفقیت ویرایش شد ",
//                    showConfirmButton: true,
//                    confirmButtonText: "تائید"
//                });

//                $("#viewAll").load("/Admin/VRsuperior/ViewAll/");
//            }
//        },
//        error: function (result) {
//        }
//    });
//    //}
//    //else {
//    //    return false;
//    //}
//}

////================================================== VRHR

//function ViewAllVRHRRequest() {
//    debugger;
//    $.get("/Admin/VRHR/ViewAll/", function (res) {
//        $("#viewAll").html(res);
//    });
//}

//function openModalForEditVRHRVacation(id) {
//    $.get("/Admin/VRHR/EditVRHRVacation/" + id, function (result) {
//        $("#myModal").modal();
//        $("#myModalLabel").html("ویرایش درخواست مرخصی");
//        $("#myModalBody").html(result);
//        $(".modal-dialog").css({ 'width': '65%' });
//    });
//}

//function UpdateVRHRVacation() {
//    $("#div-loading").removeClass('hidden');

//    var section = "";
//    if (vacationType1 == "True") {
//        section = $("#Section3").val();
//    }
//    else {
//        section = $("#Section4").val();
//    }
//    var vacation = {
//        VecationType: vacationType1,
//        CreateDate: $('#CreateDate').val(),
//        PersonalCode_3: $("#PersonalCode_3").val(),
//        StartTime: $("#StartTime_1").val(),
//        EndTime: $("#EndTime_1").val(),
//        Duration: $("#Duration_1").val(),
//        Vecation_ID: $("#Vecation_ID").val(),
//        Section: section,
//        AlternateSignature: $("#AlternateSignature").val(),
//        StatusAlternateSignature: $("#StatusAlternateSignature").val(),
//        //AlternateSignatureReason: $("#AlternateSignatureReason").val(),
//        StatusUnitsSupervisorSignature: $("#StatusUnitsSupervisorSignature").val(),
//        UnitsSupervisorSignature: $("#UnitsSupervisorSignature").val(),
//        //UnitsSupervisorReason: $("#UnitsSupervisorReason").val(),
//        StatusAdministrativeAffairsSignature: $("#StatusAdministrativeAffairsSignature").val(),
//        AdministrativeAffairsReason: $("#AdministrativeAffairsReason").val()
//    }

//    $.ajax({
//        method: 'POST',
//        url: '/Admin/VRHR/EditVRsuperiorVacation',
//        data: { vacation: vacation, sdate: $("#tarikhAlt4").val(), eDate: $("#tarikhAlt5").val(), timeDate: $("#tarikhAlt6").val(), st: status1 },//
//        success: function (data) {
//            $("#div-loading").addClass('hidden');
//            if (data.status == "ok") {
//                swal({
//                    type: 'success',
//                    title: "درخواست مرخصی",
//                    text: "درخواست شما با موفقیت ویرایش شد ",
//                    showConfirmButton: true,
//                    confirmButtonText: "تائید"
//                });

//                $("#viewAll").load("/Admin/VRHR/ViewAll/");
//            }
//        },
//        error: function (result) {
//        }
//    });
//}

////================================================== VacationHRManager

//function ViewAllVacationHRManager() {
//    debugger;
//    $.get("/Admin/VacationHRManager/ViewAll/", function (res) {
//        $("#viewAll").html(res);
//    });
//}

//function openModalForEditVacationHRManager(id) {
//    $.get("/Admin/VacationHRManager/Edit/" + id, function (result) {
//        $("#myModal").modal();
//        $("#myModalLabel").html("ویرایش درخواست مرخصی");
//        $("#myModalBody").html(result);
//        $(".modal-dialog").css({ 'width': '65%' });
//    });
//}

//function UpdateHRVacation() {
//    $("#div-loading").removeClass('hidden');

//    var section = "";
//    if (vacationType1 == "True") {
//        section = $("#Section3").val();
//    }
//    else {
//        section = $("#Section4").val();
//    }
//    var vacation = {
//        VecationType: vacationType1,
//        CreateDate: $('#CreateDate').val(),
//        PersonalCode_3: $("#PersonalCode_3").val(),
//        StartTime: $("#StartTime_1").val(),
//        EndTime: $("#EndTime_1").val(),
//        Duration: $("#Duration_1").val(),
//        Vecation_ID: $("#Vecation_ID").val(),
//        Section: section,
//        AlternateSignature: $("#AlternateSignature").val(),
//        StatusAlternateSignature: $("#StatusAlternateSignature").val(),
//        //AlternateSignatureReason: $("#AlternateSignatureReason").val(),
//        StatusUnitsSupervisorSignature: $("#StatusUnitsSupervisorSignature").val(),
//        UnitsSupervisorSignature: $("#UnitsSupervisorSignature").val(),
//        //UnitsSupervisorReason: $("#UnitsSupervisorReason").val(),
//        StatusAdministrativeAffairsSignature: $("#StatusAdministrativeAffairsSignature").val(),
//        AdministrativeAffairsReason: $("#AdministrativeAffairsReason").val(),
//        AdministrativeAffairsSignature: $("#AdministrativeAffairsSignature").val(),
//        StatusHRManagerSignature: $("#StatusHRManagerSignature").val(),
//        HRManagerReason: $("#HRManagerReason").val()
//    }

//    $.ajax({
//        method: 'POST',
//        url: '/Admin/VacationHRManager/Edit',
//        data: { vacation: vacation, sdate: $("#tarikhAlt4").val(), eDate: $("#tarikhAlt5").val(), timeDate: $("#tarikhAlt6").val(), st: status1 },//
//        success: function (data) {
//            $("#div-loading").addClass('hidden');
//            if (data.status == "ok") {
//                swal({
//                    type: 'success',
//                    title: "درخواست مرخصی",
//                    text: "درخواست شما با موفقیت ویرایش شد ",
//                    showConfirmButton: true,
//                    confirmButtonText: "تائید"
//                });

//                $("#viewAll").load("/Admin/VacationHRManager/ViewAll/");
//            }
//        },
//        error: function (result) {
//        }
//    });
//}



//=============================================== Extra work

function openModalForAddExtraWork() {
    $.get("/Admin/ExtraWork/Create/", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ثبت درخواست اضافه کاری");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}
function AddExtraWork() {
    var frm = $("#AddExtraWork_frm").valid();
    if (frm === false) {
        return false;
    }
    if ($('#StartDate').val() == null || $('#StartDate').val() == '') {
        swal({
            type: 'warning',
            title: "خطا",
            text: 'تاریخ شروع اضافه کاری را وارد نمایید',
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        return false;
    }
    if ($('#EndDate').val() == null || $('#EndDate').val() == '') {
        swal({
            type: 'warning',
            title: "خطا",
            text: 'تاریخ پایان اضافه کاری را وارد نمایید',
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        return false;
    }
    if ($('#StartTime').val() == null || $('#StartTime').val() == '') {
        swal({
            type: 'warning',
            title: "خطا",
            text: 'ساعت شروع اضافه کاری را وارد نمایید',
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        return false;
    }
    if ($('#EndTime').val() == null || $('#EndTime').val() == '') {
        swal({
            type: 'warning',
            title: "خطا",
            text: 'ساعت پایان اضافه کاری را وارد نمایید',
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        return false;
    }
    $("#div-loading").removeClass('hidden');


    var ExtraWork = {
        PersonalCode: $('#PersonalCode').text(),
        StartDate: $("#StartDate").val(),
        EndDate: $("#EndDate").val(),
        StartTime: $("#StartTime").val(),
        EndTime: $("#EndTime").val(),
        ExtraWorktime: $("#ExtraWorktime").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/ExtraWork/Create',
        data: { ExtraWork: ExtraWork, sdate: $("#tarikhAlt1").val(), eDate: $("#tarikhAlt2").val() },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ثبت درخواست اضافه کاری",
                    text: "درخواست شما با موفقیت ثبت شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllExtraWorks();
            }
        },
        error: function (result) {
        }
    });
}

function ViewAllExtraWorks() {
    debugger;
    $.get("/Admin/ExtraWork/ViewAll/", function (res) {
        $("#viewAll").html(res);
    });
}


function openModalForEditExtraWork(id) {
    $.get("/Admin/ExtraWork/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست اضافه کاری");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}
function UpdateExtraWork() {
    var frm = $("#EditExtraWork_frm").valid();
    if (frm === false) {
        return false;
    }

    if ($('#StartDate').val() == null || $('#StartDate').val() == '') {
        swal({
            type: 'warning',
            title: "خطا",
            text: 'تاریخ شروع اضافه کاری را وارد نمایید',
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        return false;
    }
    if ($('#EndDate').val() == null || $('#EndDate').val() == '') {
        swal({
            type: 'warning',
            title: "خطا",
            text: 'تاریخ پایان اضافه کاری را وارد نمایید',
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        return false;
    }
    if ($('#StartTime').val() == null || $('#StartTime').val() == '') {
        swal({
            type: 'warning',
            title: "خطا",
            text: 'ساعت شروع اضافه کاری را وارد نمایید',
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        return false;
    }
    if ($('#EndTime').val() == null || $('#EndTime').val() == '') {
        swal({
            type: 'warning',
            title: "خطا",
            text: 'ساعت پایان اضافه کاری را وارد نمایید',
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        return false;
    }
    $("#div-loading").removeClass('hidden');


    var ExtraWork = {
        ExtraWorkID: $('#ExtraWorkID').val(),
        PersonalCode: $('#PersonalCode').text(),
        StartDate: $("#StartDate").val(),
        EndDate: $("#EndDate").val(),
        StartTime: $("#StartTime").val(),
        EndTime: $("#EndTime").val(),
        ExtraWorktime: $("#ExtraWorktime").val(),
        CreateDate: $("#CreateDate").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/ExtraWork/Edit',
        data: { ExtraWork: ExtraWork, sdate: $("#tarikhAlt1").val(), eDate: $("#tarikhAlt2").val() },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست اضافه کاری",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllExtraWorks();
            }
        },
        error: function (result) {
        }
    });
}

function DeleteExtraWork(id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این درخواست اطمینان دارید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/ExtraWork/Delete',
                    data: { id: id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }
                        if (data == 'ok') {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "اضافه کاری",
                                text: "درخواست مورد نظر شما با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });

                            ViewAllExtraWorks();
                        } else {
                            swal({
                                title: "اضافه کاری",
                                text: "درخواست مورد نظر شما با خطا مواجه گردید",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
//==================================================== Extra Work Superior

function ViewAllExtraWorksSuperior(filterList) {

    //$.get("/Admin/ExtraWorkSuperior/ViewAll/" + filterList, function (res) {
    //    $("#viewAll").html(res);
    //});
    $.ajax({
        method: 'POST',
        url: '/Admin/ExtraWorkSuperior/ViewAll',
        data: {
            id: filterList, ExtraWorkReason_txt: $('#ExtraWorkReason_txt').val().trim(),
            ApplicationUserFullName_txt: $('#ApplicationUserFullName_txt').val().trim(),
            SuperiorName_txt: $('#SuperiorName_txt').val().trim(),
            managerName_txt: $('#managerName_txt').val().trim(), HRManagerName_txt: $('#HRManagerName_txt').val().trim(),
            StartCreateDate: $('#StartCreateDate').val(), EndCreateDate: $('#EndCreateDate').val()
        },//
        success: function (data) {
            debugger;
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}

function ClearExtraWorksSuperiorFilters() {
    $('#ApplicationUserFullName_txt').val('');
    $('#AlternateName_txt').val('');
    $('#SuperiorName_txt').val('');
    $('#managerName_txt').val('');
    $('#HRManagerName_txt').val('');
    $('#StartCreateDate').val('');
    $('#StartCreateDate_txt').val('');
    $('#EndCreateDate').val('');
    $('#EndCreateDate_txt').val('');
}
function openModalForEditExtraWorkSuprior(id) {
    $.get("/Admin/ExtraWorkSuperior/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست اضافه کاری");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}
function UpdateExtraWorkSuperior() {
    $("#div-loading").removeClass('hidden');


    var ExtraWork = {
        ExtraWorkID: $('#ExtraWorkID').val(),
        //PersonalCode: $('#PersonalCode').text(),
        //StartDate: $("#StartDate").val(),
        //EndDate: $("#EndDate").val(),
        //StartTime: $("#StartTime").val(),
        //EndTime: $("#EndTime").val(),
        //ExtraWorktime: $("#ExtraWorktime").val(),
        //CreateDate: $("#CreateDate").val(),
        PersonalCode_superior: $("#PersonalCode_superior").val(),
        StatusSuperiorSignature: $("#StatusSuperiorSignature").val(),
        SuperiorReason: $("#SuperiorReason").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/ExtraWorkSuperior/Edit',
        data: { ExtraWork: ExtraWork },//, sdate: $("#tarikhAlt1").val(), eDate: $("#tarikhAlt2").val()
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست اضافه کاری",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllExtraWorksSuperior();
            }
        },
        error: function (result) {
        }
    });
}

//==================================================== Extra Work Manager

function ViewAllExtraWorksManager(filterList) {

    //$.get("/Admin/ExtraWorkManager/ViewAll/" + filterList, function (res) {
    //    $("#viewAll").html(res);
    //});
    $.ajax({
        method: 'POST',
        url: '/Admin/ExtraWorkManager/ViewAll',
        data: {
            id: filterList, ExtraWorkReason_txt: $('#ExtraWorkReason_txt').val().trim(),
            ApplicationUserFullName_txt: $('#ApplicationUserFullName_txt').val().trim(),
            SuperiorName_txt: $('#SuperiorName_txt').val().trim(),
            managerName_txt: $('#managerName_txt').val().trim(), HRManagerName_txt: $('#HRManagerName_txt').val().trim(),
            StartCreateDate: $('#StartCreateDate').val(), EndCreateDate: $('#EndCreateDate').val()
        },//
        success: function (data) {
            debugger;
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}

function ClearExtraWorksManagerFilters() {
    $('#ApplicationUserFullName_txt').val('');
    $('#AlternateName_txt').val('');
    $('#SuperiorName_txt').val('');
    $('#managerName_txt').val('');
    $('#HRManagerName_txt').val('');
    $('#StartCreateDate').val('');
    $('#StartCreateDate_txt').val('');
    $('#EndCreateDate').val('');
    $('#EndCreateDate_txt').val('');
}
function openModalForEditExtraWorkManager(id) {
    $.get("/Admin/ExtraWorkManager/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست اضافه کاری");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}
function UpdateExtraWorkManager() {
    $("#div-loading").removeClass('hidden');


    var ExtraWork = {
        ExtraWorkID: $('#ExtraWorkID').val(),
        //PersonalCode: $('#PersonalCode').text(),
        //StartDate: $("#StartDate").val(),
        //EndDate: $("#EndDate").val(),
        //StartTime: $("#StartTime").val(),
        //EndTime: $("#EndTime").val(),
        //ExtraWorktime: $("#ExtraWorktime").val(),
        //CreateDate: $("#CreateDate").val(),
        //PersonalCode_superior: $("#PersonalCode_superior").val(),
        //StatusSuperiorSignature: $("#StatusSuperiorSignature").val(),
        //SuperiorReason: $("#SuperiorReason").val(),
        PersonalCode_manager: $("#PersonalCode_manager").val(),
        StatusManagerSignature: $("#StatusManagerSignature").val(),
        ManagerReason: $("#ManagerReason").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/ExtraWorkManager/Edit',
        data: { ExtraWork: ExtraWork },//, sdate: $("#tarikhAlt1").val(), eDate: $("#tarikhAlt2").val()
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست اضافه کاری",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllExtraWorksManager();
            }
        },
        error: function (result) {
        }
    });
}

//==================================================== Extra Work HRManager

function ViewAllExtraWorksHRManager(filterList) {

    //$.get("/Admin/ExtraWorkHRManager/ViewAll/" + filterList, function (res) {
    //    $("#viewAll").html(res);
    //});
    $.ajax({
        method: 'POST',
        url: '/Admin/ExtraWorkHRManager/ViewAll',
        data: {
            id: filterList, ExtraWorkReason_txt: $('#ExtraWorkReason_txt').val().trim(),
            ApplicationUserFullName_txt: $('#ApplicationUserFullName_txt').val().trim(),
            SuperiorName_txt: $('#SuperiorName_txt').val().trim(),
            managerName_txt: $('#managerName_txt').val().trim(), HRManagerName_txt: $('#HRManagerName_txt').val().trim(),
            StartCreateDate: $('#StartCreateDate').val(), EndCreateDate: $('#EndCreateDate').val()
        },//
        success: function (data) {
            debugger;
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}

function ClearExtraWorkHRManagerFilters() {
    $('#ApplicationUserFullName_txt').val('');
    $('#AlternateName_txt').val('');
    $('#SuperiorName_txt').val('');
    $('#managerName_txt').val('');
    $('#HRManagerName_txt').val('');
    $('#StartCreateDate').val('');
    $('#StartCreateDate_txt').val('');
    $('#EndCreateDate').val('');
    $('#EndCreateDate_txt').val('');
}
function openModalForEditExtraWorkHRManager(id) {
    $.get("/Admin/ExtraWorkHRManager/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست اضافه کاری");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}
function UpdateExtraWorkHRManager() {
    $("#div-loading").removeClass('hidden');


    var ExtraWork = {
        ExtraWorkID: $('#ExtraWorkID').val(),
        //PersonalCode: $('#PersonalCode').text(),
        //StartDate: $("#StartDate").val(),
        //EndDate: $("#EndDate").val(),
        //StartTime: $("#StartTime").val(),
        //EndTime: $("#EndTime").val(),
        //ExtraWorktime: $("#ExtraWorktime").val(),
        //CreateDate: $("#CreateDate").val(),
        //PersonalCode_superior: $("#PersonalCode_superior").val(),
        //StatusSuperiorSignature: $("#StatusSuperiorSignature").val(),
        //SuperiorReason: $("#SuperiorReason").val(),
        //PersonalCode_manager: $("#PersonalCode_manager").val(),
        //StatusManagerSignature: $("#StatusManagerSignature").val(),
        //ManagerReason: $("#ManagerReason").val(),
        PersonalCode_HRmanager: $("#PersonalCode_HRmanager").val(),
        StatusHRManagerSignature: $("#StatusHRManagerSignature").val(),
        HRManagerReason: $("#HRManagerReason").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/ExtraWorkHRManager/Edit',
        data: { ExtraWork: ExtraWork, sdate: $("#tarikhAlt1").val(), eDate: $("#tarikhAlt2").val() },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست اضافه کاری",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllExtraWorksHRManager();
            }
        },
        error: function (result) {
        }
    });
}

//======================================= Loan
function openModalForAddLoan() {
    $.get("/Admin/Loan/Create/", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("درخواست وام");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function AddLoan() {
    $("#div-loading").removeClass('hidden');


    var loan = {
        PersonalCode: $('#PersonalCode').text(),
        loanType: $("#loanType").val(),
        loanReason: $("#loanReason").val(),
        loanReasonDescription: $("#loanReasonDescription").val(),
        amount: $("#amount").val(),
        Description: $("#Description").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/Loan/Create',
        data: { loan: loan },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ثبت درخواست وام",
                    text: "درخواست شما با موفقیت ثبت شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllLoans();
            }
        },
        error: function (result) {
        }
    });
}

function ViewAllLoans() {

    $.get("/Admin/Loan/ViewAll/", function (res) {
        $("#viewAll").html(res);
    });
}

function openModalForEditLoan(id) {
    $.get("/Admin/Loan/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست وام");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function UpdateLoan() {
    $("#div-loading").removeClass('hidden');

    var loan = {
        loanID: $('#loanID').val(),
        PersonalCode: $('#PersonalCode').text(),
        loanType: $("#loanType").val(),
        loanReason: $("#loanReason").val(),
        loanReasonDescription: $("#loanReasonDescription").val(),
        amount: $("#amount").val(),
        Description: $("#Description").val(),
        CreateDate: $("#CreateDate").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/Loan/Edit',
        data: { loan: loan },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست وام",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllLoans();
            }
        },
        error: function (result) {
        }
    });
}

function DeleteLoan(id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این درخواست اطمینان دارید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Loan/Delete',
                    data: { id: id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }
                        if (data == 'ok') {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "وام",
                                text: "درخواست مورد نظر شما با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });

                            ViewAllLoans();
                        } else {
                            swal({
                                title: "وام",
                                text: "درخواست مورد نظر شما با خطا مواجه گردید",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}

//========================================= Loan Superior
function ViewAllLoansSuperior(filterList) {

    //$.get("/Admin/LoanSuperior/ViewAll/" + filterList, function (res) {
    //    $("#viewAll").html(res);
    //});
    $.ajax({
        method: 'POST',
        url: '/Admin/LoanSuperior/ViewAll',
        data: {
            id: filterList, LoanPrice_txt: $('#LoanPrice_txt').val().trim(),
            ApplicationUserFullName_txt: $('#ApplicationUserFullName_txt').val().trim(),
            SuperiorName_txt: $('#SuperiorName_txt').val().trim(),
            managerName_txt: $('#managerName_txt').val().trim(), HRManagerName_txt: $('#HRManagerName_txt').val().trim(),
            StartCreateDate: $('#StartCreateDate').val(), EndCreateDate: $('#EndCreateDate').val()
        },//
        success: function (data) {
            debugger;
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}

function ClearLoansSuperiorFilters() {
    $('#ApplicationUserFullName_txt').val('');
    $('#AlternateName_txt').val('');
    $('#SuperiorName_txt').val('');
    $('#managerName_txt').val('');
    $('#HRManagerName_txt').val('');
    $('#StartCreateDate').val('');
    $('#StartCreateDate_txt').val('');
    $('#EndCreateDate').val('');
    $('#EndCreateDate_txt').val('');
}
function openModalForEditLoanSuperior(id) {
    $.get("/Admin/LoanSuperior/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست وام");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function UpdateLoanSuperior() {
    $("#div-loading").removeClass('hidden');

    var loan = {
        loanID: $('#loanID').val(),
        //PersonalCode: $('#PersonalCode').text(),
        //loanType: $("#loanType").val(),
        //loanReason: $("#loanReason").val(),
        //loanReasonDescription: $("#loanReasonDescription").val(),
        //amount: $("#amount").val(),
        //Description: $("#Description").val(),
        //CreateDate: $("#CreateDate").val(),
        PersonalCode_superior: $("#PersonalCode_superior").val(),
        StatusSuperiorSignature: $("#StatusSuperiorSignature").val(),
        SuperiorReason: $("#SuperiorReason").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/LoanSuperior/Edit',
        data: { loan: loan },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست وام",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllLoansSuperior();
            }
        },
        error: function (result) {
        }
    });
}

//========================================= Loan Manager
function ViewAllLoansManager(filterList) {

    //$.get("/Admin/LoanManager/ViewAll/" + filterList, function (res) {
    //    $("#viewAll").html(res);
    //});
    $.ajax({
        method: 'POST',
        url: '/Admin/LoanManager/ViewAll',
        data: {
            id: filterList, LoanPrice_txt: $('#LoanPrice_txt').val().trim(),
            ApplicationUserFullName_txt: $('#ApplicationUserFullName_txt').val().trim(),
            SuperiorName_txt: $('#SuperiorName_txt').val().trim(),
            managerName_txt: $('#managerName_txt').val().trim(), HRManagerName_txt: $('#HRManagerName_txt').val().trim(),
            StartCreateDate: $('#StartCreateDate').val(), EndCreateDate: $('#EndCreateDate').val()
        },//
        success: function (data) {
            debugger;
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}

function ClearLoansManagerFilters() {
    $('#ApplicationUserFullName_txt').val('');
    $('#AlternateName_txt').val('');
    $('#SuperiorName_txt').val('');
    $('#managerName_txt').val('');
    $('#HRManagerName_txt').val('');
    $('#StartCreateDate').val('');
    $('#StartCreateDate_txt').val('');
    $('#EndCreateDate').val('');
    $('#EndCreateDate_txt').val('');
}
function openModalForEditLoanManager(id) {
    $.get("/Admin/LoanManager/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست وام");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function UpdateLoanManager() {
    $("#div-loading").removeClass('hidden');

    var loan = {
        loanID: $('#loanID').val(),
        //PersonalCode: $('#PersonalCode').text(),
        //loanType: $("#loanType").val(),
        //loanReason: $("#loanReason").val(),
        //loanReasonDescription: $("#loanReasonDescription").val(),
        //amount: $("#amount").val(),
        //Description: $("#Description").val(),
        //CreateDate: $("#CreateDate").val(),
        //PersonalCode_superior: $("#PersonalCode_superior").val(),
        //StatusSuperiorSignature: $("#StatusSuperiorSignature").val(),
        //SuperiorReason: $("#SuperiorReason").val(),
        PersonalCode_manager: $("#PersonalCode_manager").val(),
        StatusManagerSignature: $("#StatusManagerSignature").val(),
        ManagerReason: $("#ManagerReason").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/LoanManager/Edit',
        data: { loan: loan },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست وام",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllLoansManager();
            }
        },
        error: function (result) {
        }
    });
}

//========================================= Loan HRManager
function ViewAllLoansHRManager(filterList) {

    //$.get("/Admin/LoanHRManager/ViewAll/" + filterList, function (res) {
    //    $("#viewAll").html(res);
    //});
    $.ajax({
        method: 'POST',
        url: '/Admin/LoanHRManager/ViewAll',
        data: {
            id: filterList,  LoanPrice_txt: $('#LoanPrice_txt').val().trim(),
            ApplicationUserFullName_txt: $('#ApplicationUserFullName_txt').val().trim(),
            SuperiorName_txt: $('#SuperiorName_txt').val().trim(),
            managerName_txt: $('#managerName_txt').val().trim(), HRManagerName_txt: $('#HRManagerName_txt').val().trim(),
            StartCreateDate: $('#StartCreateDate').val(), EndCreateDate: $('#EndCreateDate').val()
        },//
        success: function (data) {
            debugger;
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}

function ClearLoansHRManagerFilters() {
    $('#ApplicationUserFullName_txt').val('');
    $('#AlternateName_txt').val('');
    $('#SuperiorName_txt').val('');
    $('#managerName_txt').val('');
    $('#HRManagerName_txt').val('');
    $('#StartCreateDate').val('');
    $('#StartCreateDate_txt').val('');
    $('#EndCreateDate').val('');
    $('#EndCreateDate_txt').val('');
}
function openModalForEditLoanHRManager(id) {
    $.get("/Admin/LoanHRManager/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست وام");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function UpdateLoanHRManager() {
    $("#div-loading").removeClass('hidden');

    var loan = {
        loanID: $('#loanID').val(),
        //PersonalCode: $('#PersonalCode').text(),
        //loanType: $("#loanType").val(),
        //loanReason: $("#loanReason").val(),
        //loanReasonDescription: $("#loanReasonDescription").val(),
        //amount: $("#amount").val(),
        //Description: $("#Description").val(),
        //CreateDate: $("#CreateDate").val(),
        //PersonalCode_superior: $("#PersonalCode_superior").val(),
        //StatusSuperiorSignature: $("#StatusSuperiorSignature").val(),
        //SuperiorReason: $("#SuperiorReason").val(),
        //PersonalCode_manager: $("#PersonalCode_manager").val(),
        //StatusManagerSignature: $("#StatusManagerSignature").val(),
        //ManagerReason: $("#ManagerReason").val(),
        PersonalCode_HRmanager: $("#PersonalCode_HRmanager").val(),
        StatusHRManagerSignature: $("#StatusHRManagerSignature").val(),
        HRManagerReason: $("#HRManagerReason").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/LoanHRManager/Edit',
        data: { loan: loan },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست وام",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllLoansHRManager();
            }
        },
        error: function (result) {
        }
    });
}

//=============================================== Shift change

function openModalForAddShiftChange(id) {
    $.get("/Admin/ShiftChange/Create/", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ثبت درخواست تعویض شیفت");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function AddShiftChange() {
    $("#div-loading").removeClass('hidden');


    var ShiftChange = {
        PersonalCodeFirst: $('#PersonalCode').text(),
        UnitID_1: $("#UnitID_1").val(),
        PersonalCodeSecond: $("#alternateList_ddl").val(),
        shiftDate: $("#StartDate").val(),
        ShiftName1: $("#ShiftName1").val(),
        ShiftName2: $("#ShiftName2").val(),
        shiftReason: $("#shiftReason").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/ShiftChange/Create',
        data: { ShiftChange: ShiftChange, sdate: $("#tarikhAlt1").val() },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ثبت درخواست تعویض شیفت",
                    text: "درخواست شما با موفقیت ثبت شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllShiftChangesList();
            } else {
                swal({
                    type: 'warning',
                    title: "خطا در ثبت درخواست تعویض شیفت",
                    text: data.status,
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });
            }
        },
        error: function (result) {
        }
    });
}

function ViewAllShiftChangesList() {
    debugger;
    $.get("/Admin/ShiftChange/viewAll/", function (res) {
        $("#viewAll").html(res);
    });
}


function openModalForEditShiftChange(id) {
    $.get("/Admin/ShiftChange/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست تعویض شیفت");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}
function UpdateShiftChange() {
    $("#div-loading").removeClass('hidden');


    var ShiftChange = {
        ShiftChange_ID: $('#ShiftChange_ID').val(),
        CreateDate: $('#CreateDate').val(),
        PersonalCodeFirst: $('#PersonalCode').text(),
        UnitID_1: $("#UnitID_1").val(),
        PersonalCodeSecond: $("#alternateList_ddl").val(),
        shiftDate: $("#StartDate").val(),
        ShiftName1: $("#ShiftName1").val(),
        ShiftName2: $("#ShiftName2").val(),
        shiftReason: $("#shiftReason").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/ShiftChange/Edit',
        data: { ShiftChange: ShiftChange, sdate: $("#tarikhAlt1").val() },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست تعویض شیفت",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllShiftChangesList();
            }
        },
        error: function (result) {
        }
    });
}


function DeleteShiftChange(id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این درخواست اطمینان دارید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/ShiftChange/Delete',
                    data: { id: id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }
                        if (data == 'ok') {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "تعویض شیفت",
                                text: "درخواست مورد نظر شما با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });

                            ViewAllShiftChangesList();
                        } else {
                            swal({
                                title: "تعویض شیفت",
                                text: "درخواست مورد نظر شما با خطا مواجه گردید",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
//===================================== CoWorker Shift Change Request
ALtshcPersonID = '';
function ViewAllCoworkerShiftChangesList(id, filterList) {
    ALtshcPersonID = id;
    $.get("/Admin/ShiftCoworker/ViewAll?id=" + id + "&filterList=" + filterList, function (res) {
        $("#viewAll").html(res);
    });
}

function openModalForEditCoworkerShiftChange(id) {
    $.get("/Admin/ShiftCoworker/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست تعویض شیفت");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function UpdateCoworkerShiftChange() {
    $("#div-loading").removeClass('hidden');


    var ShiftChange = {
        ShiftChange_ID: $('#ShiftChange_ID').val(),
        PersonalCodeSecond: $("#alternateList_ddl").val(),
        StatusAlternateSignature: $("#StatusAlternateSignature").val(),
        AlternateSignatureReason: $("#AlternateSignatureReason").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/ShiftCoworker/Edit',
        data: { ShiftChange: ShiftChange },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست تعویض شیفت",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllCoworkerShiftChangesList(ALtshcPersonID);
            }
        },
        error: function (result) {
        }
    });
}

//===================================== Suprior Shift Change Request
function ViewAllSupriorShiftChangesList(filterList) {
    //$.get("/Admin/ShiftSuprior/ViewAll/" + filterList, function (res) {
    //    $("#viewAll").html(res);
    //});
    $.ajax({
        method: 'POST',
        url: '/Admin/ShiftSuprior/ViewAll',
        data: {
            id: filterList,
            ApplicationUserFullName_txt: $('#ApplicationUserFullName_txt').val().trim(),
            AlternateName_txt: $('#AlternateName_txt').val().trim(), SuperiorName_txt: $('#SuperiorName_txt').val().trim(),
            managerName_txt: $('#managerName_txt').val().trim(), HRManagerName_txt: $('#HRManagerName_txt').val().trim(),
            StartCreateDate: $('#StartCreateDate').val(), EndCreateDate: $('#EndCreateDate').val()
        },//
        success: function (data) {
            debugger;
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}

function ClearAllSupriorShiftChangesFilters() {
    $('#ApplicationUserFullName_txt').val('');
    $('#AlternateName_txt').val('');
    $('#SuperiorName_txt').val('');
    $('#managerName_txt').val('');
    $('#HRManagerName_txt').val('');
    $('#StartCreateDate').val('');
    $('#StartCreateDate_txt').val('');
    $('#EndCreateDate').val('');
    $('#EndCreateDate_txt').val('');
}
function openModalForEditSupriorShiftChange(id) {
    $.get("/Admin/ShiftSuprior/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست تعویض شیفت");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function UpdateSupriorShiftChange() {
    $("#div-loading").removeClass('hidden');


    var ShiftChange = {
        ShiftChange_ID: $('#ShiftChange_ID').val(),
        PersonalCode_superior: $("#PersonalCode_superior").val(),
        StatusSuperiorSignature: $("#StatusSuperiorSignature").val(),
        SuperiorReason: $("#SuperiorReason").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/ShiftSuprior/Edit',
        data: { ShiftChange: ShiftChange },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست تعویض شیفت",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllSupriorShiftChangesList();
            }
        },
        error: function (result) {
        }
    });
}

//===================================== Manager Shift Change Request
function ViewAllManagerShiftChangesList(filterList) {
    //$.get("/Admin/ShiftManager/ViewAll/" + filterList, function (res) {
    //    $("#viewAll").html(res);
    //});
    $.ajax({
        method: 'POST',
        url: '/Admin/ShiftManager/viewAll',
        data: {
            id: filterList,
            ApplicationUserFullName_txt: $('#ApplicationUserFullName_txt').val().trim(),
            AlternateName_txt: $('#AlternateName_txt').val().trim(), SuperiorName_txt: $('#SuperiorName_txt').val().trim(),
            managerName_txt: $('#managerName_txt').val().trim(), HRManagerName_txt: $('#HRManagerName_txt').val().trim(),
            StartCreateDate: $('#StartCreateDate').val(), EndCreateDate: $('#EndCreateDate').val()
        },//
        success: function (data) {
            debugger;
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}

function ClearManagerShiftChangesFilters() {
    $('#ApplicationUserFullName_txt').val('');
    $('#AlternateName_txt').val('');
    $('#SuperiorName_txt').val('');
    $('#managerName_txt').val('');
    $('#HRManagerName_txt').val('');
    $('#StartCreateDate').val('');
    $('#StartCreateDate_txt').val('');
    $('#EndCreateDate').val('');
    $('#EndCreateDate_txt').val('');
}
function openModalForEditManagerShiftChange(id) {
    $.get("/Admin/ShiftManager/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست تعویض شیفت");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function UpdateManagerShiftChange() {
    $("#div-loading").removeClass('hidden');


    var ShiftChange = {
        ShiftChange_ID: $('#ShiftChange_ID').val(),
        PersonalCode_manager: $("#PersonalCode_manager").val(),
        StatusManagerSignature: $("#StatusManagerSignature").val(),
        ManagerReason: $("#ManagerReason").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/ShiftManager/Edit',
        data: { ShiftChange: ShiftChange },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست تعویض شیفت",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllManagerShiftChangesList();
            }
        },
        error: function (result) {
        }
    });
}

//===================================== HRManager Shift Change Request
function ViewAllHRManagerShiftChangesList(filterList) {
    //$.get("/Admin/ShiftHRManager/ViewAll/" + filterList, function (res) {
    //    $("#viewAll").html(res);
    //});
    $.ajax({
        method: 'POST',
        url: '/Admin/ShiftHRManager/ViewAll',
        data: {
            id: filterList,
            ApplicationUserFullName_txt: $('#ApplicationUserFullName_txt').val().trim(),
            AlternateName_txt: $('#AlternateName_txt').val().trim(), SuperiorName_txt: $('#SuperiorName_txt').val().trim(),
            managerName_txt: $('#managerName_txt').val().trim(), HRManagerName_txt: $('#HRManagerName_txt').val().trim(),
            StartCreateDate: $('#StartCreateDate').val(), EndCreateDate: $('#EndCreateDate').val()
        },//
        success: function (data) {
            debugger;
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}

function ClearHRManagerShiftChangesFilters() {
    $('#ApplicationUserFullName_txt').val('');
    $('#AlternateName_txt').val('');
    $('#SuperiorName_txt').val('');
    $('#managerName_txt').val('');
    $('#HRManagerName_txt').val('');
    $('#StartCreateDate').val('');
    $('#StartCreateDate_txt').val('');
    $('#EndCreateDate').val('');
    $('#EndCreateDate_txt').val('');
}
function openModalForEditHRManagerShiftChange(id) {
    $.get("/Admin/ShiftHRManager/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست تعویض شیفت");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function UpdateHRManagerShiftChange() {
    $("#div-loading").removeClass('hidden');


    var ShiftChange = {
        ShiftChange_ID: $('#ShiftChange_ID').val(),
        PersonalCode_HRmanager: $("#PersonalCode_HRmanager").val(),
        StatusHRManagerSignature: $("#StatusHRManagerSignature").val(),
        HRManagerReason: $("#HRManagerReason").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/ShiftHRManager/Edit',
        data: { ShiftChange: ShiftChange },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست تعویض شیفت",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllHRManagerShiftChangesList();
            }
        },
        error: function (result) {
        }
    });
}

//========================================== Mission New
function openModalForAddMission() {
    $.get("/Admin/Mission/Create/", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ثبت درخواست ماموریت");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function ViewAllMissions() {
    debugger;
    $.get("/Admin/Mission/viewAll/", function (res) {
        $("#viewAll").html(res);
    });
}

function AddMission() {
    var frm = $("#mission_frm").valid();
    if (frm === false) {
        return false;
    }
    if ($('#MissionStart').val() == null || $('#MissionStart').val() == '') {
        swal({
            type: 'warning',
            title: "خطا",
            text: 'تاریخ شروع ماموریت را وارد نمایید',
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        return false;
    }
    if ($('#MissionEnd').val() == null || $('#MissionEnd').val() == '') {
        swal({
            type: 'warning',
            title: "خطا",
            text: 'تاریخ پایان ماموریت را وارد نمایید',
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        return false;
    }
    if ($('#MissionType').val() == 1) {
        if ($('#StartTime').val()==''){
            swal({
                type: 'warning',
                title: "خطا",
                text: 'ساعت شروع ماموریت را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        } else if ($('#EndTime').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'ساعت پایان ماموریت را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        }
    }

    $("#div-loading").removeClass('hidden');
    var mission = {
        PersonalCode: $('#PersonalCode').text(),
        MissionSubject: $("#MissionSubject").val(),
        MissionStart: $("#MissionStart").val(),
        MissionEnd: $("#MissionEnd").val(),
        Destination: $("#Destination").val(),
        MissionType: $("#MissionType").val(),
        Reason: $("#Reason").val(),
        Followers: $("#Followers").val(),
        StartTime: $("#StartTime").val(),
        EndTime: $("#EndTime").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/Mission/Create',
        data: { mission: mission, sdate: $("#tarikhAlt1").val(), edate: $("#tarikhAlt2").val() },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ثبت درخواست ماموریت",
                    text: "درخواست شما با موفقیت ثبت شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllMissions();
            } else {
                swal({
                    type: 'warning',
                    title: "خطا در ثبت درخواست ماموریت",
                    text: data.status,
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });
            }
        },
        error: function (result) {
        }
    });
}

function openModalForEditMission(id) {
    $.get("/Admin/Mission/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست ماموریت");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function UpdateMission() {
    var frm = $("#mission_frmEdit").valid();
    if (frm === false) {
        return false;
    }
    if ($('#MissionStart').val() == null || $('#MissionStart').val() == '') {
        swal({
            type: 'warning',
            title: "خطا",
            text: 'تاریخ شروع ماموریت را وارد نمایید',
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        return false;
    }
    if ($('#MissionEnd').val() == null || $('#MissionEnd').val() == '') {
        swal({
            type: 'warning',
            title: "خطا",
            text: 'تاریخ پایان ماموریت را وارد نمایید',
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        return false;
    }

    if ($('#MissionType').val() == 1) {
        if ($('#StartTime').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'ساعت شروع ماموریت را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        } else if ($('#EndTime').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'ساعت پایان ماموریت را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        }
    }

    $("#div-loading").removeClass('hidden');
    var mission = {
        Mission_ID: $('#Mission_ID').val(),
        //PersonalCode: $('#PersonalCode').text(),
        MissionSubject: $("#MissionSubject").val(),
        MissionStart: $("#MissionStart").val(),
        MissionEnd: $("#MissionEnd").val(),
        Destination: $("#Destination").val(),
        MissionType: $("#MissionType").val(),
        Reason: $("#Reason").val(),
        Followers: $("#Followers").val(),
        StartTime: $("#StartTime").val(),
        EndTime: $("#EndTime").val()
        //CreateDate: $("#CreateDate").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/Mission/Edit',
        data: { mission: mission, sdate: $("#tarikhAlt1").val(), edate: $("#tarikhAlt2").val() },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست ماموریت",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllMissions();
            } else {
                swal({
                    type: 'warning',
                    title: "خطا در ویرایش درخواست ماموریت",
                    text: data.status,
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });
            }
        },
        error: function (result) {
        }
    });
}

function DeleteMission(id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این درخواست اطمینان دارید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Mission/Delete',
                    data: { id: id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }
                        if (data == 'ok') {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "ماموریت",
                                text: "درخواست مورد نظر شما با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });

                            ViewAllMissions();
                        } else {
                            swal({
                                title: "ماموریت",
                                text: "درخواست مورد نظر شما با خطا مواجه گردید",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
//=========================================== Mission Superior

function ViewAllSuperiorMissions(filterList) {
    //debugger;
    //$.get("/Admin/MissionSuprior/viewAll/" + filterList, function (res) {
    //    $("#viewAll").html(res);
    //});
    $.ajax({
        method: 'POST',
        url: '/Admin/MissionSuprior/viewAll',
        data: {
            id: filterList, MissionSubject_txt: $('#MissionSubject_txt').val().trim(),
            ApplicationUserFullName_txt: $('#ApplicationUserFullName_txt').val().trim(),
            SuperiorName_txt: $('#SuperiorName_txt').val().trim(),
            managerName_txt: $('#managerName_txt').val().trim(), HRManagerName_txt: $('#HRManagerName_txt').val().trim(),
            StartCreateDate: $('#StartCreateDate').val(), EndCreateDate: $('#EndCreateDate').val()
        },//
        success: function (data) {
            debugger;
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}

function ClearSuperiorMissionsFilters() {
    $('#ApplicationUserFullName_txt').val('');
    $('#AlternateName_txt').val('');
    $('#SuperiorName_txt').val('');
    $('#managerName_txt').val('');
    $('#HRManagerName_txt').val('');
    $('#StartCreateDate').val('');
    $('#StartCreateDate_txt').val('');
    $('#EndCreateDate').val('');
    $('#EndCreateDate_txt').val('');
}
function openModalForEditSuperiorMission(id) {
    $.get("/Admin/MissionSuprior/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست ماموریت");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function UpdateSuperiorMission() {
    $("#div-loading").removeClass('hidden');
    var mission = {
        Mission_ID: $('#Mission_ID').val(),
        PersonalCode_superior: $('#PersonalCode_superior').val(),
        StatusSuperiorSignature: $("#StatusSuperiorSignature").val(),
        SuperiorReason: $("#SuperiorReason").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/MissionSuprior/Edit',
        data: { mission: mission },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست ماموریت",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllSuperiorMissions();
            } else {
                swal({
                    type: 'warning',
                    title: "خطا در ویرایش درخواست ماموریت",
                    text: data.status,
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });
            }
        },
        error: function (result) {
        }
    });
}

//=========================================== Mission Manager

function ViewAllManagerMissions(filterList) {
    //debugger;
    //$.get("/Admin/MissionManager/viewAll/" + filterList, function (res) {
    //    $("#viewAll").html(res);
    //});
    $.ajax({
        method: 'POST',
        url: '/Admin/MissionManager/viewAll',
        data: {
            id: filterList, MissionSubject_txt: $('#MissionSubject_txt').val().trim(),
            ApplicationUserFullName_txt: $('#ApplicationUserFullName_txt').val().trim(),
            SuperiorName_txt: $('#SuperiorName_txt').val().trim(),
            managerName_txt: $('#managerName_txt').val().trim(), HRManagerName_txt: $('#HRManagerName_txt').val().trim(),
            StartCreateDate: $('#StartCreateDate').val(), EndCreateDate: $('#EndCreateDate').val()
        },//
        success: function (data) {
            debugger;
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}
function ClearManagerMissionsFilters() {
    $('#ApplicationUserFullName_txt').val('');
    $('#AlternateName_txt').val('');
    $('#SuperiorName_txt').val('');
    $('#managerName_txt').val('');
    $('#HRManagerName_txt').val('');
    $('#StartCreateDate').val('');
    $('#StartCreateDate_txt').val('');
    $('#EndCreateDate').val('');
    $('#EndCreateDate_txt').val('');
}
function openModalForEditManagerMission(id) {
    $.get("/Admin/MissionManager/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست ماموریت");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function UpdateManagerMission() {
    $("#div-loading").removeClass('hidden');
    var mission = {
        Mission_ID: $('#Mission_ID').val(),
        PersonalCode_manager: $('#PersonalCode_manager').val(),
        StatusManagerSignature: $("#StatusManagerSignature").val(),
        ManagerReason: $("#ManagerReason").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/MissionManager/Edit',
        data: { mission: mission },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست ماموریت",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllManagerMissions();
            } else {
                swal({
                    type: 'warning',
                    title: "خطا در ویرایش درخواست ماموریت",
                    text: data.status,
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });
            }
        },
        error: function (result) {
        }
    });
}

//=========================================== Mission HRManager

function ViewAllHRManagerMissions(filterList) {
    //debugger;
    //$.get("/Admin/MissionHRManager/viewAll/" + filterList, function (res) {
    //    $("#viewAll").html(res);
    //});
    $.ajax({
        method: 'POST',
        url: '/Admin/MissionHRManager/viewAll',
        data: {
            id: filterList, MissionSubject_txt: $('#MissionSubject_txt').val().trim(),
            ApplicationUserFullName_txt: $('#ApplicationUserFullName_txt').val().trim(),
            SuperiorName_txt: $('#SuperiorName_txt').val().trim(),
            managerName_txt: $('#managerName_txt').val().trim(), HRManagerName_txt: $('#HRManagerName_txt').val().trim(),
            StartCreateDate: $('#StartCreateDate').val(), EndCreateDate: $('#EndCreateDate').val()
        },//
        success: function (data) {
            debugger;
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}

function ClearHRManagerMissionsFilters() {
    $('#ApplicationUserFullName_txt').val('');
    $('#AlternateName_txt').val('');
    $('#SuperiorName_txt').val('');
    $('#managerName_txt').val('');
    $('#HRManagerName_txt').val('');
    $('#StartCreateDate').val('');
    $('#StartCreateDate_txt').val('');
    $('#EndCreateDate').val('');
    $('#EndCreateDate_txt').val('');
}
function openModalForEditHRManagerMission(id) {
    $.get("/Admin/MissionHRManager/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست ماموریت");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function UpdateHRManagerMission() {
    $("#div-loading").removeClass('hidden');
    var mission = {
        Mission_ID: $('#Mission_ID').val(),
        PersonalCode_HRmanager: $('#PersonalCode_HRmanager').val(),
        StatusHRManagerSignature: $("#StatusHRManagerSignature").val(),
        HRManagerReason: $("#HRManagerReason").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/MissionHRManager/Edit',
        data: { mission: mission },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست ماموریت",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllHRManagerMissions();
            } else {
                swal({
                    type: 'warning',
                    title: "خطا در ویرایش درخواست ماموریت",
                    text: data.status,
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });
            }
        },
        error: function (result) {
        }
    });
}

//=================================== vacation New
function openModalForAddVacationNew() {
    $.get("/Admin/Vacationnew/Create/", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ثبت درخواست مرخصی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function AddVacation() {
    var frm = $("#AddVacation_frm").valid();
    if (frm === false) {
        return false;
    }
    if ($('#VacationType').val()=='true') {
        if ($('#StartDate').val() == null || $('#StartDate').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'تاریخ شروع مرخصی روزانه را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        }
        if ($('#EndDate').val() == null || $('#EndDate').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'تاریخ پایان مرخصی روزانه را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        }
    }
    else {


        if ($('#VacationTimeDate').val() == null || $('#VacationTimeDate').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'تاریخ مرخصی ساعتی را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        }
        if ($('#StartTime').val() == null || $('#StartTime').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'ساعت شروع مرخصی ساعتی را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        }
        if ($('#EndTime').val() == null || $('#EndTime').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'ساعت پایان مرخصی ساعتی را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        }
    }

    $("#div-loading").removeClass('hidden');

    var vacation = {
        PersonalCodeFirst: $('#PersonalCode').text(),
        VacationType: $("#VacationType").val(),
        PersonalCodeSecond: $("#alternateList_ddl").val(),
        DayVacationStartDate: $("#StartDate").val(),
        DayVacationEndDate: $("#EndDate").val(),
        Duration: $("#Duration").val(),
        DateVacationTime: $("#VacationTimeDate").val(),
        StartTime: $("#StartTime").val(),
        EndTime: $("#EndTime").val(),
        VacationReason: $("#VacationReason").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/Vacationnew/Create',
        data: { vacation: vacation, sdate: $("#tarikhAlt1").val(), edate: $("#tarikhAlt2").val(), vtdate: $("#tarikhAlt3").val() },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ثبت درخواست مرخصی",
                    text: "درخواست شما با موفقیت ثبت شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllVacationList();
            } else {
                swal({
                    type: 'warning',
                    title: "خطا در ثبت درخواست مرخصی",
                    text: data.status,
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });
            }
        },
        error: function (result) {
        }
    });
}


function ViewAllVacationList() {
    debugger;
    $.get("/Admin/Vacationnew/viewAll/", function (res) {
        $("#viewAll").html(res);
    });
}


function openModalForEditVacation(id) {
    $.get("/Admin/Vacationnew/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست مرخصی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}
function UpdateVacation() {
    var frm = $("#EditVacation_frm").valid();
    if (frm === false) {
        return false;
    }
    if ($('#VacationType').val() == 'true') {
        if ($('#StartDate').val() == null || $('#StartDate').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'تاریخ شروع مرخصی روزانه را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        }
        if ($('#EndDate').val() == null || $('#EndDate').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'تاریخ پایان مرخصی روزانه را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        }
    }
    else {


        if ($('#VacationTimeDate').val() == null || $('#VacationTimeDate').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'تاریخ مرخصی ساعتی را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        }
        if ($('#StartTime').val() == null || $('#StartTime').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'ساعت شروع مرخصی ساعتی را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        }
        if ($('#EndTime').val() == null || $('#EndTime').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'ساعت پایان مرخصی ساعتی را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        }
    }

    $("#div-loading").removeClass('hidden');


    var vacation = {
        Vacation_ID: $('#Vacation_ID').val(),
        VacationType: $("#VacationType").val(),
        PersonalCodeSecond: $("#alternateList_ddl").val(),
        DayVacationStartDate: $("#StartDate").val(),
        DayVacationEndDate: $("#EndDate").val(),
        Duration: $("#Duration").val(),
        DateVacationTime: $("#VacationTimeDate").val(),
        StartTime: $("#StartTime").val(),
        EndTime: $("#EndTime").val(),
        VacationReason: $("#VacationReason").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/Vacationnew/Edit',
        data: { vacation: vacation, sdate: $("#tarikhAlt1").val(), edate: $("#tarikhAlt2").val(), vtdate: $("#tarikhAlt3").val() },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست مرخصی",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllVacationList();
            }
        },
        error: function (result) {
        }
    });
}

function DeleteVacation(id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این درخواست اطمینان دارید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Vacationnew/Delete',
                    data: { id: id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }
                        if (data == 'ok') {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "مرخصی",
                                text: "درخواست مورد نظر شما با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });

                            ViewAllVacationList();
                        } else {
                            swal({
                                title: "مرخصی",
                                text: "درخواست مورد نظر شما با خطا مواجه گردید",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}

//========================================= VacationCoworkerNew 

function ViewAllVacationCoworkerList(id, filterList) {
    debugger;
    $.get("/Admin/VacationCoworkernew/viewAll?id=" + id + "&filterList=" + filterList, function (res) {
        $("#viewAll").html(res);
    });
}

function openModalForEditVacationCoworker(id) {
    $.get("/Admin/VacationCoworkernew/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست مرخصی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function UpdateVacationCoworker() {
    $("#div-loading").removeClass('hidden');


    var vacation = {
        Vacation_ID: $('#Vacation_ID').val(),
        StatusAlternateSignature: $("#StatusAlternateSignature").val(),
        AlternateSignatureReason: $("#AlternateSignatureReason").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/VacationCoworkernew/Edit',
        data: { vacation: vacation, sdate: $("#tarikhAlt1").val(), edate: $("#tarikhAlt2").val(), vtdate: $("#tarikhAlt3").val() },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست مرخصی",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllVacationCoworkerList($("#alternateList_ddl").val());
            }
        },
        error: function (result) {
        }
    });
}

//========================================= VacationSuperiorNew 

function ViewAllVacationSuperiorList(filterList) {
    //debugger;
    //$.get("/Admin/VacationSuperiornew/viewAll/" + filterList, function (res) {
    //    $("#viewAll").html(res);
    //});
    $.ajax({
        method: 'POST',
        url: '/Admin/VacationSuperiornew/viewAll',
        data: {
            id: filterList,
            ApplicationUserFullName_txt: $('#ApplicationUserFullName_txt').val().trim(),
            AlternateName_txt: $('#AlternateName_txt').val().trim(), SuperiorName_txt: $('#SuperiorName_txt').val().trim(),
            managerName_txt: $('#managerName_txt').val().trim(), HRManagerName_txt: $('#HRManagerName_txt').val().trim(),
            StartCreateDate: $('#StartCreateDate').val(), EndCreateDate: $('#EndCreateDate').val()
        },//
        success: function (data) {
            debugger;
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}

function ClearVacationSuperiornewFilters() {
    $('#ApplicationUserFullName_txt').val('');
    $('#AlternateName_txt').val('');
    $('#SuperiorName_txt').val('');
    $('#managerName_txt').val('');
    $('#HRManagerName_txt').val('');
    $('#StartCreateDate').val('');
    $('#StartCreateDate_txt').val('');
    $('#EndCreateDate').val('');
    $('#EndCreateDate_txt').val('');
}

function openModalForEditVacationSuperior(id) {
    $.get("/Admin/VacationSuperiornew/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست مرخصی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function UpdateVacationSuperior() {
    $("#div-loading").removeClass('hidden');


    var vacation = {
        Vacation_ID: $('#Vacation_ID').val(),
        PersonalCode_superior: $("#PersonalCode_superior").val(),
        StatusSuperiorSignature: $("#StatusSuperiorSignature").val(),
        SuperiorReason: $("#SuperiorReason").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/VacationSuperiornew/Edit',
        data: { vacation: vacation },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست مرخصی",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllVacationSuperiorList();
            }
        },
        error: function (result) {
        }
    });
}

//========================================= VacationManagerNew 

function ViewAllVacationManagerList(filterList) {
    //$.get("/Admin/VacationManagernew/viewAll/" + filterList, function (res) {
    //    $("#viewAll").html(res);
    //});
    $.ajax({
        method: 'POST',
        url: '/Admin/VacationManagernew/viewAll',
        data: {
            id: filterList,
            ApplicationUserFullName_txt: $('#ApplicationUserFullName_txt').val().trim(),
            AlternateName_txt: $('#AlternateName_txt').val().trim(), SuperiorName_txt: $('#SuperiorName_txt').val().trim(),
            managerName_txt: $('#managerName_txt').val().trim(), HRManagerName_txt: $('#HRManagerName_txt').val().trim(),
            StartCreateDate: $('#StartCreateDate').val(), EndCreateDate: $('#EndCreateDate').val()
        },//
        success: function (data) {
            debugger;
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}

function ClearVacationManagerFilters() {
    $('#ApplicationUserFullName_txt').val('');
    $('#AlternateName_txt').val('');
    $('#SuperiorName_txt').val('');
    $('#managerName_txt').val('');
    $('#HRManagerName_txt').val('');
    $('#StartCreateDate').val('');
    $('#StartCreateDate_txt').val('');
    $('#EndCreateDate').val('');
    $('#EndCreateDate_txt').val('');
}

function openModalForEditVacationManager(id) {
    $.get("/Admin/VacationManagernew/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست مرخصی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function UpdateVacationManager() {
    $("#div-loading").removeClass('hidden');


    var vacation = {
        Vacation_ID: $('#Vacation_ID').val(),
        PersonalCode_manager: $("#PersonalCode_manager").val(),
        StatusManagerSignature: $("#StatusManagerSignature").val(),
        ManagerReason: $("#ManagerReason").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/VacationManagernew/Edit',
        data: { vacation: vacation },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست مرخصی",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllVacationManagerList();
            }
        },
        error: function (result) {
        }
    });
}

//========================================= VacationHRManagerNew 

function ViewAllVacationHRManagerList(filterList) {
    //debugger;
    //$.get("/Admin/VacationHRManagernew/viewAll/" + filterList, function (res) {
    //    $("#viewAll").html(res);
    //});
    $.ajax({
        method: 'POST',
        url: '/Admin/VacationHRManagernew/viewAll',
        data: {
            id: filterList,
            ApplicationUserFullName_txt: $('#ApplicationUserFullName_txt').val().trim(), 
            AlternateName_txt: $('#AlternateName_txt').val().trim(), SuperiorName_txt: $('#SuperiorName_txt').val().trim(),
            managerName_txt: $('#managerName_txt').val().trim(), HRManagerName_txt: $('#HRManagerName_txt').val().trim(),
            StartCreateDate: $('#StartCreateDate').val(), EndCreateDate: $('#EndCreateDate').val()
        },//
        success: function (data) {
            debugger;
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}

function ClearVacationHRManagerFilters() {
    $('#ApplicationUserFullName_txt').val('');
    $('#AlternateName_txt').val('');
    $('#SuperiorName_txt').val('');
    $('#managerName_txt').val('');
    $('#HRManagerName_txt').val('');
    $('#StartCreateDate').val('');
    $('#StartCreateDate_txt').val('');
    $('#EndCreateDate').val('');
    $('#EndCreateDate_txt').val('');
}

function openModalForEditVacationHRManager(id) {
    $.get("/Admin/VacationHRManagernew/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست مرخصی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function UpdateVacationHRManager() {
    $("#div-loading").removeClass('hidden');


    var vacation = {
        Vacation_ID: $('#Vacation_ID').val(),
        PersonalCode_HRmanager: $("#PersonalCode_HRmanager").val(),
        StatusHRManagerSignature: $("#StatusHRManagerSignature").val(),
        HRManagerReason: $("#HRManagerReason").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/VacationHRManagernew/Edit',
        data: { vacation: vacation },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست مرخصی",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllVacationHRManagerList();
            }
        },
        error: function (result) {
        }
    });
}

//================================ مرخصی ویژه
function openModalForAddVacationNew2() {
    $.get("/Admin/Vacation/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ثبت درخواست مرخصی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function AddVacation2() {
    var frm = $("#AddVacation_frm").valid();
    if (frm === false) {
        return false;
    }
    if ($('#VacationType').val() == 'true') {
        if ($('#StartDate').val() == null || $('#StartDate').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'تاریخ شروع مرخصی روزانه را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        }
        if ($('#EndDate').val() == null || $('#EndDate').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'تاریخ پایان مرخصی روزانه را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        }
    }
    else {
        if ($('#VacationTimeDate').val() == null || $('#VacationTimeDate').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'تاریخ مرخصی ساعتی را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        }
        if ($('#StartTime').val() == null || $('#StartTime').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'ساعت شروع مرخصی ساعتی را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        }
        if ($('#EndTime').val() == null || $('#EndTime').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'ساعت پایان مرخصی ساعتی را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        }
    }

    $("#div-loading").removeClass('d-none');
    var vr = '';
    if ($('#reseonOption_ddl').val() == 'سایر') {
        vr = $("#VacationReason").val()
    } else {
        vr = $('#reseonOption_ddl').val();
    }
    var vacation = {
        PersonalCodeFirst: $('#PersonalCode').text(),
        VacationType: $("#VacationType").val(),
        PersonalCodeSecond: $("#alternateList_ddl").val(),
        DayVacationStartDate: $("#StartDate").val(),
        DayVacationEndDate: $("#EndDate").val(),
        Duration: $("#Duration").val(),
        DateVacationTime: $("#VacationTimeDate").val(),
        StartTime: $("#StartTime").val(),
        EndTime: $("#EndTime").val(),
        VacationReason: vr
    }
    $.ajax({
        method: 'POST',
        url: '/Admin/Vacation/Create',
        data: { vacation: vacation, sdate: $("#tarikhAlt1").val(), edate: $("#tarikhAlt2").val(), vtdate: $("#tarikhAlt3").val() },//
        success: function (data) {
            $("#div-loading").addClass('d-none');
            if (data.status) {
                swal({
                    type: 'success',
                    title: "ثبت درخواست مرخصی",
                    text: "درخواست شما با موفقیت ثبت شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });
                $('#insert-modal').modal('toggle');

                ViewAllVacationList2();
            } else {
                swal({
                    type: 'warning',
                    title: "خطا در ثبت درخواست مرخصی",
                    text: data.message,
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });
            }
        },
        error: function (result) {
        }
    });
}


function ViewAllVacationList2() {
    debugger;
    //$.get("/Admin/Vacation/viewAll", function (res) {
    //    $("#viewAll").html(res);
    //});
    //loadingPageShow();
    $("#vacation_tb").DataTable({

        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,
        "ajax": {
            "url": "/Admin/Vacation/viewAll",
            "type": "POST",
            "datatype": "json",
            "data": {
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },

        "columns": [
            //این فیلد row خالی هست و صرفا برای این قرار داده شده تا کد انتهای همین تابع مقادیر شماره سطر را داخل آن نمایش دهد.
            { "data": "row", "name": "row" },
            { "data": "createDate", "name": "createDate" },
            {
                data: "vacationType",
                render: function (data) {
                    if (data) {
                        return '<span>روزانه</span>';
                    }
                    else {
                        return '<span>ساعتی</span>';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    if (row.vacationType) {
                        return '<span>' + row.dayVacationStartDate + '</span>';
                    } else {
                        return '<span>' + row.dateVacationTime + '</span> <span>' + row.startTime + '</span>';
                    }
                }
            },
            {
                mRender: function (data, type, row) {
                    if (row.vacationType) {
                        return '<span>' + row.dayVacationEndDate + '</span>';
                    } else {
                        return '<span>' + row.dateVacationTime + '</span> <span>' + row.endTime + '</span>';
                    }
                }
            },
            {
                data: "finalStatus",
                render: function (data) {
                    if (data == null) {
                        return '<span class="p-2 status_">نا مشخص</span>';
                    }
                    else if (data == true) {
                        return '<span class="p-2 status_True">تایید شده</span>';
                    }
                    else {
                        return '<span class="p-2 status_False">تایید نشده</span>';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
console.log('alert');                    
console.log(row);                    
return '<button type="button" class="btn btn-icon btn-primary btnIcon" title="ویرایش" onclick="openModalForEditVacation2(' + row.vacation_ID + ')"><i class="fa fa-edit"></i></button>' +
                        '<button type="button" class="btn btn-icon btn-danger btnIcon" title = "حذف" onclick = "DeleteVacation2(' + row.vacation_ID + ');" > <i class="fa fa-trash"></i> </button >';
                }
            },
        ],
        //کد مربوط به نمایش شماره سطر
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $("td:first", nRow).html(iDisplayIndex + 1);
            return nRow;
        },

    });
    //loadingPageHide();
}


function openModalForEditVacation2(id) {
    $.get("/Admin/Vacation/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست مرخصی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function UpdateVacation2() {
    var frm = $("#EditVacation_frm").valid();
    if (frm === false) {
        return false;
    }
    if ($('#VacationType').val() == 'true') {
        if ($('#StartDate').val() == null || $('#StartDate').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'تاریخ شروع مرخصی روزانه را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        }
        if ($('#EndDate').val() == null || $('#EndDate').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'تاریخ پایان مرخصی روزانه را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        }
    }
    else {


        if ($('#VacationTimeDate').val() == null || $('#VacationTimeDate').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'تاریخ مرخصی ساعتی را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        }
        if ($('#StartTime').val() == null || $('#StartTime').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'ساعت شروع مرخصی ساعتی را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        }
        if ($('#EndTime').val() == null || $('#EndTime').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'ساعت پایان مرخصی ساعتی را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        }
    }

    $("#div-loading").removeClass('d-none');

    var vr = '';
    if ($('#reseonOption_ddl').val() == 'سایر') {
        vr = $("#VacationReason").val()
    } else {
        vr = $('#reseonOption_ddl').val();
    }
    var vacation = {
        Vacation_ID: $('#Vacation_ID').val(),
        VacationType: $("#VacationType").val(),
        PersonalCodeSecond: $("#alternateList_ddl").val(),
        DayVacationStartDate: $("#StartDate").val(),
        DayVacationEndDate: $("#EndDate").val(),
        Duration: $("#Duration").val(),
        DateVacationTime: $("#VacationTimeDate").val(),
        StartTime: $("#StartTime").val(),
        EndTime: $("#EndTime").val(),
        VacationReason: vr
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/Vacation/Edit',
        data: { vacation: vacation, sdate: $("#tarikhAlt1").val(), edate: $("#tarikhAlt2").val(), vtdate: $("#tarikhAlt3").val() },//
        success: function (data) {
            $("#div-loading").addClass('d-none');
            if (data.status) {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست مرخصی",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });
                $('#edit-modal').modal('toggle');
                ViewAllVacationList2();
            } else {
                swal({
                    type: 'warning',
                    title: "خطا در ثبت درخواست مرخصی",
                    text: data.message,
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });
            }
        },
        error: function (result) {
        }
    });
}

function DeleteVacation2(id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این درخواست اطمینان دارید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('d-none');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Vacation/Delete',
                    data: { id: id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }
                        if (data == 'ok') {
                            $("#div-loading").addClass('d-none');
                            swal({
                                title: "مرخصی",
                                text: "درخواست مورد نظر شما با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });

                            ViewAllVacationList2();
                        }
                        else if (data == "notAllowed") {
                            swal({
                                title: "مرخصی",
                                text: "به علت تغییر وضعیت درخواست توسط مافوق حذف امکان پذیر نیست",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                        else {
                            swal({
                                title: "مرخصی",
                                text: "درخواست مورد نظر شما با خطا مواجه گردید",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}


//========================================= Vacation Superior
function ViewAllVacationList3() {
    var i = 0;
    //loadingPageShow();
    $("#vacation_tb").DataTable({

        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,
        "ajax": {
            "url": "/Admin/VacationSuperior/viewAll",
            "type": "POST",
            "datatype": "json",
            "data": {
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },

        "columns": [
            //این فیلد row خالی هست و صرفا برای این قرار داده شده تا کد انتهای همین تابع مقادیر شماره سطر را داخل آن نمایش دهد.
            { "data": "row", "name": "row" },
            { "data": "fullName", "name": "fullName" },
            { "data": "createDate", "name": "createDate" },
            {
                data: "vacationType",
                render: function (data) {
                    if (data) {
                        return '<span>روزانه</span>';
                    }
                    else {
                        return '<span>ساعتی</span>';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    if (row.vacationType) {
                        return '<span>' + row.dayVacationStartDate + '</span>';
                    } else {
                        return '<span>' + row.dateVacationTime + '</span> <span>' + row.startTime + '</span>';
                    }
                }
            },
            {
                mRender: function (data, type, row) {
                    if (row.vacationType) {
                        return '<span>' + row.dayVacationEndDate + '</span>';
                    } else {
                        return '<span>' + row.dateVacationTime + '</span> <span>' + row.endTime + '</span>';
                    }
                }
            },
            {
                data: "finalStatus",
                render: function (data) {
                    if (data == null) {
                        return '<span class="p-2 status_">نا مشخص</span>';
                    }
                    else if (data == true) {
                        return '<span class="p-2 status_True">تایید شده</span>';
                    }
                    else {
                        return '<span class="p-2 status_False">تایید نشده</span>';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    return '<button type="button" class="btn btn-icon btn-primary btnIcon" title="ویرایش" onclick="openModalForEditVacation3(' + row.vacation_ID + ')"><i class="fa fa-edit"></i></button>';
                }
            },
        ],
        //کد مربوط به نمایش شماره سطر
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $("td:first", nRow).html(iDisplayIndex + 1);
            return nRow;
        },

    });
    //loadingPageHide();
}

function openModalForEditVacation3(id) {
    $.get("/Admin/VacationSuperior/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست مرخصی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function UpdateVacation3() {

    var vacationSignature = {
        VacationSignature_ID: $('#VacationSignature_ID').val(),
        Vacation_ID: $('#Vacation_ID').val(),
        SignatureStatus: $("#SignatureStatus").val(),
        SignatureReason: $("#SignatureReason").val(),
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/VacationSuperior/Edit',
        data: { vacationSignature: vacationSignature },
        success: function (data) {
            $("#div-loading").addClass('d-none');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست مرخصی",
                    text: "درخواست مرخصی با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });
                $('#edit-modal').modal('toggle');
                ViewAllVacationList3();
            }
        },
        error: function (result) {
        }
    });
}

//================================ ماموریت ویژه
function openModalForAddMission2() {
    $.get("/Admin/MissionSP/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ثبت درخواست ماموریت");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function AddMission2() {
    var frm = $("#mission_frm").valid();
    if (frm === false) {
        return false;
    }
    if ($('#MissionStart').val() == null || $('#MissionStart').val() == '') {
        swal({
            type: 'warning',
            title: "خطا",
            text: 'تاریخ شروع ماموریت را وارد نمایید',
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        return false;
    }
    if ($('#MissionEnd').val() == null || $('#MissionEnd').val() == '') {
        swal({
            type: 'warning',
            title: "خطا",
            text: 'تاریخ پایان ماموریت را وارد نمایید',
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        return false;
    }
    if ($('#MissionType').val() == 1) {
        if ($('#StartTime').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'ساعت شروع ماموریت را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        } else if ($('#EndTime').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'ساعت پایان ماموریت را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        }
    }

    $("#div-loading").removeClass('hidden');
    var mission = {
        PersonalCodeFirst: $('#PersonalCode').text(),
        MissionSubject: $("#MissionSubject").val(),
        MissionStart: $("#MissionStart").val(),
        MissionEnd: $("#MissionEnd").val(),
        Destination: $("#Destination").val(),
        MissionType: $("#MissionType").val(),
        Reason: $("#Reason").val(),
        Followers: $("#Followers").val(),
        StartTime: $("#StartTime").val(),
        EndTime: $("#EndTime").val(),
        type: $("#type").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/MissionSP/Create',
        data: { mission: mission, sdate: $("#tarikhAlt1").val(), edate: $("#tarikhAlt2").val() },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status) {
                swal({
                    type: 'success',
                    title: "ثبت درخواست ماموریت",
                    text: "درخواست شما با موفقیت ثبت شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllMissions2();
            } else {
                swal({
                    type: 'warning',
                    title: "خطا در ثبت درخواست ماموریت",
                    text: data.message,
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });
            }
        },
        error: function (result) {
        }
    });
}

function ViewAllMissions2() {
    debugger;
    //$.get("/Admin/MissionSP/viewAll/", function (res) {
    //    $("#viewAll").html(res);
    //});

    $("#vacation_tb").DataTable({

        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,
        "ajax": {
            "url": "/Admin/MissionSP/ViewAll",
            "type": "POST",
            "datatype": "json",
            "data": {
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },

        "columns": [
            //این فیلد row خالی هست و صرفا برای این قرار داده شده تا کد انتهای همین تابع مقادیر شماره سطر را داخل آن نمایش دهد.
            { "data": "row", "name": "row" },
            { "data": "createDate", "name": "createDate" },
            {
                mRender: function (data, type, row) {
                    if (row.startTime == null) {
                        return '<span>' + row.missionStart + '</span>';
                    } else {
                        return '<span>' + row.missionStart + '</span> <span>' + row.startTime + '</span>';
                    }
                }
            },
            {
                mRender: function (data, type, row) {
                    if (row.endTime == null) {
                        return '<span>' + row.missionStart + '</span>';
                    } else {
                        return '<span>' + row.missionStart + '</span> <span>' + row.endTime + '</span>';
                    }
                }
            },
            { "data": "missionSubject", "name": "missionSubject" },
            {
                data: "type",
                render: function (data) {
                    if (data == true) {
                        return '<span class="p-2 status_True">روزانه</span>';
                    }
                    else {
                        return '<span class="p-2 status_False">ساعتی</span>';
                    }
                },
            },
            {
                data: "finalStatus",
                render: function (data) {
                    if (data == null) {
                        return '<span class="p-2 status_">نا مشخص</span>';
                    }
                    else if (data == true) {
                        return '<span class="p-2 status_True">تایید شده</span>';
                    }
                    else {
                        return '<span class="p-2 status_False">تایید نشده</span>';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    console.log('alert');
                    console.log(row);
                    return '<button type="button" class="btn btn-icon btn-primary btnIcon" title="ویرایش" onclick="openModalForEditMission2(' + row.mission_ID + ')"><i class="fa fa-edit"></i></button>' +
                        '<button type="button" class="btn btn-icon btn-danger btnIcon" title = "حذف" onclick = "DeleteMission2(' + row.mission_ID + ');" > <i class="fa fa-trash"></i> </button >';
                }
            },
        ],
        //کد مربوط به نمایش شماره سطر
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $("td:first", nRow).html(iDisplayIndex + 1);
            return nRow;
        },

    });
}

function openModalForEditMission2(id) {
    $.get("/Admin/MissionSP/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست ماموریت");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}


function UpdateMission2() {
    var frm = $("#mission_frmEdit").valid();
    if (frm === false) {
        return false;
    }
    if ($('#MissionStart').val() == null || $('#MissionStart').val() == '') {
        swal({
            type: 'warning',
            title: "خطا",
            text: 'تاریخ شروع ماموریت را وارد نمایید',
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        return false;
    }
    if ($('#MissionEnd').val() == null || $('#MissionEnd').val() == '') {
        swal({
            type: 'warning',
            title: "خطا",
            text: 'تاریخ پایان ماموریت را وارد نمایید',
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        return false;
    }

    if ($('#MissionType').val() == 1) {
        if ($('#StartTime').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'ساعت شروع ماموریت را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        } else if ($('#EndTime').val() == '') {
            swal({
                type: 'warning',
                title: "خطا",
                text: 'ساعت پایان ماموریت را وارد نمایید',
                showConfirmButton: true,
                confirmButtonText: "تائید"
            });
            return false;
        }
    }

    $("#div-loading").removeClass('hidden');
    var mission = {
        Mission_ID: $('#mission_Mission_ID').val(),
        //PersonalCode: $('#PersonalCode').text(),
        MissionSubject: $("#mission_MissionSubject").val(),
        MissionStart: $("#MissionStart").val(),
        MissionEnd: $("#MissionEnd").val(),
        Destination: $("#mission_Destination").val(),
        MissionType: $("#mission_MissionType").val(),
        Reason: $("#mission_Reason").val(),
        Followers: $("#mission_Followers").val(),
        StartTime: $("#StartTime").val(),
        EndTime: $("#EndTime").val(),
        type: $("#mission_type").val()
        //CreateDate: $("#CreateDate").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/MissionSP/Edit',
        data: { mission: mission, sdate: $("#tarikhAlt1").val(), edate: $("#tarikhAlt2").val() },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status) {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست ماموریت",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllMissions2();
            } else {
                swal({
                    type: 'warning',
                    title: "خطا در ویرایش درخواست ماموریت",
                    text: data.message,
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });
            }
        },
        error: function (result) {
        }
    });
}


function DeleteMission2(id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این درخواست اطمینان دارید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/MissionSP/Delete',
                    data: { id: id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }
                        if (data == 'ok') {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "ماموریت",
                                text: "درخواست مورد نظر شما با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });

                            ViewAllMissions2();
                        } else {
                            swal({
                                title: "ماموریت",
                                text: "درخواست مورد نظر شما با خطا مواجه گردید",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}

//========================================= Mission Superior
function ViewAllSuperiorMissions2() {
    $("#vacation_tb").DataTable({

        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,
        "ajax": {
            "url": "/Admin/MissionSuperior/viewAll",
            "type": "POST",
            "datatype": "json",
            "data": {
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },

        "columns": [
            //این فیلد row خالی هست و صرفا برای این قرار داده شده تا کد انتهای همین تابع مقادیر شماره سطر را داخل آن نمایش دهد.
            { "data": "row", "name": "row" },
            { "data": "fullName", "name": "fullName" },
            { "data": "createDate", "name": "createDate" },
            {
                mRender: function (data, type, row) {
                    if (row.startTime == null) {
                        return '<span>' + row.missionStart + '</span>';
                    } else {
                        return '<span>' + row.missionStart + '</span> <span>' + row.startTime + '</span>';
                    }
                }
            },
            {
                mRender: function (data, type, row) {
                    if (row.endTime == null) {
                        return '<span>' + row.missionStart + '</span>';
                    } else {
                        return '<span>' + row.missionStart + '</span> <span>' + row.endTime + '</span>';
                    }
                }
            },
            //{ "data": "missionSubject", "name": "missionSubject" },
            {
                data: "type",
                render: function (data) {
                    if (data == true) {
                        return '<span class="p-2 status_True">روزانه</span>';
                    }
                    else {
                        return '<span class="p-2 status_False">ساعتی</span>';
                    }
                },
            },
            {
                data: "finalStatus",
                render: function (data) {
                    if (data == null) {
                        return '<span class="p-2 status_">نا مشخص</span>';
                    }
                    else if (data == true) {
                        return '<span class="p-2 status_True">تایید شده</span>';
                    }
                    else {
                        return '<span class="p-2 status_False">تایید نشده</span>';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    console.log('alert');
                    console.log(row);
                    return '<button type="button" class="btn btn-icon btn-primary btnIcon" title="ویرایش" onclick="openModalForEditSuperiorMission2(' + row.mission_ID + ')"><i class="fa fa-edit"></i></button>';
                }
            },
        ],
        //کد مربوط به نمایش شماره سطر
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $("td:first", nRow).html(iDisplayIndex + 1);
            return nRow;
        },

    });
}

function ClearSuperiorMissionsFilters2() {
    $('#ApplicationUserFullName_txt').val('');
    $('#AlternateName_txt').val('');
    $('#SuperiorName_txt').val('');
    $('#managerName_txt').val('');
    $('#HRManagerName_txt').val('');
    $('#StartCreateDate').val('');
    $('#StartCreateDate_txt').val('');
    $('#EndCreateDate').val('');
    $('#EndCreateDate_txt').val('');
}

function openModalForEditSuperiorMission2(id) {
    $.get("/Admin/MissionSuperior/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست ماموریت");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function UpdateSuperiorMission2() {
    $("#div-loading").removeClass('hidden');
   
    var missionSignature = {
        MissionSignature_ID: $('#MissionSignature_ID').val(),
        Mission_ID: $('#Mission_ID').val(),
        SignatureStatus: $("#SignatureStatus").val(),
        SignatureReason: $("#SignatureReason").val(),
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/MissionSuperior/Edit',
        data: { missionSignature: missionSignature },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست ماموریت",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllSuperiorMissions2();
            } else {
                swal({
                    type: 'warning',
                    title: "خطا در ویرایش درخواست ماموریت",
                    text: data.status,
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });
            }
        },
        error: function (result) {
        }
    });
}

//=============================================== Shift change ویژه

function openModalForAddShiftChange2() {
    $.get("/Admin/ShiftChange/Create/", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ثبت درخواست تعویض شیفت");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function AddShiftChange2() {
    $("#div-loading").removeClass('hidden');


    var ShiftChange = {
        PersonalCodeFirst: $('#PersonalCode').text(),
        UnitID_1: $("#UnitID_1").val(),
        PersonalCodeSecond: $("#alternateList_ddl").val(),
        shiftDate: $("#StartDate").val(),
        ShiftName1: $("#ShiftName1").val(),
        ShiftName2: $("#ShiftName2").val(),
        shiftReason: $("#shiftReason").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/ShiftChange/Create',
        data: { ShiftChange: ShiftChange, sdate: $("#tarikhAlt1").val() },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status) {
                swal({
                    type: 'success',
                    title: "ثبت درخواست تعویض شیفت",
                    text: "درخواست شما با موفقیت ثبت شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllShiftChangesList2();
            } else {
                swal({
                    type: 'warning',
                    title: "خطا در ثبت درخواست تعویض شیفت",
                    text: data.status,
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });
            }
        },
        error: function (result) {
        }
    });
}

function ViewAllShiftChangesList2() {
    debugger;
    //$.get("/Admin/ShiftChange/viewAll/", function (res) {
    //    $("#viewAll").html(res);
    //});

    $("#vacation_tb").DataTable({

        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,
        "ajax": {
            "url": "/Admin/ShiftChange/viewAll",
            "type": "POST",
            "datatype": "json",
            "data": {
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },

        "columns": [
            //این فیلد row خالی هست و صرفا برای این قرار داده شده تا کد انتهای همین تابع مقادیر شماره سطر را داخل آن نمایش دهد.
            { "data": "row", "name": "row" },
            { "data": "alternateFullName", "name": "alternateFullName" },
            { "data": "createDate", "name": "createDate" },
            { "data": "shiftDate", "name": "shiftDate" },
            {
                data: "finalStatus",
                render: function (data) {
                    if (data == null) {
                        return '<span class="p-2 status_">نا مشخص</span>';
                    }
                    else if (data == true) {
                        return '<span class="p-2 status_True">تایید شده</span>';
                    }
                    else {
                        return '<span class="p-2 status_False">تایید نشده</span>';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    console.log('alert');
                    console.log(row);
                    return '<button type="button" class="btn btn-icon btn-primary btnIcon" title="ویرایش" onclick="openModalForEditShiftChange2(' + row.shiftChange_ID + ')"><i class="fa fa-edit"></i></button>' +
                        '<button class="btn btn-icon btn-danger btnIcon" title="حذف" onclick="DeleteShiftChange2(' + row.shiftChange_ID+');"> <i class="fa fa-trash"></i> </button>';
                }
            },
        ],
        //کد مربوط به نمایش شماره سطر
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $("td:first", nRow).html(iDisplayIndex + 1);
            return nRow;
        },

    });
}


function openModalForEditShiftChange2(id) {
    $.get("/Admin/ShiftChange/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست تعویض شیفت");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function UpdateShiftChange2() {
    $("#div-loading").removeClass('hidden');


    var ShiftChange = {
        ShiftChange_ID: $('#ShiftChange_ID').val(),
        CreateDate: $('#CreateDate').val(),
        PersonalCodeFirst: $('#PersonalCode').text(),
        UnitID_1: $("#UnitID_1").val(),
        PersonalCodeSecond: $("#alternateList_ddl").val(),
        shiftDate: $("#StartDate").val(),
        ShiftName1: $("#ShiftName1").val(),
        ShiftName2: $("#ShiftName2").val(),
        shiftReason: $("#shiftReason").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/ShiftChange/Edit',
        data: { ShiftChange: ShiftChange, sdate: $("#tarikhAlt1").val() },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست تعویض شیفت",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllShiftChangesList2();
            }
        },
        error: function (result) {
        }
    });
}


function DeleteShiftChange2(id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این درخواست اطمینان دارید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/ShiftChange/Delete',
                    data: { id: id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }
                        if (data == 'ok') {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "تعویض شیفت",
                                text: "درخواست مورد نظر شما با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });

                            ViewAllShiftChangesList2();
                        } else {
                            swal({
                                title: "تعویض شیفت",
                                text: "درخواست مورد نظر شما با خطا مواجه گردید",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}

//===================================== Superior Shift Change Request ویژه
function ViewAllSuperiorShiftChangesList2(filterList) {
   
    $("#vacation_tb").DataTable({

        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,
        "ajax": {
            "url": "/Admin/ShiftChangeSuperior/viewAll",
            "type": "POST",
            "datatype": "json",
            "data": {
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },

        "columns": [
            //این فیلد row خالی هست و صرفا برای این قرار داده شده تا کد انتهای همین تابع مقادیر شماره سطر را داخل آن نمایش دهد.
            { "data": "row", "name": "row" },
            { "data": "fullName", "name": "fullName" },
            { "data": "createDate", "name": "createDate" },
            { "data": "shiftDate", "name": "shiftDate" },
            {
                data: "finalStatus",
                render: function (data) {
                    if (data == null) {
                        return '<span class="p-2 status_">نا مشخص</span>';
                    }
                    else if (data == true) {
                        return '<span class="p-2 status_True">تایید شده</span>';
                    }
                    else {
                        return '<span class="p-2 status_False">تایید نشده</span>';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    console.log('alert');
                    console.log(row);
                    return '<button type="button" class="btn btn-icon btn-primary btnIcon" title="ویرایش" onclick="openModalForEditSuperiorShiftChange2(' + row.shiftChange_ID + ')"><i class="fa fa-edit"></i></button>';
                }
            },
        ],
        //کد مربوط به نمایش شماره سطر
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $("td:first", nRow).html(iDisplayIndex + 1);
            return nRow;
        },

    });

}

function ClearAllSuperiorShiftChangesFilters2() {
    $('#ApplicationUserFullName_txt').val('');
    $('#AlternateName_txt').val('');
    $('#SuperiorName_txt').val('');
    $('#managerName_txt').val('');
    $('#HRManagerName_txt').val('');
    $('#StartCreateDate').val('');
    $('#StartCreateDate_txt').val('');
    $('#EndCreateDate').val('');
    $('#EndCreateDate_txt').val('');
}

function openModalForEditSuperiorShiftChange2(id) {
    $.get("/Admin/ShiftChangeSuperior/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست تعویض شیفت");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function UpdateSuperiorShiftChange2() {
    $("#div-loading").removeClass('hidden');


    var shiftChangeSignature = {
        ShiftChangeSignature_ID: $('#ShiftChangeSignature_ID').val(),
        ShiftChange_ID: $('#ShiftChange_ID').val(),
        SignatureStatus: $("#SignatureStatus").val(),
        SignatureReason: $("#SignatureReason").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/ShiftChangeSuperior/Edit',
        data: { shiftChangeSignature: shiftChangeSignature },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست تعویض شیفت",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllSuperiorShiftChangesList2();
            }
        },
        error: function (result) {
        }
    });
}

//=============================================== Extra work

function openModalForAddExtraWork2() {
    $.get("/Admin/ExtraWork/Create/", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ثبت درخواست اضافه کاری");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}
function AddExtraWork2() {
    var frm = $("#AddExtraWork_frm").valid();
    if (frm === false) {
        return false;
    }
    if ($('#StartDate').val() == null || $('#StartDate').val() == '') {
        swal({
            type: 'warning',
            title: "خطا",
            text: 'تاریخ شروع اضافه کاری را وارد نمایید',
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        return false;
    }
    if ($('#EndDate').val() == null || $('#EndDate').val() == '') {
        swal({
            type: 'warning',
            title: "خطا",
            text: 'تاریخ پایان اضافه کاری را وارد نمایید',
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        return false;
    }
    if ($('#StartTime').val() == null || $('#StartTime').val() == '') {
        swal({
            type: 'warning',
            title: "خطا",
            text: 'ساعت شروع اضافه کاری را وارد نمایید',
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        return false;
    }
    if ($('#EndTime').val() == null || $('#EndTime').val() == '') {
        swal({
            type: 'warning',
            title: "خطا",
            text: 'ساعت پایان اضافه کاری را وارد نمایید',
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        return false;
    }
    $("#div-loading").removeClass('hidden');


    var ExtraWork = {
        PersonalCodeFirst: $('#PersonalCode').text(),
        StartDate: $("#StartDate").val(),
        EndDate: $("#EndDate").val(),
        StartTime: $("#StartTime").val(),
        EndTime: $("#EndTime").val(),
        ExtraWorktime: $("#ExtraWorktime").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/ExtraWork/Create',
        data: { ExtraWork: ExtraWork, sdate: $("#tarikhAlt1").val(), eDate: $("#tarikhAlt2").val() },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status ) {
                swal({
                    type: 'success',
                    title: "ثبت درخواست اضافه کاری",
                    text: "درخواست شما با موفقیت ثبت شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllExtraWorks2();
            }
        },
        error: function (result) {
        }
    });
}

function ViewAllExtraWorks2() {
    debugger;
    //$.get("/Admin/ExtraWork/ViewAll/", function (res) {
    //    $("#viewAll").html(res);
    //});

    $("#vacation_tb").DataTable({

        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,
        "ajax": {
            "url": "/Admin/ExtraWork/ViewAll",
            "type": "POST",
            "datatype": "json",
            "data": {
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },

        "columns": [
            //این فیلد row خالی هست و صرفا برای این قرار داده شده تا کد انتهای همین تابع مقادیر شماره سطر را داخل آن نمایش دهد.
            { "data": "row", "name": "row" },
            { "data": "fullName", "name": "fullName" },
            { "data": "createDate", "name": "createDate" },
            {
                data: "finalStatus",
                render: function (data) {
                    if (data == null) {
                        return '<span class="p-2 status_">نا مشخص</span>';
                    }
                    else if (data == true) {
                        return '<span class="p-2 status_True">تایید شده</span>';
                    }
                    else {
                        return '<span class="p-2 status_False">تایید نشده</span>';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    console.log('alert');
                    console.log(row);
                    return '<button type="button" class="btn btn-icon btn-primary btnIcon" title="ویرایش" onclick="openModalForEditExtraWork2(' + row.extraWork_ID + ')"><i class="fa fa-edit"></i></button>' +
                        '<button type="button" class="btn btn-icon btn-danger btnIcon" title = "حذف" onclick = "DeleteExtraWork2(' + row.extraWork_ID + ');" > <i class="fa fa-trash"></i> </button >';
                }
            },
        ],
        //کد مربوط به نمایش شماره سطر
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $("td:first", nRow).html(iDisplayIndex + 1);
            return nRow;
        },

    });

}


function openModalForEditExtraWork2(id) {
    $.get("/Admin/ExtraWork/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست اضافه کاری");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}
function UpdateExtraWork2() {
    var frm = $("#EditExtraWork_frm").valid();
    if (frm === false) {
        return false;
    }

    if ($('#StartDate').val() == null || $('#StartDate').val() == '') {
        swal({
            type: 'warning',
            title: "خطا",
            text: 'تاریخ شروع اضافه کاری را وارد نمایید',
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        return false;
    }
    if ($('#EndDate').val() == null || $('#EndDate').val() == '') {
        swal({
            type: 'warning',
            title: "خطا",
            text: 'تاریخ پایان اضافه کاری را وارد نمایید',
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        return false;
    }
    if ($('#StartTime').val() == null || $('#StartTime').val() == '') {
        swal({
            type: 'warning',
            title: "خطا",
            text: 'ساعت شروع اضافه کاری را وارد نمایید',
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        return false;
    }
    if ($('#EndTime').val() == null || $('#EndTime').val() == '') {
        swal({
            type: 'warning',
            title: "خطا",
            text: 'ساعت پایان اضافه کاری را وارد نمایید',
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        return false;
    }
    $("#div-loading").removeClass('hidden');


    var ExtraWork = {
        ExtraWorkID: $('#ExtraWorkID').val(),
        PersonalCode: $('#PersonalCode').text(),
        StartDate: $("#StartDate").val(),
        EndDate: $("#EndDate").val(),
        StartTime: $("#StartTime").val(),
        EndTime: $("#EndTime").val(),
        ExtraWorktime: $("#ExtraWorktime").val(),
        CreateDate: $("#CreateDate").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/ExtraWork/Edit',
        data: { ExtraWork: ExtraWork, sdate: $("#tarikhAlt1").val(), eDate: $("#tarikhAlt2").val() },//
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست اضافه کاری",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllExtraWorks();
            }
        },
        error: function (result) {
        }
    });
}

function DeleteExtraWork2(id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این درخواست اطمینان دارید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/ExtraWork/Delete',
                    data: { id: id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }
                        if (data == 'ok') {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "اضافه کاری",
                                text: "درخواست مورد نظر شما با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });

                            ViewAllExtraWorks();
                        } else {
                            swal({
                                title: "اضافه کاری",
                                text: "درخواست مورد نظر شما با خطا مواجه گردید",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}

//==================================================== Extra Work Superior

function ViewAllExtraWorksSuperior2() {

    $("#vacation_tb").DataTable({

        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,
        "ajax": {
            "url": "/Admin/ExtraWorkSuperior/ViewAll",
            "type": "POST",
            "datatype": "json",
            "data": {
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },

        "columns": [
            //این فیلد row خالی هست و صرفا برای این قرار داده شده تا کد انتهای همین تابع مقادیر شماره سطر را داخل آن نمایش دهد.
            { "data": "row", "name": "row" },
            { "data": "fullName", "name": "fullName" },
            { "data": "createDate", "name": "createDate" },
            {
                data: "finalStatus",
                render: function (data) {
                    if (data == null) {
                        return '<span class="p-2 status_">نا مشخص</span>';
                    }
                    else if (data == true) {
                        return '<span class="p-2 status_True">تایید شده</span>';
                    }
                    else {
                        return '<span class="p-2 status_False">تایید نشده</span>';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    return '<button type="button" class="btn btn-icon btn-primary btnIcon" title="ویرایش" onclick="openModalForEditExtraWorkSuprior2(' + row.extraWork_ID + ')"><i class="fa fa-edit"></i></button>';
                }
            },
        ],
        //کد مربوط به نمایش شماره سطر
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $("td:first", nRow).html(iDisplayIndex + 1);
            return nRow;
        },

    });
}

function ClearExtraWorksSuperiorFilters2() {
    $('#ApplicationUserFullName_txt').val('');
    $('#StartCreateDate').val('');
    $('#StartCreateDate_txt').val('');
    $('#EndCreateDate').val('');
    $('#EndCreateDate_txt').val('');
}

function openModalForEditExtraWorkSuprior2(id) {
    $.get("/Admin/ExtraWorkSuperior/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست اضافه کاری");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

function UpdateExtraWorkSuperior2() {
    $("#div-loading").removeClass('hidden');


    var extraworkSignature = {
        ExtraworkSignature_ID: $('#ExtraworkSignature_ID').val(),
        ExtraWork_ID: $('#ExtraWork_ID').val(),
        SignatureStatus: $("#SignatureStatus").val(),
        SignatureReason: $("#SignatureReason").val(),
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/ExtraWorkSuperior/Edit',
        data: { extraworkSignature: extraworkSignature },//, sdate: $("#tarikhAlt1").val(), eDate: $("#tarikhAlt2").val()
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.status == "ok") {
                swal({
                    type: 'success',
                    title: "ویرایش درخواست اضافه کاری",
                    text: "درخواست شما با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                ViewAllExtraWorksSuperior2();
            }
        },
        error: function (result) {
        }
    });
}

//========================================= All Vacations Of All Eployees
function ViewAllVacationList4() {
    //loadingPageShow();
    $("#vacation_tb").DataTable({

        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,
        "ajax": {
            "url": "/Admin/VacationsAllEmployee/viewAll",
            "type": "POST",
            "datatype": "json",
            "data": {
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },

        "columns": [
            //این فیلد row خالی هست و صرفا برای این قرار داده شده تا کد انتهای همین تابع مقادیر شماره سطر را داخل آن نمایش دهد.
            { "data": "row", "name": "row" },
            { "data": "fullName", "name": "fullName" },
            { "data": "createDate", "name": "createDate" },
            {
                data: "vacationType",
                render: function (data) {
                    if (data) {
                        return '<span>روزانه</span>';
                    }
                    else {
                        return '<span>ساعتی</span>';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    if (row.vacationType) {
                        return '<span>' + row.dayVacationStartDate + '</span>';
                    } else {
                        return '<span>' + row.dateVacationTime + '</span> <span>' + row.startTime + '</span>';
                    }
                }
            },
            {
                mRender: function (data, type, row) {
                    if (row.vacationType) {
                        return '<span>' + row.dayVacationEndDate + '</span>';
                    } else {
                        return '<span>' + row.dateVacationTime + '</span> <span>' + row.endTime + '</span>';
                    }
                }
            },
            {
                data: "finalStatus",
                render: function (data) {
                    if (data == null) {
                        return '<span class="p-2 status_">نا مشخص</span>';
                    }
                    else if (data == true) {
                        return '<span class="p-2 status_True">تایید شده</span>';
                    }
                    else {
                        return '<span class="p-2 status_False">تایید نشده</span>';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    return '<button type="button" class="btn btn-icon btn-primary btnIcon" title="ویرایش" onclick="openModalForEditVacation4(' + row.vacation_ID + ')"><i class="fa fa-edit"></i></button>';
                }
            },
        ],
        //کد مربوط به نمایش شماره سطر
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $("td:first", nRow).html(iDisplayIndex + 1);
            return nRow;
        },

    });
    //loadingPageHide();
}

function openModalForEditVacation4(id) {
    $.get("/Admin/VacationsAllEmployee/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست مرخصی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

//========================================= Missions All Employees
function ViewAllSuperiorMissions3() {
    $("#vacation_tb").DataTable({

        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,
        "ajax": {
            "url": "/Admin/MissionsAllEmployees/viewAll",
            "type": "POST",
            "datatype": "json",
            "data": {
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },

        "columns": [
            //این فیلد row خالی هست و صرفا برای این قرار داده شده تا کد انتهای همین تابع مقادیر شماره سطر را داخل آن نمایش دهد.
            { "data": "row", "name": "row" },
            { "data": "fullName", "name": "fullName" },
            { "data": "createDate", "name": "createDate" },
            {
                mRender: function (data, type, row) {
                    if (row.startTime == null) {
                        return '<span>' + row.missionStart + '</span>';
                    } else {
                        return '<span>' + row.missionStart + '</span> <span>' + row.startTime + '</span>';
                    }
                }
            },
            {
                mRender: function (data, type, row) {
                    if (row.endTime == null) {
                        return '<span>' + row.missionStart + '</span>';
                    } else {
                        return '<span>' + row.missionStart + '</span> <span>' + row.endTime + '</span>';
                    }
                }
            },
            //{ "data": "missionSubject", "name": "missionSubject" },
            {
                data: "finalStatus",
                render: function (data) {
                    if (data == null) {
                        return '<span class="p-2 status_">نا مشخص</span>';
                    }
                    else if (data == true) {
                        return '<span class="p-2 status_True">تایید شده</span>';
                    }
                    else {
                        return '<span class="p-2 status_False">تایید نشده</span>';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    console.log('alert');
                    console.log(row);
                    return '<button type="button" class="btn btn-icon btn-primary btnIcon" title="ویرایش" onclick="openModalForEditSuperiorMission3(' + row.mission_ID + ')"><i class="fa fa-edit"></i></button>';
                }
            },
        ],
        //کد مربوط به نمایش شماره سطر
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $("td:first", nRow).html(iDisplayIndex + 1);
            return nRow;
        },

    });
}


function openModalForEditSuperiorMission3(id) {
    $.get("/Admin/MissionsAllEmployees/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست ماموریت");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

//===================================== All Shift Change Of All Employees
function ViewAllSuperiorShiftChangesList3() {

    $("#vacation_tb").DataTable({

        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,
        "ajax": {
            "url": "/Admin/ShiftChangesAllEmployees/viewAll",
            "type": "POST",
            "datatype": "json",
            "data": {
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },

        "columns": [
            //این فیلد row خالی هست و صرفا برای این قرار داده شده تا کد انتهای همین تابع مقادیر شماره سطر را داخل آن نمایش دهد.
            { "data": "row", "name": "row" },
            { "data": "fullName", "name": "fullName" },
            { "data": "createDate", "name": "createDate" },
            { "data": "shiftDate", "name": "shiftDate" },
            {
                data: "finalStatus",
                render: function (data) {
                    if (data == null) {
                        return '<span class="p-2 status_">نا مشخص</span>';
                    }
                    else if (data == true) {
                        return '<span class="p-2 status_True">تایید شده</span>';
                    }
                    else {
                        return '<span class="p-2 status_False">تایید نشده</span>';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    console.log('alert');
                    console.log(row);
                    return '<button type="button" class="btn btn-icon btn-primary btnIcon" title="ویرایش" onclick="openModalForEditSuperiorShiftChange3(' + row.shiftChange_ID + ')"><i class="fa fa-edit"></i></button>';
                }
            },
        ],
        //کد مربوط به نمایش شماره سطر
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $("td:first", nRow).html(iDisplayIndex + 1);
            return nRow;
        },

    });

}

function openModalForEditSuperiorShiftChange3(id) {
    $.get("/Admin/ShiftChangesAllEmployees/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست تعویض شیفت");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}

//==================================================== Extra Work Superior

function ViewAllExtraWorksSuperior3() {

    $("#vacation_tb").DataTable({

        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,
        "ajax": {
            "url": "/Admin/ExtraWorksAllEmployees/ViewAll",
            "type": "POST",
            "datatype": "json",
            "data": {
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },

        "columns": [
            //این فیلد row خالی هست و صرفا برای این قرار داده شده تا کد انتهای همین تابع مقادیر شماره سطر را داخل آن نمایش دهد.
            { "data": "row", "name": "row" },
            { "data": "fullName", "name": "fullName" },
            { "data": "createDate", "name": "createDate" },
            {
                data: "finalStatus",
                render: function (data) {
                    if (data == null) {
                        return '<span class="p-2 status_">نا مشخص</span>';
                    }
                    else if (data == true) {
                        return '<span class="p-2 status_True">تایید شده</span>';
                    }
                    else {
                        return '<span class="p-2 status_False">تایید نشده</span>';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    console.log('alert');
                    console.log(row);
                    return '<button type="button" class="btn btn-icon btn-primary btnIcon" title="ویرایش" onclick="openModalForEditExtraWorkSuprior3(' + row.extraWork_ID + ')"><i class="fa fa-edit"></i></button>';
                }
            },
        ],
        //کد مربوط به نمایش شماره سطر
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $("td:first", nRow).html(iDisplayIndex + 1);
            return nRow;
        },

    });
}

function openModalForEditExtraWorkSuprior3(id) {
    $.get("/Admin/ExtraWorksAllEmployees/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درخواست اضافه کاری");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}
