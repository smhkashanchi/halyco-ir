﻿$(function () {
    
   

    table = $('#users_tb').DataTable();
    table.destroy();
    $('#users_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [

            { "className": "dt-center", "targets": "_all" }

        ]

    });

    table = $('#cnGroup_tb').DataTable();
    table.destroy();
    $('#cnGroup_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [

            { "className": "dt-center", "targets": "_all" }

        ]

    });


    table = $('#city_tb').DataTable();
    table.destroy();
    $('#city_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [

            { "className": "dt-center", "targets": "_all" }

        ]

    });

    table = $('#fileGroup_tb').DataTable();
    table.destroy();
    $('#fileGroup_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [

            { "className": "dt-center", "targets": "_all" }

        ]

    });

    table = $('#aboutUs_tb').DataTable();
    table.destroy();
    $('#aboutUs_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [

            { "className": "dt-center", "targets": "_all" }

        ]

    });

    table = $('#unit_tb').DataTable();
    table.destroy();
    $('#unit_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [

            { "className": "dt-center", "targets": "_all" }

        ]

    });

    table = $('#off_tb').DataTable();
    table.destroy();
    $('#off_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [

            { "className": "dt-center", "targets": "_all" }

        ]

    });

    table = $('#producer_tb').DataTable();
    table.destroy();
    $('#producer_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [

            { "className": "dt-center", "targets": "_all" }

        ]

    });

    table = $('#country_tb').DataTable();
    table.destroy();
    $('#country_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [
            
            { "className": "dt-center", "targets": "_all" }

        ]

    });

    table = $('#language_tb').DataTable();
    table.destroy();
    $('#language_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [
            { "className": "dt-center", "targets": "_all" }
        ]

    });

    table = $('#color_tb').DataTable();
    table.destroy();
    $('#color_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [
            { "className": "dt-center", "targets": "_all" }
        ]

    });

   
    
    table = $('#connetion_tb').DataTable();
    table.destroy();
    $('#connetion_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [
            { "className": "dt-center", "targets": "_all" }
        ]

    });


    table = $('#contactUsGroup_tb').DataTable();
    table.destroy();
    $('#contactUsGroup_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [
            { "className": "dt-center", "targets": "_all" }
        ]

    });

    table = $('#contactUs_tb').DataTable();
    table.destroy();
    $('#contactUs_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [

            { "className": "dt-center", "targets": "_all" }

        ]

    });

    table = $('#contentComment_tb').DataTable();
    table.destroy();
    $('#contentComment_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [
            { "className": "dt-center", "targets": "_all" }
        ]

    });

    table = $('#content_tb').DataTable();
    table.destroy();
    $('#content_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [
            { "className": "dt-center", "targets": "_all" }
        ]

    });

    table = $('#contentGroup_tb').DataTable();
    table.destroy();
    $('#contentGroup_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [

            { "className": "dt-center", "targets": "_all" }

        ]

    });

    table = $('#coWorker_tb').DataTable();
    table.destroy();
    $('#coWorker_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [
            { "className": "dt-center", "targets": "_all" }
        ]

    });

    table = $('#faq_tb').DataTable();
    table.destroy();
    $('#faq_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [
            { "className": "dt-center", "targets": "_all" }
        ]

    });
    //
    table = $('#featureReply_tb').DataTable();
    table.destroy();
    $('#featureReply_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [

            { "className": "dt-center", "targets": "_all" }

        ]

    });

    table = $('#gallery_tb').DataTable();
    table.destroy();
    $('#gallery_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [
            { "className": "dt-center", "targets": "_all" }
        ]

    });

    
    table = $('#file_tb').DataTable();
    table.destroy();
    $('#file_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [
            { "className": "dt-center", "targets": "_all" }
        ]

    });

    table = $('#fileGroup_tb').DataTable();
    table.destroy();
    $('#fileGroup_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [

            { "className": "dt-center", "targets": "_all" }

        ]

    });

    table = $('#garanty_tb').DataTable();
    table.destroy();
    $('#garanty_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [
            { "className": "dt-center", "targets": "_all" }
        ]

    });

    table = $('#city_tb').DataTable();
    table.destroy();
    $('#city_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [
            { "className": "dt-center", "targets": "_all" }
        ]

    });

    table = $('#newsLetterGroup_tb').DataTable();
    table.destroy();
    $('#newsLetterGroup_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [

            { "className": "dt-center", "targets": "_all" }

        ]

    });

    table = $('#pSimilar_tb').DataTable();
    table.destroy();
    $('#pSimilar_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [
            { "className": "dt-center", "targets": "_all" }
        ]

    });

    table = $('#pAdvantage').DataTable();
    table.destroy();
    $('#pAdvantage').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [
            { "className": "dt-center", "targets": "_all" }
        ]

    });

    table = $('#productGroup_tb').DataTable();
    table.destroy();
    $('#productGroup_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [

            { "className": "dt-center", "targets": "_all" }

        ]

    });

    table = $('#pjVideo_tb').DataTable();
    table.destroy();
    $('#pjVideo_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [
            { "className": "dt-center", "targets": "_all" }
        ]

    });

    table = $('#projects_tb').DataTable();
    table.destroy();
    $('#projects_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [
            { "className": "dt-center", "targets": "_all" }
        ]

    });

    table = $('#slideShow_tb').DataTable();
    table.destroy();
    $('#slideShow_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [
            { "className": "dt-center", "targets": "_all" }
        ]

    });

    table = $('#pjAdvantage').DataTable();
    table.destroy();
    $('#pjAdvantage').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [
            { "className": "dt-center", "targets": "_all" }
        ]

    });

    table = $('#socialNet_tb').DataTable();
    table.destroy();
    $('#socialNet_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [

            { "className": "dt-center", "targets": "_all" }

        ]

    });

    table = $('#slideGroup_tb').DataTable();
    table.destroy();
    $('#slideGroup_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [
            { "className": "dt-center", "targets": "_all" }
        ]

    });

    table = $('#seo_tb').DataTable();
    table.destroy();
    $('#seo_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [
            { "className": "dt-center", "targets": "_all" }
        ]

    });



   

    table = $('#empSalary_tb').DataTable();
    table.destroy();
    $('#empSalary_tb').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        "deferRender": false,
        'autoWidth': true,
        'language': {
            "paginate": {
                'next': "بعدی",
                'previous': "قبلی"

            },
            "search": 'جستجو  :   ',
            "lengthMenu": "نمایش _MENU_ رکورد",
            "emptyTable": "هیچ داده ای ثبت نشده",
            "info": "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
            "infoEmpty": "نمایش 0 تا 0 از 0  رکورد",
            "zeroRecords": "هیچ رکوردی یافت نشد",
            "infoFiltered": ""
        }, "columnDefs": [

            { "className": "dt-center", "targets": "_all" }

        ]

    });

});


