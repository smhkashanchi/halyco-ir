﻿


function showArticles(contentId) {
    $("#viewAll").load("/Contents/showArticles/" + contentId);

}

function InsertComments() {
    //var frm = $("#commentFrm").valid();
    var mssg = checkCommentForm();
    if (mssg === "ok") {
        $("#loading").removeClass('hidden');



        var contentComment = {
            UserName: $("#recipient-name").val(),
            Email: $("#recipient-email").val(),
            IsConfirmed: false,
            IsNew: true,
            Comment: $("#comment_txt").val(),
            ContentId: $("#contentId").val(),
            ContentGroupId: $("#ContentGroupId").val()
        }



        $.ajax({
            method: 'POST',
            url: '/Contents/InsertComment',
            data: contentComment,
            success: function (data) {
                if (data == "ok") {
                    swal({ title: "تبریک", text: "نظر شما ثبت شد و بعد از تایید مدیر سایت نمایش داده خواهد شد", icon: "success", buttons: [false, "تایید"], });
                    clearCommentForm();
                }
                else {
                    swal({ title: "هشدار", text: "خطایی رخ داده است !!", icon: "warning", buttons: [false, "تایید"], });
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        $("#message").text(mssg);
    }
}

function checkCommentForm() {
    var msg = "ok";
    if ($("#recipient-name").val() == "") {
        msg = "لطفا نام خود را وارد نمایید";
    }
    else if ($("#recipient-email").val() == "") {
        msg = "لطفا ایمیل خود را وارد نمایید";
    }
    else if ($("#comment_txt").val() == "") {
        msg = "لطفا متن نظر خود را وارد نمایید";
    }
    return msg;
}

function clearCommentForm() {
    $("#recipient-name").val('');
    $("#recipient-email").val('');
    $("#comment_txt").val('');
}

function InsertContactMessage() {
    var frm = $("#commentFrm").valid();
    var mssg = checkContactForm();

    if (!frm) return false;

    if (frm) {
        var token = grecaptcha.getResponse();
        var contactusMessage = {
            SenderName: $("#SenderName").val(),
            SenderEmail: $("#SenderEmail").val(),
            SenderMobile: "",// $("#mobile").val(),
            CMGId: $("#reason").val(),
            IsNew: true,
            MessageText: $("#MessageText").val(),
            Captcha: token
        }

        $.ajax({
            method: 'POST',
            url: '/ContactUs/InsertContactMessage',
            data: contactusMessage,
            success: function (data) {
                if (data == "ok") {
                    $.toast({
                        heading: 'تبریک',
                        text: 'پیام شما با موفقیت ثبت شد و جواب آن از طریق ایمیل به شما ارسال خواهد شد',
                        showHideTransition: 'slide',
                        icon: 'success'
                    });
                    clearContactForm();
                }
                else if (data == "incorrect") {
                    $.toast({
                        heading: 'هشدار',
                        text: 'کد امنیتی وارد شده صحیح نمی باشد',
                        showHideTransition: 'slide',
                        icon: 'warning'
                    });
                }
                else {
                    $.toast({
                        heading: 'هشدار',
                        text: 'خطایی رخ داده است !!',
                        showHideTransition: 'slide',
                        icon: 'warning'
                    });
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        swal("هشدار", mssg, "warning");
        //$("#messageContact").text(mssg);
    }
}

function checkContactForm() {
    var msg = "ok";
    if ($("#name").val() == "") {
        msg = "لطفا نام خود را وارد نمایید";
    }
    else if ($("#email").val() == "") {
        msg = "لطفا ایمیل خود را وارد نمایید";
    }
    else if ($("#text_txt").val() == "") {
        msg = "لطفا متن  خود را وارد نمایید";
    }
    else if ($("#reason").val() == "0") {
        msg = "لطفا موضوع ارتباطی  خود را انتخاب نمایید";
    }
    else if ($("#captcha").val() == "") {
        msg = "لطفا کد امنیتی را انتخاب نمایید";
    }
    return msg;
}
function clearContactForm() {
    $("#name").val("");
    $("#email").val("");
    $("#text_txt").val("");
    $("#reason").val("0");
    $("#mobile").val("");
    $("#captcha").val("");
    $("#messageContact").text('');
}

function InsertProductComments() {
    //var frm = $("#commentFrm").valid();
    var mssg = checkProductCommentForm();
    if (mssg === "ok") {
        $("#loading").removeClass('hidden');



        var productComment = {
            UserName: $("#recipient-name").val(),
            Email: $("#recipient-email").val(),
            IsConfirmed: false,
            IsNew: true,
            Comment: $("#comment_txt").val(),
            ProductInforId: $("#ProductInfo_ID").val(),
            ProductGroupId: $("#ProductGroupId").val()
        }



        $.ajax({
            method: 'POST',
            url: '/Product/InsertComment',
            data: productComment,
            success: function (data) {
                if (data == "ok") {
                    swal({ title: "تبریک", text: "نظر شما ثبت شد و بعد از تایید مدیر سایت نمایش داده خواهد شد", icon: "success", buttons: [false, "تایید"], });
                    clearProductCommentForm();
                }
                else {
                    swal({ title: "هشدار", text: "خطایی رخ داده است !!", icon: "warning", buttons: [false, "تایید"], });
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        $("#message").text(mssg);
    }
}

function checkProductCommentForm() {
    var msg = "ok";
    if ($("#recipient-name").val() == "") {
        msg = "لطفا نام خود را وارد نمایید";
    }
    else if ($("#recipient-email").val() == "") {
        msg = "لطفا ایمیل خود را وارد نمایید";
    }
    else if ($("#comment_txt").val() == "") {
        msg = "لطفا متن نظر خود را وارد نمایید";
    }
    return msg;
}

function clearProductCommentForm() {
    $("#recipient-name").val('');
    $("#recipient-email").val('');
    $("#comment_txt").val('');
}



///////////////////////////////////////////////////////Video
function showVideo(videoId) {

    $.ajax({
        method: 'POST',
        url: '/Video/ShowVide',
        data: { id: videoId },
        success: function (data) {
            debugger;
            if (data != "nall") {
                var video = "ویدیو";
                var htmls = "";
                if (data.fileType.trim() == video.trim() && data.fileOrLink == true) {
                    htmls = "<video width='100%' height='357' controls><source src='/Files/FileManage/" + data.fileNameOrLink + "'></video>"
                }
                else if (data.fileType.trim() == video.trim() && data.fileOrLink == false) {
                    htmls = data.fileNameOrLink;
                }
                $("#screen").html(htmls);
                //data.fileType.trim() == video.trim() &&
            }//~/Files/FileManage/item.FileNameOrLink
        },
        error: function (result) {
        }
    });
}

function showVideoDefault() {

    $.ajax({
        method: 'POST',
        url: '/Video/ShowVideoDefault',
        data: '',
        success: function (data) {
            debugger;
            htmls = "<video width='100%' height='357' controls><source src='/Files/FileManage/" + data.fileNameOrLink + "'></video>"
            $("#screen").html(htmls);
            //data.fileType.trim() == video.trim() &&
            //~/Files/FileManage/item.FileNameOrLink
        },
        error: function (result) {
        }
    });
}

function GetVideoByGroupId_SideMenu(groupId) {
    $.ajax({
        method: 'POST',
        url: '/Video/GetVideoByGroupId',
        data: { id: groupId },
        success: function (data) {

            var options = "";
            data.forEach(function (obj) {
                options += "<li><a onclick='showVideo(" + obj.avF_ID + ")'> <span class='fa fa-check'></span>" + obj.avfTitle + "</a></li >";
            });
            debugger;
            $("#videos").html(options);
        },
        error: function (result) {
        }
    });
}


//$("btnSendEmail").click(function (e) {
function InsertNewsLetters() {
    if ($("#email_txt").val() != "") {
        var newsLetter = {
            GroupId: 1,
            FullName: "",
            Email: $("#email_txt").val(),
            Mobile: ""

        }


        $.ajax({
            method: 'POST',
            url: '/Home/InsertNewsLetter',
            data: newsLetter,
            success: function (data) {
                debugger;
                if (data == "ok") {
                    swal({ title: "تبریک", text: "ثبت نام شما در اشتراک ایمیلی سایت با موفقیت انجام شد", icon: "success", buttons: [false, "تایید"], });
                    $("#email_txt").val("");
                }
                //else if (data == "incorrect") {
                //    swal("هشدار", "کد امنیتی وارد شده صحیح نمی باشد", "warning");
                //}
                //else {
                //    swal({ title: "هشدار", text: "خطایی رخ داده است !!", icon: "warning", buttons: [false, "تایید"], });
                //}
            },
            error: function (result) {
            }
        });
    }
    else {
        swal({ title: "هشدار", text: "ایمیل خود را وارد نمایید", icon: "warning", buttons: [false, "تایید"], });
    }
};
////////////////////////////////////////////////// اشتراک پیامکی
function InsertSMS() {
    if ($("#mobile_txt").val() != "") {
        var persons = {
            MobileNumber: $("#mobile_txt").val()

        }

        $.ajax({
            method: 'POST',
            url: '/Home/InsertMobileNumber',
            data: persons,
            success: function (data) {
                debugger;
                if (data == "ok") {
                    swal({ title: "تبریک", text: "ثبت نام شما در اشتراک پیامکی سایت با موفقیت انجام شد", icon: "success", buttons: [false, "تایید"], });
                    $("#mobile_txt").val("");
                }
                else if (data == "repeate") {
                    swal({ title: "هشدار", text: "شما عضو اشتراک پیامکی سایت هستید", icon: "warning", buttons: [false, "تایید"], });
                    $("#mobile_txt").val("");
                }
                //else if (data == "incorrect") {
                //    swal("هشدار", "کد امنیتی وارد شده صحیح نمی باشد", "warning");
                //}
                //else {
                //    swal({ title: "هشدار", text: "خطایی رخ داده است !!", icon: "warning", buttons: [false, "تایید"], });
                //}
            },
            error: function (result) {
            }
        });
    }
    else {
        swal({ title: "هشدار", text: "شماره موبایل خود را وارد نمایید", icon: "warning", buttons: [false, "تایید"], });
    }
};
//////////////////////////////////////////////////

function searchProduct() {
    var pNameCode = $('#pnameCode_txt').val();
    var pgListStr = '';
    var pColor = '';
    $('.pgproduct').each(function () {
        if ($(this).prop('checked')) {
            pgListStr += $(this).val() + '#';
        }
    });
    $('.pcolor').each(function () {
        if ($(this).prop('checked')) {
            pColor += $(this).val() + '#';
        }
    });
    var pfListStr = '';
    $('.pfg').children('div').children('div').children('.pf').each(function () {
        if ($(this).prop('checked')) {
            pfListStr += $(this).val() + '#';
        }
    });
    //console.log(pfListStr);
    // alert(pfListStr);
    if (pNameCode == "" && pColor == "" && pgListStr == "" && pfListStr == "") {
        //$('#product_div').html('');
        //getAllProductByPgName();
        alert("");
    }
    //alert(pColor);
    else {
        $.ajax({
            method: 'POST',
            url: '/Search/ArchiveSearch',
            data: { 'pnameCode': pNameCode, 'pColor': pColor, 'pgProduct': pgListStr, 'pf': pfListStr },
            success: function (response) {

            },
            error: function (response) {
                //showModalMessage('.modalMessage', 'خطای کلاینت', response, 'danger');
            }
        });
    }
}

//////////       ///////////         Supplier Register

function GeneralInformationSupplier() {

    var frm = $("#mainSupplierForm").valid();

    if (frm) {
        //var mainSuppliers = {
        //    FullName: $("#txtFullName").val(),
        //    Brand: $("#txtCompany").val(),
        //    SuppliersType: $("#supplierType_ddl").val(),
        //    CentralOfficeTell: $("#txtCentralOfficeTell").val(),
        //    SalesTell: $("#txtSalesTell").val(),
        //    FactoyTell: $("#txtFactoyTell").val(),
        //    CentralOfficFax: $("#txtCentralOfficFax").val(),
        //    SalesFax: $("#txtSalesFax").val(),
        //    FactoryFax: $("#txtFactoryFax").val(),
        //    CentralOfficePostalCode: $("#txtCentralOfficePostalCode").val(),
        //    SalesPostalCode: $("#txtSalesPostalCode").val(),
        //    FactoyPostalCode: $("#txtFactoyPostalCode").val(),
        //    PhoneNumber: $("#txtPhoneNumber").val(),
        //    Email: $("#txtEmail").val(),
        //    Website: $("#txtWebsite").val(),
        //    Address: $("#txtAddress").val(),
        //    Affordability: $('input[name=Affordability_rbl]:checked').val(),
        //    PersonnelNumber: $("#txtPersonnelNumber").val(),
        //    ExpertsNumber: $("#txtExpertsNumber").val(),
        //    TecknecianNumber: $("#txtTecknecianNumber").val(),
        //    QualityControlNumber: $("#txtQualityControlNumber").val()
        //}

        $("#Supplier_Form").addClass('hiddens');
        $("#SupplierPrint").removeClass('hiddens');
        $('html,body').animate({ 'scrollTop': '0px' }, 1000)

        SupplierFormFill();
    }
    else {
        return false;
    }
}

function ShareholderSuppliers() {
    var frm = $("#suppliersShareholderForm").valid();

    if (frm) {
        var type = $("#Type option:selected").text();
        var typeVal = $("#Type option:selected").val();
        var fullName = $("#FullName2").val();
        var percent = $("#PercentageShare").val();
        debugger;
        var i = $(".num :last").text();
        console.log(i);
        var tr = "";
        tr = ' <tr>' +
            '<th scope ="row" class="num">1</th>' +
            '<td>' + fullName + '</td>' +
            '<td data-id=' + typeVal + '>' + type + '</td>' +
            '<td>' + percent + '</td>' +
            '<td><i class="fa fa-remove btnshareholderSuppliersDelete"></i></td>' +
            '</tr >';
        $("#shareholderSuppliers_tr").append(tr);
        $("#shareholderSuppliers_tr2").append(tr);
        $("#suppliersShareholderForm")[0].reset();
    }
    else {
        return false;
    }
}

$("#shareholderSuppliers_tr").on('click', '.btnshareholderSuppliersDelete', function () {
    $(this).closest('tr').remove();
});


function ActivitySuppliers() {
    var frm = $("#activitySupplierForm").valid();

    if (frm) {
        var activityField = $("#ActivityField_ddl option:selected").text();
        var activity = $("#activity_ddl option:selected").text();
        var supplyCapacity = $("#SupplyCapacity").val();
        var activityFieldId = $("#ActivityField_ddl option:selected").val();
        var activityId = $("#activity_ddl option:selected").val();
        //debugger;
        //var i = $(".num :last").text();
        //console.log(i);
        var tr = "";
        tr = ' <tr>' +
            '<th scope ="row" class="num">1</th>' +
            '<td data-id=' + activityFieldId + '>' + activityField + '</td>' +
            '<td data-id=' + activityId + '>' + activity + '</td>' +
            '<td>' + supplyCapacity + '</td>' +
            '<td><i class="fa fa-remove btnActivitySuppliersDelete"></i></td>' +
            '</tr >';
        $("#activitySuppliers_tr").append(tr);
        $("#activitySuppliers_tr2").append(tr);
        $("#activitySupplierForm")[0].reset();
    }
    else {
        return false;
    }
}

$("#activitySuppliers_tr").on('click', '.btnActivitySuppliersDelete', function () {
    $(this).closest('tr').remove();
});

function GetAllActivity(Id) {
    $.ajax({
        url: "/Supplier/GetAllActivity",
        type: "Post",
        data: { id: Id },
        success: function (data) {

            var options = "";
            //options += "<option value='0'>زبان ...</option>";
            data.forEach(function (obj) {
                options += "<option value=" + obj.activity_ID + ">" + obj.activityTitle + "</option>";
            });
            $("#activity_ddl").html(options);
        },
        error: function (result) {
        }
    });
}

function ResumeSuppliers() {
    var frm = $("#resumeSupplierForm").valid();

    if (frm) {

        var Title = $("#txtCompanyName").val();
        var Employer = $("#txtEmployer").val();
        var CollaborationPeriod = $("#txtCollaborationPeriod").val();
        var ResumeDescription = $("#ResumeDescription").val();
        //var i = $(".num :last").text();
        //console.log(i);
        var tr = "";
        tr = ' <tr>' +
            '<th scope ="row" class="num">1</th>' +
            '<td>' + Title + '</td>' +
            '<td>' + Employer + '</td>' +
            '<td>' + CollaborationPeriod + '</td>' +
            '<td>' + ResumeDescription + '</td>' +
            '<td><i class="fa fa-remove btnResumeSuppliersDelete"></i></td>' +
            '</tr >';
        $("#resumeSuppliers_tr").append(tr);
        $("#resumeSuppliers_tr2").append(tr);
        $("#resumeSupplierForm")[0].reset();
    }
    else {
        return false;
    }
}

$("#resumeSuppliers_tr").on('click', '.btnResumeSuppliersDelete', function () {
    $(this).closest('tr').remove();
});

function CertificateSuppliers() {
    var frm = $("#certificatesSupplierForm").valid();

    if (frm) {

        var Title = $("#txtCertificateTitle").val();
        var Description = $("#Description").val();
        var Type = $("#certificateType_ddl option:selected").text();
        var TypeId = $("#certificateType_ddl").val();
        //debugger;
        //var i = $(".num :last").text();
        //console.log(i);
        var tr = "";
        tr = ' <tr>' +
            '<th scope ="row" class="num">1</th>' +
            '<td>' + Description + '</td>' +
            '<td>' + Title + '</td>' +
            '<td data-id=' + TypeId+'>' + Type + '</td>' +
            '<td><i class="fa fa-remove btnCertificateSuppliersDelete"></i></td>' +
            '</tr >';
        $("#certificateSuppliers_tr").append(tr);
        $("#certificateSuppliers_tr2").append(tr);
        $("#certificatesSupplierForm")[0].reset();
    }
    else {
        return false;
    }
}

$("#certificateSuppliers_tr").on('click', '.btnCertificateSuppliersDelete', function () {
    $(this).closest('tr').remove();
});



function SupplierFormFill() {
    $("#lblSupplierFullName").text($("#FullName").val());
    $("#lblBrand").text($("#Brand").val());
    $("#lblSuppType").text($("#supplierType_ddl option:selected").text());
    $("#lblCentralOfficeTell").text($("#CentralOfficeTell").val());
    $("#lblSalesTell").text($("#SalesTell").val());
    $("#lblFactoyTell").text($("#FactoyTell").val());
    $("#lblCentralOfficFax").text($("#CentralOfficFax").val());
    $("#lblSalesFax").text($("#SalesFax").val());
    $("#lblFactoryFax").text($("#FactoryFax").val());
    $("#lblCentralOfficePostalCode").text($("#CentralOfficePostalCode").val());
    $("#lblSalesPostalCode").text($("#SalesPostalCode").val());
    $("#lblFactoyPostalCode").text($("#FactoyPostalCode").val());
    $("#lblPhoneNumber").text($("#PhoneNumber").val());
    $("#lblEmail").text($("#Email").val());
    $("#lblWebsite").text($("#Website").val());
    $("#lblAddress").text($("#Address").val());
    $("#lblAffordability").text($('input[name=Affordability_rbl]:checked').attr('data-title'));
    $("#lblPersonnelNumber").text($("#PersonnelNumber").val());
    $("#lblExpertsNumber").text($("#ExpertsNumber").val());
    $("#lblTecknecianNumber").text($("#TecknecianNumber").val());
    $("#lblQualityControlNumber").text($("#QualityControlNumber").val());

    //$(".btnshareholderSuppliersDelete").closest('td').hide();
    //$(".btnActivitySuppliersDelete").closest('td').hide();
    //$(".btnResumeSuppliersDelete").closest('td').hide();
    //$(".btnCertificateSuppliersDelete").closest('td').hide();
    //$(".btnDeleteDocs").hide();
    //$("#shareholderSuppliers_div").html($("#shareholderSuppliers_tb").html());
}

function ComebackToSupplierForm() {
    $("#Supplier_Form").removeClass('hiddens');
    $("#SupplierPrint").addClass('hiddens');
    $('html,body').animate({ 'scrollTop': '0px' }, 1000)
}

function SaveSupplierData() {
    var fullName = [],type = [],percent = [];
    var activity = [],capacity = [];
    var company = [], employer = [], periods = [], descrip = [];
    var certificateDescrip = [], certiName = [], certiType = [];
    var docTitle = [], docFile = [];

    var mainSuppliers = {
        FullName: $("#FullName").val(),
        Brand: $("#Brand").val(),
        SuppliersType: $("#supplierType_ddl").val(),
        CentralOfficeTell: $("#CentralOfficeTell").val(),
        SalesTell: $("#SalesTell").val(),
        FactoyTell: $("#FactoyTell").val(),
        CentralOfficFax: $("#CentralOfficFax").val(),
        SalesFax: $("#SalesFax").val(),
        FactoryFax: $("#FactoryFax").val(),
        CentralOfficePostalCode: $("#CentralOfficePostalCode").val(),
        SalesPostalCode: $("#SalesPostalCode").val(),
        FactoyPostalCode: $("#FactoyPostalCode").val(),
        PhoneNumber: $("#PhoneNumber").val(),
        Email: $("#Email").val(),
        Website: $("#Website").val(),
        Address: $("#Address").val(),
        Affordability: $('input[name=Affordability_rbl]:checked').attr('data-title'),
        PersonnelNumber: $("#PersonnelNumber").val(),
        ExpertsNumber: $("#ExpertsNumber").val(),
        TecknecianNumber: $("#TecknecianNumber").val(),
        QualityControlNumber: $("#QualityControlNumber").val()
    }


    $("#shareholderSuppliers_tr").children('tr').each(function () {
        fullName.push($(this).children('td:eq(0)').html());
        type.push($(this).children('td:eq(1)').attr('data-id'));
        percent.push($(this).children('td:eq(2)').html());

    });

    $("#activitySuppliers_tr").children('tr').each(function () {
        activity.push($(this).children('td:eq(1)').attr('data-id'));
        capacity.push($(this).children('td:eq(2)').html());

    });

    $("#resumeSuppliers_tr").children('tr').each(function () {
        company.push($(this).children('td:eq(0)').html());
        employer.push($(this).children('td:eq(1)').html());
        periods.push($(this).children('td:eq(2)').html());
        descrip.push($(this).children('td:eq(3)').html());

    });

    $("#certificateSuppliers_tr").children('tr').each(function () {
        certificateDescrip.push($(this).children('td:eq(0)').html());
        certiName.push($(this).children('td:eq(1)').html());
        certiType.push($(this).children('td:eq(2)').attr('data-id'));

    });
    debugger;
    $("#docsPreviews").children('.myDoc').children('.forms-file-record').each(function () {
        docTitle.push($(this).children('p').attr('data-id'));
        docFile.push($(this).children('p').attr('data-file'));
    });
    //alert(certiType);
    //alert(docFile);
    //alert(type);
    //alert(percent);

    $.ajax({
        method: 'POST',
        url: '/Supplier/SupplierRegister',
        data: {
            mainSuppliers: mainSuppliers, 'fullName': fullName, 'types': type, 'percents': percent, 'activity': activity, 'capacity': capacity
            , 'company': company, 'employer': employer, 'periods': periods, 'descrip': descrip, 'certificateDescrip': certificateDescrip, 'certiName': certiName, 'certiType': certiType
            , 'docTitle': docTitle, 'docFile': docFile
        },
        success: function (response) {
            if (response == "ok") {
                $.toast({
                    heading: 'ثبت فرم ارزیابی',
                    text: 'اطلاعات شما با موفقیت ثبت گردید',
                    showHideTransition: 'slide',
                    icon: 'success'
                });
                $("#btnRegisterSupplier").addClass("disabled");
            }
        },
        error: function (response) {
        }
    });
}
//-------------------------------------- Complaint
function ComplaintRegister() {
    var frm = $("#complaintForm").valid();

    if (frm) {
        $("#complaintPrint_div").removeClass('hiddens');
        $("#complaints_div").addClass('hiddens');
        $('html,body').animate({ 'scrollTop': '0px' }, 1000);

        ComplaintsFormFill();
    }
    else {
        return false;
    }
}

function ComebackToSupplierForm() {
    $("#complaints_div").removeClass('hiddens');
    $("#complaintPrint_div").addClass('hiddens');
    $('html,body').animate({ 'scrollTop': '0px' }, 1000)
}

function ComplaintsFormFill() {
    $("#lblPerson").text($("#FullName").val());
    $("#lblCompanyName").text($("#CompanyName").val());
    $("#lblTell").text($("#Tell").val());
    $("#lblMobile").text($("#PhoneNumber").val());
    $("#lblFax").text($("#Fax").val());
    $("#lblEmail").text($("#Email").val());
    $("#lblAgancy").text($("#AgencyName").val());
    $("#lblAddress").text($("#Address").val());
    $("#lblDate").text($("#OccurrenceDate").val());
    $("#lblProduct").text($("#ProductId option:selected").text());
    $("#lblProductId").text($("#ProductId").val());
    $("#lblDescrip").text($("#Description").val());
    $("#lblRequest").text($("#Request").val());
}

function SaveComplaintsData() {
   
    var docTitle = [], docFile = [];

    var customerComplaintMaster = {
        FullName: $("#lblPerson").text(),
        CompanyName: $("#lblCompanyName").text(),
        AgencyName: $("#lblAgancy").text(),
        Tell: $("#lblTell").text(),
        PhoneNumber: $("#lblMobile").text(),
        Fax: $("#lblFax").text(),
        Email: $("#lblEmail").text(),
        Address: $("#lblAddress").text(),
        OccurrenceDate: $("#lblDate").text(),
        ProductId: $("#lblProductId").text(),
        Description: $("#lblDescrip").text(),
        Request: $("#lblRequest").text()
    }
   

    $("#docsComplaintPreviews").children('.myDoc').children('.forms-file-record').each(function () {
        docTitle.push($(this).children('p').text());
        docFile.push($(this).children('p').attr('data-file'));
    });
    //alert(certiType);
    //alert(docFile);
    //alert(type);
    //alert(percent);

    $.ajax({
        method: 'POST',
        url: '/Complaints/ComplaintsRegister',
        data: { customerComplaintMaster: customerComplaintMaster,'docTitle': docTitle, 'docFile': docFile},
        success: function (response) {
            if (response == "ok") {
                $.toast({
                    heading: 'ثبت فرم شکایات مشتریان',
                    text: 'اطلاعات شما با موفقیت ثبت گردید',
                    showHideTransition: 'slide',
                    icon: 'success'
                });
                $("#btnRegisterComplaints").addClass("disabled");
            }
        },
        error: function (response) {
        }
    });
}


// -----------------------------نظرسنجی تامین کنندگان
function SupplierSurvey() {
    
    var frm = $("#supplierSurveyFrm").valid();

    if (frm) {
        if (SupplierSurveyValidate()) {

            var surveySuppliersOtherData = {
                FullName: $("#FullName").val(),
                Company: $("#Company").val(),
                Proposal: $("#Proposal").val(),
                MainSurveyId: $("#lblMainSurveyId").text()
            }
            var votes = $('input[name=supplierSurvey_rbl]:checked').val();
            $.ajax({
                method: 'POST',
                url: '/Survey/SurveySubmit',
                data: { surveySuppliersOtherData: surveySuppliersOtherData, vote: votes },
                success: function (response) {
                    if (response == "ok") {
                        CheckValueOfRadiobutton();
                    }
                },
                error: function (response) {
                    //showModalMessage('.modalMessage', 'خطای کلاینت', response, 'danger');
                }
            });
        }

        else {
            $.toast({
                heading: 'هشدار',
                text: 'لطفا تمام سوالات را پاسخ دهید',
                showHideTransition: 'slide',
                icon: 'warning'
            });
        }
    }
    else {
        return false;
    }
}
function SupplierSurveyValidate() {
    var status = true;

    var questionIds = [];
    var selectedReplies = [];

    $('.notLabel').each(function () {
        if ($(this).find('input[type="radio"]:checked').length > 0) {
            questionIds.push($(this).find('input[type="radio"]:checked').attr('data-val'));
            selectedReplies.push($(this).find('input[type="radio"]:checked').attr('data-id'));
        }
        else {
            status = false;
        }
    });

    if (status != true) {
        return false;
    }
    return true;
}

function CheckValueOfRadiobutton() {
    var status = true;

    var questionIds=[];
    var selectedReplies=[];

    $('.notLabel').each(function () {
        if ($(this).find('input[type="radio"]:checked').length > 0) {
            selectedReplies.push($(this).find('input[type="radio"]:checked').attr('data-val'));
            questionIds.push($(this).find('input[type="radio"]:checked').attr('data-id'));
        }
        else {
            status = false;
        }
    });

    if (status != true) {
        $.toast({
            heading: 'هشدار',
            text: 'لطفا تمام سوالات را پاسخ دهید',
            showHideTransition: 'slide',
            icon: 'warning'
        });
    }
    else {

        $.ajax({
            method: 'POST',
            url: '/Survey/GetSurveyData',
            data: { questionId: questionIds, selectedReply: selectedReplies },
            success: function (response) {
                if (response == "ok") {
                    $.toast({
                        heading: 'نظرسنجی ',
                        text: 'با تشکر از شرکت شما در نظرسنجی سایت',
                        showHideTransition: 'slide',
                        icon: 'success'
                    });
                }
            },
            error: function (response) {
            }
        });
    }
}


function CustomerSurvey2() {

    var frm = $("#customerSurveyFrm").valid();

    if (frm) {
        if (SupplierSurveyValidate()) {
            debugger;
            var products = [];
            $("input:checkbox[name=products]:checked").each(function () {
                products.push($(this).attr('data-id'));
            });

            var surveyCustomerOtherData = {
                //ProductId: $("input[name=products]:checked").attr('data-id'),
                FullName: $("#FullName").val(),
                OfficePost: $("#OfficePost").val(),
                Proposal: $("#Proposal").val(),
                PhoneNumber: $("#PhoneNumber").val(),
                MainSurveyId: $("#lblMainSurveyId").text(),
                CompanyName: $('#CompanyName').val()
            }
            console.log(products);
            //var votes = $('input[name=supplierSurvey_rbl]:checked').val();
            $.ajax({
                method: 'POST',
                url: '/Survey/CustomerSurveySubmit',
                data: { surveyCustomerOtherData: surveyCustomerOtherData, product: products },
                success: function (response) {
                    if (response == "ok") {
                        CheckValueOfRadiobutton();
                    }
                },
                error: function (response) {
                    //showModalMessage('.modalMessage', 'خطای کلاینت', response, 'danger');
                }
            });
        }

        else {
            $.toast({
                heading: 'هشدار',
                text: 'لطفا تمام سوالات را پاسخ دهید',
                showHideTransition: 'slide',
                icon: 'warning'
            });
        }
    }
    else {
        return false;
    }
}
//////////////////////////////////// بخش استخدام
function EducationalRecords() {
    var frm = $("#educationalRecord").valid();

    if (frm) {
        var Evidence = $("#Evidence").val();
        var Field = $("#Field").val();
        var Average = $("#Average").val();
        var EndDate = $("#EndDate").val();
        var UniversityType = $("#UniversityType").val();
        var InstitutionName = $("#InstitutionName").val();
        var City_Country = $("#City_Country").val();

        var tr = "";
        tr = ' <tr>' +
            //'<th scope ="row" class="num">1</th>' +
            '<td>' + Evidence + '</td>' +
            '<td>' + Field + '</td>' +
            '<td>' + Average + '</td>' +
            '<td>' + EndDate + '</td>' +
            '<td>' + UniversityType + '</td>' +
            '<td>' + InstitutionName + '</td>' +
            '<td>' + City_Country + '</td>' +
            '<td><i class="fa fa-remove btnEducationalDelete"></i></td>' +
            '</tr >';
        $("#EducationalRecords_tr").append(tr);
        $("#educationalRecord")[0].reset();
    }
    else {
        return false;
    }
}

$("#EducationalRecords_tr").on('click', '.btnEducationalDelete', function () {
    $(this).closest('tr').remove();
});

function WorkExperience() {
    var frm = $("#workExperienceFrm").valid();

    if (frm) {
        var OrganizationName = $("#OrganizationName").val();
        var Post = $("#Post").val();
        var AssistPeriod = $("#AssistPeriod").val();
        var AssistCutDate = $("#AssistCutDate").val();
        var Tell = $("#Tell").val();
        var AverageSalary = $("#AverageSalary").val();
        var AssistCutReason = $("#AssistCutReason").val();

        var tr = "";
        tr = ' <tr>' +
            //'<th scope ="row" class="num">1</th>' +
            '<td>' + OrganizationName + '</td>' +
            '<td>' + Post + '</td>' +
            '<td>' + AssistPeriod + '</td>' +
            '<td>' + AssistCutDate + '</td>' +
            '<td>' + Tell + '</td>' +
            '<td>' + AverageSalary + '</td>' +
            '<td>' + AssistCutReason + '</td>' +
            '<td><i class="fa fa-remove btnWorkExperienceDelete"></i></td>' +
            '</tr >';
        $("#WorkExperience_tr").append(tr);
        $("#workExperienceFrm")[0].reset();
    }
    else {
        return false;
    }
}

$("#WorkExperience_tr").on('click', '.btnWorkExperienceDelete', function () {
    $(this).closest('tr').remove();
});

function TraningCourses() {
    var frm = $("#traningFrm").valid();

    if (frm) {
        var TraningName = $("#TraningName").val();
        var OrganizationName = $("#OrganizationNameTraning").val();
        var TraningPeroid = $("#TraningPeroid").val();
        var StartDate = $("#StartDate").val();
        var Description = $("#Description").val();
        var IsEvidence = $("input[name=IsEvidence]:checked").val();
        var IsEvidenceVal = $("input[name=IsEvidence]:checked").attr('data-val');

        var tr = "";
        tr = ' <tr>' +
            //'<th scope ="row" class="num">1</th>' +
            '<td>' + TraningName + '</td>' +
            '<td>' + OrganizationName + '</td>' +
            '<td>' + StartDate + '</td>' +
            '<td>' + TraningPeroid + '</td>' +
            '<td data-id=' + IsEvidence+'>' + IsEvidenceVal + '</td>' +
            '<td>' + Description + '</td>' +
            '<td><i class="fa fa-remove btnTraningCoursesDelete"></i></td>' +
            '</tr >';
        $("#Traning_tr").append(tr);
        $("#traningFrm")[0].reset();
    }
    else {
        return false;
    }
}

$("#Traning_tr").on('click', '.btnTraningCoursesDelete', function () {
    $(this).closest('tr').remove();
});

function GarantyName() {
    var frm = $("#garantyNamesFrm").valid();

    if (frm) {
        var FullName = $("#FullName").val();
        var Relation = $("#Relation").val();
        var Job = $("#Job").val();
        var Mobile = $("#Mobile").val();
        var WorkPlace = $("#WorkPlace").val();

        var tr = "";
        tr = ' <tr>' +
            //'<th scope ="row" class="num">1</th>' +
            '<td>' + FullName + '</td>' +
            '<td>' + Relation + '</td>' +
            '<td>' + Job + '</td>' +
            '<td>' + Mobile + '</td>' +
            '<td>' + WorkPlace + '</td>' +
            '<td><i class="fa fa-remove btnGarantyNameDelete"></i></td>' +
            '</tr >';
        $("#garanty_tr").append(tr);
        $("#garantyNamesFrm")[0].reset();
    }
    else {
        return false;
    }
}

$("#garanty_tr").on('click', '.btnGarantyNameDelete', function () {
    $(this).closest('tr').remove();
});

function Suppervisors() {
    var frm = $("#suppervisorFrm").valid();

    if (frm) {
        var FullName = $("#FullName2").val();
        var Jender = $("#Jender").val();
        var Relation = $("#Relation2").val();
        var BirthDate = $("#BirthDate2").val();
        var LevelEducation = $("#LevelEducation").val();
        var Job = $("#Job2").val();

        var tr = "";
        tr = ' <tr>' +
            //'<th scope ="row" class="num">1</th>' +
            '<td>' + FullName + '</td>' +
            '<td>' + Jender + '</td>' +
            '<td>' + Relation + '</td>' +
            '<td>' + BirthDate + '</td>' +
            '<td>' + LevelEducation + '</td>' +
            '<td>' + Job + '</td>' +
            '<td><i class="fa fa-remove btnSuppervisorsDelete"></i></td>' +
            '</tr >';
        $("#Supervisor_tr").append(tr);
        $("#suppervisorFrm")[0].reset();
    }
    else {
        return false;
    }
}
$("#Supervisor_tr").on('click', '.btnSuppervisorsDelete', function () {
    $(this).closest('tr').remove();
});

function SaveEmployement() {
    var frm = $("#basInformationFrm").valid();
    if (frm) {

        if (ValidateEmployementForm()) {
            var emp_BaseInformation = new FormData();
            var files = $("#Image").get(0).files;

            var workGarantyVal = "";
            var workGaranty = $("input[name='GarantyType_rbl']:checked").val();
            if (workGaranty == "3") {
                workGarantyVal = $("#txtWorkGaranty").val();
            }
            else {
                workGarantyVal = $("input[name='GarantyType_rbl']:checked").val();
            }


            emp_BaseInformation.append("FirstName", $("#FirstName").val()),
                emp_BaseInformation.append("LastName", $("#LastName").val()),
                emp_BaseInformation.append("FatherName", $("#FatherName").val()),
                emp_BaseInformation.append("Id_Cer", $("#Id_Cer").val()),
                emp_BaseInformation.append("BirthDate", $("#BirthDate").val()),
                emp_BaseInformation.append("NationalCode", $("#NationalCode").val()),
                emp_BaseInformation.append("TarikhSodorShenasname", $("#TarikhSodorShenasname").val()),
                emp_BaseInformation.append("MahaleSodorShenasname", $("#MahaleSodorShenasname").val()),
                emp_BaseInformation.append("BirthPlace", $("#BirthPlace").val()),
                emp_BaseInformation.append("Religion", $("#Religion").val()),
                emp_BaseInformation.append("MaritalStatus", $("input[name='MaritalStatus']:checked").val()),
                emp_BaseInformation.append("MaritalDescription", $("#MaritalDescription").val()),
                emp_BaseInformation.append("ChildrenDescription", $("#ChildrenDescription").val()),
                emp_BaseInformation.append("IsHealth", $("input[name='IsHealth']:checked").val()),
                emp_BaseInformation.append("HealthDescription", $("#HealthDescription").val()),
                emp_BaseInformation.append("Duty", $("input[name='Duty']:checked").val()),
                emp_BaseInformation.append("ExemptReason", $("#ExemptReason").val()),
                emp_BaseInformation.append("employementImage", files[0]),
                emp_BaseInformation.append("IsCriminalRecord", $("input[name='Arrest_rbl']:checked").val()),
                emp_BaseInformation.append("CriminalRecordReason", $("#txtCriminalRecordReason").val()),
                emp_BaseInformation.append("IsSmoking", $("input[name='IsSmoke_rbl']:checked").val()),
                emp_BaseInformation.append("SmokingReason", $("#txtSmokingReason").val()),
                emp_BaseInformation.append("EnglishRead", $("input[name='ReadEnglish_rbl']:checked").val()),
                emp_BaseInformation.append("EnglishWrite", $("input[name='WriteEnglish_rbl']:checked").val()),
                emp_BaseInformation.append("EnglishSpeake", $("input[name='SpeakeEnglish_rbl']:checked").val()),
                emp_BaseInformation.append("SelectedJob", $("#txtSelectedJob").val()),
                emp_BaseInformation.append("TypeCooperation", $("input[name='AssistType_rbl']:checked").val()),
                emp_BaseInformation.append("TimeCooperation", $("#txtTimeCooperation").val()),
                emp_BaseInformation.append("IsOvertime", $("#IsOvertime_ddl").val()),
                emp_BaseInformation.append("OvertimeHours", $("#txtOvertimeHours").val()),
                emp_BaseInformation.append("HolidayIsWork", $("#HolidayIsWork_ddl").val()),
                emp_BaseInformation.append("IsMissionInternal", $("#IsMissionInternal_ddl").val()),
                emp_BaseInformation.append("IsMissionExternal", $("#IsMissionExternal_ddl").val()),
                emp_BaseInformation.append("IsMissionExternal", $("#IsMissionExternal_ddl").val()),
                emp_BaseInformation.append("IsInsuranceHistory", $("input[name='IsInsurance_rbl']:checked").val()),
                emp_BaseInformation.append("InsuranceDescription", $("#txtInsuranceDescription").val()),
                emp_BaseInformation.append("MethodIntroduction", $("#txtMethodIntroduction").val()),
                emp_BaseInformation.append("IsSpecialPhysicalItems", $("input[name='SpecialIllness_rbl']:checked").val()),
                emp_BaseInformation.append("SpecialPhysicalItemsDescription", $("#txtSpecialPhysicalItemsDescription").val()),
                emp_BaseInformation.append("IsWorking", $("input[name='IsWorking_rbl']:checked").val()),
                emp_BaseInformation.append("WorkGuarantee", workGarantyVal),
                emp_BaseInformation.append("ExpectedSalary", $("#txtExpectedSalary").val()),
                emp_BaseInformation.append("SalaryType", $("input[name='SalaryType_rbl']:checked").val()),
                emp_BaseInformation.append("IsHeadFamily", $("input[name='IsSupervisor_rbl']:checked").val()),
                emp_BaseInformation.append("HomeType", $("input[name='HomeType_rbl']:checked").val()),
                emp_BaseInformation.append("HomeDescription", $("#txtHomeDescription").val()),
                emp_BaseInformation.append("Address", $("#txtAddress").val()),
                emp_BaseInformation.append("Tell", $("#txtTelll").val()),
                emp_BaseInformation.append("Mobile", $("#txtMobile").val())

            //for (var pair of emp_BaseInformation.entries()) {
            //    console.log(pair[0] + ', ' + pair[1]);
            //}     /// print formdata
            $.ajax({
                method: 'POST',
                url: '/Employement/SaveEmployement',
                data: emp_BaseInformation,
                contentType: false,
                processData: false,
                async: false,
                success: function (data) {
                    $("#div-loading").addClass('hidden');
                    if (data=="ok") {
                        SaveEducational();
                    }

                },
                error: function (result) {
                    alert(result.error);
                }
            });
        }
        else {
            $.toast({
                heading: 'فرم استخدام',
                text: 'اطلاعات فرم استخدام شما ناقص است',
                showHideTransition: 'slide',
                icon: 'warning'
            });
            return false;
        }
    }
    else {
        $.toast({
            heading: 'فرم استخدام',
            text: 'اطلاعات فرم استخدام شما ناقص است',
            showHideTransition: 'slide',
            icon: 'warning'
        });
        return false;
    }
}
function ValidateEmployementForm() {
    var flag = false;
    if (!$('input[name="Arrest_rbl"]').is(':checked')) {
        $("#lblArrest").text("لطفا وضعیت سو سابقه خود را انتخاب کنید");
        $("#lblIsSmoke").text("");
        $("#lblKnowEnglish").text("");
        $("#lblAssistType").text("");
        $("#lblIsInsurance").text("");
        $("#lblSpecialIllness").text("");
        $("#lblIsWorking").text("");
        $("#lblGarantyType").text("");
        $("#lblSalaryType").text("");
        $("#lblIsSupervisor").text("");
        $("#lblHomeType").text("");
        $("#lblAdresss").text("");
    }
    else if (!$('input[name="IsSmoke_rbl"]').is(':checked')) {
        $("#lblIsSmoke").text("لطفا وضعیت سیگاری بودن خود را انتخاب کنید");
        $("#lblArrest").text("");
        $("#lblKnowEnglish").text("");
        $("#lblAssistType").text("");
        $("#lblIsInsurance").text("");
        $("#lblSpecialIllness").text("");
        $("#lblIsWorking").text("");
        $("#lblGarantyType").text("");
        $("#lblSalaryType").text("");
        $("#lblIsSupervisor").text("");
        $("#lblHomeType").text("");
        $("#lblAdresss").text("");
    }
    else if (!$('input[name="ReadEnglish_rbl"]').is(':checked') || !$('input[name="WriteEnglish_rbl"]').is(':checked') || !$('input[name="SpeakeEnglish_rbl"]').is(':checked')) {
        $("#lblKnowEnglish").text("لطفا میزان آشنایی خود با زبان انگلیسی را انتخاب کنید");
        $("#lblArrest").text("");
        $("#lblIsSmoke").text("");
        $("#lblAssistType").text("");
        $("#lblIsInsurance").text("");
        $("#lblSpecialIllness").text("");
        $("#lblIsWorking").text("");
        $("#lblGarantyType").text("");
        $("#lblSalaryType").text("");
        $("#lblIsSupervisor").text("");
        $("#lblHomeType").text("");
        $("#lblAdresss").text("");
    }
    else if (!$('input[name="AssistType_rbl"]').is(':checked')) {
        $("#lblAssistType").text("لطفا نوع همکاری را انتخاب کنید");
        $("#lblArrest").text("");
        $("#lblIsSmoke").text("");
        $("#lblKnowEnglish").text("");
        $("#lblIsInsurance").text("");
        $("#lblSpecialIllness").text("");
        $("#lblIsWorking").text("");
        $("#lblGarantyType").text("");
        $("#lblSalaryType").text("");
        $("#lblIsSupervisor").text("");
        $("#lblHomeType").text("");
        $("#lblAdresss").text("");
    }
    else if (!$('input[name="IsInsurance_rbl"]').is(':checked')) {
        $("#lblIsInsurance").text("لطفا سابقه بیمه خود را انتخاب کنید");
        $("#lblArrest").text("");
        $("#lblIsSmoke").text("");
        $("#lblKnowEnglish").text("");
        $("#lblAssistType").text("");
        $("#lblSpecialIllness").text("");
        $("#lblIsWorking").text("");
        $("#lblGarantyType").text("");
        $("#lblSalaryType").text("");
        $("#lblIsSupervisor").text("");
        $("#lblHomeType").text("");
        $("#lblAdresss").text("");
    }
    else if (!$('input[name="SpecialIllness_rbl"]').is(':checked')) {
        $("#lblSpecialIllness").text("لطفا سابقه بیماری خود را انتخاب کنید");
        $("#lblArrest").text("");
        $("#lblIsSmoke").text("");
        $("#lblKnowEnglish").text("");
        $("#lblAssistType").text("");
        $("#lblIsInsurance").text("");
        $("#lblIsWorking").text("");
        $("#lblGarantyType").text("");
        $("#lblSalaryType").text("");
        $("#lblIsSupervisor").text("");
        $("#lblHomeType").text("");
        $("#lblAdresss").text("");
    }
    else if (!$('input[name="IsWorking_rbl"]').is(':checked')) {
        $("#lblIsWorking").text("لطفا وضعیت کاری جاری خود را انتخاب کنید");
        $("#lblArrest").text("");
        $("#lblIsSmoke").text("");
        $("#lblKnowEnglish").text("");
        $("#lblAssistType").text("");
        $("#lblIsInsurance").text("");
        $("#lblSpecialIllness").text("");
        $("#lblGarantyType").text("");
        $("#lblSalaryType").text("");
        $("#lblIsSupervisor").text("");
        $("#lblHomeType").text("");
        $("#lblAdresss").text("");
    }
    else if (!$('input[name="GarantyType_rbl"]').is(':checked')) {
        $("#lblGarantyType").text("لطفا نحوه تضمین کاری خود را انتخاب کنید");
        $("#lblArrest").text("");
        $("#lblIsSmoke").text("");
        $("#lblKnowEnglish").text("");
        $("#lblAssistType").text("");
        $("#lblIsInsurance").text("");
        $("#lblSpecialIllness").text("");
        $("#lblIsWorking").text("");
        $("#lblSalaryType").text("");
        $("#lblIsSupervisor").text("");
        $("#lblHomeType").text("");
        $("#lblAdresss").text("");
    }
    else if (!$('input[name="SalaryType_rbl"]').is(':checked')) {
        $("#lblSalaryType").text("لطفا نحوه دریافتی حقوق خود را انتخاب کنید");
        $("#lblArrest").text("");
        $("#lblIsSmoke").text("");
        $("#lblKnowEnglish").text("");
        $("#lblAssistType").text("");
        $("#lblIsInsurance").text("");
        $("#lblSpecialIllness").text("");
        $("#lblIsWorking").text("");
        $("#lblGarantyType").text("");
        $("#lblIsSupervisor").text("");
        $("#lblHomeType").text("");
        $("#lblAdresss").text("");
    }
    else if (!$('input[name="IsSupervisor_rbl"]').is(':checked')) {
        $("#lblIsSupervisor").text("لطفا سرپرست بودن خود را مشخص کنید");
        $("#lblArrest").text("");
        $("#lblIsSmoke").text("");
        $("#lblKnowEnglish").text("");
        $("#lblAssistType").text("");
        $("#lblIsInsurance").text("");
        $("#lblSpecialIllness").text("");
        $("#lblIsWorking").text("");
        $("#lblGarantyType").text("");
        $("#lblSalaryType").text("");
        $("#lblHomeType").text("");
        $("#lblAdresss").text("");
    }
    else if (!$('input[name="HomeType_rbl"]').is(':checked')) {
        $("#lblHomeType").text("لطفا یکی از موارد را انتخاب کنید");
        $("#lblArrest").text("");
        $("#lblIsSmoke").text("");
        $("#lblKnowEnglish").text("");
        $("#lblAssistType").text("");
        $("#lblIsInsurance").text("");
        $("#lblSpecialIllness").text("");
        $("#lblIsWorking").text("");
        $("#lblGarantyType").text("");
        $("#lblSalaryType").text("");
        $("#lblIsSupervisor").text("");
        $("#lblAdresss").text("");
    }
    //else if ($("#txtAddress").val() == "" || $("#txtTellllllll").val() == "" || $("#txtMobile").val() == "") {
    //    $("#lblAdresss").text("اطلاعات آدرس خود را تکمیل کنید");
    //    $("#lblArrest").text("");
    //    $("#lblIsSmoke").text("");
    //    $("#lblKnowEnglish").text("");
    //    $("#lblAssistType").text("");
    //    $("#lblIsInsurance").text("");
    //    $("#lblSpecialIllness").text("");
    //    $("#lblIsWorking").text("");
    //    $("#lblGarantyType").text("");
    //    $("#lblSalaryType").text("");
    //    $("#lblIsSupervisor").text("");
    //    $("#lblHomeType").text("");
    //}
    else
    {
        $("#lblAdresss").text("");
        $("#lblArrest").text("");
        $("#lblIsSmoke").text("");
        $("#lblKnowEnglish").text("");
        $("#lblAssistType").text("");
        $("#lblIsInsurance").text("");
        $("#lblSpecialIllness").text("");
        $("#lblIsWorking").text("");
        $("#lblGarantyType").text("");
        $("#lblSalaryType").text("");
        $("#lblIsSupervisor").text("");
        $("#lblHomeType").text("");
        flag = true;
    }
    return flag;
}

function SaveEducational() {
    var Evidence = [], Field = [], Average = [], EndDate = [], UniversityType = [], InstitutionName = [], City_Country = [];
    $("#EducationalRecords_tr").children('tr').each(function () {
        Evidence.push($(this).children('td:eq(0)').html());
        Field.push($(this).children('td:eq(1)').html());
        Average.push($(this).children('td:eq(2)').html());
        EndDate.push($(this).children('td:eq(3)').html());
        UniversityType.push($(this).children('td:eq(4)').html());
        InstitutionName.push($(this).children('td:eq(5)').html());
        City_Country.push($(this).children('td:eq(6)').html());
    });

    $.ajax({
        method: 'POST',
        async: false,
        url: '/Employement/SaveEducational',
        data: { 'evidence': Evidence, 'field': Field, 'average': Average, 'endDate': EndDate, 'universityType': UniversityType, 'institutionName': InstitutionName, 'city_Country': City_Country },
        success: function (response) {
            if (response == "ok") {
                SaveWorkExperience();
            }
        },
        error: function (response) {
        }
    });
}

function SaveWorkExperience() {

    var OrganizationName = [], Post = [], AssistPeriod = [], AssistCutDate = [], Tell = [], AverageSalary = [], AssistCutReason = [];
    $("#WorkExperience_tr").children('tr').each(function () {
        OrganizationName.push($(this).children('td:eq(0)').html());
        Post.push($(this).children('td:eq(1)').html());
        AssistPeriod.push($(this).children('td:eq(2)').html());
        AssistCutDate.push($(this).children('td:eq(3)').html());
        Tell.push($(this).children('td:eq(4)').html());
        AverageSalary.push($(this).children('td:eq(5)').html());
        AssistCutReason.push($(this).children('td:eq(6)').html());
    });

    $.ajax({
        method: 'POST',
        url: '/Employement/SaveWorkExperience',
        data: { 'organizationName': OrganizationName, 'post': Post, 'assistPeriod': AssistPeriod, 'assistCutDate': AssistCutDate, 'tell': Tell, 'averageSalary': AverageSalary, 'assistCutReason': AssistCutReason },
        success: function (response) {
            if (response == "ok") {
                SaveTraningCourses();
            }
        },
        error: function (response) {
        }
    });
}

function SaveTraningCourses() {
    var OrganizationName = [], TraningName = [], StartDate = [], TraningPeroid = [], IsEvidence = [], Description = [];
    $("#Traning_tr").children('tr').each(function () {
        TraningName.push($(this).children('td:eq(0)').html());
        OrganizationName.push($(this).children('td:eq(1)').html());
        StartDate.push($(this).children('td:eq(2)').html());
        TraningPeroid.push($(this).children('td:eq(3)').html());
        IsEvidence.push($(this).children('td:eq(4)').attr('data-id'));
        Description.push($(this).children('td:eq(5)').html());
    });

    $.ajax({
        method: 'POST',
        url: '/Employement/SaveTraningCourses',
        data: { 'organizationName': OrganizationName, 'traningName': TraningName, 'startDate': StartDate, 'traningPeroid': TraningPeroid, 'isEvidence': IsEvidence, 'description': Description},
        success: function (response) {
            if (response == "ok") {
                SaveGarantyName();
            }
        },
        error: function (response) {
        }
    });
}

function SaveGarantyName() {
    var FullName = [], Relation = [], Job = [], Mobile = [], WorkPlace = [];
    $("#garanty_tr").children('tr').each(function () {
        FullName.push($(this).children('td:eq(0)').html());
        Relation.push($(this).children('td:eq(1)').html());
        Job.push($(this).children('td:eq(2)').html());
        Mobile.push($(this).children('td:eq(3)').html());
        WorkPlace.push($(this).children('td:eq(4)').html());
    });

    $.ajax({
        method: 'POST',
        url: '/Employement/SaveGarantyName',
        data: { 'fullName': FullName, 'relation': Relation, 'job': Job, 'mobile': Mobile, 'workPlace': WorkPlace},
        success: function (response) {
            if (response == "ok") {
                SaveSuppervisors();
            }
        },
        error: function (response) {
        }
    });
}

function SaveSuppervisors() {
    var docTitle = [], docFile = [];
    var SkillComputers = [], Activities=[];
    var FullName = [], Relation = [], Job = [], Jender = [], BirthDate = [], LevelEducation = [];
    $("#Supervisor_tr").children('tr').each(function () {
        FullName.push($(this).children('td:eq(0)').html());
        Jender.push($(this).children('td:eq(1)').html());
        Relation.push($(this).children('td:eq(2)').html());
        BirthDate.push($(this).children('td:eq(3)').html());
        LevelEducation.push($(this).children('td:eq(4)').html());
        Job.push($(this).children('td:eq(5)').html());
    });

    $("input[name='skillComputer_rbl']:checked").each(function () {
        SkillComputers.push($(this).val());
    });

    $("input[name='activityItems_rbl']:checked").each(function () {
        Activities.push($(this).val());
    });

    $("#docsPreviews").children('.myDoc').children('.forms-file-record').each(function () {
        docTitle.push($(this).children('p').text());
        docFile.push($(this).children('p').attr('data-fn'));
    });

    $.ajax({
        method: 'POST',
        url: '/Employement/SaveSuppervisors',
        data: { 'fullName': FullName, 'relation': Relation, 'job': Job, 'jender': Jender, 'birthDate': BirthDate, 'levelEducation': LevelEducation, 'skillComputers': SkillComputers, 'activities': Activities, 'docTitle': docTitle, 'docFile': docFile},
        success: function (response) {
            if (response == "ok") {
                $.toast({
                    heading: 'فرم استخدام',
                    text: 'اطلاعات فرم استخدام شما با موفقیت ثبت شد',
                    showHideTransition: 'slide',
                    icon: 'success'
                });
            }
        },
        error: function (response) {
        }
    });
}

function test() {
    console.log($("#txtTelll").val());
}
$(".menuLevel12").first().children("a").children("span").removeClass("fa-caret-left").removeClass("p-spam").addClass("fa-caret-down").addClass("pr-2");

$('#kadr>img').css('max-width', '418px');
$('#kadr>img').css('min-width', '418px');


$('#kadr2>img').css('max-width', '576px');
$('#kadr2>img').css('min-width', '576px');