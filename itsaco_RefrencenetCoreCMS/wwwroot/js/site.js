﻿function RegisterInNewsLetter() {
    var email = $("#EmailNewsLetter").val();
    if (email == "") {
        $.toast({
            heading: 'عضویت در خبرنامه',
            text: 'ایمیل خود را وارد کنید',
            showHideTransition: 'plain',
            icon: 'error',
            stack: false,
            position: 'mid-center',
            textAlign: 'center',
        }); return false;
    }
    if (!validateEmail(email)) {
        $.toast({
            heading: 'عضویت در خبرنامه',
            text: 'ایمیل وارد شده نامعتبر است',
            showHideTransition: 'plain',
            icon: 'error',
            stack: false,
            position: 'mid-center',
            textAlign: 'center',
        });
        return false;
    }
    debugger;
    $.ajax({
        method: 'POST',
        url: '/Home/RegisterInNewsLetter',
        data: { email: email },
        success: function (data) {
            if (data.res) {
                $.toast({
                    heading: 'عضویت در خبرنامه',
                    text: 'ایمیل شما در خبرنامه ثبت شد',
                    showHideTransition: 'plain',
                    stack: false,
                    icon: 'success',
                    position: 'mid-center',
                    textAlign: 'center',
                });
                $("#EmailNewsLetter").text("");

            }

            else {
                $.toast({
                    heading: 'عضویت در خبرنامه',
                    text: 'ایمیل شما قبلا در خبرنامه ثبت شده',
                    showHideTransition: 'plain',
                    stack: false,
                    icon: 'warning',
                    position: 'mid-center',
                    textAlign: 'center',
                })

            }
        },
        error: function (result) {
        }
    });

}
function validateEmail(email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test(email);
}
function RenderProduct_SubMenu()
{
    alert("")
}