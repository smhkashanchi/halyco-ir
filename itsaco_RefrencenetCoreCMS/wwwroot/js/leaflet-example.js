﻿/*
 the script mus be loaded after the map div is defined.
 otherwise this will not work (we would need a listener to
 wait for the DOM to be fully loaded).

 Just put the script tag below the map div.

 The source code below is the example from the leaflet start page.
 */
var map;
var lat = $("#lblLat").text(), lon = $("#lblLong").text();

if (lat != "" && lon != "") {
    map = L.map('map').setView([lat, lon], 13);
}
else {

    map = L.map('map').setView([33.997243, 51.434958], 13);
}

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

if (lat != "" && lon != "") {
    L.marker([lat, lon]).addTo(map)
        .bindPopup('Marked')
        .openPopup();

}


/////////////
function InsertMap() {
    var frm = $("#mapFrm").valid();
    if (frm == true) {
        $("#div-loading").removeClass('hidden');

        var map = {
            ID: $("#ID").val(),
            Latitude: $("#Latitude").val(),
            longitude: $("#longitude").val(),
            Type: $("#Type").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Connection/InsertOrUpdateMap',
            data: map,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                if (data == "add") {
                    swal({

                        type: 'success',
                        title: "نقشه سایت",
                        text: "نقشه سایت شما با موفقیت ثبت شد",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });
                }
                else if (data == "update") {
                    swal({

                        type: 'success',
                        title: "نقشه سایت",
                        text: "نقشه سایت شما با موفقیت ویرایش شد",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

////
function GetLocationStatus(type) {
    //$.get("/Admin/Connection/GetLocationStatus/" + type, function (res) {
    //    debugger;
    //    $("#Latitude").val(res.lat);
    //    $("#longitude").val(res.longi);

    //    if (res.lat != "" && res.longi != "") {
    //        map.off();
    //        map.remove();                          ///Destroy Map
    //        map = L.map('map').setView([res.lat, res.longi], 13);
    //    }
    //    else {

    //        map = L.map('map').setView([33.997243, 51.434958], 13);
    //    }

    //    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    //        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    //    }).addTo(map);

    //    if (res.lat != "" && res.longi != "") {
    //        L.marker([res.lat, res.longi]).addTo(map)
    //            .bindPopup('Marked')
    //            .openPopup();
    //    }

    //});
    window.location.href = "/Admin/Connection/Map/" + type;
}