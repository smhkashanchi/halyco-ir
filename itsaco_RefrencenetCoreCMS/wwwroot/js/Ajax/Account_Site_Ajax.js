﻿function RegisterInLogin() {
    $.ajax({
        url: "/Account/Register",
        type: "Get",
    }).done(function (result) {
        $('#modal-info').modal('show');
        $('#modal-body').html(result);

    })
}
function Register() {
    debugger;
    $.ajax({
        url: "/Account/Register",
        type: "Get",
    }).done(function (result) {
        $('#modal-info').modal('show');
        $('#modal-body').html(result);

    })
}
function Login() {
    debugger;
    $.ajax({
        url: "/Account/Login",
        type: "Get",
    }).done(function (result) {
        debugger;
        console.log(result);
        $('#modal-info').modal('show');
        $('#modal-body').html(result);

    })
}
function Login_Success(data) {
    debugger;
    if (data.return_status) {
            window.location.href = '/Home/Index';
    }
    else {
        debugger;
        $("#password").val("");
        grecaptcha.reset();
        swal({

            type: 'warning',
            title: "ورود کاربر",
            text: data.message,
            showConfirmButton: true,
            confirmButtonText: "تائید",
        })
        debugger;
        $(".swal2-container").css('z-index:100000');

    }
}
function Login_Failure() {
    grecaptcha.reset();

    debugger;
    swal("خطا", "خطا در ارسال اطلاعات دوباره سعی کنید", "error");

}
function Register_Success(data) {
    debugger;
    if (data.res) {

        swal({

            type: 'success',
            title: "ثبت نام کاربر",
            text: "حساب کاربری شما با موفقیت ثبت شد",
            showConfirmButton: true,
            confirmButtonText: "تائید",
        }).then(function () {
            window.location.href = '/Home/Index/';
        });
    }
    else
    {
        swal({

            type: 'warning',
            title: "ثبت نام کاربر",
            text: data.message,
            showConfirmButton: true,
            confirmButtonText: "تائید",
        })
        grecaptcha.reset();

    }
 
}
function Register_Failure() {

    debugger;
    swal("خطا", "خطا در ارسال اطلاعات دوباره سعی کنید", "error");

}
//$(function () {
//    $('input').iCheck({
//        checkboxClass: 'icheckbox_square-blue',
//        radioClass: 'iradio_square-blue',
//        increaseArea: '20%' // optional
//    });
//});
function ForgotPassword_Success(data) {
    debugger;
    if (data.return_status) {
        window.location.href = '/Account/ForgotPasswordConfirmation';
    }
    else {
        debugger;
        swal({

            type: 'warning',
            title: "بازیابی رمز عبور",
            text: data.message,
            showConfirmButton: true,
            confirmButtonText: "تائید",
        })
        debugger;
        $(".swal2-container").css('z-index:100000');

    }
}
function ForgotPassword_Failure() {

    debugger;
    swal("خطا", "خطا در ارسال اطلاعات دوباره سعی کنید", "error");

}
function ResetPassword_Success(data) {
    debugger;
    if (data.status) {


        var url = "/Account/ResetPasswordConfirmation";
        window.location.href = url;
    }
    else {
        swal({

            type: 'warning',
            title: "تغییر رمز عبور",
            text: "ایمیل یا رمز عبور شما اشتباه است",
            showConfirmButton: true,
            confirmButtonText: "تائید",
        })
        $("#Email").val("");
        $("#Pass").val("");
        $("#ConfirmPass").val("");
    }


}
function ResetPassword_Failure() {

    debugger;
    swal("خطا", "خطا در ارسال اطلاعات دوباره سعی کنید", "error");

}
