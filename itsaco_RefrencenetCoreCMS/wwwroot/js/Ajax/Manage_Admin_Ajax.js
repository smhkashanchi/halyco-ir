﻿
function Create_Success(data) {
    debugger;
    if (data.return_status) {
        $(function () {
            $("[id='UserFullName']").text(data.fullName);

        });
        debugger;

        swal({

            type: 'success',
            title: "ثبت اطلاعات",
            text: "اطلاعات با موفقیت ثبت شد",
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
       
    }
    else {

        swal({

            type: 'warning',
            title: "ثبت اطلاعات",
            text: data.message,
            showConfirmButton: true,
            confirmButtonText: "تائید",
        });
        $(".swal2-container").css('z-index:100000');

    }
}

function Create_Failure() {

    swal("خطا", "خطا در ارسال اطلاعات دوباره سعی کنید", "error");
}

//------------< jQuery - script > ------------

function ChangePassword() {

    var data = new FormData();
    debugger;
    var isvalid = $("#Form_Password").valid();  // Tells whether the form is valid
    if (isvalid === false)
        return false;
    else {
        data.append("OldPassword", $("#OldPassword").val());
        data.append("NewPassword", $("#NewPassword").val());
        data.append("ConfirmPassword", $("#ConfirmPassword").val());

        $.ajax({

            type: "Post",
            url: "/Admin/Manage/ChangePassword",
            contentType: false,
            processData: false,
            data: data,
            success: function (data) {
                if (data.result) {

                    debugger;
                    swal({

                        type: 'success',
                        title: "تغییر رمز عبور  ",
                        text: "رمز عبور با موفقیت ویرایش شد",
                        showConfirmButton: true,
                        confirmButtonText: "تائید",
                    })
                    $("#OldPassword").val("");
                    $("#NewPassword").val("");
                    $("#ConfirmPassword").val("");

                }
                else {
                    swal({

                        type: 'warning',
                        title: "تغییر رمز عبور  ",
                        text: data.message,
                        showConfirmButton: true,
                        confirmButtonText: "تائید",
                    })
                }
            },

            error: function (e) {
                swal("خطا", "خطا در ارسال اطلاعات دوباره سعی کنید", "error");
            }

        });
    }
}




//------------< jQuery - script > ------------
//$("#hasUserPhoto_Div").on("click", "#upload_userPhoto", function () {
function UploadUserPhoto() {

    var data = new FormData();
    var files = $("#files").get(0).files;
    var isvalid = $("#Form_UserPhoto").valid();  // Tells whether the form is valid
    if (isvalid === false)
        return false;
    else {
        data.append("UserPhoto", files[0]);
        $.ajax({

            type: "Post",

            url: "/Admin/Manage/UploadUserPhoto",

            contentType: false,

            processData: false,

            data: data,

            success: function (data) {
                debugger;
                $("[id='userphoto']").attr("src", data.userPhoto_Address);

                var div = '<input type="button" value="ویرایش" class="btn btn-instagram" id="upload_userPhoto" onclick="UploadUserPhoto()" /> <button type="button" value="" class="btn btn-danger" onclick="ChangePic()" id="remove_userPhoto">حذف تصویر پروفایل</button>';
                $("#hasUserPhoto_Div").html(div);

                $("#files").val("");

                swal({

                    type: 'success',
                    title: "ویرایش تصویر پروفایل ",
                    text: "تصویر پروفایل با موفقیت ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید",
                })

            },

            error: function (e) {
                swal("خطا", "خطا در ارسال اطلاعات دوباره سعی کنید", "error");
            }

        });
    }
};

function ChangePic() {
    $.ajax({

        type: "Post",

        url: "/Admin/Manage/RemoveUserPhoto",

        contentType: false,

        processData: false,

        success: function (data) {
            debugger;
            $(function () {
                $("[id='userphoto']").attr("src", data.userPhoto_Address);
            });
            debugger;
            if (document.getElementById("remove_userPhoto") !== null) {
                $("#remove_userPhoto").remove();
                debugger;
                $("#files").val("");

            }
            swal({

                type: 'success',
                title: "ویرایش تصویر پروفایل",
                text: "حذف تصویر پروفایل با موفقیت انجام شد",
                showConfirmButton: true,
                confirmButtonText: "تائید",
            })

        },

        error: function (e) {
            swal("خطا", "خطا در ارسال اطلاعات دوباره سعی کنید", "error");
        }

    });

}

function ChangeUsername() {

    var data = new FormData();
    var isvalid = $("#Form_UserName").valid();  // Tells whether the form is valid
    debugger;
    if (isvalid === false)
        return false;
    
        debugger;
        data.append("NewUserName", $("#NewUserName").val());
        data.append("ConfirmUserName", $("#ConfirmUserName").val());
        data.append("Password", $("#Password").val());
        $.ajax({

            type: "Post",
            url: "/Admin/Manage/ChangeUserName",
            contentType: false,
            processData: false,
            data: data,
            success: function (data) {
                if (data.result) {

                    swal({

                        type: 'success',
                        title: "تغییر نام کاربری  ",
                        text: "نام کاربری با موفقیت ویرایش شد",
                        showConfirmButton: true,
                        confirmButtonText: "تائید",
                    }).then(function () {
                        window.location.href = '/Admin/Account/Login/';
                    });


                }
                else {
                    $("#Password").val("")
                    swal({

                        type: 'warning',
                        title: "تغییر نام کاربری  ",
                        text: data.message,
                        showConfirmButton: true,
                        confirmButtonText: "تائید",
                    })
                }
            }
        ,

            error: function (e) {
                swal("خطا", "خطا در ارسال اطلاعات دوباره سعی کنید", "error");
            }

        });
    

}

