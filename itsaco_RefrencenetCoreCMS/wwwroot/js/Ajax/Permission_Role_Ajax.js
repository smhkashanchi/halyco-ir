﻿$(document).ready(function () {

    $(function () {
        $('#tree').bonsai({
            expandAll: false,
            checkboxes: true,
            createInputs: 'checkbox'
        });
        debugger;
        $('form').submit(function () {
            var i = 0, j = 0;
            $('.controller > input[type="checkbox"]:checked, .controller > input[type="checkbox"]:indeterminate').each(function () {
                debugger;
                var controller = $(this);
                if ($(controller).prop('indeterminate')) {
                    $(controller).prop("checked", true);
                }
                var controllerName = 'SelectedControllers[' + i + ']';
                $(controller).prop('name', controllerName + '.Name');
                debugger;
                var area = $(controller).next().next();
                $(area).prop('name', controllerName + '.AreaName');
                debugger;
                $('ul > li > input[type="checkbox"]:checked', $(controller).parent()).each(function () {
                    var action = $(this);
                    var actionName = controllerName + '.Actions[' + j + '].Name';
                    $(action).prop('name', actionName);
                    j++;
                });
                j = 0;
                i++;
            });

            return true;
        });
    });
});
function ChangeAccess(controller) {
    var p = $(controller).parent();
    var id = p.get(0).id;


    if ($("#" + id + " :checkbox[value='Index']").is(':checked')) {
        $("#" + id + " :checkbox").each(function () {
            $("#" + id + " :checkbox").prop("checked", true);
        });
    }
    else {
        $("#" + id + " :checkbox").each(function () {
            $("#" + id + " :checkbox").prop("checked", false);
        });
    }


    
        //if ($(".NewsLetters:checkbox[value='Index']").is(':checked')) {

        //    $(":checkbox[value='ViewAllGroupList']").attr("checked", true);
        //    $(":checkbox[value='ViewAllMemberOfNewsLetterByGroupId']").attr("checked", true);
        //}
        //else
        //{
        //    $(":checkbox[value='ViewAllGroupList']").attr("checked", false);
        //    $(":checkbox[value='ViewAllMemberOfNewsLetterByGroupId']").attr("checked", false);
        //}




}
function ChangeAccessActions(controller) {
    var p = $(controller).parent();
    var id = p.get(0).id;
    //Gallery
    if (id == "Gallery") {
        if ($("#" + id + " :checkbox[value='ViewAllGalleryPicture']").is(':checked')) {

            $("#" + id + " :checkbox[value='RemoveFileFromTemp']").prop("checked", true);
            $("#" + id + " :checkbox[value='RemoveFile']").prop("checked", true);
        }
        else {
            $("#" + id + " :checkbox[value='RemoveFileFromTemp']").prop("checked", false);
            $("#" + id + " :checkbox[value='RemoveFile']").prop("checked", false);
        }
        //*************************
        if ($("#" + id + " :checkbox[value='ViewAllGallery']").is(':checked')) {

            $("#" + id + " :checkbox[value='ViewAllGroupGallery']").prop("checked", true);
        }
        else {
            $("#" + id + " :checkbox[value='ViewAllGroupGallery']").prop("checked", false);
        }
        //********************

    }
    if (id == "Language") {
        if (!$("#" + id + " :checkbox[value='GetAllLangForLayoutPage']").is(':checked')) {

            $("#" + id + " :checkbox[value='GetAllLangForLayoutPage']").prop("checked", true);
        }
    }
    if (id == "Location") {
        if ($("#" + id + " :checkbox[value='ViewAllState']").is(':checked')) {

            $("#" + id + " :checkbox[value='GetAllCountry']").prop("checked", true);
        }
        else {
            $("#" + id + " :checkbox[value='GetAllCountry']").prop("checked", false);
        }

        //*************************
        if ($("#" + id + " :checkbox[value='ViewAllCity']").is(':checked')) {

            $("#" + id + " :checkbox[value='GetAllState']").prop("checked", true);
        }
        else {
            $("#" + id + " :checkbox[value='GetAllState']").prop("checked", false);
        }
        //********************

    }
    if (id == "Off") {
        if ($("#" + id + " :checkbox[value='ViewAll']").is(':checked')) {

            $("#" + id + " :checkbox[value='ViewAllOffList']").prop("checked", true);
        }
        else {
            $("#" + id + " :checkbox[value='ViewAllOffList']").prop("checked", false);
        }

    }
    if (id == "Producer") {
        if ($("#" + id + " :checkbox[value='ViewAll']").is(':checked')) {

            $("#" + id + " :checkbox[value='ViewAllProducerList']").prop("checked", true);
            $("#" + id + " :checkbox[value='DeleteFilesInProj']").prop("checked", true);
        }
        else {
            $("#" + id + " :checkbox[value='ViewAllProducerList']").prop("checked", false);
            $("#" + id + " :checkbox[value='DeleteFilesInProj']").prop("checked", true);

        }

    }
    if (id == "SlideShow") {
        if ($("#" + id + " :checkbox[value='SlideShowViewAll']").is(':checked')) {

            $("#" + id + " :checkbox[value='ViewAllSSG']").prop("checked", true);
            $("#" + id + " :checkbox[value='DeleteFilesInProj']").prop("checked", true);
            $("#" + id + " :checkbox[value='DeleteFiles']").prop("checked", true);
            $("#" + id + " :checkbox[value='checkboxSelected']").prop("checked", true);
        }
        else {
            $("#" + id + " :checkbox[value='ViewAllSSG']").prop("checked", false);
            $("#" + id + " :checkbox[value='DeleteFilesInProj']").prop("checked", false);
            $("#" + id + " :checkbox[value='DeleteFiles']").prop("checked", false);
            $("#" + id + " :checkbox[value='checkboxSelected']").prop("checked", false);
        }

    }
    if (id == "Unit") {
        if ($("#" + id + " :checkbox[value='ViewAll']").is(':checked')) {

            $("#" + id + " :checkbox[value='ViewAllProducerList']").prop("checked", true);
        }
        else {
            $("#" + id + " :checkbox[value='ViewAllProducerList']").prop("checked", false);
        }

    if (id == "Unit") {
        if ($("#" + id + " :checkbox[value='ViewAll']").is(':checked')) {

            $("#" + id + " :checkbox[value='ViewAllProducerList']").prop("checked", true);
        }
        else {
            $("#" + id + " :checkbox[value='ViewAllProducerList']").prop("checked", false);
        }

    }
    }
    if (id == "Products") {
        if ($("#" + id + " :checkbox[value='EditMainProduct']").is(':checked')) {
            $("#" + id + " :checkbox[value='GetProductImageSizesViewComponent']").prop("checked", true);
            $("#" + id + " :checkbox[value='EditProduct']").prop("checked", true);
        }
        else {
            $("#" + id + " :checkbox[value='GetProductImageSizesViewComponent']").prop("checked", true);
            $("#" + id + " :checkbox[value='EditProduct']").prop("checked", false);
        }
        //************************
        if ($("#" + id + " :checkbox[value='CreateMainProduct']").is(':checked')) {
            $("#" + id + " :checkbox[value='GetProductImageSizesViewComponent']").prop("checked", true);
            $("#" + id + " :checkbox[value='CreateProduct']").prop("checked", true);
        }
        else {
            $("#" + id + " :checkbox[value='GetProductImageSizesViewComponent']").prop("checked", true);
            $("#" + id + " :checkbox[value='CreateProduct']").prop("checked", false);
        }
        //************************
        if ($("#" + id + " :checkbox[value='ViewAllProductColor']").is(':checked')) {

            $("#" + id + " :checkbox[value='GetAllColorForAdd']").prop("checked", true);
        }
        else {
            $("#" + id + " :checkbox[value='GetAllColorForAdd']").prop("checked", false);
        }
        //************************
      
      
    }
    if (id == "Catalog") {
        if ($("#" + id + " :checkbox[value='ViewAllCatalog']").is(':checked')) {

            $("#" + id + " :checkbox[value='DeleteFilesInProj']").prop("checked", true);
        }
        else {
            $("#" + id + " :checkbox[value='DeleteFilesInProj']").prop("checked", false);
        }
        
    }
    if (id == "Projects") {
        if ($("#" + id + " :checkbox[value='ViewAllVideo']").is(':checked')) {

            $("#" + id + " :checkbox[value='createTreeViewVideo']").prop("checked", true);
        }
        else {
            $("#" + id + " :checkbox[value='createTreeViewVideo']").prop("checked", false);
        }

    }

    

}
function Permission_Success(data) {
    debugger;
    if (data.return_status) {
        debugger;
        $('#modal-info').modal('hide');
        $(".modal-backdrop").remove();
        swal({

            type: 'success',
            title: "ثبت اطلاعات",
            text: "اطلاعات با موفقیت ثبت شد",
            showConfirmButton: true,
            confirmButtonText: "تائید",
        })

    }
    else {

        swal({

            type: 'warning',
            title: "ثبت اطلاعات",
            text: data.message,
            showConfirmButton: true,
            confirmButtonText: "تائید",
        })
        debugger;
        $(".swal2-container").css('z-index:100000');

    }
}
function Permission_Failure() {

    debugger;
    swal("خطا", "خطا در ارسال اطلاعات دوباره سعی کنید", "error");

}

