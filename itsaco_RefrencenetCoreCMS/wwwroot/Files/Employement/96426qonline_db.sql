USE [master]
GO
/****** Object:  Database [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF]    Script Date: 12/05/2018 09:18:12 ق.ظ ******/
CREATE DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'db_OnlineQ', FILENAME = N'F:\db_OnlineQ.mdf' , SIZE = 3328KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'db_OnlineQ_log', FILENAME = N'F:\C__USERS_MEHDI MAJD_DESKTOP_NEW FOLDER (7)_ONLINEQ_APP_DATA_DB_ONLINEQ.MDF_log.ldf' , SIZE = 3456KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET ARITHABORT OFF 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET  DISABLE_BROKER 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET  MULTI_USER 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET DB_CHAINING OFF 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF', N'ON'
GO
USE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF]
GO
/****** Object:  Table [dbo].[tbl_answer]    Script Date: 12/05/2018 09:18:12 ق.ظ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_answer](
	[id_answer] [int] IDENTITY(1,1) NOT NULL,
	[id_question] [int] NULL,
	[username] [nvarchar](50) NULL,
	[espolid_answer] [nvarchar](10) NULL,
	[emteyaz_question] [nvarchar](10) NULL,
 CONSTRAINT [PK_tbl_answer] PRIMARY KEY CLUSTERED 
(
	[id_answer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_book]    Script Date: 12/05/2018 09:18:12 ق.ظ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_book](
	[id_book] [int] IDENTITY(1,1) NOT NULL,
	[category] [nvarchar](50) NULL,
	[subcategory] [nvarchar](50) NULL,
	[book] [nvarchar](50) NULL,
	[book_total] [int] NULL,
 CONSTRAINT [PK_tbl_book] PRIMARY KEY CLUSTERED 
(
	[id_book] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_category]    Script Date: 12/05/2018 09:18:12 ق.ظ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_category](
	[id_category] [int] IDENTITY(1,1) NOT NULL,
	[category] [nvarchar](50) NULL,
	[category_total] [int] NULL,
 CONSTRAINT [PK_tbl_category] PRIMARY KEY CLUSTERED 
(
	[id_category] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_news]    Script Date: 12/05/2018 09:18:12 ق.ظ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_news](
	[id_news] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](30) NULL,
	[title_news] [nvarchar](50) NULL,
	[context_news] [nvarchar](max) NULL,
	[date_news] [nvarchar](50) NULL,
	[time_news] [nvarchar](10) NULL,
	[visited_id_news] [int] NULL,
 CONSTRAINT [PK_tbl_news] PRIMARY KEY CLUSTERED 
(
	[id_news] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_question]    Script Date: 12/05/2018 09:18:12 ق.ظ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_question](
	[id_question] [int] IDENTITY(1,1) NOT NULL,
	[category] [nvarchar](50) NULL,
	[subcategory] [nvarchar](50) NULL,
	[book] [nvarchar](50) NULL,
	[title_question] [nvarchar](80) NULL,
	[description_question] [nvarchar](max) NULL,
	[barom_question] [float] NULL,
	[manfi_question] [float] NULL,
	[time_question] [nvarchar](10) NULL,
	[answertype_question] [nvarchar](50) NULL,
	[answerA] [nvarchar](50) NULL,
	[answerB] [nvarchar](50) NULL,
	[answerC] [nvarchar](50) NULL,
	[answerD] [nvarchar](50) NULL,
	[answertest] [int] NULL,
 CONSTRAINT [PK_tbl_question] PRIMARY KEY CLUSTERED 
(
	[id_question] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_sendPM]    Script Date: 12/05/2018 09:18:12 ق.ظ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_sendPM](
	[id_pm] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](30) NULL,
	[comment] [nvarchar](max) NULL,
	[date_pm] [nvarchar](10) NULL,
	[time_pm] [nvarchar](10) NULL,
 CONSTRAINT [PK_tbl_sendPM] PRIMARY KEY CLUSTERED 
(
	[id_pm] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_subcategory]    Script Date: 12/05/2018 09:18:12 ق.ظ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_subcategory](
	[id_subcategory] [int] IDENTITY(1,1) NOT NULL,
	[category] [nvarchar](50) NULL,
	[subcategory] [nvarchar](50) NULL,
	[subcategory_total] [int] NULL,
 CONSTRAINT [PK_tbl_subcategory] PRIMARY KEY CLUSTERED 
(
	[id_subcategory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_users]    Script Date: 12/05/2018 09:18:12 ق.ظ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_users](
	[id_username] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](30) NOT NULL,
	[password] [nvarchar](30) NOT NULL,
	[user_email] [nvarchar](30) NULL,
	[user_age] [nvarchar](2) NULL,
	[tahsilat] [nvarchar](15) NULL,
	[gerayesh] [nvarchar](50) NULL,
	[time_reg] [nvarchar](10) NULL,
	[date_reg] [nvarchar](10) NULL,
	[rols] [int] NULL,
 CONSTRAINT [PK_tbl_users] PRIMARY KEY CLUSTERED 
(
	[id_username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[Vw_question_answer]    Script Date: 12/05/2018 09:18:12 ق.ظ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_question_answer]
AS
SELECT        dbo.tbl_question.*, dbo.tbl_answer.id_answer, dbo.tbl_answer.username, dbo.tbl_answer.espolid_answer, dbo.tbl_answer.emteyaz_question
FROM            dbo.tbl_question INNER JOIN
                         dbo.tbl_answer ON dbo.tbl_question.id_question = dbo.tbl_answer.id_question

GO
SET IDENTITY_INSERT [dbo].[tbl_answer] ON 

INSERT [dbo].[tbl_answer] ([id_answer], [id_question], [username], [espolid_answer], [emteyaz_question]) VALUES (22, 7, N'12345', N'4', N'1')
INSERT [dbo].[tbl_answer] ([id_answer], [id_question], [username], [espolid_answer], [emteyaz_question]) VALUES (23, 7, N'12345', N'3', N'0')
INSERT [dbo].[tbl_answer] ([id_answer], [id_question], [username], [espolid_answer], [emteyaz_question]) VALUES (24, 7, N'12345', N'سفید', N'0')
INSERT [dbo].[tbl_answer] ([id_answer], [id_question], [username], [espolid_answer], [emteyaz_question]) VALUES (25, 8, N'علی', N'2', N'3')
INSERT [dbo].[tbl_answer] ([id_answer], [id_question], [username], [espolid_answer], [emteyaz_question]) VALUES (26, 8, N'علی', N'2', N'3')
INSERT [dbo].[tbl_answer] ([id_answer], [id_question], [username], [espolid_answer], [emteyaz_question]) VALUES (27, 8, N'علی', N'2', N'3')
INSERT [dbo].[tbl_answer] ([id_answer], [id_question], [username], [espolid_answer], [emteyaz_question]) VALUES (28, 8, N'علی', N'سفید', N'0')
SET IDENTITY_INSERT [dbo].[tbl_answer] OFF
SET IDENTITY_INSERT [dbo].[tbl_book] ON 

INSERT [dbo].[tbl_book] ([id_book], [category], [subcategory], [book], [book_total]) VALUES (1, N'ابتدایی', N'اول دبستان', N'ریاضی', 1)
INSERT [dbo].[tbl_book] ([id_book], [category], [subcategory], [book], [book_total]) VALUES (5, N'کاردانی', N'مهندسی نرم افزار', N'برنامه نویس پیشرفته 1', 2)
INSERT [dbo].[tbl_book] ([id_book], [category], [subcategory], [book], [book_total]) VALUES (8, N'راهنمایی', N'اول راهنمایی', N'ادبیات فارسی', 0)
INSERT [dbo].[tbl_book] ([id_book], [category], [subcategory], [book], [book_total]) VALUES (9, N'کاردانی', N'مهندسی نرم افزار', N'مهندسی نرم افزار', 0)
INSERT [dbo].[tbl_book] ([id_book], [category], [subcategory], [book], [book_total]) VALUES (12, N'کارشناسی', N'مهندسی نرم افزار', N'آزمایشگاه مهندسی نرم افزار', 0)
INSERT [dbo].[tbl_book] ([id_book], [category], [subcategory], [book], [book_total]) VALUES (13, N'کاردانی', N'مهندسی نرم افزار', N'سخت افزار', 0)
INSERT [dbo].[tbl_book] ([id_book], [category], [subcategory], [book], [book_total]) VALUES (14, N'کاردانی', N'مهندسی نرم افزار', N'ساختمان داده', 0)
INSERT [dbo].[tbl_book] ([id_book], [category], [subcategory], [book], [book_total]) VALUES (15, N'کارشناسی', N'مهندسی نرم افزار', N'هوش مصنوعی', 0)
INSERT [dbo].[tbl_book] ([id_book], [category], [subcategory], [book], [book_total]) VALUES (16, N'کارشناسی', N'مهندسی نرم افزار', N'سیستم های خبره', 0)
INSERT [dbo].[tbl_book] ([id_book], [category], [subcategory], [book], [book_total]) VALUES (18, N'دبیرستان', N'ریاضی و فیزیک', N'جبر و احتمال', 0)
INSERT [dbo].[tbl_book] ([id_book], [category], [subcategory], [book], [book_total]) VALUES (19, N'دبیرستان', N'ریاضی و فیزیک', N'فیزیک', 0)
INSERT [dbo].[tbl_book] ([id_book], [category], [subcategory], [book], [book_total]) VALUES (21, N'کارشناسی', N'مهندسی نرم افزار', N'تاریخ اسلام', 0)
INSERT [dbo].[tbl_book] ([id_book], [category], [subcategory], [book], [book_total]) VALUES (22, N'کارشناسی', N'حسابداری', N'تاریخ اسلام', 0)
INSERT [dbo].[tbl_book] ([id_book], [category], [subcategory], [book], [book_total]) VALUES (23, N'کارشناسی', N'مهندسی عمران', N'تاریخ اسلام', 0)
INSERT [dbo].[tbl_book] ([id_book], [category], [subcategory], [book], [book_total]) VALUES (24, N'کارشناسی', N'مهندسی عمران', N'اندیشه اسلامی 1', 0)
INSERT [dbo].[tbl_book] ([id_book], [category], [subcategory], [book], [book_total]) VALUES (25, N'کارشناسی', N'مهندسی عمران', N'اندیشه اسلامی 2', 0)
INSERT [dbo].[tbl_book] ([id_book], [category], [subcategory], [book], [book_total]) VALUES (26, N'کارشناسی', N'حسابداری', N'اندیشه اسلامی 1', 0)
INSERT [dbo].[tbl_book] ([id_book], [category], [subcategory], [book], [book_total]) VALUES (27, N'کارشناسی', N'حسابداری', N'اندیشه اسلامی 2', 0)
INSERT [dbo].[tbl_book] ([id_book], [category], [subcategory], [book], [book_total]) VALUES (28, N'کارشناسی', N'مهندسی نرم افزار', N'اندیشه اسلامی 1', 0)
INSERT [dbo].[tbl_book] ([id_book], [category], [subcategory], [book], [book_total]) VALUES (29, N'کارشناسی', N'مهندسی نرم افزار', N'اندیشه اسلامی 2', 0)
INSERT [dbo].[tbl_book] ([id_book], [category], [subcategory], [book], [book_total]) VALUES (32, N'دبیرستان', N'علوم تجربی', N'زیست شناسی', 0)
INSERT [dbo].[tbl_book] ([id_book], [category], [subcategory], [book], [book_total]) VALUES (33, N'دکتری', N'هوش مصنوعی', N'هوش مصنوعی', 1)
INSERT [dbo].[tbl_book] ([id_book], [category], [subcategory], [book], [book_total]) VALUES (34, N'دکتری', N'هوش مصنوعی', N'شبکه های عصبی', 1)
SET IDENTITY_INSERT [dbo].[tbl_book] OFF
SET IDENTITY_INSERT [dbo].[tbl_category] ON 

INSERT [dbo].[tbl_category] ([id_category], [category], [category_total]) VALUES (17, N'ابتدایی', 6)
INSERT [dbo].[tbl_category] ([id_category], [category], [category_total]) VALUES (18, N'راهنمایی', 3)
INSERT [dbo].[tbl_category] ([id_category], [category], [category_total]) VALUES (19, N'دبیرستان', 3)
INSERT [dbo].[tbl_category] ([id_category], [category], [category_total]) VALUES (20, N'کاردانی', 4)
INSERT [dbo].[tbl_category] ([id_category], [category], [category_total]) VALUES (21, N'کارشناسی', 3)
INSERT [dbo].[tbl_category] ([id_category], [category], [category_total]) VALUES (22, N'کارشناسی ارشد', 1)
INSERT [dbo].[tbl_category] ([id_category], [category], [category_total]) VALUES (23, N'دکتری', 2)
SET IDENTITY_INSERT [dbo].[tbl_category] OFF
SET IDENTITY_INSERT [dbo].[tbl_news] ON 

INSERT [dbo].[tbl_news] ([id_news], [username], [title_news], [context_news], [date_news], [time_news], [visited_id_news]) VALUES (1, N'admin', N'محصول انقلابی اپل در راه است', N'<p style="text-align: right;">یک طراح مشهور محصولات رایانه ای در فرانسه مدعی شده که اپل به زودی محصول تازه ای را روانه بازارهای جهانی می کند که انقلابی در حوزه آی تی و رایانه ایجاد خواهد کرد. <br /> فیلیپ استراک می گوید وی در طراحی این محصول با اپل همکاری دارد و امیدوار است تا هشت ماه دیگر شاهد عرضه آن باشد.<br /> استراک هیچ جزییات دیگری در مورد این محصول و مشخصات آن ارائه نکرده و در برابر فشارها و درخواست هایی که از او به عمل آمده فعلا مقاومت کرده است.<br /> به گزارش فارس به نقل از اپل اینسایدر، وی این پروژه را یک طرح بزرگ توصیف کرده که فعلا مقامات اپل تمایلی به افشای اطلاعات بیشتری در مورد آن ندارند.<br /> به گفته این طراح وی به طور منظم با استیو جابز مدیر عامل فقید اپل که چند ماه قبل به دنبال بیماری سرطان درگذشت، دیدار داشته است.<br /> وی که از 7 سال پیش دوست استیو جابز بوده آخرین بار وی را در خانه اش در پالوآلتو ملاقات کرده بود.</p>', N'جمعه 5 خرداد', N'12:17 PM', 7)
INSERT [dbo].[tbl_news] ([id_news], [username], [title_news], [context_news], [date_news], [time_news], [visited_id_news]) VALUES (3, N'admin', N'آخرین آرزوی استیو جابز ', N'<p style="text-align: right;">یکی از اعضای هیئت مدیره شرکت اپل می گوید استیو جابز قبل از مرگ همواره رویای ساخت یک iCar را در سر می پروراند. دیکی درکسلر، طی سخنرانی در یک کنفرانس با اشاره به اینکه صنعت اتوموبیل سازی در آمریکا به یک تراژدی شبیه است گفت استیو جابز دوست داشت با ساختن یک iCar تحولی عظیم در صنعت اتومبیل سازی به وجود آورد. لیویو تودوران، طراح ایتالیایی، با الهام از محصولات اپل، مدل اتومبیلی به نام iMove را برای سال 2020طراحی کرده است.</p>', N'جمعه 5 خرداد', N'05:09 PM', 15)
INSERT [dbo].[tbl_news] ([id_news], [username], [title_news], [context_news], [date_news], [time_news], [visited_id_news]) VALUES (4, N'admin', N'سونی و عرضه اولین هندی کم ضد آب ', N'<p style="text-align: right;">شرکت سونی در این هفته ای که گذشت اولین هندی کم ضد آب خود را معرفی نمود. این دوربین فیلم برداری که با نام HDR-GW77V شناخته می&zwnj;شود به گونه ای طراحی شده که می&zwnj;تواند به آسانی در زیر آب و تا عمق ۵ متری زیر آب و بمدت ۶۰ دقیقه ماندن در آن عمق به فیلم برداری بپردازد. این دوربین دارای رزولوشن ۲۰٫۴ مگا پیکسل بوده و قابلیت ضبط ویدیویی با کیفیت فول اپ دی ( ۱۰۸۰P ) را داراست و نرخ ضبط ویدیو ۶۰ فریم در ثانیه است<br /> <br /> این هندی کم ضد آب قابلیت زوم تا ۱۰x را داراست و حافظه داخلی آن ۱۶ گیگابایت می&zwnj;باشد که با پشتیبانی از حافظه های Micro SDHC قابل ارتقاء می&zwnj;باشد.این دوربین با قیمت هفتاد هزار ین معادل ۸۶۰ دلار در ژاپن روانه بازار می شود.<br /> </p>', N'جمعه 5 خرداد', N'06:13 PM', 10)
INSERT [dbo].[tbl_news] ([id_news], [username], [title_news], [context_news], [date_news], [time_news], [visited_id_news]) VALUES (5, N'admin', N'نرم افزار امنيتي با كنترل از راه دور ', N'<p style="text-align: right;">اين نرم افزار به گونه اي طراحي شده كه مي تواند جلوي دسترسي هكرها به اطلاعات حساس شغلي و كاري كه بر روي اين سيستم ها ذخيره شده را بگيرد و امنيت آنها را تا حد زيادي تضمين نمايد. <br /> نرم افزار ياد شده با نام Eset Mobile Security Business Edition فعلا به صورت بتا يا آزمايشي عرضه شده و در حال تست عمومي است. اين نرم افزار با سيستم عامل هاي ويندوز موبايل و سيمبين سازگاري كامل دارد. <br /> يكي از مهم ترين امكانات اين نرم افزار قابليت كنترل از راه دور آن است كه اسكن، بررسي و تنظيم فيلترهاي فايروال و هرزنامه را از راه دور ممكن كرده و بررسي اطلاعات رد و بدل شده ميان سيستم و شبكه هاي رايانه اي را به راحتي ممكن مي كند. همين مساله به افشاي اطلاعاتي كه بدون اجازه از سيستم به خارج منتقل شده كمك مي كند. <br /> به روزرساني اين نرم افزار به طور خودكار انجام شده و براي اين كار نيازي به مداخله دستي نخواهد بود. امكان ايجاد تغييرات در فايل ها از راه دور و همين طور افزودن كلمه عبور و فيلترهاي مختلف از جمله ديگر مزاياي اين نرم افزار است. همچنين در صورت تمايل با استفاده از اين نرم افزار مي توان بخشي يا تمامي اطلاعات را رمزگذاري نمود.</p>', N'جمعه 5 خرداد', N'06:14 PM', 29)
SET IDENTITY_INSERT [dbo].[tbl_news] OFF
SET IDENTITY_INSERT [dbo].[tbl_question] ON 

INSERT [dbo].[tbl_question] ([id_question], [category], [subcategory], [book], [title_question], [description_question], [barom_question], [manfi_question], [time_question], [answertype_question], [answerA], [answerB], [answerC], [answerD], [answertest]) VALUES (7, N'دکتری', N'هوش مصنوعی', N'هوش مصنوعی', N'aaaaaaaaaaaaa', N'', 1, 0, N'00:01:00', N'تستی', N'111111111111', N'222222222222', N'333333333333', N'444444444444', 1)
INSERT [dbo].[tbl_question] ([id_question], [category], [subcategory], [book], [title_question], [description_question], [barom_question], [manfi_question], [time_question], [answertype_question], [answerA], [answerB], [answerC], [answerD], [answertest]) VALUES (8, N'دکتری', N'هوش مصنوعی', N'شبکه های عصبی', N'شبکه عصبی چیست؟', N'انا', 3, 1, N'00:01:00', N'تستی', N'نورون', N'برنامه کامپیوتری', N'عصب', N'هیچکدام', 2)
SET IDENTITY_INSERT [dbo].[tbl_question] OFF
SET IDENTITY_INSERT [dbo].[tbl_sendPM] ON 

INSERT [dbo].[tbl_sendPM] ([id_pm], [username], [comment], [date_pm], [time_pm]) VALUES (1, N'6801', N'123 test', N'2012-05-23', N'04:17 PM')
INSERT [dbo].[tbl_sendPM] ([id_pm], [username], [comment], [date_pm], [time_pm]) VALUES (4, N'6801', N'<p>666</p>
<p>666</p>', N'2012-05-24', N'05:35 PM')
SET IDENTITY_INSERT [dbo].[tbl_sendPM] OFF
SET IDENTITY_INSERT [dbo].[tbl_subcategory] ON 

INSERT [dbo].[tbl_subcategory] ([id_subcategory], [category], [subcategory], [subcategory_total]) VALUES (26, N'کاردانی', N'مهندسی آی تی', 0)
INSERT [dbo].[tbl_subcategory] ([id_subcategory], [category], [subcategory], [subcategory_total]) VALUES (27, N'دبیرستان', N'علوم تجربی', 1)
INSERT [dbo].[tbl_subcategory] ([id_subcategory], [category], [subcategory], [subcategory_total]) VALUES (28, N'دبیرستان', N'ریاضی و فیزیک', 3)
INSERT [dbo].[tbl_subcategory] ([id_subcategory], [category], [subcategory], [subcategory_total]) VALUES (29, N'دبیرستان', N'ادبیات', 0)
INSERT [dbo].[tbl_subcategory] ([id_subcategory], [category], [subcategory], [subcategory_total]) VALUES (30, N'ابتدایی', N'اول دبستان', 1)
INSERT [dbo].[tbl_subcategory] ([id_subcategory], [category], [subcategory], [subcategory_total]) VALUES (31, N'ابتدایی', N'دوم دبستان', 0)
INSERT [dbo].[tbl_subcategory] ([id_subcategory], [category], [subcategory], [subcategory_total]) VALUES (32, N'ابتدایی', N'سوم دبستان', 0)
INSERT [dbo].[tbl_subcategory] ([id_subcategory], [category], [subcategory], [subcategory_total]) VALUES (33, N'ابتدایی', N'چهارم دبستان', 0)
INSERT [dbo].[tbl_subcategory] ([id_subcategory], [category], [subcategory], [subcategory_total]) VALUES (34, N'ابتدایی', N'پنجم دبستان', 0)
INSERT [dbo].[tbl_subcategory] ([id_subcategory], [category], [subcategory], [subcategory_total]) VALUES (35, N'ابتدایی', N'ششم دبستان', 0)
INSERT [dbo].[tbl_subcategory] ([id_subcategory], [category], [subcategory], [subcategory_total]) VALUES (36, N'راهنمایی', N'اول راهنمایی', 1)
INSERT [dbo].[tbl_subcategory] ([id_subcategory], [category], [subcategory], [subcategory_total]) VALUES (37, N'راهنمایی', N'دوم راهنمایی', 0)
INSERT [dbo].[tbl_subcategory] ([id_subcategory], [category], [subcategory], [subcategory_total]) VALUES (38, N'راهنمایی', N'سوم راهنمایی', 0)
INSERT [dbo].[tbl_subcategory] ([id_subcategory], [category], [subcategory], [subcategory_total]) VALUES (41, N'کاردانی', N'مهندسی نرم افزار', 4)
INSERT [dbo].[tbl_subcategory] ([id_subcategory], [category], [subcategory], [subcategory_total]) VALUES (43, N'کارشناسی ارشد', N'مهندسی نرم افزار', 0)
INSERT [dbo].[tbl_subcategory] ([id_subcategory], [category], [subcategory], [subcategory_total]) VALUES (44, N'کارشناسی', N'مهندسی عمران', 3)
INSERT [dbo].[tbl_subcategory] ([id_subcategory], [category], [subcategory], [subcategory_total]) VALUES (45, N'کاردانی', N'نقشه کشی', 0)
INSERT [dbo].[tbl_subcategory] ([id_subcategory], [category], [subcategory], [subcategory_total]) VALUES (46, N'کارشناسی', N'حسابداری', 3)
INSERT [dbo].[tbl_subcategory] ([id_subcategory], [category], [subcategory], [subcategory_total]) VALUES (47, N'کاردانی', N'حسابداری', 0)
INSERT [dbo].[tbl_subcategory] ([id_subcategory], [category], [subcategory], [subcategory_total]) VALUES (48, N'دکتری', N'هوش مصنوعی', 2)
INSERT [dbo].[tbl_subcategory] ([id_subcategory], [category], [subcategory], [subcategory_total]) VALUES (50, N'کارشناسی', N'مهندسی نرم افزار', 6)
INSERT [dbo].[tbl_subcategory] ([id_subcategory], [category], [subcategory], [subcategory_total]) VALUES (51, N'دکتری', N'هوش مصنویی', 0)
SET IDENTITY_INSERT [dbo].[tbl_subcategory] OFF
SET IDENTITY_INSERT [dbo].[tbl_users] ON 

INSERT [dbo].[tbl_users] ([id_username], [username], [password], [user_email], [user_age], [tahsilat], [gerayesh], [time_reg], [date_reg], [rols]) VALUES (1, N'admin', N'202CB962AC59075B964B07152D234B', N'admin@site.com', N'23', N'کارشناسی', NULL, N'10:25 PM', N'2012-05-22', 1)
INSERT [dbo].[tbl_users] ([id_username], [username], [password], [user_email], [user_age], [tahsilat], [gerayesh], [time_reg], [date_reg], [rols]) VALUES (2, N'6801', N'202CB962AC59075B964B07152D234B', N'moh.6801@yahoo.com', N'23', N'کارشناسی', N'مهندسی نرم افزار', N'10:44 AM', N'2012-05-23', 2)
INSERT [dbo].[tbl_users] ([id_username], [username], [password], [user_email], [user_age], [tahsilat], [gerayesh], [time_reg], [date_reg], [rols]) VALUES (3, N'Mohammad', N'202CB962AC59075B964B07152D234B', N'adm_takdune@yahoo.com', N'23', N'کارشناسی', NULL, N'11:36 AM', N'2012-05-23', 2)
INSERT [dbo].[tbl_users] ([id_username], [username], [password], [user_email], [user_age], [tahsilat], [gerayesh], [time_reg], [date_reg], [rols]) VALUES (4, N'omid', N'202CB962AC59075B964B07152D234B', N'moh.6801@yahoo.com', N'22', N'ابتدایی', NULL, N'01:49 PM', N'2012-05-31', 2)
INSERT [dbo].[tbl_users] ([id_username], [username], [password], [user_email], [user_age], [tahsilat], [gerayesh], [time_reg], [date_reg], [rols]) VALUES (5, N'123', N'202CB962AC59075B964B07152D234B', N'aliiiiiiii123@yahoo.com', N'23', N'کاردانی', N'مهندسی نرم افزار', N'12:46 AM', N'2012-06-03', 2)
INSERT [dbo].[tbl_users] ([id_username], [username], [password], [user_email], [user_age], [tahsilat], [gerayesh], [time_reg], [date_reg], [rols]) VALUES (6, N'1234', N'81DC9BDB52D04DC20036DBD8313ED0', N'axsbnwhj@yahoo.com', N'23', N'کاردانی', N'مهندسی نرم افزار', N'05:19 PM', N'2012-06-09', 2)
INSERT [dbo].[tbl_users] ([id_username], [username], [password], [user_email], [user_age], [tahsilat], [gerayesh], [time_reg], [date_reg], [rols]) VALUES (7, N'hadi', N'5F0AAD29193AEBF1A188D555F7820F', N'hadi@yahoo.com', N'32', N'دکتری', N'هوش مصنوعی', N'11:05 AM', N'2012-07-01', 2)
INSERT [dbo].[tbl_users] ([id_username], [username], [password], [user_email], [user_age], [tahsilat], [gerayesh], [time_reg], [date_reg], [rols]) VALUES (8, N'12345', N'827CCB0EEA8A706C4C34A16891F84E', N'uhygh@yahoo.com', N'23', N'دکتری', N'هوش مصنوعی', N'03:30 PM', N'2012-07-07', 2)
INSERT [dbo].[tbl_users] ([id_username], [username], [password], [user_email], [user_age], [tahsilat], [gerayesh], [time_reg], [date_reg], [rols]) VALUES (9, N'علی', N'81DC9BDB52D04DC20036DBD8313ED0', N'ali@yahoo.com', N'27', N'دکتری', N'هوش مصنوعی', N'01:29 ق.ظ', N'07/08/2012', 2)
SET IDENTITY_INSERT [dbo].[tbl_users] OFF
ALTER TABLE [dbo].[tbl_answer]  WITH CHECK ADD  CONSTRAINT [FK_tbl_answer_tbl_answer] FOREIGN KEY([id_question])
REFERENCES [dbo].[tbl_question] ([id_question])
GO
ALTER TABLE [dbo].[tbl_answer] CHECK CONSTRAINT [FK_tbl_answer_tbl_answer]
GO
/****** Object:  StoredProcedure [dbo].[CreateBookCategory]    Script Date: 12/05/2018 09:18:12 ق.ظ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreateBookCategory]

@category	nvarchar(50)	,
@subcategory	nvarchar(50)	,
@book	nvarchar(50)	,
@book_total	int	

AS
	insert into tbl_book ([category],[subcategory],[book],[book_total]) values (@category,@subcategory,@book,@book_total)

GO
/****** Object:  StoredProcedure [dbo].[CreateCategory_d]    Script Date: 12/05/2018 09:18:12 ق.ظ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreateCategory_d]

	@category	nvarchar(50)	,
	@category_total	int	

AS
	insert into tbl_category ([category],[category_total]) values (@category,@category_total)

GO
/****** Object:  StoredProcedure [dbo].[CreateQuestion]    Script Date: 12/05/2018 09:18:12 ق.ظ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreateQuestion]

	@category	nvarchar(50)	,
	@subcategory	nvarchar(50)	,
	@book	nvarchar(50)	,
	@title_question	nvarchar(80)	,
	@description_question	nvarchar(MAX)	,
	@barom_question	float	,
	@manfi_question	float	,
	@time_question	nvarchar(10)	,
	@answertype_question	nvarchar(50)	,
	@answerA	nvarchar(50)	,
	@answerB	nvarchar(50)	,
	@answerC	nvarchar(50)	,
	@answerD	nvarchar(50)	,
	@answertest	int	

AS
	insert into tbl_question ([category],[subcategory],[book],[title_question],[description_question],[barom_question],[manfi_question],[time_question],[answertype_question],[answerA],[answerB],[answerC],[answerD],[answertest]) 
	values (@category,@subcategory,@book,@title_question,@description_question,@barom_question,@manfi_question,@time_question,@answertype_question,@answerA,@answerB,@answerC,@answerD,@answertest)
GO
/****** Object:  StoredProcedure [dbo].[CreateSubCategory_d]    Script Date: 12/05/2018 09:18:12 ق.ظ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreateSubCategory_d]

	@category	nvarchar(50),
	@subcategory	nvarchar(50),
	@subcategory_total	int	

AS
	insert into tbl_subcategory ([category],[subcategory],[subcategory_total]) 
	values (@category,@subcategory,@subcategory_total)
GO
/****** Object:  StoredProcedure [dbo].[insert_answer_exam]    Script Date: 12/05/2018 09:18:12 ق.ظ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[insert_answer_exam]

	@id_question	int	,
	@username	nvarchar(50)	,
	@espolid_answer	nvarchar(10)	,
	@emteyaz_question	nvarchar(10)	

AS
	insert into tbl_answer ([id_question],[username],[espolid_answer],[emteyaz_question]) 
	values (@id_question,@username,@espolid_answer,@emteyaz_question)
GO
/****** Object:  StoredProcedure [dbo].[insert_news]    Script Date: 12/05/2018 09:18:12 ق.ظ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[insert_news]

	@username	nvarchar(30)	,
	@title_news	nvarchar(50)	,
	@context_news	nvarchar(MAX)	,
	@date_news	nvarchar(50)	,
	@time_news	nvarchar(10)	,
	@visited_id_news	int	

AS
	insert into tbl_news ([username],[title_news],[context_news],[date_news],[time_news],[visited_id_news])
	values (@username,@title_news,@context_news,@date_news,@time_news,@visited_id_news)
GO
/****** Object:  StoredProcedure [dbo].[login]    Script Date: 12/05/2018 09:18:12 ق.ظ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[login]

	@username	nvarchar(30),
	@password	nvarchar(30)

AS
	select [username] , [password] from tbl_users where [username]=@username AND [password]=@password 

GO
/****** Object:  StoredProcedure [dbo].[loginAdmin]    Script Date: 12/05/2018 09:18:12 ق.ظ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[loginAdmin]

	@username	nvarchar(30),
	@password	nvarchar(30),
	@rols       int

AS
	select [username] , [password] from tbl_users where [username]=@username AND [password]=@password AND [rols]=@rols

GO
/****** Object:  StoredProcedure [dbo].[Registers]    Script Date: 12/05/2018 09:18:12 ق.ظ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Registers]

	@username	nvarchar(30)	,
	@password	nvarchar(30)	,
	@user_email	nvarchar(30)	,
	@user_age	nvarchar(2)	,
	@tahsilat	nvarchar(15)	,
	@gerayesh	nvarchar(50)	,
	@time_reg	nvarchar(10)	,
	@date_reg	nvarchar(10)	,
	@rols       int

AS

	insert into tbl_users ([username],[password],[user_email],[user_age],[tahsilat],[gerayesh],[time_reg],[date_reg],[rols])
 values (@username,@password,@user_email,@user_age,@tahsilat,@gerayesh,@time_reg,@date_reg,@rols)
GO
/****** Object:  StoredProcedure [dbo].[select_tbl_category]    Script Date: 12/05/2018 09:18:12 ق.ظ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[select_tbl_category]

		@category nvarchar(50) 

AS
		select [category],[category_total] from tbl_category where [category] = @category
GO
/****** Object:  StoredProcedure [dbo].[select_tbl_news]    Script Date: 12/05/2018 09:18:12 ق.ظ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[select_tbl_news]

	@id_news int 

AS
		select [username],[title_news],[context_news],[date_news],[time_news],[visited_id_news]
	from tbl_news where [id_news] = @id_news
GO
/****** Object:  StoredProcedure [dbo].[sendPM]    Script Date: 12/05/2018 09:18:12 ق.ظ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sendPM]

	@username	nvarchar(30)	,
	@comment	nvarchar(MAX)	,
	@date_pm	nvarchar(10)	,
	@time_pm	nvarchar(10)	

AS
	insert into tbl_sendPM ([username],[comment],[date_pm],[time_pm]) 
	values (@username,@comment,@date_pm,@time_pm)

GO
/****** Object:  StoredProcedure [dbo].[update_visited_post_news]    Script Date: 12/05/2018 09:18:12 ق.ظ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[update_visited_post_news]

	@id_news int ,
	@visited_id_news	int	

AS
	update tbl_news set [visited_id_news]=@visited_id_news where [id_news] = @id_news
GO
/****** Object:  StoredProcedure [dbo].[Verifi]    Script Date: 12/05/2018 09:18:12 ق.ظ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Verifi]

@username	nvarchar(30)

AS
	select [username] from tbl_users where username=@username

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[21] 2[13] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbl_question"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 203
               Right = 238
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_answer"
            Begin Extent = 
               Top = 6
               Left = 276
               Bottom = 175
               Right = 465
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_question_answer'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_question_answer'
GO
USE [master]
GO
ALTER DATABASE [C:\USERS\MEHDI MAJD\DESKTOP\NEW FOLDER (7)\ONLINEQ\APP_DATA\DB_ONLINEQ.MDF] SET  READ_WRITE 
GO
