create trigger Add_ProductGroupTrigger
on  ProductGroup_tb
after insert
as

declare @date varchar(max)
set @date = ( select convert(varchar, getdate(), 101)+' ' +  convert(varchar, getdate(), 8))

declare @id nvarchar(50)
set @id= (select ProductGroup_ID from inserted)
insert into Log_tb values(N'ProductGroup',1,@id,@date,1)
go



create trigger Delete_ProductGroupTrigger
on ProductGroup_tb
for delete
as

declare @date varchar(max)
set @date = ( select convert(varchar, getdate(), 101)+' ' +  convert(varchar, getdate(), 8))

declare @id nvarchar(50)
set @id= (select ProductGroup_ID  from deleted)
insert into Log_tb values(N'ProductGroup',3,@id,@date,1)
go


create trigger Update_ProductGroupTrigger
on ProductGroup_tb
for update
as

declare @date varchar(max)
set @date = ( select convert(varchar, getdate(), 101)+' ' +  convert(varchar, getdate(), 8))

declare @id nvarchar(50)
set @id= (select ProductGroup_ID from deleted)
insert into Log_tb values(N'ProductGroup',2,@id,@date,1)
go


/****************************************** Size *************************************************/

create trigger Add_SizeTrigger
on  Size_tb
after insert
as

declare @date varchar(max)
set @date = ( select convert(varchar, getdate(), 101)+' ' +  convert(varchar, getdate(), 8))

declare @id nvarchar(50)
set @id= (select SizeID from inserted)
insert into Log_tb values(N'Size',1,@id,@date,1)
go



create trigger Delete_SizeTrigger
on Size_tb
for delete
as

declare @date varchar(max)
set @date = ( select convert(varchar, getdate(), 101)+' ' +  convert(varchar, getdate(), 8))

declare @id nvarchar(50)
set @id= (select SizeID  from deleted)
insert into Log_tb values(N'Size',3,@id,@date,1)
go


create trigger Update_SizeTrigger
on Size_tb
for update
as

declare @date varchar(max)
set @date = ( select convert(varchar, getdate(), 101)+' ' +  convert(varchar, getdate(), 8))

declare @id nvarchar(50)
set @id= (select SizeID from deleted)
insert into Log_tb values(N'Size',2,@id,@date,1)
go



/****************************************** ProductColor *************************************************/

create trigger Add_ProductColorTrigger
on  ProductColor_tb
after insert
as

declare @date varchar(max)
set @date = ( select convert(varchar, getdate(), 101)+' ' +  convert(varchar, getdate(), 8))

declare @id nvarchar(50)
set @id= (select ProductColor_ID from inserted)


insert into Log_tb values(N'ProductColor',1,@id,@date,1)
go



create trigger Delete_ProductColorTrigger
on ProductColor_tb
for delete
as

declare @date varchar(max)
set @date = ( select convert(varchar, getdate(), 101)+' ' +  convert(varchar, getdate(), 8))

declare @id nvarchar(50)
set @id= (select ProductColor_ID from inserted)

insert into Log_tb values(N'ProductColor',3,@id,@date,1)
go


create trigger Update_ProductColorTrigger
on ProductColor_tb
for update
as

declare @date varchar(max)
set @date = ( select convert(varchar, getdate(), 101)+' ' +  convert(varchar, getdate(), 8))

declare @id nvarchar(50)
set @id= (select ProductColor_ID from inserted)

insert into Log_tb values(N'ProductColor',2,@id,@date,1)
go



/****************************************** Color *************************************************/

create trigger Add_ColorTrigger
on  Color_tb
after insert
as

declare @date varchar(max)
set @date = ( select convert(varchar, getdate(), 101)+' ' +  convert(varchar, getdate(), 8))

declare @id nvarchar(50)
set @id= (select Color_ID from inserted)
insert into Log_tb values(N'Color',1,@id,@date,1)
go



create trigger Delete_ColorTrigger
on Color_tb
for delete
as

declare @date varchar(max)
set @date = ( select convert(varchar, getdate(), 101)+' ' +  convert(varchar, getdate(), 8))

declare @id nvarchar(50)
set @id= (select Color_ID  from deleted)
insert into Log_tb values(N'Color',3,@id,@date,1)
go


create trigger Update_ColorTrigger
on Color_tb
for update
as

declare @date varchar(max)
set @date = ( select convert(varchar, getdate(), 101)+' ' +  convert(varchar, getdate(), 8))

declare @id nvarchar(50)
set @id= (select Color_ID from deleted)
insert into Log_tb values(N'Color',2,@id,@date,1)
go

/****************************************** ZarProduct *************************************************/

create trigger Add_ZarProductTrigger
on  ZarProduct_tb
after insert
as

declare @date varchar(max)
set @date = ( select convert(varchar, getdate(), 101)+' ' +  convert(varchar, getdate(), 8))

declare @id nvarchar(50)
set @id= (select Product_ID  from inserted)
insert into Log_tb values(N'ZarProduct',1,@id,@date,1)
go



create trigger Delete_ZarProductTrigger
on ZarProduct_tb
for delete
as

declare @date varchar(max)
set @date = ( select convert(varchar, getdate(), 101)+' ' +  convert(varchar, getdate(), 8))

declare @id nvarchar(50)
set @id= (select Product_ID  from deleted)
insert into Log_tb values(N'ZarProduct',3,@id,@date,1)
go


create trigger Update_ZarProductTrigger
on ZarProduct_tb
for update
as

declare @date varchar(max)
set @date = ( select convert(varchar, getdate(), 101)+' ' +  convert(varchar, getdate(), 8))

declare @id nvarchar(50)
set @id= (select Product_ID from deleted)
insert into Log_tb values(N'ZarProduct',2,@id,@date,1)
go


/****************************************** State *************************************************/

create trigger Add_StateTrigger
on  State_tb
after insert
as

declare @date varchar(max)
set @date = ( select convert(varchar, getdate(), 101)+' ' +  convert(varchar, getdate(), 8))

declare @id nvarchar(50)
set @id= (select State_ID  from inserted)
insert into Log_tb values(N'State',1,@id,@date,1)
go



create trigger Delete_StateTrigger
on State_tb
for delete
as

declare @date varchar(max)
set @date = ( select convert(varchar, getdate(), 101)+' ' +  convert(varchar, getdate(), 8))

declare @id nvarchar(50)
set @id= (select State_ID  from deleted)
insert into Log_tb values(N'State',3,@id,@date,1)
go


create trigger Update_StateTrigger
on State_tb
for update
as

declare @date varchar(max)
set @date = ( select convert(varchar, getdate(), 101)+' ' +  convert(varchar, getdate(), 8))

declare @id nvarchar(50)
set @id= (select State_ID from deleted)
insert into Log_tb values(N'State',2,@id,@date,1)
go


/****************************************** City *************************************************/

create trigger Add_CityTrigger
on  City_tb
after insert
as

declare @date varchar(max)
set @date = ( select convert(varchar, getdate(), 101)+' ' +  convert(varchar, getdate(), 8))

declare @id nvarchar(50)
set @id= (select City_ID  from inserted)
insert into Log_tb values(N'City',1,@id,@date,1)
go



create trigger Delete_CityTrigger
on City_tb
for delete
as

declare @date varchar(max)
set @date = ( select convert(varchar, getdate(), 101)+' ' +  convert(varchar, getdate(), 8))

declare @id nvarchar(50)
set @id= (select City_ID  from deleted)
insert into Log_tb values(N'City',3,@id,@date,1)
go


create trigger Update_CityTrigger
on City_tb
for update
as

declare @date varchar(max)
set @date = ( select convert(varchar, getdate(), 101)+' ' +  convert(varchar, getdate(), 8))

declare @id nvarchar(50)
set @id= (select City_ID from deleted)
insert into Log_tb values(N'City',2,@id,@date,1)
go
