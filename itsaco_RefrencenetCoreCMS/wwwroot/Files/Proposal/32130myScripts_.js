﻿$(document).ready(function () {
    $("#language_ddl").val("fa");

});

$(document).ajaxStart(function () {
    $(this).html("<img src='demo_wait.gif'>");
});

$(document).ajaxStop(function () {
    $(this).html("<img src='demo_wait.gif'>");
});
//--------------------------------------------------- کد مربوط به فعال بودن یک منو و باز ماندن منوی حاوی زیرمنو --------------------------
$('ul.sidebar-menu li:first').addClass('active');

$('ul.sidebar-menu>li a').click(function () {

    $('ul.sidebar-menu>li').removeClass('active');
    $(this).parent('li').addClass('active');
    var oldLink = $(this).attr('href');
    $.cookie('zaroldLink', oldLink);
});
var lastLink = $.cookie('zaroldLink');
$('ul.sidebar-menu>li a[href="' + lastLink + '"]').click();


$('ul.sidebar-menu>li.treeview').click(function () {
    if ($(this).hasClass('active')) {
        $('ul.sidebar-menu>li.treeview').removeClass('menu-open');
        $('ul.sidebar-menu>li.treeview ul.treeview-menu').slideUp(500);
        $('.arrow', $(this)).removeClass("open");
    }
    else {
        $('ul.sidebar-menu>li.treeview').removeClass('menu-open').removeClass('active');
        $(this).addClass('menu-open').addClass('active');
        $('ul.sidebar-menu>li.treeview ul.treeview-menu').slideUp(500);
        $(this).children('ul.treeview-menu').slideDown(500);
        $('.arrow').removeClass("open");
        $('.arrow', $(this)).addClass("open");
        var lastSubmenuId = $(this).attr('id');
        $.cookie('lastSubmenuId', lastSubmenuId);
    }

});
//alert(lastLink);
$('a[href="' + lastLink + '"]').parent().parent().parent('li.treeview').click();//css({ 'display': 'block' });
//--------------------------------------------------- کد مربوط به فعال بودن یک منو و باز ماندن منوی حاوی زیرمنو --------------------------


//-----------------------------------------------------------------
function openModalForContactWithDeveloper() {
    $.get("/Admin/ContactDev/SendMessage", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ارسال پیام");
        $("#myModalBody").html(result);
    });

}
function ContactWithDeveloper() {
    var frm = $("#ContactWithDeveloper_frm").valid();
    if (frm === false) {
        return false;
    }
    debugger;
    $("#div-loading").removeClass('hidden');
    var person = new FormData();
    var files = $("#attachFile_fu").get(0).files;

    person.append("Name", $("#Name").val()),
        person.append("Mobile", $("#Mobile").val()),
        person.append("Email", $("#Email").val()),
        person.append("MessageText", $("#MessageText").val()),
        person.append("atFile", files[0])


    $.ajax({
        method: 'POST',
        url: '/Admin/ContactDev/SendMessage',
        data: person,
        contentType: false,
        processData: false,
        success: function (data) {
            $("#div-loading").addClass('hidden');
            if (data.statusCode === 200) {
                swal({
                    title: "ارتباط با توسعه دهنده",
                    text: "پیام شما به ایمیل برنامه نویس ارسال شد",
                    type: "success"
                    , confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                })
            }

        },
        error: function (result) {
            alert(result.error);
        }
    });
}
//عدم دسترسی
function ShowAccessDenied() {
    //window.location.href = "/Admin/Account/AccessDenied"; return false; 
    swal({

        type: 'warning',
        title: "مجوز دسترسی",
        text: "متاسفانه شما به این بخش دسترسی ندارید",
        showConfirmButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: "تائید",
    });
}
//Get lang
function GetAllLanguageForMasterPage() {

    $.ajax({
        method: 'POST',
        url: '/Admin/Language/GetAllLangForLayoutPage',
        datatype: "json",
        contentType: "application/json;charset=utf-8",
        data: '',
        success: function (data) {

            var options = "";
            //options += "<option value='0'>زبان ...</option>";
            data.forEach(function (obj) {
                options += "<option value=" + obj.lang_ID + ">" + obj.lang_Name + "</option>";
            });
            $("#language_ddl").html(options);
        },
        error: function (result) {
        }
    });
}

function SetTabsId(id) {
    $('#tab_color').attr('onclick', 'GetAllProductColorByProductId(' + id + ')');
    $('#tab_feature').attr('onclick', 'GetAllProductFeatureByProductId(' + id + ')');
    $('#tab_garanty').attr('onclick', 'GetAllProductGarantyByProductId(' + id + ')');
    $('#tab_advantage').attr('onclick', 'GetAllProductAdvantageByProductId(' + id + ')');
    $('#tab_comment').attr('onclick', 'GetAllCommentsByProductId(' + id + ')');
    $('#tab_similar').attr('onclick', 'GetAllSimilarProductsByProductId(' + id + ')');


}
//دسترسی ها //
function Checkbox() {
}

//
//قسمت نقش

//

function GetAllRoles() {

    $.ajax({
        method: 'POST',
        url: '/Admin/Roles/ViewAll',
        data: '',
        success: function (data) {
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}

function openModalForAddRole() {
    $.get("/Admin/Roles/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن نقش ");
        $("#myModalBody").html(result);
    });
}
function BeginAddRole() {
    debugger;
    $("#div-loading").removeClass('hidden');
}
function CreateRole_Success(data) {
    $("#div-loading").addClass('hidden');
    $("#viewAll").load("/Admin/Roles/ViewAll");
    $("#myModal").modal('hide');
    swal({
        title: "نقش",
        text: "نقش با موفقیت ثبت شد",
        type: "success"
        , confirmButtonColor: '#3085d6',
        confirmButtonText: "تائید"
    })
    GetAllRoles(data);
}
function CreateUserRole_Failure() {

    debugger;
    swal("خطا", "خطا در ارسال اطلاعات دوباره سعی کنید", "error");

}

function openModalForEditRole(id) {
    $.get("/Admin/Roles/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش نقش ");
        $("#myModalBody").html(result);
    });
}
function EditRole_Success(data) {
    if (!data.res) {
        swal({

            type: 'warning',
            title: " نقش",
            text: data.message,
            showConfirmButton: true,
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید",
        });
        return false;

    }
    $("#div-loading").addClass('hidden');
    $("#viewAll").load("/Admin/Roles/ViewAll");
    $("#myModal").modal('hide');

    swal({
        title: "نقش",
        text: "نقش با موفقیت ویرایش شد",
        type: "success",
        confirmButtonColor: '#3085d6',
        confirmButtonText: "تائید"
    });
    GetAllRoles();
}
function EditRole_Failure() {

    debugger;
    swal("خطا", "خطا در ارسال اطلاعات دوباره سعی کنید", "error");

}

function DeleteRole(id) {
    swal({
        title: "هشدار !!!",
        text: "آیا برای حذف این نقش مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"

    })
        .then((willDelete) => {
            if (willDelete.value) {

                $.ajax({
                    url: "/Admin/Roles/Delete",
                    type: "Post",
                    data: { id: id }
                }).done(function (data) {
                    if (data.redirect) { ShowAccessDenied(); return false; }
                    if (!data.res) {
                        swal({

                            type: 'warning',
                            title: " نقش",
                            text: data.message,
                            showConfirmButton: true,
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید",
                        });
                        return false;
                    }
                    else {
                        swal({

                            type: 'success',
                            title: " نقش",
                            text: "نقش با موفیقیت حذف شد",
                            showConfirmButton: true,
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید",
                        });
                        debugger;
                        $('#role_tb').find('tr[id=role_' + data.message + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#role_tb').DataTable();
                            table.row("#role_" + data.message).remove().draw(false);
                        });

                        GetAllRoles();

                    }
                });
            }
        });
}


//
//قسمت کاربران

//
function SetUserName() {

    $("#UserName").val($("#Email").val());
}

function GetAllUsers() {

    $.ajax({
        method: 'POST',
        url: '/Admin/Users/ViewAll',
        data: '',
        success: function (data) {
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}
function openModalForAddUser(id) {
    $.get("/Admin/Users/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن مدیر ");
        $("#myModalBody").html(result);
    });
}
function CreateUser_Success(data) {
    if (!data.res) {
        swal({

            type: 'warning',
            title: " کاربران",
            text: data.message,
            showConfirmButton: true,
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید",
        });
        return false;

    }
    $("#div-loading").addClass('hidden');
    $("#viewAll").load("/Admin/Users/ViewAll");
    $("#myModal").modal('hide');
    swal({
        title: "کاربران", text: "کاربر  با موفقیت ثبت شد", type: "success", showConfirmButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: "تائید",
    });
    GetAllUsers(data);
}
function CreateUser_Failure() {

    debugger;
    swal("خطا", "خطا در ارسال اطلاعات دوباره سعی کنید", "error");

}

function openModalForEditUser(id) {
    debugger;
    $.get("/Admin/Users/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش مدیر ");
        $("#myModalBody").html(result);
    });

}
function EditUser_Success(data) {
    debugger;
    if (!data.res) {
        swal({

            type: 'warning',
            title: " کاربران",
            text: data.message,
            showConfirmButton: true,
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید",
        })
        return false;
    }
    $("#div-loading").addClass('hidden');
    $("#viewAll").load("/Admin/Users/ViewAll");
    $("#myModal").modal('hide');
    swal({
        title: "کاربران", text: "اطلاعات کاربر با موفقیت ویرایش شد", type: "success", showConfirmButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: "تائید",
    });
    GetAllUsers();

}
function EditUser_Failure() {
    swal("خطا", "خطا در ارسال اطلاعات دوباره سعی کنید", "error");
}

function openModalForPermission(id) {
    $.get("/Admin/Roles/Permission/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("تعیین سطح دسترسی");
        $("#myModalBody").html(result);
    });
}
function openModalForAddRoleToUser(id) {
    $.get("/Admin/Roles/AddRole/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن نقش به کاربر");
        $("#myModalBody").html(result);
    });
}
function AddRoleToUser_Success(data) {

    $("#div-loading").addClass('hidden');
    $("#viewAll").load("/Admin/Users/ViewAll");
    $("#myModal").modal('hide');
    swal({
        title: "کاربران", text: "نقش های کاربر با موفقیت ویرایش شد", type: "success", showConfirmButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: "تائید",
    });
    GetAllUsers();

}
function AddRoleToUser_Failure() {
    swal("خطا", "خطا در ارسال اطلاعات دوباره سعی کنید", "error");
}
//

//
//قسمت زبان

//
function GetAllDataInDropDownList(lang, controller, action, selector) {
    $.ajax({
        method: "GET",
        url: "/Admin/" + controller + "/" + action + "",
        data: { id: lang },
        success: function (result) {
            var option = "";
            result.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $(selector).html(option);
            //
        },
        error: function (res) {

        }
    });
}
//
function GetAllLanguage() {

    $.ajax({
        method: 'POST',
        url: '/Admin/Language/ViewAll',
        data: '',
        success: function (data) {
            //$("#viewAll").load('@(Url.Action("ViewAll", "Language", null))');
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}
//
function openModalForAddLang() {
    $.get("/Admin/Language/Create/", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن زبان جدید");
        $("#myModalBody").html(result);
    });

}
//زمانی که شروع به پست کردن اطلاعات میکنیم
var onBegin = function () {

    $("#div-loading").removeClass('hidden');
};

//زمانی که اطلاعات به درستی ثبت شد این تابع فراخوانی می شود
var onSuccessLang = function (res) {
    $("#div-loading").addClass('hidden');

    if (res === "repeate") {
        //swal("هشدار", "شناسه وارد شده تکراری است", "warning");
        swal({

            type: 'warning',
            title: "افزودن زبان",
            text: "شناسه وارد شده قبلا ثبت شده",
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        debugger;
    }
    else {

        $("#viewAll").load("/Admin/Language/ViewAll");
        $("#myModal").modal('hide');
        swal({

            type: 'success',
            title: "افزودن زبان",
            text: "اطلاعات زبان با موفقیت ثبت شد",
            showConfirmButton: true,
            confirmButtonText: "تائید"
        });
        $(".swal2-container").css('z-index:100000');

        //swal("تبریک", "زبان " + res.lang_Name + " با موفقیت ثبت شد", "success");
    }
};
//

function openModalForEditLang(id) {
    $.get("/Admin/Language/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش زبان ");
        $("#myModalBody").html(result);
    });

}

var onSuccessLangEdit = function (res) {
    $("#div-loading").addClass('hidden');


    $("#viewAll").load("/Admin/Language/ViewAll");
    $("#myModal").modal('hide');

    swal({

        type: 'success',
        title: "ویرایش زبان",
        text: "زبان " + res.lang_Name + " با موفقیت ویرایش شد",
        showConfirmButton: true,
        confirmButtonText: "تائید"
    });
};

//////////////////////////قسمت گروه اسلایدشو
//
function ViewAll(lang) {
    var controller = window.location.href;
    controller = controller.substring(controller.indexOf("/admin/"), controller.length);
    $.ajax({
        method: 'POST',
        url: controller + '/ViewAll',
        data: { id: lang },
        success: function (data) {

            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}

function changeLanguage(lang) {


    var controller = window.location.href;
    controller = controller.toLowerCase();
    controller = controller.substring(controller.indexOf("/admin/"), controller.length);
    if (controller.includes("contents")) { //اگر در صفحه مطالب باشیم
        getAllContentsGroupForTreeView();
        GetAllGalleryGroupByLangForContents(lang);
    }
    else if (controller.includes("gallery")) {
        ViewAll(lang);
        GetAllGalleryGroup(lang);
    }
    else if (controller.includes("contactusmessage")) {
        ViewAll(lang);
        GetAllContactusMessageGroupByLang(lang); ///تابعی برای نمایش گروه ها در دراپ داون
    }
    else if (controller.includes("slideshow")) {
        ViewAll(lang);

        GetAllSlideShowGroupByLang(lang); ///تابعی برای نمایش گروه ها در دراپ داون

        GetAllSlideShowByGroupId($('#slideShowGroup_ddl').val());
    }
    else if (controller.includes("products")) {
        //$("#viewAllProducts").html("");
        getAllProductsGroupForTreeView();
    }
    else if (controller.includes("features")) {
        getAllProductsGroupForFeatureTreeView();
    }
    else if (controller.includes("connection")) {
        ViewAll(lang);
        GetAllConnectionGroups(lang, "#connectionsGroup_ddl");
    }
    else if (controller.includes("location")) {
        ViewAll(lang);
        GetAllDataInDropDownList(lang, "Location", "GetAllCountry", "#country_ddl");
        //GetAllDataInDropDownList($("#state_ddl").val(), "Location", "GetAllState", "#state_ddl");
    }
    else if (controller.includes("filesmanage")) {
        //ViewAll(lang);
        //GetAllDownloadGroupByLang(lang); ///تابعی برای نمایش گروه ها دانلود در دراپ داون
        getAllAVFGroupForTreeView();

    }
    else if (controller.includes("projects")) {
        getAllProjectsGroupForTreeView();
    }
    else {
        ViewAll(lang);
    }
}
//
function GetAllSlideShowGroup() {
    $("#div-loading").removeClass('hidden');
    $.ajax({
        method: 'POST',
        url: '/Admin/SlideShow/ViewAll',
        data: '',
        success: function (data) {
            $("#div-loading").addClass('hidden');
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}
//
function openModalForAddSlideShowGroup() {
    $.get("/Admin/SlideShow/Create/", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گروه جدید");
        $("#myModalBody").html(result);
    });

}
//
function InsertSlideShowGroup() {
    //var frm = $("#sgFrm").valid();
    //if (frm === true) {

    $("#div-loading").removeClass('hidden');

    var isactive = $('#IsActiveSsg:checked').length;
    var status = "";
    if (isactive === 1) {
        status = "true";
    }
    else {
        status = "false";
    }

    var slideShowGroup = {
        SSGName: $("#SSGName").val(),
        IsActive: status,
        Lnag: $("#language_ddl").val()
    }



    //    formData.append("IsActive", "true")
    //}
    //else {
    //    formData.append("IsActive", "false")
    //}
    // formData.append("SSGName", $("#SSGName").val()),
    //    formData.append("Language", $("#language_ddl").val())


    $.ajax({
        method: 'Get',
        url: '/Admin/SlideShow/Create',
        data: slideShowGroup,
        success: function (data) {
            $("#div-loading").addClass('hidden');
            $("#viewAll").load("/Admin/SlideShow/ViewAll/" + data + "");
            $("#myModal").modal('hide');
            swal({
                title: "اسلایدشو",
                text: "گروه اسلایدشو با موفقیت ثبت شد",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            })
            GetAllSlideShowGroupByLang(data);
        },
        error: function (result) {
        }
    });
    //}
    //else {
    //    return false;
    //}
}
//
function openModalForEditSlideShowGroup(id) {
    $.get("/Admin/SlideShow/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گروه ");
        $("#myModalBody").html(result);
    });

}
//
function UpdateSlideShowGroup() {
    //var frm = $("#sgFrmEdit").valid();
    //if (frm === true) {

    $("#div-loading").removeClass('hidden');

    var isactive = $('#IsActiveSsgEdit:checked').length;
    var status = "";
    if (isactive === 1) {
        status = "true";
    }
    else {
        status = "false";
    }

    var slideShowGroup = {
        SlideShowGroupID: $("#SlideShowGroupID").val(),
        SSGName: $("#SSGName").val(),
        IsActive: status,
        Lnag: $("#language_ddl").val()
    }
    $.ajax({
        method: 'POST',
        url: '/Admin/SlideShow/Edit',
        data: slideShowGroup,
        success: function (data) {
            $("#div-loading").addClass('hidden');
            $("#viewAll").load("/Admin/SlideShow/ViewAll/" + data + "");
            $("#myModal").modal('hide');
            swal({
                title: "اسلایدشو",
                text: "گروه اسلایدشو با موفقیت ویرایش شد",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            })
            GetAllSlideShowGroupByLang(data);
        },
        error: function (result) {
        }
    });
    //}
    //else {
    //    return false;
    //}
}
//
function DeleteSlideShowGroup(id) {
    swal({
        title: "هشدار !!!",
        text: "آیا برای حذف این گروه مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"

    })
        .then((willDelete) => {
            if (willDelete.value) {

                $.ajax({
                    url: "/Admin/SlideShow/Delete",
                    type: "Get",
                    data: { id: id }
                }).done(function (result) {
                    if (data.redirect) { ShowAccessDenied(); return false; }

                    if (result === "NOK") {
                        swal({
                            title: "اسلایدشو",
                            text: "برای این گروه اسلایدشو تعریف شده , لطفا ابتدا آنها را حذف کنید تا قادر به حذف گروه باشید",
                            type: "warning",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        })
                    }
                    else {
                        $('#slideShow_tb').find('tr[id=ssg_' + id + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#slideShow_tb').DataTable();
                            table.row("#ssg_" + id).remove().draw(false);
                        });
                        GetAllSlideShowGroupByLang(result.lnag);
                        swal({ title: "اسلایدشو", text: "گروه " + result.ssgName + " با موفقیت حذف شد", icon: "success", buttons: [false, "تایید"], });
                    }
                });
            }
        });
}

//////////////////////////قسمت اسلایدشو
//
function GetAllSlideShowGroupByLang(lang) {
    $.ajax({ //گرفتن اطلاعات جیسون از سمت سرور و دی سریالایز کردن آن
        method: "GET",
        async: false,
        url: "/Admin/SlideShow/ViewAllSSG",
        data: { id: lang },
        success: function (result) {
            var option = "";
            result.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $("#slideShowGroup_ddl").html(option);
            $("#SSGId").html(option);
            //
        },
        error: function (res) {

        }
    });
}
//

function GetAllSlideShowByGroupId(id) {
    console.log(id);
    $.ajax({
        method: 'POST',
        async: false,
        url: '/Admin/SlideShow/SlideShowViewAll',
        data: { id: id },
        success: function (data) {
            debugger;
            $("#viewAllSlideShow").html(data);
        },
        error: function (result) {
        }
    });
}


//
function openModalForAddSlideShow() {
    var groupId = $("#slideShowGroup_ddl").val();
    if (groupId == 0) {
        swal({
            title: "اسلایدشو",
            text: "لطفا گروهی برای اسلایدشو انتخاب نمایید",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید"
        })
    }
    else {
        $.get("/Admin/SlideShow/CreateSlideShow/", function (result) {
            $("#myModal").modal();
            $("#myModalLabel").html("افزودن اسلایدشو جدید");
            $("#myModalBody").html(result);
        });
    }
}
//

function AddSlideShow() {
    var frm = $("#sliderFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');

        var slideShow = new FormData();
        var files = $("#imgSlideShowUploader").get(0).files;
        var isactive = $('#isActiveSlideShow:checked').length;
        var groupId = $("#slideShowGroup_ddl").val();

        if (files.length > 0) {


            if (isactive === 1) {
                slideShow.append("IsActive", "true")
            }
            else {
                slideShow.append("IsActive", "false")
            }
            slideShow.append("SSGId", groupId),
                slideShow.append("SSTitle", $("#SSTitle").val()),
                slideShow.append("SSText", $("#SSText").val()),
                slideShow.append("Link", $("#Link").val()),
                slideShow.append("imgSlideShow", files[0])
        }


        $.ajax({
            method: 'POST',
            url: '/Admin/SlideShow/CreateSlideShow',
            data: slideShow,
            contentType: false,
            processData: false,
            success: function (data) {
                $("#div-loading").addClass('hidden');

                if (data === "NoImage") {
                    swal({
                        title: "اسلایدشو",
                        text: "لطفا عکسی را برای اسلایدشو انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    })
                }
                else {
                    $("#viewAllSlideShow").load("/Admin/SlideShow/SlideShowViewAll/" + data);
                    $("#myModal").modal("hide");
                    swal({
                        title: "اسلایدشو",
                        text: "اسلایدشو شما با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            },
            error: function (result) {
                alert(result.error);
            }
        });
    }
    else {
        return false;
    }
}
//
function openModalForEditSlideShow(id) {
    $.get("/Admin/SlideShow/EditSlideShow/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش اسلایدشو");
        $("#myModalBody").html(result);
    });

}
//
function EditSlideShow() {
    $("#div-loading").removeClass('hidden');

    var slideShow = new FormData();
    var files = $("#imgSlideShowUploaderEdit").get(0).files;
    var isactive = $('#isActiveSlideShowEdit:checked').length;

    debugger;

    if (isactive === 1) {
        slideShow.append("IsActive", "true")
    }
    else {
        slideShow.append("IsActive", "false")
    }
    slideShow.append("SlideShowID", $("#SlideShowID").val()),
        slideShow.append("SSGId", $("#SSGId").val()),
        slideShow.append("SSTitle", $("#SSTitle").val()),
        slideShow.append("SSText", $("#SSText").val()),
        slideShow.append("Link", $("#Link").val()),
        slideShow.append("SSPic", $("#SSPic").val())
    debugger;

    if (files.length > 0) {
        slideShow.append("imgSliderUpload", files[0])
    }


    $.ajax({
        method: 'POST',
        url: '/Admin/SlideShow/EditSlideShow',
        data: slideShow,
        contentType: false,
        processData: false,
        success: function (data) {
            $("#div-loading").addClass('hidden');

            $("#viewAllSlideShow").load("/Admin/SlideShow/SlideShowViewAll/" + data);
            $("#myModal").modal("hide");

            swal({
                title: "اسلایدشو",
                text: "اسلایدشو با موفقیت ویرایش شد",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
        },
        error: function (result) {
            alert(result.error);
        }
    });
}

//
function checkboxSelectedForSlider() {
    swal({
        type: "warning",
        title: "هشدار",
        text: "آیا برای حذف اسلایدشوهای انتخاب شده مطمئن هستید؟",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {
                var checkboxValues = "";
                $("div.checked").children('.forRemove').each(function () { // گرفتن ولیو های چکس باکس هایی که تیک خورده باشند
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");
                if (checkboxValues != "") {

                    $.ajax({
                        url: "/Admin/SlideShow/checkboxSelected",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        for (var i = 0; i < cv.length - 1; i++) {

                            $('#slideShow_tb').find('tr[id=ss_' + cv[i] + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#slideShow_tb').DataTable();
                                table.row("#ss_" + cv[i]).remove().draw(false);
                            });
                        }
                        swal({
                            title: "اسلایدشو",
                            text: "اسلایدشو با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                    });
                }
                else {
                    swal({
                        title: "اسلایدشو",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            }
        });

}
//////////////////////////قسمت سوالات متداول

//
//function changeLanguage(lang) {
//    $.ajax({
//        method: 'POST',
//        url: '/Admin/FAQ/ViewAll',
//        data: { lang: lang },
//        success: function (data) {
//            $("#viewAll").html(data);
//        },
//        error: function (result) {
//        }
//    });
//}

//
function GetAllFAQ() {

    $.ajax({
        method: 'POST',
        url: '/Admin/FAQ/ViewAll',
        data: '',
        success: function (data) {
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}
//
//function GetAllFAQByLang(id) {

//    $.ajax({
//        method: 'POST',
//        url: '/Admin/FAQ/ViewAll',
//        data: '',
//        success: function (data) {
//            $("#viewAll").html(data);
//        },
//        error: function (result) {
//        }
//    });
//}
//
//

function openModalForAddFAQ() {
    $.get("/Admin/FAQ/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن سوال جدید");
        $("#myModalBody").html(result);
    });

}
//

function InsertFAQ() {
    var frm = $("#faqFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActiveFaq:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var fAQ = {
            FAQuestion: $("#FAQuestion").val(),
            FAQAnswer: $("#FAQAnswer").val(),
            IsActive: status,
            Lnag: $("#language_ddl").val()
        }



        $.ajax({
            method: 'POST',
            url: '/Admin/FAQ/Create',
            data: fAQ,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                swal({
                    title: "ثبت سوال",
                    text: "سوال شما با موفقیت ثبت شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });

                $("#viewAll").load("/Admin/FAQ/ViewAll/" + data);
                $("#myModal").modal('hide');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//

//
function openModalForEditFAQ(id) {
    $.get("/Admin/FAQ/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش جدید");
        $("#myModalBody").html(result);
    });

}
//

function EditFAQ() {
    var frm = $("#faqFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActiveFaqEdit:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var fAQ = {
            FAQ_ID: $("#FAQ_ID").val(),
            FAQuestion: $("#FAQuestion").val(),
            FAQAnswer: $("#FAQAnswer").val(),
            IsActive: status,
            Lnag: $("#language_ddl").val()
        }



        $.ajax({
            method: 'POST',
            url: '/Admin/FAQ/Edit',
            data: fAQ,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                swal({
                    title: "ویرایش سوال",
                    text: "سوال شما با موفقیت ویرایش شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#viewAll").load("/Admin/FAQ/ViewAll/" + data);
                $("#myModal").modal('hide');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function checkboxSelectedForFAQ() {
    swal({
        title: "هشدار",
        text: "آیا برای حذف سوال مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {
                var cv = [];
                var checkboxValues = "";
                $("div.checked").children('.forRemove').each(function () {
                    checkboxValues += $(this).val() + "&";
                    cv.push($(this).val());
                });


                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/FAQ/checkboxSelected",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {
                        debugger;
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        for (var i = 0; i < cv.length; i++) {

                            $('#faq_tb').find('tr[id=faq_' + cv[i] + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                table.row("#faq_" + cv[i]).remove().draw(false);
                            });
                        }
                        swal({
                            title: "حذف",
                            text: "سوالات انتخاب شده با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                    });
                }
                else {
                    swal({
                        title: "حذف",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            }
        });
}

//////////////////////قسمت گروه پیام های ارتباطی
//
function GetAllContactusMessageGroup() {

    $.ajax({
        method: 'POST',
        url: '/Admin/ContactUsMessage/ViewAll',
        data: '',
        success: function (data) {

            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}
//
function GetAllContactusMessageGroupByLang(lang) {
    $.ajax({
        method: "GET",
        url: "/Admin/ContactUsMessage/ViewAllCMG",
        data: { id: lang },
        success: function (result) {
            var option = "";
            result.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $("#contactusMsgGroup_ddl").html(option);
        },
        error: function (res) {

        }
    });
}
//
function openModalForAddContactusMessageGroup() {
    $.get("/Admin/ContactUsMessage/CreateCMG/", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گروه جدید");
        $("#myModalBody").html(result);
    });

}
//
function InsertContactusMessageGroup() {
    var frm = $("#cmgFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActiveCmg:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var contactusMessageGroup = {
            CMGName: $("#CMGName").val(),
            IsActive: status,
            Lang: $("#language_ddl").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/ContactUsMessage/CreateCMG',
            data: contactusMessageGroup,
            success: function (data) {

                $("#div-loading").addClass('hidden');
                GetAllContactusMessageGroupByLang(data.lang);
                swal({
                    title: "گروه پیام های ارتباطی",
                    text: "گروه " + data.cmgName + " با موفقیت ثبت شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#viewAll").load("/Admin/ContactUsMessage/ViewAll/" + data.lang);
                $("#myModal").modal('hide');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function openModalForEditContactusMessageGroup(id) {
    $.get("/Admin/ContactUsMessage/EditCMG/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گروه");
        $("#myModalBody").html(result);
    });

}
//
function UpdateContactusMessageGroup() {
    var frm = $("#cmgFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActiveCmgEdit:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var contactusMessageGroup = {
            CMG_ID: $("#CMG_ID").val(),
            CMGName: $("#CMGName").val(),
            IsActive: status,
            Lang: $("#language_ddl").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/ContactUsMessage/EditCMG',
            data: contactusMessageGroup,
            success: function (data) {

                $("#div-loading").addClass('hidden');
                GetAllContactusMessageGroupByLang(data.lang);

                swal({
                    title: "گروه پیام های ارتباطی",
                    text: "گروه " + data.cmgName + " با موفقیت ویرایش شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#viewAll").load("/Admin/ContactUsMessage/ViewAll/" + data.lang);
                $("#myModal").modal('hide');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//

function DeleteContactusMessageGroup(id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این گروه مطمئن هستید؟",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $.ajax({
                    url: "/Admin/ContactUsMessage/DeleteCMG",
                    type: "Get",
                    data: { id: id }
                }).done(function (data) {
                    if (data.redirect) { ShowAccessDenied(); return false; }
                    if (data === "NOK") {
                        swal({
                            title: "گروه پیام های ارتباطی",
                            text: "این گروه شامل پیام ارتباطی می باشد بعد از حذف آنها می توانید گروه را حذف نمایید",
                            type: "warning",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                    }

                    else {

                        $('#contactUsGroup_tb').find('tr[id=cmg_' + id + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#contactUsGroup_tb').DataTable();
                            table.row("#cmg_" + id).remove().draw(false);
                        });
                        GetAllContactusMessageGroupByLang(data.lang);
                        swal({
                            title: "گروه پیام های ارتباطی",
                            text: "گروه " + data.cmgName + " با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                    }
                });
            }
        });
}
////////// بخش پیام های ارتباطی
function GetAllContactusMessageByGroupId(id) {

    $.ajax({
        method: 'POST',
        url: '/Admin/ContactUsMessage/ViewAllCM',
        data: { id: id },
        success: function (data) {

            $("#viewAllContactusMessage").html(data);
        },
        error: function (result) {
        }
    });
}
//
function openModalForReplyToMessage(id) {
    $.get("/Admin/ContactUsMessage/EditCM/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("شرح پیام");
        $("#myModalBody").html(result);
    });

}
//
function CheckIsEmptyCMForm() {
    if ($("#MessageAnswer").val() == "") {
        $("#btnSend").prop("disabled", true);
    }
    else {
        $("#btnSend").prop("disabled", false);
    }
}
///
function UpdateContactusMessage() {
    // if ($("#MessageAnswer").val() != "") {
    $("#div-loading").removeClass('hidden');

    var contactusMessage = {
        ContactusMessage_ID: $("#ContactusMessage_ID").val(),
        SenderName: $("#SenderName").text(),
        SenderEmail: $("#SenderEmail").text(),
        SenderMobile: $("#SenderMobile").text(),
        MessageText: $("#MessageText").text(),
        MessageAnswer: $("#MessageAnswer").val(),
        CMGId: $("#CMGId").val(),
        SendDate: $("#SendDate").val(),
        IsNew: false
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/ContactUsMessage/EditCM',
        data: contactusMessage,
        success: function (data) {
            $("#div-loading").addClass('hidden');

            $("#viewAllContactusMessage").load("/Admin/ContactUsMessage/ViewAllCM/" + data.cmgId);
            $("#myModal").modal('hide');
            swal({
                title: "پیام های ارتباطی",
                text: "پاسخ پیام شما برای " + data.senderName + " ارسال گردید",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });

        },
        error: function (result) {
        }
    });
    //}
    //else {
    //    swal("هشدار ", "", "warning");
    //}
}
//
function checkboxSelectedForCM() {

    swal({
        title: "هشدار",
        text: "آیا برای حذف این موارد مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                var checkboxValues = "";
                var Ids = [];
                $("div.checked").children('.forRemove').each(function () {
                    checkboxValues += $(this).val() + "&";
                    Ids.push($(this).val());

                });
                debugger;
                var cv = [];
                cv = checkboxValues.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/ContactUsMessage/checkboxSelected",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }
                        for (var i = 0; i < Ids.length; i++) {
                            debugger;
                            $('#contactUs_tb').find('tr[id=cm_' + Ids[i] + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#contactUs_tb').DataTable();
                                table.row("#cm_" + cv[i]).remove().draw(false);
                            });
                        }
                        swal({
                            title: "حذف",
                            text: "موارد انتخاب شده با موفیقت حذف گردید",
                            type: "warning",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید",
                        })

                    });
                }
                else {

                    swal({
                        title: "هشدار",
                        text: "لطفا یک مورد را انتخاب نمایید !",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    })
                }
            }
        });
}

////////////////////////قسمت گروه مطالب

function GetContentsGroupById(parentId) {

    $.ajax({
        method: 'POST',
        url: '/Admin/Contents/ViewCnGroupData',
        data: { id: parentId },
        success: function (data) {

            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}
//
function openModalForAddContentsGroup() {
    if (ParentId2 != undefined) {
        $.get("/Admin/Contents/CreateContentsGroup", function (result) {

            $("#myModal").modal();
            $("#myModalLabel").html("افزودن گروه جدید");
            $("#myModalBody").html(result);
            //getAllContentsGroupForTreeViewWithoutOpt();
        });
    } else {
        swal({
            title: "گروه مطالب",
            text: "ابتدا یک گروه را به عنوان گروه پدر انتخاب کنید",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید"
        });
    }
}
//
var ParentId, ParentId2, GroupId, GroupIdEdit = "";
function getAllContentsGroupForTreeView() { //خواندن گروه ها برای تری ویو ها
    var lang = $("#language_ddl").val();

    $.ajax({
        method: 'POST',
        url: '/Admin/Contents/createTreeView',
        data: { pgId: 1, lang: lang },
        //data: "{'parentId':'0','lang':'" + lang + "','trviewElement':''}",

        success: function (response) {
            debugger;
            var $checkableTree = $('#contentsGroup1_tw').treeview({
                data: response,
                showBorder: true,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    GetContentsGroupById(node.tags);//نمایش جزئیات گروه ها در لیست جزئیات
                    ParentId2 = node.tags;
                }
            });
            var $checkableTree2 = $('#contentsGroup2_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    ParentId = node.tags;//گرفتن آی دی گروه کلیک شده برای ثبت بعنوان فیلد پدر
                }
            });

            var $checkableTree3 = $('#contentsGroup3_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    GroupId = node.tags;
                    ParentId = node.tags;
                    GetAllContentsByGroupId(node.tags);//گرفتن تمامی مطالب براساس گروه 
                }
            });

            //var $checkableTree4 = $('#contentsGroup4_tw').treeview({
            //    data: response,
            //    showBorder: false,
            //    showIcon: false,
            //    showCheckbox: false,

            //    onNodeSelected: function (event, node) {
            //        GroupId = node.tags;//گرفتن آی دی گروه کلیک شده برای ثبت مطلب
            //    }
            //});

            var $checkableTree5 = $('#contentsGroup5_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    GroupIdEdit = node.tags;//گرفتن آی دی گروه کلیک شده برای ویرایش مطلب
                }
            });

        },
        error: function (response) {

        }
    });
}

//این تابع برای تری ویو هایی است که عملیات چندگانه حذف و ... را ندارند
function getAllContentsGroupForTreeViewWithoutOpt() { //خواندن گروه ها برای تری ویو ها
    var lang = $("#language_ddl").val();

    $.ajax({
        method: 'POST',
        url: '/Admin/Contents/ViewInTreeView',
        data: { parentId: "null", lang: lang, treeview: "", flag: false },
        //data: "{'parentId':'0','lang':'" + lang + "','trviewElement':''}",
        success: function (response) {
            $("#cn2g").html(response);
            $("#cn3g").html(response);
            $("#cn4g").html(response);
        },
        error: function (response) {

        }
    });
}
//
//
function InsertContentsGroup() {
    var frm = $("#contentGroupFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActiveCnGroup:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        if (ParentId2 == undefined) {
            ParentId = 0;
        }

        var contentsGroup = {
            ContentGroup_ID: $("#ContentGroup_ID").val(),
            ContentGroupName: $("#ContentGroupName").val(),
            ParentID: ParentId2,
            IsActive: status,
            Lang: $("#language_ddl").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Contents/CreateContentsGroup',
            data: contentsGroup,
            success: function (data) {

                if (data[0] == "repeate") {
                    swal({
                        title: "گروه مطالب",
                        text: "شناسه گروه وارد شده تکراری است. میتوانید شناسه را از " + data[1] + " به بعد وارد نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });

                    $("#div-loading").addClass('hidden');
                }
                else {
                    $("#div-loading").addClass('hidden');

                    //$("#viewAll").load("/Admin/Contents/ViewAllContentsGroup/" + data.parentID);
                    $("#myModal").modal('hide');
                    $("#cnGroupModal").modal('hide');
                    //اضافه کردن سطر به تری ویو
                    getAllContentsGroupForTreeView();
                    //
                    swal({
                        title: "گروه مطالب",
                        text: "گروه " + data.contentGroupName + " با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function openModalForEditContentsGroup(id) {
    $.get("/Admin/Contents/EditContentsGroup/" + id, function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گروه");
        $("#myModalBody").html(result);

        $("li[data-title=" + id + "]").css({ 'color': 'white' });
    });

}

//

//
function UpdateContentsGroup() {
    var frm = $("#contentGroupFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActiveCnGroupEdit:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }
        var contentsGroup = {
            ContentGroup_ID: $("#ContentGroup_ID").val(),
            ContentGroupName: $("#ContentGroupName").val(),
            ParentID: $("#ParentID").val(),
            IsActive: status,
            Lang: $("#language_ddl").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Contents/EditContentsGroup',
            data: contentsGroup,
            success: function (data) {

                $("#div-loading").addClass('hidden');

                $("#viewAll").load("/Admin/Contents/ViewCnGroupData/" + data.ContentGroup_ID);
                $("#myModal").modal('hide');
                getAllContentsGroupForTreeView();
                swal({
                    title: "گروه مطالب",
                    text: "گروه " + data.contentGroupName + " با موفقیت ویرایش شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//

function DeleteContentsGroup(id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این گروه مطمئن هستید؟",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $.ajax({
                    url: "/Admin/Contents/DeleteContentsGroup",
                    type: "Get",
                    data: { id: id }
                }).done(function (result) {
                    if (data.redirect) { ShowAccessDenied(); return false; }

                    if (result === "NOK") {
                        swal("هشدار", "این گروه شامل مطالبی می باشد بعد از حذف آنها می توانید گروه را حذف نمایید", "warning")
                    }
                    else {
                        $("li[data-title" + id + "]").remove();

                        $('#cnGroup_tb').find('tr[id=cng_' + result.contentGroup_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#cnGroup_tb').DataTable();
                            table.row("#cng_" + result.contentGroup_ID).remove().draw(false);
                        });

                        swal({
                            title: "گروه مطالب",
                            text: "گروه " + result.contentGroupName + " با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                        getAllContentsGroupForTreeView();
                    }
                });
            }
        });
}

///////////////////// بخش مطالب

function GetAllContentsByGroupId(Id) {
    $.ajax({
        method: "GET",
        url: "/Admin/Contents/ViewAllContents",
        data: { id: Id },
        success: function (result) {
            $("#viewAllContents").html(result);
        },
        error: function (res) {

        }
    });
}
//
function openModalForAddContents() {
    if (GroupId != undefined) {
        $.get("/Admin/Contents/CreateContents", function (result) {

            $("#myModal").modal();
            $("#myModalLabel").html("افزودن مطلب جدید");
            $("#myModalBody").html(result);
            $(".modal-dialog").css({ 'width': '75%' });
            //$("li[data-title=" + id + "]").css({ 'color': 'white' });
        });
    }
    else {
        swal("هشدار", "لطفا گروهی را برای مطلب خود انتخاب کنید", "warning");
    }
}
//
function InsertContents() {
    var frm = $("#contentFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');


        var contents = new FormData();
        var isactive = $('#isActiveContents:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var files = $("#ContentUploader").get(0).files;
        if (files.length == 0) {
            $("#validation_file").text("عکس مورد نظر خود را انتخاب نمائید");
            $("#div-loading").addClass('hidden');
            return false;
        }
        if (files.length > 0) {
            contents.append("Content_ID", $("#Content_ID").val()),
                contents.append("ContentTitle", $("#ContentTitle").val()),
                contents.append("ContentImage", files[0]),
                contents.append("ContentText", CKEDITOR.instances['ContentText'].getData()),
                contents.append("ContentSummary", $("#ContentSummary").val()),
                contents.append("IsActive", status),
                contents.append("ContentGroupId", GroupId)
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Contents/CreateContents',
            data: contents,
            contentType: false,//برای ارسال عکس باید این مولفه با فالس مقداردهی شود
            processData: false, //برای استفاده از فرم دیتا باید این مولفه اضافه شود
            success: function (data) {

                if (data[0] == "repeate") {
                    swal({
                        title: "مطلب",
                        text: "شناسه وارد شده تکراری است. میتوانید شناسه را از " + data[1] + " به بعد وارد نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                }
                else {
                    $("#div-loading").addClass('hidden');
                    $("#viewAllContents").load("/Admin/Contents/ViewAllContents/" + data.contentGroupId);

                    $("#myModal").modal('hide');
                    swal({
                        title: "مطلب",
                        text: "مطلب " + data.contentTitle + " با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            },
            error: function (result) {
            }
        });

    }
    else {
        return false;
    }
}
//
function openModalForEditContents(id) {
    $.get("/Admin/Contents/EditContents/" + id, function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش مطلب");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
        $("li[data-title=" + id + "]").css({ 'color': 'white' });

    });

}
//
function UpdateContents() {
    var frm = $("#contentFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');


        var contents = new FormData();
        var isactive = $('#isActiveContentsEdit:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var files = $("#ContentUploader").get(0).files;

        contents.append("Content_ID", $("#Content_ID").val()),
            contents.append("ContentTitle", $("#ContentTitle").val()),
            contents.append("ContentImage", $("#OldContentImage").val()),
            contents.append("ContentText", CKEDITOR.instances['ContentTextEdit'].getData()),
            //contents.append("ContentText", $("#ContentTextEdit").val()),
            contents.append("ContentSummary", $("#ContentSummary").val()),
            contents.append("CreateDate", $("#CreateDate").val()),
            contents.append("IsActive", status)

        if (GroupIdEdit != "") {

            contents.append("ContentGroupId", GroupIdEdit)
        }
        else {
            contents.append("ContentGroupId", $("#GroupIdEdit").val())
        }
        if (files.length > 0) {
            contents.append("NewContentImage", files[0])
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Contents/EditContents',
            data: contents,
            contentType: false,//برای ارسال عکس باید این مولفه با فالس مقداردهی شود
            processData: false, //برای استفاده از فرم دیتا باید این مولفه اضافه شود
            success: function (data) {

                if (data[0] == "repeate") {
                    swal({
                        title: "مطلب",
                        text: "شناسه وارد شده تکراری است.میتوانید شناسه را از " + data[1] + " به بعد وارد نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                }
                else {
                    $("#div-loading").addClass('hidden');
                    $("#viewAllContents").load("/Admin/Contents/ViewAllContents/" + data.contentGroupId);

                    $("#myModal").modal('hide');

                    swal({
                        title: "مطلب",
                        text: "مطلب " + data.contentTitle + " با موفقیت ویرایش شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            },
            error: function (result) {
                //$("#div-loading").addClass('hidden');
            }
        });
    }
    else {
        return false;
    }
}
//
function DeleteContents(id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این مطلب مطمئن هستید؟",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $.ajax({
                    url: "/Admin/Contents/DeleteContents",
                    type: "Get",
                    data: { id: id }
                }).done(function (result) {
                    if (data.redirect) { ShowAccessDenied(); return false; }

                    $('#content_tb').find('tr[id=cn_' + id + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                        var table = $('#content_tb').DataTable();
                        table.row("#cn_" + id).remove().draw(false);
                    });

                    swal({
                        title: "مطلب",
                        text: "مطلب " + result + " با موفقیت حذف شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                });
            }
        });
}
//
function GetAllContentsGalleryByContentId(contentId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Contents/ViewAllContentsGallery",
        data: { id: contentId },
        success: function (result) {

            $("#contents").removeClass("active");
            $("#contentsGallery").addClass("active");

            $("#contentTab").removeClass("active");
            $("#contentGalleryTab").addClass("active");
            $("#viewAllContentsGallery").html(result);
        },
        error: function (res) {

        }
    });
}
//
function openModalForAddContentsGallery() {
    var lang = $("#language_ddl").val();
    $.get("/Admin/Contents/CreateContentsGallery/" + lang, function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گالری مطلب");
        $("#myModalBody").html(result);
        //$("li[data-title=" + id + "]").css({ 'color': 'white' });
    });

}
//
function GetAllGalleryGroupByLangForContents(lang) {//

    $.ajax({ //
        method: "GET",
        url: "/Admin/Gallery/ViewAllGroupGallery",
        data: { id: lang },
        success: function (result) {

            var option = "";//<option value='0'>انتخاب کنید ...</option>";
            result.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $("#galleryGroup_ddl").html(option);
            //
        },
        error: function (res) {

        }
    });
}
//
function GetAllGalleryByGroupIdForContents(GroupId) {//نمایش گالری براساس گروه گالری در دراپ داون ثبت گالری مطلب 

    $.ajax({ //گرفتن اطلاعات جیسون از سمت سرور و دی سریالایز کردن آن
        method: "GET",
        url: "/Admin/Contents/GetAllGalleryByGroupId",
        data: { id: GroupId },
        success: function (result) {

            var option = "";
            if (result.length > 0) {
                result.forEach(function (obj) {
                    option += "<option value=" + obj.value + ">" + obj.text + "</option>";
                });
            }
            else {
                option = "<option value='0'>گالری ندارد</option>";
            }
            $("#GalleryId").html(option);

        },
        error: function (res) {

        }
    });

}
//
function InsertContentsGallery() {
    $("#div-loading").removeClass('hidden');

    var ContentID = $("#contentId").text();

    if (ContentID != "") {
        var contentsGallery = {
            ContentId: ContentID,
            GalleryId: $("#GalleryId").val()
        }


        $.ajax({
            method: 'POST',
            url: '/Admin/Contents/CreateContentsGallery',
            data: contentsGallery,
            success: function (data) {
                if (data == "repeate") {
                    $("#div-loading").addClass('hidden');
                    swal("هشدار", "گالری انتخاب شده در لیست این مطلب ثبت شده است!", "warning");
                }
                else {
                    $("#div-loading").addClass('hidden');
                    $("#viewAllContentsGallery").load("/Admin/Contents/ViewAllContentsGallery/" + data + "");
                    $("#myModal").modal('hide');
                    swal({
                        title: "گالری مطلب",
                        text: "گالری مطلب با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        swal({
            title: "گالری مطلب",
            text: "مطلبی برای گالری انتخاب نشده است",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید"
        });
        $("#div-loading").addClass('hidden');
    }
}
//
function checkboxSelectedForContentsGallery() {

    swal({
        type: 'warning',
        title: "هشدار",
        text: "آیا برای حذف این موارد مطمئن هستید؟",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                var checkboxValues = "";
                $("div.checked").children('.forRemove').each(function () {
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Contents/checkboxSelected",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        for (var i = 0; i < cv.length - 1; i++) {
                            $('#ContentGallery_tb').find('tr[id=cngall_' + cv[i] + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#ContentGallery_tb').DataTable();
                                table.row("#cngall_" + cv[i]).remove().draw(false);
                            });

                        }
                        swal({
                            title: "گالری مطلب",
                            text: "گالری این مطلب با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                    });
                }
                else {
                    swal({
                        title: "گالری مطلب",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            }
        });
}
////////////////// بخش کلمات کلیدی مطالب
function InsertOrUpdateContentsTag(contentId) {
    $("#div-loading").removeClass('hidden');


    $.ajax({
        method: 'POST',
        url: '/Admin/Contents/CreateOrEditContentsTag',
        data: { id: contentId },
        success: function (data) {

            if (data == "add") {
                openModalForAddContentsTags(contentId);
            }
            else {
                openModalForEditContentsTags(contentId);
            }
        },
        error: function (result) {
        }
    });
}
function openModalForAddContentsTags(ContentId) { //برای دادن آی دی مطلب به ویو تگ ها آی دی رو بصورت پارامتر میگیریم و به کنترلر پاس میدهیم و از آنجا به ویو
    // ایجاد تگ توسط ویو بگ ارسال میکنیم
    $.get("/Admin/Contents/CreateContentsTag/" + ContentId, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن کلمات کلیدی");
        $("#myModalBody").html(result);
    });

}
//
function InsertContentsTags() {
    $("#div-loading").removeClass('hidden');
    var contentsTags = {
        ContentId: $("#ContentId").val(),
        TagText: $("#TagText").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/Contents/CreateContentsTag',
        data: contentsTags,
        success: function (data) {
            $("#div-loading").addClass('hidden');
            $("#myModal").modal('hide');
            $("#viewAllContents").load("/Admin/Contents/ViewAllContents/" + GroupId + "");
            swal({
                title: "کلمات کلیدی",
                text: "کلمات کلیدی مطلب ثبت شد",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
        },
        error: function (result) {
        }
    });
}
//
function openModalForEditContentsTags(id) {
    $.get("/Admin/Contents/EditContentsTag/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش کلمات کلیدی");
        $("#myModalBody").html(result);
    });

}
//
function UpdateContentsTags() {
    var contentID = $("#ContentId").val();

    if ($("#TagText").val() == "") { //اگر کاربر تصمیم به پاک کردن کلمات کلیدی کرد اون رکورد در دیتابیس هم حذف شود
        DeleteContentsTag(contentID);
    }
    else {

        $("#div-loading").removeClass('hidden');
        var contentsTags = {
            Tag_ID: $("#Tag_ID").val(),
            ContentId: contentID,
            TagText: $("#TagText").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Contents/EditContentsTag',
            data: contentsTags,
            success: function (data) {

                $("#div-loading").addClass('hidden');
                $("#myModal").modal('hide');
                $("#viewAllContents").load("/Admin/Contents/ViewAllContents/" + GroupId + "");
                swal({
                    title: "کلمات کلیدی",
                    text: "کلمات کلیدی مطلب با موفقیت ویرایش شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
            },
            error: function (result) {
            }
        });
    }
}
//
function DeleteContentsTag(contentId) {

    $("#div-loading").removeClass('hidden');

    $.ajax({
        method: 'POST',
        url: '/Admin/Contents/DeleteContentsTag',
        data: { id: contentId },
        success: function (data) {
            if (data.redirect) { ShowAccessDenied(); return false; }

            $("#myModal").modal('hide');
            $("#div-loading").addClass('hidden');

            swal({
                title: "کلمات کلیدی",
                text: "کلمات کلیدی مطلب با موفقیت حذف شد",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
            //swal("تبریک", "کلمات ", "success");
            //$("#galleryGroup_" + Id).hide();
        },
        error: function (result) {

        }
    });
}
//
////////////////////// بخش نظرات مطالب
function GetAllCommentsByContentId(contentId) {//

    $.ajax({ //
        method: "GET",
        url: "/Admin/Contents/ViewAllComments",
        data: { id: contentId },
        success: function (result) {

            $("#contents").removeClass("active");
            $("#contentsComment").addClass("active");

            $("#contentTab").removeClass("active");
            $("#contentCommentTab").addClass("active");

            $("#viewAllContentComments").html(result);
            //
        },
        error: function (res) {

        }
    });
}
//
function checkboxSelectedForContentsComments() {

    swal({
        title: "هشدار",
        text: "آیا برای حذف این موارد مطمئن هستید؟",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete.value) {

                var checkboxValues = "";
                $("div.checked").children('.forRemove').each(function () {
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Contents/checkboxSelectedComments",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        for (var i = 0; i < cv.length - 1; i++) {
                            $("#comment_" + cv[i]).hide("slow");
                        }
                        swal({
                            title: "نظرات مطالب",
                            text: "نظر این مطلب با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });

                    });
                }
                else {
                    swal({
                        title: "نظرات مطالب",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            }
        });
}
//
function openModalForEditComment(id) {
    $.get("/Admin/Contents/EditComment/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش وضعیت نظرات ");
        $("#myModalBody").html(result);
    });

}
//زمانی که اطلاعات به درستی ثبت شد این تابع فراخوانی می شود
var onSuccessEditComment = function (res) {
    $("#div-loading").addClass('hidden');

    $("#viewAllContentComments").load("/Admin/Contents/ViewAllComments");
    $("#myModal").modal('hide');
    //swal("تبریک", "زبان " + res.lang_Name + " با موفقیت ثبت شد", "success");
};


//
function UpdateContentsComment() {
    $("#div-loading").removeClass('hidden');
    var status = $("#IsConfirmedEdit:checked").length;

    if (status == 1) {
        status = "true";
    }
    else {
        status = "false";
    }
    var contentsComments = {
        Comment_ID: $("#Comment_ID").val(),
        Comment: $("#Comment").val(),
        UserName: $("#UserName").val(),
        Email: $("#Email").val(),
        CommentDate: $("#dateComment").val(),
        PositiveScore: $("#PositiveScore").val(),
        NegativeScore: $("#NegativeScore").val(),
        IsConfirmed: status,
        IsNew: $("#IsNew").val(),
        ContentId: $("#ContentId").val(),
        ContentGroupId: $("#ContentGroupId").val(),

    }


    $.ajax({
        method: 'POST',
        url: '/Admin/Contents/EditComment',
        data: contentsComments,
        success: function (data) {


            $("#div-loading").addClass('hidden');

            $("#viewAllContentComments").load("/Admin/Contents/ViewAllComments/" + data.contentId);
            $("#myModal").modal('hide');
            swal({
                title: "نظرات مطالب",
                text: "وضعیت این نظر با موفقیت ویرایش شد",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });

        },
        error: function (result) {
        }
    });
}
/////////////////////////بخش گروه گالری
function openModalForAddGalleryGroup() {
    $.get("/Admin/Gallery/CreateGalleryGroup", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گروه جدید");
        $("#myModalBody").html(result);
    });

}


//
function IsExist(Id, Selector, ElementMsg) {
    $.ajax({
        method: 'POST',
        url: '/Admin/Gallery/IsExist',
        data: { id: Id },
        success: function (data) {

            if (data[0] === "repeate") {
                $(Selector).focus();
                $(ElementMsg).text("شناسه وارد شده تکرار است شما می توانید شناسه را از " + data[1] + " به بعد وارد نمایید");
            }
        },
        error: function (result) {

        }
    });
}
///
function InsertGalleryGroup() {
    var frm = $("#galleryGroupFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');

        var galleryGroup = {
            GalleryGroup_ID: $("#GalleryGroup_ID").val(),
            GalleryGroupTitle: $("#GalleryGroupTitle").val(),
            Lang: $("#language_ddl").val()
        }


        $.ajax({
            method: 'POST',
            url: '/Admin/Gallery/CreateGalleryGroup',
            data: galleryGroup,
            success: function (data) {
                //if (data[0] === "repeate") {
                //    swal("هشدار", "شناسه وارد شده تکراری است. میتوانید شناسه را از " + data[1] + " به بعد وارد نمایید", "warning");
                //    $("#div-loading").addClass('hidden');
                //}
                //else {
                $("#div-loading").addClass('hidden');
                $("#viewAll").load("/Admin/Gallery/ViewAll/" + data.lang + "");
                $("#myModal").modal('hide');
                GetAllGalleryGroup(data.lang);
                swal({
                    title: "گروه گالری",
                    text: "گروه گالری " + data.galleryGroupTitle + " با موفقیت ثبت گردید",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                //}
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function openModalForEditGalleryGroup(id) {
    $.get("/Admin/Gallery/EditGalleryGroup/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گروه گالری");
        $("#myModalBody").html(result);
    });

}
//
function UpdateGalleryGroup() {
    var frm = $("#galleryGroupFrmEdit").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');

        var galleryGroup = {
            GalleryGroup_ID: $("#GalleryGroup_ID").val(),
            GalleryGroupTitle: $("#GalleryGroupTitle").val(),
            Lang: $("#language_ddl").val()
        }


        $.ajax({
            method: 'POST',
            url: '/Admin/Gallery/EditGalleryGroup',
            data: galleryGroup,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                $("#viewAll").load("/Admin/Gallery/ViewAll/" + data.lang + "");
                $("#myModal").modal('hide');
                GetAllGalleryGroup(data.lang);
                swal({
                    title: "گروه گالری",
                    text: "گروه گالری " + data.galleryGroupTitle + " با موفقیت ویرایش گردید",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });

            },
            error: function (result) {
            }
        });
    } else {
        return false;
    }
}
//
function DeleteGalleryGroup(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این گروه مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Gallery/DeleteGalleryGroup',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }
                        if (data === "NOK") {
                            swal({
                                title: "حذف",
                                text: "این گروه شامل گالری می باشد بعد از حذف آنها می توانید گروه را حذف نمایید",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            })
                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            GetAllGalleryGroup(data.lang);
                            swal({
                                title: "حذف",
                                text: "گروه گالری " + data.galleryGroupTitle + " با موفقیت حذف شد.",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                            $('#galleryg_tb').find('tr[id=galleryGroup_' + Id + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#galleryg_tb').DataTable();
                                table.row("#galleryGroup_" + Id).remove().draw(false);
                            });

                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
//////////////////////////بخش گالری

function GetAllGalleryGroup(lang) { //برای نمایش گروه ها در دراپ داون صفحه گالری
    $.ajax({
        method: "GET",
        url: "/Admin/Gallery/ViewAllGroupGallery",
        data: { id: lang },
        success: function (result) {

            var option = "";
            result.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $("#galleryGroup_ddl").html(option);
            $("#GalleryGroupId").html(option);
        },
        error: function (res) {

        }
    });
}
//
function GetAllGalleryByGroupId(GroupId) {//نمایش گالری براساس گروه گالری
    $.ajax({
        method: "GET",
        url: "/Admin/Gallery/ViewAllGallery",
        data: { id: GroupId },
        success: function (result) {

            $("#viewAllGallery").html(result);
        },
        error: function (res) {

        }
    });
}
//
function openModalForAddGallery() {
    var groupId = $("#galleryGroup_ddl").val();
    if (groupId == 0) {
        swal({
            title: " گالری",
            text: "لطفا گروهی برای گالری انتخاب نمایید",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید"
        });
    }
    else {
        $.get("/Admin/Gallery/CreateGallery", function (result) {
            $("#myModal").modal();
            $("#myModalLabel").html("افزودن گالری جدید");
            $("#myModalBody").html(result);
        });
    }
}
//
function InsertGallery() {
    var frm = $("#galleryFrm").valid();
    if (frm === true) {


        $("#div-loading").removeClass('hidden');
        var gallery = new FormData();
        var files = $("#ImageUpload").get(0).files;
        var isactive = $('#IsActiveGallery:checked').length;
        var groupId = $("#galleryGroup_ddl").val();



        //if (files.length > 0) {
        if (isactive == 1) {
            gallery.append("IsActive", "true")
        }
        else {
            gallery.append("IsActive", "false")
        }
        gallery.append("GalleryGroupId", groupId),
            gallery.append("GalleryTitle", $("#GalleryTitle").val()),
            gallery.append("galleryPic", files[0])
        //}


        $.ajax({
            method: 'POST',
            url: '/Admin/Gallery/CreateGallery',
            data: gallery,
            contentType: false,
            processData: false,
            success: function (data) {
                swal({
                    title: " گالری",
                    text: "گالری " + data.galleryTitle + " با موفقیت ثبت شد  ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#viewAllGallery").load("/Admin/Gallery/ViewAllGallery/" + data.galleryGroupId);
                $("#div-loading").addClass('hidden');
                $("#myModal").modal('hide');

            },
            error: function (result) {
                alert(result.error);
            }
        });
    }
    else {
        return false;
    }
}

//
function DeleteGallery(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این گالری مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Gallery/DeleteGallery',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }
                        if (data === "Invalid") {
                            swal({
                                title: "هشدار",
                                text: "این گالری شامل تصاویری می باشد بعد از حذف آنها می توانید گالری را حذف نمایید",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            })
                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "حذف",
                                text: "گالری با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            })

                            $('#gallery_tb').find('tr[id=gallery_' + Id + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#gallery_tb').DataTable();
                                table.row("#gallery_" + Id).remove().draw(false);
                            });
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}

//
function openModalForEditGallery(id) {
    $.get("/Admin/Gallery/EditGallery/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گالری");
        $("#myModalBody").html(result);
    });
}
//
function UpdateGallery() {
    var frm = $("#galleryFrmEdit").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');
        var gallery = new FormData();
        var files = $("#ImageUpload").get(0).files;
        var isactive = $('#IsActiveEdit:checked').length;



        if (isactive == 1) {
            gallery.append("IsActive", "true")
        }
        else {
            gallery.append("IsActive", "false")
        }
        gallery.append("Gallery_ID", $("#Gallery_ID").val()),
            gallery.append("GalleryGroupId", $("#GalleryGroupId").val()),
            gallery.append("GalleryTitle", $("#GalleryTitle").val()),
            gallery.append("ImageNameGallery", files[0]),
            gallery.append("GalleryImage", $("#oldImage").val())

        $.ajax({
            method: 'POST',
            url: '/Admin/Gallery/EditGallery',
            data: gallery,
            contentType: false,
            processData: false,
            success: function (data) {
                swal({
                    title: " گالری",
                    text: "گالری " + data.galleryTitle + " با موفقیت ویرایش شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#viewAllGallery").load("/Admin/Gallery/ViewAllGallery/" + data.galleryGroupId);
                $("#div-loading").addClass('hidden');
                $("#myModal").modal('hide');
            },
            error: function (result) {
                alert(result.error);
            }
        });
    }
    else {
        return false;
    }
}
/////////////////// بخش تصاویر گالری


function ShowAllViewPictures(id) {

    $.get("/Admin/Gallery/ViewAllGalleryPicture/" + id, function (result) {
        $("#viewAllGalleryPictures").html(result);
        $("#gallery").removeClass("active");
        $("#galleryPic").addClass("active");

        $("#galleryTab").removeClass("active");
        $("#galleryPicTab").addClass("active");
    });
}
//
function openModalForAddGalleryPicture() {
    $.get("/Admin/Gallery/Upload", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن تصاویر گالری جدید");
        $("#myModalBody").html(result);
    });

}
//
function checkboxSelectedGalleryPictures() {

    swal({
        type: "warning",
        title: "هشدار",
        text: "آیا برای حذف این راه ارتباطی مطمئن هستید؟",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                var checkboxValues = "";
                $("div.checked").children('.forRemove').each(function () { // گرفتن ولیو های چکس باکس هایی که تیک خورده باشند
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");
                debugger;
                if (cv == "") {
                    swal({
                        title: "حذف",
                        text: "عکس های مورد نظر را انتخاب کنید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    })
                    return false;
                }
                $.ajax({
                    url: "/Admin/Gallery/checkboxSelected",
                    type: "Get",
                    data: { values: checkboxValues }
                }).done(function (data) {
                    if (data.redirect) { ShowAccessDenied(); return false; }

                    for (var i = 0; i < cv.length - 1; i++) {
                        var id = "gp_" + cv[i].replace(".", "_");
                        $('#galleryPic_tb').find('tr[id=gp_' + cv[i].replace(".", "_") + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#galleryPic_tb').DataTable();
                            table.row("#gp_" + cv[i].replace(".", "_")).remove().draw(false);
                        });
                    }
                    swal({
                        title: "حذف",
                        text: "تصاویر گالری با موفقیت حذف گردید",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                });
            }
        });
}
//
function removePictureFile2(pictureFileName) {
    //تابعی که با زدن ضرب در روی عکس فراخوانی میشود

    $.ajax({
        url: "/Admin/Gallery/RemoveFileFromTemp",
        type: "Get",
        data: { filename: pictureFileName }
    }).done(function (result) {
        var responsePart = eval(result.d);

        removedPicName = pictureFileName;
        for (var i = 0; i < newImageArray.length; i++) {
            if (newImageArray[i] == pictureFileName) {
                newImageArray.splice(i, 1);
                break;
            }
        }
        var arr = '';
        for (var i = 0; i < newImageArray.length; i++) {
            arr += newImageArray[i] + ' * ';
        }
    });

}
//
function Upload() { //ذخیره اصلی اطلاعات

    var frm = $("#galleryPicFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var galleryId = $("#GalleryId").val();
        if (galleryId == undefined) {
            swal({
                type: "warning",
                title: "هشدار",
                text: "لطفا ابتدا گالری تصاویر را انتخاب کنید",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
            })
            $("#div-loading").addClass('hidden');

        }
        else {
            var formData = {
                GalleryId: galleryId,
                GalleryPicTitle: $("#GalleryPicTitle").val(),
            }

            $.ajax({
                data: formData,
                type: 'get',
                url: '/Admin/Gallery/SaveToMainFolder',
                success: function (response) {
                    if (response != null) {
                        //$("#myModal").modal('hide');
                        $("#viewAllGalleryPictures").load("/Admin/Gallery/ViewAllGalleryPicture/" + response);
                        $("#div-loading").addClass('hidden');
                        swal("ثبت", "تصاویر گالری با موفقیت ثبت گردید", "success");
                        newImageArray = [];
                        $("#imgPreviews").html('');
                        $("#GalleryPicTitle").val('');
                    }
                },

            });
        }
    }
    else {
        return false;
    }
}

/////////////////////////Validate Length Of TextBox

function ValueTextBoxCounter(textValue, element, label, maxLength) {

    var len = textValue.value.length;
    len = maxLength - len;
    if (len < 0) {
        element.val(textValue.value.slice(0, maxLength));
        return;
    }
    label.text(len);
}
//////////////////// بخش گروه پروژه ها         
var pcgParentId, pcGroupId, projectGroupId, projectGroupIdEdit = "";
function getAllProjectsGroupForTreeView() { //خواندن گروه ها برای تری ویو ها
    var lang = $("#language_ddl").val();

    $.ajax({
        method: 'POST',
        url: '/Admin/Projects/createTreeView',
        data: { pgId: 1, lang: lang },
        success: function (response) {

            var $checkableTree = $('#projectGroup1_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    pcgParentId = node.tags;
                    GetProjectGroupById(node.tags);
                },
                onNodeUnselected: function (event, node) {
                    pcgParentId = undefined;
                    //GetProductGroupById(node.tags);
                }
            });

            var $checkableTree2 = $('#projectsGroup2_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    pcGroupId = node.tags;
                    GetAllProjectsByGroupById(node.tags, "#viewAllProjects");
                }
            });

            //// جهت نمایش در ویو ثبت جزئیات محصول
            //var $checkableTree3 = $('#productsGroup3_tw').treeview({
            //    data: response,
            //    showBorder: false,
            //    showIcon: false,
            //    showCheckbox: false,

            //    onNodeSelected: function (event, node) {
            //        productGroupId = node.tags;
            //    }
            //});

            //var $checkableTree4 = $('#productsGroup4_tw').treeview({
            //    data: response,
            //    showBorder: false,
            //    showIcon: false,
            //    showCheckbox: false,

            //    onNodeSelected: function (event, node) {
            //        GetAllProductsForSimilarByGroupById(node.tags);
            //    }
            //});
        },
        error: function (response) {

        }
    });
}
/////
function GetProjectGroupById(Id) {
    $.ajax({
        method: "GET",
        url: "/Admin/Projects/ViewProjectGroup",
        data: { id: Id },
        success: function (result) {

            $("#viewAll").html(result);
        },
        error: function (res) {

        }
    });
}
/////
function openModalForAddProjectsGroup() {
    id = $("#language_ddl").val();
    $.get("/Admin/Projects/CreateProjectsGroup", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گروه پروژه جدید");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '60%' });
    });

}
/////
function InsertProjectGroup() {
    var frm = $("#ProjectGroup_Form").valid();
    if (!frm) return false;

    $("#div-loading").removeClass('hidden');
    var projectGroup = new FormData();
    var files = $("#imgUpload").get(0).files;


    var isParent = false;
    if (pcgParentId == undefined) {
        pcgParentId = null;
        isParent = true;
    }


    if (files.length > 0) {

        projectGroup.append("Title", $("#Title").val()),
            projectGroup.append("Description", $("#Description").val()),
            projectGroup.append("ParentId", pcgParentId),
            projectGroup.append("IsParent", isParent),
            projectGroup.append("pgImage", files[0])
        projectGroup.append("Lang", $("#language_ddl").val())
    }


    $.ajax({
        method: 'POST',
        url: '/Admin/Projects/CreateProjectsGroup',
        data: projectGroup,
        contentType: false,
        processData: false,
        success: function (data) {
            getAllProjectsGroupForTreeView();
            swal({
                title: " گروه پروژه",
                text: "گروه پروژه " + data.title + " با موفقیت ثبت شد  ",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
            $("#viewAll").load("/Admin/Projects/ViewProjectGroup/" + data.parentId);
            $("#div-loading").addClass('hidden');
            $("#myModal").modal('hide');

        },
        error: function (result) {
            alert(result.error);
        }
    });

}
/////
function openModalForEditProjectsGroup(id) {
    $.get("/Admin/Projects/EditProjectsGroup/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گروه پروژه ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '60%' });
    });

}
/////
function UpdateProjectGroup() {
    $("#div-loading").removeClass('hidden');
    var projectGroup = new FormData();
    var files = $("#imgUpload").get(0).files;

    var isParent = false;
    if (pcgParentId == undefined) {
        pcgParentId = null;
        isParent = true;
    }



    projectGroup.append("ProjectGroup_ID", $("#ProjectGroup_ID").val()),
        projectGroup.append("Title", $("#Title").val()),
        projectGroup.append("Description", $("#Description").val()),
        projectGroup.append("ParentId", $("#ParentId").val()),
        projectGroup.append("IsParent", $("#IsParent").val()),
        projectGroup.append("GroupImage", $("#oldGroupImage").val()),
        projectGroup.append("pgImage", files[0]),

        projectGroup.append("Lang", $("#language_ddl").val())


    $.ajax({
        method: 'POST',
        url: '/Admin/Projects/EditProjectsGroup',
        data: projectGroup,
        contentType: false,
        processData: false,
        success: function (data) {
            getAllProjectsGroupForTreeView();
            swal({
                title: " گروه پروژه",
                text: "گروه پروژه " + data.title + " با موفقیت ویرایش شد  ",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
            $("#viewAll").load("/Admin/Projects/ViewProjectGroup/" + data.parentId);
            $("#div-loading").addClass('hidden');
            $("#myModal").modal('hide');

        },
        error: function (result) {
            alert(result.error);
        }
    });

}
//////
function DeleteProjectGroup(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این گروه پروژه مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Projects/DeleteProjectGroup',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data === "NOK") {
                            swal({
                                title: " گروه پروژه",
                                text: "گروه پروژهاین گروه شامل گروه های فرزند می باشد ابتدا باید فرزندان حذف شوند",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                            $("#div-loading").addClass('hidden');
                        }
                        else if (data === "isProjects") {
                            swal({
                                title: " گروه پروژه",
                                text: "این گروه شامل پروژه هایی می باشد ابتدا باید محصولات آن حذف شوند",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                            $("#div-loading").addClass('hidden');
                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: " گروه پروژه",
                                text: "گروه " + data.title + "  با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });

                            $("#pg_" + data.projectGroup_ID).hide("slow");
                            getAllProjectsGroupForTreeView();
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
///////

///////////////////// بخش پروژه ها
function SetSlugProjects() {

    var pTitle = $("#Title").val();
    pTitle = pTitle.replace(/\s+/g, "-");
    //pTitle = pTitle + "/";
    $("#Slug").val(pTitle);
}
function GetAllProjectsByGroupById(groupId, elementView) {
    $.ajax({
        method: "GET",
        url: "/Admin/Projects/ViewAllProjects",
        data: { id: groupId },
        success: function (result) {

            $(elementView).html(result);
        },
        error: function (res) {

        }
    });
}
/////
function openModalForAddProjects() {
    //id = $("#language_ddl").val();
    if (pcGroupId != undefined) {
        $.get("/Admin/Projects/CreateProjects", function (result) {
            $("#myModal").modal();
            $("#myModalLabel").html("افزودن پروژه جدید");
            $("#myModalBody").html(result);
            $(".modal-dialog").css({ 'width': '75%' });
        });
    }
    else {
        swal({
            title: "پروژه",
            text: "لطفا برای ثبت پروژه گروهی را انتخاب نمایید",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید"
        });
    }
}
function InsertProjects() {

    debugger;
    var frm = $("#projectsFrm").valid();
    if ($("#Description").val() == "") {
        $("#Desc_val").text("توضیحات را وارد کنید");
        $("#imgUploader_val").text("لطفا تصویر را انتخاب نمائید");
        return false;
    }
    var files = $("#imgUploader").get(0).files;
    if (files.length <= 0) {
        $("#imgUploader_val").text("لطفا تصویر را انتخاب نمائید"); return false;
    }
    if (frm === true) {


        $("#div-loading").removeClass('hidden');
        var projects = new FormData();
        var files = $("#imgUploader").get(0).files;
        //var video = $("#videoUploader").get(0).files;

        var isactive = $('#IsActive:checked').length;
        var isPopular = $('#IsPopular:checked').length;



        // if (files.length > 0) {
        if (isactive == 1) {
            projects.append("IsActive", "true")
        }
        else {
            projects.append("IsActive", "false")
        }

        if (isPopular == 1) {
            projects.append("IsPopular", "true")
        }
        else {
            projects.append("IsPopular", "false")
        }
        debugger;
        projects.append("Title", $("#Title").val()),
            projects.append("Description", CKEDITOR.instances['Description'].getData()),
            projects.append("GroupId", pcGroupId),
            //projects.append("video", video[0]),
            projects.append("imgProject", files[0]),
            projects.append("GalleryId", $("#GalleryId").val()),
            projects.append("Tags", $("#Tags").val()),
            projects.append("Slug", $("#Slug").val()),
            projects.append("MetaTitle", $("#MetaTitle").val()),
            projects.append("MetaKeyword", $("#MetaKeyword").val()),
            projects.append("MetaDescription", $("#MetaDescription").val()),
            projects.append("MetaOther", $("#MetaOther").val())
        // }


        $.ajax({
            method: 'POST',
            url: '/Admin/Projects/CreateProjects',
            data: projects,
            contentType: false,
            processData: false,
            success: function (data) {
                swal({
                    title: "پروژه",
                    text: " محصول " + data.title + " با موفقیت ثبت شد  ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#viewAllProjects").load("/Admin/Projects/ViewAllProjects/" + data.groupId);
                $("#div-loading").addClass('hidden');
                $("#myModal").modal('hide');

            },
            error: function (result) {
                alert(result.error);
            }
        });

    }
    else {
        return false;
    }
}

/////
function openModalForEditProjects(id) {
    //id = $("#language_ddl").val();
    $.get("/Admin/Projects/EditProjects/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش محصول ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
    });

}
//////
function UpdateProjects() {

    var frm = $("#projectsFrmEdit").valid();
    if (frm === true) {

        //if (pcGroupId != undefined) {
        $("#div-loading").removeClass('hidden');
        var projects = new FormData();
        var files = $("#imgUploader").get(0).files;
        //var video = $("#videoUploader").get(0).files;
        var isactive = $('#IsActive:checked').length;
        var isPopular = $('#IsPopular:checked').length;



        if (isactive == 1) {
            projects.append("IsActive", "true")
        }
        else {
            projects.append("IsActive", "false")
        }
        if (isPopular == 1) {
            projects.append("IsPopular", "true")
        }
        else {
            projects.append("IsPopular", "false")
        }

        projects.append("Project_ID", $("#Project_ID").val()),
            projects.append("Title", $("#Title").val()),
            projects.append("Description", CKEDITOR.instances['DescriptionEdit'].getData()),
            projects.append("GroupId", pcGroupId),
            //projects.append("video", video[0]),
            projects.append("imgProject", files[0]),
            projects.append("GalleryId", $("#GalleryId").val()),
            projects.append("Tags", $("#Tags").val()),
            projects.append("Slug", $("#Slug").val()),
            projects.append("Catalog", $("#oldCatalog").val()),
            projects.append("CreateDate", $("#CreateDate").val()),
            projects.append("Image", $("#oldImage").val()),
            projects.append("MetaTitle", $("#MetaTitle").val()),
            projects.append("MetaKeyword", $("#MetaKeyword").val()),
            projects.append("MetaDescription", $("#MetaDescription").val()),
            projects.append("MetaOther", $("#MetaOther").val())


        $.ajax({
            method: 'POST',
            url: '/Admin/Projects/EditProjects',
            data: projects,
            contentType: false,
            processData: false,
            success: function (data) {
                swal({
                    title: "پروژه",
                    text: " پروژه " + data.title + " با موفقیت ویرایش شد  ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#viewAllProjects").load("/Admin/Projects/ViewAllProjects/" + data.groupId);
                $("#div-loading").addClass('hidden');
                $("#myModal").modal('hide');

            },
            error: function (result) {
                alert(result.error);
            }
        });
        //}
        //else {
        //    swal("هشدار ", "لطفا برای ثبت پروژه گروهی را انتخاب نمایید", "warning");
        //}
    }
    else {
        return false;
    }
}
//////
function checkboxSelectedForProjects() {
    swal({
        title: "هشدار",
        text: "آیا برای حذف پروژه ها مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {
                var checkboxValues = "";
                $("div.checked").children('.forRemove').each(function () {
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Projects/checkboxSelectedProjects",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "ok") {
                            for (var i = 0; i < cv.length - 1; i++) {
                                $('#projects_tb').find('tr[id=project_' + cv[i] + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                    var table = $('#projects_tb').DataTable();
                                    table.row("#project_" + cv[i]).remove().draw(false);
                                });
                            }
                            swal({
                                title: "پروژه",
                                text: "پروژه با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                        }
                    });
                }
                else {
                    swal({
                        title: "پروژه",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            }
        });
}

//////
function openModalForShowRateOption(id) {
    $.get("/Admin/Projects/ViewRateOptionDetails/" + id, function (result) {
        $("#rateModal").modal();
        $("#rateModalLabel").html("امتیازات پروژه ");
        $("#rateModalBody").html(result);
    });

}

/////////////////// بخش گروه محصولات

var pgParentId, pGroupId, productGroupId, productGroupIdEdit = "";
function getAllProductsGroupForTreeView() { //خواندن گروه ها برای تری ویو ها
    var lang = $("#language_ddl").val();

    $.ajax({
        method: 'POST',
        url: '/Admin/Products/createTreeView',
        data: { pgId: 1, lang: lang },
        success: function (response) {

            var $checkableTree = $('#productsGroup1_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    debugger;
                    //GetContentsGroupById(node.tags);//نمایش جزئیات گروه ها در لیست جزئیات
                    pgParentId = node.tags;
                    GetProductGroupById(node.tags);
                },
                onNodeUnselected: function (event, node) {

                    pgParentId = undefined;
                    //GetProductGroupById(node.tags);
                }
            });

            var $checkableTree2 = $('#productsGroup2_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    pGroupId = node.tags;/// برای دادن آی دی به مین پروداکت جهت نمایش اندازه های عکس
                    GetAllProductsByGroupById(node.tags);
                }
            });

            // جهت نمایش در ویو ثبت جزئیات محصول
            var $checkableTree3 = $('#productsGroup3_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    productGroupId = node.tags;
                }
            });

            var $checkableTree4 = $('#productsGroup4_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    GetAllProductsForSimilarByGroupById(node.tags);

                }
            });
        },
        error: function (response) {

        }
    });
}
//
//
function getAllProductsGroupForTreeView2(lang) { //برای نمایش گروه در ثبت جزئیات محصول

    $.ajax({
        method: 'POST',
        url: '/Admin/Products/createTreeView',
        data: { pgId: 1, lang: lang },
        success: function (response) {

            // جهت نمایش در ویو ثبت جزئیات محصول
            var $checkableTree3 = $('#productsGroup3_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    productGroupId = node.tags;
                }
            });
            var $checkableTree4 = $('#productsGroup4_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    productGroupIdEdit = node.tags;
                }
            });
            var $checkableTree4 = $('#productsGroup5_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    productGroupIdEdit = node.tags;
                }
            });
        },
        error: function (response) {

        }
    });
}
//
function openModalForAddProductsGroup(id) {
    id = $("#language_ddl").val();
    $.get("/Admin/Products/CreateProductsGroup/" + id, function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گروه محصول جدید");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '60%' });
    });

}
//

function InsertProductGroup() {
    var frm = $("#Form_ProductGroup").valid();
    if (!frm) return false;

    $("#div-loading").removeClass('hidden');
    var productsGroup = new FormData();
    var files = $("#imgUpload").get(0).files;

    var isParent = false;

    if (pgParentId == undefined) {
        pgParentId = null;
        isParent = true;
    }


    if (files.length > 0) {

        productsGroup.append("ProductGroupTitle", $("#ProductGroupTitle").val()),
            productsGroup.append("OffId", $("#OffId").val()),
            productsGroup.append("Description", $("#Description").val()),
            productsGroup.append("PicLargeWidth", $("#PicLargeWidth").val()),
            productsGroup.append("PicSmallWidth", $("#PicSmallWidth").val()),
            productsGroup.append("PicLargeHeight", $("#PicLargeHeight").val()),
            productsGroup.append("PicSmallHeight", $("#PicSmallHeight").val()),
            productsGroup.append("ParentId", pgParentId),
            productsGroup.append("IsParent", isParent),
            productsGroup.append("pgImage", files[0])
        productsGroup.append("Lang", $("#language_ddl").val())
    }


    $.ajax({
        method: 'POST',
        url: '/Admin/Products/CreateProductsGroup',
        data: productsGroup,
        contentType: false,
        processData: false,
        success: function (data) {
            var dataP = data.productsGroup;
            if (!data.res) {
                swal({
                    title: "گروه محصول",
                    text: "ابتدا گروه مورد نظر را انتخاب کنید",
                    type: "warning",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                })
                $("#div-loading").addClass('hidden');

                return false;
            }
            getAllProductsGroupForTreeView();
            swal({
                title: "گروه محصول",
                text: "گروه محصول " + dataP.productGroupTitle + " با موفقیت ثبت شد",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید",
            });
            $("#viewAll").load("/Admin/Products/ViewProductGroup/" + dataP.productGroup_ID);
            $("#div-loading").addClass('hidden');
            $("#myModal").modal('hide');
        },
        error: function (result) {
            alert(result.error);
        }
    });

}
//
function GetProductGroupById(Id) {
    $.ajax({
        method: "GET",
        url: "/Admin/Products/ViewProductGroup",
        data: { id: Id },
        success: function (result) {

            $("#viewAll").html(result);
        },
        error: function (res) {

        }
    });
}
//
function openModalForEditProductsGroup(id) {
    $.get("/Admin/Products/EditProductsGroup/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گروه محصولات ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '60%' });
    });

}
//
//

function UpdateProductGroup() {
    $("#div-loading").removeClass('hidden');
    var productsGroup = new FormData();
    var files = $("#imgUpload").get(0).files;
    //
    //var isParent = false;
    //if (pgParentId == undefined) {
    //    pgParentId = null;
    //    isParent = true;
    //}




    productsGroup.append("ProductGroup_ID", $("#ProductGroup_ID").val()),
        productsGroup.append("ProductGroupTitle", $("#ProductGroupTitle").val()),
        productsGroup.append("OffId", $("#OffId").val()),
        productsGroup.append("Description", $("#Description").val()),
        productsGroup.append("PicLargeWidth", $("#PicLargeWidth").val()),
        productsGroup.append("PicSmallWidth", $("#PicSmallWidth").val()),
        productsGroup.append("PicLargeHeight", $("#PicLargeHeight").val()),
        productsGroup.append("PicSmallHeight", $("#PicSmallHeight").val()),
        productsGroup.append("ParentId", $("#ParentId").val()),
        productsGroup.append("IsParent", $("#IsParent").val()),
        productsGroup.append("ProductGroupImage", $("#ProductGroupImage").val()),

        productsGroup.append("Lang", $("#language_ddl").val())
    if (files.length > 0) {
        productsGroup.append("pgImage", files[0])
    }


    $.ajax({
        method: 'POST',
        url: '/Admin/Products/EditProductsGroup',
        data: productsGroup,
        contentType: false,
        processData: false,
        success: function (data) {
            getAllProductsGroupForTreeView();
            swal({
                title: "گروه محصول",
                text: "گروه محصول " + data.productGroupTitle + " با موفقیت ویرایش شد  ",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید",
            })
            $("#viewAll").load("/Admin/Products/ViewProductGroup/" + data.productGroup_I);
            $("#div-loading").addClass('hidden');
            $("#myModal").modal('hide');

        },
        error: function (result) {
            alert(result.error);
        }
    });

}
//
function DeleteProductGroup(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این گروه محصول مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Products/DeleteProductsGroup',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data === "NOK") {
                            swal({
                                title: "گروه محصول",
                                text: "این گروه شامل گروه های فرزند می باشد ابتدا باید فرزندان حذف شوند",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                            $("#div-loading").addClass('hidden');
                        }
                        else if (data == "isProduct") {
                            swal({
                                title: "گروه محصول",
                                text: "این گروه شامل محصول می باشد ابتدا باید محصولات آن حذف شود",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                            $("#div-loading").addClass('hidden');
                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "گروه محصول",
                                text: "گروه " + data.productGroupTitle + "  با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                            $('#pGroup_tb').find('tr[id=pg_' + data.productGroup_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#pGroup_tb').DataTable();
                                table.row("#pg_" + data.productGroup_ID).remove().draw(false);
                            });

                            getAllProductsGroupForTreeView();
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
//


///////////////////////////// بخش تخفیفات
function openModalForAddOff() {
    $.get("/Admin/Off/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن تخفیف جدید");
        $("#myModalBody").html(result);
    });

}
//
function InsertOff() {
    var frm = $("#offFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');
        var isactive = $('#isActiveOff:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var off = {
            OffTitle: $("#OffTitle").val(),
            StartDate: $("#StartDate").val(),
            EndDate: $("#EndDate").val(),
            Price: $("#Price").val(),
            IsActive: status,
            Lang: $("#language_ddl").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Off/Create',
            data: { off, sdate: $("#tarikhAlt1").val(), eDate: $("#tarikhAlt2").val() },
            success: function (data) {
                if (data.res) {
                    swal({
                        title: "تخفیف",
                        text: "تاریخ پایان باید بعد از تاریخ شروع انتخاب شود",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                    return false;

                }
                $("#viewAll").load("/Admin/Off/ViewAll/" + data.lang);
                $("#myModal").modal("hide");
                swal("تبریک", "تخفیف با عنوان " + data.offTitle + " با موفقیت ثبت شد ", "success");
                swal({
                    title: "تخفیف",
                    text: "تخفیف با عنوان " + data.offTitle + " با موفقیت ثبت شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function openModalForEditOff(id) {
    $.get("/Admin/Off/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش تخفیف ");
        $("#myModalBody").html(result);
    });

}
//
function UpdateOff() {

    var frm = $("#offFrmEdit").valid();
    if (frm === true) {


        $("#div-loading").removeClass('hidden');
        var isactive = $('#isActiveOff:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var off = {
            OffTitle: $("#OffTitle").val(),
            Off_ID: $("#Off_ID").val(),
            StartDate: $("#StartDate").val(),
            EndDate: $("#EndDate").val(),
            Price: $("#Price").val(),
            IsActive: status,
            Lang: $("#language_ddl").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Off/Edit',
            data: { off, sdate: $("#tarikhAlt1").val(), eDate: $("#tarikhAlt2").val() },
            success: function (data) {
                if (data.res) {
                    swal({
                        title: "تخفیف",
                        text: "تاریخ پایان باید بعد از تاریخ شروع انتخاب شود",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                    return false;

                }
                $("#viewAll").load("/Admin/Off/ViewAll/" + data.lang);
                $("#myModal").modal("hide");
                swal({
                    title: "تخفیف",
                    text: "تخفیف با عنوان " + data.offTitle + " با موفقیت ویرایش شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}


//
function DeleteOff(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف تخفیف مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Off/DeleteOff',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        $("#div-loading").addClass('hidden');
                        swal({
                            title: "تخفیف",
                            text: "تخفیف " + data.offTitle + "  با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });

                        $('#off_tb').find('tr[id=off_' + data.off_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#off_tb').DataTable();
                            table.row("#off_" + data.off_ID).remove().draw(false);
                        });

                    },
                    error: function (result) {
                    }
                });
            }
        });
}
//
//////////////////////////// بخش محصولات

function GetAllProductsByGroupById(groupId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Products/ViewAllProducts",
        data: { id: groupId },
        success: function (result) {
            $("#viewAllProducts").html(result);
        },
        error: function (res) {

        }
    });
}
//
function GetAllProductsForSimilarByGroupById(groupId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Products/ViewAllProductsForSimilar",
        data: { id: groupId },
        success: function (result) {

            $("#viewAllProductsForSimilar").html(result);
        },
        error: function (res) {

        }
    });
}
///
function openModalForAddMainProduct() {
    if (pGroupId == undefined) {
        swal({
            title: "محصول",
            text: "برای نمایش اندازه های تصویر محصول ابتدا باید گروه محصول را انتخاب کنید",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید"
        });
    }
    var id = $("#language_ddl").val();
    $.get("/Admin/Products/CreateMainProduct/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن اطلاعات مشترک محصولات ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '85%' });
    });

}

//
function MainProductProductImageSize(id) {
    /// گرفتن ویو کامپوننت بصورت آیجکس

    $.get("/Admin/Products/GetProductImageSizesViewComponent/" + id, function (result) {
        $("#imageSize").html(result);
    });

}
//
function GetAllLanguageForCreateProductView(result) {
    //این تابع جهت سااخت بدنه هر تب در زبان های محصول می باشد که توسط تابع زیرین صدا زده می شود

    $.ajax({
        method: 'POST',
        url: '/Admin/Language/GetAllLangForLayoutPage',
        datatype: "json",
        contentType: "application/json;charset=utf-8",
        data: '',
        success: function (data) {
            var res = "";

            data.forEach(function (obj) {
                res += "<div id='productInfo_" + obj.lang_ID + "' class='tab-pane'>" + result + "</div>"
            });

            $("#contentDiv").append(res);

            res = "<input type='button' value='ثبت' class='btn btn-default' onclick='test()' />";
        },
        error: function (result) {
        }
    });
}


function openModalForAddProductInfo() {

    $.get("/Admin/Products/CreateProduct", function (result) {
        GetAllLanguageForCreateProductView(result);

        //$("#Description").ckeditor();
    });
}

/////
var ProductId = "";
function InsertMainProduct() {
    var frm = $("#mainProductFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');


        var mainProduct = new FormData();
        var isactive = $('#isActiveProduct:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }
        //
        var isExist = $('#isExistProduct:checked').length;
        var exist = "";
        if (isExist === 1) {
            exist = "true";
        }
        else {
            exist = "false";
        }
        //
        //
        var isPopular = $('#IsPopularProduct:checked').length;
        var popular = "";
        if (isPopular === 1) {
            popular = "true";
        }
        else {
            popular = "false";
        }
        //
        var files = $("#ProductImageUploader").get(0).files;
        var catalog = $("#catalog").get(0).files;
        if (files.length > 0) {
            mainProduct.append("ProductCode", $("#ProductCode").val()),
                mainProduct.append("SaledCount", $("#SaledCount").val()),
                mainProduct.append("GalleryId", $("#GalleryId").val()),
                mainProduct.append("imgProduct", files[0]),
                mainProduct.append("pCatalog", catalog[0]),
                mainProduct.append("ExistCount", $("#ExistCount").val()),
                mainProduct.append("IsExsit", exist),
                mainProduct.append("IsActive", status),
                mainProduct.append("ContentGroupId", GroupId),
                mainProduct.append("IsPopular", popular),

                mainProduct.append("smallPicWidth", $("#smallPicWidth").val()),
                mainProduct.append("smallPicHeight", $("#smallPicHeight").val()),
                mainProduct.append("IsDecore", "0")
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Products/CreateMainProduct',
            data: mainProduct,//mainProduct,
            contentType: false,//برای ارسال عکس باید این مولفه با فالس مقداردهی شود
            processData: false, //برای استفاده از فرم دیتا باید این مولفه اضافه شود
            async: false,
            success: function (data) {
                if (data == "noImage") {
                    //swal("هشدار", "اطلاعات وارد شده ناقص می باشد", "warning");
                    $("#ProductImage").addClass("required").text("لطفا برای محصول عکسی انتخاب شود !");
                }
                else if (data == "ok") {
                    $("#div-loading").addClass('hidden');

                    $("#mainProduct").removeClass("active");
                    $("#productInfo").addClass("active");

                    $("#mainProduct_tab").removeClass("active");
                    $("#productInfo_tab").addClass("active");


                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
////
function SendSizeImageForProduct() {
    $.ajax({
        method: "GET",
        url: "/Admin/Products/resizeImage",
        async: false,
        data: { smallPicWidth: $("#smallPicWidth").val(), smallPicHeight: $("#smallPicHeight").val() },
        success: function (result) {

        },
        error: function (res) {

        }
    });
}
///
function SetSlug() {
    //مقدار دادن به اینپوت اسلاگ با گرفتن مقدار نام محصول و جایگزینی فاصله هاش با دَش
    var pTitle = $("#ProductTitle").val();
    pTitle = pTitle.replace(/\s+/g, "-");
    $("#Slug").val(pTitle);
}
///
function GetDropDownDataInProductInfo(lang) {
    /// این تابع برای فراخوانی موارد ذکر شده درصورت تغییر زبان در ویو ثبت محصول
    getAllProductsGroupForTreeView2(lang);
    GetAllDataInDropDownList(lang, "Off", "ViewAllOffList", "#OffId");
    GetAllDataInDropDownList(lang, "Unit", "ViewAllUnitList", "#UnitId");
    GetAllDataInDropDownList(lang, "Producer", "ViewAllProducerList", "#ProducerId");
}
///
function ClearProductForm() {
    $("#ProductTitle").val('');
    $("#Slug").val('');
    $("#Description").val('');
    $("#UnitId").val('0');
    $("#OffId").val('0');
    $("#ProducerId").val('0');
    $("#MetaTitle").val('');
    $("#MetaKeyword").val('');
    $("#MetaDescription").val('');
    $("#MetaOther").val('');
    $("#Tags").val('');
}
///
function InsertProductInfo() {
    var frm = $("#productFrm").valid();
    if (frm === true) {

        if (productGroupId == undefined) {
            swal({
                title: " محصول",
                text: "لطفا یک گروه را انتخاب نمایید",
                type: "warning",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید",
            });
        }
        else {
            $("#div-loading").removeClass('hidden');


            var productInfo = new FormData();

            productInfo.append("ProductGroupId", productGroupId),
                productInfo.append("ProductTitle", $("#ProductTitle").val()),
                productInfo.append("Slug", $("#Slug").val()),
                productInfo.append("Description", CKEDITOR.instances['Description'].getData()),
                productInfo.append("UnitId", $("#UnitId").val()),
                productInfo.append("OffId", $("#OffId").val()),
                productInfo.append("ProducerId", $("#ProducerId").val()),
                productInfo.append("MetaTitle", $("#MetaTitle").val()),
                productInfo.append("MetaKeyword", $("#MetaKeyword").val()),
                productInfo.append("MetaDescription", $("#MetaDescription").val()),
                productInfo.append("MetaOther", $("#MetaOther").val()),
                productInfo.append("Tags", $("#Tags").val())

            $.ajax({
                method: 'POST',
                url: '/Admin/Products/CreateProduct',
                data: productInfo,
                processData: false,
                contentType: false,
                success: function (data) {
                    if (data == "NoId") {
                        swal({
                            title: " محصول",
                            text: "برای ثبت محصول ابتدا باید اطلاعات مشترک محصول ثبت گردد!",
                            type: "warning",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید",
                        });
                    }
                    else {
                        $("#myModal").modal("hide");

                        $("#div-loading").addClass('hidden');
                        $("#viewAllProducts").load("/Admin/Products/ViewAllProducts/" + data.productGroupId);
                        swal("ثبت", "محصول " + data.productTitle + " با موفقیت ثبت شد", "success");
                        swal({
                            title: " ثبت محصول",
                            text: "محصول " + data.productTitle + " با موفقیت ثبت شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید",
                        })

                        ClearProductForm();
                    }

                },
                error: function (result) {
                }
            });
        }
    }
    else {
        return false;
    }
}

///
function openModalForEditMainProduct(infoId) {
    var lang = $("#language_ddl").val();
    $.ajax({
        method: "GET",
        url: "/Admin/Products/EditMainProduct",
        data: { id: infoId, lang: lang },
        success: function (result) {

            $("#myModal").modal();
            $("#myModalLabel").html("ویرایش اطلاعات مشترک محصول ");
            $("#myModalBody").html(result);
            $(".modal-dialog").css({ 'width': '85%' });
        },
        error: function (res) {

        }
    });

    //$.get("/Admin/Products/EditMainProduct/" + id + "/" + lang, function (result) {
    //    $("#myModal").modal();
    //    $("#myModalLabel").html("ویرایش اطلاعات مشترک محصول ");
    //    $("#myModalBody").html(result);
    //    $(".modal-dialog").css({ 'width': '85%' });
    //});

}
///
function UpdateMainProduct() {


    $("#div-loading").removeClass('hidden');


    var mainProduct = new FormData();
    var isactive = $('#isActiveProduct:checked').length;
    var status = "";
    if (isactive === 1) {
        status = "true";
    }
    else {
        status = "false";
    }
    //
    var isExist = $('#isExistProduct:checked').length;
    var exist = "";
    if (isExist === 1) {
        exist = "true";
    }
    else {
        exist = "false";
    }
    //
    //
    var isPopular = $('#IsPopularProductEdit:checked').length;
    var popular = "";
    if (isPopular === 1) {
        popular = "true";
    }
    else {
        popular = "false";
    }
    //
    var files = $("#ProductImageUploader").get(0).files;
    var catalog = $("#catalog").get(0).files;

    mainProduct.append("Product_ID", $("#Product_ID").val()),
        mainProduct.append("ProductCode", $("#ProductCode").val()),
        mainProduct.append("SaledCount", $("#SaledCount").val()),
        mainProduct.append("GalleryId", $("#GalleryId").val()),

        mainProduct.append("ExistCount", $("#ExistCount").val()),
        mainProduct.append("IsExsit", exist),
        mainProduct.append("ProductImage", $("#oldImage").val()),
        mainProduct.append("ProductCatalog", $("#oldCatalog").val()),
        mainProduct.append("CreateDate", $("#CreateDate").val()),
        mainProduct.append("IsActive", status),
        mainProduct.append("ContentGroupId", GroupId),
        mainProduct.append("IsPopular", popular),
        mainProduct.append("pCatalog", catalog[0])

    //mainProduct.append("smallPicWidth", $("#smallPicWidth").val()),
    //mainProduct.append("smallPicHeight", $("#smallPicHeight").val()),
    mainProduct.append("IsDecore", "0")
    if (files.length > 0) {
        mainProduct.append("imgProduct", files[0])
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/Products/EditMainProduct',
        data: mainProduct,
        contentType: false,//برای ارسال عکس باید این مولفه با فالس مقداردهی شود
        processData: false, //برای استفاده از فرم دیتا باید این مولفه اضافه شود
        success: function (data) {
            if (data == "no") {
                swal({
                    title: " هشدار",
                    text: "اطلاعات وارد شده ناقص می باشد",
                    type: "warning",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                });
            }
            else {
                $("#div-loading").addClass('hidden');
                //$("#viewAllContents").load("/Admin/Products/ViewAllProducts/" + data.productGroupId);

                $("#mainProduct").removeClass("active");
                $("#productInfo").addClass("active");

                $("#mainProduct_tab").removeClass("active");
                $("#productInfo_tab").addClass("active");
            }
        },
        error: function (result) {
        }
    });

}
///
function changeHiddenInputValue() {
    $.ajax({
        method: "GET",
        url: "/Admin/Products/EditProduct",
        data: { id: productId, lang: lang },
        success: function (result) {
            if (result == "nall") {
                swal({
                    title: " هشدار",
                    text: "برای ویرایش جزئیات محصول دکمه ویرایش محصول را کلیک کنید",
                    type: "warning",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                });
            }
            else {
                getAllProductsGroupForTreeView2(lang);
                $("#productInfoBody").html(result);
            }
        },
        error: function (res) {

        }
    });
}
///
function openModalForEditProductInfo(productId, lang) {
    //این تابع آی دی مشترک و زبان رو میگیره و دیکشنری محصولاتی که با این آیدی مشترک هستند رو برمیگرددونه
    $.ajax({
        method: "GET",
        url: "/Admin/Products/EditProduct",
        data: { id: productId, lang: lang },
        success: function (result) {

            if (result == "nall") {
                swal({
                    title: " محصول",
                    text: "برای ویرایش جزئیات محصول دکمه ویرایش محصول را کلیک کنید",
                    type: "warning",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                });
            }
            else if (result == "noSet") {
                var htm = "<br/><br/><br/><br/><div class='alert alert-warning'><p>این محصول برای این زبان تعریف نشده است !</p></div>"
                $("#productInfoBody").html(htm);
            }
            else {
                getAllProductsGroupForTreeView2(lang);
                $("#productInfoBody").html(result);
            }
        },
        error: function (res) {

        }
    });


}
///
function UpdateProductInfo() {
    var frm = $("#editProductForm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');


        var productInfo = new FormData();

        if (productGroupIdEdit != "") {
            productInfo.append("ProductGroupId", productGroupIdEdit)
        }
        else {
            productInfo.append("ProductGroupId", $("#ProductGroupId").val())
        }
        productInfo.append("ProductInfo_ID", $("#ProductInfo_IDEdit").val()),
            productInfo.append("ProductTitle", $("#ProductTitle").val()),
            productInfo.append("ProductId", $("#ProductId").val()),
            productInfo.append("Slug", $("#Slug").val()),
            productInfo.append("Description", CKEDITOR.instances['DescriptionEdit'].getData()),
            productInfo.append("UnitId", $("#UnitId").val()),
            productInfo.append("OffId", $("#OffId").val()),
            productInfo.append("ProducerId", $("#ProducerId").val()),
            productInfo.append("MetaTitle", $("#MetaTitle").val()),
            productInfo.append("MetaKeyword", $("#MetaKeyword").val()),
            productInfo.append("MetaDescription", $("#MetaDescription").val()),
            productInfo.append("MetaOther", $("#MetaOther").val()),
            productInfo.append("Tags", $("#Tags").val())

        $.ajax({
            method: 'POST',
            url: '/Admin/Products/EditProduct',
            data: productInfo,
            processData: false,
            contentType: false,
            success: function (data) {

                //else {

                $("#div-loading").addClass('hidden');
                $("#viewAllProducts").load("/Admin/Products/ViewAllProducts/" + data.productGroupId);
                swal({
                    title: " محصول",
                    text: "محصول " + data.productTitle + " با موفقیت ویرایش شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                });

                $("#DescriptionEdit").val('');
                $("#editProductForm")[0].reset();
                //}
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
///

function checkboxSelectedForProduct() {
    swal({
        type: "warning",
        title: "هشدار",
        text: "آیا برای حذف این محصولات انتخاب شده مطمئن هستید؟",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {
                var checkboxValues = "";
                $("div.checked").children('.forRemoveProduct').each(function () {
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Products/checkboxSelected",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "ok") {
                            for (var i = 0; i < cv.length - 1; i++) {
                                $('#productsTb').find('tr[id=product_' + cv[i] + ']').css('background-color', '#B26666').fadeOut(1000, function () {

                                    var table = $('#productsTb').DataTable();
                                    table.row("#product_" + cv[i]).remove().draw(false);
                                });
                            }
                            swal({
                                title: " محصول",
                                text: "محصول با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                    });
                }
                else {
                    swal({
                        title: " محصول",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
            }
        });
}
/////////////////////////////// بخش رنگ محصولات

function GetAllProductColorByProductId(productId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Products/ViewAllProductColor",
        data: { id: productId },
        success: function (result) {
            $(".products_tabs").removeClass("active");
            $("#products").removeClass("active");
            $("#productColor").addClass("active");

            $("#productsTab").removeClass("active");
            $("#productColorTab").addClass("active");


            $("#viewAllProductColor").html(result);
        },
        error: function (res) {

        }
    });
}
///
function GetAllColorByLang() {
    $.ajax({
        method: "GET",
        url: "/Admin/Products/GetAllColorForAdd",
        data: { id: $("#language_ddl").val() },
        success: function (result) {

            var option = "";
            result.forEach(function (obj) {
                option += "<option value=" + obj.value + " style='background-color:" + obj.value + ";color:'white''>" + obj.text + "</option>";
                //$("#ColorId").prop("disabled", false).css('background-color', obj.value);
            });

            $("#ColorId").html(option);

        },
        error: function (res) {

        }
    });
}
//
function openModalForAddProductColor() {
    var id = $("#language_ddl").val();
    $.get("/Admin/Products/CreatePoductColor/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن رنگ محصول ");
        $("#myModalBody").html(result);
    });
    GetAllColorByLang();
}
//
function InsertProductColor() {
    $("#div-loading").removeClass('hidden');
    var producColor = {
        ProductId: $("#pInfoId").text(),
        ProductPrice: $("#ProductPrice").val(),
        ColorId: $("#ColorId").val(),
        ProductCount: $("#ProductCount").val()
    }

    $.ajax({
        method: 'POST',
        url: '/Admin/Products/CreatePoductColor',
        data: producColor,
        success: function (data) {
            if (data == "nall") {
                swal({
                    title: " رنگ محصول",
                    text: "برای ثبت رنگ محصول  ابتدا از بخش محصولات , محصولی را انتخاب نمائید",
                    type: "warning",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                });
                return false;
            }
            $("#viewAllProductColor").load("/Admin/Products/ViewAllProductColor/" + data.productId);
            $("#myModal").modal("hide");
            swal({
                title: " رنگ محصول",
                text: "رنگ محصول با موفقیت ثبت شد",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید",
            });
        },
        error: function (result) {
        }
    });
}

///
function checkboxSelectedForProductColor() {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این رنگ محصول  مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {
                var checkboxValues = "";
                $("div.checked").children('.forRemoveProductColor').each(function () {
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Products/checkboxSelectedProductColor",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "ok") {
                            for (var i = 0; i < cv.length - 1; i++) {
                                $('#product_color').find('tr[id=pcolor_' + cv[i] + ']').css('background-color', '#B26666').fadeOut(1000, function () {

                                    var table = $('#product_color').DataTable();
                                    table.row("#pcolor_" + cv[i]).remove().draw(false);
                                });
                            }
                            swal({
                                title: " رنگ محصول",
                                text: "رنگ محصول با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                    });
                }
                else {
                    swal({
                        title: " رنگ محصول",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
            }
        });
}


/////////////////////////////// بخش ویژگی محصولات
function GetAllProductFeatureByProductId(productId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Products/ViewAllProductFeaturesByProductId",
        data: { id: productId },
        success: function (result) {

            $("#products").removeClass("active");
            $("#productFeature").addClass("active");

            $("#productsTab").removeClass("active");
            $("#productFeatureTab").addClass("active");

            $("#addProductFeature").load("/Admin/Products/CreateProductFeature/" + productId);
            $("#viewAllProductFeatures").html(result);
        },
        error: function (res) {

        }
    });
}
/////
function openModalForAddProductFeature() {
    var id = $("#pInfoId").text();

    $.get("/Admin/Products/CreateProductFeature/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن ویژگی محصول ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '600px' });
    });
}
////
function GetAllProductFeatureReplyByFeatureId(FeatureId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Products/GetAllFeatureReplyByFeatureId",
        data: { id: FeatureId },
        success: function (result) {
            var option = "";
            result.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $("#featureReply_ddl").html(option);
        },
        error: function (res) {

        }
    });
}
////////
function InsertProductFeature(featureId) {
    if ($("#pInfoId").text() != "") {
        $("#loading").removeClass('hidden');
        var productFeature = {
            ProductInfoId: $("#pInfoId").text(),
            FeatureReplyId: $("#fr_" + featureId).val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Products/CreateProductFeature',
            data: productFeature,
            success: function (data) {
                if (data != "repeate") {
                    debugger;
                    $("#viewAllProductFeatures").load("/Admin/Products/ViewAllProductFeaturesByProductId/" + data.productInfoId);
                    $("#myModal").modal("hide");
                    swal({
                        title: " ویژگی محصول",
                        text: "ویژگی محصول با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
                else {
                    swal({
                        title: " ویژگی محصول",
                        text: "این ویژگی در لیست ویژگی های محصول وجود دارد",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        swal({
            title: " ویژگی محصول",
            text: "برای ثبت ویژگی محصول ابتدا از بخش محصولات , محصولی را انتخاب نمایید",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید",
        });
    }
}
///////
function checkboxSelectedForProductFeatures() {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این ویژگی محصول  مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {
                var checkboxValues = "";
                var featureReplyId = "";
                $("div.checked").children('.forRemoveFeature').each(function () {
                    checkboxValues += $(this).val() + "&";
                    featureReplyId += $(this).attr('id') + "&";
                });

                var cv = [];
                var fr = [];
                cv = checkboxValues.split("&");
                fr = featureReplyId.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Products/checkboxSelectedProductFeature",
                        type: "Get",
                        data: { values: checkboxValues, featureReply: featureReplyId }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        var el;
                        if (data == "ok") {
                            for (var i = 0; i < cv.length - 1; i++) {
                                debugger;
                                el = "pf_" + cv[i] + "_" + fr[i];

                                $('#productFeature_tb').find('tr[id=' + el + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                    var table = $('#productFeature_tb').DataTable();
                                    table.row("#" + el).remove().draw(false);
                                });

                            }
                            swal({
                                title: " ویژگی محصول",
                                text: "ویژگی محصول با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                    });
                }
                else {
                    swal({
                        title: " ویژگی محصول",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
            }
        });
}
/////////////////////////////// بخش محصولات مشابه
function GetAllSimilarProductsByProductId(productId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Products/ViewAllSimilarProducts",
        data: { id: productId },
        success: function (result) {

            $("#products").removeClass("active");
            $("#similarProduct").addClass("active");

            $("#productsTab").removeClass("active");
            $("#similarProductTab").addClass("active");

            $("#viewAllSimilarProduct").load("/Admin/Products/ViewAllSimilarProducts/" + productId);
            //$("#viewAllSimilarProduct").html(result);
        },
        error: function (res) {

        }
    });
}
////
function InsertSimilarProduct(similarId) {
    if ($("#pInfoId").text() != "") {
        $("#div-loading").removeClass('hidden');
        var similarProduct = {
            ProductID: $("#pInfoId").text(),
            SimilarProduct_ID: similarId
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Products/CreateSimilarProduct',
            data: similarProduct,
            success: function (data) {
                if (data == "repeate") {
                    swal({
                        title: " محصول مشابه ",
                        text: "این محصول به لیست محصولات مشابه اضافه شده است",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
                else if (data == "error") {
                    swal({
                        title: " محصول مشابه ",
                        text: "مشابه محصول با خود محصول یکسان است",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
                else {//if (data == "ok")
                    debugger;
                    $("#viewAllSimilarProduct").load("/Admin/Products/ViewAllSimilarProducts/" + data.productID);
                    $("#myModal").modal("hide");
                    swal({
                        title: " محصول مشابه ",
                        text: "محصول مشابه با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        swal({
            title: " محصول مشابه ",
            text: "برای ثبت محصول مشابه ابتدا از بخش محصولات , محصولی را انتخاب نمایید",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید",
        });
    }
}
////
function DeleteSimilarProduct(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این محصول مشابه مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Products/DeleteSimilarProduct',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        $("#div-loading").addClass('hidden');
                        swal({
                            title: " محصول مشابه ",
                            text: "محصول مشابه مورد نظر با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید",
                        });

                        $('#similar_tb').find('tr[id=sim_' + Id + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#similar_tb').DataTable();
                            table.row("#sim_" + Id).remove().draw(false);
                        });

                    },
                    error: function (result) {
                    }
                });
            }
        });
}
/////////////////////////////// بخش گارانتی محصولات

function GetAllProductGarantyByProductId(productId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Products/ViewAllProductGaranty",
        data: { id: productId },
        success: function (result) {

            $("#products").removeClass("active");
            $("#productGaranty").addClass("active");

            $("#productsTab").removeClass("active");
            $("#productGarantyTab").addClass("active");

            $("#viewAllProductGaranty").html(result);
        },
        error: function (res) {

        }
    });
}

///
function openModalForAddProductGaranty() {
    var id = $("#language_ddl").val();
    $.get("/Admin/Products/CreateProductGaranty/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گارانتی محصول ");
        $("#myModalBody").html(result);
    });
}

///
function InsertProductGaranty() {
    if ($("#pInfoId").text() != "") {
        $("#div-loading").removeClass('hidden');
        var productGaranty = {
            ProductInfoId: $("#pInfoId").text(),
            GarantyId: $("#GarantyId").val(),
            GarantyPrice: $("#GarantyPrice").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Products/CreateProductGaranty',
            data: productGaranty,
            success: function (data) {

                $("#viewAllProductGaranty").load("/Admin/Products/ViewAllProductGaranty/" + data.productInfoId);
                $("#myModal").modal("hide");
                swal({
                    title: " گارانتی محصول ",
                    text: "گارانتی محصول با موفقیت ثبت شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                });
            },
            error: function (result) {
            }
        });
    }
    else {
        swal({
            title: " گارانتی محصول ",
            text: "برای ثبت گارانتی محصول ابتدا از بخش محصولات , محصولی را انتخاب نمایید",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید",
        });
    }
}
///
function checkboxSelectedForProductGaranty() {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این گارانتی محصول  مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"

    })
        .then((willDelete) => {
            if (willDelete.value) {
                var checkboxValues = "";
                var garantyId = "";
                $("div.checked").children('.forRemoveProductGaranty').each(function () {
                    checkboxValues += $(this).val() + "&";
                    garantyId += $(this).attr('id') + "&";
                });

                var cv = [];
                var fr = [];
                cv = checkboxValues.split("&");
                fr = garantyId.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Products/checkboxSelectedProductGaranty",
                        type: "Get",
                        data: { values: checkboxValues, garantyID: garantyId }
                    }).done(function (data) {
                        debugger;
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        var el;
                        if (data == "ok") {
                            for (var i = 0; i < cv.length - 1; i++) {
                                el = "pg_" + cv[i] + "_" + fr[i];
                                $('#productGaranty_tb').find('tr[id=' + el + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                    var table = $('#productGaranty_tb').DataTable();
                                    table.row("#" + el).remove().draw(false);
                                });

                            }
                            swal({
                                title: " گارانتی محصول ",
                                text: "گارانتی محصول با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                    });
                }
                else {
                    swal({
                        title: " گارانتی محصول ",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
            }
        });
}
/////////////////////////////// بخش مزایا  / معایب محصولات
function GetAllProductAdvantageByProductId(productId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Products/ViewAllAdvantage",
        data: { id: productId },
        success: function (result) {

            $("#products").removeClass("active");
            $("#productAdvantage").addClass("active");

            $("#productsTab").removeClass("active");
            $("#productAdvantageTab").addClass("active");

            $("#viewAllProductAdvantage").html(result);
        },
        error: function (res) {

        }
    });
}
////
function openModalForAddProductAdvantage() {
    $.get("/Admin/Products/CreateAdvantage", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن قابلیت برای محصول ");
        $("#myModalBody").html(result);
    });
}
////
function InsertProductAdvantage() {
    var frm = $("#advantageFrm").valid();
    if (frm === true) {

        if ($("#pInfoId").text() != "") {
            $("#div-loading").removeClass('hidden');

            var type = $("#adType_ddl").val();
            if (type == 1) {
                type = true;
            }
            else {
                type = false;
            }
            var productAdvantage = {
                ProductInfoId: $("#pInfoId").text(),
                AdvantageTitle: $("#AdvantageTitle").val(),
                AdvantageType: type
            }

            $.ajax({
                method: 'POST',
                url: '/Admin/Products/CreateAdvantage',
                data: productAdvantage,
                success: function (data) {

                    $("#viewAllProductAdvantage").load("/Admin/Products/ViewAllAdvantage/" + data.productInfoId);
                    $("#myModal").modal("hide");
                    if ($("#adType_ddl").val() == "0") {
                        swal({
                            title: " قابلیت محصول ",
                            text: "قابلیت محصول با موفقیت ثبت شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید",
                        });
                    }
                    else {
                        swal({
                            title: " قابلیت محصول ",
                            text: "قابلیت محصول با موفقیت ثبت شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید",
                        });
                        $("#AdvantageTitle").val('');
                    }
                    $("#div-loading").addClass('hidden');
                },
                error: function (result) {
                }
            });
        }
        else {
            swal({
                title: " قابلیت محصول ",
                text: "برای ثبت قابلیت محصول ابتدا از بخش محصولات , محصولی را انتخاب نمایید",
                type: "warning",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید",
            });
        }
    }
    else {
        return false;
    }
}

///
function openModalForEditProductAdvantage(id) {
    $.get("/Admin/Products/EditAdvantage/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش قابلیت محصول ");
        $("#myModalBody").html(result);
    });
}
///
function UpdateProductAdvantage() {
    var frm = $("#advantageFrmEdit").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');

        var type = $("#adType_ddl").val();
        if (type == 1) {
            type = true;
        }
        else {
            type = false;
        }
        var productAdvantage = {
            Advantage_ID: $("#Advantage_ID").val(),
            ProductInfoId: $("#pInfoId").text(),
            AdvantageTitle: $("#AdvantageTitle").val(),
            AdvantageType: type
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Products/EditAdvantage',
            data: productAdvantage,
            success: function (data) {

                $("#viewAllProductAdvantage").load("/Admin/Products/ViewAllAdvantage/" + data.productInfoId);
                $("#myModal").modal("hide");
                if ($("#adType_ddl").val() == "0") {
                    swal({
                        title: " قابلیت محصول ",
                        text: "قابلیت  محصول با موفقیت ویرایش شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
                else {
                    swal({
                        title: " قابلیت محصول ",
                        text: "قابلیت  محصول با موفقیت ویرایش شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                    $("#AdvantageTitle").val('');
                }
                $("#div-loading").addClass('hidden');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

///
///
function checkboxSelectedForProductAdvantage() {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این قابلیت  مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"

    })
        .then((willDelete) => {
            if (willDelete.value) {
                var checkboxValues = "";
                $("div.checked").children('.forRemove').each(function () {
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Products/checkboxSelectedProductAdvantage",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "ok") {
                            for (var i = 0; i < cv.length - 1; i++) {


                                $('#pAdvantage').find('tr[id=advan_' + cv[i] + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                    var table = $('#pAdvantage').DataTable();
                                    table.row("#advan_" + cv[i]).remove().draw(false);
                                });


                            }
                            swal({
                                title: " قابلیت محصول ",
                                text: "قابلیت محصول با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                    });
                }
                else {
                    swal({
                        title: " قابلیت محصول ",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
            }
        });
}
/////////////////////////////// بخش مزایا  / معایب پروژه ها
function GetAllProjectsAdvantageByProjectsId(projectId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Projects/ViewAllAdvantage",
        data: { id: projectId },
        success: function (result) {

            $("#projects").removeClass("active");
            $("#advantage").addClass("active");

            $("#projectTab").removeClass("active");
            $("#advantageTab").addClass("active");

            $("#viewAllProjectsAdvantage").html(result);
        },
        error: function (res) {

        }
    });
}
////
function openModalForAddProjectsAdvantage() {
    $.get("/Admin/Projects/CreateAdvantage", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن قابلیت برای پروژه ");
        $("#myModalBody").html(result);
    });
}
////
function InsertProjectsAdvantage() {
    var frm = $("#advantageFrm").valid();
    if (frm === true) {

        if ($("#pInfoId").text() != "") {
            $("#div-loading").removeClass('hidden');

            var type = $("#adType_ddl").val();
            if (type == 1) {
                type = true;
            }
            else {
                type = false;
            }
            var projectsAdvantage = {
                ProductInfoId: $("#pInfoId").text(),
                AdvantageTitle: $("#AdvantageTitle").val(),
                AdvantageType: type
            }

            $.ajax({
                method: 'POST',
                url: '/Admin/Projects/CreateAdvantage',
                data: projectsAdvantage,
                success: function (data) {

                    $("#viewAllProjectsAdvantage").load("/Admin/Projects/ViewAllAdvantage/" + data.productInfoId);
                    $("#myModal").modal("hide");
                    if ($("#adType_ddl").val() == "0") {
                        swal({
                            title: " قابلیت پروژه ",
                            text: "قابلیت پروژه با موفقیت ثبت شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید",
                        });
                    }
                    else {
                        swal({
                            title: " قابلیت پروژه ",
                            text: "قابلیت پروژه با موفقیت ثبت شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید",
                        });
                    }
                    $("#div-loading").addClass('hidden');
                },
                error: function (result) {
                }
            });
        }
        else {
            swal({
                title: " قابلیت پروژه ",
                text: "برای ثبت قابلیت پروژه ابتدا از بخش پروژه ها , پروژه ای را انتخاب نمایید",
                type: "warning",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید",
            });
        }
    }
    else {
        return false;
    }
}

///
function openModalForEditProjectsAdvantage(id) {
    $.get("/Admin/Projects/EditAdvantage/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش قابلیت پروژه ");
        $("#myModalBody").html(result);
    });
}
///
function UpdateProjectsAdvantage() {
    var frm = $("#advantageFrmEdit").valid();
    if (frm === true) {


        $("#div-loading").removeClass('hidden');

        var type = $("#adType_ddl").val();
        if (type == 1) {
            type = true;
        }
        else {
            type = false;
        }
        var projectsAdvantage = {
            Advantage_ID: $("#Advantage_ID").val(),
            ProductInfoId: $("#pInfoId").text(),
            AdvantageTitle: $("#AdvantageTitle").val(),
            AdvantageType: type
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Projects/EditAdvantage',
            data: projectsAdvantage,
            success: function (data) {

                $("#viewAllProjectsAdvantage").load("/Admin/Projects/ViewAllAdvantage/" + data.productInfoId);
                $("#myModal").modal("hide");
                if ($("#adType_ddl").val() == "0") {
                    swal({
                        title: " قابلیت پروژه ",
                        text: "قابلیت پروژه با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
                else {
                    swal({
                        title: " قابلیت پروژه ",
                        text: "قابلیت پروژه با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
                $("#div-loading").addClass('hidden');
            },
            error: function (result) {
            }
        });

    }
    else {
        return false;
    }
}

///
///
function checkboxSelectedForProjectAdvantage() {
    swal({
        title: "هشدار",
        text: "آیا برای حذف قابلیت های این محصول مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {
                var checkboxValues = "";
                $("div.checked").children('.forRemoveAdvantage').each(function () {
                    checkboxValues += $(this).val() + "&";
                });
                debugger;
                var cv = [];
                cv = checkboxValues.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Projects/checkboxSelectedProjectsAdvantage",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {
                        debugger;
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "ok") {
                            for (var i = 0; i < cv.length - 1; i++) {
                                $('#pjAdvantage').find('tr[id=advan_' + cv[i] + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                    var table = $('#pjAdvantage').DataTable();
                                    table.row("#advan_" + cv[i]).remove().draw(false);
                                });


                            }
                            swal({
                                title: " قابلیت پروژه ",
                                text: "قابلیت پروژه با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                    });
                }
                else {
                    swal({
                        title: " قابلیت پروژه ",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
            }
        });
}
//////////////////////////////////// مربوط به بخش ویدئو های محصول
function getAllAVFGroupForTreeViewInProject() { //خواندن گروه ها برای تری ویو ها
    var lang = $("#language_ddl").val();

    $.ajax({
        method: 'POST',
        url: '/Admin/Projects/createTreeViewVideo',
        data: { pgId: 1, lang: lang },
        //data: "{'parentId':'0','lang':'" + lang + "','trviewElement':''}",
        success: function (response) {
            var $checkableTree = $('#video_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    GetAllVideoByIdInProject(node.tags);
                }
            });

        },
        error: function (response) {

        }
    });
}

function GetAllVideoByIdInProject(groupId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Projects/GetAllVideoByIdInProject",
        data: { id: groupId },
        success: function (result) {

            var option = "";
            result.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $("#videos_ddl").html(option);
            //$("#SSGId").html(option);
            //
        },
        error: function (res) {

        }
    });
}

function getAllProjectVideoById(projectId) {
    $.ajax({ //
        method: "GET",
        url: "/Admin/Projects/ViewAllVideo",
        data: { id: projectId },
        success: function (result) {
            $("#productId_lbl").text(projectId);
            $("#projects").removeClass("active");
            $("#videos").addClass("active");

            $("#projectTab").removeClass("active");
            $("#videoTab").addClass("active");
            $("#viewAllProjectsVideo").html(result);
            //
        },
        error: function (res) {

        }
    });
}

function InsertProjectVideo() {

    if ($("#productId_lbl").text() != "") {
        $("#div-loading").removeClass('hidden');

        var values = [];
        $('#videos_lst option').each(function () {
            values.push($(this).attr('value'));
        });

        var projectVideo = {
            ProjectId: $("#productId_lbl").text(),
            VideoIds: values
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Projects/CreateProjectVideo',
            data: projectVideo,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                if (data.status == true) {
                    $("#viewAllProjectsVideo").load("/Admin/Projects/ViewAllVideo/" + data.projectId);
                    swal({
                        title: " ویدئو پروژه ",
                        text: "ویدئوهای پروژه با موفقیت ثبت گردید",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                    $("#videos_lst").html('');
                }
                else if (data.status == "repeate") {
                    swal({
                        title: " ویدئو پروژه ",
                        text: " ویدئو قبلا برای این محصول ثبت شده است",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
                else {
                    swal({
                        title: " ویدئو پروژه ",
                        text: "خطایی رخ داده است ",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        swal({
            title: " ویدئو پروژه ",
            text: "برای ثبت ویدئو محصولی انتخاب نشده ",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید",
        });
    }
}
///////
function checkboxSelectedForProjectsVideo() {

    swal({
        title: "هشدار",
        text: "آیا برای حذف ویدئو محصول مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                var checkboxValues = "";
                $("div.checked").children('.forRemove').each(function () {
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Projects/checkboxSelectedProjectsVideo",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        for (var i = 0; i < cv.length - 1; i++) {
                            $("#pjv_" + cv[i]).hide("slow");
                        }
                        swal({
                            title: " ویدئو پروژه ",
                            text: "ویدئوهای این محصول با موفقیت حذف شد ",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید",
                        });
                    });
                }
                else {
                    swal({
                        title: " ویدئو پروژه ",
                        text: "لطفا یک مورد را انتخاب نمایید ",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
            }
        });
}

///////
/////////////////////////////// بخش نظرات محصول
function GetAllCommentsByProductId(productId) {//

    $.ajax({ //
        method: "GET",
        url: "/Admin/Products/ViewAllComments",
        data: { id: productId },
        success: function (result) {

            $("#products").removeClass("active");
            $("#productComment").addClass("active");

            $("#productsTab").removeClass("active");
            $("#productCommentTab").addClass("active");

            $("#viewAllProductComments").html(result);
            //
        },
        error: function (res) {

        }
    });
}
////
function openModalForEditProductComment(id) {
    $.get("/Admin/Products/EditComment/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش وضعیت نظرات ");
        $("#myModalBody").html(result);
    });

}
//
function UpdateProductComment() {
    $("#div-loading").removeClass('hidden');
    var status = $("#IsConfirmedEdit:checked").length;

    if (status == 1) {
        status = "true";
    }
    else {
        status = "false";
    }
    var productComment = {
        Comment_ID: $("#Comment_ID").val(),
        Comment: $("#Comment").val(),
        CommentReply: $("#CommentReply").val(),
        UserName: $("#UserName").val(),
        Email: $("#Email").val(),
        CommentDate: $("#dateComment").val(),
        PositiveScore: $("#PositiveScore").val(),
        NegativeScore: $("#NegativeScore").val(),
        IsConfirmed: status,
        IsNew: false,
        ProductInforId: $("#ProductInforId").val(),
        ProductGroupId: $("#ProductGroupId").val(),
        CommentDate: $("#CommentDate").val(),

    }


    $.ajax({
        method: 'POST',
        url: '/Admin/Products/EditComment',
        data: productComment,
        success: function (data) {


            $("#div-loading").addClass('hidden');

            $("#viewAllProductComments").load("/Admin/Products/ViewAllComments/" + data.productInforId);
            $("#myModal").modal('hide');
            swal({
                title: "نظرات محصول ",
                text: "وضعیت این نظر با موفقیت ویرایش شد ",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید",
            });

        },
        error: function (result) {
        }
    });
}

////
function checkboxSelectedForProductComments() {

    swal({
        title: "هشدار",
        text: "آیا برای حذف نظرات محصولات مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                var checkboxValues = "";
                $("div.checked").children('.forRemove').each(function () {
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Products/checkboxSelectedComments",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        for (var i = 0; i < cv.length - 1; i++) {
                            $("#comment_" + cv[i]).hide("slow");
                        }
                        swal({
                            title: "نظرات محصول ",
                            text: "نظر این محصول با موفقیت حذف شد ",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید",
                        });
                    });
                }
                else {
                    swal({
                        title: "نظرات محصول ",
                        text: "لطفا یک مورد را انتخاب نمایید ",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید",
                    });
                }
            }
        });
}
/////////////////////////////// بخش تولیدکننده
function openModalForAddProducer() {
    $.get("/Admin/Producer/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن تولید کننده محصول ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}
////
function InsertProducer() {
    var frm = $("#producerFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');
        var producer = new FormData();
        var files = $("#producerUploader").get(0).files;


        producer.append("ProducerTitle", $("#ProducerTitle").val()),
            producer.append("Description", $("#Description").val()),
            producer.append("producerImage", files[0])
        producer.append("Lang", $("#language_ddl").val())

        $.ajax({
            method: 'POST',
            url: '/Admin/Producer/Create',
            data: producer,
            contentType: false,
            processData: false,
            success: function (data) {

                $("#viewAll").load("/Admin/Producer/ViewAll/" + data.lang);
                $("#myModal").modal("hide");
                swal({
                    title: "تولید کننده ",
                    text: "تولید کننده " + data.producerTitle + " با موفقیت ثبت شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                });
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
////
function openModalForEditProducer(id) {
    $.get("/Admin/Producer/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش تولید کننده محصول ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}
////
function UpdateProducer() {
    var frm = $("#producerFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');
        var producer = new FormData();
        var files = $("#producerUploader").get(0).files;


        producer.append("Producer_ID", $("#Producer_ID").val()),
            producer.append("ProducerTitle", $("#ProducerTitle").val()),
            producer.append("Description", $("#Description").val()),
            producer.append("Icon", $("#oldImage").val()),
            producer.append("producerImage", files[0])
        producer.append("Lang", $("#language_ddl").val())

        $.ajax({
            method: 'POST',
            url: '/Admin/Producer/Edit',
            data: producer,
            contentType: false,
            processData: false,
            success: function (data) {

                $("#viewAll").load("/Admin/Producer/ViewAll/" + data.lang);
                $("#myModal").modal("hide");
                swal({
                    title: "تولید کننده ",
                    text: "تولید کننده " + data.producerTitle + " با موفقیت ویرایش شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                });
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
///
function DeleteProducer(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف تولیدکننده مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Producer/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "NOK") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "تولید کننده ",
                                text: "این تولیدکننده برای محصولی تعریف شده است ابتدا محصول باید حذف شود",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                        else {
                            debugger;
                            console.log(data.producer_ID);
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "تولید کننده ",
                                text: "تولیدکننده " + data.producerTitle + "  با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });

                            $('#producer_tb').find('tr[id=producer_' + data.producer_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#producer_tb').DataTable();
                                table.row("#producer_" + data.producer_ID).remove().draw(false);
                            });

                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
////////////////////////////// بخش گارانتی
function openModalForAddGaranty() {
    $.get("/Admin/Garanty/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گارانتی جدید ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}
/////
function InsertGaranty() {
    var frm = $("#garantyFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');
        var garanty = new FormData();
        var files = $("#garantyUploader").get(0).files;

        var isactive = $('#isActiveGaranty:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        garanty.append("GarantyName", $("#GarantyName").val()),
            garanty.append("Description", $("#Description").val()),
            garanty.append("imgGaranty", files[0]),
            garanty.append("IsActive", status),
            garanty.append("Lang", $("#language_ddl").val())

        $.ajax({
            method: 'POST',
            url: '/Admin/Garanty/Create',
            data: garanty,
            contentType: false,
            processData: false,
            success: function (data) {

                $("#viewAll").load("/Admin/Garanty/ViewAll/" + data.lang);
                $("#myModal").modal("hide");
                swal({
                    title: "گارانتی ",
                    text: "گارانتی " + data.garantyName + " با موفقیت ثبت شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                });
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
///
function openModalForEditGaranty(id) {
    $.get("/Admin/Garanty/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گارانتی  ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });
}
///
function UpdateGaranty() {
    var frm = $("#garantyFrmEdit").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');
        var garanty = new FormData();
        var files = $("#garantyUploader").get(0).files;

        var isactive = $('#isActiveGaranty:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        garanty.append("Garanty_ID", $("#Garanty_ID").val()),
            garanty.append("GarantyName", $("#GarantyName").val()),
            garanty.append("Description", $("#Description").val()),
            garanty.append("Icon", $("#oldImage").val()),
            garanty.append("imgGaranty", files[0]),
            garanty.append("IsActive", status),
            garanty.append("Lang", $("#language_ddl").val())

        $.ajax({
            method: 'POST',
            url: '/Admin/Garanty/Edit',
            data: garanty,
            contentType: false,
            processData: false,
            success: function (data) {

                $("#viewAll").load("/Admin/Garanty/ViewAll/" + data.lang);
                $("#myModal").modal("hide");
                swal({
                    title: "گارانتی ",
                    text: "گارانتی " + data.garantyName + " با موفقیت ویرایش شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                });
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
///
function DeleteGaranty(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف گارانتی مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Garanty/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "NOK") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "گارانتی ",
                                text: "این گارانتی برای محصولی تعریف شده است ابتدا محصول باید حذف شود ",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "گارانتی ",
                                text: "گارانتی " + data.garantyName + "  با موفقیت حذف شد ",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });

                            $('#garanty_tb').find('tr[id=garanty_' + data.garanty_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#garanty_tb').DataTable();
                                table.row("#garanty_" + data.garanty_ID).remove().draw(false);
                            });

                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
////////////////////////////// بخش واحد
function openModalForAddUnit() {
    $.get("/Admin/Unit/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن واحد جدید ");
        $("#myModalBody").html(result);
    });

}
//
function InsertUnit() {
    var frm = $("#unitFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');

        var unit = {
            UnitTitle: $("#UnitTitle").val(),
            Lang: $("#language_ddl").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Unit/Create',
            data: unit,
            success: function (data) {

                $("#viewAll").load("/Admin/Unit/ViewAll/" + data.lang);
                $("#myModal").modal("hide");
                swal({
                    title: "واحد محصول ",
                    text: "واحد با عنوان " + data.unitTitle + " با موفقیت ثبت شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                });
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function openModalForEditUnit(id) {
    $.get("/Admin/Unit/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش واحد  ");
        $("#myModalBody").html(result);
    });

}
//
function UpdateUnit() {
    var frm = $("#unitFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var unit = {
            Unit_ID: $("#Unit_ID").val(),
            UnitTitle: $("#UnitTitle").val(),
            Lang: $("#language_ddl").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Unit/Edit',
            data: unit,
            success: function (data) {

                $("#viewAll").load("/Admin/Unit/ViewAll/" + data.lang);
                $("#myModal").modal("hide");
                swal({
                    title: "واحد محصول ",
                    text: "واحد با عنوان " + data.unitTitle + " با موفقیت ویرایش شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید",
                });
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function DeleteUnit(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف واحد مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Unit/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "NOK") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "واحد محصول ",
                                text: "این واحد برای محصولی تعریف شده است ابتدا محصول باید حذف شود",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });
                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "واحد محصول ",
                                text: "واحد " + data.unitTitle + "  با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید",
                            });

                            $('#unit_tb').find('tr[id=unit_' + data.unit_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#unit_tb').DataTable();
                                table.row("#unit_" + data.unit_ID).remove().draw(false);
                            });

                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
//////////////////////////// بخش رنگ ها
function openModalForAddColor() {
    $.get("/Admin/Color/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن رنگ جدید ");
        $("#myModalBody").html(result);
    });

}
//
function InsertColor() {
    var frm = $("#colorFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var color = {
            ColorTitle: $("#ColorTitle").val(),
            ColorCode: $("#ColorCode").val(),
            Lang: $("#language_ddl").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Color/Create',
            data: color,
            success: function (data) {
                if (data == "repeate") {
                    $("#div-loading").addClass('hidden');
                    swal({

                        type: 'warning',
                        title: "افزودن رنگ",
                        text: "این کد رنگ قبلا ثبت شده",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });


                }
                else if (data != "nall") {

                    $("#viewAll").load("/Admin/Color/ViewAll/" + data.lang);
                    $("#myModal").modal("hide");

                    swal({

                        type: 'success',
                        title: "افزودن رنگ",
                        text: "رنگ  " + data.colorTitle + " با موفقیت ثبت شد ",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

//
function openModalForEditColor(id) {
    $.get("/Admin/Color/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش رنگ");
        $("#myModalBody").html(result);
    });

}
//
function UpdateColor() {
    debugger;
    var frm = $("#colorFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var color = {
            Color_ID: $("#Color_ID").val(),
            ColorTitle: $("#ColorTitle").val(),
            ColorCode: $("#ColorCode").val(),
            Lang: $("#language_ddl").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Color/Edit',
            data: color,
            success: function (data) {

                $("#viewAll").load("/Admin/Color/ViewAll/" + data.lang);
                $("#myModal").modal("hide");
                swal({
                    type: 'success',
                    title: "افزودن رنگ",
                    text: "رنگ  " + data.colorTitle + " با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                $("#div-loading").addClass('hidden');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function DeleteColor(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این رنگ مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Color/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "NOK") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                type: 'warning',
                                title: "افزودن رنگ",
                                text: "این واحد برای محصولی تعریف شده است ابتدا محصول باید حذف شود",
                                showConfirmButton: true,
                                confirmButtonText: "تائید"
                            });

                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                type: 'success',
                                title: "افزودن رنگ",
                                text: "رنگ " + data.colorTitle + "  با موفقیت حذف شد",
                                showConfirmButton: true,
                                confirmButtonText: "تائید"
                            });
                            $('#color_tb').find('tr[id=color_' + data.color_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#color_tb').DataTable();
                                table.row("#color_" + data.color_ID).remove().draw(false);
                            });
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}

/////////////////////////////////////////// زبان
function InsertLang() {
    var frm = $("#lang_form").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActiveLang:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var lang = {
            Lang_ID: $("#Lang_ID").val(),
            Lang_Name: $("#Lang_Name").val(),
            IsActive: status
        }
        debugger;
        $.ajax({
            method: 'POST',
            url: '/Admin/Language/Create',
            data: lang,
            success: function (data) {
                if (data == "repeate") {
                    $("#div-loading").addClass('hidden');
                    swal({

                        type: 'warning',
                        title: "افزودن زبان",
                        text: "این شناسه زبان قبلا ثبت شده",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });


                }
                else if (data != "nall") {

                    $("#viewAll").load("/Admin/Language/ViewAll/" + data.lang);
                    $("#myModal").modal("hide");

                    swal({

                        type: 'success',
                        title: "افزودن زبان",
                        text: "زبان  " + lang.Lang_Name + " با موفقیت ثبت شد ",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
function UpdateLang() {
    debugger;
    var frm = $("#lang_form").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActiveLang:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var lang = {
            Lang_ID: $("#Lang_ID").val(),
            Lang_Name: $("#Lang_Name").val(),
            IsActive: status
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Language/Edit',
            data: lang,
            success: function (data) {
                if (data == "repeate") {
                    swal({

                        type: 'warning',
                        title: "ویرایش زبان",
                        text: "این شناسه زبان قبلا ثبت شده",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                    return false;

                }
                debugger;
                swal({
                    type: 'success',
                    title: "ویرایش زبان",
                    text: "زبان  " + data.lang_ID + " با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });
                $("#viewAll").load("/Admin/Language/ViewAll/" + data.lang_ID);
                $("#myModal").modal("hide");


                $("#div-loading").addClass('hidden');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

/////////////////////////////////////// بخش فایل های دانلودی


var avfParentId, groupIdForAddFile = "", avfGroupId, avfGroupIdEdit = "";
function getAllAVFGroupForTreeView() { //خواندن گروه ها برای تری ویو ها
    var lang = $("#language_ddl").val();

    $.ajax({
        method: 'POST',
        url: '/Admin/FilesManage/createTreeView',
        data: { pgId: 1, lang: lang },
        //data: "{'parentId':'0','lang':'" + lang + "','trviewElement':''}",
        success: function (response) {
            var $checkableTree = $('#fileManageGroup1_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    GetAVFGroupById(node.tags);//نمایش جزئیات گروه ها در لیست جزئیات
                    avfGroupId = node.tags;
                }
            });

            var $checkableTree2 = $('#fileManageGroup2_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    avfGroupIdEdit = node.tags;
                    groupIdForAddFile = node.tags;
                    GetAllFilesByGroupId(node.tags);
                }
            });

        },
        error: function (response) {

        }
    });
}
function GetAVFGroupById(parentId) {

    $.ajax({
        method: 'POST',
        url: '/Admin/FilesManage/ViewGroupData',
        data: { id: parentId },
        success: function (data) {

            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}

function openModalForAddAVFGroup() {
    $.get("/Admin/FilesManage/CreateGroup", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گروه دانلود جدید ");
        $("#myModalBody").html(result);
    });

}
//
function InsertAVFGroup() {
    var frm = $("#fileGroupFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActiveAVFG:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }


        if (avfGroupId == undefined) {
            avfGroupId = null;
        }
        debugger;
        var file = $('#ImageUpload').get(0).files;
        var aVFGroup = new FormData();

        aVFGroup.append("AVFGroupTitle", $("#AVFGroupTitle").val()),
            aVFGroup.append("IsActive", status),
            aVFGroup.append("ParentId", avfGroupId),
            aVFGroup.append("Lang", $("#language_ddl").val()),
            aVFGroup.append("AVFGroupImage", file[0])

        //aVFGroup.append("AVFGroupImage", file);

        $.ajax({
            method: 'POST',
            url: '/Admin/FilesManage/CreateGroup',
            data: aVFGroup,
            contentType: false,
            processData: false,
            success: function (data) {
                var model = data.model;
                if (!data.res) {
                    $("#div-loading").addClass('hidden');

                    swal({
                        title: "گروه دانلود",
                        text: "ابتدا یک گروه انتخاب نمائید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
                $("#viewAll").load("/Admin/FilesManage/ViewAll/" + model.lang);
                $("#myModal").modal("hide");
                swal({
                    title: "گروه دانلود",
                    text: "گروه دانلود  " + model.avfGroupTitle + " با موفقیت ثبت شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#div-loading").addClass('hidden');
                getAllAVFGroupForTreeView();
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//

function openModalForEditAVFGroup(id) {
    $.get("/Admin/FilesManage/EditGroup/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گروه دانلود ");
        $("#myModalBody").html(result);
    });

}
//
function UpdateAVFGroup() {
    var frm = $("#fileGroupFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActiveAVFG:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }
        var file = $("#ImageUpload").get(0).files;
        var aVFGroup = new FormData();
        if (avfGroupId != undefined) {
            avfGroupId = avfGroupId;

        }
        else {
            avfGroupId = $("#ParentId").val();
        }
        debugger;
        aVFGroup.append("AVFGroup_ID", $("#AVFGroup_ID").val()),
            aVFGroup.append("AVFGroupTitle", $("#AVFGroupTitle").val()),
            aVFGroup.append("IsActive", status),
            aVFGroup.append("ParentId", $('#ParentId').val()),
            aVFGroup.append("Lang", $("#language_ddl").val()),
            aVFGroup.append("AVFGroupImage", $('#oldImage').val()),
            aVFGroup.append("AVFGroupPic", file[0])

        //var aVFGroup = {
        //    AVFGroup_ID: $("#AVFGroup_ID").val(),
        //    ParentId: $("#ParentId").val(),
        //    AVFGroupTitle: $("#AVFGroupTitle").val(),
        //    IsActive: status,
        //    Lang: $("#language_ddl").val()
        //}

        $.ajax({
            method: 'POST',
            url: '/Admin/FilesManage/EditGroup',
            data: aVFGroup,
            contentType: false,
            processData: false,
            success: function (data) {
                debugger;
                $("#viewAll").load("/Admin/FilesManage/ViewAll/" + data);
                $("#myModal").modal("hide");

                $("#div-loading").addClass('hidden');
                getAllAVFGroupForTreeView();
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//

function DeleteAVFGroup(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این گروه مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"


    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/FilesManage/DeleteGroup',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "NOK") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "هشدار",
                                text: "این گروه شامل فایل می باشد ابتدا فایل ها باید حذف شوند",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                        }
                        else {
                            $("#div-loading").addClass('hidden');

                            $('#fileGroup_tb').find('tr[id=avfg_' + data.avfGroup_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#fileGroup_tb').DataTable();
                                table.row("#avfg_" + data.avfGroup_ID).remove().draw(false);
                            });

                            getAllAVFGroupForTreeView();
                            swal({
                                title: "تبریک",
                                text: " گروه " + data.avfGroupTitle + " با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
//
/////////////////////////////////// بخش فایل ها
function GetAllDownloadGroupByLang(lang) {
    $.ajax({ //گرفتن اطلاعات جیسون از سمت سرور و دی سریالایز کردن آن
        method: "GET",
        url: "/Admin/FilesManage/GetAllGroupsByLang",
        data: { id: lang },
        success: function (result) {

            var option = "";
            result.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $("#downloadGroup_ddl").html(option);
            //$("#SSGId").html(option);
            //
        },
        error: function (res) {

        }
    });
}
function GetAllFilesByGroupId(groupId) {

    $.ajax({
        method: 'POST',
        url: '/Admin/FilesManage/ViewAllFiles',
        data: { id: groupId },
        success: function (data) {

            $("#viewAllFiles").html(data);
        },
        error: function (result) {
        }
    });
}

function openModalForAddAVF() {
    //alert(avfGroupIdEdit);
    if (groupIdForAddFile != "") {
        $.get("/Admin/FilesManage/Create", function (result) {
            $("#myModal").modal();
            $("#myModalLabel").html("افزودن فایل جدید ");
            $("#myModalBody").html(result);
            $(".modal-dialog").css({ 'width': "60%" })
        });
    }
    else {
        swal({
            title: "هشدار",
            text: "برای ثبت فایل ابتدا گروه را انتخاب نمایید",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید"
        });
    }
}
function changeFormDesign(value) {
    if (value == "0") {
        $("#link").removeClass('hidden');
        $("#file").addClass('hidden');
        $("#Link_lbl").text("لینک");
    }
    else {
        $("#link").addClass('hidden');
        $("#file").removeClass('hidden');
        $("#FileNameOrLink").val('');
        $("#file_lbl").text("فایل");
    }
}
function InsertFile() {
    var frm = $("#fileFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');



        var aVF = new FormData();
        var files = $("#fileUpload").get(0).files;
        var image = $("#fileImageUpload").get(0).files;

        var isactive = $('#isActiveFile:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }
        ///
        var type = $("#FileOrLink").val();
        if (type == 1) {
            type = true;
        }
        else {
            type = false;
        }
        ///
        if ($("#file").hasClass("hidden")) {
            aVF.append("AVFSize", $(".AVFSize").val())
        }
        else {
            aVF.append("AVFSize", $(".AVFSize2").val())
        }
        aVF.append("AVFTitle", $("#AVFTitle").val()),
            aVF.append("AVFGroupId", groupIdForAddFile),
            aVF.append("FileOrLink", type),
            aVF.append("IsActive", status),
            aVF.append("FileNameOrLink", $("#FileNameOrLink").val()),
            aVF.append("file", files[0]),

            aVF.append("AVFDownloadCount", $("#AVFDownloadCount").val()),
            aVF.append("FileType", $("#FileType").val())
        if (image.length > 0) {
            aVF.append("vidoImg", image[0])
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/FilesManage/Create',
            data: aVF,
            contentType: false,
            processData: false,
            success: function (data) {

                $("#viewAllFiles").load("/Admin/FilesManage/ViewAllFiles/" + data.avfGroupId);
                $("#myModal").modal("hide");
                swal({
                    title: "فایل",
                    text: "فایل " + data.avfTitle + " با موفقیت ثبت شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
            },
            error: function (result) {
            }
        });

    }
    else {
        return false;
    }
}

function openModalForEditAVF(id) {
    $.get("/Admin/FilesManage/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش فایل  ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': "60%" })
    });

}

function UpdateFile() {
    //var frm = $("#fileFrmEdit").valid();
    //if (frm === true) {
    $("#div-loading").removeClass('hidden');

    var aVF = new FormData();
    var files = $("#fileUpload").get(0).files;
    var image = $("#fileImageUpload").get(0).files;

    var isactive = $('#isActiveFile:checked').length;
    var status = "";
    if (isactive === 1) {
        status = "true";
    }
    else {
        status = "false";
    }
    ///
    var type = $("#FileOrLink").val();
    if (type == 1) {
        type = true;
    }
    else {
        type = false;
    }
    ///
    if (type == true) {
        aVF.append("FileNameOrLink", $("#OldFileName").val())
    }
    ///
    if ($("#file").hasClass("hidden")) {
        aVF.append("AVFSize", $(".AVFSize").val())
    }
    else {
        aVF.append("AVFSize", $(".AVFSize2").val())
    }
    aVF.append("AVFTitle", $("#AVFTitle").val()),
        aVF.append("AVF_ID", $("#AVF_ID").val()),
        aVF.append("AVFGroupId", $("#AVFGroupId").val()),
        aVF.append("VideoImage", $("#OldVideoImage").val()),
        aVF.append("FileOrLink", type),
        aVF.append("IsActive", status),
        aVF.append("FileNameOrLink", $("#FileNameOrLink").val()),
        aVF.append("file", files[0]),
        aVF.append("videoImg", image[0]),

        aVF.append("AVFDownloadCount", $("#AVFDownloadCount").val()),
        aVF.append("CreateDate", $("#CreateDate").val()),
        aVF.append("FileType", $("#FileType").val())


    $.ajax({
        method: 'POST',
        url: '/Admin/FilesManage/Edit',
        data: aVF,
        contentType: false,
        processData: false,
        success: function (data) {

            $("#viewAllFiles").load("/Admin/FilesManage/ViewAllFiles/" + data.avfGroupId);
            $("#myModal").modal("hide");
            swal({
                title: "فایل",
                text: "فایل " + data.avfTitle + " با موفقیت ویرایش شد ",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
        },
        error: function (result) {
        }
    });
    //}
    //else {
    //    return false;
    //}
}

function DeleteAVF(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این فایل مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"


    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/FilesManage/DeleteAVF',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "nall") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "خطا",
                                text: "خطای سروری رخ داده است ",
                                type: "error",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "فایل",
                                text: "فایل " + data.avfTitle + "  با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                            debugger;
                            $('#file_tb').find('tr[id=avf_' + data.avF_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#file_tb').DataTable();
                                table.row("#avf_" + data.avF_ID).remove().draw(false);
                            });


                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
/////////////////////////////////// بخش ویژگی ها
var pgId, pgId2 = "";
function getAllProductsGroupForFeatureTreeView() { //خواندن گروه ها برای تری ویو ها
    var lang = $("#language_ddl").val();

    $.ajax({
        method: 'POST',
        url: '/Admin/Features/createTreeView',
        data: { pgId: 1, lang: lang },
        success: function (response) {

            var $checkableTree = $('#feature1_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    //نمایش لیست ویژگی ها بر اساس گروه
                    pgId = node.tags;
                    GetAllFeatureByProductGroupId(node.tags);
                }
            });

            var $checkableTree2 = $('#feature2_tw').treeview({
                data: response,
                showBorder: false,
                showIcon: false,
                showCheckbox: false,

                onNodeSelected: function (event, node) {
                    pgId2 = node.tags;
                    GetAllFeatureBygroupId(node.tags);
                }
            });
        },
        error: function (response) {

        }
    });
}
//

function GetAllFeatureByProductGroupId(groupId) {
    $.ajax({
        method: 'POST',
        url: '/Admin/Features/ViewAllFeatures',
        data: { id: groupId },
        success: function (data) {
            $("#viewAllFeatures").html(data);
        },
        error: function (result) {
        }
    });
}
///
function openModalForAddFeature() {
    $.get("/Admin/Features/CreateFeature", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن ویژگی جدید ");
        $("#myModalBody").html(result);
    });

}
///
function InsertFeature() {
    var frm = $("#featureFrm").valid();
    if (frm === true) {
        if (pgId != undefined) {
            $("#div-loading").removeClass('hidden');

            var isactive = $('#isActive:checked').length;
            var status = "";
            if (isactive === 1) {
                status = "true";
            }
            else {
                status = "false";
            }

            var feature = {
                FeatureTitle: $("#FeatureTitle").val(),
                IsActive: status,
                ProductGroupId: pgId
            }

            $.ajax({
                method: 'POST',
                url: '/Admin/Features/CreateFeature',
                data: feature,
                success: function (data) {

                    $("#viewAllFeatures").load("/Admin/Features/ViewAllFeatures/" + data.productGroupId);
                    $("#myModal").modal("hide");
                    swal({
                        title: "ویژگی",
                        text: "ویژگی با عنوان   " + data.featureTitle + " با موفقیت ثبت شد ",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                },
                error: function (result) {
                }
            });
        }
        else {
            swal({
                title: "ویژگی",
                text: "لطفا برای ثبت ویژگی ابتدا گروه محصول را انتخاب نمایید",
                type: "warning",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
        }
    }
    else {
        return false;
    }
}
///
function openModalForEditFeature(id) {
    $.get("/Admin/Features/EditFeature/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش ویژگی ها ");
        $("#myModalBody").html(result);
    });

}
///
function UpdateFeature() {
    var frm = $("#featureFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActive:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var feature = {
            Feature_ID: $("#Feature_ID").val(),
            FeatureTitle: $("#FeatureTitle").val(),
            IsActive: status,
            ProductGroupId: $("#ProductGroupId").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Features/EditFeature',
            data: feature,
            success: function (data) {

                $("#viewAllFeatures").load("/Admin/Features/ViewAllFeatures/" + data.productGroupId);
                $("#myModal").modal("hide");
                swal({
                    title: "ویژگی",
                    text: "ویژگی با عنوان   " + data.featureTitle + " با موفقیت ویرایش شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#div-loading").addClass('hidden');
            },
            error: function (result) {
            }
        });
    }
    else {
        return true;
    }
}
///
function DeleteFeature(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف واحد مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Features/DeleteFeature',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "NOK") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "ویژگی",
                                text: "این گروه شامل فایل می باشد ابتدا فایل ها باید حذف شوند",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "ویژگی",
                                text: "ویژگی  " + data.featureTitle + "  با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                            $("#div-loading").addClass('hidden');
                            $('#feature_tb').find('tr[id=feature_' + data.feature_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#feature_tb').DataTable();
                                table.row("#feature_" + data.feature_ID).remove().draw(false);
                            });
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}

////////////////////////////// بخش جواب ویژگی ها
function GetAllFeatureBygroupId(groupId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Features/GetAllFeatureByProductGroupId",
        data: { id: groupId },
        success: function (result) {

            var option = "";
            result.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $("#feature_ddl").html(option);
            //$("#SSGId").html(option);
            //
        },
        error: function (res) {

        }
    });
}
///
function GetAllFeatureReplyByFeatureId(featureId) {

    $.ajax({
        method: 'POST',
        url: '/Admin/Features/ViewAllFeatureReply',
        data: { id: featureId },
        success: function (data) {

            $("#viewAllFeatureReply").html(data);
        },
        error: function (result) {
        }
    });
}
///
function openModalForAddFeatureReply(id) {
    var id = pgId2;
    if (pgId2 == "") {
        swal({
            title: "جواب ویژگی",
            text: "ویژگی برای مشاهده لیست ویژگی ها ابتدا گروه محصول را انتخاب نمایید",
            type: "warning",
            confirmButtonColor: '#3085d6',
            confirmButtonText: "تائید"
        });
    }
    $.get("/Admin/Features/CreateFeatureReply/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن جواب ویژگی جدید ");
        $("#myModalBody").html(result);
    });

}
///
function InsertFeatureReply() {
    var frm = $("#frFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');



        var featureReply = {
            FeatureId: $("#FeatureId").val(),
            FeatureReplyText: $("#FeatureReplyText").val(),
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Features/CreateFeatureReply',
            data: featureReply,
            success: function (data) {

                $("#viewAllFeatureReply").load("/Admin/Features/ViewAllFeatureReply/" + data.featureId);
                $("#myModal").modal("hide");
                swal({
                    title: "جواب ویژگی",
                    text: "جواب ویژگی با عنوان    " + data.featureReplyText + " با موفقیت ثبت شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#div-loading").addClass('hidden');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
///

function openModalForEditFeatureReply(id) {

    $.get("/Admin/Features/EditFeatureReply/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش جواب ویژگی ");
        $("#myModalBody").html(result);
    });

}
///
function UpdateFeatureReply() {
    var frm = $("#frFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');



        var featureReply = {
            FeatureId: $("#Feature_Id").val(),
            FeatureReply_ID: $("#FeatureReply_ID").val(),
            FeatureReplyText: $("#FeatureReplyText").val(),
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Features/EditFeatureReply',
            data: featureReply,
            success: function (data) {

                $("#viewAllFeatureReply").load("/Admin/Features/ViewAllFeatureReply/" + data.featureId);
                $("#myModal").modal("hide");
                swal({
                    title: "جواب ویژگی",
                    text: "جواب ویژگی با عنوان    " + data.featureReplyText + " با موفقیت ویرایش شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#div-loading").addClass('hidden');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
///
function checkboxSelectedForDeleteFeatureReply() {
    swal({
        title: "هشدار",
        text: "آیا برای حذف جواب ویژگی ها مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {
                var checkboxValues = "";
                $("div.checked").children('.forRemove').each(function () {
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");

                if (checkboxValues != "") {
                    $.ajax({
                        url: "/Admin/Features/checkboxSelected",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        for (var i = 0; i < cv.length - 1; i++) {

                            $('#featureReply_tb').find('tr[id=fReply_' + cv[i] + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#featureReply_tb').DataTable();
                                table.row("#fReply_" + cv[i]).remove().draw(false);
                            });

                        }
                        swal({
                            title: "جواب ویژگی",
                            text: "جواب ویژگی با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                    });
                }
                else {
                    swal({
                        title: "جواب ویژگی",
                        text: "لطفا یک مورد را انتخاب نمایید",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            }
        });
}
///
function SetIsDefualtReply(featureReplyId, featureId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Features/SetIsDefault",
        data: { id: featureReplyId, featureId: featureId },
        success: function (result) {

        },
        error: function (res) {

        }
    });
}

/////////////////////////////////////////// گروه راههای ارتباطی
function openModalForAddConnectionGroup() {
    $.get("/Admin/Connection/CreateGroup", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گروه راه های ارتباطی جدید ");
        $("#myModalBody").html(result);
    });

}

/////
function InsertConnectionGroup() {
    var frm = $("#cnGroupFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');
        var status = $("#isActive_cg:checked").length;
        if (status == 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var connectionGroup = {
            ConnectionGroupTitle: $("#ConnectionGroupTitle").val(),
            IsActive: status,
            Lang: $("#language_ddl").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Connection/CreateGroup',
            data: connectionGroup,
            success: function (data) {

                $("#viewAll").load("/Admin/Connection/ViewAll/" + data.lang);
                $("#myModal").modal("hide");
                swal({
                    title: "گروه ارتباطی",
                    text: "گروه راه های ارتباطی    " + data.connectionGroupTitle + " با موفقیت ثبت شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#div-loading").addClass('hidden');
                GetAllConnectionGroups(data.lang, "#connectionsGroup_ddl");
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
/////
function openModalForEditConnectionGroup(id) {
    $.get("/Admin/Connection/editGroup/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گروه راه های ارتباطی");
        $("#myModalBody").html(result);
    });

}
/////
function UpdateConnectionGroup() {
    var frm = $("#cnGroupFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');
        var status = $("#isActive_cg:checked").length;
        if (status == 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var connectionGroup = {
            ConnectionGroup_ID: $("#ConnectionGroup_ID").val(),
            ConnectionGroupTitle: $("#ConnectionGroupTitle").val(),
            IsActive: status,
            Lang: $("#language_ddl").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Connection/editGroup',
            data: connectionGroup,
            success: function (data) {

                $("#viewAll").load("/Admin/Connection/ViewAll/" + data.lang);
                $("#myModal").modal("hide");
                swal({
                    title: "گروه ارتباطی",
                    text: "گروه راه های ارتباطی    " + data.connectionGroupTitle + " با موفقیت ویرایش شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#div-loading").addClass('hidden');
                GetAllConnectionGroups(data.lang, "#connectionsGroup_ddl");
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

/////
function DeleteConnectionGroup(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این گروه مطمئن هستید؟",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ["خیر", "بله"],
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Connection/DeleteGroup',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "nall") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "گروه ارتباطی",
                                text: "خطایی سروری رخ داده است!",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                        }
                        else if (data == "repeate") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "گروه ارتباطی",
                                text: "این گروه شامل راههای ارتباطی می باشد ",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "گروه ارتباطی",
                                text: "گروه راههای ارتباطی  " + data.connectionGroupTitle + "  با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                            $("#cg_" + data.connectionGroup_ID).hide("slow");
                            GetAllConnectionGroups(data.lang, "#connectionsGroup_ddl");
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
/////////////////////////////////بخش راههای ارتباطی
function GetAllConnectionGroups(lang, selector) {
    $.ajax({
        method: 'POST',
        url: '/Admin/Connection/ViewAllCnGroupList',
        data: { id: lang },
        success: function (data) {

            var option = "";
            data.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $(selector).html(option);
        },
        error: function (result) {
        }
    });
}

///
function GetAllConnectionByGroupId(groupId) {
    $.ajax({
        method: 'POST',
        url: '/Admin/Connection/ViewAllConnection',
        data: { id: groupId },
        success: function (data) {
            console.log(groupId);
            $("#viewAllConnection").html(data);
        },
        error: function (result) {
        }
    });
}
///

///
function openModalForAddConnection() {
    $.get("/Admin/Connection/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن راه های ارتباطی جدید ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': "60%" })
    });

}
///
function InsertConnection() {
    var frm = $("#connectionFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');
        var groupId = $("#connectionsGroup_ddl").val();
        debugger;
        if (groupId != null) {

            var type = $("#type_ddl").val();
            if (type == 1) {
                type = true;
            }
            else {
                type = false;
            }
            var connection = {
                ConnectionGroupId: groupId,
                ConnectionName: $("#ConnectionName").val(),
                ConnectionValue: $("#ConnectionValue").val(),
                CImage: $("#image_ddl").val(),
                CType: type
            }

            $.ajax({
                method: 'POST',
                url: '/Admin/Connection/Create',
                data: connection,
                success: function (data) {

                    $("#viewAllConnection").load("/Admin/Connection/ViewAllConnection/" + data.connectionGroupId);
                    $("#myModal").modal("hide");
                    swal({
                        title: "راه ارتباطی",
                        text: "راه ارتباطی    " + data.connectionName + " با موفقیت ثبت شد ",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');

                },
                error: function (result) {
                }
            });
        }
        else {
            swal({
                title: "راه ارتباطی",
                text: "برای ثبت راه ارتباطی یک گروه را انتخاب نمایید !",
                type: "warning",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
            $("#div-loading").addClass('hidden');

        }
    }
    else {
        return false;
    }
}
///
function openModalForEditConnection(id) {
    $.get("/Admin/Connection/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش راه های ارتباطی ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': "60%" })
    });

}
///
function UpdateConnection() {
    var frm = $("#connectionFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var type = $("#type_ddl").val();
        if (type == 1) {
            type = true;
        }
        else {
            type = false;
        }
        var connection = {
            Connection_ID: $("#Connection_ID").val(),
            ConnectionGroupId: $("#ConnectionGroupId").val(),
            ConnectionName: $("#ConnectionName").val(),
            ConnectionValue: $("#ConnectionValue").val(),
            CImage: $("#image_ddl").val(),
            CType: type
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Connection/Edit',
            data: connection,
            success: function (data) {

                $("#viewAllConnection").load("/Admin/Connection/ViewAllConnection/" + data.connectionGroupId);
                $("#myModal").modal("hide");
                swal({
                    title: "راه ارتباطی",
                    text: "راه ارتباطی    " + data.connectionName + " با موفقیت ویرایش شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#div-loading").addClass('hidden');
            },
            error: function (result) {
            }
        });
    }
    else {
        false;
    }
}
////

function DeleteConnection(Id) {

    swal({
        type: "warning",
        title: "هشدار",
        text: "آیا برای حذف این راه ارتباطی مطمئن هستید؟",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Connection/DeleteConnection',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        //if (data == "NOK") {
                        //    $("#div-loading").addClass('hidden');
                        //    swal("هشدار", "برای این کشور استان ثبت شده است ابتدا باید استان ها حذف شوند", "warning");
                        //}
                        //else {
                        $("#div-loading").addClass('hidden');
                        $('#connetion_tb').find('tr[id=connection_' + data.connection_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#connetion_tb').DataTable();
                            table.row("#connection_" + data.connection_ID).remove().draw(false);
                        });
                        //}
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
////////////////////////////// بخش کشور
function openModalForAddCountry() {
    $.get("/Admin/Location/CreateCountry", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن کشور جدید ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': "50%" })
    });

}

function InsertCountry() {
    var frm = $("#countryFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');
        var country = new FormData();
        var status = $("#isActiveCountry:checked").length;
        if (status == 1) {
            status = true;
        }
        else {
            status = false;
        }

        country.append("CountryName", $("#CountryName").val()),
            country.append("CountryCode", $("#CountryCode").val()),
            country.append("IsActive", status),
            country.append("Lang", $("#language_ddl").val())
        debugger;
        $.ajax({
            method: 'POST',
            url: '/Admin/Location/CreateCountry',
            data: country,
            contentType: false,
            processData: false,
            success: function (data) {
                debugger;
                if (data == "repeate") {
                    swal({
                        title: "کشور",
                        text: "این کشور قبلا ثبت شده است ",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                }
                else {
                    $("#viewAll").load("/Admin/Location/ViewAll/" + data.lang);
                    $("#myModal").modal("hide");
                    swal({
                        title: "کشور",
                        text: "کشور    " + data.countryName + " با موفقیت ثبت شد ",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                    GetAllDataInDropDownList(data.lang, "Location", "GetAllCountry", "#country_ddl");
                }
            },
            error: function (result) {
                alert("sd");
            }
        });
    }
    else {
        return false;
    }
}

function openModalForEditCountry(id) {
    $.get("/Admin/Location/EditCountry/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش کشور  ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': "50%" })
    });

}

function UpdateCountry() {
    var frm = $("#countryFrmEdit").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');
        var country = new FormData();
        var status = $("#isActiveCountry:checked").length;
        if (status == 1) {
            status = true;
        }
        else {
            status = false;
        }

        country.append("Country_ID", $("#Country_ID").val()),
            country.append("CountryName", $("#CountryName").val()),
            country.append("CountryCode", $("#CountryCode").val()),
            country.append("IsActive", status),
            country.append("Lang", $("#language_ddl").val())

        $.ajax({
            method: 'POST',
            url: '/Admin/Location/EditCountry',
            data: country,
            contentType: false,
            processData: false,
            success: function (data) {

                $("#viewAll").load("/Admin/Location/ViewAll/" + data.lang);
                $("#myModal").modal("hide");
                swal({
                    title: "کشور",
                    text: "کشور    " + data.countryName + " با موفقیت ویرایش شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#div-loading").addClass('hidden');
                GetAllDataInDropDownList(data.lang, "Location", "GetAllCountry", "#country_ddl");
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function DeleteCountry(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این کشور مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Location/DeleteCountry',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "NOK") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "کشور",
                                text: "برای این کشور استان ثبت شده است ابتدا باید استان ها حذف شوند",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "کشور",
                                text: "کشور  " + data.countryName + "  با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                            $("#country_" + data.country_ID).hide("slow");
                            GetAllDataInDropDownList(data.lang, "Location", "GetAllCountry", "#country_ddl");
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}

////////////////////////////////////بخش استان
function GetAllStateByCountryId(countryId) {
    $.ajax({
        method: 'POST',
        url: '/Admin/Location/ViewAllState',
        data: { id: countryId },
        success: function (data) {
            $("#viewAllState").html(data);
            GetAllDataInDropDownList($("#country_ddl").val(), "Location", "GetAllState", "#state_ddl");
        },
        error: function (result) {
        }
    });
}

function openModalForAddState() {
    $.get("/Admin/Location/CreateState", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن استان جدید ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': "50%" })
    });

}

function InsertState() {
    var frm = $("#stateFrm").valid();
    if (frm === true) {

        if ($("#country_ddl").val() != "0") {

            $("#div-loading").removeClass('hidden');
            var state = new FormData();
            var status = $("#isActiveState:checked").length;
            if (status == 1) {
                status = true;
            }
            else {
                status = false;
            }

            state.append("StateName", $("#StateName").val()),
                state.append("StateCode", $("#StateCode").val()),
                state.append("IsActive", status),
                state.append("CountryId", $("#country_ddl").val())

            $.ajax({
                method: 'POST',
                url: '/Admin/Location/CreateState',
                data: state,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data == "repeate") {
                        swal({
                            title: "استان",
                            text: "این استان قبلا ثبت شده است",
                            type: "warning",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                        $("#div-loading").addClass('hidden');
                    }
                    else {
                        $("#viewAllState").load("/Admin/Location/ViewAllState/" + data.countryId);
                        $("#myModal").modal("hide");
                        swal({
                            title: "استان",
                            text: "استان    " + data.stateName + " با موفقیت ثبت شد ",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                        $("#div-loading").addClass('hidden');
                        GetAllStateByCountryId(data.countryId);
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            swal({
                title: "استان",
                text: "برای ثبت استان ابتدا کشور را انتخاب نمایید ",
                type: "warning",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
        }
    }
    else {
        return false;
    }
}

function openModalForEditState(satetId) {
    $.ajax({
        method: 'GET',
        url: '/Admin/Location/EditState',
        data: { id: satetId, lang: $("#language_ddl").val() },
        success: function (result) {
            $("#myModal").modal();
            $("#myModalLabel").html("ویرایش استان  ");
            $("#myModalBody").html(result);
            $(".modal-dialog").css({ 'width': "50%" });
        },
        error: function (result) {
        }
    });
}

function DeleteState(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این استان مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Location/DeleteState',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        if (data == "NOK") {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "استان",
                                text: "برای این استان شهر ثبت شده است ابتدا باید شهر ها حذف شوند",
                                type: "warning",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                        }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                title: "استان",
                                text: "استان  " + data.stateName + "  با موفقیت حذف شد",
                                type: "success",
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "تائید"
                            });
                            $('#state_tb').find('tr[id=state_' + data.state_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                                var table = $('#state_tb').DataTable();
                                table.row("#state_" + data.state_ID).remove().draw(false);
                            });


                            GetAllStateByCountryId(data.countryId);
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}

function UpdateState() {
    var frm = $("#stateFrmEdit").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');
        var state = new FormData();
        var status = $("#isActiveState:checked").length;
        if (status == 1) {
            status = true;
        }
        else {
            status = false;
        }

        state.append("State_ID", $("#State_ID").val()),
            state.append("StateName", $("#StateName").val()),
            state.append("StateCode", $("#StateCode").val()),
            state.append("IsActive", status),
            state.append("CountryId", $("#CountryId").val())

        $.ajax({
            method: 'POST',
            url: '/Admin/Location/EditState',
            data: state,
            contentType: false,
            processData: false,
            success: function (data) {

                $("#viewAllState").load("/Admin/Location/ViewAllState/" + data.countryId);
                $("#myModal").modal("hide");
                swal({
                    title: "استان",
                    text: "استان    " + data.stateName + " با موفقیت ویرایش شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#div-loading").addClass('hidden');
                GetAllStateByCountryId(data.countryId);
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

//////////////////////////////////////////////// بخش شهرستان
function GetAllCityByStateId(stateId) {
    $.ajax({
        method: 'POST',
        url: '/Admin/Location/ViewAllCity',
        data: { id: stateId },
        success: function (data) {
            $("#viewAllCity").html(data);
        },
        error: function (result) {
        }
    });
}

function openModalForAddCity() {
    $.get("/Admin/Location/CreateCity", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن شهر جدید ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': "50%" })
    });

}

function InsertCity() {

    var frm = $("#cityFrm").valid();
    if (frm === true) {
        var state = $("#state_ddl").val();
        debugger;
        if ($("#state_ddl").val() != "0") {

            $("#div-loading").removeClass('hidden');
            var city = new FormData();
            var status = $("#isActiveCity:checked").length;
            if (status == 1) {
                status = true;
            }
            else {
                status = false;
            }

            city.append("CityName", $("#CityName").val()),
                city.append("CityCode", $("#CityCode").val()),
                city.append("IsActive", status),
                city.append("State_ID", $("#state_ddl").val())

            $.ajax({
                method: 'POST',
                url: '/Admin/Location/CreateCity',
                data: city,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data == "repeate") {
                        swal({
                            title: "شهر",
                            text: "این شهر قبلا ثبت شده است ",
                            type: "warning",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                        $("#div-loading").addClass('hidden');
                    }
                    else {
                        $("#myModal").modal("hide");
                        swal({
                            title: "شهر",
                            text: "شهر    " + data.cityName + " با موفقیت ثبت شد ",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                        $("#div-loading").addClass('hidden');
                        GetAllCityByStateId(data.state_ID);
                    }
                },
                error: function (result) {
                }
            });

        }
        else {
            swal({
                title: "شهر",
                text: "برای ثبت شهر ابتدا استان را انتخاب نمایید ",
                type: "warning",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
        }
    }
    else {
        return false;
    }
}

function openModalForEditCity(id) {
    $.get("/Admin/Location/EditCity/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش شهر ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': "50%" })
    });

}

function UpdateCity() {
    var frm = $("#cityFrmEdit").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');
        var city = new FormData();
        var status = $("#isActiveCity:checked").length;
        if (status == 1) {
            status = true;
        }
        else {
            status = false;
        }

        city.append("City_ID", $("#City_ID").val()),
            city.append("CityName", $("#CityName").val()),
            city.append("CityCode", $("#CityCode").val()),
            city.append("IsActive", status),
            city.append("State_ID", $("#State_ID").val())

        $.ajax({
            method: 'POST',
            url: '/Admin/Location/EditCity',
            data: city,
            contentType: false,
            processData: false,
            success: function (data) {

                $("#myModal").modal("hide");
                swal({
                    title: "شهر",
                    text: "شهر    " + data.cityName + " با موفقیت ویرایش شد ",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#div-loading").addClass('hidden');
                GetAllCityByStateId(data.state_ID);
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function DeleteCity(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این شهر مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Location/DeleteCity',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }


                        $("#div-loading").addClass('hidden');
                        swal({
                            title: "شهر",
                            text: "شهر  " + data.cityName + "  با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });

                        $('#city_tb').find('tr[id=city_' + data.cityId + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#city_tb').DataTable();
                            table.row("#city_" + data.cityId).remove().draw(false);
                        });
                        debugger;
                        GetAllCityByStateId(data.state_ID);
                    },
                    error: function (result) {
                    }
                });
            }
        });
}

function GetAllCustomers() {

    $.ajax({
        method: 'POST',
        url: '/Admin/Customer/ViewAll',
        data: '',
        success: function (data) {
            $("#viewAll").html(data);
        },
        error: function (result) {
        }
    });
}
/////////////////////////////////////////////////// درباره ما
function GetAboutUs(groupId) {
    $.ajax({
        method: "GET",
        url: "/Admin/AboutUs/ViewAllAboutUs",
        data: { id: groupId },
        success: function (result) {

            $("#viewAll").html(result);
            //
        },
        error: function (res) {

        }
    });
}
/////
function openModalForAddAboutUs() {
    $.get("/Admin/AboutUs/Create", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن درباره ما");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
        //$("li[data-title=" + id + "]").css({ 'color': 'white' });
    });

}
//
function InsertAboutUs() {
    debugger;
    if (CKEDITOR.instances['ContentText'].getData() == "") {
        $("#contentText_val").text("متن اصلی را وارد کنید")
        return false;
    }
    $("#div-loading").removeClass('hidden');
    var contents = new FormData();
    var status = "true";

    var files = $("#ContentUploader").get(0).files;

    contents.append("Content_ID", $("#Content_ID").val()),
        contents.append("ContentTitle", "درباره ما "),
        contents.append("ContentText", CKEDITOR.instances['ContentText'].getData()),
        contents.append("ContentSummary", $("#ContentSummary").val()),
        contents.append("IsActive", status),
        //contents.append("img", files[0]),
        //contents.append("img", files[0]),
        contents.append("ContentGroupId", $("#ContentGroupId").val())

    $.ajax({
        method: 'POST',
        url: '/Admin/AboutUs/Create',
        data: contents,
        processData: false,
        contentType: false,
        success: function (data) {


            $("#div-loading").addClass('hidden');
            $("#viewAll").load("/Admin/AboutUs/ViewAllAboutUs/" + data.contentGroupId);

            $("#myModal").modal('hide');

            swal({
                title: "درباره ما",
                text: "درباره ما با موفقیت ثبت شد",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
        },
        error: function (result) {
        }
    });


}
//
function openModalForEditAboutUs(id) {
    $.get("/Admin/AboutUs/Edit/" + id, function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش درباره ما");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
        //$("li[data-title=" + id + "]").css({ 'color': 'white' });
    });

}
/////
function UpdateAboutUs() {
    $("#div-loading").removeClass('hidden');
    var contents = new FormData();
    var status = "true";

    var files = $("#ContentUploader").get(0).files;

    contents.append("Content_ID", $("#Content_ID").val()),
        contents.append("ContentTitle", $('#ContentTitle').val()),
        contents.append("ContentText", CKEDITOR.instances['ContentTextEdit'].getData()),
        contents.append("ContentSummary", $("#ContentSummary").val()),
        contents.append("ContentImage", $("#oldImage").val()),
        contents.append("IsActive", status),
        //contents.append("img", files[0]),
        contents.append("ContentGroupId", $("#ContentGroupId").val())

    $.ajax({
        method: 'POST',
        url: '/Admin/AboutUs/Edit',
        data: contents,
        processData: false,
        contentType: false,
        success: function (data) {


            $("#div-loading").addClass('hidden');
            $("#viewAll").load("/Admin/AboutUs/ViewAllAboutUs/4");

            $("#myModal").modal('hide');

            swal({
                title: "درباره ما",
                text: $('#ContentTitle').val()+" با موفقیت ویرایش شد",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
        },
        error: function (result) {
        }
    });


}
////////////////////////////////////////// بخش حامیان
function openModalForAddSupporter() {
    $.get("/Admin/CoWorker/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گواهینامه یا افتخار");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '55%' });
    });
}
//////
function InsertSupporter() {
    var frm = $("#cwFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var coWorker = new FormData();
        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }
        ///
        var type = "";
        if ($("#Type").val() == "0") {
            type = "false";
        }
        else {
            type = "true";
        }

        var files = $("#photoUploader").get(0).files;
        if (files.length > 0) {
            coWorker.append("Title", $("#Title").val()),
                coWorker.append("Link", $("#Link").val()),
                coWorker.append("photo", files[0]),
                coWorker.append("Type", type),
                coWorker.append("IsActive", status)
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/CoWorker/Create',
            data: coWorker,
            contentType: false,//برای ارسال عکس باید این مولفه با فالس مقداردهی شود
            processData: false, //برای استفاده از فرم دیتا باید این مولفه اضافه شود
            success: function (data) {

                if (data[0] == "nall") {
                    swal({
                        title: "گواهینامه / افتخار ",
                        text: "خطایی رخ داده است",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                }
                else {
                    $("#div-loading").addClass('hidden');
                    $("#viewAll").load("/Admin/CoWorker/ViewAll");

                    $("#myModal").modal('hide');
                    swal({
                        title: "گواهینامه / افتخار ",
                        text: "گواهینامه / افتخار " + data.title + " با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

//////
function openModalForEditSupporter(id) {
    $.get("/Admin/CoWorker/Edit/" + id, function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گواهینامه یا افتخار");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '55%' });
    });

}
////////
function UpdateSupporter() {



    var coWorker = new FormData();
    var isactive = $('#IsActive:checked').length;
    var status = "";
    if (isactive === 1) {
        status = "true";
    }
    else {
        status = "false";
    }
    ///
    var type = "";
    if ($("#Type").val() == "0") {
        type = "false";
    }
    else {
        type = "true";
    }

    var files = $("#photoUploader").get(0).files;
    ///if (files.length > 0) {
    coWorker.append("CoWorker_ID", $("#CoWorker_ID").val()),
        coWorker.append("Title", $("#Title").val()),
        coWorker.append("Link", $("#Link").val()),
        coWorker.append("Photo", $("#oldPhoto").val()),
        coWorker.append("photo", files[0]),
        coWorker.append("Type", type),
        coWorker.append("IsActive", status)

    // }

    $.ajax({
        method: 'POST',
        url: '/Admin/CoWorker/Edit',
        data: coWorker,
        processData: false,
        contentType: false,
        success: function (data) {

            if (data == "nall") {
                swal({
                    title: "گواهینامه / افتخار ",
                    text: "خطایی رخ داده است",
                    type: "warning",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#div-loading").addClass('hidden');
            }
            else {
                $("#div-loading").addClass('hidden');
                $("#viewAll").load("/Admin/CoWorker/ViewAll");

                $("#myModal").modal('hide');
                swal({
                    title: "گواهینامه / افتخار ",
                    text: "گواهینامه / افتخار " + data.title + " با موفقیت ویرایش شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
            }
        },
        error: function (result) {
        }
    });

}


///////
function DeleteSupporter(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این گواهینامه / افتخار مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/CoWorker/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }


                        $("#div-loading").addClass('hidden');
                        swal({
                            title: "گواهینامه / افتخار ",
                            text: "گواهینامه / افتخار  " + data.title + "  با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });

                        $('#coWorker_tb').find('tr[id=cw_' + data.coWorker_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#coWorker_tb').DataTable();
                            table.row("#cw_" + data.coWorker_ID).remove().draw(false);
                        });

                    },
                    error: function (result) {
                    }
                });
            }
        });
}
///////////////////////////////////////////////////بخش ساخت تجهیزات 
function GetMaterial(groupId) {
    $.ajax({
        method: "GET",
        url: "/Admin/Material/ViewAllAboutUs",
        data: { id: groupId },
        success: function (result) {

            $("#viewAll").html(result);
            //
        },
        error: function (res) {

        }
    });
}
///
function openModalForAddMaterial() {
    $.get("/Admin/Material/Create", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن بخش ساخت تجهیزات");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
        //$("li[data-title=" + id + "]").css({ 'color': 'white' });
    });

}
//
function InsertMaterial() {
    $("#div-loading").removeClass('hidden');
    var contents = new FormData();
    var status = "true";

    var files = $("#ContentUploader").get(0).files;

    contents.append("Content_ID", $("#Content_ID").val()),
        contents.append("ContentTitle", "ساخت تجهیزات "),
        contents.append("ContentText", CKEDITOR.instances['ContentText'].getData()),
        contents.append("ContentSummary", $("#ContentSummary").val()),
        contents.append("IsActive", status),
        contents.append("img", files[0]),
        contents.append("ContentGroupId", $("#ContentGroupId").val())

    $.ajax({
        method: 'POST',
        url: '/Admin/Material/Create',
        data: contents,
        processData: false,
        contentType: false,
        success: function (data) {


            $("#div-loading").addClass('hidden');
            $("#viewAll").load("/Admin/AboutUs/ViewAllAboutUs/3");

            $("#myModal").modal('hide');

            swal("تبریک", "بخش « ساخت تجهیزات » با موفقیت ثبت شد", "success");
        },
        error: function (result) {
        }
    });


}
//
function openModalForEditMaterial() {
    $.get("/Admin/Material/Edit/-2", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش بخش ساخت تجهیزات");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
        //$("li[data-title=" + id + "]").css({ 'color': 'white' });
    });

}
/////
function UpdateMaterial() {
    $("#div-loading").removeClass('hidden');
    var contents = new FormData();
    var status = "true";

    var files = $("#ContentUploader").get(0).files;

    contents.append("Content_ID", $("#Content_ID").val()),
        contents.append("ContentTitle", "ساخت تجهیزات  "),
        contents.append("ContentText", CKEDITOR.instances['ContentText'].getData()),
        contents.append("ContentSummary", $("#ContentSummary").val()),
        contents.append("ContentImage", $("#oldImage").val()),
        contents.append("IsActive", status),
        contents.append("img", files[0]),
        contents.append("ContentGroupId", $("#ContentGroupId").val())

    $.ajax({
        method: 'POST',
        url: '/Admin/Material/Edit',
        data: contents,
        processData: false,
        contentType: false,
        success: function (data) {


            $("#div-loading").addClass('hidden');
            $("#viewAll").load("/Admin/Material/ViewAllAboutUs/3");

            $("#myModal").modal('hide');

            swal("تبریک", "بخش « ساخت تجهیزات » با موفقیت ویرایش شد", "success");
        },
        error: function (result) {
        }
    });


}

///////////////////////////////////////////////////بخش واردات تجهیزات 
function GetImportaion(groupId) {
    $.ajax({
        method: "GET",
        url: "/Admin/ImportaionPart/ViewAllAboutUs",
        data: { id: groupId },
        success: function (result) {
            $("#viewAll").html(result);
            //
        },
        error: function (res) {

        }
    });
}
///
function openModalForAddImportaion() {
    $.get("/Admin/ImportaionPart/Create", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن بخش واردات و تامین تجهیزات");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
        //$("li[data-title=" + id + "]").css({ 'color': 'white' });
    });

}
//
function InsertImportaion() {
    $("#div-loading").removeClass('hidden');
    var contents = new FormData();
    var status = "true";

    var files = $("#ContentUploader").get(0).files;

    contents.append("Content_ID", $("#Content_ID").val()),
        contents.append("ContentTitle", "واردات و تامین تجهیزات"),
        contents.append("ContentText", CKEDITOR.instances['ContentText'].getData()),
        contents.append("ContentSummary", $("#ContentSummary").val()),
        contents.append("IsActive", status),
        contents.append("img", files[0]),
        contents.append("ContentGroupId", $("#ContentGroupId").val())

    $.ajax({
        method: 'POST',
        url: '/Admin/ImportaionPart/Create',
        data: contents,
        processData: false,
        contentType: false,
        success: function (data) {


            $("#div-loading").addClass('hidden');
            $("#viewAll").load("/Admin/ImportaionPart/ViewAllAboutUs/4");

            $("#myModal").modal('hide');

            swal("تبریک", "بخش « واردات و تامین تجهیزات » با موفقیت ثبت شد", "success");
        },
        error: function (result) {
        }
    });


}
//
function openModalForEditImportaionPart() {
    $.get("/Admin/ImportaionPart/Edit/-3", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش بخش واردات و تامین تجهیزات");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
        //$("li[data-title=" + id + "]").css({ 'color': 'white' });
    });

}
/////
function UpdateImportaionPart() {
    $("#div-loading").removeClass('hidden');
    var contents = new FormData();
    var status = "true";

    var files = $("#ContentUploader").get(0).files;

    contents.append("Content_ID", $("#Content_ID").val()),
        contents.append("ContentTitle", "واردات و تامین تجهیزات  "),
        contents.append("ContentText", CKEDITOR.instances['ContentText'].getData()),
        contents.append("ContentSummary", $("#ContentSummary").val()),
        contents.append("ContentImage", $("#oldImage").val()),
        contents.append("IsActive", status),
        contents.append("img", files[0]),
        contents.append("ContentGroupId", $("#ContentGroupId").val())

    $.ajax({
        method: 'POST',
        url: '/Admin/ImportaionPart/Edit',
        data: contents,
        processData: false,
        contentType: false,
        success: function (data) {


            $("#div-loading").addClass('hidden');
            $("#viewAll").load("/Admin/ImportaionPart/ViewAllAboutUs/4");

            $("#myModal").modal('hide');

            swal("تبریک", "بخش « واردات و تامین تجهیزات » با موفقیت ویرایش شد", "success");
        },
        error: function (result) {
        }
    });


}
/////////////////////////////////////////////////////// بخش طراحی و مهندسی
function GetEngineering(groupId) {
    $.ajax({
        method: "GET",
        url: "/Admin/EnginerDesign/ViewAllAboutUs",
        data: { id: groupId },
        success: function (result) {
            $("#viewAll").html(result);
            //
        },
        error: function (res) {

        }
    });
}
///
function openModalForAddEnginnering() {
    $.get("/Admin/EnginerDesign/Create", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن بخش طراحی و مهندسی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
        //$("li[data-title=" + id + "]").css({ 'color': 'white' });
    });

}
//
function InsertEnginnering() {
    $("#div-loading").removeClass('hidden');
    var contents = new FormData();
    var status = "true";

    var files = $("#ContentUploader").get(0).files;

    contents.append("Content_ID", $("#Content_ID").val()),
        contents.append("ContentTitle", "طراحی و مهندسی"),
        contents.append("ContentText", CKEDITOR.instances['ContentText'].getData()),
        contents.append("ContentSummary", $("#ContentSummary").val()),
        contents.append("IsActive", status),
        contents.append("img", files[0]),
        contents.append("ContentGroupId", $("#ContentGroupId").val())

    $.ajax({
        method: 'POST',
        url: '/Admin/EnginerDesign/Create',
        data: contents,
        processData: false,
        contentType: false,
        success: function (data) {


            $("#div-loading").addClass('hidden');
            $("#viewAll").load("/Admin/EnginerDesign/ViewAllAboutUs/5");

            $("#myModal").modal('hide');

            swal("تبریک", "بخش « طراحی و مهندسی » با موفقیت ثبت شد", "success");
        },
        error: function (result) {
        }
    });


}
//
function openModalForEditEnginnering() {
    $.get("/Admin/EnginerDesign/Edit/-4", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش بخش طراحی و مهندسی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '75%' });
        //$("li[data-title=" + id + "]").css({ 'color': 'white' });
    });

}
/////
function UpdateEnginnering() {
    $("#div-loading").removeClass('hidden');
    var contents = new FormData();
    var status = "true";

    var files = $("#ContentUploader").get(0).files;

    contents.append("Content_ID", $("#Content_ID").val()),
        contents.append("ContentTitle", "طراحی و مهندسی  "),
        contents.append("ContentText", CKEDITOR.instances['ContentText'].getData()),
        contents.append("ContentSummary", $("#ContentSummary").val()),
        contents.append("ContentImage", $("#oldImage").val()),
        contents.append("IsActive", status),
        contents.append("img", files[0]),
        contents.append("ContentGroupId", $("#ContentGroupId").val())

    $.ajax({
        method: 'POST',
        url: '/Admin/EnginerDesign/Edit',
        data: contents,
        processData: false,
        contentType: false,
        success: function (data) {


            $("#div-loading").addClass('hidden');
            $("#viewAll").load("/Admin/EnginerDesign/ViewAllAboutUs/5");

            $("#myModal").modal('hide');

            swal("تبریک", "بخش « طراحی و مهندسی » با موفقیت ویرایش شد", "success");
        },
        error: function (result) {
        }
    });


}
//////////////////////////////////////////// بخش گروه خبرنامه
function openModalForAddNewsLetterGroup() {
    $.get("/Admin/NewsLetters/CreateGroup", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن گروه خبرنامه");
        $("#myModalBody").html(result);
    });

}
function InsertNewsLetterGroup() {
    var frm = $("#nlGroupFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActive:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var newsLetterGroup = {
            Title: $("#Title").val(),
            IsActive: status,
            Lang: $("#language_ddl").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/NewsLetters/CreateGroup',
            data: newsLetterGroup,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                $("#viewAll").load("/Admin/NewsLetters/ViewAll/" + data.lang + "");
                $("#myModal").modal('hide');
                swal({
                    title: "گروه خبرنامه",
                    text: "گروه خبرنامه " + data.title + " با موفقیت ثبت شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });


                GetAllNewsLetterGroup($("#language_ddl").val(), "#newsLetterGroup_ddl");
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function openModalForEditNewsLetterGroup(id) {
    $.get("/Admin/NewsLetters/EditGroup/" + id, function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش گروه خبرنامه");
        $("#myModalBody").html(result);
    });

}
function UpdateNewsLetterGroup() {
    var frm = $("#nlGroupFrmEdit").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');

        var isactive = $('#isActive:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }

        var newsLetterGroup = {
            Group_ID: $("#Group_ID").val(),
            Title: $("#Title").val(),
            IsActive: status,
            Lang: $("#language_ddl").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/NewsLetters/EditGroup',
            data: newsLetterGroup,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                $("#viewAll").load("/Admin/NewsLetters/ViewAll/" + data.lang + "");
                $("#myModal").modal('hide');
                swal({
                    title: "گروه خبرنامه",
                    text: "گروه خبرنامه " + data.title + " با موفقیت ویرایش شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                GetAllNewsLetterGroup($("#language_ddl").val(), "#newsLetterGroup_ddl");
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function DeleteNewsLettersGroup(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف گروه خبرنامه مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/NewsLetters/DeleteGroup',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        $("#div-loading").addClass('hidden');
                        swal({
                            title: "گروه خبرنامه",
                            text: "گروه  " + data.title + "  با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                        $('#newsLetterGroup_tb').find('tr[id=nlg_' + data.group_ID + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#newsLetterGroup_tb').DataTable();
                            table.row("#nlg_" + data.group_ID).remove().draw(false);
                        });




                        GetAllNewsLetterGroup($("#language_ddl").val(), "#newsLetterGroup_ddl");
                    },
                    error: function (result) {
                    }
                });
            }
        });
}
//////////////////////////////////////////// بخش  خبرنامه
function GetAllNewsLetterGroup(lang, selector) {
    $.ajax({
        method: 'POST',
        url: '/Admin/NewsLetters/ViewAllGroupList',
        data: { id: lang },
        success: function (data) {

            var option = "";
            data.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $(selector).html(option);
        },
        error: function (result) {
        }
    });
}

function GetAllNewsLetterByGroupId(groupId) {
    $.ajax({
        method: 'POST',
        url: '/Admin/NewsLetters/ViewAllMemberOfNewsLetterByGroupId',
        data: { id: groupId },
        success: function (data) {
            var option = "";
            data.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $("#NLMember_lb").html(option);
        },
        error: function (result) {
        }
    });
}

function SendEmail() {
    var lenghList = $('.NLMemberSend_lb > option').length;
    var emailListString = '';  //ریختن ایمیل ها در نوعی از رشته 
    for (var i = 1; i <= lenghList; i++) {
        emailListString += $('.NLMemberSend_lb').children('option:nth-child(' + i + ')').text() + ';';
    }

    //var ckValue = CKEDITOR.instances['cke_txtContext'].getData();
    alert(emailListString);
    $.ajax({
        method: 'POST',
        url: '/Admin/NewsLetters/SendMail',
        data: { mailFrom: 'amirhosseink.7795@gmail.com', mailTo: emailListString, titleEmail: $('#txtTitle').val(), ckValue: CKEDITOR.instances['txtContext'].getData() },
        success: function (add) {
            var responsePart = eval(add.d);
            if (responsePart[0] == "ok") {
                swal({
                    title: "اشتراک ایمیل",
                    text: "ایمیل شما ارسال گردید",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#txtTitle").val('');
                CKEDITOR.instances['txtContext'].setData('');
            }
            else {
                swal({
                    title: "اشتراک ایمیل",
                    text: "خطایی در ارسال ایمیل رخ داده است",
                    type: "warning",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
            }
        },
        error: function (response) {
            //showModalMessage('.modalMessage', 'خطا', '<h3>خطایی رخ داده است</h3>', 'danger');
        }
    });
}
///////////////////////////////////////// بخش اشتراک پیامکی
function GetAllSmsSubscribes() {
    $.ajax({
        method: 'POST',
        url: '/Admin/SMS_Subscribe/ViewAllMemberOfSms',
        data: '',
        success: function (data) {
            var option = "";
            data.forEach(function (obj) {
                option += "<option value=" + obj.value + ">" + obj.text + "</option>";
            });
            $("#SSMember_lb").html(option);
        },
        error: function (result) {
        }
    });
}

function SendSMS() {
    $("#div-loading").removeClass('hidden');

    var lenghList = $('.SSMemberSend_lb > option').length;
    var mobileListString = '';  //ریختن ایمیل ها در نوعی از رشته 
    for (var i = 1; i <= lenghList; i++) {
        mobileListString += $('.SSMemberSend_lb').children('option:nth-child(' + i + ')').text() + ';';
    }
    $.ajax({
        method: 'POST',
        url: '/Admin/SMS_Subscribe/SendSMS',
        data: { mobiles: mobileListString, userName: "itsamojtaba", password: "9370270898", message: $("#txtContext").val(), from: "+985000125475" },
        success: function () {
            if (data == "ok") {
                swal({
                    title: "اشتراک پیامک",
                    text: "پیامک به شماره های موردنظر ارسال گردید",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#txtContext").val('');
                $("#div-loading").addClass('hidden');
            }
            else {
                swal({
                    title: "اشتراک پیامک",
                    text: "خطایی در ارسال پیامک رخ داده است",
                    type: "warning",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
            }
        },
        error: function (response) {
        }
    });
}
///////////////////////////////////////// بخش حامیان
function openModalForAddSocialNetwork() {
    $.get("/Admin/SocialNetwork/Create", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن شبکه مجازی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '55%' });
    });

}
//////
function InsertSocialNetwork() {
    var frm = $("#socialNetFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');


        var socialNetworks = new FormData();
        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }
        ///

        socialNetworks.append("Title", $("#Title").val()),
            socialNetworks.append("Link", $("#Link").val()),
            socialNetworks.append("Type", $("#Type").val()),
            socialNetworks.append("IsActive", status)


        $.ajax({
            method: 'POST',
            url: '/Admin/SocialNetwork/Create',
            data: socialNetworks,
            contentType: false,//برای ارسال عکس باید این مولفه با فالس مقداردهی شود
            processData: false, //برای استفاده از فرم دیتا باید این مولفه اضافه شود
            success: function (data) {

                if (data == "nall") {
                    swal("هشدار", "خطایی رخ داده است", "warning");
                    $("#div-loading").addClass('hidden');
                }
                else {
                    $("#div-loading").addClass('hidden');
                    $("#viewAll").load("/Admin/SocialNetwork/ViewAll");

                    $("#myModal").modal('hide');

                    swal({
                        title: "شبکه مجازی",
                        text: "شبکه مجازی " + data.title + " با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
function openModalForEditSocialNetwork(id) {
    $.get("/Admin/SocialNetwork/Edit/" + id, function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش شبکه مجازی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '55%' });
    });

}
//////
function UpdateSocialNetwork() {
    var frm = $("#socialNetFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');


        var socialNetworks = new FormData();
        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }
        ///

        socialNetworks.append("ID", $("#ID").val()),
            socialNetworks.append("Title", $("#Title").val()),
            socialNetworks.append("Link", $("#Link").val()),
            socialNetworks.append("Type", $("#Type").val()),
            socialNetworks.append("IsActive", status)


        $.ajax({
            method: 'POST',
            url: '/Admin/SocialNetwork/Edit',
            data: socialNetworks,
            contentType: false,//برای ارسال عکس باید این مولفه با فالس مقداردهی شود
            processData: false, //برای استفاده از فرم دیتا باید این مولفه اضافه شود
            success: function (data) {

                if (data == "nall") {
                    swal("هشدار", "خطایی رخ داده است", "warning");
                    $("#div-loading").addClass('hidden');
                }
                else {
                    $("#div-loading").addClass('hidden');
                    $("#viewAll").load("/Admin/SocialNetwork/ViewAll");

                    $("#myModal").modal('hide');
                    swal({
                        title: "شبکه مجازی",
                        text: "شبکه مجازی " + data.title + " با موفقیت ویرایش شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function DeleteSocialNetwork(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این شبکه اجتماعی مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/SocialNetwork/Delete',
                    data: { id: Id },
                    success: function (data) {
                        debugger;
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        $("#div-loading").addClass('hidden');
                        swal({
                            title: "شبکه مجازی",
                            text: "شبکه اجتماعی  " + data.title + "  با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });

                        $('#socialNet_tb').find('tr[id=scNet_' + data.id + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#socialNet_tb').DataTable();
                            table.row("#scNet_" + data.id).remove().draw(false);
                        });

                    },
                    error: function (result) {
                    }
                });
            }
        });
}

/////////////////////////////////////////////////// کاتالوگ
function GetCatalog() {
    $.ajax({
        method: "GET",
        url: "/Admin/Catalog/ViewAllCatalog",
        data: '',
        success: function (result) {

            $("#viewAll").html(result);
            //
        },
        error: function (res) {

        }
    });
}
function openModalForAddCatalog() {
    $.get("/Admin/Catalog/Create", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن کاتالوگ");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '50%' });
        //$("li[data-title=" + id + "]").css({ 'color': 'white' });
    });

}
//
function InsertCatalog() {
    var frm = $("#catalogFrm").valid();
    if (!frm) return false;

    $("#div-loading").removeClass('hidden');
    var catalog = new FormData();


    var files = $("#catalogUploader").get(0).files;

    catalog.append("thisFile", files[0]),
        catalog.append("Title", $("#Title").val())

    $.ajax({
        method: 'POST',
        url: '/Admin/Catalog/Create',
        data: catalog,
        processData: false,
        contentType: false,
        success: function (data) {


            $("#div-loading").addClass('hidden');
            $("#viewAll").load("/Admin/catalog/ViewAllCatalog");

            $("#myModal").modal('hide');

            swal({
                title: "کاتالوگ",
                text: "کاتالوگ با موفقیت ثبت شد",
                type: "success",
                confirmButtonColor: '#3085d6',
                confirmButtonText: "تائید"
            });
        },
        error: function (result) {
        }
    });


}

function DeleteCatalog(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این کاتالوگ مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Catalog/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        $("#div-loading").addClass('hidden');
                        swal({
                            title: "کاتالوگ",
                            text: "کاتالوگ با موفقیت حذف شد ",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });
                        $('#catalog_tb').find('tr[id=cat_' + data.id + ']').css('background-color', '#B26666').fadeOut(1000, function () {
                            var table = $('#catalog_tb').DataTable();
                            table.row("#cat_" + data.id).remove().draw(false);
                        });

                    },
                    error: function (result) {
                    }
                });
            }
        });
}
/////////////// /////  /////////////////////// Profile
var onFailed = function () {
    $("#div-loading").addClass('hidden');
    swal({
        title: "کاتالوگ",
        text: "خطایی رخ داده است !!",
        type: "warning",
        confirmButtonColor: '#3085d6',
        confirmButtonText: "تائید"
    });
};

var onSuccess_gp = function () {
    $("#div-loading").addClass('hidden');
    swal({
        title: "کاتالوگ",
        text: "اطلاعات شما با موفقیت به روز شد",
        type: "success",
        confirmButtonColor: '#3085d6',
        confirmButtonText: "تائید"
    });
};

var onBegin_gp = function () {
    $("#div-loading").removeClass('hidden');
};
///////////////////////////////////////////// SeoPage

function openModalForAddSeoPage() {
    $.get("/Admin/SeoPage/Create", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن صفحه سئو");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });

}

function InsertSeoPage() {
    var frm = $("#seoFrm").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');
        //var url=
        var seoPage = {
            UrlPage: decodeURIComponent($("#UrlPage").val()),
            MetaTitle: $("#MetaTitle").val(),
            MetaKeyword: $("#MetaKeyword").val(),
            MetaDescription: $("#MetaDescription").val(),
            MetaOther: $("#MetaOther").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/SeoPage/Create',
            data: seoPage,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                $("#viewAll").load("/Admin/SeoPage/ViewAll/");
                //$("#myModal").modal('hide');
                swal({ title: "تبریک", text: "صفحه برای سئو با موفقیت ثبت شد", icon: "success", buttons: [false, "تایید"], });
                $("#seoFrm")[0].reset();
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function openModalForEditSeoPage(Id) {
    $.get("/Admin/SeoPage/Edit/" + Id, function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش صفحه سئو");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '65%' });
    });

}
function UpdateSeoPage() {
    var frm = $("#seoFrmEdit").valid();
    if (frm === true) {

        $("#div-loading").removeClass('hidden');
        //var url=
        var seoPage = {
            SeoPage_ID: $("#SeoPage_ID").val(),
            UrlPage: decodeURIComponent($("#UrlPage").val()),
            MetaTitle: $("#MetaTitle").val(),
            MetaKeyword: $("#MetaKeyword").val(),
            MetaDescription: $("#MetaDescription").val(),
            MetaOther: $("#MetaOther").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/SeoPage/Edit',
            data: seoPage,
            success: function (data) {
                $("#div-loading").addClass('hidden');
                $("#viewAll").load("/Admin/SeoPage/ViewAll/");
                //$("#myModal").modal('hide');
                swal({ title: "تبریک", text: "صفحه برای سئو با موفقیت ویرایش شد", icon: "success", buttons: [false, "تایید"], });
                $("#seoFrmEdit")[0].reset();
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function checkboxSelectedForSeoPage() {
    swal({
        title: "هشدار !!!",
        text: "آیا برای حذف موارد انتخاب شده مطمئن هستید؟",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ["خیر", "بله"],
    })
        .then((willDelete) => {
            if (willDelete.value) {
                var checkboxValues = "";
                $("div.checked").children('.forRemove').each(function () {
                    checkboxValues += $(this).val() + "&";
                });

                var cv = [];
                cv = checkboxValues.split("&");
                if (checkboxValues != "") {

                    $.ajax({
                        url: "/Admin/SeoPage/checkboxSelected",
                        type: "Get",
                        data: { values: checkboxValues }
                    }).done(function (result) {
                        for (var i = 0; i < cv.length - 1; i++) {
                            $("#seo_" + cv[i]).hide("slow");
                        }
                        swal({ title: "تبریک", text: "عملیات حذف با موفقیت انجام شد", icon: "success", buttons: [false, "تایید"], });
                    });
                }
                else {
                    swal({ title: "هشدار", text: "لطفا یک مورد را انتخاب نمایید !", icon: "warning", buttons: [false, "تایید"], });
                }
            }
        });

}
function test() {

    var featureReply = "";
    //var checkboxValues = "";
    //$("div[class=icheckbox_square-blue]").each(function () { // گرفتن ولیو های چکس باکس هایی که تیک خورده باشند
    //    checkboxValues += $(this).html();//.children("input").val()+ "&";
    //});
    $("div.checked").children('.forRemove').each(function () {
        // checkboxValues += $(this).val() + "&";
        featureReply += $(this).attr('id') + "&";
    });
    alert(featureReply);
}


////////////////////////////////////////// بخش اعضای هیئت مدیره و مدیران اجرایی
function openModalForAddManager() {
    $.get("/Admin/HalycoManagers/Create", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن عضو هیئت مدیره و مدیر اجرایی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '55%' });
    });

}
//////
function InsertManager() {
    var frm = $("#HMForm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');


        var HManager = new FormData();
        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }
        ///
        var type = "";
        if ($("#Type").val() == "0") {
            type = "false";
        }
        else {
            type = "true";
        }

        var files = $("#photoUploader").get(0).files;
        if (files.length > 0) {
            HManager.append("ManagerName", $("#ManagerName").val()),
                HManager.append("ManagerPost", $("#ManagerPost").val()),
                HManager.append("ManagerEmail", $("#ManagerEmail").val()),
                HManager.append("ManagerPhoto", files[0]),
                HManager.append("ManagerType", type),
                HManager.append("IsActive", status)
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/HalycoManagers/Create',
            data: HManager,
            contentType: false,//برای ارسال عکس باید این مولفه با فالس مقداردهی شود
            processData: false, //برای استفاده از فرم دیتا باید این مولفه اضافه شود
            success: function (data) {

                if (data[0] == "nall") {
                    swal({
                        title: "عضو هیئت مدیره / مدیر اجرایی ",
                        text: "خطایی رخ داده است",
                        type: "warning",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                }
                else {
                    $("#div-loading").addClass('hidden');
                    $("#viewAll").load("/Admin/HalycoManagers/ViewAll/" + $('#managertype_ddl').val());

                    $("#myModal").modal('hide');
                    swal({
                        title: "عضو هیئت مدیره / مدیر اجرایی ",
                        text: "عضو هیئت مدیره / مدیر اجرایی " + data.managerName + " با موفقیت ثبت شد",
                        type: "success",
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "تائید"
                    });
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

//////
function openModalForEditManager(id) {
    $.get("/Admin/HalycoManagers/Edit/" + id, function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش عضو هیئت مدیره یا مدیر اجرایی");
        $("#myModalBody").html(result);
        $(".modal-dialog").css({ 'width': '55%' });
    });

}
////////
function UpdateManager() {



    var HManager = new FormData();
    var isactive = $('#IsActive:checked').length;
    var status = "";
    if (isactive === 1) {
        status = "true";
    }
    else {
        status = "false";
    }
    ///
    var type = "";
    if ($("#Type").val() == "0") {
        type = "false";
    }
    else {
        type = "true";
    }

    var files = $("#photoUploader").get(0).files;
    ///if (files.length > 0) {
    debugger;
    HManager.append("HManager_ID", $("#HManager_ID").val()),
        HManager.append("ManagerName", $("#ManagerName").val()),
        HManager.append("ManagerPost", $("#ManagerPost").val()),
        HManager.append("ManagerEmail", $("#ManagerEmail").val()),
        HManager.append("ManagerPhoto", $("#oldPhoto").val()),
        HManager.append("photo", files[0]),
        HManager.append("ManagerType", type),
        HManager.append("IsActive", status)

    // }

    $.ajax({
        method: 'POST',
        url: '/Admin/HalycoManagers/Edit',
        data: HManager,
        processData: false,
        contentType: false,
        success: function (data) {

            if (data == "nall") {
                swal({
                    title: "عضو هیئت مدیره / مدیر اجرایی ",
                    text: "خطایی رخ داده است",
                    type: "warning",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#div-loading").addClass('hidden');
            }
            else {
                $("#div-loading").addClass('hidden');
                $("#viewAll").load("/Admin/HalycoManagers/ViewAll/" + $('#managertype_ddl').val());

                $("#myModal").modal('hide');
                swal({
                    title: "عضو هیئت مدیره / مدیر اجرایی ",
                    text: "عضو هیئت مدیره / مدیر اجرایی " + data.managerName + " با موفقیت ویرایش شد",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
            }
        },
        error: function (result) {
        }
    });

}


///////
function DeleteManager(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این عضو هیئت مدیره / مدیر اجرایی مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/HalycoManagers/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }


                        $("#div-loading").addClass('hidden');
                        swal({
                            title: "عضو هیئت مدیره / مدیر اجرایی ",
                            text: "عضو هیئت مدیره / مدیر اجرایی  " + data.managerName + "  با موفقیت حذف شد",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "تائید"
                        });

                        $("#viewAll").load("/Admin/HalycoManagers/ViewAll/" + $('#managertype_ddl').val());
                    },
                    error: function (result) {
                    }
                });
            }
        });
}

function showViewAllManager(managerType) {
    //  alert(managerType);
    $("#viewAll").load("/Admin/HalycoManagers/ViewAll/" + managerType);
}

////////////////////////////////////////////////////////////// Employee

function openModalForAddEmployee() {
    $.get("/Admin/Employee/CreateEmployee", function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("افزودن کارمند جدید");
        $("#myModalBody").html(result);
        //$(".modal-dialog").css({ 'width': '65%' });
    });

}
function InsertEmployee() {
    debugger;
    var frm = $("#employFrm").valid();
    if (frm === false) {
        return false;
    }
    $("#div-loading").removeClass('hidden');
    var employee = new FormData();
    var files = $("#imgEmployUpload").get(0).files;

    employee.append("FullName", $("#FullName").val()),
        employee.append("PersonalCode", $("#PersonalCode").val()),
        employee.append("NationalCode", $("#NationalCode").val()),
        employee.append("PersonPassword", $("#PersonPassword").val()),
        employee.append("PersonRePassword", $("#PersonRePassword").val()),
        employee.append("OfficePostId", $("#OfficePostId").val()),
        employee.append("OfficeUnitId", $("#OfficeUnitId").val()),
        employee.append("Tel", $("#Tel").val()),
        employee.append("Mobile", $("#Mobile").val()),
        employee.append("imgUser", files[0])


    $.ajax({
        method: 'POST',
        url: '/Admin/Employee/CreateEmployee',
        data: employee,
        contentType: false,
        processData: false,
        success: function (data) {
            debugger;
            $("#div-loading").addClass('hidden');
            if (data == "ok") {
                swal({
                    title: "ثبت کارمند",
                    text: "کارمند جدید با موفقیت ثبت شد",
                    type: "success"
                    , confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#viewAll").load("/Admin/Employee/ViewAll");
            }
            else if (data == "repeate") {
                swal({
                    title: "ثبت کارمند",
                    text: "این کارمند وجود دارد !",
                    type: "warning"
                    , confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                })
            } else {
                swal({
                    title: "ثبت کارمند",
                    text: data.message,
                    type: "warning"
                    , confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                console.log(data);
            }

        },
        error: function (result) {
            alert(result.error);
        }
    });
}

function openModalForEditEmployee(Id) {
    $.get("/Admin/Employee/EditEmployee/" + Id, function (result) {

        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش کارمند");
        $("#myModalBody").html(result);
        //$(".modal-dialog").css({ 'width': '65%' });
    });

}
function UpdateEmployee() {
    debugger;
    var frm = $("#employFrmEdit").valid();
    if (frm === false) {
        return false;
    }
    $("#div-loading").removeClass('hidden');
    var employee = new FormData();
    var files = $("#imgEmployUpload").get(0).files;

    employee.append("UserIdentity_Id", $("#UserIdentity_Id").val()),
        employee.append("FullName", $("#FullName").val()),
        employee.append("PersonalCode", $("#PersonalCode").val()),
        employee.append("NationalCode", $("#NationalCode").val()),
        employee.append("PersonPassword", $("#PersonPassword").val()),
        employee.append("PersonRePassword", $("#PersonRePassword").val()),
        employee.append("OfficePostId", $("#OfficePostId").val()),
        employee.append("OfficeUnitId", $("#OfficeUnitId").val()),
        employee.append("PersonPic", $("#oldPersonPic").val()),
        employee.append("Tel", $("#Tel").val()),
        employee.append("Mobile", $("#Mobile").val()),
        employee.append("imgUser", files[0])


    $.ajax({
        method: 'POST',
        url: '/Admin/Employee/EditEmployee',
        data: employee,
        contentType: false,
        processData: false,
        success: function (data) {
            debugger;
            $("#div-loading").addClass('hidden');
            if (data == "ok") {
                swal({
                    title: "ثبت کارمند",
                    text: "کارمند جدید با موفقیت ثبت شد",
                    type: "success"
                    , confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                });
                $("#viewAll").load("/Admin/Employee/ViewAll");
            }
            else if (data == "repeate") {
                swal({
                    title: "ثبت کارمند",
                    text: "این کارمند وجود دارد !",
                    type: "warning"
                    , confirmButtonColor: '#3085d6',
                    confirmButtonText: "تائید"
                })
            } else {
                console.log(data);
            }

        },
        error: function (result) {
            alert(result.error);
        }
    });
}

//////////////////////////// بخش زمینه فعالیت ها
function openModalForAddActivityField() {
    $.get("/Admin/ActivityField/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن زمینه فعالیت جدید ");
        $("#myModalBody").html(result);
    });

}
//
function InsertActivityField() {
    debugger;
    var frm = $("#ActivityFieldFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');
        var isactive = $('#isActiveOff:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }
        var ActivityField = {
            ActivityFieldTitle: $("#ActivityFieldTitle").val(),
            ActivityFieldDescription: $("#ActivityFieldDescription").val(),
            IsActive: status
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/ActivityField/Create',
            data: ActivityField,
            success: function (data) {
                if (data == "repeate") {
                    $("#div-loading").addClass('hidden');
                    swal({

                        type: 'warning',
                        title: "افزودن زمینه فعالیت",
                        text: "این زمینه فعالیت قبلا ثبت شده",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });


                }
                else if (data != "nall") {

                    $("#viewAll").load("/Admin/ActivityField/ViewAll/");
                    $("#myModal").modal("hide");

                    swal({

                        type: 'success',
                        title: "افزودن زمینه فعالیت",
                        text: "زمینه فعالیت  " + data.activityFieldTitle + " با موفقیت ثبت شد ",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

//
function openModalForEditActivityField(id) {
    $.get("/Admin/ActivityField/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش زمینه فعالیت");
        $("#myModalBody").html(result);
    });

}
//
function UpdateActivityField() {
    debugger;
    var frm = $("#ActivityFieldFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');
        var isactive = $('#isActiveOff:checked').length;
        var status = "";
        if (isactive === 1) {
            status = "true";
        }
        else {
            status = "false";
        }
        var ActivityField = {
            ActivityFieldID: $("#ActivityFieldID").val(),
            ActivityFieldTitle: $("#ActivityFieldTitle").val(),
            ActivityFieldDescription: $("#ActivityFieldDescription").val(),
            IsActive: status
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/ActivityField/Edit',
            data: ActivityField,
            success: function (data) {

                $("#viewAll").load("/Admin/ActivityField/ViewAll/");
                $("#myModal").modal("hide");
                swal({
                    type: 'success',
                    title: "افزودن زمینه فعالیت",
                    text: "زمینه فعالیت  " + data.activityFieldTitle + " با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                $("#div-loading").addClass('hidden');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function DeleteActivityField(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این زمینه فعالیت مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonActivityField: '#3085d6',
        cancelButtonActivityField: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/ActivityField/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        //if (data == "NOK") {
                        //    $("#div-loading").addClass('hidden');
                        //    swal({
                        //        type: 'warning',
                        //        title: "حذف زمینه فعالیت",
                        //        text: "این واحد برای محصولی تعریف شده است ابتدا محصول باید حذف شود",
                        //        showConfirmButton: true,
                        //        confirmButtonText: "تائید"
                        //    });

                        //}
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                type: 'success',
                                title: "حذف زمینه فعالیت",
                                text: "زمینه فعالیت " + data.activityFieldTitle + "  با موفقیت حذف شد",
                                showConfirmButton: true,
                                confirmButtonText: "تائید"
                            });
                            //$('#ActivityField_tb').find('tr[id=ActivityField_' + data.ActivityField_ID + ']').css('background-ActivityField', '#B26666').fadeOut(1000, function () {
                            //    var table = $('#ActivityField_tb').DataTable();
                            //    table.row("#ActivityField_" + data.ActivityField_ID).remove().draw(false);
                            //});
                            $("#viewAll").load("/Admin/ActivityField/ViewAll/");
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}


//////////////////////////// بخش واحد اداری
function openModalForAddOfficeUnit() {
    $.get("/Admin/OfficeUnit/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن واحد اداری جدید ");
        $("#myModalBody").html(result);
    });

}
//
function InsertOfficeUnit() {
    debugger;
    var frm = $("#OfficeUnitFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var OfficeUnit = {
            OfficeUnitTitle: $("#OfficeUnitTitle").val(),
            OfficeUnitTasks: $("#OfficeUnitTasks").val(),
            OfficeUnitResponsibility: $("#OfficeUnitResponsibility").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/OfficeUnit/Create',
            data: OfficeUnit,
            success: function (data) {
                if (data == "repeate") {
                    $("#div-loading").addClass('hidden');
                    swal({

                        type: 'warning',
                        title: "افزودن واحد اداری",
                        text: "این واحد اداری قبلا ثبت شده",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });


                }
                else if (data != "nall") {

                    $("#viewAll").load("/Admin/OfficeUnit/ViewAll/");
                    $("#myModal").modal("hide");

                    swal({

                        type: 'success',
                        title: "افزودن واحد اداری",
                        text: "واحد اداری  " + data.officeUnitTitle + " با موفقیت ثبت شد ",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

//
function openModalForEditOfficeUnit(id) {
    $.get("/Admin/OfficeUnit/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش سمت");
        $("#myModalBody").html(result);
    });

}
//
function UpdateOfficeUnit() {
    debugger;
    var frm = $("#OfficeUnitFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var OfficeUnit = {
            OfficeUnitID: $("#OfficeUnitID").val(),
            OfficeUnitTitle: $("#OfficeUnitTitle").val(),
            OfficeUnitTasks: $("#OfficeUnitTasks").val(),
            OfficeUnitResponsibility: $("#OfficeUnitResponsibility").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/OfficeUnit/Edit',
            data: OfficeUnit,
            success: function (data) {

                $("#viewAll").load("/Admin/OfficeUnit/ViewAll/");
                $("#myModal").modal("hide");
                swal({
                    type: 'success',
                    title: "ویرایش واحد اداری",
                    text: "واحد اداری  " + data.officeUnitTitle + " با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                $("#div-loading").addClass('hidden');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function DeleteOfficeUnit(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این واحد اداری مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonOfficeUnit: '#3085d6',
        cancelButtonOfficeUnit: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/OfficeUnit/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        //if (data == "NOK") {
                        //    $("#div-loading").addClass('hidden');
                        //    swal({
                        //        type: 'warning',
                        //        title: "حذف سمت",
                        //        text: "این واحد برای محصولی تعریف شده است ابتدا محصول باید حذف شود",
                        //        showConfirmButton: true,
                        //        confirmButtonText: "تائید"
                        //    });

                        //}


                        else {
                            $("#div-loading").addClass('hidden');
                            debugger;
                            console.log(data);
                            swal({
                                type: 'success',
                                title: "حذف واحد اداری",
                                text: "واحد اداری " + data.title + "  با موفقیت حذف شد",
                                showConfirmButton: true,
                                confirmButtonText: "تائید"
                            });
                            //$('#OfficeUnit_tb').find('tr[id=OfficeUnit_' + data.OfficeUnit_ID + ']').css('background-OfficeUnit', '#B26666').fadeOut(1000, function () {
                            //    var table = $('#OfficeUnit_tb').DataTable();
                            //    table.row("#OfficeUnit_" + data.OfficeUnit_ID).remove().draw(false);
                            //});
                            $("#viewAll").load("/Admin/OfficeUnit/ViewAll/");
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}


//////////////////////////// بخش پست اداری
function openModalForAddOfficePost() {
    $.get("/Admin/OfficePost/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن پست اداری جدید ");
        $("#myModalBody").html(result);
    });

}
//
function InsertOfficePost() {
    debugger;
    var frm = $("#OfficePostFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var OfficePost = {
            OfficePostTitle: $("#OfficePostTitle").val(),
            OfficePostRoles: $("#OfficePostRoles").val(),
            OfficePostResponsibility: $("#OfficePostResponsibility").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/OfficePost/Create',
            data: OfficePost,
            success: function (data) {
                if (data == "repeate") {
                    $("#div-loading").addClass('hidden');
                    swal({

                        type: 'warning',
                        title: "افزودن پست اداری",
                        text: "این پست اداری قبلا ثبت شده",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });


                }
                else if (data != "nall") {

                    $("#viewAll").load("/Admin/OfficePost/ViewAll/");
                    $("#myModal").modal("hide");

                    swal({

                        type: 'success',
                        title: "افزودن پست اداری",
                        text: "پست اداری  " + data.officePostTitle + " با موفقیت ثبت شد ",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

//
function openModalForEditOfficePost(id) {
    $.get("/Admin/OfficePost/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش سمت");
        $("#myModalBody").html(result);
    });

}
//
function UpdateOfficePost() {
    debugger;
    var frm = $("#OfficePostFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var OfficePost = {
            OfficePostID: $("#OfficePostID").val(),
            OfficePostTitle: $("#OfficePostTitle").val(),
            OfficePostRoles: $("#OfficePostRoles").val(),
            OfficePostResponsibility: $("#OfficePostResponsibility").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/OfficePost/Edit',
            data: OfficePost,
            success: function (data) {

                $("#viewAll").load("/Admin/OfficePost/ViewAll/");
                $("#myModal").modal("hide");
                swal({
                    type: 'success',
                    title: "ویرایش پست اداری",
                    text: "پست اداری  " + data.officePostTitle + " با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                $("#div-loading").addClass('hidden');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function DeleteOfficePost(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این پست اداری مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonOfficePost: '#3085d6',
        cancelButtonOfficePost: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/OfficePost/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }

                        //if (data == "NOK") {
                        //    $("#div-loading").addClass('hidden');
                        //    swal({
                        //        type: 'warning',
                        //        title: "حذف سمت",
                        //        text: "این پست برای محصولی تعریف شده است ابتدا محصول باید حذف شود",
                        //        showConfirmButton: true,
                        //        confirmButtonText: "تائید"
                        //    });

                        //}


                        else {
                            $("#div-loading").addClass('hidden');
                            debugger;
                            console.log(data);
                            swal({
                                type: 'success',
                                title: "حذف پست اداری",
                                text: "پست اداری " + data.title + "  با موفقیت حذف شد",
                                showConfirmButton: true,
                                confirmButtonText: "تائید"
                            });
                            //$('#OfficePost_tb').find('tr[id=OfficePost_' + data.OfficePost_ID + ']').css('background-OfficePost', '#B26666').fadeOut(1000, function () {
                            //    var table = $('#OfficePost_tb').DataTable();
                            //    table.row("#OfficePost_" + data.OfficePost_ID).remove().draw(false);
                            //});
                            $("#viewAll").load("/Admin/OfficePost/ViewAll/");
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}



//////////////////////////// بخش فعالیت ها
function openModalForAddActivity() {
    $.get("/Admin/Activity/Create", function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("افزودن فعالیت جدید ");
        $("#myModalBody").html(result);
    });

}
//
function InsertActivity() {
    debugger;
    var frm = $("#ActivityFrm").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var Activity = {
            ActivityTitle: $("#ActivityTitle").val(),
            ActivityDescription: $("#ActivityDescription").val(),
            ActivityFieldID: $("#ActivityFieldID").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Activity/Create',
            data: Activity,
            success: function (data) {
                if (data == "repeate") {
                    $("#div-loading").addClass('hidden');
                    swal({

                        type: 'warning',
                        title: "افزودن فعالیت",
                        text: "این فعالیت قبلا ثبت شده",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });


                }
                else if (data != "nall") {

                    $("#viewAll").load("/Admin/Activity/ViewAll/" + $('#ActivityFieldList').val());
                    $("#myModal").modal("hide");

                    swal({

                        type: 'success',
                        title: "افزودن فعالیت",
                        text: "فعالیت  " + data.activityTitle + " با موفقیت ثبت شد ",
                        showConfirmButton: true,
                        confirmButtonText: "تائید"
                    });
                    $("#div-loading").addClass('hidden');
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

//
function openModalForEditActivity(id) {
    $.get("/Admin/Activity/Edit/" + id, function (result) {
        $("#myModal").modal();
        $("#myModalLabel").html("ویرایش فعالیت");
        $("#myModalBody").html(result);
    });

}
//
function UpdateActivity() {
    debugger;
    var frm = $("#ActivityFrmEdit").valid();
    if (frm === true) {
        $("#div-loading").removeClass('hidden');

        var Activity = {
            Activity_ID: $("#Activity_ID").val(),
            ActivityTitle: $("#ActivityTitle").val(),
            ActivityDescription: $("#ActivityDescription").val(),
            ActivityFieldID: $("#ActivityFieldID").val()
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Activity/Edit',
            data: Activity,
            success: function (data) {

                $("#viewAll").load("/Admin/Activity/ViewAll/" + $('#ActivityFieldList').val());
                $("#myModal").modal("hide");
                swal({
                    type: 'success',
                    title: "ویرایش فعالیت",
                    text: "فعالیت  " + data.activityTitle + " با موفقیت ویرایش شد ",
                    showConfirmButton: true,
                    confirmButtonText: "تائید"
                });

                $("#div-loading").addClass('hidden');
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
//
function DeleteActivity(Id) {
    swal({
        title: "هشدار",
        text: "آیا برای حذف این فعالیت مطمئن هستید؟",
        type: "warning",
        showCancelButton: true,
        confirmButtonActivity: '#3085d6',
        cancelButtonActivity: '#d33',
        confirmButtonText: "بله",
        cancelButtonText: "لغو"
    })
        .then((willDelete) => {
            if (willDelete.value) {

                $("#div-loading").removeClass('hidden');

                $.ajax({
                    method: 'POST',
                    url: '/Admin/Activity/Delete',
                    data: { id: Id },
                    success: function (data) {
                        if (data.redirect) { ShowAccessDenied(); return false; }
                        else {
                            $("#div-loading").addClass('hidden');
                            swal({
                                type: 'success',
                                title: "حذف فعالیت",
                                text: "فعالیت " + data.title + "  با موفقیت حذف شد",
                                showConfirmButton: true,
                                confirmButtonText: "تائید"
                            });

                            $("#viewAll").load("/Admin/Activity/ViewAll/" + $('#ActivityFieldList').val());
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
}

function GetAllActivityByActivityField() {
    $("#viewAll").load("/Admin/Activity/ViewAll/" + $('#ActivityFieldList').val());
}