﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DomainClasses.Role
{
    public class ApplicationRole:IdentityRole<string>
    {
        public ApplicationRole()
        {}
    
        [MaxLength(450)]
        public string ParentRoleId { get; set; }

        [ForeignKey("ParentRoleId")]
        public ApplicationRole ParentRole { get; set; }

        [StringLength(100)]
        [Display(Name =("وظیفه"))]
        public string Task { get; set; }

        [StringLength(100)]
        [Display(Name = ("مسئولیت"))]
        public string Responsibilty { get; set; }

        [Display(Name = ("توضیحات"))]
        public string Description { get; set; }

        [Required]
        [Display(Name = ("وضعیت "))]
        public byte Role_Status { get; set; }

        [Display(Name = ("نوع "))]
        public byte Role_Type { get; set; }
       
        public string Access { get; set; }

        [DisplayName("آیا پیامک درخواست کارمند زیرمجموعه این نقش برای نقش پدر نیز ارسال شود؟")]
        public bool SendSmsToParent { get; set; }

        [DisplayName("آیا نتیجه تعیین وضعیت درخواست توسط این نقش برای درخواست دهنده ارسال شود؟")]
        //در واقع در این فیلد شناسه نقشی که تعیین کننده وضعیت نهایی درخواست هست مشخص می شود
        [ForeignKey("ApplicationRoleSendFinalSmsToUser")]
        public string SendFinalSmsToUser { get; set; }

        [DisplayName("آیا نماینده مدیریت(مدیر پشتیبانی) درخواست های این کاربر را ببیند؟")]
        public bool IsAgentOfManager { get; set; }
        public virtual ApplicationRole ApplicationRoleSendFinalSmsToUser { get; set; }
    }
   
    
}
