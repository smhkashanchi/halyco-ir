﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Catalog
{
    public class Catalog
    {
        public Catalog()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte ID { get; set; }
        [DisplayName("عنوان")]
        [MaxLength(100)]
        [Required(ErrorMessage = "{0} را وارد نمائید")]
        public string Title { get; set; }
        [Required(ErrorMessage ="{0} را انتخاب  نمائید")]
        [DisplayName("فایل")]
        [MaxLength(250)]
        public string Files { get; set; }
    }
}
