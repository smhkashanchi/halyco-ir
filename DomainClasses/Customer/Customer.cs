﻿using DomainClasses.Cart;
using DomainClasses.User;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Customer
{
    public class Customer
    {
        public Customer()
        {

        }
        [Key]
        public int Customer_ID { get; set; }
        [ForeignKey("UserIdentity_Id")]
        public ApplicationUser ApplicationUser { get; set; }
        [StringLength(450)]

        public string UserIdentity_Id { get; set; }
        [DisplayName("تاریخ تولد")]
        public DateTime? BirthDate { get; set; }
        [DisplayName("کد ملی")]
        [MaxLength(20)]
        public string NationalCode { get; set; }
        [DisplayName("جنسیت")]
        public bool Jender { get; set; }
       
        [DisplayName("عضو خبرنامه")]
        public bool IsNewsLetter { get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }
        [DisplayName("کد فعالسازی")]
        [MaxLength(50)]
        public string ActiveCode { get; set; }
        public virtual IEnumerable<CustomerAddress> CustomerAddresses { get; set; }
        public IEnumerable<Factor> Factor { get; set; }
    }
}
