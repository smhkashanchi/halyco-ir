﻿using DomainClasses.Cart;
using DomainClasses.Region;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DomainClasses.Customer
{
   public class CustomerAddress
    {
        [Key]
        public int CustomerAddress_Id { get; set; }
        [Display(Name = "آدرس")]
        [Required(ErrorMessage = "{0} را وارد کنید")]
        public string Address { get; set; }
        [Display(Name = "کد پستی")]
        [Required(ErrorMessage = "{0} را وارد کنید")]
        public string PostalCode { get; set; }
        [Display(Name ="شماره همراه")]
        [Required(ErrorMessage ="{0} را وارد کنید")]
        public string Phone { get; set; }

        public Customer Customer { get; set; }
        public int Customer_Id { get; set; }

        public virtual Country Country{ get; set; }
        public int Country_Id { get; set; }

        public virtual State State { get; set; }
        [Display(Name = "استان")]
        public int State_Id { get; set; }


        public virtual City City { get; set; }
        [Display(Name = "شهرستان")]
        public int City_Id { get; set; }
        public IEnumerable<Factor> Factor { get; set; }


    }
}
