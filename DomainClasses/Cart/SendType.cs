﻿using DomainClasses.Language;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Cart
{
    public class SendType
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte SendType_Id { get; set; }
        public string SendType_Title { get; set; }
        public string SendType_Description { get; set; }
        public byte SendType_Type { get; set; }
        public int SendType_Price { get; set; }


        //Lang
        public virtual Language.Language Language{ get; set; }
        public string Lang_ID { get; set; }

        public IEnumerable<Factor> Factor { get; set; }

    }
}
