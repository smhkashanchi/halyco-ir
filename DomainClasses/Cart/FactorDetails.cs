﻿using DomainClasses.Product;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DomainClasses.Cart
{
    public class FactorDetails
    {
        public FactorDetails()
        {

        }
        [Key]
        public int FactorDetails_Id { get; set; }
       
        public Int16 Count { get; set; }
        public int FinalPrice { get; set; }

        public Garanty Garanty { get; set; }
        public Int16? Garanty_Id { get; set; }

        public ProductInfo ProductInfo { get; set; }
        public int ProductInfo_Id { get; set; }

        public Factor Factor { get; set; }
        public int Factor_Id { get; set; }
        public string Name{ get; set; }
    }
}
