﻿using DomainClasses.Customer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
namespace DomainClasses.Cart
{
    public class Factor
    {
        [Key]
        public int Factor_Id { get; set; }
    [Display(Name ="تاریخ")]
        public string Factor_Date { get; set; }
        [Display(Name = "قیمت کل")]

        public int TotalPrice { get; set; }
        [Display(Name = "وضعیت سفارش")]

        public byte Order_Status { get; set; }
        [Display(Name = "وضعیت پرداخت")]

        public byte PayMent_Status { get; set; }

        public string Factor_Lang { get; set; }
        [Display(Name = "کد رهگیری بانک")]

        public int Bank_Code { get; set; }
        [Display(Name = "روش پرداخت")]

        public byte Payment_Type { get; set; }

       

        public virtual Customer.Customer Customer { get; set; }
        public int Customer_Id { get; set; }
        public virtual CustomerAddress CustomerAddress { get; set; }
        public int CustomerAddress_Id { get; set; }

        public virtual Language.Language Language { get; set; }
        public string Lang_ID { get; set; }

        public virtual SendType SendType { get; set; }
        [Display(Name = "روش ارسال")]

        public byte SendType_ID { get; set; }

        public virtual IEnumerable<FactorDetails> FactorDetails { get; set; }


    }
}
