﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DomainClasses.AVF
{
    public class AVFGroup
    {
        public AVFGroup()
        {

        }
        [Key]
        public Int16 AVFGroup_ID { get; set; }
        [DisplayName("عنوان گروه")]
        [Required(ErrorMessage="لطفا {0} را وارد نمایید")]
        [MaxLength(150)]
        public string AVFGroupTitle { get; set; }
        [Column(TypeName="varchar(8)")]
        [ForeignKey("Language")]
        public string Lang { get; set; }
        [DisplayName("فعال / غیر فعال")]
        public bool IsActive { get; set; }
        [ForeignKey("AVFGroups")]
        public Int16? ParentId { get; set; }

        [MaxLength(200)]
        [DisplayName("تصویر گروه")]
        public string AVFGroupImage { get; set; }

        public virtual Language.Language Language { get; set; }
        public virtual AVFGroup AVFGroups { get; set; }
        public virtual List<AVF> AVFs { get; set; }
    }
}