﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DomainClasses.AVF
{
    public class AVF
    {
        public AVF()
        {

        }
        [Key]
        public int AVF_ID { get; set; }
        [DisplayName("گروه")]
        [ForeignKey("AVFGroup")]
        public Int16 AVFGroupId { get; set; }

        [DisplayName("عنوان فایل")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(200)]
        public string AVFTitle { get; set; }
        [DisplayName("فایل / لینک")]
        public bool FileOrLink { get; set; }

        [DisplayName("نام فایل / لینک")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(2000)]
        public string FileNameOrLink { get; set; }
        [DisplayName("اندازه فایل")]
        [MaxLength(50)]
        public string AVFSize { get; set; }
        [DisplayName("تعداد دانلود")]
        public Int16? AVFDownloadCount { get; set; }
        [DisplayName("نوع فایل")]
        [MaxLength(50)]
        public string FileType { get; set; }
        [DisplayName("فعال / غیر فعال")]
        public bool IsActive { get; set; }
        [DisplayName("تاریخ ")]
        public DateTime CreateDate { get; set; }
        [DisplayName("تصویر ")]
        [MaxLength(100)]
        public string VideoImage { get; set; }
        public virtual AVFGroup AVFGroup  { get; set; }
        public virtual List<Project.ProjectVideo> ProjectVideos{ get; set; }
    }
}