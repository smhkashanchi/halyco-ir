﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.CustomerComplaint
{
   public class CustomerComplaintFiles
    {
        public CustomerComplaintFiles()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Int16 CC_File_ID { get; set; }
        [DisplayName("شکایت")]
        [ForeignKey("CustomerComplaintMaster")]
        public Int16 ComplaintId { get; set; }
        [DisplayName("فایل")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string FileNames { get; set; }
        [DisplayName("عنوان")]
        [MaxLength(50)]
        public string Title { get; set; }
        public virtual CustomerComplaintMaster CustomerComplaintMaster { get; set; }
    }
}
