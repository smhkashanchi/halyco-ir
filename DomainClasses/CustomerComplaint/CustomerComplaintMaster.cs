﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.CustomerComplaint
{
    public class CustomerComplaintMaster
    {
        public CustomerComplaintMaster()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int16 ComplaintID { get; set; }
        [DisplayName("محصول")]
        [ForeignKey("ProductInfo")]
        public int ProductId { get; set; }
        [DisplayName("نام و نام خانوادگی")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string FullName { get; set; }
        [DisplayName("نام شرکت")]
        [MaxLength(30)]
        public string CompanyName { get; set; }
        [DisplayName("آدرس")]
        [MaxLength(200)]
        public string Address { get; set; }
        [DisplayName("تلفن")]
        [MaxLength(15)]
        public string Tell { get; set; }
        [DisplayName("تلفن همراه")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(15)]
        public string PhoneNumber { get; set; }
        [DisplayName("فکس")]
        [MaxLength(20)]
        public string Fax { get; set; }
        [DisplayName("ایمیل")]
        [EmailAddress(ErrorMessage ="لطفا {0} را بصورت صحیح وارد نمایید")]
        [MaxLength(100)]
        public string Email { get; set; }
        [DisplayName("نام نمایندگی")]
        [MaxLength(50)]
        public string AgencyName { get; set; }
        [DisplayName("تاریخ وقوع مشکل")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(25)]
        public string OccurrenceDate { get; set; }
        [DisplayName("شرح مشکل")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string Description { get; set; }
        [DisplayName("درخواست")]
        [MaxLength(2000)]
        public string Request { get; set; }

        public virtual Product.ProductInfo ProductInfo { get; set; }
        public virtual List<CustomerComplaintFiles> CustomerComplaintFiles { get; set; }
    }
}
