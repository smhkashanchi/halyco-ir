﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Employement
{
    public class Emp_ComputerSkillItems
    {
        public Emp_ComputerSkillItems()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int16 Item_ID { get; set; }
        [DisplayName("عنوان مهارت")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string Title { get; set; }

        public virtual List<Emp_ComputerSkills> Emp_ComputerSkills { get; set; }
    }
}
