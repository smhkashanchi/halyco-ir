﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Employement
{
    public class Emp_UnderSupervisor
    {
        public Emp_UnderSupervisor()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }
        [DisplayName("فرد استخدامی")]
        [ForeignKey("Emp_BaseInformation")]
        public int Employement_Id { get; set; }
        [DisplayName("نام و نام خانوادگی")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(150)]
        public string FullName { get; set; }
        [DisplayName("جنسیت")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(10)]
        public string Jender { get; set; }
        [DisplayName("نسبت")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string Relation { get; set; }
        [DisplayName("تاریخ تولد")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(15)]
        public string BirthDate { get; set; }
        [DisplayName("سطح تحصلیات")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(30)]
        public string LevelEducation { get; set; }
        [DisplayName("شغل")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(150)]
        public string Job { get; set; }

        public virtual Emp_BaseInformation Emp_BaseInformation { get; set; }
    }
}
