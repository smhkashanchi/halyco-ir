﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Employement
{
    public class Emp_Guarantor
    {
        public Emp_Guarantor()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Guarantor_ID { get; set; }
        [DisplayName("فرد استخدامی")]
        [ForeignKey("Emp_BaseInformation")]
        public int Employement_Id { get; set; }
        [DisplayName("نام و نام خانوادگی")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(150)]
        public string FullName { get; set; }
        [DisplayName("نسبت")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(80)]
        public string Relation { get; set; }
        [DisplayName("شغل")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string Job { get; set; }
        [DisplayName("تلفن همراه")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(15)]
        public string Mobile { get; set; }
        [DisplayName("محل کار")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(400)]
        public string WorkPlace { get; set; }

        public virtual Emp_BaseInformation Emp_BaseInformation { get; set; }
    }
}
