﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Employement
{
   public class Emp_WorkExperience
    {
        public Emp_WorkExperience()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int WorkExp_ID { get; set; }
        [DisplayName("اطلاعات شخصی")]
        [ForeignKey("Emp_BaseInformation")]
        public int Employment_Id { get; set; }
        [DisplayName("نام سازمان / شرکت")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string OrganizationName { get; set; }
        [DisplayName("سمت")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string Post { get; set; }
        [DisplayName("مدت همکاری")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(20)]
        public string AssistPeriod { get; set; }
        [DisplayName("تاریخ قطع همکاری")]
        //[Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(15)]
        public string AssistCutDate { get; set; }
        [DisplayName("تلفن")]
        [MaxLength(15)]
        public string Tell { get; set; }
        [DisplayName("متوسط حقوق دریافتی")]
        [MaxLength(20)]
        public string AverageSalary { get; set; }
        [DisplayName("علت ترک همکاری")]
        [MaxLength(100)]
        public string AssistCutReason { get; set; }

        public virtual Emp_BaseInformation Emp_BaseInformation { get; set; }
    }
}
