﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Employement
{
    public class Emp_TraningCourse
    {
        public Emp_TraningCourse()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TraningCourse_ID { get; set; }
        [DisplayName("فرد استخدامی")]
        [ForeignKey("Emp_BaseInformation")]
        public int Employement_Id { get; set; }
        [DisplayName("نام دوره")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string TraningName { get; set; }
        [DisplayName("نام سازمان")]
        [MaxLength(100)]
        public string OrganizationName { get; set; }
        [DisplayName("مدت دوره")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(30)]
        public string TraningPeroid { get; set; }
        [DisplayName("تاریخ شروع")]
        [MaxLength(15)]
        public string StartDate { get; set; }
        [DisplayName("توضیحات")]
        [MaxLength(300)]
        public string Description { get; set; }
        [DisplayName("مدرک ؟")]
        public bool IsEvidence { get; set; }

        public virtual Emp_BaseInformation Emp_BaseInformation { get; set; }
    }
}
