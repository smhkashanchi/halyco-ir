﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Employement
{
    public class Emp_Activity
    {
        public Emp_Activity()
        {

        }

        [ForeignKey("Emp_BaseInformation")]
        [DisplayName("فرد استخدامی")]
        public int Employement_Id { get; set; }
        [ForeignKey("Emp_ActivityItems")]
        [DisplayName("عنوان فعالیت")]
        public Int16 ItemId { get; set; }

        public virtual Emp_BaseInformation Emp_BaseInformation { get; set; }
        public virtual Emp_ActivityItems Emp_ActivityItems { get; set; }
    }
}
