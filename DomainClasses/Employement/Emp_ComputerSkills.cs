﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Employement
{
    public class Emp_ComputerSkills
    {
        public Emp_ComputerSkills()
        {

        }
        [ForeignKey("Emp_BaseInformation")]
        [DisplayName("فرد استخدامی")]
        public int Employement_Id { get; set; }
        [ForeignKey("Emp_ComputerSkillItems")]
        [DisplayName("عنوان مهارت")]
        public Int16 ItemId { get; set; }

        public virtual Emp_BaseInformation Emp_BaseInformation { get; set; }
        public virtual Emp_ComputerSkillItems Emp_ComputerSkillItems{ get; set; }
    }
}
