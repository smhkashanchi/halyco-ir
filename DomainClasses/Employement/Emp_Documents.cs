﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Employement
{
    public class Emp_Documents
    {
        public Emp_Documents()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Doc_ID { get; set; }
        [DisplayName("فرد استخدامی")]
        [ForeignKey("Emp_BaseInformation")]
        public int Employement_Id { get; set; }
        [DisplayName("عنوان")]
        [MaxLength(50)]
        public string Title { get; set; }
        [DisplayName("فایل")]
        [MaxLength(100)]
        public string FileName { get; set; }

        public virtual Emp_BaseInformation Emp_BaseInformation { get; set; }
    }
}
