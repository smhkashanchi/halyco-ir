﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Employement
{
   public class Emp_EducationalRecords
    {
        public Emp_EducationalRecords()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Edu_ID { get; set; }
        [DisplayName("اطلاعات شخصی")]
        [ForeignKey("Emp_BaseInformation")]
        public int Employment_Id { get; set; }
        [DisplayName("مدرک تحصیلی")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(30)]
        public string Evidence { get; set; }
        [DisplayName("رشته تحصیلی")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(60)]
        public string Field { get; set; }
        [DisplayName("معدل")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(6,ErrorMessage ="لطفا {0} را بصورت صحیح وارد نمایید")]
        public string Average { get; set; }
        [DisplayName("تاریخ پایان")]
        //[Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(15)]
        public string EndDate { get; set; }
        [DisplayName("نوع دانشگاه")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(30)]
        public string UniversityType { get; set; }
        [DisplayName("نام آموزشگاه")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(60)]
        public string InstitutionName { get; set; }
        [DisplayName("کشور - شهر")]
        //[Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string City_Country { get; set; }

        public virtual Employement.Emp_BaseInformation Emp_BaseInformation { get; set; }
    }
}
