﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DomainClasses.Employement
{
   public class Emp_BaseInformation
    {
        public Emp_BaseInformation()
        {

        }
        [Key]
        public int Employment_ID { get; set; }
        [DisplayName("نام")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string FirstName { get; set; }
        [DisplayName("نام خانوادگی")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(150)]
        public string LastName { get; set; }
        [DisplayName("نام پدر")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string FatherName { get; set; }
        [DisplayName("شماره شناسنامه")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(20)]
        public string Id_Cer { get; set; }
        [DisplayName("تاریخ تولد")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(15)]
        public string BirthDate { get; set; }
        [DisplayName("کد ملی")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(10,ErrorMessage ="کد ملی بیش از 10 کاراکتر مجاز نیست")]
        public string NationalCode { get; set; }
        [DisplayName("تاریخ صدور شناسنامه")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(15)]
        public string TarikhSodorShenasname { get; set; }
        [DisplayName("محل صدور شناسنامه")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string MahaleSodorShenasname { get; set; }
        [DisplayName("محل تولد")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string BirthPlace { get; set; }
        [DisplayName("مذهب")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(30)]
        public string Religion { get; set; }
        [DisplayName("وضعیت تاهل")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(10)]
        public string MaritalStatus { get; set; }
        [DisplayName("برنام سه سال آینده برای ازدواج")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string MaritalDescription { get; set; }
        [DisplayName("برنام سه سال آینده برای داشتن فرزند")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string ChildrenDescription { get; set; }
        [DisplayName("سلامت وضعیت جسمی و روحی")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public bool IsHealth { get; set; }
        [DisplayName("بیماری ها")]
        [MaxLength(100)]
        public string HealthDescription { get; set; }
        [DisplayName("وضعیت نظام وظیفه")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public bool Duty { get; set; }
        [DisplayName("دلیل معافیت")]
        [MaxLength(100)]
        public string ExemptReason { get; set; }
        [DisplayName("تصویر")]
        [MaxLength(100)]
        public string Image { get; set; }
        [DisplayName("سابقه کیفری")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public bool IsCriminalRecord { get; set; } 
        [DisplayName("دلیل سابقه کیفری")]
        [MaxLength(200)]
        //[Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string CriminalRecordReason { get; set; }
        [DisplayName("سیگاری")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public bool IsSmoking { get; set; }
        [DisplayName("دلیل سیکاری")]
        [MaxLength(200)]
        //[Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string SmokingReason { get; set; }
        [DisplayName("خواندن انگلیسی")]
        //[Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public byte EnglishRead { get; set; }
        [DisplayName("نوشتن انگلیسی")]
        public byte EnglishWrite { get; set; }
        [DisplayName("مکالمه انگلیسی")]
        public byte EnglishSpeake { get; set; }
        [DisplayName("درصورت انتخاب شغل")]
        [MaxLength(300)]
        public string SelectedJob { get; set; }
        [DisplayName("نوع همکاری")]
        public byte TypeCooperation { get; set; }
        [DisplayName("ساعات همکاری بصورت پاره وقت")]
        [MaxLength(200)]
        public string TimeCooperation { get; set; }
        [DisplayName("اضافه کاری")]
        public bool IsOvertime { get; set; }
        [DisplayName("تعداد ساعت اضافه کاری")]
        public Int16? OvertimeHours { get; set; }
        [DisplayName("کار در تعطیلات")]
        public bool HolidayIsWork { get; set; }
        [DisplayName("ماموریت داخلی ؟")]
        public bool IsMissionInternal { get; set; }
        [DisplayName("ماموریت خارجی ؟")]
        public bool IsMissionExternal { get; set; }
        [DisplayName("سابقه بیمه ؟")]
        public bool IsInsuranceHistory { get; set; }
        [DisplayName("تعداد سال بیمه و شماره بیمه")]
        [MaxLength(100)]
        public string InsuranceDescription { get; set; }
        [DisplayName("چگونگی آشنایی")]
        [MaxLength(300)]
        public string MethodIntroduction { get; set; }
        [DisplayName("موارد خاص جسمی و روحی")]
        public bool IsSpecialPhysicalItems { get; set; }
        [DisplayName("موارد خاص جسمی و روحی")]
        [MaxLength(500)]
        public string SpecialPhysicalItemsDescription { get; set; }
        [DisplayName("مشغول به كار")]
        public bool IsWorking { get; set; }
        [DisplayName("نحوه تضمین کاری")]
        [MaxLength(100)]
        public string WorkGuarantee { get; set; }
        [DisplayName("حقوق مورد انتظار")]
        [MaxLength(50)]
        public string ExpectedSalary { get; set; }
        [DisplayName("نوع حقوق")]
        public byte SalaryType { get; set; }
        [DisplayName("سرپرست خانواده")]
        public bool IsHeadFamily { get; set; }
        [DisplayName("نوع منزل")]
        public byte HomeType { get; set; }
        [DisplayName("نوع منزل ")]
        [MaxLength(200)]
        //[Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string HomeDescription { get; set; }
        [DisplayName("آدرس")]
        [MaxLength(400)]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string Address { get; set; }
        [DisplayName("تلفن")]
        [MaxLength(15)]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string Tell { get; set; }
        [DisplayName("تلفن همراه")]
        [MaxLength(15)]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string Mobile { get; set; }

        public virtual List<Employement.Emp_EducationalRecords> Emp_EducationalRecords { get; set; }
        public virtual List<Employement.Emp_WorkExperience> Emp_WorkExperience { get; set; }
        public virtual List<Employement.Emp_TraningCourse> Emp_TraningCourse { get; set; }
        public virtual List<Employement.Emp_Guarantor> Emp_Guarantor { get; set; }
        public virtual List<Employement.Emp_UnderSupervisor> Emp_UnderSupervisor { get; set; }
        public virtual List<Employement.Emp_Documents> Emp_Documents { get; set; }
    }
}
