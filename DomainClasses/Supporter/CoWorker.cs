﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DomainClasses.Supporter
{
    public class CoWorker
    {
        public CoWorker()
        {

        }
        [Key]
        public Int16 CoWorker_ID { get; set; }
        [DisplayName("توضیحات")]
        [MaxLength(100)]
        public string Title { get; set; }
        [DisplayName("عنوان")]
        [MaxLength(100)]
        public string Link { get; set; }
        [DisplayName("تصویر")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string Photo { get; set; }
        [DisplayName("گواهینامه / افتخار")]
        public bool Type { get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }
    }
}
