﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DomainClasses.Supporter
{
    public  class Customers
    {
        public Customers()
        {

        }
        [Key]
        public Int16 SCustomer_ID { get; set; }
        [DisplayName("عنوان")]
        [MaxLength(100)]
        public string Title { get; set; }
        [DisplayName("لینک")]
        [MaxLength(100)]
        public string Link { get; set; }
        [DisplayName("تصویر")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string Photo { get; set; }

        [DisplayName("فعال /غیرفعال")]
        public bool IsActive { get; set; }
    }
}
