﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace DomainClasses.BasicInformation
{
    public class ActivityField
    {
        public ActivityField() { }

        [Key]
        public Int16 ActivityFieldID { get; set; }

        [DisplayName("عنوان زمینه فعالیت")]
        [Required(ErrorMessage = "زمینه فعالیت را درج نمایید")]
        [MaxLength(50, ErrorMessage = "زمینه فعالیت نمی تواند بیش از 50 کاراکتر باشد")]
        public string ActivityFieldTitle { get; set; }

        [DisplayName("توضیحات")]
        [Required(ErrorMessage = "توضیحات را درج نمایید")]
        [MaxLength(500, ErrorMessage = "توضیحات نمی تواند بیش از 500 کاراکتر باشد")]
        public string ActivityFieldDescription { get; set; }

        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }
        public virtual List<Activity> Activities { get; set; }
    }
}
