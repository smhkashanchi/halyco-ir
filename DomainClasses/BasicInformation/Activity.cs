﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using DomainClasses.Suppliers;

namespace DomainClasses.BasicInformation
{
    public class Activity
    {
        public Activity() { }

        [Key]
        public Int16 Activity_ID { get; set; }

        [DisplayName("عنوان فعالیت")]
        [Required(ErrorMessage = "عنوان فعالیت را درج نمایید")]
        [MaxLength(50, ErrorMessage = "عنوان فعالیت نمی تواند بیش از 50 کاراکتر باشد")]
        public string ActivityTitle { get; set; }

        [DisplayName("توضیحات")]
        [Required(ErrorMessage = "توضیحات را درج نمایید")]
        [MaxLength(500, ErrorMessage = "توضیحات نمی تواند بیش از 500 کاراکتر باشد")]
        public string ActivityDescription { get; set; }

        [DisplayName("زمینه فعالیت")]
        [ForeignKey("ActivityField")]
        public Int16 ActivityFieldID { get; set; }

        public virtual ActivityField ActivityField { get; set; }
        //public virtual List<SuppliersActivityFields> SuppliersActivityFields{ get; set; }
    }
}
