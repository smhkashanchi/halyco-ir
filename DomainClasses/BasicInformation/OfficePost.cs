﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using DomainClasses.Employees;
using DomainClasses.Proposal;

namespace DomainClasses.BasicInformation
{
   public class OfficePost
    {
        public OfficePost() { }

        [Key]
        public Int16 OfficePostID { get; set; }

        [DisplayName("عنوان سمت")]
        [Required(ErrorMessage = "عنوان سمت را درج نمایید")]
        [MaxLength(50, ErrorMessage = "عنوان نمی تواند بیش از 50 کاراکتر باشد")]
        public string OfficePostTitle { get; set; }

        [DisplayName("نقش های سمت")]
        [Required(ErrorMessage = "نقش های سمت را درج نمایید")]
        [MaxLength(500, ErrorMessage = "نقش های سمت نمی تواند بیش از 500 کاراکتر باشد")]
        public string OfficePostRoles { get; set; }

        [DisplayName("مسئولیت های سمت")]
        [Required(ErrorMessage = "مسئولیت های سمت را درج نمایید")]
        [MaxLength(500, ErrorMessage = "مسئولیت های سمت نمی تواند بیش از 500 کاراکتر باشد")]
        public string OfficePostResponsibility { get; set; }
        [DisplayName("پدر")]
        [ForeignKey("OfficePostId")]
        public Int16? ParentId { get; set; }

        public virtual List<Employee> Employees { get; set; }
        public virtual OfficePost OfficePostId { get; set; }
        public virtual List<Employe_Post> Employe_Posts{ get; set; }
        public virtual List<ProposalProviders> ProposalProviders{ get; set; }
    }
}
