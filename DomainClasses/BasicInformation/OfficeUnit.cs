﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using DomainClasses.Employees;
using DomainClasses.Proposal;

namespace DomainClasses.BasicInformation
{
   public class OfficeUnit
    {
        public OfficeUnit() { }

        [Key]
        public Int16 OfficeUnitID { get; set; }

        [DisplayName("عنوان واحد")]
        [Required(ErrorMessage ="عنوان واحد را درج نمایید")]
        [MaxLength(50,ErrorMessage ="عنوان نمی تواند بیش از 50 کاراکتر باشد")]
        public string OfficeUnitTitle { get; set; }

        [DisplayName("وظایف واحد")]
        //[Required(ErrorMessage = "وظایف واحد را درج نمایید")]
        [MaxLength(500, ErrorMessage = "وظایف واحد نمی تواند بیش از 500 کاراکتر باشد")]
        public string OfficeUnitTasks { get; set; }

        [DisplayName("مسئولیت های واحد")]
        //[Required(ErrorMessage = "مسئولیت های واحد را درج نمایید")]
        [MaxLength(500, ErrorMessage = "مسئولیت های واحد نمی تواند بیش از 500 کاراکتر باشد")]
        public string OfficeUnitResponsibility { get; set; }

        public virtual List<Employee> Employees { get; set; }
        public virtual List<ProposalProviders> ProposalProviders{ get; set; }

        [InverseProperty("OfficeUnit1")]
        public virtual List<ShiftChange> ShiftChanges11 { get; set; }

       
    }

    
}
