﻿using DomainClasses.Survey;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Product
{
   public class ProductInfo
    {
        public ProductInfo()
        {

        }
        [Key]
        public int ProductInfo_ID { get; set; }
        [ForeignKey("MainProduct")]
        public int ProductId { get; set; }
        [DisplayName("عنوان محصول")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(500)]
        public string ProductTitle { get; set; }
        [DisplayName("خلاصه محصول")]
        [MaxLength(100,ErrorMessage ="خلاصه نمی تواند بیش از 100 کاراکتر باشد")]
        public string ProductSummary { get; set; }
        [ForeignKey("ProductsGroup")] ////////////////////////////////////////////Group
        [DisplayName("گروه محصول")]
        public Int16? ProductGroupId { get; set; }
        [DisplayName("توضیحات محصول")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        //[Allowhtml]
        public string Description { get; set; }
        [ForeignKey("Unit")] ////////////////////////////////////////////Unit
        [DisplayName("واحد")]
        public Int16? UnitId { get; set; }
        [MaxLength(500)]
        public string MetaTitle { get; set; }
        [MaxLength(500)]
        public string MetaKeyword { get; set; }
        [MaxLength(500)]
        public string MetaDescription { get; set; }
        [MaxLength(500)]
        public string MetaOther { get; set; }
        [DisplayName("تخفیف")]
        [ForeignKey("Off")] ////////////////////////////Off
        public Int16? OffId { get; set; }
        [DisplayName("تولید کننده")]
        [ForeignKey("Producer")] ////////////////////////////producer
        public Int16? ProducerId { get; set; }
        [DisplayName("کلمات کلیدی")]
        [MaxLength(400)]
        public string Tags { get; set; }
        [DisplayName("اسلاگ")]
        [MaxLength(500)]
        public string Slug { get; set; }

        public virtual MainProduct MainProduct { get; set; }
        public virtual ProductsGroup ProductsGroup { get; set; }
        public virtual Unit Unit { get; set; }
        public virtual Off Off { get; set; }
        public virtual Producer Producer { get; set; }
        
        public virtual List<ProductColor> ProductColor { get; set; }

        //[InverseProperty("ProductInfo_1")]
        //public virtual List<SimilarProduct> Products { get; set; }
        //[InverseProperty("ProductInfo_2")]
        public virtual List<SimilarProduct> SimilarProduct { get; set; }
        public virtual List<ProductFeature> ProductFeatures { get; set; }

        public List<CustomerSurveyProducts> CustomerSurveyProducts { get; set; }
    }
}

