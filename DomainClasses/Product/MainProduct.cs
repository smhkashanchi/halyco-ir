﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Product
{
    public class MainProduct
    {
        public MainProduct()
        {

        }
        [Key]
        public int Product_ID { get; set; } 
        [DisplayName("تصویر")]
        [MaxLength(150)]
        public string ProductImage { get; set; }
        [DisplayName("گالری")]
        [ForeignKey("Gallery")] ////////////////////////////////////////Galllery
        public Int16? GalleryId { get; set; }
        [DisplayName("کد محصول")]
        [MaxLength(100)]
        public string ProductCode { get; set; }
        [DisplayName("محبوب ترین ؟")]
        public bool IsPopular { get; set; }
        [DisplayName("تاریخ ثبت")]
        public DateTime CreateDate { get; set; }
        [DisplayName("موجود ؟")]
        public bool IsExsit { get; set; }
        [DisplayName("تعداد موجودی")]
        public int? ExistCount { get; set; }
        [DisplayName("در دکور")]
        public bool IsDecor { get; set; }
        [DisplayName("تعداد فروش")]
        public int? SaledCount { get; set; }
        [DisplayName("فعال / غیر فعال")]
        public bool IsActive { get; set; }

        [DisplayName("کاتالوگ")]
        [MaxLength(150)]
        public string ProductCatalog { get; set; }

        //Relation For Gallery
        public virtual List<ProductInfo> ProductInfos { get; set; }
        public virtual Gallery.Gallery Gallery { get; set; }
    }
}
