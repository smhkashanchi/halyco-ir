﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Product
{
   public class Feature
    {
        public Feature()
        {

        }
        [Key]
        public int Feature_ID { get; set; }

        [ForeignKey("ProductsGroup")] ////////////////////////////////////////////Group
        [DisplayName("گروه محصول")]
        public Int16? ProductGroupId { get; set; }

        [DisplayName("عنوان ویژگی")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(200)]

        public string FeatureTitle { get; set; }


        
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }

        public virtual Product.ProductsGroup ProductsGroup { get; set; }
        public virtual List<FeatureReply> FeatureReplies { get; set; }
    }
}
