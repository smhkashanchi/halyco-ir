﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Product
{
   public class Color
    {
        public Color()
        {

        }
        [Key]
        public Int16 Color_ID { get; set; }
        [DisplayName("عنوان رنگ")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string ColorTitle { get; set; }
        [DisplayName("کد رنگ")]
        [MaxLength(50)]
        public string ColorCode { get; set; }

        [Column(TypeName = "varchar(8)")]
        [ForeignKey("Language")]
        public string Lang { get; set; }
        public virtual Language.Language Language { get; set; }
    }
}
