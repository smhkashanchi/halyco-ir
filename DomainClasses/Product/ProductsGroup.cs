﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Product
{
   public class ProductsGroup
    {
        public ProductsGroup()
        {

        }
        [Key]
        public Int16 ProductGroup_ID { get; set; }
        [DisplayName("عنوان گروه")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(300)]
        public string ProductGroupTitle { get; set; }
        [DisplayName("توضیحات")]
        [MaxLength(500)]
        public string Description { get; set; }
        [DisplayName("تصویر")]
        [MaxLength(100)]
        [Required(ErrorMessage ="لطفا {0} را انتخاب نمایید")]

        public string ProductGroupImage { get; set; }
        [DisplayName("تخفیف")]
        [ForeignKey("Off")]
        public Int16? OffId { get; set; }

        [Column(TypeName = "varchar(8)")]
        [ForeignKey("Language")]
        public string Lang { get; set; }

        [ForeignKey("ProductsGroups")]
        public Int16? ParentId { get; set; }
        public bool IsParent { get; set; }
        [DisplayName("عرض عکس بزرگ")]
        public Int16? PicLargeWidth { get; set; }
        [DisplayName("عرض عکس کوچک")]
        public Int16? PicSmallWidth { get; set; }
        [DisplayName("ارتفاع عکس بزرگ")]
        public Int16? PicLargeHeight { get; set; } 
        [DisplayName("ارتفاع عکس کوچک")]
        public Int16? PicSmallHeight { get; set; }

        public ProductsGroup ProductsGroups { get; set; }
        public virtual Language.Language Language { get; set; }
        public virtual Off Off { get; set; }
    }
}
