﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DomainClasses.Cart;

namespace DomainClasses.Product
{
    public class Size
    {
        public Size()
        {

        }

        [Key]
        public Int16 SizeID { get; set; }

        [DisplayName("عنوان اندازه")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string SizeTitle { get; set; }

    //    public virtual List<ZarProduct> ZarProducts { get; set; }

        public virtual List<FactorDetails> FactorDetails { get; set; }
    }
}
