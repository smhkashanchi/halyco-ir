﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Product
{
    public class ProductGaranty
    {
        public ProductGaranty()
        {

        }
        [Key]
        [DisplayName("محصول")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [ForeignKey("ProductInfo")]

        public int ProductInfoId { get; set; }

        [Key]
        [DisplayName("گارانتی")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [ForeignKey("Garanty")]
        public Int16 GarantyId { get; set; }

        [DisplayName("قیمت گارانتی")]
        public int? GarantyPrice { get; set; }

        public virtual ProductInfo ProductInfo { get; set; }

        public virtual Garanty Garanty { get; set; }
        
    }
}
