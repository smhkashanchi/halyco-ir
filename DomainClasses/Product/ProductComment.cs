﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Product
{
    public class ProductComment
    {
        public ProductComment()
        {

        }
        [Key]
        public int Comment_ID { get; set; }

        [DisplayName("عنوان محصول")]
        [ForeignKey("ProductInfo")]
        public int? ProductInforId { get; set; }
        [DisplayName("گروه محصول")]
        [ForeignKey("ProductsGroup")]
        public Int16 ProductGroupId { get; set; }
        [DisplayName("متن پیام")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string Comment { get; set; }
        [DisplayName("جواب پیام")]
        public string CommentReply { get; set; }
        [DisplayName("نام کاربری")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string UserName { get; set; }
        [DisplayName("ایمیل")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        [EmailAddress(ErrorMessage = "لطفا {0} را بصورت صحیح وارد نمایید")]
        public string Email { get; set; }
        [DisplayName("جدید است؟")]
        public bool IsNew { get; set; }
        [DisplayName("تاریخ")]
        public DateTime CommentDate { get; set; }
        [DisplayName("امتیاز مثبت")]
        public byte? PositiveScore { get; set; }
        [DisplayName("امتیاز منفی")]
        public byte? NegativeScore { get; set; }
        [DisplayName("تایید / عدم تایید")]
        public bool IsConfirmed { get; set; }

        public virtual ProductInfo ProductInfo { get; set; }
        public virtual ProductsGroup ProductsGroup { get; set; }
    }
}
