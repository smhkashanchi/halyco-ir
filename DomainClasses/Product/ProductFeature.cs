﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Product
{
    public class ProductFeature
    {
        public ProductFeature()
        {

        }
        [Key]
        [DisplayName("محصول")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [ForeignKey("ProductInfo")]
        public int ProductInfoId { get; set; }
        [Key]
        [DisplayName("ویژگی")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [ForeignKey("FeatureReply")]
        public int FeatureReplyId { get; set; }

        public virtual ProductInfo ProductInfo { get; set; }
        public virtual FeatureReply FeatureReply { get; set; }
    }
}
