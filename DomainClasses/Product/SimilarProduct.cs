﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Product
{
    public class SimilarProduct
    {
        public SimilarProduct()
        {

        }
        
        public int ID { get; set; }
        public int? ProductID { get; set; }
        public int? SimilarProduct_ID { get; set; }


        //[ForeignKey("ProductID")]
        //[InverseProperty("Products")]
        public virtual  ProductInfo ProductInfo { get; set; }
        //[ForeignKey("SimilarProduct_ID")]
        //[InverseProperty("SimilarProduct")]
        public virtual ProductInfo SimilarProducts { get; set; }
    }
}
