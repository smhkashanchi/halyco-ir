﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Product
{
    public class ProductAdvantage
    {
        public ProductAdvantage()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int16 Advantage_ID { get; set; }
        [DisplayName("محصول")]
        [ForeignKey("ProductInfo")]
        public int ProductInfoId { get; set; }
        [DisplayName("عنوان")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(500)]
        public string AdvantageTitle { get; set; }
        [DisplayName("مزیت / عیب")]
        public bool AdvantageType { get; set; }

        public virtual ProductInfo ProductInfo { get; set; }
    }
}
