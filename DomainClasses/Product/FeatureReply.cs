﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Product
{
    public class FeatureReply
    {
        public FeatureReply()
        {

        }
        [Key]
        public int FeatureReply_ID { get; set; }
        [ForeignKey("Feature")]
        [DisplayName("ویژگی")]
        public int FeatureId { get; set; }
        [DisplayName("جواب ویژگی")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(200)]
        public string FeatureReplyText { get; set; }
        [DisplayName("جواب پیش فرض")]
        public bool IsDefault { get; set; }

        public virtual Feature Feature { get; set; }
    }
}
