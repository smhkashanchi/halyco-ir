﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Product
{
    public class Unit
    {
        public Unit()
        {

        }
        [Key]
        public Int16 Unit_ID { get; set; }
        [DisplayName("عنوان واحد")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string UnitTitle { get; set; }

        [Column(TypeName = "varchar(8)")]
        [ForeignKey("Language")]
        public string Lang { get; set; }

        public virtual Language.Language Language { get; set; }
    }
}
