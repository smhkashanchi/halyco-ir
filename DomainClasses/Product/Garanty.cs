﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Product
{
    public class Garanty
    {
        public Garanty()
        {

        }
        [Key]
        public Int16 Garanty_ID { get; set; }
        [DisplayName("نام گارانتی")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(150)]
        public string GarantyName { get; set; }
        [DisplayName("تصویر")]
        [MaxLength(100)]
        public string Icon { get; set; }
        [DisplayName("توضیحات")]
        public string Description { get; set; }
        [Column(TypeName = "varchar(8)")]
        [ForeignKey("Language")]
        public string Lang { get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }

        public virtual Language.Language Language { get; set; }
    }
}
