﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Product
{
    public class ProductColor
    {
        public ProductColor()
        {

        }

        [Key]
        public int ProductColor_ID { get; set; }
        [DisplayName("محصول")]
        [ForeignKey("ProductInfo")]
        public int ProductId { get; set; }
        [DisplayName("رنگ")]
        [ForeignKey("Color")]
        public Int16 ColorId { get; set; }
        [DisplayName("قیمت محصول")]
        public int? ProductPrice { get; set; }
        [DisplayName("تعداد محصول")]
        public int? ProductCount { get; set; }

        public virtual ProductInfo ProductInfo{ get; set; }
        public virtual Color Color { get; set; }
    }
}
