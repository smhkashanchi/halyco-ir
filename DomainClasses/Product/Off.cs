﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Product
{
   public class Off
    {
        public Off()
        {

        }

        [Key]
        public Int16 Off_ID { get; set; }
        [DisplayName("عنوان تخفیف")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(200)]
        public string OffTitle { get; set; }
        [DisplayName("تاریخ شروع")]
        public DateTime StartDate { get; set; }
        [DisplayName("تاریخ پایان")]
        public DateTime EndDate { get; set; }
        [DisplayName("تخفیف")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public byte Price { get; set; }

        [Column(TypeName = "varchar(8)")]
        [ForeignKey("Language")]
        public string Lang { get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }

        public virtual Language.Language Language { get; set; }
    }
}
