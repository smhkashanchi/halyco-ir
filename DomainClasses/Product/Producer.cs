﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Product
{
   public class Producer
    {
        public Producer()
        {

        }
        [Key]
        public Int16 Producer_ID { get; set; }
        [DisplayName("نام تولیدکننده")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string ProducerTitle { get; set; }
        [DisplayName("توضیحات")]
        [MaxLength(500)]
        public string Description { get; set; }
        [DisplayName("تصویر")]
        [MaxLength(100)]
        public string Icon { get; set; }

        [Column(TypeName = "varchar(8)")]
        [ForeignKey("Language")]
        public string Lang { get; set; }
        public virtual Language.Language Language { get; set; }
    }
}
