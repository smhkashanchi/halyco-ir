﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Seo
{
    public class SeoPageProduct
    {
        public SeoPageProduct()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int16 SeoPage_ID { get; set; }
        public int PageId { get; set; }
        [DisplayName("آدرس صفحه")]
        [MaxLength(300)]
        //[Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        public string UrlPage { get; set; }
        [DisplayName("عنوان")]
        [MaxLength(200)]
        //[Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string MetaTitle { get; set; }
        [MaxLength(300)]
        [DisplayName("کلمات کلیدی")]
        public string MetaKeyword { get; set; }
        [MaxLength(500)]
        [DisplayName("توضیحات")]
        public string MetaDescription { get; set; }
        [MaxLength(500)]
        public string MetaOther { get; set; }
    }
}
