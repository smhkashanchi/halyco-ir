﻿using DomainClasses.Customer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Region
{
    public class State
    {
        public State()
        {

        }
        [Key]
        public int State_ID { get; set; }
        [DisplayName("نام استان")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string StateName { get; set; }
        [DisplayName("کد استان")]
        [Column(TypeName = "varchar(10)")]
        public string StateCode { get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }
        [DisplayName("کشور")]
        [ForeignKey("Country")]
        public int CountryId { get; set; }

        public virtual Country Country { get; set; }
        public virtual List<City> Cities { get; set; }
        public virtual IEnumerable<CustomerAddress> CustomerAddresses { get; set; }
    }
}
