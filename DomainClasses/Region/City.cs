﻿using DomainClasses.Customer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Region
{
   public class City
    {
        public City()
        {

        }
        [Key]
        public int City_ID { get; set; }
        [DisplayName("نام شهر")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string CityName { get; set; }
        [DisplayName("کد شهر")]
        [Column(TypeName = "varchar(10)")]
        public string CityCode { get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }
        [DisplayName("استان")]
        public virtual State State { get; set; }
        [DisplayName("استان")]

        public int State_ID { get; set; }

        public virtual IEnumerable<CustomerAddress> CustomerAddresses { get; set; }
    }
}
