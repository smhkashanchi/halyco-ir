﻿using DomainClasses.Customer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Region
{
    public class Country
    {
        public Country()
        {

        }
        [Key]
        public int Country_ID { get; set; }
        [DisplayName("نام کشور")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string CountryName { get; set; }
        [DisplayName("کد کشور")]
        [Column(TypeName = "varchar(10)")]
        public string CountryCode { get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }
        [Column(TypeName = "varchar(8)")]
        [ForeignKey("Language")]
        public string Lang { get; set; }

        public virtual Language.Language Language { get; set; }
        public virtual List<State> States { get; set; }
        public virtual IEnumerable<CustomerAddress> CustomerAddresses { get; set; }

    }
}
