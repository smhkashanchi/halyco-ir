﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DomainClasses.SMS_Subscription
{
   public class Persons
    {
        public Persons()
        {

        }
        [Key]
        public int ID { get; set; }
        [DisplayName("شماره موبایل")]
        [MaxLength(15)]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید.")]
        public string MobileNumber { get; set; }
    }
}
