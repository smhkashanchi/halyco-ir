﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.SocialNetwork
{
    public class SocialNetworks
    {
        public SocialNetworks()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte ID { get; set; }
        [DisplayName("عنوان شبکه اجتماعی")]
        [MaxLength(50)]
        public string Title { get; set; }
        [DisplayName("لینک ")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(200)]
        public string Link { get; set; }
        [DisplayName("نوع شبکه اجتماعی")]
        public byte Type { get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }
    }
}
