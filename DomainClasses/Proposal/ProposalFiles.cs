﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Proposal
{
    public class ProposalFiles
    {
        public ProposalFiles()
        {

        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Int16 P_File_ID { get; set; }
        [DisplayName("پیشنهاد")]
        [ForeignKey("MainProposal")]
        public Int16 ProposalId { get; set; }
        [DisplayName("فایل")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(60)]
        public string P_FileName { get; set; }
        [DisplayName("عنوان مستند مرتبط")]
        [MaxLength(50)]
        public string Title { get; set; }

        public virtual MainProposal MainProposal { get; set; }
    }
}
