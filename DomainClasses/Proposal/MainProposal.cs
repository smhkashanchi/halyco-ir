﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DomainClasses.Proposal
{
    public class MainProposal
    {
        public MainProposal()
        {

        }
        [Key]
        public Int16 Proposal_ID { get; set; }
        [DisplayName("عنوان")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(200)]
        public string Title { get; set; }
        [DisplayName("تاریخ پیشنهاد")]
        [MaxLength(15)]
        public string ProposalDate { get; set; }
        [DisplayName("شرح پیشنهاد")]
        [MaxLength(900)]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        public string ProposalDescription { get; set; }
        [DisplayName("وضعیت جاری")]
        [MaxLength(100)]
        public string CurrentStatus { get; set; }
        [DisplayName("روش اجرا")]
        [MaxLength(500)]
        public string ImplementMethod { get; set; }
        [DisplayName("امکانات مورد نیاز")]
        [MaxLength(200)]
        public string Possibilities { get; set; }
        [DisplayName("میزان صرفه جویی مالی")]
        [MaxLength(10)]
        public string FinancialSavings { get; set; }
        [DisplayName("هزینه اجرا")]
        public int? ImplementPrice { get; set; }

        public virtual List<ProposalProviders> ProposalProviders { get; set; }
        //public virtual List<ProposalFiles> ProposalFiles{ get; set; }

    }
}
