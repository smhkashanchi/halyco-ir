﻿using DomainClasses.BasicInformation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Proposal
{
   public class ProposalProviders
    {
        public ProposalProviders()
        {

        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PP_ID { get; set; }

        [DisplayName("کد پرسنلی")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(10,ErrorMessage ="کد پرسنلی نمی تواند بیش از 10 رقم باشد")]
        [ForeignKey("Employee")]
        public string PersonalCode { get; set; }

        [DisplayName("پیشنهاد اصلی")]
        [ForeignKey("MainProposal")]
        public Int16 ProposalId { get; set; }

        [DisplayName("واحد")]
        [Required(ErrorMessage = "لطفا {0} را انتخاب نمایید")]
        [ForeignKey("OfficeUnit")]
        public Int16 UnitId { get; set; }

        [DisplayName("سمت")]
        [Required(ErrorMessage = "لطفا {0} را انتخاب نمایید")]
        [ForeignKey("OfficePost")]
        public Int16 PostId { get; set; }
        [DisplayName("تلفن همراه")]
        [MaxLength(12)]
        public string PhoneNumber { get; set; }

        public virtual Employees.Employee Employee { get; set; }
        public virtual MainProposal MainProposal { get; set; }
        public virtual OfficeUnit OfficeUnit { get; set; }
        public virtual OfficePost OfficePost { get; set; }
    }
}
