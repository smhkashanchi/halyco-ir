﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Survey
{
    public class SurveyCustomerOtherData
    {
        public SurveyCustomerOtherData()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int16 C_OtherData_ID { get; set; }

        [DisplayName("نظرسنجی اصلی")]
        [ForeignKey("MainSurvey")]
        public byte MainSurveyId { get; set; }

        //[DisplayName("محصول")]
        //[ForeignKey("ProductInfo")]
        //public int ProductId { get; set; }
        [DisplayName("نام و نام خانوادگی")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string FullName { get; set; }

        [DisplayName("نام شرکت")]
        [MaxLength(50)]
        public string CompanyName { get; set; }

        [DisplayName("سمت")]
        [MaxLength(50)]
        public string OfficePost { get; set; }

        [DisplayName("تلفن همراه")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(15)]
        public string PhoneNumber { get; set; }
        
        [DisplayName("تاریخ تکمیل فرم")]
        public DateTime CompeletFromDate { get; set; }

        [DisplayName("پیشنهادات")]
        [MaxLength(200)]
        public string Proposal { get; set; }

        public virtual MainSurvey MainSurvey { get; set; }
        //public virtual Product.ProductInfo ProductInfo { get; set; }

        public List<CustomerSurveyProducts> CustomerSurveyProducts{ get; set; }

    }
}
