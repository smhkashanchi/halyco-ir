﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Survey
{
    public class CustomerSurveyProducts
    {
        public CustomerSurveyProducts()
        {

        }

        [DisplayName("محصول")]
        [ForeignKey("ProductInfo")]
        public int ProductId { get; set; }
        [DisplayName("نظرسنجی مشتری")]
        [ForeignKey("SurveyCustomerOtherData")]
        public Int16 CustomerSurveyId { get; set; }

        public virtual Product.ProductInfo ProductInfo { get; set; }
        public virtual SurveyCustomerOtherData SurveyCustomerOtherData { get; set; }
    }
}
