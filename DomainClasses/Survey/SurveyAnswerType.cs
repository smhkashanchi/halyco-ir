﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Survey
{
    public class SurveyAnswerType
    {
        public SurveyAnswerType()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int16 AnswerType_ID { get; set; }
       
        [DisplayName("عنوان")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(300)]
        public string Title { get; set; }
        [DisplayName("عنوان گزینه اول")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string OptionTitle1 { get; set; }
        [DisplayName("عنوان گزینه دوم")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string OptionTitle2 { get; set; }
        [DisplayName("عنوان گزینه سوم")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string OptionTitle3 { get; set; }
        [DisplayName("عنوان گزینه چهارم")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string OptionTitle4 { get; set; }
        [DisplayName("عنوان گزینه پنجم")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string OptionTitle5 { get; set; }


    }
}
