﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Survey
{
   public class SurveyQuestionReply
    {
        public SurveyQuestionReply()
        {

        }
        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        //public int QuestionReply_ID { get; set; }
        [DisplayName("سوال")]
        [ForeignKey("SurveyQuestion")]
        public Int16 QuestionId { get; set; }
        [DisplayName("جواب انتخاب شده")]
        public byte SelectedReply { get; set; }
        [DisplayName("شناسه جدول اطلاعات اضافی")]
        public int ExtraInformationId { get; set; }

        public virtual SurveyQuestion SurveyQuestion { get; set; }
    }
}
