﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Survey
{
    public class SurveySuppliersOtherData
    {
        public SurveySuppliersOtherData()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int16 S_OtherData_ID { get; set; }
        [DisplayName("نظرسنجی اصلی")]
        [ForeignKey("MainSurvey")]
        public byte MainSurveyId { get; set; }
        [DisplayName("نام تامین کننده")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(400)]
        public string FullName { get; set; }
        [DisplayName("شرکت")]
        [MaxLength(50)]
        public string Company { get; set; }
        [DisplayName("پیشنهاد")]
        [MaxLength(450)]
        public string Proposal { get; set; }
        [DisplayName("تاریخ تکمیل فرم")]
        public DateTime CompleteFormDate { get; set; }

        public virtual MainSurvey MainSurvey { get; set; }

    }
}
