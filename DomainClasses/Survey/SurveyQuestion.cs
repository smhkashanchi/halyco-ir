﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Survey
{
    public class SurveyQuestion
    {
        public SurveyQuestion()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int16 Question_ID { get; set; }
        [DisplayName("گروه سوال")]
        [ForeignKey("SurvayQuestionGroup")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public Int16 QuestionGroupId { get; set; }
        [DisplayName("نوع جواب")]
        [ForeignKey("SurveyAnswerType")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public Int16 AnswerTypeId { get; set; }
        [DisplayName("سوال")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(500)]
        public string Question { get; set; }
        [DisplayName("تعداد پاسخ ها")]
        public int ReplyCount { get; set; }
        [DisplayName("امتیاز گزینه 1")]
        public int OptionVoteCount1 { get; set; }
        [DisplayName(" امتیاز گزینه 2")]
        public int OptionVoteCount2 { get; set; } 
        [DisplayName(" امتیاز گزینه 3")]
        public int OptionVoteCount3 { get; set; }
        [DisplayName(" امتیاز گزینه 4")]
        public int OptionVoteCount4 { get; set; }
        [DisplayName(" امتیاز گزینه 5")]
        public int OptionVoteCount5 { get; set; }

        public virtual SurvayQuestionGroup SurvayQuestionGroup { get; set; }
        public virtual SurveyAnswerType SurveyAnswerType { get; set; }
        public virtual List<SurveyQuestionReply> SurveyQuestionReplies { get; set; }
    }
}
