﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Survey
{
    public class SurvayQuestionGroup
    {
        public SurvayQuestionGroup()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int16 SurveyGroup_ID { get; set; }
        [DisplayName("نظرسنجی اصلی")]
        [ForeignKey("MainSurvey")]
        public byte MainSurveyId { get; set; }
        [DisplayName("عنوان")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string Title { get; set; }

        public virtual MainSurvey MainSurvey { get; set; }
        public virtual List<SurveyQuestion> SurveyQuestions{ get; set; }
    }
}
