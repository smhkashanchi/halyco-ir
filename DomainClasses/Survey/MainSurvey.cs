﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Survey
{
    public class MainSurvey
    {
        public MainSurvey()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte MainSurvey_ID { get; set; }
        [DisplayName("نوع نظرسنجی")]
        public byte SurveyType { get; set; }
        [DisplayName("عنوان")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string Title { get; set; }
        [DisplayName("تاریخ ایجاد")]
        public DateTime CreateDate { get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }

        public virtual List<SurvayQuestionGroup> SurvayQuestionGroups { get; set; }
        public virtual List<SurveySuppliersOtherData> SurveySuppliersOtherDatas{ get; set; }
        public virtual List<SurveyCustomerOtherData> SurveyCustomerOtherDatas{ get; set; }
    }
}
