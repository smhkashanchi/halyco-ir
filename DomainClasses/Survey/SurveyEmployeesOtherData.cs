﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Survey
{
    public class SurveyEmployeesOtherData
    {
        public SurveyEmployeesOtherData()
        {

        }
        
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int16 SE_ID { get; set; }
        [DisplayName("کارمند")]
        [ForeignKey("Employee")]
        public string PersonalCode { get; set; }
        [DisplayName("نظرسنجی اصلی")]
        [ForeignKey("MainSurvey")]
        public byte MainSurveyId { get; set; }
        [DisplayName("تاریخ تکمیل")]
        public DateTime CompleteFormDate { get; set; }
        [DisplayName("توضیحات")]
        [MaxLength(200)]
        public string Description { get; set; }

        public Employees.Employee Employee { get; set; }
        public MainSurvey MainSurvey { get; set; }
    }
}
