﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Gallery
{
   public class GalleryGroup
    {
        public GalleryGroup()
        {

        }
        [Key]
        [DisplayName("شناسه گروه")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public byte GalleryGroup_ID { get; set; }

        [DisplayName("عنوان گروه گالری")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(150)]
        public string GalleryGroupTitle { get; set; }

        [Column(TypeName = "varchar(8)")]
        [ForeignKey("Language")]
        public string Lang { get; set; }

        public virtual Language.Language Language { get; set; }
        public virtual List<Gallery> Galleries { get; set; }

    }
}
