﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Gallery
{
   public class Gallery
    {
        public Gallery()
        {

        }
        [Key]
        public Int16 Gallery_ID { get; set; }

        [DisplayName("گروه گالری")]
        [ForeignKey("GalleryGroup")]
        public byte GalleryGroupId { get; set; }

        [DisplayName("عنوان گالری")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(200)]
        public string GalleryTitle { get; set; }

        [DisplayName("تصویر گالری")]
        [MaxLength(150)]
        public string GalleryImage { get; set; }

        [DisplayName("فعال/غیرفعال")]
        public bool IsActive { get; set; }

        public virtual GalleryGroup GalleryGroup { get; set; }
        public virtual List<GalleryPicture> GalleryPictures { get; set; }

    }
}
