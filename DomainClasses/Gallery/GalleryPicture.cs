﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Gallery
{
    public class GalleryPicture
    {
        public GalleryPicture()
        {

        }
        [DisplayName("نام گالری")]
        [ForeignKey("Gallery")]
        public Int16 GalleryId { get; set; }

        [DisplayName("عنوان تصویر")]
        [MaxLength(150)]
        public string GalleryPicTitle { get; set; }
        [Key]
        [DisplayName("تصاویر")]
        [MaxLength(200)]
        public string ImageNames { get; set; }

        public virtual Gallery Gallery { get; set; }
    }
}
