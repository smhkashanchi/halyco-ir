﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.FAQ
{
   public class FAQ
    {
        public FAQ()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte FAQ_ID { get; set; }

        [DisplayName("سوال")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(500)]
        public string FAQuestion { get; set; }

        [DisplayName("جواب")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [DataType(DataType.MultilineText)]
        public string FAQAnswer { get; set; }

        [Column(TypeName = "varchar(8)")]
        [ForeignKey("Language")]
        public string Lnag { get; set; }


        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }

        public int TestField { get; set; }

        public virtual Language.Language Language { get; set; }

    }
}
