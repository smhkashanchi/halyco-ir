﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.NewsLetters
{
    public class NewsLetterGroup     
    {
        public NewsLetterGroup()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte Group_ID { get; set; }
        [DisplayName("عنوان گروه خبرنامه")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string Title { get; set; }

        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }

        [Column(TypeName = "varchar(8)")]
        [ForeignKey("Language")]
        public string Lang { get; set; }

        public virtual Language.Language Language { get; set; }
        public virtual List<NewsLetter> NewsLetters{ get; set; }
    }
}
