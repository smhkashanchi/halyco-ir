﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.NewsLetters
{
    public class NewsLetter
    {
        public NewsLetter()
        {

        }

        [Key]
        public int NewsLetter_ID { get; set; }
        [DisplayName("نام گروه")]
        [ForeignKey("NewsLetterGroup")]
        public byte GroupId { get; set; }
        [DisplayName("نام و نام خانوادگی")]
        [MaxLength(150)]
        public string FullName { get; set; }
        [DisplayName("ایمیل")]
        [MaxLength(150)]
        [Required(ErrorMessage ="{0} را وارد کنید")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "ایمیل وارد شده نامعتبر است")]
        public string Email { get; set; }
        [DisplayName("تلفن همراه")]
        [MaxLength(15)]
        public string Mobile { get; set; }

        public virtual NewsLetterGroup NewsLetterGroup { get; set; }

    }
}
