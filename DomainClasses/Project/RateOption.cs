﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Project
{
    public class RateOption
    {
        public RateOption()
        {

        }
        [Key]
        public int RateOption_ID { get; set; }
        [DisplayName("نام محصول")]
        [ForeignKey("Projects")]
        public int ProjectId { get; set; }
        [DisplayName("جمع امتیاز دهنده ها")]
        public int RatorTotal { get; set; }
        [DisplayName("جمع امتیاز")]
        public int RateTotal { get; set; }
        [DisplayName("میانگین امتیاز")]
        public float RateAverage { get; set; }

        public virtual Projects Projects { get; set; }
    }
}
