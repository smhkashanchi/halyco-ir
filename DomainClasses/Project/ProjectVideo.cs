﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Project
{
    public class ProjectVideo
    {
        public ProjectVideo()
        {

        }
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }
        public int? ProjectId { get; set; }
        public int? VideoId { get; set; }

        public virtual Projects Projects { get; set; }
        public virtual AVF.AVF AVF { get; set; }
    }
}
