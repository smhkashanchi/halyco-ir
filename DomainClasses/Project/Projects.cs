﻿using DomainClasses.Product;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Project
{
    public class Projects  /// در این پروژه از ساختار جداول پروژه ها برای محصولات استفاده شده است
    {
        public Projects()
        {

        }
        [Key]
        public int Project_ID { get; set; }
        [DisplayName("تصویر")]
        [MaxLength(150)]
        public string Image { get; set; }
        [DisplayName("گالری")]
        [ForeignKey("Gallery")]
        public Int16? GalleryId { get; set; }
        
        [DisplayName("تاریخ ثبت")]
        public DateTime CreateDate { get; set; }
        
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }
        [DisplayName("محبوب ترین")]
        public bool IsPopular { get; set; }

        //[DisplayName("ویدئو")]
        //[MaxLength(150)]
        //public string Catalog { get; set; }

        [DisplayName("عنوان پروژه")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(500)]
        public string Title { get; set; }
        [ForeignKey("ProjectGroup")] 
        [DisplayName("گروه پروژه")]
        public Int16? GroupId { get; set; }
        [DisplayName("توضیحات پروژه")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        //[Allowhtml]
        public string Description { get; set; }
        public string MetaTitle { get; set; }
        [MaxLength(500)]
        public string MetaKeyword { get; set; }
        [MaxLength(500)]
        public string MetaDescription { get; set; }
        [MaxLength(500)]
        public string MetaOther { get; set; }
        [DisplayName("تولید کننده")]
        [ForeignKey("Producer")] 
        public Int16? ProducerId { get; set; }
        [DisplayName("کلمات کلیدی")]
        [MaxLength(400)]
        public string Tags { get; set; }
        [DisplayName("اسلاگ")]
        [MaxLength(500)]
        public string Slug { get; set; }
        //Relation For Gallery
        public virtual Gallery.Gallery Gallery { get; set; }
        public virtual Producer Producer { get; set; }
        public virtual ProjectGroup ProjectGroup { get; set; }
        public virtual List<ProjectVideo> ProjectVideos { get; set; }

    }
}
