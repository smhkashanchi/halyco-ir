﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Project
{
    public  class ProjectGroup
    {
        public ProjectGroup()
        {

        }
        [Key]
        public Int16 ProjectGroup_ID { get; set; }
        [DisplayName("عنوان گروه")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(300)]
        public string Title { get; set; }
        [DisplayName("توضیحات")]
        [MaxLength(500)]

        public string Description { get; set; }
        [DisplayName("تصویر")]
        [Required(ErrorMessage = "لطفا {0} را انتخاب نمایید")]
        [MaxLength(100)]
        public string GroupImage { get; set; }

        [Column(TypeName = "varchar(8)")]
        [ForeignKey("Language")]
        public string Lang { get; set; }

        [ForeignKey("ProjectGroups")]
        public Int16? ParentId { get; set; }
        public bool IsParent { get; set; }

        //[DisplayName("عرض عکس بزرگ")]
        //public Int16? PicLargeWidth { get; set; }
        //[DisplayName("عرض عکس کوچک")]
        //public Int16? PicSmallWidth { get; set; }
        //[DisplayName("ارتفاع عکس بزرگ")]
        //public Int16? PicLargeHeight { get; set; }
        //[DisplayName("ارتفاع عکس کوچک")]
        //public Int16? PicSmallHeight { get; set; }

        public ProjectGroup ProjectGroups { get; set; }
        public virtual Language.Language Language { get; set; }
    }
}
