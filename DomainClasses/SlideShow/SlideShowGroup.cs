﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.SlideShow
{
    public class SlideShowGroup
    {
        public SlideShowGroup()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte SlideShowGroupID { get; set; }
        [DisplayName("عنوان گروه")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(150)]
        public string SSGName { get; set; }
        [Column(TypeName = "varchar(8)")]
        [ForeignKey("Language")]
        public string Lnag { get; set; }
        [DisplayName("فعال/غیرفعال")]
        public bool IsActive { get; set; }

        public virtual Language.Language Language { get; set; }
        public virtual List<SlideShow> slideShows { get; set; }
    }
}
