﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.SlideShow
{
    public class SlideShow
    {
        public SlideShow()
        {

        }
        [Key]
        public int SlideShowID { get; set; }

        [Column(TypeName = "tinyint")]
        [ForeignKey("slideShowGroup")]
        public byte SSGId { get; set; }

        [DisplayName("تصویر")]
        
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(150)]
        public string SSPic { get; set; }


        [DisplayName("عنوان ")]
        [MaxLength(150)]
        public string SSTitle { get; set; }

        [DisplayName("متن اسلاید شو")]
        [MaxLength(700)]
        [DataType(DataType.MultilineText)]
        public string SSText { get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }
        [DisplayName("لینک")]
        [MaxLength(300)]
        public string Link { get; set; }

        //public byte SlideShowGroupID { get; set; }
        public virtual SlideShowGroup slideShowGroup { get; set; }

    }
}
