﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainClasses.User

{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {

        }
        [StringLength(200)]
        [Required]
        [Display(Name = "نام و نام خانوادگی")]
        public string FullName { get; set; }
        [Display(Name = "تصویر پروفایل")]

        public string UserPhoto { get; set; }

        [Display(Name = "تلفن همراه1")]
        [StringLength(12)]
        public string Mobile_1 { get; set; }

        [Display(Name = "تلفن همراه2")]
        [StringLength(12)]
        public string Mobile_2 { get; set; }

        [Display(Name = "تلفن ثابت")]
        [StringLength(12)]
        public override string PhoneNumber { get; set; }
        [Display(Name = "ایمیل")]

        public override string Email { get; set; }
        [Display(Name = "تاریخ ثبت نام")]

        public string RegisterDate { get; set; }

        public virtual Customer.Customer Customer { get; set; }
        public virtual Employees.Employee Employee { get; set; }

    }
}


