﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.ContactUsMessage
{
    public class ContactusMessageGroup
    {
        public ContactusMessageGroup()
        {
            
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte CMG_ID { get; set; }
        [DisplayName("نام گروه")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string CMGName { get; set; }
        [Column(TypeName = "varchar(8)")]
        [ForeignKey("language")] /////////////برای ایجاد ارتباط خارجی با فیلد دیگر جدول
        public string Lang { get; set; }
        [DisplayName("فعال/غیرفعال")]
        public bool IsActive { get; set; }
        public virtual Language.Language language { get; set; }
        public virtual List<ContactusMessage> contactusMessages { get; set; }
    }
}
