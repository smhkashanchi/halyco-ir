﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.ContactUsMessage
{
    public class ContactusMessage
    {
        public ContactusMessage()
        {

        }
        [Key]
        public int ContactusMessage_ID { get; set; }
        [DisplayName("نام گروه")]
        [ForeignKey("contactusMessageGroup")]
        
        public byte CMGId { get; set; }
        [DisplayName("نام فرستنده")]
        [MaxLength(150)]
        public string SenderName { get; set; }
        [DisplayName("ایمیل فرستنده")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(200)]
        [EmailAddress(ErrorMessage ="لطفا یک ایمیل معتبر وارد نمایید")]
        public string SenderEmail { get; set; }
        [DisplayName("تلفن فرستنده")]
        [MaxLength(15)]
        public string SenderMobile { get; set; }
        [DisplayName("متن پیام")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(2000)]
        [DataType(DataType.MultilineText)]
        public string MessageText { get; set; }
        [DisplayName("پاسخ پیام")]
        [MaxLength(2000)]
        [DataType(DataType.MultilineText)]
        public string MessageAnswer { get; set; }
        [DisplayName("جدید است؟")]
        public bool IsNew { get; set; }
        [DisplayName("تاریخ ارسال")]
        public DateTime SendDate { get; set; }
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        [Display(Name = "حاصل جمع")]
        [NotMapped]
        public string Captcha { get; set; }

        public virtual ContactUsMessage.ContactusMessageGroup contactusMessageGroup { get; set; }
    }
}
