﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Suppliers
{
   public class MainSuppliers 
    {
        public MainSuppliers()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int16 Suppliers_ID { get; set; }
        [DisplayName("نوع تامین کننده")]
        public bool SuppliersType { get; set; }
        [DisplayName("نام و نام خانوادگی")]
        [MaxLength(100)]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        public string FullName { get; set; }
        [DisplayName("نام تجاری")]
        [MaxLength(50)]
        public string Brand { get; set; }
        [DisplayName("تلفن دفتر مرکزی")]
        [MaxLength(15)]
        public string CentralOfficeTell { get; set; }
        [DisplayName("تلفن دفتر فروش")]
        [MaxLength(15)]
        public string SalesTell { get; set; }
        [DisplayName("تلفن کارگاه/کارخانه")]
        [MaxLength(15)]
        public string FactoyTell { get; set; }
        [DisplayName("دورنگار دفتر مرکزی")]
        [MaxLength(20)]
        public string CentralOfficFax { get; set; }
        [DisplayName("دورنگار دفتر فروش")]
        [MaxLength(20)]
        public string SalesFax { get; set; }
        [DisplayName("دورنگار کارگاه/کارخانه")]
        [MaxLength(20)]
        public string FactoryFax { get; set; }
        [DisplayName("کدپستی دفتر مرکزی")]
        [MaxLength(15)]
        public string CentralOfficePostalCode { get; set; }
        [DisplayName("کدپستی دفتر فروش")]
        [MaxLength(15)]
        public string SalesPostalCode { get; set; }
        [DisplayName("کدپستی کارگاه/کارخانه")]
        [MaxLength(15)]
        public string FactoyPostalCode { get; set; }
        [DisplayName("تلفن همراه")]
        [MaxLength(15)]
        public string PhoneNumber { get; set; }
        [DisplayName("آدرس")]
        [MaxLength(200)]
        public string Address { get; set; }
        [DisplayName("ایمیل")]
        [EmailAddress(ErrorMessage ="لطفا {0} را بصورت صحیح وارد نمایید")]
        [MaxLength(50)]
        public string Email { get; set; }
        [DisplayName("وب سایت")]
        [Url(ErrorMessage ="لطفا {0} را بصورت صحیح همراه با http یا https وارد نمایید")]
        [MaxLength(30)]
        public string Website { get; set; }
        [DisplayName("توان مالی")]
        [MaxLength(50)]
        public string Affordability { get; set; }
        [DisplayName("تعداد پرسنل")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public Int16 PersonnelNumber { get; set; }
        [DisplayName("تعداد تکنسین")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public Int16 TecknecianNumber { get; set; }
        [DisplayName("تعداد کارشناسان")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public Int16 ExpertsNumber { get; set; }
        [DisplayName(" تعداد پرسنل کنترل کیفیت")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public Int16 QualityControlNumber { get; set; }

        public virtual List<SupplierShareholders> SupplierShareholders { get; set; }

        public virtual List<SuppliersActivityFields> SuppliersActivityFields { get; set; }
        public virtual List<SuppliersResume> SuppliersResumes { get; set; }
        public virtual List<SuppliersCertificates> SuppliersCertificates { get; set; }
    }
}
