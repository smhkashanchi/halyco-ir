﻿using DomainClasses.Proposal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Suppliers
{
   public class SuppliersFiles
    {
        public SuppliersFiles()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        [DisplayName("تامین کننده")]
        [ForeignKey("MainSuppliers")]
        public Int16 SupplierId { get; set; }

        public string FileNames { get; set; }
        public virtual MainSuppliers MainSuppliers { get; set; }
    }
}
