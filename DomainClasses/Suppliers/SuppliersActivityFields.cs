﻿using DomainClasses.BasicInformation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Suppliers
{
    public class SuppliersActivityFields
    {
        public SuppliersActivityFields()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Int16 S_ActivityFields_ID { get; set; }
        [DisplayName("زمینه فعالیت")]
        [ForeignKey("Activity")]
        public Int16 ActivityId { get; set; }
        [DisplayName("ظرفیت تامین در سال")]
        public int SupplyCapacity { get; set; }
        [DisplayName("تامین کنندگان اصلی")]
        [ForeignKey("MainSuppliers")]
        public Int16 SupplierId { get; set; }

        public virtual Activity Activity { get; set; }
        public virtual Suppliers.MainSuppliers MainSuppliers{ get; set; }
    }
}
