﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Suppliers
{
    public class SuppliersResume
    {
        public SuppliersResume()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Resume_ID { get; set; }
        [DisplayName("تامین کنندگان اصلی")]
        [ForeignKey("MainSuppliers")]
        public Int16 SupplierId { get; set; }
        [DisplayName("نام شرکت / پروژه")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string Title { get; set; }
        [DisplayName("کارفرما")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string Employer { get; set; }
        [DisplayName("شرح قرارداد")]
        [MaxLength(500,ErrorMessage ="اجازه ورود 500 کاراکتر")]
        public string ResumeDescription { get; set; }
        [DisplayName("مدت همکاری")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string CollaborationPeriod { get; set; }

        public virtual MainSuppliers MainSuppliers { get; set; }
    }
}
