﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Suppliers
{
    public class SuppliersCertificates
    {
        public SuppliersCertificates()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Certificate_ID { get; set; }
        [DisplayName("تامین کنندگان اصلی")]
        [ForeignKey("MainSuppliers")]
        public Int16 SupplierId { get; set; }
        [DisplayName("نام موسسه")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(200)]
        public string Title { get; set; }
        [DisplayName("نوع گواهینامه")]
        public bool Type { get; set; }
        [DisplayName("شرح گواهینامه")]
        [MaxLength(500, ErrorMessage = "اجازه ورود 500 کاراکتر")]
        public string Description { get; set; }

        public virtual MainSuppliers MainSuppliers { get; set; }
    }
}
