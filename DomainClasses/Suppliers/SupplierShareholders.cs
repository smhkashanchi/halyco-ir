﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Suppliers
{
   public class SupplierShareholders
    {
        public SupplierShareholders()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Int16 ShareHolder_ID { get; set; }
        [DisplayName("تامین کننده اصلی")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        //[ForeignKey("MainSuppliers")]
        public Int16? Suppliers_ID { get; set; }
        public virtual MainSuppliers MainSuppliers { get; set; }
        [DisplayName("نام و نام خانوادگی")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string FullName { get; set; }
        [DisplayName("نوع")]
        public bool Type { get; set; }
        [DisplayName("درصد سهم")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public byte PercentageShare { get; set; }

        
    }
}
