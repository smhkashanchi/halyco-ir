﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Language
{
    public class Language
    {
        public Language()
        {

        }
        [Key]
        [Column(TypeName = "varchar(8)")]
        [DisplayName("شناسه زبان")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string Lang_ID { get; set; }

        [Column(TypeName ="nvarchar(50)")]
        [DisplayName("نام زبان")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        public string Lang_Name { get; set; }

        [DisplayName("فعال/غیرفعال")]
        public bool IsActive { get; set; }
    }
}
