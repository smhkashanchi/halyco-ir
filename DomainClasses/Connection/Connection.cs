﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Connection
{
    public class Connection
    {
        public Connection()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte Connection_ID { get; set; }
        [DisplayName("نام گروه ارتباطی")]
        [ForeignKey("ConnectionGroup")]
        public byte ConnectionGroupId { get; set; }
        [DisplayName("نام ارتباط")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string ConnectionName { get; set; }
        [DisplayName("مقادیر")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(500)]
        public string ConnectionValue { get; set; }
        [DisplayName("متنی / نقشه")]
        public bool CType { get; set; }
        [DisplayName("تصویر")]
        public string CImage { get; set; }

        public virtual ConnectionGroup ConnectionGroup { get; set; }
    }
}
