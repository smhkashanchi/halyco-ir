﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Connection
{
   public class ConnectionGroup
    {
        public ConnectionGroup()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte ConnectionGroup_ID { get; set; }
        [DisplayName("نام گروه ارتباطی")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string ConnectionGroupTitle { get; set; }
        [Column(TypeName = "varchar(8)")]
        [ForeignKey("Language")]
        public string Lang { get; set; }
        [DisplayName("فعال / غیر فعال")]
        public bool IsActive { get; set; }

        public virtual Language.Language Language { get; set; }
        public virtual List<Connection> Connections { get; set; }
    }
}
