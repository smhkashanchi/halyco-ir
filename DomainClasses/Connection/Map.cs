﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Connection
{
    public class Map
    {
        public Map()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte ID { get; set; }
        [Required]
        [MaxLength(50)]
        public string Latitude { get; set; }
        [Required]
        [MaxLength(50)]
        public string longitude { get; set; }
        [Required]
        public byte Type { get; set; }
    }
}
