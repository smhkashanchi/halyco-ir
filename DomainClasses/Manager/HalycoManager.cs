﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DomainClasses.Manager
{
    public class HalycoManager
    {
        public HalycoManager()
        {

        }

        [Key]
        public Int16 HManager_ID { get; set; }

        [DisplayName("نام و نام خانوادگی")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string ManagerName { get; set; }

        [DisplayName("سمت")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string ManagerPost { get; set; }

        [DisplayName("ایمیل")]
        [MaxLength(100)]
        public string ManagerEmail { get; set; }

        [DisplayName("تصویر")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(200)]
        public string ManagerPhoto { get; set; }

        [DisplayName("عضو هیئت مدیره / مدیر اجرایی")]
        public bool ManagerType { get; set; }

        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }
        [DisplayName("نام به انگلیسی")]
        [MaxLength(100)]
        public string EnglishName { get; set; }
        [DisplayName("سمت به انگلیسی")]
        [MaxLength(100)]
        public string EnglishPost { get; set; }
        
    }
}
