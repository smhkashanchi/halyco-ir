﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Employees
{
    public class ExtraWork
    {

        [Key]
        public int ExtraWorkID { get; set; }

        [DisplayName("کد پرسنلی")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(10, ErrorMessage = "کد پرسنلی نمی تواند بیش از 10 رقم باشد")]
        [ForeignKey("PersonalCodeFirst")]
        public virtual Employee Employee_Ext_1 { get; set; }
        public string PersonalCodeFirst { get; set; }

        [DisplayName("تاریخ شروع")]
        [Required(ErrorMessage = "{0} را وارد کنید")]
        public string StartDate { get; set; }

        [DisplayName("تاریخ پایان")]
        [Required(ErrorMessage = "{0} را وارد کنید")]
        public string EndDate { get; set; }

        [DisplayName("ساعت شروع")]
        [Required(ErrorMessage = "{0} را وارد کنید")]
        [MaxLength(10)]
        public string StartTime { get; set; }

        [DisplayName("ساعت پایان")]
        [Required(ErrorMessage = "{0} را وارد کنید")]
        [MaxLength(10)]
        public string EndTime { get; set; }

        [DisplayName("علت اضافه کاری")]
        [MaxLength(500)]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string ExtraWorktime { get; set; }
       
        [DisplayName("تاریخ ثبت")]
        [Required(ErrorMessage = "{0} را وارد کنید")]
        public DateTime CreateDate { get; set; }

        [DisplayName("وضعیت نهایی درخواست")]
        public bool? FinalStatus { get; set; }

    }
}
