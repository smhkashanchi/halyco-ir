﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Employees
{
    public class Loan
    {
        [Key]
        public int loanID { get; set; }

        [DisplayName("کد پرسنلی")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(10, ErrorMessage = "کد پرسنلی نمی تواند بیش از 10 رقم باشد")]
        [ForeignKey("PersonalCode")]
        public virtual Employee Employee_lo_1 { get; set; }
        public string PersonalCode { get; set; }

        [DisplayName("نوع وام")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public byte loanType { get; set; }

        [DisplayName("علت وام")]
        public byte loanReason { get; set; }

        [DisplayName("توضیح علت وام")]
        [MaxLength(100, ErrorMessage = "علت وام نمی تواند بیش از 100 رقم باشد")]
        public string loanReasonDescription { get; set; }

        [DisplayName("مبلغ وام")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public int amount { get; set; }

        [DisplayName("توضیحات وام")]
        [MaxLength(1000, ErrorMessage = "توضیحات وام نمی تواند بیش از 1000 رقم باشد")]
        public string Description { get; set; }

        [DisplayName("کد پرسنلی مافوق")]
        [MaxLength(10, ErrorMessage = "کد پرسنلی مافوق نمی تواند بیش از 10 رقم باشد")]
        [ForeignKey("PersonalCode_superior")]
        public virtual Employee Employee_lo_3 { get; set; }
        public string PersonalCode_superior { get; set; }

        [DisplayName("علت عدم تایید مافوق")]
        [MaxLength(500)]
        public string SuperiorReason { get; set; }

        [DisplayName("وضعیت امضای مافوق")]
        public bool? StatusSuperiorSignature { get; set; }

        [DisplayName("کد پرسنلی مدیر")]
        [MaxLength(10, ErrorMessage = "کد پرسنلی مدیر نمی تواند بیش از 10 رقم باشد")]
        [ForeignKey("PersonalCode_manager")]
        public virtual Employee Employee_lo_4 { get; set; }
        public string PersonalCode_manager { get; set; }

        [DisplayName("علت عدم تایید مدیر")]
        [MaxLength(500)]
        public string ManagerReason { get; set; }

        [DisplayName("وضعیت امضای مدیر")]
        public bool? StatusManagerSignature { get; set; }


        [DisplayName("کد پرسنلی م.م.انسانی")]
        [MaxLength(10, ErrorMessage = "کد پرسنلی م.م.انسانی نمی تواند بیش از 10 رقم باشد")]
        [ForeignKey("PersonalCode_HRmanager")]
        public virtual Employee Employee_lo_5 { get; set; }
        public string PersonalCode_HRmanager { get; set; }

        [DisplayName("علت عدم تایید م.م.انسانی")]
        [MaxLength(500)]
        public string HRManagerReason { get; set; }

        [DisplayName("وضعیت امضای م.م.انسانی")]
        public bool? StatusHRManagerSignature { get; set; }

        [DisplayName("تاریخ ثبت")]
        [Required(ErrorMessage = "{0} را وارد کنید")]
        public DateTime CreateDate { get; set; }

        [DisplayName("تاریخ تایید مافوق")]
        public DateTime? SuperiorDateTime { get; set; }

        [DisplayName("تاریخ تایید مدیر")]
        public DateTime? ManagerDateTime { get; set; }

        [DisplayName("تاریخ تایید م.مانسانی")]
        public DateTime? HRManagerDateTime { get; set; }
    }
}
