﻿using DomainClasses.BasicInformation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Employees
{
    public class Employe_Post
    {
        public Employe_Post()
        {

        }
        [Key]
        public int Employ_Post_ID { get; set; }
        [DisplayName("کارمند")]
        [ForeignKey("Employee")]
        public string PersonalCode { get; set; }
        [DisplayName("سمت")]
        [ForeignKey("OfficePost")]
        public Int16 PostId { get; set; }
        [DisplayName("تاریخ آغاز به کار")]
        public DateTime StartDate { get; set; }
        [DisplayName("تاریخ پایان سمت")]
        public DateTime EndDate { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual OfficePost OfficePost { get; set; }

        //[InverseProperty("Employe_Post_1")]
        //public virtual List<Mission> Missions { get; set; }
        //[InverseProperty("Employe_Post_2")]
        //public virtual List<Mission> Missions2 { get; set; }

    }
}
