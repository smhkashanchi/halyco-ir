﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using DomainClasses.BasicInformation;

namespace DomainClasses.Employees
{
    public class ShiftChange
    {
        [Key]
        public int ShiftChange_ID { get; set; }

        [DisplayName("کد پرسنلی درخواست دهنده")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(10, ErrorMessage = "کد پرسنلی نمی تواند بیش از 10 رقم باشد")]
        [ForeignKey("PersonalCodeFirst")]
        public virtual Employee Employee_Shc_1 { get; set; }
        public string PersonalCodeFirst { get; set; }

        [DisplayName("کد پرسنلی جایگزین")]
        //[Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(10, ErrorMessage = "کد پرسنلی نمی تواند بیش از 10 رقم باشد")]
        [ForeignKey("PersonalCodeSecond")]
        public virtual Employee Employee_Shc_2 { get; set; }
        public string PersonalCodeSecond { get; set; }

        [DisplayName("وضعیت امضای جایگزین")]
        public bool? StatusAlternateSignature { get; set; }

        [DisplayName("علت عدم تایید جایگزین")]
        [MaxLength(500)]
        public string AlternateSignatureReason { get; set; }

        [DisplayName("واحد")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [ForeignKey("UnitID_1")]
        public virtual OfficeUnit OfficeUnit1 { get; set; }
        public Int16 UnitID_1 { get; set; }

        [DisplayName("شیفت درخواست دهنده")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string ShiftName1 { get; set; }

        [DisplayName("شیفت جایگزین")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string ShiftName2 { get; set; }

        [DisplayName("تاریخ ثبت")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public DateTime CreateDate { get; set; }
        
        [DisplayName("تاریخ تایید جایگزین")]
        public DateTime? AlternateDateTime { get; set; }

        [DisplayName("تاریخ شیفت")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string shiftDate { get; set; }

        [DisplayName("علت تغییر شیفت")]
        [MaxLength(500)]
        public string shiftReason { get; set; }
       
        [DisplayName("وضعیت نهایی درخواست")]
        public bool? FinalStatus { get; set; }
    }
}
