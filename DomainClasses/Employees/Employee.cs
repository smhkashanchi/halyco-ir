﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DomainClasses.BasicInformation;
using DomainClasses.Survey;
using DomainClasses.User;

namespace DomainClasses.Employees
{
   public class Employee
    {
        public Employee() { }

        [Key]
        [DisplayName("کد پرسنلی")]
        [MaxLength(10)]
        [Required(ErrorMessage = "درج کد پرسنلی الزامی می باشد")]
        public string PersonalCode { get; set; }

        [ForeignKey("UserIdentity_Id")]
        public ApplicationUser ApplicationUser { get; set; }
        [StringLength(450)]

        public string UserIdentity_Id { get; set; }
        //[DisplayName("نام")]
        //[Required(ErrorMessage ="نام کارمند را درج نمایید")]
        //[MaxLength(50,ErrorMessage ="نام نمی تواند بیش از 50 کاراکتر باشد")]
        //public string FName { get; set; }

        //[DisplayName("نام خانوادگی")]
        //[Required(ErrorMessage = "نام خانوادگی کارمند را درج نمایید")]
        //[MaxLength(50, ErrorMessage = "نام خانوادگی نمی تواند بیش از 50 کاراکتر باشد")]
        //public string LName { get; set; }

        [DisplayName("کد ملی")]
        [MaxLength(10,ErrorMessage ="کد ملی حتما باید 10 رقم باشد")]
        [Required(ErrorMessage = "درج کد ملی الزامی می باشد")]
        public string NationalCode { get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }

        //[DisplayName("کلمه عبور")]
        //[MaxLength(50)]
        //[Required(ErrorMessage = "درج کلمه عبور الزامی می باشد")]
        //public string PersonPassword { get; set; }

        //[DisplayName("تصویر")]
        //[MaxLength(500)]
        ////[Required(ErrorMessage = "درج کلمه عبور الزامی می باشد")]
        //public string PersonPic { get; set; }

        //[DisplayName("تلفن ثابت")]
        //[MaxLength(15)]
        ////[Required(ErrorMessage = "درج کلمه عبور الزامی می باشد")]
        //public string Tel { get; set; }

        //[DisplayName("تلفن همراه")]
        //[MaxLength(15)]
        //[Required(ErrorMessage = "درج تلفن همراه الزامی می باشد")]
        //public string Mobile { get; set; }

        [DisplayName("سمت")]
        [Required(ErrorMessage = "انتخاب سمت الزامی می باشد")]
        [ForeignKey("OfficePost")]
        public Int16 OfficePostId { get; set; }

        [DisplayName("واحد")]
        [Required(ErrorMessage = "انتخاب واحد الزامی می باشد")]
        [ForeignKey("OfficeUnit")]
        public Int16 OfficeUnitId { get; set; }

        [DisplayName("وظایف")]
        [MaxLength(100)]
        public string responsibility { get; set; }

        [DisplayName("وضعیت شیفت")]
        public bool? HasShift { get; set; }

        [DisplayName("وضعیت جایگزین")]
        public bool? HasAlternateEmployee { get; set; }

        [DisplayName("شیفت")]
        [MaxLength(50)]
        public string ShiftName { get; set; }

        [DisplayName("سطح نظارت")]
        public byte? SupervisionLevel { get; set; }

        [DisplayName("شغل")][MaxLength(100)]
        public string Job { get; set; }

        public virtual OfficePost OfficePost { get; set; }

        public virtual OfficeUnit OfficeUnit { get; set; }


        //[InverseProperty("Employee_V_1")]
        //public virtual List<Vacation> Vacation { get; set; }

        //[InverseProperty("Employee_V_5")]
        //public virtual List<Vacation> Vacation2 { get; set; }

        //[InverseProperty("Employee_V_4")]
        //public virtual List<Vacation> Vacation3 { get; set; }

        //[InverseProperty("Employee_V_3")]
        //public virtual List<Vacation> Vacation4 { get; set; }

        [InverseProperty("Employee_V_2")]
        public virtual List<Vacation> Vacation5 { get; set; }

        public virtual List<EmployeeSalary> EmployeeSalaries { get; set; }

        [InverseProperty("Employee_Shc_1")]
        public virtual List<ShiftChange> ShiftChanges1 { get; set; }

        [InverseProperty("Employee_Shc_2")]
        public virtual List<ShiftChange> ShiftChanges2 { get; set; }

        //[InverseProperty("Employee_Shc_3")]
        //public virtual List<ShiftChange> ShiftChanges3 { get; set; }

        //[InverseProperty("Employee_Shc_4")]
        //public virtual List<ShiftChange> ShiftChanges4 { get; set; }

        //[InverseProperty("Employee_Shc_5")]
        //public virtual List<ShiftChange> ShiftChanges5 { get; set; }

        [InverseProperty("Employee_Ext_1")]
        public virtual List<ExtraWork> ExtraWork1 { get; set; }

        //[InverseProperty("Employee_Ext_3")]
        //public virtual List<ExtraWork> ExtraWork2 { get; set; }

        //[InverseProperty("Employee_Ext_4")]
        //public virtual List<ExtraWork> ExtraWork3 { get; set; }

        //[InverseProperty("Employee_Ext_5")]
        //public virtual List<ExtraWork> ExtraWork4 { get; set; }

        [InverseProperty("Employee_lo_1")]
        public virtual List<Loan> Loan1 { get; set; }

        [InverseProperty("Employee_lo_3")]
        public virtual List<Loan> Loan2 { get; set; }

        [InverseProperty("Employee_lo_4")]
        public virtual List<Loan> Loan3 { get; set; }

        [InverseProperty("Employee_lo_5")]
        public virtual List<Loan> Loan4 { get; set; }


        [InverseProperty("Employee_M_1")]
        public virtual List<Mission> Mission1 { get; set; }

        //[InverseProperty("Employee_M_3")]
        //public virtual List<Mission> Mission3 { get; set; }

        //[InverseProperty("Employee_M_4")]
        //public virtual List<Mission> Mission4 { get; set; }

        //[InverseProperty("Employee_M_5")]
        //public virtual List<Mission> Mission5 { get; set; }


        //[InverseProperty("Employee_Vec_2")]
        //public virtual List<Vacation> Vacation2 { get; set; }
        //[InverseProperty("Employee_Vec_3")]
        //public virtual List<Vacation> Vacation3 { get; set; }

        // public virtual List<Employe_Post> Employ_Posts{ get; set; }
        // public virtual List<SurveyEmployeesOtherData> SurveyEmployeesOtherDatas{ get; set; }
    }
}
