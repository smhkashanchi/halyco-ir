﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Employees
{
    public class EmployeeSalary
    {
        public EmployeeSalary()
        {

        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [MaxLength(50)]
        public string Salary_ID { get; set; }

        [DisplayName("کد پرسنلی")]
        [MaxLength(10)]
        [ForeignKey("Employee")]
        public string PersonalCode { get; set; }

        [DisplayName("دوره")]
        [MaxLength(10)]
        public string SalMah{ get; set; }

        [DisplayName("نام محل خدمت")]
        [MaxLength(100)]
        public string NameMahaleKhedmatHokmi { get; set; }

        [DisplayName("خالص پرداختی")]
        public int SP_Khales_Pardakhti{ get; set; }

        [DisplayName("حق ایاب و ذهاب")]
        public int FR_Ayab_Zahab{ get; set; }

        [DisplayName("بیمه تکمیلی")]
        public int FR_Bime_Takmili{ get; set; }

        [DisplayName("دستمزد روزانه")]
        public int FR_DastmozdRoozaneh{ get; set; }

        [DisplayName("فوق العاده")]
        public int FR_FOGHOLADE{ get; set; }

        [DisplayName("حق جذب")]
        public int FR_Hag_Jazb{ get; set; }

        [DisplayName("حق مسکن")]
        public int FR_HAG_MASKAN{ get; set; }

        [DisplayName("حق مسؤلیت")]
        public int FR_HAG_MASULEYAT{ get; set; }

        [DisplayName("حق اولاد")]
        public int FR_HAG_OLAD{ get; set; }

        [DisplayName("حقوق پایه")]
        public int FR_Hogug_Payeh{ get; set; }

        [DisplayName("سایر مزایا")]
        public int FR_Sayer_Mazaya{ get; set; }

        [DisplayName("حق اضافه کاری")]
        public int SP_Ezafe_Kar{ get; set; }

        [DisplayName("حق بیمه کارمند")]
        public int SP_Hag_Bimeh_Karmand{ get; set; }

        [DisplayName("جمع اضافات")]
        public int SP_Jame_Ezafat{ get; set; }

        [DisplayName("جمع کسورات")]
        public int SP_Jame_Kosurat{ get; set; }

        [DisplayName("مالیات")]
        public int SP_Maleyat{ get; set; }

        [DisplayName("مشمول بیمه")]
        public int SP_MashMul_Bimeh{ get; set; }

        [DisplayName("مشمول مالیات")]
        public int SP_Mashmul_Maleyat{ get; set; }

        [DisplayName("قسط وام 1")]
        public int SY_KE_VamGhest1{ get; set; }

        [DisplayName("مانده وام 1")]
        public Int64 SY_KE_VamMandeh1{ get; set; }

        [DisplayName("ساعات اضافه کاری")]
        public Int16 SY_Saat_AzafeKar{ get; set; }

        [DisplayName("سایر کسورات")]
        public int SY_sayer_Kosurat{ get; set; }

        [DisplayName("تعداد روز کارکرد")]
        public Int16 SY_Tadad_Ruz_Kar{ get; set; }

        [DisplayName("فوق العاده ویژه")]
        public int ZR_FOGHOLADE_Special{ get; set; }

        [DisplayName("ساعات نوبت کاری")]
        public int SY_saat_nobat_kari{ get; set; }

        [DisplayName("معوقه")]
        public int FR_moavaghe { get; set; }

        [DisplayName("مساعده")]
        public int FR_mosaede { get; set; }

        [DisplayName("پاداش")]
        public int FR_padash__ { get; set; }

        [DisplayName("سایر اضافات")]
        public int FR_sayer_ezafat { get; set; }

        [DisplayName("مبلغ نوبت کاری")]
        public int SP_mablagh_nobatkari { get; set; }

        [DisplayName("مالیات حقوق")]
        public int SP_Malyat_hoghogh { get; set; }

        [DisplayName("بن کارگری")]
        public int FR_bon_kargari { get; set; }

        [DisplayName("مزد سنوات")]
        public int FR_mozd_sanavat { get; set; }

        [DisplayName("حق مأموریت")]
        public int SP_mablagh_MAMOREYAT { get; set; }

        [DisplayName("کارانه")]
        public int FR_Karane { get; set; }

        [DisplayName("تعداد روز مأموریت")]
        public int FR_mamoriat { get; set; }

        [DisplayName("عیدی")]
        public int FR_Eydi { get; set; }

        [DisplayName("مالیات عیدی")]
        public int FR_m_maliat_EYDI{ get; set; }

        [DisplayName("تاریخ شروع همکاری")]
        [MaxLength(15)]
        public string FR_Date_StartKar { get; set; }

        [DisplayName("مانده مرخصی")]
        [MaxLength(20)]
        public string FR_MandehMorakhasi { get; set; }

        [DisplayName("تعداد روز غیبت")]
        public Int16 SY_GHaybat_Ruz { get; set; }

        [DisplayName("تعداد روز مرخصی")]
        public Int16 SY_Morkhasi_Ruz { get; set; }

        [DisplayName("تعداد ساعات مرخصی")]
        [MaxLength(20)]
        public string SY_Morkhasi_Saat { get; set; }

        [DisplayName("بیمه خودرو")]
        public int FR_Bimeh_mashin { get; set; }

        [DisplayName("ذخیره صندوق وام")]
        public int FR_Sandogh_Z_V { get; set; }

        [DisplayName("قسط وام 2")]
        public int SY_KE_VamGhest2 { get; set; }

        [DisplayName("نام وام 1")]
        [MaxLength(100)]
        public string SY_KE_NameVam1 { get; set; }

        [DisplayName("نام وام 2")]
        [MaxLength(100)]
        public string SY_KE_NameVam2 { get; set; }

        [DisplayName("نام وام 3")]
        [MaxLength(100)]
        public string SY_KE_NameVam3 { get; set; }

        [DisplayName("نام وام 4")][MaxLength(100)]
        public string SY_KE_NameVam4 { get; set; }

        [DisplayName("نام وام 5")]
        public string SY_KE_NameVam5 { get; set; }

        [DisplayName("قسط وام 3")]
        public int SY_KE_VamGhest3 { get; set; }

        [DisplayName("قسط وام 4")]
        public int SY_KE_VamGhest4 { get; set; }

        [DisplayName("قسط وام 5")]
        public int SY_KE_VamGhest5 { get; set; }

        [DisplayName("مانده وام 2")]
        public Int64 SY_KE_VamMandeh2 { get; set; }

        [DisplayName("مانده وام 3")]
        public Int64 SY_KE_VamMandeh3 { get; set; }

        [DisplayName("مانده وام 4")]
        public Int64 SY_KE_VamMandeh4 { get; set; }

        [DisplayName("مانده وام 5")]
        public Int64 SY_KE_VamMandeh5 { get; set; }

        [DisplayName("ماموریت ویژه")]
        public int FR_mamoriat_vizhe__ { get; set; }

        [DisplayName("تفاوت تطبیق")]
        public int FR_tafavot_tatbigh__ { get; set; }

        public virtual Employees.Employee Employee { get; set; }

    }
}
