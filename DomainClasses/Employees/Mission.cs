﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Employees
{
    public class Mission
    {
        public Mission()
        {

        }
        [Key]
        public int Mission_ID { get; set; }
       
        [DisplayName("تاریخ شروع ماموریت")]
        [Required(ErrorMessage ="{0} را وارد کنید")]
        public string MissionStart { get; set; }
        [DisplayName("تاریخ پایان ماموریت")]
        [Required(ErrorMessage = "{0} را وارد کنید")]
        public string MissionEnd { get; set; }
        [DisplayName("نوع مقصد ماموریت")]
        public byte MissionType { get; set; }
        [DisplayName("علت ماموریت")]
        [Required(ErrorMessage = "{0} را وارد کنید")]
        [MaxLength(1000,ErrorMessage ="{0} نمی تواند بیش از 1000 کاراکتر باشد")]
        public string Reason { get; set; }
        [DisplayName("همراهان")]
        [MaxLength(200)]
        public string Followers { get; set; }
        [DisplayName("مقصد")]
        [MaxLength(500)]
        [Required(ErrorMessage = "{0} را وارد کنید")]
        public string Destination { get; set; }
        [DisplayName("موضوع ماموریت")]
        [MaxLength(500)]
        [Required(ErrorMessage = "{0} را وارد کنید")]
        public string MissionSubject { get; set; }

        [DisplayName("کد پرسنلی")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(10, ErrorMessage = "کد پرسنلی نمی تواند بیش از 10 رقم باشد")]
        [ForeignKey("PersonalCodeFirst")]
        public virtual Employee Employee_M_1 { get; set; }
        public string PersonalCodeFirst { get; set; }
       
        [DisplayName("تاریخ ثبت")]
        [Required]
        public DateTime CreateDate { get; set; }

        [DisplayName("ساعت شروع")]
        [MaxLength(10)]
        public string StartTime { get; set; }

        [DisplayName("ساعت پایان")]
        [MaxLength(10)]
        public string EndTime { get; set; }

        [DisplayName("وضعیت نهایی درخواست")]
        public bool? FinalStatus { get; set; }

        
        [DisplayName("نوع ماموریت")]
        //T=روزانه , F=ساعتی
        public bool type { get; set; }
    }
}
