﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Employees
{
    public class ShiftChangeSignature
    {
        [Key]
        public Int64 ShiftChangeSignature_ID { get; set; }

        [ForeignKey("shiftChange")]
        public int ShiftChange_ID { get; set; }
        public ShiftChange shiftChange { get; set; }

        [StringLength(450)]
        [ForeignKey("applicationUser")]
        [DisplayName("مافوق")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string PersonalCodeSignaturer { get; set; }
        public virtual User.ApplicationUser applicationUser { get; set; }

        [DisplayName("وضعیت امضا")]
        public bool? SignatureStatus { get; set; }

        [DisplayName("علت")]
        [StringLength(1000)]
        public string SignatureReason { get; set; }

        [DisplayName("تاریخ")]
        public DateTime? SignatureDateTime { get; set; }
    }
}
