﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Employees
{
    public class Vacation
    {
        public Vacation()
        {

        }
        [Key]
        public int Vacation_ID { get; set; }

        [DisplayName("علت")]
        [MaxLength(300)]
        //[Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string VacationReason { get; set; }

        [DisplayName("نوع مرخصی")]
        public bool VacationType { get; set; }

        [DisplayName("تاریخ مرخصی ساعتی")]
        public string DateVacationTime { get; set; }

        [DisplayName("ساعت شروع")]
        [MaxLength(10)]
        public string StartTime { get; set; }

        [DisplayName("ساعت پایان")]
        [MaxLength(10)]
        public string EndTime { get; set; }

        [DisplayName("تاریخ شروع ")]
        public string DayVacationStartDate { get; set; }

        [DisplayName("تاریخ پایان")]
        public string DayVacationEndDate { get; set; }

        [DisplayName("مدت")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public byte Duration { get; set; }

        [DisplayName("تاریخ ثبت")]
        public DateTime CreateDate { get; set; }

        [DisplayName("کد پرسنلی")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(450, ErrorMessage = "کد پرسنلی نمی تواند بیش از 450 رقم باشد")]
        [ForeignKey("PersonalCodeFirst")]
        public virtual Employee Employee_V_1 { get; set; }
        public string PersonalCodeFirst { get; set; }


        [DisplayName("کد پرسنلی جایگزین")]
        //[Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(10, ErrorMessage = "کد پرسنلی نمی تواند بیش از 10 رقم باشد")]
        [ForeignKey("PersonalCodeSecond")]
        public virtual Employee Employee_V_2 { get; set; }
        public string PersonalCodeSecond { get; set; }

        [DisplayName("وضعیت امضای جایگزین")]
        public bool? StatusAlternateSignature { get; set; }

        [DisplayName("علت عدم تایید جایگزین")]
        [MaxLength(500)]
        public string AlternateSignatureReason { get; set; }

        [DisplayName("تاریخ تایید جایگزین")]
        public DateTime? AlternateDateTime { get; set; }

        [DisplayName("وضعیت نهایی درخواست")]
        public bool? FinalStatus { get; set; }

        public virtual IEnumerable<VacationSignature> VacationSignatures { get; set; }
    }
}
