﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Contents
{
    public class ContentsVotes
    {
        public ContentsVotes()
        {

        }
        [Key]
        public Int16 ContentVotes_ID { get; set; }
        [DisplayName("مطلب")]
        [ForeignKey("contents")]
        public int ContentId { get; set; }
        [DisplayName("تعداد امتیاز")]
        public Int16 CVoteCount { get; set; }
        [DisplayName("جمع امتیاز")]
        public Int16 CSumVotes { get; set; }
        [DisplayName("میانگین امتیاز")]
        public float CAvgVote { get; set; }
        [DisplayName("تعداد بازدید امتیاز")]
        public Int16 CVisitCount { get; set; }
        public virtual Contents contents { get; set; }
    }
}
