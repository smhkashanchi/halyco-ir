﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Contents
{
   public class ContentsTags
    {
        public ContentsTags()
        {

        }
        [Key]
        public Int16 Tag_ID { get; set; }
        [DisplayName("مطلب")]
        [ForeignKey("contents")]
        public int ContentId { get; set; }
        [DisplayName("کلمات کلیدی")]
        [MaxLength(1000)]
        [Required(ErrorMessage ="{0} را وارد کنید")]
        public string TagText { get; set; }
        public virtual Contents contents { get; set; }
    }
}
