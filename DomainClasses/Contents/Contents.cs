﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace DomainClasses.Contents
{
    public class Contents
    {
        public Contents()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [DisplayName("شناسه مطلب")]
        public int Content_ID { get; set; }
        [DisplayName("نام گروه")]
        [ForeignKey("contentsGroup")]
        public Int16 ContentGroupId { get; set; }

        [DisplayName("عنوان")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(200)]
        public string ContentTitle { get; set; }
        [DisplayName("خلاصه مطلب")]
        [MaxLength(1500)]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [DataType(DataType.MultilineText)]
        public string ContentSummary { get; set; }
        [DisplayName("متن اصلی")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [DataType(DataType.Text)]
        //[AllowHtml]
        public string ContentText { get; set; }
        [DisplayName("تصویر")]
        //[Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(150)]
        public string ContentImage { get; set; }
        public DateTime CreateDate { get; set; }
        [DisplayName("فعال/غیرفعال")]
        public bool IsActive { get; set; }

        public virtual ContentsGroup contentsGroup { get; set; }
        public virtual List<ContentsTags> contentsTags { get; set; }
        public virtual List<ContentsVotes> contentsVotes { get; set; }
        public virtual List<ContentsGallery> contentsGalleries { get; set; }
        public virtual List<ContentFiles> ContentFiles { get; set; }
        public virtual List<ContentsComments> ContentsComments { get; set; }
    }
}
