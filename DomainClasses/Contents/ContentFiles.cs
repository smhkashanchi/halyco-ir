﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Contents
{
    public class ContentFiles
    {
        public ContentFiles()
        {

        }
        //[Key]
        [DisplayName("گالری")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [ForeignKey("AVF")]
        public int AvfId { get; set; }
        //[Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [DisplayName("مطلب")]
        [ForeignKey("contents")]
        public int ContentId { get; set; }
        public virtual Contents contents { get; set; }
        public virtual AVF.AVF AVF{ get; set; }
    }
}
