﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Contents
{
    public class ContentsGroup
    {
        public ContentsGroup()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [DisplayName("شناسه گروه")]
        public Int16 ContentGroup_ID { get; set; }
        [DisplayName("نام گروه")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string ContentGroupName { get; set; }
        [Column(TypeName = "varchar(8)")]
        [ForeignKey("Language")]
        public string Lang { get; set; }

        [DisplayName("فعال/غیرفعال")]
        public bool IsActive { get; set; }
        //[DisplayName("")]
        //[ForeignKey("contentsGroup")]
        public Nullable<Int16> ParentID { get; set; }

        public virtual Language.Language Language { get; set; }
     
        public virtual List<Contents> contents { get; set; }
    }
}
