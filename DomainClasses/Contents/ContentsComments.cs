﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainClasses.Contents
{
    public class ContentsComments
    {
        public ContentsComments()
        {

        }

        [Key]
        public int Comment_ID { get; set; }

        [DisplayName("عنوان مطلب")]
        [ForeignKey("Contents")]
        public int? ContentId { get; set; }
        [DisplayName("گروه مطلب")]
        [ForeignKey("ContentsGroup")]
        public Int16 ContentGroupId { get; set; }
        [DisplayName("متن پیام")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string Comment { get; set; }
        [DisplayName("نام کاربری")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string UserName { get; set; }
        [DisplayName("ایمیل")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        [EmailAddress(ErrorMessage ="لطفا {0} را بصورت صحیح وارد نمایید")]
        public string Email { get; set; }
        [DisplayName("جدید است؟")]
        public bool IsNew { get; set; }
        [DisplayName("تاریخ")]
        public DateTime CommentDate { get; set; }
        [DisplayName("امتیاز مثبت")]
        public byte? PositiveScore { get; set; }
        [DisplayName("امتیاز منفی")]
        public byte? NegativeScore { get; set; }
        [DisplayName("تایید / عدم تایید")]
        public bool IsConfirmed { get; set; }

        public virtual Contents Contents { get; set; }
        public virtual ContentsGroup ContentsGroup { get; set; }
    }
}
